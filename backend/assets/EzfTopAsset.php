<?php

/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class EzfTopAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    
    public $js = [
	'js/jquery.cookie.js'
    ];
    
    public $jsOptions = [
	'position' => \yii\web\View::POS_HEAD
    ];
    
    public $depends = [
	'yii\web\YiiAsset'
    ];

}
