<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class EzfAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/sd.css'
    ];
    public $js = [
        'js/jscondition.js'
    ];

    public $depends = [
	'backend\assets\EzfTopAsset'
    ];
}
