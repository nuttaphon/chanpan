<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class IconAsset extends AssetBundle
{
    public $sourcePath='@backend/modules/ezforms2/assets';

    public $css = [
        'css/fontawesome-iconpicker.min.css',
    ];
    public $js = [
	
        'js/fontawesome-iconpicker.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
