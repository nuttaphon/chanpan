<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class ColorAsset extends AssetBundle
{
    public $basePath = '/';
    public $baseUrl = '@backendUrl';

    public $css = [
        //'css/colorpicker.css',
        //'css/layout.css',
    ];
    public $js = [
//        'js/eye.js',
//        'js/utils.js',
//        //'js/layout.js',
//        'js/colorpicker.js',
	'js/colors.js',
	'js/jqColorPicker.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'common\assets\AdminLte',
        'common\assets\Html5shiv',
    ];
}
