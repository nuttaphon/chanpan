<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class AutoSaveAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        
    ];
    
    public $js = [
        'js/auto_save.js',
    ];

    public $depends = [
	'yii\web\YiiAsset',
    ];
}
