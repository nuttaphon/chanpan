<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/3/14
 * Time: 3:14 PM
 */

namespace backend\assets;

use yii\web\AssetBundle;

class MapAsset extends AssetBundle
{
    public $basePath = '/';
    public $baseUrl = '@backendUrl';

    public $css = [
        
    ];
    public $js = [
        'js/map.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
	'yii\bootstrap\BootstrapAsset',
    ];
}
