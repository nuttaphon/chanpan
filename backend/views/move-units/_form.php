<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use kartik\widgets\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\MoveUnits */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="move-units-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Move Units</h4>
	</div>

	<div class="modal-body">
		<?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

		<?= $form->field($model, 'sitecode_old')->hiddenInput()->label(false) ?>

		<div class="alert alert-danger" role="alert">กรุณาอัปโหลดเอกสารรักษาความลับ หากไม่อัปโหลดเอกสาร ระบบจะไม่อนุญาตให้ย้ายหน่วยงาน</div>
		
		<?php

		echo $form->field($model, 'sitecode_new')->widget(Select2::classname(), [
		    'initValueText' => $dataHospital["code"] . " " . $dataHospital["name"], // set the initial display text
		    'options' => ['placeholder' => 'ค้นหาหน่วยงาน ...'],
    //                        'name'=>'hospitacode',
		    'pluginOptions' => [
			'allowClear' => true,
			'minimumInputLength' => 1,
			'ajax' => [
			    'url' => '/sign-in/findhospital',
			    'dataType' => 'json',
			    'data' => new JsExpression('function(params) { return {q:params.term}; }')
			],
			'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			'templateResult' => new JsExpression('function(city) { return city.text; }'),
			'templateSelection' => new JsExpression('function (city) { return city.text; }'),
		    ],
		    'pluginEvents' => [
			"select2:unselect" => "function() { $('#note-comt').html(''); }",
			"select2:select" => "function(data) { 
				$.ajax({
				    url: '".\yii\helpers\Url::to(['move-units/checkadmin'])."',
				    type: 'GET',
				    data: {sitecode:data.params.data.id},
				    dataType: 'JSON',
				    success: function (result) {
				      if(result.status == 'success'){
					  ". SDNoty::show('result.message', 'result.status') ."
					  $('#note-comt').html(result.text);
				      } 
				      if(result.status == 'unsuccess'){
					$('#note-comt').html('');
				      }
				    }
				});
			    }",
			
		    ],
		]);

		$imgPath = Yii::getAlias('@storageUrl') . '/source/';
		$initialPreview = ($model->file!='')?Html::img($imgPath.$model->file, ['class'=>'file-preview-image']):'';
		?>
		
		
		<?= $form->field($model, 'file')->widget(kartik\widgets\FileInput::classname(),[
		    'pluginOptions' => [
			'previewFileType' => 'image',
			'initialPreview'=>$initialPreview,
			'overwriteInitial'=>true,
			'showRemove' => false,
			'showUpload' => false,
			'allowedFileExtensions'=>['pdf','png','jpg','jpeg'],
			'maxFileSize' => 5000,
		    ],
		    'options' => ['multiple' => false]//,'accept' => 'image/*'
		])->label($text);?>
		<?php
		if($model->file!=''){
		    $file = Yii::getAlias('@storageUrl') . '/source/'.$model->file;
		    
		    $mimeType = backend\modules\ovcca\classes\OvccaFunc::exists($file);
		    $img = $file;
		    if ($mimeType != 'application/pdf') {
			$img = \yii\helpers\Url::to(['/site/view-img', 'img'=>$file]);
		    }
		?>
		<div class=""> <a href="<?=$img?>" target="_blank"><?=$model->file?></a></div>
		<br>
		<?php } ?>
		<?= $form->field($model, 'status')->hiddenInput()->label(false) ?>

		<?= Html::radioList('scom', null,['เลือกหน่วยงานผิด', 'ย้ายหน่วยงานใหม่', 'อื่นๆ ระบุ: '], ['id'=>'scommt']) ?>
		
		<?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

		<?= $form->field($model, 'created_by')->hiddenInput()->label(false) ?>

		<?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

		<?= $form->field($model, 'updated_by')->hiddenInput()->label(false) ?>

		<?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

		<div id="note-comt" style="color: #ff0000;"></div>
	</div>
	<div class="modal-footer">
	    
	    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>
    <?php ActiveForm::end(); ?>
    
<!--    หน่วยงานใหม่ที่ท่านต้องการย้าย มี admin site โปรดติดต่อ admin เพื่ออนุมัติการย้ายหน่วยงาน ชื่อ: e-mail: เบอร์โทร:-->
</div>

<?php  $this->registerJs("
    $('input[name=\"scom\"]').change(function(){
   var value = $('input[name=\"scom\"]:checked').val();
   if(value==0){
	$('#moveunits-comment').val('เลือกหน่วยงานผิด');
    } else if(value==1) {
	$('#moveunits-comment').val('ย้ายหน่วยงานใหม่');
    } else if(value==2) {
	$('#moveunits-comment').val('อื่นๆ ระบุ: ');
	$('#moveunits-comment').focus();
    }
});  

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    $('#modal-move-units .modal-footer').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    
    var \$form = $(this);
    var formData = new FormData($(this)[0]);
    $.ajax({
          url: \$form.attr('action'),
          type: 'POST',
          data: formData,
	  dataType: 'JSON',
	  enctype: 'multipart/form-data',
	  processData: false,  // tell jQuery not to process the data
	  contentType: false,   // tell jQuery not to set contentType
          success: function (result) {
	    if(result.status == 'success'){
		". SDNoty::show('result.message', 'result.status') ."
		if(result.action == 'create'){
		    $(document).find('#modal-move-units').modal('hide');
		    location.reload();
		} else if(result.action == 'update'){
		    $(document).find('#modal-move-units').modal('hide');
		}
	    } else{
		". SDNoty::show('result.message', 'result.status') ."
	    } 
          },
          error: function () {
	    console.log('server error');
          }
      });
    return false;
});

");?>