<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $model backend\models\MoveUnits */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="move-units-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">ขอย้ายหน่วยงาน</h4>
	</div>
	<?php
	$user = \common\models\UserProfile::find()->where('user_id=:user_id',[':user_id'=>$model['user_id']])->one();
	$old = backend\modules\ezforms\components\EzformFunc::getSite($model['sitecode_old']);
	$new = backend\modules\ezforms\components\EzformFunc::getSite($model['sitecode_new']);
	?>
	<div class="modal-body">
	    <?=  Html::a('profile user', ['userlist/update', 'id'=>$model->user_id], ['class'=>'btn btn-success', 'target'=>'_blank'])?>
	    <div class="form-group">
		<?= Html::label('ขอย้ายหน่วยงานโดย') ?>
		<?= Html::textInput('user_code', $user->firstname.' '.$user->lastname, ['class'=>'form-control', 'readonly'=>true]) ?>
	    </div>
	    <div class="form-group">
		<?= Html::label('หน่วยงานเดิม') ?>
		<?= Html::textInput('old_code', $model['sitecode_old'].' '.$old['name'], ['class'=>'form-control', 'readonly'=>true]) ?>
	    </div>
	    <div class="form-group">
		<?= Html::label('หน่วยงานที่ย้าย') ?>
		<?= Html::textInput('new_code', $model['sitecode_new']. ' ' .$new['name'], ['class'=>'form-control', 'readonly'=>true]) ?>
	    </div>
		<?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
		
		<?= $form->field($model, 'sitecode_old')->hiddenInput()->label(false) ?>

		<?= $form->field($model, 'sitecode_new')->hiddenInput()->label(false) ?>
	    
		<dl> 
		    <dt>หมายเหตุ</dt> 
		    <dd><?=$model->comment?></dd>
		</dl>
		
		<?php
		if($model->file!=''){
		    $file = Yii::getAlias('@storageUrl') . '/source/'.$model->file;
		    $mimeType = backend\modules\ovcca\classes\OvccaFunc::exists($file);
		    $img = $file;
		    if ($mimeType != 'application/pdf') {
			$img = \yii\helpers\Url::to(['/site/view-img', 'img'=>$file]);
		    }
		?>
		<div class="">เอกสารรักษาความลับ <a href="<?=$img?>" target="_blank"><?=$model->file?></a></div>
		<br>
		
		<br>
		<?php } ?>
		<?= $form->field($model, 'status')->radioList(['approve'=>'อนุมัติ', 'disapproval'=>'ไม่อนุมัติ']) ?>

		<?= $form->field($model, 'file')->hiddenInput()->label(false) ?>
		
		<?= $form->field($model, 'feedback')->textarea(['rows' => 3]) ?>

		<?= $form->field($model, 'created_by')->hiddenInput()->label(false) ?>

		<?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

		<?= $form->field($model, 'updated_by')->hiddenInput()->label(false) ?>

		<?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

	</div>
	<div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('#moveunits-status').change(function(){
   var value = $('input[type=radio]:checked').val();
   if(value=='approve'){
	$('#moveunits-feedback').val('ระบบได้ทำการย้ายหน่วยงานให้ท่านแล้ว');
    } else {
	$('#moveunits-feedback').val('ระบบไม่สามารถย้านหน่วยงานให้ท่านได้ เนื่องจากเอกสารไม่ถูกต้อง โปรดอัพโหลดเข้ามาใหม่ หรือ ข้อย้ายหน่วยงานอีกครั้ง');
    }
});    

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    $('#modal-move-units .modal-footer').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create'){
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#move-units-grid-pjax'});
	    } else if(result.action == 'update'){
		$(document).find('#modal-move-units').modal('hide');
		$.pjax.reload({container:'#move-units-grid-pjax'});
	    }
	} else{
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function(){
	console.log('server error');
    });
    return false;
});

");?>