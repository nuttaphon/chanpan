<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MoveUnits */

$this->title = Yii::t('app', 'Create Move Units');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Move Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="move-units-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
