<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MoveUnits */

$this->title = 'Move Units#'.$model->muid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Move Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="move-units-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'muid',
		'user_id',
		'sitecode_old',
		'sitecode_new',
		'status',
		'file:ntext',
		'comment:ntext',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
	    ],
	]) ?>
    </div>
</div>
