<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ScriptValidateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Script Validates');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="script-validate-index">

    <?php  // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="btn-group" role="group" >
	<a href="<?=  Url::to(['script-validate/index'])?>" class="btn btn-success">Create Rule</a>
	<a href="<?=  Url::to(['script-validate/group'])?>" class="btn btn-default">Results</a>
	<?=  Html::a('Run All Script', ['/script-validate/script'], ['class'=>'btn btn-warning']);//, 'target'=>'_blank'?>
    </div>
    <p style="padding-top: 10px;">
	
    </p>

    <?php  Pjax::begin(['id'=>'script-validate-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'script-validate-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['script-validate/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-script-validate']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['script-validate/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-script-validate', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionScriptValidateIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            //'sid',
            'title',
            'description:ntext',
	    'name',
            //'sql:ntext',
            //'template:ntext',
	    'return',
	    [
		'attribute'=>'rating',
		'value'=>function ($data){ 
		    $rating = $data['rating'];
		    $str = '';
		    for($i=1;$i<=5;$i++){
			if($rating>=$i){
			    $str .= '<i class="glyphicon glyphicon-star"></i> ';
			} else {
			    $str .= '<i class="glyphicon glyphicon-star-empty"></i> ';
			}
		    }
		    return $str;
		},
		'format' => 'raw',	
	    ],
             'enable',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{testing} {view} {update} {delete}',
		'buttons' => [
                    'testing' => function ($url, $model) {
			
			return Html::a('<span class="glyphicon glyphicon-search"></span>', $url, [
			    'title' => Yii::t('app', 'Testing'),
			    'data-action' => 'testing',
			    'data-pjax' => 0,
			]);
                    },
                ],
		'contentOptions' => ['style' => 'width:100px;']	    
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-script-validate',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#script-validate-grid-pjax').on('click', '#modal-addbtn-script-validate', function() {
    modalScriptValidate($(this).attr('data-url'));
});

$('#script-validate-grid-pjax').on('click', '#modal-delbtn-script-validate', function() {
    selectionScriptValidateGrid($(this).attr('data-url'));
});

$('#script-validate-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#script-validate-grid').yiiGridView('getSelectedRows');
	disabledScriptValidateBtn(key.length);
    },100);
});

$('#script-validate-grid-pjax').on('click', '.selectionScriptValidateIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledScriptValidateBtn(key.length);
});

$('#script-validate-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalScriptValidate('".Url::to(['script-validate/update', 'id'=>''])."'+id);
});	

$('#script-validate-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view' || action === 'testing') {
	modalScriptValidate(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#script-validate-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledScriptValidateBtn(num) {
    if(num>0) {
	$('#modal-delbtn-script-validate').attr('disabled', false);
    } else {
	$('#modal-delbtn-script-validate').attr('disabled', true);
    }
}

function selectionScriptValidateGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionScriptValidateIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#script-validate-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalScriptValidate(url) {
    $('#modal-script-validate .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-script-validate').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>