<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ScriptValidateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Script Groups');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="script-group-index">

  
    <?php  // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="btn-group" role="group" >
	<a href="<?=  Url::to(['script-validate/index'])?>" class="btn btn-default">Create Rule</a>
	<a href="<?=  Url::to(['script-validate/group'])?>" class="btn btn-success">Results</a>
    </div>
    
    <br><br>
    <?php  Pjax::begin(['id'=>'script-group-grid-pjax']);?>
    <?= GridView::widget([
    'id' => 'script-group-grid',
    'panelBtn' => '',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
        'columns' => [
        
        [
        'class' => 'yii\grid\SerialColumn',
        'headerOptions' => ['style'=>'text-align: center;'],
        'contentOptions' => ['style'=>'width:60px;text-align: center;'],
        ],

            [
		'attribute'=>'created_at',
		'label'=>'Date',
		'value'=>function ($data){ 
		    return \common\lib\sdii\components\utils\SDdate::mysql2phpDateTime($data['created_at']);
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:130px; width:130px; text-align: center;'],	
	    ],
            'passed',
            'error',
            'total',
            

        [
	    'class' => 'appxq\sdii\widgets\ActionColumn',
	    'contentOptions' => ['style'=>'width:80px;text-align: center;'],
	    'template' => '{link}',
	    'buttons' => [
		'link' => function ($url, $model) {

		    return Html::a('<span class="glyphicon glyphicon-search"></span>', Url::to(['script-validate/result', 'gid'=>$model->gid]), [
			'title' => Yii::t('app', 'Detail'),
			'data-action' => 'link',
			'data-pjax' => 0,
		    ]);
		},
	    ],
        ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>
