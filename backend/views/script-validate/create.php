<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ScriptValidate */

$this->title = Yii::t('app', 'Create Script Validate');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Script Validates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="script-validate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
