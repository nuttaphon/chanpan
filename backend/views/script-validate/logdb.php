<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Result#'.$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Testing'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="script-validate-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
	<span class="label label-primary"><?=$model->description?></span> <span class="label label-success">[<?=$model->name?>]</span>
	<br>
        <?=$model->result?>
    </div>
</div>
