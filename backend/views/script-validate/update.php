<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ScriptValidate */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Script Validate',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Script Validates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->sid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="script-validate-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
