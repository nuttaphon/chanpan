<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\models\ScriptValidate */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="script-validate-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Script Validate</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	<div class="row">
	    <div class="col-md-6" >
		<?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
	    </div>
	    <div class="col-md-6 sdbox-col" >
		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		
	    </div>
	</div>
	
	<div class="row">
	    <div class="col-md-4" >
		<?= $form->field($model, 'enable')->checkbox() ?>
	    </div>
	    <div class="col-md-4 sdbox-col" >
		<?= $form->field($model, 'rating', ['inline'=>true])->radioList([1=>1,2=>2,3=>3,4=>4,5=>5]) ?>
	    </div>
	    <div class="col-md-4 sdbox-col" >
		<?= $form->field($model, 'return', ['inline'=>true])->radioList(['one'=>'one', 'all'=>'all']) ?>
	    </div>
	</div>
	
	<?= $form->field($model, 'sql')->textarea(['rows' => 4]) ?>
	<?= $form->field($model, 'sql_log')->textarea(['rows' => 4]) ?>
	<div class="row">
	    <div class="col-md-6" >
		<?= $form->field($model, 'template')->textarea(['rows' => 4]) ?>
	    </div>
	    <div class="col-md-6 sdbox-col" >
		<?= $form->field($model, 'process')->textarea(['rows' => 4])->hint('Use $fields set property.') ?>
	    </div>
	</div>
	
    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#script-validate-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-script-validate').modal('hide');
		$.pjax.reload({container:'#script-validate-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>