<?php
use kartik\select2\Select2;
use backend\models\Ezform;
?>
<div class="row"><label class="control-label" for="tbltree-name">คำอธิบาย</label> </div>
<div class="row">
<div class="col-md-2 col-md-offset-1"><span class="text-warning kv-node-icon kv-icon-parent fa fa-folder kv-node-closed"> = หมวด(หุบ)</span> </div>
<div class="col-md-2"><span class="text-warning kv-node-icon kv-icon-parent fa fa-folder-open kv-node-opened"> = หมวด(คลี่)</span> </div>
<div class="col-md-2"><span class="text-info kv-node-icon kv-icon-parent fa fa-file kv-node-file"> = หมวด(ว่าง)</span> </div>
<div class="col-md-2"><span class="text-info kv-node-icon kv-icon-parent fa fa-institution kv-node-closed"> = โครงการ</span></div>
<div class="col-md-3"><span class="text-info kv-node-icon kv-icon-parent fa fa-user kv-node-user"> = หมวด(ส่วนตัว)</span> </div>
</div>
<?php
if (Yii::$app->user->can('administrator')) {
    echo "<h4>สำหรับผู้ดูแลระบบ</h4><hr>";
    echo $form->field($node, 'readonly')->checkbox(['label' => 'หัวข้อกิจกรรมของส่วนกลาง']);
    //echo $form->field($node, 'collapsed')->checkbox(['label' => 'หุบกิจกรรมไว้']);
    //
    //echo $form->field($node, 'readonly', 'ประเภทกิจกรรม')->widget(Select2::classname(), [
    //    'data' => [1 => "กิจกรรมของระบบ (เห็นทุกคน, แก้ไขไม่ได้)", 0 => "กิจกรรมส่วนตัว"],
    //    'options' => ['placeholder' => 'กรุณาเลือกชนิด'],
    //    'pluginOptions' => [
    //        'allowClear' => false
    //    ],
    //]);
    
    echo $form->field($node, 'userid')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(backend\models\UserProfile::find()->all(),'user_id','firstname'),
        'options' => ['placeholder' => 'กรุณาเลือก'],
        'pluginOptions' => [
            'allowClear' => false
        ],
    ]);
//    $node->ezf_id = explode(',', $node->ezf_id);
//    echo $form->field($node, 'ezf_id')->widget(Select2::classname(), [
//        'data' => \yii\helpers\ArrayHelper::map(backend\models\Ezform::find()->where('ezf_id>100000 and status=1')->all(),'ezf_id','ezf_name'),
//        'options' => ['placeholder' => 'กรุณาเลือกฟอร์มบันทึกข้อมูล'],
//        'pluginOptions' => [
//            'allowClear' => true,
//            'multiple'  => true,
//        ],
//    ]);
}
?>