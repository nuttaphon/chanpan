<?php

use yii\helpers\Html;
use kartik\tree\TreeView;
use kartik\tree\TreeViewInput;
use backend\models\TblTree;
use yii\helpers\Url;

$this->title = Yii::t('backend', 'จัดหมวดกิจกรรม');
$this->params['breadcrumbs'][] = $this->title;

if (TblTree::find()->count() == 0) {
            $home = new TblTree(['name' => 'หน่วยงาน','readonly'=>'1','userid'=>Yii::$app->user->id]);
            $home->makeRoot();
            $home = new TblTree(['name' => 'การวิจัย','readonly'=>'1','userid'=>Yii::$app->user->id]);
            $home->makeRoot();
            $site1 = new TblTree(['name' => 'โครงการหลัก','readonly'=>'1','userid'=>Yii::$app->user->id]);
            $site1->prependTo($home);            
            $home = new TblTree(['name' => 'ส่วนตัว','readonly'=>'1','userid'=>Yii::$app->user->id]);
            $home->makeRoot();
}


//echo TreeViewInput::widget([
//    // single query fetch to render the tree
//    // use the Product model you have in the previous step
//    'query' => TblTree::find()->addOrderBy('root, lft'),
//    'headingOptions'=>['label'=>'Categories'],
//    'name' => 'kv-product', // input name
////    'value' => '1,2,3',     // values selected (comma separated for multiple select)
//    'asDropdown' => true,   // will render the tree input widget as a dropdown.
//    'multiple' => false,     // set to false if you do not need multiple selection
//    'fontAwesome' => true,  // render font awesome icons
//    'rootOptions' => [
//        'label'=>'<i class="fa fa-home"></i> คณะสาธารณสุขศาสตร์',  // custom root label
//        'class'=>'text-success'
//    ],
//    //'options'=>['disabled' => true],
//]);
//echo "<br><br>";
//echo "<br><br>";
//echo "<br><br>";echo "<br><br>";
//
//echo "<br><br>";
?>

    <div class="box">
    <div class="box-body">
<?php 
echo \kartik\tree\TreeView::widget([
    'query' => TblTree::find()->where('readonly=1 or userid='.Yii::$app->user->id.' or id IN (select distinct root from tbl_tree where userid='.Yii::$app->user->id.')')->addOrderBy('root, lft'),
    'headingOptions' => ['label' => 'หมวดกิจกรรม'],
    'rootOptions' => ['label'=>'<span class="text-primary">Root</span>'],
    'fontAwesome' => true,
    'isAdmin' => false,
    'displayValue' => 1,
    'rootOptions' => [
        'label'=>'<i class="fa fa-home"></i> Root',  // custom root label
        'class'=>'text-success'
    ],
    'iconEditSettings'=> [
        'show' => 'list',
        'listData' => [
            'institution' => 'โครงการ',
//            'newspaper-o' => 'ฟอร์มบันทึกข้อมูล',
        ]
    ],
    'softDelete' => true,
    'cacheSettings' => ['enableCache' => true],
    'nodeAddlViews' => [
        kartik\tree\Module::VIEW_PART_2 => '@backend/views/tbl-tree/extra',
//        kartik\tree\Module::VIEW_PART_1 => '@backend/views/tbl-tree/addnew',
    ],
    'nodeActions' => [
        kartik\tree\Module::NODE_MOVE => Url::to(['/treemanager/node/movex']),
    ],
//    'toolbar'  => [
//        TreeView::BTN_CREATE => [
//            'icon' => 'plus',
//            'options' => ['title' => Yii::t('kvtree', 'Add new'), 'disabled' => true]
//        ],
//        TreeView::BTN_CREATE_ROOT => [
//            'icon' => 'tree',
//            'options' => ['title' => Yii::t('kvtree', 'Add new root')]
//        ],
//        TreeView::BTN_REMOVE => [
//            'icon' => 'trash',
//            'options' => ['title' => Yii::t('kvtree', 'Delete'), 'disabled' => true]
//        ],
//        TreeView::BTN_SEPARATOR,
//        TreeView::BTN_MOVE_UP => [
//            'icon' => 'arrow-up',
//            'options' => ['title' => Yii::t('kvtree', 'Move Up'), 'disabled' => true]
//        ],
//        TreeView::BTN_MOVE_DOWN => [
//            'icon' => 'arrow-down',
//            'options' => ['title' => Yii::t('kvtree', 'Move Down'), 'disabled' => true]
//        ],
//        TreeView::BTN_MOVE_LEFT => [
//            'icon' => 'arrow-left',
//            'options' => ['title' => Yii::t('kvtree', 'Move Left'), 'disabled' => true]
//        ],
//        TreeView::BTN_MOVE_RIGHT => [
//            'icon' => 'arrow-right',
//            'options' => ['title' => Yii::t('kvtree', 'Move Right'), 'disabled' => true]
//        ],
//        TreeView::BTN_SEPARATOR,
//        TreeView::BTN_REFRESH => [
//            'icon' => 'refresh',
//            'options' => ['title' => Yii::t('kvtree', 'Refresh')],
//            'url' => Yii::$app->request->url
//        ],
//    ]
]);

if (!Yii::$app->user->can('administrator')) {
    $this->registerJs("
        $('.kv-move-up').hide();
        $('.kv-move-down').hide();
        $('.kv-move-left').hide();
        $('.kv-move-right').hide();
        $('.kv-create-root').hide();
    ");
}




?>
    </div></div>