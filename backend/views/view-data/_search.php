<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TbdataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbdata-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'xsourcex') ?>

    <?= $form->field($model, 'rstat') ?>

    <?= $form->field($model, 'user_create') ?>

    <?= $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'user_update') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'var1') ?>

    <?php // echo $form->field($model, 'var2') ?>

    <?php // echo $form->field($model, 'var3') ?>

    <?php // echo $form->field($model, 'var4') ?>

    <?php // echo $form->field($model, 'var5') ?>

    <?php // echo $form->field($model, 'var6') ?>

    <?php // echo $form->field($model, 'var7') ?>

    <?php // echo $form->field($model, 'var8') ?>

    <?php // echo $form->field($model, 'var9') ?>

    <?php // echo $form->field($model, 'var10') ?>

    <?php // echo $form->field($model, 'var11') ?>

    <?php // echo $form->field($model, 'var12') ?>

    <?php // echo $form->field($model, 'var13') ?>

    <?php // echo $form->field($model, 'var14') ?>

    <?php // echo $form->field($model, 'var15') ?>

    <?php // echo $form->field($model, 'var16') ?>

    <?php // echo $form->field($model, 'var17') ?>

    <?php // echo $form->field($model, 'var18') ?>

    <?php // echo $form->field($model, 'var19') ?>

    <?php // echo $form->field($model, 'var20') ?>

    <?php // echo $form->field($model, 'var21') ?>

    <?php // echo $form->field($model, 'var22') ?>

    <?php // echo $form->field($model, 'var23') ?>

    <?php // echo $form->field($model, 'var24') ?>

    <?php // echo $form->field($model, 'var25') ?>

    <?php // echo $form->field($model, 'var26') ?>

    <?php // echo $form->field($model, 'var27') ?>

    <?php // echo $form->field($model, 'var28') ?>

    <?php // echo $form->field($model, 'var29') ?>

    <?php // echo $form->field($model, 'var30') ?>

    <?php // echo $form->field($model, 'var31') ?>

    <?php // echo $form->field($model, 'var32') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
