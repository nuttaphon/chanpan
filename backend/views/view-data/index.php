<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TbdataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ดูข้อมูล/ส่งออกข้อมูล';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
a.asc:after {
  content: "\e151";
}
a.asc:after, a.desc:after {
  position: relative;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: normal;
  padding-left: 5px;
}
:after, :before {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
a.desc:after {
  content: "\e152";
}
a.asc:after, a.desc:after {
  position: relative;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: normal;
  padding-left: 5px;
}
</style>
<div class="tbdata-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
        <?= Html::a('Create Tbdata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    <br>
    <?= Html::a('<i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;จัดการฟอร์ม', ['/ezforms/ezform/update', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-comments-o"></i>&nbsp;&nbsp;&nbsp;เพิ่มคำถามพิเศษ', ['/component/ezform-component/index', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;&nbsp;สร้าง EZForm จาก Excel', ['/file-upload', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-globe"></i>&nbsp;&nbsp;&nbsp;ดูฟอร์มออนไลน์', ['/ezforms/ezform/viewform', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-primary btn-flat']) ?>
    <?= Html::a('<i class="fa fa-table"></i>&nbsp;&nbsp;&nbsp;ดูข้อมูล/ส่งออกข้อมูล', ['/view-data', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-info btn-flat ']) ?>
    <div style='float:right'> 
        <?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/ezforms/ezform/', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <br>
    <br>
    
    <div class="box">
    <div class="box-body">
<?php
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
//$columns = [
//    ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
//    'var2',
//    'var3',
//    'var4',
//    'var6',
//    'var7',
//    'var9',
//    'var10',
//    'var11',
//    'var12',
//    'var13',
//    'var14',
//    'var15',
//    'var16',
//    'var17',
//    'var18',
//    'var19',
//    'var20',
//    ['class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT]
//];

        $columns['0']['class']='kartik\grid\SerialColumn';
        $columns['0']['order']=DynaGrid::ORDER_FIX_LEFT;
        $aindex=1;
        $params=Yii::$app->request->queryParams;
        $ezfid=$params['id'];
		
		$arrField = ArrayHelper::map($dataField, 'ezf_field_name', 'ezf_field_label');
       
//        $ezfield = Yii::$app->db->createCommand('SELECT * FROM `ezform_fields` WHERE ezf_id =\''.$ezfid.'\'')->query()->readAll();
        if (count($dataColumn)>0) {
            foreach($dataColumn as $key => $form) {
				
                if(in_array($form["column"], ['xsourcex', 'target',  'user_create', 'create_date', 'user_update', 'update_date']))
						continue;
				$title = ($arrField[$form["column"]]!='')?$arrField[$form["column"]]:$form["column"];
				if($form['column']=='rstat'){
				    $columns[$aindex]=[
					'attribute'=>$form['column'],
					'label'=>$form['column'],
					'value' => function ($data)
					    {
						$rstat = '';
						if($data['rstat']==2){
						    $rstat = 'Safe draft';
						} elseif ($data['rstat']==4) {
						    $rstat = 'Submitted';
						} elseif ($data['rstat']==3) {
						    $rstat = 'Delete';
						} 
						return $rstat;
					    },
					'noWrap' => TRUE,
					'sortLinkOptions' =>['data-toggle'=>'tooltip', 'title'=>$title],
					//'headerOptions'=>['style'=>'text-align: center; width:100px;'],
					//'contentOptions'=>['style'=>'width:100px; text-align: center;'],
				    ];
				}else {
				    $columns[$aindex]=[
					'attribute'=>$form['column'],
					'label'=>$form['column'],
					'noWrap' => TRUE,
					'sortLinkOptions' =>['data-toggle'=>'tooltip', 'title'=>$title],
					//'headerOptions'=>['style'=>'text-align: center; width:100px;'],
					//'contentOptions'=>['style'=>'width:100px; text-align: center;'],
				    ];
				}
                
                $aindex++;
            }
			
        }
        $columns[$aindex]['class']='kartik\grid\CheckboxColumn';
        $columns[$aindex]['order']=DynaGrid::ORDER_FIX_RIGHT;   


        

     ?>
    <?php 
//use kartik\export\ExportMenu;

//$gridColumns = [
//    ['class' => 'yii\grid\SerialColumn'],
//    'id',
//    'name',
//    'color',
//    'publish_date',
//    'status',
//    ['class' => 'yii\grid\ActionColumn'],
//];
//
//        $columns['0']['class']='yii\grid\SerialColumn';
//        $aindex=1;
//        $params=Yii::$app->request->queryParams;
//        $ezfid=$params['id'];
//        
//        $ezfield = Yii::$app->db->createCommand('SELECT * FROM `ezform_fields` WHERE ezf_id =\''.$ezfid.'\'')->query()->readAll();
//        if (count($ezfield)>0) {
//            foreach($ezfield as $key => $form) {
//                $columns[$aindex]=$form['ezf_field_name'];
//                $aindex++;
//            }
//        }
//        $columns[$aindex]['class']='yii\grid\ActionColumn';
   
        
// Renders a export dropdown menu
//echo ExportMenu::widget([
//    'dataProvider' => $dataProvider,
//    'columns' => $columns
//]);

// You can choose to render your own GridView separately
//echo "<div class='table-responsive' style='overflow-x: auto;'>";        
//echo \kartik\grid\GridView::widget([
//    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'columns' => $columns,
//    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
//    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
//    'pjax'=>true, // pjax is set to always true for this demo
//    'beforeHeader'=>[
//        [
//            'columns'=>[
//                ['content'=>'Header Before 1', 'options'=>['colspan'=>5, 'class'=>'text-center warning']], 
//                ['content'=>'Header Before 2', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
//                ['content'=>'Header Before 3', 'options'=>['colspan'=>3, 'class'=>'text-center warning']], 
//            ],
//            'options'=>['class'=>'skip-export'] // remove this row from export
//        ]
//    ],
//    // set your toolbar
//    'toolbar'=> [
//        ['content'=>
//            Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=>Yii::t('kvgrid', 'Reset Grid')])
//        ],
//        '{export}',
//        '{toggleData}',
//    ],
//    'responsive' => true,
//    // set export properties
//    'export'=>[
//        'fontAwesome'=>true
//    ],
//    // parameters from the demo form
//
//]);    
//echo "</div>";
//print_r($columns);exit;
             
$dynagrid = DynaGrid::begin([
    'columns'=>$columns,
    'theme'=>'panel-info',
    'showPersonalize'=>true,
    'storage'=>'cookie',
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        'filterModel'=>$searchModel,
        'showPageSummary'=>true,
        'floatHeader'=>true,
		'responsiveWrap'=>FALSE,
//        'export'=>true,   //Got error at server
        'pjax'=>true,
        'panel'=>[
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ชื่อฟอร์ม '.$dataForm['ezf_name'].'</h3>',
            'before' =>  '<div style="padding-top: 7px;"><em>* ท่านสามารถส่งออกข้อมูลได้โดยการเลือกปุ่มส่งออก ที่ด้านขวาบนของตาราง</em></div>',
            'after' => false
        ],        
        'toolbar' =>  [
            ['content'=>'{dynagrid}'],
            '{export}',
        ],
    ],
    'options'=>['id'=>'dynagrid-1','pageSize'=>50,] // a unique identifier is important
]);

if (substr($dynagrid->theme, 0, 6) == 'simple') {
    $dynagrid->gridOptions['panel'] = false;
}  
DynaGrid::end();  
    ?>

</div>
    </div></div>