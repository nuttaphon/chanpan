<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Tbdata */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tbdatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbdata-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'xsourcex',
            'rstat',
            'user_create',
            'create_date',
            'user_update',
            'update_date',
            'var1',
            'var2',
            'var3',
            'var4',
            'var5',
            'var6',
            'var7',
            'var8',
            'var9',
            'var10',
            'var11',
            'var12',
            'var13',
            'var14',
            'var15',
            'var16',
            'var17',
            'var18',
            'var19',
            'var20',
            'var21',
            'var22',
            'var23',
            'var24',
            'var25',
            'var26',
            'var27',
            'var28',
            'var29',
            'var30',
            'var31',
            'var32',
        ],
    ]) ?>

</div>
