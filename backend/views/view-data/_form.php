<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Tbdata */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbdata-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'xsourcex')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rstat')->textInput() ?>

    <?= $form->field($model, 'user_create')->textInput() ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'user_update')->textInput() ?>

    <?= $form->field($model, 'update_date')->textInput() ?>

    <?= $form->field($model, 'var1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var9')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var10')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var11')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var12')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var13')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var14')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var15')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var16')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var17')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var18')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var19')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var20')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var21')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var22')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var23')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var24')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var25')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var26')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var27')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var28')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var29')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var30')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var31')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'var32')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
