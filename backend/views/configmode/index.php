<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = Yii::t('backend', 'Config Mode');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Config Mode'), 'url' => ['/configmode/index']];
?>

<?php $form = ActiveForm::begin(['id'=>$siteconfig->formName()]); ?>
<h4><i class="fa fa-home"></i>  ตั้งค่าเว็บไซต์</h4><br>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($siteconfig, 'sitename')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($siteconfig, 'sitemode')->radioList(
                [
                    1 => 'Faculty Mode', 
                    2 => 'Patient Mode',
                    3 => 'Mixed Mode'
                ]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-3">
	    <?= Html::submitButton($siteconfig->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $siteconfig->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</dvi>
<?php //var_dump(Yii::$app->user->identity,10,true); ?>
