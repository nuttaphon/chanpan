<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$url_condition="/verification/datasummarycca01advance/?orderby=".$_vget['orderby'];
$url_sort="/verification/datasummarycca01advance/?criteriamoreth=".$_vget['criteriamoreth']."&obj1=".$_vget['obj1']."&obj2=".$_vget['obj2']."&obj3=".$_vget['obj3'];

?>
<style>
.form-group input[type="checkbox"] {
    display: none;
}

.form-group input[type="checkbox"] + .btn-group > label span {
    width: 20px;
}

.form-group input[type="checkbox"] + .btn-group > label span:first-child {
    display: none;
}
.form-group input[type="checkbox"] + .btn-group > label span:last-child {
    display: inline-block;   
}

.form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
    display: inline-block;
}
.form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
    display: none;   
}
</style>
<script type="text/javascript">
    function selectCriteriaMoreThan(obj)
    {
        window.open(obj.options[obj.selectedIndex].value,'_self');
    }
    function selectAddOption(vobj1,vobj2,vobj3){
        var url="<?php echo $url_condition; ?>&criteriamoreth=1";
        var obj1=document.getElementById(vobj1);
        var obj2=document.getElementById(vobj2);
        var obj3=document.getElementById(vobj3);
        if( obj1.checked==true ){
            url=url+'&obj1=1';
        }
        if( obj2.checked==true ){
            url=url+'&obj2=1';
        }
        if( obj3.checked==true ){
            url=url+'&obj3=1';
        }
        window.open(url,'_self');
    }
</script>
<div class="container">
    <h2>
        Data Monitor Tools
    </h2>
    <h2>
        สรุปการนำเข้าข้อมูล 
        แบบกำหนดเงื่อนไขในส่วน CCA-01 ตามเกณฑ์การคัดเข้า
    </h2>
    <p>
        <a href="/verification/datasummary/" class="btn btn-primary btn-small"><< ย้อนกลับไปแสดงทั้งหมด</a>
        <select class="btn btn-primary btn-large" id="selectCriteria" onchange="selectCriteriaMoreThan(this)">
            <option value="<?php echo $url_condition; ?>">ตามจำนวนที่นำเข้าทั้งหมด</option>
            <option value="<?php echo $url_condition; ?>&criteriamoreth=1" <?php if($_vget['criteriamoreth']=='1') { echo "selected"; } ?>>อย่างน้อย อายุตั้งแต่ 40 ปีขึ้นไป (อย่างน้อย 1 อย่าง) <?php if($_vget['criteriamoreth']=='1') { echo "และสามารถ + เพิ่มเงื่อนไขได้ตามที่เลือก"; } ?></option>
            <option value="<?php echo $url_condition; ?>&criteriamoreth=2" <?php if($_vget['criteriamoreth']=='2') { echo "selected"; } ?>>ผ่านเกณฑ์การคัดเข้าอย่างน้อย 2 อย่าง</option>
            <option value="<?php echo $url_condition; ?>&criteriamoreth=3" <?php if($_vget['criteriamoreth']=='3') { echo "selected"; } ?>>ผ่านเกณฑ์การคัดเข้าอย่างน้อย 3 อย่าง</option>
            <option value="<?php echo $url_condition; ?>&criteriamoreth=4" <?php if($_vget['criteriamoreth']=='4') { echo "selected"; } ?>>ผ่านเกณฑ์การคัดเข้าอย่างน้อย 4 อย่าง</option>
        </select>
<?php
    if($_vget['criteriamoreth']=='1'){
?>
    <table width="100%" border="0">
        <tr>
            <td width="18%"></td>
            <td width="82%">
        <div class="[ form-group ]">
            <input type="checkbox" <?php if($_vget['obj1']=='1'){ echo "checked=\"checked\""; } ?> value="1" onclick="selectAddOption('obj1','obj2','obj3')" name="obj1" id="obj1" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="obj1" class="[ btn btn-primary ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="obj1" class="[ btn btn-default ]">
                    มีประวัติ เคยติดเชื่อพยาธิใบไม้ตับ
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" <?php if($_vget['obj2']=='1'){ echo "checked=\"checked\""; } ?> value="1" onclick="selectAddOption('obj1','obj2','obj3')" name="obj2" id="obj2"  autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="obj2" class="[ btn btn-success ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="obj2" class="[ btn btn-default ]">
                    เคยกินยาฆ่าพยาธิใบไม้ตับ
                </label>
            </div>
        </div>
        <div class="[ form-group ]">
            <input type="checkbox" <?php if($_vget['obj3']=='1'){ echo "checked=\"checked\""; } ?> value="1" onclick="selectAddOption('obj1','obj2','obj3')" name="obj3" id="obj3" autocomplete="off" />
            <div class="[ btn-group ]">
                <label for="obj3" class="[ btn btn-info ]">
                    <span class="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label for="obj3" class="[ btn btn-default ]">
                    มีประวัติการกินปลาน้ำจืดที่มีเกล็ด สุกๆ ดิบๆ
                </label>
            </div>
        </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
<?php
        $iobj=1;
        if( $_vget['obj1']=='1' ){
            $iobj++;
        }
        if( $_vget['obj2']=='1' ){
            $iobj++;
        }
        if( $_vget['obj3']=='1' ){
            $iobj++;
        }
        if($_vget['obj1']!='1' && $_vget['obj1']!='1' && $_vget['obj1']!='1'){
            
        }else{
            echo "<span class=\"text-danger\">ผ่านเกณฑ์การคัดเข้า ".$iobj." อย่าง</span>";
        }
?>
            </td>
        </tr>
    </table>
<?php
    }
?>
    </p>
  <table width="100%" class="table">
      <tr>
          <td width="50%"><p>CCA-01: ลงทะเบียนและลงข้อมูลพื้นฐาน (คน)</p></td>
          <td width="50%"></td>
      </tr>
  </table>
  <table width="100%" class="table">
      <tr>
          <td>
            <table class="table table-striped">
              <thead>
                <tr class="success">
                    <th>ลำดับ</th>
                    <th><a href="<?php echo $url_sort; ?>">จำนวน</a></th>
                    <th>อำเภอ</th>
                    <th><a href="<?php echo $url_sort; ?>&orderby=province">จังหวัด</a></th>
                    <th>เขต</th>
                    <th>หมายเหตุ</th>
                </tr>
              </thead>
              <tbody>
<?php
    $irow=1;
    if( count($sumCCA01)>0 ){
        foreach($sumCCA01 as $k => $v){
            $amphurkey=$sumCCA01[$k]['provincecode'].$sumCCA01[$k]['amphurcode'];
            $amptxt=$hospitalOfAmp['amphur'][$amphurkey]['amphur'];
            if(strlen($amptxt)==0){
                $amptxt="Province:".$sumCCA01[$k]['provincecode']." Amphur:".$sumCCA01[$k]['amphurcode'];
            }
?>
                <tr>
                    <td><?php echo $irow; ?></td>
                    <td><?php echo $sumCCA01[$k]['recs']; ?></td>
                    <!-- td><a href="/verification/datasummaryamphur/?province=<?php echo $sumCCA01[$k]['provincecode'];?>&amphur=<?php echo $sumCCA01[$k]['amphurcode'];?>" class="text-success"><?php echo $hospitalOfAmp['amphur'][$amphurkey]['amphur']; ?></a></td -->
                    <td><?php echo $hospitalOfAmp['amphur'][$amphurkey]['amphur']; ?></td>
                    <td><?php echo $hospitalOfAmp['amphur'][$amphurkey]['province']; //$sumAmphur[$k]['provincecode']; ?></td>
                    <td><?php echo $sumCCA01[$k]['zone_code']; ?></td>
                    <td class="text-danger"><?php  
            if( strlen($setupUS['amphur'][$amphurkey]['amphurcode'])>0 ){
                echo "ได้รับเครื่องแล้ว";
            }
                  ?></td>
                </tr>
<?php
            $irow++;
        }
    }
?>
              </tbody>
            </table>
          </td>
      </tr>
  </table>
</div>
<?php
if( 0 ){
    echo "<pre align='left'>";
    print_r($sumCCA01);
    echo "</pre>";
}