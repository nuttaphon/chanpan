<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * URL: for test
 *  http://backend.yii2-starter-kit.dev/verification/screening
 */
//echo "Screening page";
//echo $z;
//print_r($result);
if( 0 ){
    echo "<pre align='left'>";
    //print_r($_COOKIE);
    //print_r($_SESSION);
    //print_r($_SERVER);
    //print_r($userdet);
    //print_r($userid);
    //print_r($userprof);
    print_r($userhosp);
    //print_r($puser_login);
    //echo "\n1435745159010041100";
    echo "</pre>";
}

if( 0 ){
    
}
?>
<table class="table">
    <thead>
        <tr>
            <th colspan="2">CASCAP Report</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="left" colspan="2">
                <?php
                    if( 0 ){
                        echo $phpsessionid;
                        echo $valuetime;
                        echo "<br />".$userdet[0]["username"];
                        echo "<br />".$userdet[0]["auth_key"];
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td class="left" colspan="2">
                <span class="glyphicon glyphicon-cog"></span>
<?php
    $url="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=cascaptools&id=#username#&auth=#auth#";
    $url=str_replace("#username#",$userdet[0]["username"],$url);
    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);
    
?>
                <a href="<?php echo $url; ?>" target="_blank">เข้าสู่ระบบ CASCAP Tools</a>
            </td>
        </tr>
        <tr>
            <td class="left" colspan="2">
                <span class="glyphicon glyphicon-cog"></span>
                รายงานผลอัลตราซาวด์
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="left">
                <span class="glyphicon glyphicon-cog"></span>
<?php
    $url="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=worklist&hcode=".$userprof[0]["sitecode"]."&id=#username#&auth=#auth#";
    $url=str_replace("#username#",$userdet[0]["username"],$url);
    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);
    
?>
                <a href="<?php echo $url; ?>" target="_blank">เครื่องมือ Export work list สำหรับเครื่องอัลตราซาวด์</a>
            </td>
        </tr>
        <tr>
            <td width="2%"></td>
            <td width="98%" class="left">
                <span class="glyphicon glyphicon-list-alt"></span>
<?php
//    $url="http://www.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=usfinding&id=#username#&auth=#auth#";
//    $url=str_replace("#username#",$userdet[0]["username"],$url);
//    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);
    $url = \yii\helpers\Url::home()."/usfinding";
?>
                <a href="<?php echo $url; ?>" target="_blank">US Finding: เข้าสู่ระบบรายงานผลการตรวจอัลตราซาวด์</a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="left">
                <span class="glyphicon glyphicon-list-alt"></span>
<?php
    $url="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=usfindingall&id=#username#&auth=#auth#";
    $url=str_replace("#username#",$userdet[0]["username"],$url);
    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);
    
?>
                <a href="<?php echo $url; ?>" target="_blank">US Finding (all of ultrasound screening) show diagram</a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="left">
                <span class="glyphicon glyphicon-list-alt"></span>
                <?=Html::a('กราฟแสดงจำนวนกลุ่มเสี่ยงที่ได้รับการลงทะเบียน และจำนวนครั้งการทำอัลตร้าซาวด์',Url::to('/verification/cascap-graph'),['target'=>'_blank'])?>
            </td>
        </tr>
<?php
    if($userprof[0]["sitecode"]=="13777" || $userprof[0]["sitecode"]=="10666" || $userprof[0]["sitecode"]=="10667" || $userprof[0]["sitecode"]=="10668" || $userprof[0]["sitecode"]=="10669" || $userprof[0]["sitecode"]=="10670" || $userprof[0]["sitecode"]=="10671" || $userprof[0]["sitecode"]=="10708" || $userprof[0]["sitecode"]=="10710" || $userprof[0]["sitecode"]=="12276" || $userprof[0]["sitecode"]=="14201"){
?>
        <tr>
            <td class="left" colspan="2">
                <span class="glyphicon glyphicon-cog"></span>
                รายงานการนำเข้าข้อมูล เครื่องมือสำหรับหน่วยบริการ ที่รับรักษา
            </td>
        </tr> 
        <tr>
            <td></td>
            <td class="left">
                <span class="glyphicon glyphicon-cog"></span>
<?php
    $url="http://tools.cascap.in.th/v4cascap/version01/console/cloud/auth.php?dir=toolsls2&hcode=".$userprof[0]["sitecode"]."&id=#username#&auth=#auth#";
    $url=str_replace("#username#",$userdet[0]["username"],$url);
    $url=str_replace("#auth#",$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"],$url);
    
?>
                <a href="<?php echo $url; ?>" target="_blank">สรุปข้อมูลที่นำเข้า โดยจำแนกตามความสมบูรณ์ของข้อมูล</a>
            </td>
        </tr>
<?php
    }
?>
        
        <tr>
            <td class="left" colspan="2">
                <span class="glyphicon glyphicon-cog"></span>
                <a href="/verification/datasummary" >&nbsp;เมนูสำหรับ ติดตามการนำเข้าข้อมูล (Monitor Tools)</a>
            </td>
        </tr>
<?php
    //if( $userhosp[0]['code4']=='1' || $userdet[0]['id']=='1435745159010043375' || $userdet[0]['id']=='1435745159010043405' || $userdet[0]['id']=='1459167103058663203' ){
    if( 1 ){
?>
        <tr>
            <td></td>
            <td class="left">
                <span class="glyphicon glyphicon-list-alt"></span>
                <?=Html::a('เข้าสู่ระบบค่าตอบแทน',Url::to('/cpay'),['target'=>'_self'])?>
            </td>
        </tr>
<?php
    }
?>
        <tr>
            <td colspan="2" height="30"></td>
        </tr>
    </tbody>
</table>
<div id="report-backend" class="col-md-12">
    <?php
        $loadIconData = '\'<i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i>\'';
        //$debug = false;
        //$debug = true;
        if($debug || Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th') {
            $currentYear = intval(date('Y'));

            $maxFiscalyear = (int)(intval(date('m')) < 10 ? $currentYear : $currentYear + 1);
            $fiscalYearList = array();
            for ($y = 2013; $y <= $maxFiscalyear; ++$y) {
                $fiscalYearList[$y] = $y + 543;
            }


            $thaiMonth = ['',
                1=>['abvt'=>'ม.ค.','full'=>'มกราคม'],
                2=>['abvt'=>'ก.พ.','full'=>'กุมภาพันธ์'],
                3=>['abvt'=>'มี.ค.','full'=>'มีนาคม'],
                4=>['abvt'=>'เม.ย.','full'=>'เมษายน'],
                5=>['abvt'=>'พ.ค.','full'=>'พฤษภาคม'],
                6=>['abvt'=>'มิ.ย.','full'=>'มิถุนายน'],
                7=>['abvt'=>'ก.ค.','full'=>'กรกฎาคม'],
                8=>['abvt'=>'ส.ค.','full'=>'สิงหาคม'],
                9=>['abvt'=>'ก.ย.','full'=>'กันยายน'],
                10=>['abvt'=>'ต.ค.','full'=>'ตุลาคม'],
                11=>['abvt'=>'พ.ย.','full'=>'พฤศจิกายน'],
                12=>['abvt'=>'ธ.ค.','full'=>'ธันวาคม']
            ];



            $listMonth = function(){
                $f = '2013-02-09';
                $t = date('Y-m-d');
                $aml = array();
                list($sYear, $sMonth, $sDay) = explode("-", $f);
                list($eYear, $eMonth, $eDay) = explode("-", $t);
                $startMonth = $sMonth;
                do
                {
                    $mk = mktime(0, 0, 0, $sMonth, 1, $sYear);
                    $mY = date("Ym", $mk);
                    $endFormat = "Y-m-t";
                    if ($sMonth == $startMonth){
                        $mk = mktime(0, 0, 0, $sMonth, $sDay, $sYear);
                    } else if ($mY == $eYear.$eMonth ) {
                        $endFormat =  "Y-m-".$eDay;
                    }
                    //echo date("Y-m-d", $mk)    . " to " . date($endFormat, $mk) . "<br />";
                    array_push($aml, ['fromDate'=>date("Y-m-d", $mk),'toDate'=>date($endFormat, $mk),'y'=>date('Y',$mk),'m'=>date('m',$mk)] );
                    $sMonth++;
                }
                while ($eYear.$eMonth  > $mY);
                return $aml;
            };

            $allMonthList = $listMonth();
            foreach($allMonthList as $key =>$ym){
                $fiscalMonthList[$ym['fromDate'].'_'.$ym['toDate']] = $thaiMonth[intval($ym['m'])]['full'] . ' ' . (intval($ym['y']) + 543);
            }



            //echo '<br/><br/><!--hr style="border: 1.5px solid #AAA;"-->';


            $divControls = Html::tag(
                'div',
                '<h3 style="text-align: center;">รายงานความครอบคลุมการตรวจคัดกรองมะเร็งท่อน้ำดี</h3>

                <br/>
                <h3>ขั้นที่ 1 : เลือกประเภทรายงาน</h3>
                <div style="padding-left: 50px;">
                    <select class="form-control" id="datamonitor-type">
                        <option value="0">Basic</option>
                        <option value="1">Advance</option>
                    </select>
                </div>

                <br/><br/>

                <div id="datamonitor-controls">
                    <h3>ขั้นที่ 2 : เลือกเขตบริการสุขภาพ</h3>
                    <div style="padding-left: 50px;">
                    <input type="checkbox" id="checkboxDataZone1" name="zone" value="1" checked>
                        <label for="checkboxDataZone1">เขต 1</label>
                        <input type="checkbox" id="checkboxDataZone6" name="zone" value="6" checked>
                        <label for="checkboxDataZone6">เขต 6</label>
                        <input type="checkbox" id="checkboxDataZone7" name="zone" value="7" checked>
                        <label for="checkboxDataZone7">เขต 7</label>
                        <input type="checkbox" id="checkboxDataZone8" name="zone" value="8" checked>
                        <label for="checkboxDataZone8">เขต 8</label>
                        <input type="checkbox" id="checkboxDataZone9" name="zone" value="9" checked>
                        <label for="checkboxDataZone9">เขต 9</label>
                        <input type="checkbox" id="checkboxDataZone10" name="zone" value="10" checked>
                        <label for="checkboxDataZone10">เขต 10</label>
                    </div>

                    <br/><br/>

                    <h3>ขั้นที่ 3 : เลือกช่วงเวลา</h3>
                    <div style="padding-left: 50px;">
                        <!--input type="radio" name="date-range" id="date-range-all"> <label for="date-range-all">ทั้งหมด</label>
                        &emsp;

                        <br/><br/-->
                        <input type="radio" name="date-range" id="date-range-year"> <label for="date-range-year">ตามปีงบประมาณ ระบุ</label> '.

                        Html::dropDownList(
                            'list',
                            $maxFiscalyear,
                            $fiscalYearList,
                            [
                                'id' => 'dataMonitorFiscalYear',
                                'class' => 'form-control'
                            ]
                        ).
                        '&emsp;

                        <br/><br/>
                        <!--input type="radio" name="date-range" id="date-range-day"> <label for="date-range-day">ตามช่วงเวลา</label>

                        เริ่ม '.
                        Html::input(
                            'date',
                            'dataMonitorFromDate',
                            '2013-02-09',
                            [
                                'id' => 'dataMonitorFromDate',
                                'class' => 'form-control',
                                'min' => '2013-02-09',
                                'max' => date('Y-m-d')
                            ]
                        ) . ' ถึง ' .
                        Html::input(
                            'date',
                            'dataMonitorToDate',
                            date('Y-m-d'),
                            [
                                'id' => 'dataMonitorToDate',
                                'class' => 'form-control',
                                'min' => '2013-02-09',
                                'max' => date('Y-m-d')
                            ]
                        ).'-->
                    </div>
                    <br/><br/>'.
                '</div>
                <div style="padding-left: 50px;">
                    <button type="button" id="dataMonitorSubmit" class="btn btn-info form-control button-data-monitor">แสดงรายงาน</button>
                    <span id="excel-datamonitor" class="text-success" style="cursor:pointer;display:none;">
                        <br/><br/>
                        <i class="fa fa-2x fa-file-excel-o"></i> นำออกรายงานนี้ในรูปแบบ Excel
                    </span>
                </div>
                ',
                [
                    'class' => 'form-inline',
                    'style' => [
                        'text-align' => 'left'
                    ]
                ]
            );

            ///*

            echo Html::tag(
                'div',
                Html::tag(
                    'div',
                    $divControls.
                    '<div class="row">
                        <div class="col-md-12">
                            <div id="data-report-div">
                            </div>
                        </div>
                    </div>',
                    [
                        'class'=>'col-md-12 panel panel-primary',
                        'style'=>[
                            'padding-bottom' => '20px'
                        ]
                    ]
                )
                ,
                [
                    'class'=>'row'
                ]
            );

            //*/




            $divControlsCca02 = Html::tag(
                'div',
                '<h3 style="text-align: center;">รายงานความครอบคลุมการตรวจคัดกรองมะเร็งท่อน้ำดีด้วยอัลตร้าซาวด์</h3>

                <br/>
                <h3>ขั้นที่ 1 : เลือกประเภทรายงาน</h3>
                <div style="padding-left: 50px;">
                    <select class="form-control" id="cca02-type">
                        <option value="0">Basic</option>
                        <option value="1">Advance</option>
                    </select>
                </div>

                <br/><br/>

                <div id="cca02-controls">
                    <h3>ขั้นที่ 2 : เลือกเขตบริการสุขภาพ</h3>
                    <div style="padding-left: 50px;">
                        <input type="checkbox" id="checkboxCca02Zone1" name="zone" value="1" checked>
                        <label for="checkboxCca02Zone1">เขต 1</label>
                        <input type="checkbox" id="checkboxCca02Zone6" name="zone" value="6" checked>
                        <label for="checkboxCca02Zone6">เขต 6</label>
                        <input type="checkbox" id="checkboxCca02Zone7" name="zone" value="7" checked>
                        <label for="checkboxCca02Zone7">เขต 7</label>
                        <input type="checkbox" id="checkboxCca02Zone8" name="zone" value="8" checked>
                        <label for="checkboxCca02Zone8">เขต 8</label>
                        <input type="checkbox" id="checkboxCca02Zone9" name="zone" value="9" checked>
                        <label for="checkboxCca02Zone9">เขต 9</label>
                        <input type="checkbox" id="checkboxCca02Zone10" name="zone" value="10" checked>
                        <label for="checkboxCca02Zone10">เขต 10</label>
                    </div>
                    <br/><br/>
                    <h3>ขั้นที่ 3 : เลือกปีงบประมาณ</h3>
                    <div style="padding-left: 50px;">'.
                        Html::dropDownList(
                            'list',
                            $maxFiscalyear,
                            $fiscalYearList,
                            [
                                'id' => 'cca02FiscalYear',
                                'class' => 'form-control'
                            ]
                        ).
                        '
                    </div>
                    <br/><br/>
                </div>
                <div style="padding-left: 50px;">
                    <button type="button" id="cca02ReportSubmitAjax" class="btn btn-info form-control">แสดงรายงาน</button>
                    <span id="excel-cca02" class="text-success" style="cursor:pointer;display:none;">
                        <br/><br/>
                        <i class="fa fa-2x fa-file-excel-o"></i> นำออกรายงานนี้ในรูปแบบ Excel
                    </span>
                </div>',
                [
                    'class' => 'form-inline',
                    'style' => [
                        'text-align' => 'left'
                    ]
                ]
            );
            echo Html::tag(
                'div',
                Html::tag(
                    'div',

                    $divControlsCca02.
                    '<div class="row">
                        <div class="col-md-12">
                            <div id="cca02-report-div">
                            </div>
                        </div>
                    </div>',
                    [
                        'class'=>'col-md-12 panel panel-primary',
                        'style'=>[
                            'padding-bottom' => '20px'
                        ]
                    ]
                ),
                [
                    'class'=>'row'
                ]
            );




            // all proof

            $divControlsAllProof = '
            <div class="form-inline" style="text-align: left;">
                <h3 style="text-align: center;">รายงานผลการตรวจพบมะเร็งท่อน้ำดีด้วย CT/MRI</h3>
                <br/>

                <h3>ขั้นที่ 1 : เลือกประเภทรายงาน</h3>
                <div style="padding-left: 50px;">
                    <select class="form-control" id="allproof-type">
                        <option value="0">Basic</option>
                        <option value="1">Advance</option>
                    </select>
                </div>

                <br/><br/>

                <div id="allproof-controls">
                    <h3>ขั้นที่ 2 : เลือกเขตบริการสุขภาพ</h3>
                    <div style="padding-left: 50px;">
                        <input type="checkbox" id="checkboxAllProofZone1" name="zone" value="1" checked>
                        <label for="checkboxAllProofZone1">เขต 1</label>
                        <input type="checkbox" id="checkboxAllProofZone6" name="zone" value="6" checked>
                        <label for="checkboxAllProofZone6">เขต 6</label>
                        <input type="checkbox" id="checkboxAllProofZone7" name="zone" value="7" checked>
                        <label for="checkboxAllProofZone7">เขต 7</label>
                        <input type="checkbox" id="checkboxAllProofZone8" name="zone" value="8" checked>
                        <label for="checkboxAllProofZone8">เขต 8</label>
                        <input type="checkbox" id="checkboxAllProofZone9" name="zone" value="9" checked>
                        <label for="checkboxAllProofZone9">เขต 9</label>
                        <input type="checkbox" id="checkboxAllProofZone10" name="zone" value="10" checked>
                        <label for="checkboxAllProofZone10">เขต 10</label>
                    </div>

                    <br/><br/>
                    <h3>ขั้นที่ 3 : เลือกปีงบประมาณ</h3>
                    <div style="padding-left: 50px;">'.
                        Html::dropDownList(
                            'list',
                            $maxFiscalyear,
                            $fiscalYearList,
                            [
                                'id' => 'allProofFiscalYear',
                                'class' => 'form-control'
                            ]
                        ).
                    '
                    </div>
                    <br/><br/>

                </div>
                <div style="padding-left: 50px;">
                    <button type="button" id="allProofSubmitAjax" class="btn btn-info form-control">แสดงรายงาน</button>

                    <span id="excel-allproof" class="text-success" style="cursor:pointer;display:none;" >
                        <br/><br/>
                        <i class="fa fa-2x fa-file-excel-o"></i> นำออกรายงานนี้ในรูปแบบ Excel
                    </span>
                </div>
            </div>';

            echo Html::tag(
                'div',
                Html::tag(
                    'div',

                    $divControlsAllProof.
                    '<div class="row">
                        <div class="col-md-12">
                            <div id="allproof-report-div">
                            </div>
                        </div>
                    </div>',
                    [
                        'class'=>'col-md-12 panel panel-primary',
                        'style'=>[
                            'padding-bottom' => '20px'
                        ]
                    ]
                ),
                [
                    'class'=>'row'
                ]
            );



            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->registerJs('

                $("#datamonitor-type").change(function(){
                    if( $(this).val()==0 ){
                        $("#datamonitor-controls").hide();
                        $("#dataMonitorSubmit").removeAttr("disabled");
                    }else{
                        $("#datamonitor-controls").show();
                        var setInput = $("input[type=\'radio\'][name=\'date-range\']");
                        var hasCheck = false;
                        for(k in setInput){
                            if( setInput[k].checked == true ){
                                hasCheck = true;
                            }
                        }
                        if( hasCheck ){
                            $("#dataMonitorSubmit").removeAttr("disabled");
                        }else{
                            $("#dataMonitorSubmit").attr("disabled",true);
                        }
                    }
                });

                $("input[type=\'radio\'][name=\'date-range\']").change(function(){
                    var setInput = $("input[type=\'radio\'][name=\'date-range\']");
                    var hasCheck = false;
                    for(k in setInput){
                        if( setInput[k].checked == true ){
                            hasCheck = true;
                        }
                    }
                    if( hasCheck ){
                        $("#dataMonitorSubmit").removeAttr("disabled");
                    }else{
                        $("#dataMonitorSubmit").attr("disabled",true);
                    }
                });

                $("#cca02-type").change(function(){
                    if( $(this).val()==0 ){
                        $("#cca02-controls").hide();
                    }else{
                        $("#cca02-controls").show();
                    }
                });

                $("#allproof-type").change(function(){
                    if( $(this).val()==0 ){
                        $("#allproof-controls").hide();
                    }else{
                        $("#allproof-controls").show();
                    }
                });

            ');

            $this->registerJs('
                $("#excel-datamonitor").click(function(e){
                    e.preventDefault();
                    var dataTable = $("#data-report-table").clone();
                    dataTable.removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("border");
                    dataTable.find("*").removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("show").removeAttr("mysite");
                    var headerName = ("<tr><th colspan=\'8\'>รายงานความครอบคลุมการตรวจคัดกรองมะเร็งท่อน้ำดี</th></tr>");
                    var headerLastcal = ("<tr><td colspan=\'8\'>"+ $("#excel-data-header-lastcal").html() + "</td></tr>");
                    var headerRange = ("<tr><td colspan=\'8\'>"+ $("#excel-data-header-range").html() + "</td></tr>");
                    dataTable.prepend(headerLastcal);
                    dataTable.prepend(headerRange);
                    dataTable.prepend(headerName);
                    var excelString = dataTable.prop("outerHTML").replace(/colspan/g,"align=\"center\" colspan");
                    //console.log(excelString);
                    excelString = encodeURIComponent(excelString);
                    //console.log(excelString);
                    window.open("data:application/vnd.ms-excel," + excelString);
                });

                $("#excel-cca02").click(function(e){
                    e.preventDefault();
                    var dataTable = $("#cca02-report-table").clone();
                    dataTable.removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("border");
                    dataTable.find("*").removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("show").removeAttr("mysite");
                    var headerName = ("<tr><th colspan=\'8\'>รายงานความครอบคลุมการตรวจคัดกรองมะเร็งท่อน้ำดีด้วยอัลตร้าซาวด์</th></tr>");
                    var headerLastcal = ("<tr><td colspan=\'8\'>"+ $("#excel-cca02-header-lastcal").html() + "</td></tr>");
                    var headerRange = ("<tr><td colspan=\'8\'>"+ $("#excel-cca02-header-range").html() + "</td></tr>");
                    dataTable.prepend(headerLastcal);
                    dataTable.prepend(headerRange);
                    dataTable.prepend(headerName);
                    var excelString = dataTable.prop("outerHTML").replace(/colspan/g,"align=\"center\" colspan");
                    //console.log(excelString);
                    excelString = encodeURIComponent(excelString);
                    //console.log(excelString);
                    window.open("data:application/vnd.ms-excel," + excelString);
                });

                $("#excel-allproof").click(function(e){
                    e.preventDefault();
                    var dataTable = $("#allproof-report-table").clone();
                    dataTable.removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("border");
                    dataTable.find("*").removeAttr("class").removeAttr("id").removeAttr("style").removeAttr("show").removeAttr("mysite");
                    var headerName = ("<tr><th colspan=\'8\'>รายงานผลการตรวจพบมะเร็งท่อน้ำดีด้วย CT/MRI</th></tr>");
                    var headerLastcal = ("<tr><td colspan=\'8\'>"+ $("#excel-allproof-header-lastcal").html() + "</td></tr>");
                    var headerRange = ("<tr><td colspan=\'8\'>"+ $("#excel-allproof-header-range").html() + "</td></tr>");
                    dataTable.prepend(headerLastcal);
                    dataTable.prepend(headerRange);
                    dataTable.prepend(headerName);
                    var excelString = dataTable.prop("outerHTML").replace(/colspan/g,"align=\"center\" colspan");
                    //console.log(excelString);
                    excelString = encodeURIComponent(excelString);
                    //console.log(excelString);
                    window.open("data:application/vnd.ms-excel," + excelString);
                });
            ');
            // global function
            $this->registerJs('
                var months = ["",
                    "มกราคม",
                    "กุมภาพันธ์",
                    "มีนาคม",
                    "เมษายน",
                    "พฤษภาคม",
                    "มิถุนายน",
                    "กรกฎาคม",
                    "สิงหาคม",
                    "กันยายน",
                    "ตุลาคม",
                    "พฤศจิกายน",
                    "ธันวาคม"
                ];

                function getTodayFormat(timeStamp){
                    var day = new Date(timeStamp);
                    var dd = day.getDate();
                    var mm = day.getMonth()+1; //January is 0!

                    var yyyy = day.getFullYear();
                    if(dd<10){
                        dd = "0"+dd
                    }
                    if(mm<10){
                        mm = "0"+mm
                    }
                    return yyyy+"-"+mm+"-"+dd;
                }

                function setCheckedZone(){
                    $("[id^=\'checkboxDataZone\']").each(function(){
                        delete dataMonitorFormData["zone"+$(this).val()];
                    });

                    $("[id^=\'checkboxDataZone\']:checked").each(function(){
                        dataMonitorFormData["zone"+$(this).val()] = $(this).val();
                    });

                    getDataMonitor();
                }
            ', 1);

            // event listener
            $this->registerJs('
                // data monitor ##########


                /*
                $("#dataMonitorSubmitBasic").click(function(){

                    delete dataMonitorFormData["refresh"];

                    var fiscalYear = parseInt($("#dataMonitorFiscalYear").children("[selected]").val());

                    dataMonitorFormData["fromDate"] = (fiscalYear-1)+"-10-01";
                    dataMonitorFormData["toDate"] = fiscalYear+"-09-30";

                    dataMonitorFormData["zone1"] = 1;
                    dataMonitorFormData["zone6"] = 6;
                    dataMonitorFormData["zone7"] = 7;
                    dataMonitorFormData["zone8"] = 8;
                    dataMonitorFormData["zone9"] = 9;
                    dataMonitorFormData["zone10"] = 10;

                    dataMonitorFormData["type"]="year";
                    dataMonitorFormData["str"]= fiscalYear;

                    console.log(dataMonitorFormData);
                    getDataMonitor();

                });



                $("#dataMonitorSubmitAdvance").click(function(){

                    delete dataMonitorFormData["refresh"];

                    var fiscalYear = parseInt($("#dataMonitorFiscalYear").val());

                    dataMonitorFormData["type"]="year";
                    dataMonitorFormData["str"]= fiscalYear ;

                    dataMonitorFormData["fromDate"] = (fiscalYear-1)+"-10-01";
                    dataMonitorFormData["toDate"] = fiscalYear+"-09-30";

                    //console.log(dataMonitorFormData);
                    setCheckedZone();
                });

                $("#dataMonitorSubmitMonth").click(function(){

                    delete dataMonitorFormData["refresh"];

                    var v = $("#dataMonitorFiscalMonth").val();

                    var ft = v.split("_");
                    var s = $("#dataMonitorFiscalMonth").children("[value=\'"+v+"\']").html();

                    dataMonitorFormData["type"]="month";
                    dataMonitorFormData["str"]= s;

                    dataMonitorFormData["fromDate"] = ft[0];
                    dataMonitorFormData["toDate"] = ft[1];


                    //console.log(dataMonitorFormData);
                    setCheckedZone()

                });

                $("#dataMonitorSubmitDay").click(function(){

                    delete dataMonitorFormData["refresh"];

                    dataMonitorFormData["type"] = "day";
                    dataMonitorFormData["str"]= "";

                    dataMonitorFormData["fromDate"] = $("#dataMonitorFromDate").val();
                    dataMonitorFormData["toDate"] = $("#dataMonitorToDate").val();

                    //console.log(dataMonitorFormData);
                    setCheckedZone()
                });
                */

                $("#dataMonitorSubmit").click(function(){

                    delete dataMonitorFormData["refresh"];

                    if( $("#datamonitor-type").val()==0 ){

                        var fiscalYear = parseInt($("#dataMonitorFiscalYear").children("[selected]").val());

                        dataMonitorFormData["type"] = "year";
                        dataMonitorFormData["str"] = fiscalYear ;

                        dataMonitorFormData["fromDate"] = (fiscalYear-1)+"-10-01";
                        dataMonitorFormData["toDate"] = fiscalYear+"-09-30";

                        dataMonitorFormData["zone1"] = 1;
                        dataMonitorFormData["zone6"] = 6;
                        dataMonitorFormData["zone7"] = 7;
                        dataMonitorFormData["zone8"] = 8;
                        dataMonitorFormData["zone9"] = 9;
                        dataMonitorFormData["zone10"] = 10;

                        getDataMonitor();
                        return;
                    }

                    var getType = $("input[type=\'radio\'][name=\'date-range\']:checked").attr("id");

                    if(getType == "date-range-all"){

                        dataMonitorFormData["type"] = "all";
                        dataMonitorFormData["str"]= "";

                        var fd = "2013-02-09";
                        var td = getTodayFormat(new Date().getTime());

                        dataMonitorFormData["fromDate"] = fd;
                        dataMonitorFormData["toDate"] = td;

                    }else if(getType == "date-range-year"){

                        var fiscalYear = parseInt($("#dataMonitorFiscalYear").val());

                        dataMonitorFormData["type"] = "year";
                        dataMonitorFormData["str"] = fiscalYear ;

                        dataMonitorFormData["fromDate"] = (fiscalYear-1)+"-10-01";
                        dataMonitorFormData["toDate"] = fiscalYear+"-09-30";

                    }else if(getType == "date-range-day"){

                        var setFromDate = $("#dataMonitorFromDate").val();
                        var setToDate = $("#dataMonitorToDate").val();

                        if( new Date(setFromDate).getTime() < new Date("2013-02-09").getTime() ){
                            setFromDate = "2013-02-09";
                        }else if( new Date(setFromDate).getTime() > new Date().getTime() ){
                            setFromDate = getTodayFormat(new Date().getTime())
                        }

                        if( new Date(setToDate).getTime() < new Date("2013-02-09").getTime() ){
                            setToDate = "2013-02-09";
                        }else if( new Date(setToDate).getTime() > new Date().getTime() ){
                            setToDate = getTodayFormat(new Date().getTime())
                        }

                        $("#dataMonitorFromDate").val(setFromDate);
                        $("#dataMonitorToDate").val(setToDate);

                        dataMonitorFormData["type"] = "day";
                        dataMonitorFormData["str"]= "";

                        dataMonitorFormData["fromDate"] = setFromDate;
                        dataMonitorFormData["toDate"] = setToDate;

                    }

                    $("[id^=\'checkboxDataZone\']").each(function(){
                        delete dataMonitorFormData["zone"+$(this).val()];
                    });

                    $("[id^=\'checkboxDataZone\']:checked").each(function(){
                        dataMonitorFormData["zone"+$(this).val()] = $(this).val();
                    });

                    getDataMonitor();

                });



                // cca02 ##########
                $("#cca02ReportSubmitAjax").click(function(){

                    cca02FormData["refresh"] = false;

                    if( $("#cca02-type").val()==0 ){

                        cca02FormData["fiscalYear"] = $("#cca02FiscalYear").children("[selected]").val();
                        cca02FormData["zone1"] = 1;
                        cca02FormData["zone6"] = 6;
                        cca02FormData["zone7"] = 7;
                        cca02FormData["zone8"] = 8;
                        cca02FormData["zone9"] = 9;
                        cca02FormData["zone10"] = 10;

                        getReportCca02();
                        return;
                    }

                    cca02FormData["fiscalYear"] = $("#cca02FiscalYear").val();

                    $("[id^=\'checkboxCca02Zone\']").each(function(){
                        delete cca02FormData["zone"+$(this).val()];
                    });

                    $("[id^=\'checkboxCca02Zone\']:checked").each(function(){
                        cca02FormData["zone"+$(this).val()] = $(this).val();
                    });

                    getReportCca02();

                });



                // allproof ##########
                $("#allProofSubmitAjax").click(function(){

                    allProofFormData["refresh"] = false;

                    if( $("#allproof-type").val()==0 ){

                        allProofFormData["fiscalYear"] = $("#allProofFiscalYear").children("[selected]").val();
                        allProofFormData["zone1"] = 1;
                        allProofFormData["zone6"] = 6;
                        allProofFormData["zone7"] = 7;
                        allProofFormData["zone8"] = 8;
                        allProofFormData["zone9"] = 9;
                        allProofFormData["zone10"] = 10;

                        getAllProofCca();
                        return;
                    }

                    allProofFormData["fiscalYear"] = $("#allProofFiscalYear").val();

                    $("[id^=\'checkboxAllProof\']").each(function(){
                        delete allProofFormData["zone"+$(this).val()];
                    });

                    $("[id^=\'checkboxAllProof\']:checked").each(function(){
                        allProofFormData["zone"+$(this).val()] = $(this).val();
                    });

                    getAllProofCca();
                });

            ');

            // data monitor global
            $this->registerJs('
                var dataMonitorFormData = {};
                var dataXhr;

                function getDataMonitor(){

                    console.log(dataMonitorFormData);
                    //return;

                    if(dataXhr!=undefined && dataXhr.readyState!=0){
                        dataXhr.abort();
                    }

                    $("#data-report-div").html(' . $loadIconData . ');
                    $(".button-data-monitor").attr("disabled",true);
                    $("input[type=\'radio\'][name=\'date-range\']").attr("disabled",true);
                    $("#excel-datamonitor").hide();

                    dataXhr = $.ajax({
                        type    : "GET",
                        cache   : false,
                        url     : "' . Url::to('../timeline-event/ajax-data-monitor/') . '",
                        data    : dataMonitorFormData,
                        success :   function(response) {
                            $(".button-data-monitor").removeAttr("disabled");
                            $("input[type=\'radio\'][name=\'date-range\']").removeAttr("disabled");
                            $("#data-report-div").html(response);
                            if(dataMonitorFormData["type"]=="year"){
                                $("#excel-datamonitor").show();
                            }

                        },
                        error   :   function(){
                            $(".button-data-monitor").removeAttr("disabled");
                            $("input[type=\'radio\'][name=\'date-range\']").removeAttr("disabled");
                            $("#data-report-div").html("การแสดงข้อมูลผิดพลาด");
                        }
                    });
                }


            ', 1);


            // cca02 monitor global
            $this->registerJs('

                var cca02FormData = {};
                var cca02Xhr;

                function getReportCca02(){

                    //console.log(cca02FormData);
                    //return;

                    if(cca02Xhr!=undefined && cca02Xhr.readyState!=0){
                        cca02Xhr.abort();
                    }

                    $("#cca02-report-div").html(' . $loadIconData . ');
                    $("#cca02ReportSubmitAjax").attr("disabled",true);
                    $("#excel-cca02").hide();

                    cca02Xhr = $.ajax({
                        type    : "GET",
                        cache   : false,
                        url     : "' . Url::to('../timeline-event/ajax-report-cca02/') . '",
                        data    : cca02FormData,
                        success :   function(response) {
                            $("#cca02ReportSubmitAjax").removeAttr("disabled");
                            $("#cca02-report-div").html(response);
                            $("#excel-cca02").show();

                        },
                        error   :   function(){
                            $("#cca02ReportSubmitAjax").removeAttr("disabled");
                            $("#cca02-report-div").html("การแสดงข้อมูลผิดพลาด");
                        }
                    });

                }

            ', 1);

            // allproof monitor global
            $this->registerJs('

                var allProofFormData = {};
                var allProofXhr;

                function getAllProofCca(){

                    //console.log(allProofFormData);
                    //return;

                    if(allProofXhr!=undefined && allProofXhr.readyState!=0){
                        allProofXhr.abort();
                    }

                    $("#allproof-report-div").html(' . $loadIconData . ');
                    $("#allProofSubmitAjax").attr("disabled",true);
                    $("#excel-allproof").hide();

                    allProofXhr = $.ajax({
                        type    : "GET",
                        cache   : false,
                        url     : "' . Url::to('../timeline-event/all-proof-cca/') . '",
                        data    : allProofFormData,
                        success :   function(response) {
                            $("#allProofSubmitAjax").removeAttr("disabled");
                            $("#allproof-report-div").html(response);
                            $("#excel-allproof").show();
                        },
                        error   :   function(){
                            $("#allProofSubmitAjax").removeAttr("disabled");
                            $("#allproof-report-div").html("การแสดงข้อมูลผิดพลาด");
                        }
                    });
                }

            ', 1);

            // data monitor
            $this->registerJs('
                /*
                $("#dataMonitorFiscalYear").change(function(){
                    var fiscalYearSelect = parseInt($("#dataMonitorFiscalYear").val());

                    $("#dataMonitorFiscalMonth").attr("disabled","disabled");

                    $("#dataMonitorFiscalMonth").html("");

                    var reverted = false;
                    var currentYear = fiscalYearSelect!=undefined ? fiscalYearSelect : (new Date()).getFullYear();
                    for(m = 10; m < 13; m++){
                        var yearText = (m < 10 ? currentYear : currentYear-1);

                        $("#dataMonitorFiscalMonth").append("<option value="+yearText+"_"+m+">"+months[m]+" "+(parseInt(yearText)+543)+"</option>");

                        if(!reverted){
                            if(m==12) {
                                reverted=true;
                                m = 0;
                            }
                        }else{
                            if(m==9){
                                break;
                            }
                        }
                    }

                    var setFromDate = (fiscalYearSelect-1)+"-10-01";
                    var setToDate = (fiscalYearSelect)+"-09-30";

                    $("#dataMonitorFromDate").val(setFromDate);
                    $("#dataMonitorToDate").val( setToDate );

                    $("#dataMonitorFiscalMonth").removeAttr("disabled");
                    $("#report-date-detail").html("แสดงข้อมูลในช่วงปีงบประมาณ "+(parseInt( $("#dataMonitorFiscalYear").val() )+543) );

                    //getDataMonitor();
                });

                $("#dataMonitorFiscalMonth").change(function(){

                    var fiscalSelect = $("#dataMonitorFiscalMonth").val().split("_");
                    var fiscalM = fiscalSelect[1] < 10 ?"0"+fiscalSelect[1]:fiscalSelect[1] ;

                    var setFromDate = fiscalSelect[0]+"-"+fiscalM+"-01"
                    var setToDate = fiscalSelect[0]+"-"+fiscalM+"-"+(new Date(fiscalSelect[0], fiscalM, 0)).getDate();

                    $("#dataMonitorFromDate").val( setFromDate );

                    $("#dataMonitorToDate").val( setToDate );

                    $("#report-date-detail").html("แสดงข้อมูลในช่วงเดือน "+months[parseInt(fiscalM)]+" ของปีงบประมาณ "+(parseInt( $("#dataMonitorFiscalYear").val() )+543) );

                    //getDataMonitor();

                });
                */

            ');


            // global css
            $this->registerCss('
                /*

                [id^="cca02-span"],
                [id^="data-span"],
                [id^="allproof-span"],
                [get-data-code],
                [allproof-attr]
                {
                    cursor:pointer;
                    color: #3c8dbc;
                }

                [id^="cca02-span"]:hover,
                [id^="data-span"]:hover,
                [id^="allproof-span"]:hover,
                [get-data-code]:hover,
                [allproof-attr]:hover
                {
                    color: #72afd2;
                }

                .report-hname,
                .report-amphur
                {
                    text-align:left !important;
                }

                .report-amphur,
                .code-amphur
                {
                    cursor:pointer;
                }

                .report-data-head
                {
                    width:8%;
                }

                #report-date-detail
                {
                    text-align:center;
                }

                [get-data-attr]
                {
                    cursor : pointer;
                }

                [get-data-attr="hname"]
                {
                    cursor : default !important;
                    color : #000 !important;
                }

                [get-data-attr="hname"]:hover
                {
                    color : #000 !important;
                }

                */

                #data-report-table,
                #cca02-report-table,
                #allproof-report-table
                {
                    background-color: #FFFFFF; border-radius: 5px;
                    word-break: normal;
                    /*transform: rotateX(180deg);*/
                }


                #cca02-report-table th,
                #allproof-report-table th
                {
                    text-align:center !important;
                    vertical-align:middle;

                }

                #data-report-table th{
                    text-align:center;
                    vertical-align:middle;
                }


                #data-report-table td,#data-report-table th,
                #cca02-report-table td,#cca02-report-table th,
                #allproof-report-table td,#allproof-report-table td
                {
                    text-align:right;
                    border-left:1px solid #f4f4f4;
                }

                #data-report-div,
                #cca02-report-div,
                #allproof-report-div
                {
                    overflow-x:overlay;
                    /*transform: rotateX(180deg);*/
                    text-align:center;
                }

                .unit-section
                {
                    background-color:#000;
                    color:#FFF;
                }


                .unit-separator
                {
                    background-color:#555;
                    color:#FFF;
                    text-align:left !important;
                }

                .unit-typesplit
                {
                    background-color:#AAA;
                    color:#FFF;
                    text-align:left !important;
                }

                .unit-name
                {
                    text-align:left !important;
                }

                .unit-sum
                {
                    text-align:right !important;
                    color:blue;
                }

                .unit-sum.name
                {
                    text-align:left !important;
                }

                .unit-heading
                {
                    text-align:center !important;
                    vertical-align:middle !important;
                    font-weight:bold;
                    background-color:#f5f5f5;
                }

                #data-report-table .unit-heading
                {
                    border: 1px solid #AAA;
                }

                [id$="-controls"]
                {
                    display:none;
                }

                [id$="refresh"]
                {
                    cursor:pointer;
                    color: #3c8dbc;
                }

                [id$="refresh"]:hover
                {
                    color: #72afd2;
                }
            ');
        }
    ?>
</div>


