<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\lib\sdii\components\helpers\SDNoty;
use \common\lib\sdii\SDFunc;
/* @var $this yii\web\View */
/* @var $model backend\models\ActCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="act-category-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Act Category</h4>
	</div>

	<div class="modal-body">
		
		<?= $form->field($model, 'act_name')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'parent_id')->dropDownList(SDFunc::getTaxonomyDropDownList(0, 1), ['encode' => false, 'prompt' => 'none']) ?>
	    
		<?= $form->field($model, 'act_detail')->textarea(['rows' => 3]) ?>

		

		<?= Html::activeHiddenInput($model, 'act_order')?>
		<?= Html::activeHiddenInput($model, 'ezform_count')?>
		<?= Html::activeHiddenInput($model, 'act_id')?>
		<?= Html::activeHiddenInput($model, 'act_type')?>
		<?= Html::activeHiddenInput($model, 'status')?>
		<?= Html::activeHiddenInput($model, 'user_create')?>
		<?= Html::activeHiddenInput($model, 'date_create')?>
		<?= Html::activeHiddenInput($model, 'user_update')?>
		<?= Html::activeHiddenInput($model, 'date_update')?>

	</div>
	<div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
window.setTimeout(function(){
	$('#ActCategory #actcategory-act_name').select();
},500);    

$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create'){
		getTerms();
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#act-category-grid-pjax'});
		window.setTimeout(function(){
			$('#ActCategory #actcategory-act_name').select();
		},500); 
	    } else if(result.action == 'update'){
		$(document).find('#modal-act-category').modal('hide');
		$.pjax.reload({container:'#act-category-grid-pjax'});
	    }
	} else{
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function(){
	console.log('server error');
    });
    return false;
});

function getTerms(){
    var termsParent = $('#actcategory-parent_id').val();
    $.post(
	'" . Url::to(['act-category/terms', 'type'=>$_GET['type']]) . "'
    ).done(function(result){
	if(result.status == 'success'){
	    $('#actcategory-parent_id').html(result.content);
	    $('#actcategory-parent_id').val(termsParent);
	} else {
	     " . SDNoty::show('result.message', 'result.status') . "
	    return ;
	}
    }).fail(function(){
	console.log('server error');
    });
    
}

");?>