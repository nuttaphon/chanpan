<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ActCategory */

$this->title = Yii::t('backend', 'Create Act Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Act Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="act-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
