<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\UserAllow */

$this->title = 'Create User Allow';
$this->params['breadcrumbs'][] = ['label' => 'User Allows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-allow-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
