<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserAllowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'กำหนดสิทธิ์การใช้งานจากหน่วยงานอื่น';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-allow-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="padding-top: 10px;">
	<span class="label label-primary">คำอธิบาย</span>
	<?= Yii::t('app', 'ผู้ใช้งานจากหน่วยบริการอื่นดังต่อไปนี้ สามารถเข้าใช้งานในหน่วยบริการของท่านได้.') ?>
    </p>

    <?php  Pjax::begin(['id'=>'user-allow-grid-pjax', 'timeout' => 10000]);?>
    <?= GridView::widget([
	'id' => 'user-allow-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd() . " เพิ่มผู้ใช้งานจากหน่วยบริการอื่น", ['data-url'=>Url::to(['user-allow/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-user-allow']) . ' ' . Html::a(SDHtml::getBtnRepeat() . " กลับหน้าจัดการผู้ใช้", [Url::to(['/userlist/index'])], ['class' => 'btn btn-success btn-sm']),
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionUserAllowIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            [
                'header' => 'ชื่อ - สกุล',
                'value' => function ($model) {
                    $user = \backend\models\UserProfile::find()->where(['user_id'=>$model->user_id])->one();
                    return $user->firstname . " " . $user->lastname;
                }
            ],
            [
                'header' => 'สังกัด',
                'value' => function ($model) {
                    $user = \backend\models\UserProfile::find()->where(['user_id'=>$model->user_id])->one();
                    $hospital = app\modules\adddoctor\models\AllHospitalThai::find()->where(['hcode'=>$user->department])->one();
                    return $user->department . " " . $hospital->name;
                }
            ],                    
          
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        
                        $authSite = (Yii::$app->user->identity->userProfile->sitecode == Yii::$app->user->identity->userProfile->department);
			if((Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')) && $authSite){
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'get',    
                                    'data-action' => 'delete',
                                    'data-pjax'=>'0',
			    ]); 
			}
                    },
                ],                
	    ],
        ],
    ]); 
    
    echo Html::button(SDHtml::getBtnDelete() . " ลบผู้ใช้ที่เลือก", ['data-url'=>Url::to(['user-allow/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-user-allow', 'disabled'=>true]);
    ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-user-allow',
    'size'=>'modal-lg',
    'tabindexEnable' => false,
]);
?>

<?php  $this->registerJs("
$('#user-allow-grid-pjax').on('click', '#modal-addbtn-user-allow', function() {
    modalUserAllow($(this).attr('data-url'));
});

$('#user-allow-grid-pjax').on('click', '#modal-delbtn-user-allow', function() {
    selectionUserAllowGrid($(this).attr('data-url'));
});

$('#user-allow-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#user-allow-grid').yiiGridView('getSelectedRows');
	disabledUserAllowBtn(key.length);
    },100);
});

$('#user-allow-grid-pjax').on('click', '.selectionUserAllowIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledUserAllowBtn(key.length);
});

$('#user-allow-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalUserAllow(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'ท่านแน่ใจหรือไม่ที่จะลบการอนุญาตเข้าใช้งานของผู้ใช้ดังกล่าว?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#user-allow-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledUserAllowBtn(num) {
    if(num>0) {
	$('#modal-delbtn-user-allow').attr('disabled', false);
    } else {
	$('#modal-delbtn-user-allow').attr('disabled', true);
    }
}

function selectionUserAllowGrid(url) {
    yii.confirm('".Yii::t('app', 'ท่านแน่ใจหรือไม่ที่จะลบผู้ใช้งานที่เลือกออกจากการอนุญาต?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionUserAllowIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#user-allow-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalUserAllow(url) {
    $('#modal-user-allow .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-user-allow').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>