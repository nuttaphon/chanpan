<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\UserAllow */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="user-allow-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">User Allow</h4>
    </div>

    <div class="modal-body">
	
        <?php
            $sitecode = Yii::$app->user->identity->userProfile->sitecode;
            $userlist = yii\helpers\ArrayHelper::map(\backend\models\UserProfile::find()->where(['<>','sitecode',$sitecode])->andWhere(['is not','firstname',null])->andWhere(['<>','firstname',''])->all(), 'user_id', function ($model,$value) { return $model->firstname." ".$model->lastname ."(".$model->department.")";});
            
            echo $form->field($model, 'user_id')->widget(Select2::className(), [
                'data' => $userlist,
                'options' => ['placeholder' => 'เลือกรายชื่อผู้ใช้...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);        
        ?>
	<?= $form->field($model, 'sitecode')->hiddenInput(['value'=>Yii::$app->user->identity->userProfile->sitecode])->label(false); ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#user-allow-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-user-allow').modal('hide');
		$.pjax.reload({container:'#user-allow-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>