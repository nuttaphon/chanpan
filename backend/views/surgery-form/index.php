<?php
use common\lib\sdii\widgets\SDGridView;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'Module: ฟอร์มภาคศัลย์');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'ฟอร์มภาคศัลย์');

?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4 col-lg-4 hidden-sm hidden-xs">
<?php
echo \kartik\tree\TreeViewInput::widget(
        [
            'name' => 'kvTreeInput',
            'value' => 'false', // preselected values
            'query' => backend\models\TblTree::find()->where('readonly=1 or userid='.Yii::$app->user->id.' or id IN (select distinct root from tbl_tree where userid='.Yii::$app->user->id.')')->addOrderBy('root, lft'),
            'headingOptions' => ['label' => 'Categories'],
            'treeOptions' => ['style' => 'height:500px'],
            'rootOptions' => ['label'=>'<i class="fa fa-tree text-success"></i>'],
            'fontAwesome' => true,
            'asDropdown' => false,
            'multiple' => false,
            'options' => ['disabled' => false]            

        ]
        );
?>
    </div>
    <div class="col-sm-12 col-xs-12 hidden-lg hidden-md">
<?php
echo \kartik\tree\TreeViewInput::widget(
        [
            'name' => 'kvTreeInput',
            'value' => 'false', // preselected values
            'query' => backend\models\TblTree::find()->where('readonly=1 or userid='.Yii::$app->user->id.' or id IN (select distinct root from tbl_tree where userid='.Yii::$app->user->id.')')->addOrderBy('root, lft'),
            'headingOptions' => ['label' => 'Categories'],
            'rootOptions' => ['label'=>'<i class="fa fa-tree text-success"></i>'],
            'fontAwesome' => true,
            'asDropdown' => true,
            'multiple' => false,
            'options' => ['disabled' => false]            

        ]
        );
?><br>
    </div>      
    <div class="col-md-8 col-sm-12 col-xs-12">
<?= SDGridView::widget([
    'dataProvider' => $ezform,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],        
        'ezf_name',
        [
            'class' => 'common\lib\sdii\widgets\SDActionColumn' ,
            'template' => '{input}',
            'contentOptions' => ['style'=>'min-width:60px;'],
            'buttons' => [
                'input' => function ($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-share-alt"></span> บันทึกข้อมูล', [Url::to($model->comp_id_target=="" ? 'inputdata/step4': 'inputdata/step2'),  'comp_id_target' => $model->comp_id_target,'target'=> $model->comp_id_target=="" ? "skip":"", 'ezf_id' => $model->ezf_id], [
                            'data-action' => 'inputdata/step2',
                            'title' => Yii::t('yii', 'บันทึกข้อมูล'),
                            'class'=>'btn btn-primary btn-xs',
                            'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
                        ]);
                },

            ]
        ],
    ],
]); ?>
    </div>
  </div>
</div>
<?php $this->registerJs("
    
    $('.kv-tree li').on('click',function(event){
        event.stopPropagation();
        window.location.href = '?act='+$(this).attr('data-key');
        //console.log('Value: '+$(this).attr('data-key'));
    });

");?>
