<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/26/2016 AD
 * Time: 13:23
 */

use appxq\sdii\widgets\GridView;
use yii\widgets\Pjax;
use kartik\helpers\Html;
use yii\helpers\Url;

$this->registerJS($ezfdata['js']);
?>

<?=$ezfdata['detail'];?><hr>
<h4>เลือกช่วงเวลาแสดงข้อมูล</h4>
<div class="row">
    <form id="view-data-range" action="" method="get">
        <div class="col-md-6">
            จาก
            <?php
            echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                //'model'=>$model,
                'id'=>'start_date',
                'value' => $_GET['start_date'] ? $_GET['start_date'] : '01'.(date('-m-').(date('Y')+543)),
                'name' =>'start_date',
            ]);
            ?>
        </div>
        <div class="col-md-6">
            ถึง
            <?php
            echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                //'model'=>$model,
                'id'=>'end_date',
                'value' => $_GET['end_date'] ? $_GET['end_date'] : (date('d-m-').(date('Y')+543)),
                'name' =>'end_date',
            ]);
            ?>
        </div>
        <div class="col-md-12 text-right">
            <br>
            <?php
            echo ' '.Html::a('<span class="fa fa-list"></span> แสดงข้อมูลทั้งหมด',
                Url::to(['/surgery-form/data-management/', 'ezfdata' => $_GET['ezfdata'], 'show'=>'all']),
                [
                    'class' => 'btn btn-danger  btn-md',
                    'type' =>'submit',
                ]);
            ?>
            <?php
            echo Html::hiddenInput('ezfdata', $_GET['ezfdata']);
            echo Html::button('<span class="fa fa-search"></span> ค้นหา', [
                'class' => 'btn btn-primary btn-md',
                'type' =>'submit',
            ]);
            ?>
        </div>
    </form>
</div>
<hr>

    <?php

    if($_GET['ezfdata'] == '1476696513076161100'){
        $total = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM `dep_calendar`
        ', [':status' => 1])->queryScalar();

        $dpFix = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT * FROM `dep_calendar`',
            'totalCount' => $total,
            'sort' => [
                'attributes' => [
                    'hday', 'hmonth','hname'
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);


        echo GridView::widget([
            'panelBtn' => '',
            'dataProvider' => $dpFix,
            'columns' => [
                [
                    'header'=>'วันที่',
                    'attribute' => 'hday',
                ],
                [
                    'header'=>'วันเดือน',
                    'attribute' => 'hmonth',
                ],
                [
                    'header'=>'วันหยุด',
                    'attribute' => 'hname',
                ]
            ],
        ]);

    }
    

    $fieldSearch = $_GET['DataManagementSearch'];
    $columns = [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions'=>['style'=>'text-align: center;'],
            'contentOptions'=>['style'=>'width:50px;text-align: center;'],
        ],
    ];
    foreach ($fParams as $val){
        $columns[] = [
            'attribute' => $val,
            'value' =>  function($model) use($val){
                return $model->{$val} !='' ? $model->{$val} : '';
            },
            'filter' => count($filterType[$val]) ? (
            \kartik\widgets\Select2::widget([
                'name' => 'DataManagementSearch['.$val.']',
                'value' => is_null($fieldSearch[$val]) ? null : $fieldSearch[$val],
                'data' => $filterType[$val],
                'options' => ['placeholder' => 'Select for filter ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
            ) : true,
            //'filterOptions'=> count($filterType[$val]) ? ['style'=>'width: 50px;'] : [],
            'headerOptions'=> count($filterType[$val]) ? ['style'=>'width: 50px; text-align: center;'] : ['style'=>'text-align: center;'],
            'contentOptions'=>['style'=>'text-align: left;', 'field-id'=>$val],
        ];
    }
    $columns[] = ['class' => 'yii\grid\ActionColumn',
            'header'=>'จัดการ',
            'template' => '{view} {print}',
            'buttons' => [

                //view button
                'view' => function ($url, $model) use ($ezfdata) {
                    return Html::a('<span class="fa fa-edit"></span>', ['/inputdata/redirect-page', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id, 'rurl' => base64_encode(Yii::$app->request->url)], [
                        'title' => Yii::t('app', 'View'),
                        'class'=>'btn btn-primary btn-xs',
                        'target'=>'_blank'
                    ]);
		    
		    

                },
		'print' => function ($url, $model) use ($ezfdata) {
                    return Html::a('<span class="glyphicon glyphicon-print"></span>', ['/managedata/managedata/ezform-pdf', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id], [
                        'title' => Yii::t('app', 'View'),
                        'class'=>'btn btn-primary btn-xs',
                        'target'=>'_blank'
                    ]);
                },	
                /*
                'update' => function ($url, $model) use ($ezfdata) {
                    return Html::a('<span class="fa fa-edit"></span> Edit', ['/inputdata/redirect-page', 'ezf_id' => $ezfdata['ezf_id'], 'dataid'=>$model->id, 'rurl' => base64_encode(Yii::$app->request->url)], [
                        'title' => Yii::t('app', 'Update'),
                        'class'=>'btn btn-warning btn-xs',
                        'target'=>'_blank'
                    ]);

                },
                */
            ],
            'headerOptions'=>['style'=>'text-align: center;'],
            'contentOptions' => ['style' => 'width:100px;text-align: center;']
        ];
?>

<?php
    $url = '';
    if($ezform['comp_id_target']) {
        $url = Url::to([
            '/inputdata/step2/',
            'comp_id_target' => $ezform['comp_id_target'],
            'ezf_id' => $ezfdata['ezf_id']
        ]);
    }else{
        $url = Url::to([
            '/inputdata/insert-record/',
            'insert' => 'url',
            'ezf_id' => $ezfdata['ezf_id'],
            'target' => 'skip',
            'rurl' => base64_encode(Yii::$app->request->url)]);
    }
    echo GridView::widget([
        'panelBtn' => Html::a('<i class="fa fa-plus"></i> เพิ่มข้อมูล', $url, ['data-url'=>$url, 'class' => 'btn btn-success btn-sm', 'id'=>'btn-add-data', 'disabled'=>false]),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
]); ?>