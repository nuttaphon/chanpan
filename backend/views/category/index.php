<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

use common\components\category\CategoryWidget;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Category');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">
  <div class="row">
    <div class="form-group col-md-3">
      <button class="btn btn-block btn-primary btn-flat">เพิ่มกิจกรรมและโครงการ</button>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="treeview js-treeview1">
      </div>
    </div>
    <div class="col-md-4">
      <div class="treeview js-treeview1">
      </div>
    </div>
    <div class="col-md-4">
      <div class="treeview js-treeview1">
      </div>
    </div>
  </div>

  <?php
    $this->registerJs(CategoryWidget::widget(['categoryType' => 'full']));
  ?>
</div>

