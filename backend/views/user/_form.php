<?php

use common\models\User;
use common\models\UserProfile;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\UserForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */

$readonly = isset($_GET['readonly']) && $_GET['readonly']>0?true:false;
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-3 ">
		    <?php echo $form->field($modelProfile, 'picture')->widget(\trntv\filekit\widget\Upload::classname(), [
			'url' => ['/sign-in/avatar-upload']
		    ]) ?>
		</div>
	    <div class="col-md-9 sdbox-col">
		
		<?php echo $form->field($model, 'username')->textInput(['readonly'=>$readonly]) ?>
        <?php echo $form->field($model, 'email')->textInput(['readonly'=>$readonly]) ?>
        <?php echo $form->field($model, 'password')->passwordInput(['readonly'=>$readonly]) ?>
        <?php 
	if($readonly){
	    echo $form->field($model, 'status')->label(false)->hiddenInput();
	} else {
	    echo $form->field($model, 'status')->label(Yii::t('backend', 'Active'))->checkbox();
	}
	
		
		?>
    
	<div class="box box-solid box-success " id="personalForm2" >
    <div class="box-header">
        ชื่อและข้อมูลการติดต่อ
    </div>
    <br>
    <div class="box-body">
        <div class='form-horizontal'>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label">คำนำหน้าชื่อ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'title')->dropDownList(['0'=>'นาย','1'=>'นาง','2'=>'นางสาว'], ['readonly'=>$readonly])->label(false)
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> ชื่อ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'firstname')->textInput(['readonly'=>$readonly])->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> นามสกุล</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'lastname')->textInput(['readonly'=>$readonly])->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label">เพศ</label>
                <div class="col-sm-8">
                    <?php
		    if($readonly){
			$sex = ArrayHelper::getValue(['1'=>'ชาย','2'=>'หญิง'], $modelProfile->gender);
			echo '<div class="form-group required">'.Html::textInput('sex', $sex, ['class'=>'form-control', 'readonly'=>$readonly]).'</div>';
			echo Html::activeHiddenInput($modelProfile,'gender');
		    } else {
			echo $form->field($modelProfile,'gender')->radioList(['1'=>'ชาย','2'=>'หญิง'])->label(false);
		    }
                    
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> อีเมล์</label>
                <div class="col-sm-8">
                    <?php 
			echo $form->field($modelProfile,'email')->textInput(['readonly'=>$readonly])->label(false);
//			echo $form->field($modelProfile,'email')->widget(MaskedInput::className(),[
//                        'clientOptions'=>[
//                            'alias'=>'email'
//                        ],
//			'options'=>['class'=>'form-control', 'readonly'=>$readonly]
//                    ])->label(false);
			?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> เบอร์โทรศัพท์</label>
                <div class="col-sm-8">
                    <?php echo $form->field($modelProfile,'telephone')->widget(MaskedInput::className(),[
                        'mask'=>'9999999999',
			'options'=>['class'=>'form-control', 'readonly'=>$readonly]
                    ])->label(false);
                    ?>
                </div>
            </div>
	    
        </div>

    </div>
    
    
        <!-- <div class="box box-footer"  >
            <button type="button" id='btnForm2'  style="float:right;" class="btn btn-success">ถัดไป</button>
        </div> -->
</div>
    
    <div class="box box-solid box-success  " id="personalForm3" >
    <div class="box-header">
        เลือกบทบาท
    </div>
    <br>
        <div class="box-body">
	    <?php
	    
		if($readonly){
			$sex = ArrayHelper::getValue(['หน่วยงานบริการสุขภาพ', 'หน่วยงานอื่นๆ'], $modelProfile->inout);
			echo '<div class="form-group">'.Html::textInput('inout', $sex, ['class'=>'form-control', 'readonly'=>$readonly]).'</div>';
			echo Html::activeHiddenInput($modelProfile,'inout');
		    } else {
			echo $form->field($modelProfile, 'inout')->radioList(['หน่วยงานบริการสุขภาพ', 'หน่วยงานอื่นๆ'])->label(FALSE);
		    }  
	    ?>
            <div class='form-group <?=(strlen($modelProfile->sitecode)<10)?'has-error':''?>' id="rowDepartment" >
                  <label><code>*</code> เลือกหน่วยงาน</label>
                  <?php
		  echo Html::activeHiddenInput($modelProfile, 'cid');
		  echo Html::activeHiddenInput($modelProfile, 'department');
		  echo yii\jui\AutoComplete::widget([
		    'model' =>$modelProfile,
		    'attribute' => 'sitecode',
		      
		    'clientOptions' => [
			'minChars' => 1,
			'source' => new JsExpression("function(request, response) {
			    $.getJSON('".yii\helpers\Url::to(['/user/querys'])."', {
				q: request.term
			    }, response);
			}"),
			'select' => new JsExpression("function( event, ui ) {
			    $('#userprofile-department').val(ui.item.id);
			 }"),
			'change' => new JsExpression("function( event, ui ) {
			    if (!ui.item) {
				$('#userprofile-department').val('');
			    }
			 }"),
			],
		     'options'=>['readonly'=>$readonly, 'class'=>'form-control'] 
		]);
		  
		  ?>
		  <p class="help-block">กรุณาค้นด้วยเลขห้าหลัก หรือส่วนหนึ่งของชื่อแล้วคลิกเลือกรายการที่ปรากฏ</p>
            </div>
            
            <div class="form-group">
                <label>เลือกบทบาท (สามารถเลือกได้มากกว่าหนึ่งบทบาท)</label>
                <div class="checkbox">
                    <?php
		    if($readonly){
			$sex = ArrayHelper::getValue(array('0'=>'ผู้รับบริการหรือบุคคลทั่วไป', '1'=>'บุคลากร'), $modelProfile->status);
			if($modelProfile->status==1){
			    $sex = 'ผู้รับบริการหรือบุคคลทั่วไป, '.$sex;
			}
			
			echo '<div class="form-group">'.Html::textInput('status', $sex, ['class'=>'form-control', 'readonly'=>$readonly]).'</div>';
			echo Html::activeHiddenInput($modelProfile,'status');
		    } else {
			echo Html::checkbox('tmpStatus', true, ['label'=>'ผู้รับบริการหรือบุคคลทั่วไป']);
			echo $form->field($modelProfile,'status')->checkbox(array('label'=>'บุคลากร'))->label(false);
			
			//echo $form->field($modelProfile,'status')->radioList(array('0'=>'ผู้รับบริการหรือบุคคลทั่วไป', '1'=>'บุคลากร'))->label(false);
		    }  
		    ?>
                </div>
               
            </div>
            <div id="status-personal-form" style="<?=($modelProfile->status=='0')?'display:none;':''?>">
            <div class="row" id="rowPersonal">
                <div class="col-lg-6">
                    <label>เลือกบทบาทเฉพาะสังกัด</label>
                    <?php
                    echo $form->field($modelProfile,'status_personal')->dropDownList(
                        [
                            ''=>'-- เลือกบทบาท --',
                            '2'=>'ผู้ดูแลระบบฐานข้อมูล',
                            '3'=>'ผู้ให้บริการด้านการแพทย์และสาธารณสุข',
                            '4'=>'ผู้บริหาร',
                            '10'=>'นักวิจัย',
                            '11'=>'อื่นๆ ',

                        ], ['readonly'=>$readonly])->label(false)
                    ?>
                </div>
                <div class="col-lg-6" id="rowManager" style="<?=($modelProfile->status_personal=='4')?'':'display:none;'?>">
                    <label>เลือกระดับ</label>
                    <?php
                      echo $form->field($modelProfile,'status_manager')->dropDownList([
                              ''=>'-- เลือกระดับหน่วยงาน --',
                              '5'=>'หน่วยงานระดับประเทศ',
                              '6'=>'หน่วยงานระดับเขต',
                              '7'=>'หน่วยงานระดับจังหวัด',
                              '8'=>'หน่วยงานระดับอำเภอ',
                              '9'=>'หน่วยงานทางการแพทย์และสาธารณสุข'
                              ], ['readonly'=>$readonly])->label(false);
                    ?>
                </div>

                <div class="col-lg-6" id="rowStatusOther" style="<?=($modelProfile->status_personal=='11')?'':'display:none;'?>">
                    <label>บทบาทอื่นๆ</label>
                    <?php
                    echo $form->field($modelProfile,'status_other')->textInput(['readonly'=>$readonly])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowNation" style="<?=($modelProfile->status_manager=='5')?'':'display:none;'?>">
                <div class="col-lg-12">
                    <label>ชื่อหน่วยงานระดับประเทศ</label>
                    <?php
                    echo $form->field($modelProfile,'department_area_text')->textInput(['readonly'=>$readonly, 'disabled'=>$modelProfile->status_manager!='5'])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowArea" style="<?=($modelProfile->status_manager=='6')?'':'display:none;'?>">
                <div class="col-lg-6">
                    <label>หน่วยงานระดับเขต</label>
                    <?php
                    echo $form->field($modelProfile,'department_area')->dropDownList([''=>'-- เลือกเขตบริการ --',
                        '1'=>'เขตบริการที่ 1','2'=>'เขตบริการที่ 2','3'=>'เขตบริการที่ 3','4'=>'เขตบริการที่ 4','5'=>'เขตบริการที่ 5','6'=>'เขตบริการที่ 6','7'=>'เขตบริการที่ 7',
                        '8'=>'เขตบริการที่ 8','9'=>'เขตบริการที่ 9','10'=>'เขตบริการที่ 10','11'=>'เขตบริการที่ 12','13'=>'เขตบริการที่ 13','14'=>'เขตบริการที่ 14'
                    ], ['readonly'=>$readonly])->label(false)
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงานระดับเขต</label>
                    <?php
                    echo $form->field($modelProfile,'department_area_text')->textInput(['readonly'=>$readonly, 'disabled'=>$modelProfile->status_manager!='6'])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowProvince" style="<?=($modelProfile->status_manager=='7')?'':'display:none;'?>">
                <div class="col-lg-6">
                    <label>หน่วยงานระดับจังหวัด</label>
                    <?php
                    echo Select2::widget([
                        'options' => ['placeholder' => 'จังหวัด','id'=>'province','value'=>'test', 'readonly'=>$readonly],
                        'data' => ArrayHelper::map($dataProvince,'PROVINCE_CODE','PROVINCE_NAME'),
                        'model' =>$modelProfile,
                        'attribute'=>'department_province',
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงาน</label>
                    <?php
                        echo $form->field($modelProfile,'department_province_text')->textInput(['readonly'=>$readonly])->label(false);
                    ?>
                </div>
            </div>
            <div class="row"  id="rowAmphur" style="<?=($modelProfile->status_manager=='8')?'':'display:none;'?>">
                <div class="col-lg-6"  >
                    <label>หน่วยงานระดับอำเภอ</label>
                    <?php
                    echo $form->field($modelProfile,'department_amphur')->widget(DepDrop::classname(),[
                        'type'=>  DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'amphur', 'readonly'=>$readonly],
                        'model'=>$modelProfile,
                        'attribute'=>'department_amphur',
                        'pluginOptions'=>[
                            'depends'=>['province'],
                            'placeholder'=>'อำเภอ',
                            'url'=>'/user/genamphur'
                        ]
                    ])->label(false);
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงานระดับอำเภอ</label>
                    <?php
                    echo $form->field($modelProfile,'department_amphur_text')->textInput(['readonly'=>$readonly])->label(false);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="box box-footer"  >
        <button type="button" id="btnForm3" class="btn btn-success" style="float:right;">ถัดไป</button>
    </div> -->
</div>
    
        <?php 
	if($readonly){
	    echo $form->field($model, 'roles')->checkboxList($roles, [
		'item' => function($index, $label, $name, $checked, $value){
		    $disable = false;
		    if ($value == 'administrator') {
			$disable = true;
			if($checked){
			    $checkbox = Html::hiddenInput($name, $value);
			}
		    } else {
			$checkbox = Html::checkbox($name, $checked, ['value' => $value, 'disabled' => $disable]);
		    }

		    return Html::tag('div', Html::label($checkbox . $label), ['class' => 'checkbox']);
		}	
		]);
	} else {
	    echo $form->field($model, 'roles')->checkboxList($roles);
	}  
	
	 ?>
	
	<div class='form-group'>
                <label for="enroll_key" class="control-label"> Enroll key</label>
                
                    <?php
                    echo $form->field($modelProfile,'enroll_key')->textInput(['readonly'=>TRUE])->label(false);
                    ?>
               
            </div>
	    
	    <div class='form-group'>
                <label for="admindb" class="control-label"> Adminsite?</label>
               
		    
		    <?php 
		    if(in_array($modelProfile->department, Yii::$app->session['admindb'])){
			echo Html::textInput('admindb', 'มีแล้ว', ['class'=>'form-control','readonly'=>TRUE]);
		    } else {
			echo Html::textInput('admindb', 'ยังไม่มี', ['class'=>'form-control','readonly'=>TRUE]);
		    }
		    ?>
		    
                    
               
            </div>
		
    <label class="control-label" for="userprofile-secret_file">เอกสารรักษาความลับ</label>
    <?php
		$secret_file = '#';
		if($modelProfile->secret_file!=''){
		    $secret_file = Yii::getAlias('@storageUrl') . '/source/'.$modelProfile->secret_file;
		    $secret_file_old = $secret_file;
		    $ext = strtolower(pathinfo($modelProfile->secret_file, PATHINFO_EXTENSION));
		    if($ext=='pdf'){
			$secret_file = Yii::getAlias('@storageUrl').'/source/pdf_icon.png';
		    }
		?>
    <div class=""><a class="file-preview-image" data-filename="<?=$secret_file_old?>"><img src="<?=$secret_file?>"  height="100" ></a></div>
		<?php } ?>
		<br>
		<label class="control-label" for="userprofile-citizenid_file">สำเนาบัตรประชาชน</label>
		<?php
		$citizenid_file = '#';
		if($modelProfile->citizenid_file!=''){
		    $citizenid_file = Yii::getAlias('@storageUrl') . '/source/'.$modelProfile->citizenid_file;
		    $citizenid_file_old = $citizenid_file;
		    $ext = strtolower(pathinfo($modelProfile->citizenid_file, PATHINFO_EXTENSION));
		    if($ext=='pdf'){
			$citizenid_file = Yii::getAlias('@storageUrl').'/source/pdf_icon.png';
		    }
		?>
		<div class=""> <a class="file-preview-image" data-filename="<?=$citizenid_file_old?>"><img src="<?=$citizenid_file?>" height="100" ></a></div>
		<?php } ?>
		<br>
		<hr>
		<?=$form->field($modelProfile, 'approved')->radioList([2=>'รอเอกสาร', 0=>'ไม่อนุมัติ', 1=>'อนุมัติ'])?>
		
		<?php
		$date = Yii::$app->formatter->asDate($user->updated_at, 'php:Y-m-d H:i:s');
		$str = common\lib\sdii\components\utils\SDdate::differenceTimer($date);
		?>
		
		<?=$form->field($modelComment, 'user_id')->hiddenInput()->label(false)?>
		<?=$form->field($modelComment, 'name')->hiddenInput()->label(false)?>
		<?=$form->field($modelComment, 'comment')->textarea()?>
        <div class="form-group">
            <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
		<?php if($dataComment):?>
		<ul class="media-list">
		    <?php foreach ($dataComment as $key => $value):?>
			<?php
			$approved = $value['action'];
			$icon = 'style=" color:#333;"';
			$css = 'heart';
			$title = 'ยังไม่มีการตรวจสอบ';
			if($approved===0){
			    $icon = 'style="color:#d73925;"';
			    $title = 'ไม่อนุมัติ';
			} elseif ($approved===1) {
			    $icon = 'style="color:#008d4c;"';
			    $title = 'อนุมัติ';
			} elseif ($approved===2) {
			    $icon = 'style="color:#e08e0b;"';
			    $title = 'รอเอกสาร';
			} elseif ($approved===3) {
			    $icon = 'style="color:#d73925;"';
			    $css = 'remove';
			    $title = 'มอบสิทธิ์โดย Site admin';
			} elseif ($approved===4) {
			    $icon = 'style="color:#e8889e;"';
			    $title = 'อัพโหดไฟล์ใหม่';
			} 
//			elseif ($approved===5) {
//			    $icon = 'style="color:#3c8dbc;"';
//			    $css = 'earphone';
//			    $title = 'โทรติดต่อแล้ว';
//			}
			
			?>
			<li class="media well well-sm">
			  <div class="media-body">
			      <h4 class="media-heading"><?=$value['name']?> <small> <i class="glyphicon glyphicon-time"></i> <?=  common\lib\sdii\components\utils\SDdate::differenceTimer($value['update_at'])?> 
				      <span <?=$icon?>><i class="glyphicon glyphicon-<?=$css?>"></i> <?=$title?></span></small></h4>
			    <?=$value['comment']?>
			  </div>
			</li>
		
		    <?php endforeach; ?>
		</ul>
		<?php endif;?>
	    </div>
	</div>
	
        
    <?php ActiveForm::end(); ?>

</div>
 <?=	    common\lib\sdii\widgets\SDModalForm::widget([
		'id' => 'modal-move-units',
		'size'=>'modal-lg',
		'tabindexEnable' => false,
	    ]);
	    ?>
<?php
$this->registerJs(
    "	    $('body').tooltip('destroy');

	    $('input:checkbox[name=\"UserProfile[status]\"]').on('click',function(){
                if($(this).is(':checked')){
                    $('#status-personal-form').show();  // checked
                } else {
                    $('#status-personal-form').hide();
		    $('#userprofile-status_personal').val('');
		    $('#userprofile-status_personal').trigger('change');
		}
            });
	    

            $('#buttonCid').on('click',function(){
                var cid = $('#puser-cid').val();

                    $('#form2').attr('class','label label-success');
                    $('#personalForm2').attr('class','box box-solid box-success');
                    $('#hideCidForm').show();

            });

            $('#btnForm2').on('click',function(){
                    $('#form3').attr('class','label label-success');
                    $('#personalForm3').attr('class','box box-solid box-success');
            });

            $('#userprofile-status_personal').on('change',function(){
                var type = $(this).val();
                if(type==4){
		    $('#rowManager').show();
                    $('#rowStatusOther').hide();
                }else if(type==11){
                    $('#rowStatusOther').show();
		    $('#userprofile-status_manager').val('');
		    $('#userprofile-status_manager').trigger('change');
                    $('#rowManager').hide();
                }else{
                    $('#rowStatusOther').hide();
		    $('#userprofile-status_manager').val('');
		    $('#userprofile-status_manager').trigger('change');
                    $('#rowManager').hide();
                }
            });
            $('#userprofile-status_manager').on('change',function(){
                var type = $(this).val();

                if(type==5){
                  $('#rowNation').show();
                  $('#rowProvince').hide();
                  $('#rowArea').hide();
                  $('#rowAmphur').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', false);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', true);
                }else if(type==6){
                  $('#rowArea').show();
                  $('#rowNation').hide();
                  $('#rowProvince').hide();
                  $('#rowAmphur').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', true);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', false);
                }
                else if(type==7){
                  $('#rowProvince').show();
                  $('#rowNation').hide();
                  $('#rowArea').hide();
                  $('#rowAmphur').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', true);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', true);
                }else if(type==8){
                  $('#rowProvince').show();
                  $('#rowAmphur').show();
                  $('#rowNation').hide();
                  $('#rowArea').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', true);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', true);
                }else{
                  $('#rowArea').hide();
                  $('#rowNation').hide();
                  $('#rowProvince').hide();
                  $('#rowAmphur').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', true);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', true);
                }
            });
            
            $('#btnAddMoreHospital').on('click',function(){
              var nameHospital = $('#addMoreHospital').val();
              if(nameHospital==''){
                $('#rowMoreHospital').attr('class','form-group has-error');
                $('.hospital-error').html('กรุณาระบุ');
              }else{
                $('#rowMoreHospital').attr('class','form-group');
                $('.hospital-error').html('');
                // $('#rowMoreHospital').hide();
                $.post('/user/formquery/addmorehospital',{nameHospital:nameHospital},function(result){
                    console.log(result);
                });
              }
            });
	    
	    $('.file-preview-image').click(function(){
                    var img = $(this).attr('src');
		    var filename = $(this).attr('data-filename');
                    modalMoveUnit('" . Url::to(['/sign-in/viewimg', 'img' => '']) . "'+filename);
               });
	       
	       function modalMoveUnit(url) {
		    $('#modal-move-units .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
		    $('#modal-move-units').modal('show')
		    .find('.modal-content')
		    .load(url);
		}
    "
);
?>