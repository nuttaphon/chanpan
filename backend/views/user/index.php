<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\lib\sdii\components\helpers\SDNoty;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index" >

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

  <?php
  Yii::$app->session['admindb'] = \yii\helpers\ArrayHelper::getColumn($dataAdminsite, 'sitecode');
  //yii\helpers\VarDumper::dump(Yii::$app->session['admindb']);
  ?>
   
    <p>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'options' => ['class'=>'form-inline text-right']
    ]); ?>
	
    <?php 
    
    
    echo '<div class="form-group">';
    echo '<label for="usersearch-username">ค้นหา</label> ';
    echo Html::activeTextInput($searchModel, 'username', ['style'=>'width: 200px;', 'class'=>'form-control', 'placeholder'=>'username, ชื่อ-สกุล และ sitecode']);
    echo '</div>';
    echo ' <div class="form-group">';
    echo '<label for="usersearch-username">บทบาท</label> ';
    echo Html::activeDropDownList($searchModel, 'role', \backend\modules\article\components\ArticleFunc::itemAlias('signup'),['class'=>'form-control', 'prompt'=>'All']);
    echo '</div>';
     echo ' <div class="form-group">';
    echo '<label for="usersearch-username">สถานะ</label> ';
    echo Html::activeDropDownList($searchModel, 'approve_profile', [-2=>'All', -1=>'ยังไม่มีการตรวจสอบ', 0=>'ไม่อนุมัติ', 1=>'อนุมัติ', 2=>'รอเอกสาร', 3=>'มอบสิทธิ์โดย Site admin', 4=>'อัพโหลดไฟล์ใหม่'],['class'=>'form-control']);
    echo '</div>';
    echo ' <div class="form-group">';
    echo '<label for="usersearch-username">จำนวนวัน</label> ';
    echo Html::activeTextInput($searchModel, 'difftime', ['style'=>'width: 100px;', 'class'=>'form-control', 'type'=>'number', 'min'=>0]);
    echo '</div>';
    echo ' <div class="form-group">';
    echo Html::activeCheckbox($searchModel, 'approve');
    echo '</div>';
    echo ' <div class="form-group">';
    echo Html::activeCheckbox($searchModel, 'notfile');
    echo '</div>';
    ?>
    
    <?php echo Html::submitButton(Yii::t('backend', 'ค้นหา'), ['class' => 'btn btn-primary']) ?>
	
    <?php ActiveForm::end(); ?>
    </p>
<?php Pjax::begin(['id'=>'user-grid-pjax', 'timeout' => 10000]) ?>
    <?php echo \common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
	    'class' => 'yii\grid\SerialColumn',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:50px;text-align: center;'],
	    ],
	    [
		'header' => 'ชื่อ-สกุล',
		'value' => function ($model) {
		    return $model->userProfile->firstname.' '.$model->userProfile->lastname;
		},
		//'headerOptions'=>['style'=>'text-align: center;'],
		//'contentOptions'=>['style'=>'width:190px;'],//text-align: center;
	    ],
	    [
		'header' => 'Admin?',
		'value' => function ($model) {
		    $sitecode = $model->userProfile->sitecode;
		    if(in_array($sitecode, Yii::$app->session['admindb'])){
			return '<i class="glyphicon glyphicon-ok"></i>';
		    }
		    return '<i class="glyphicon glyphicon-remove-sign"></i>';
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
	    [
		'header' => 'Sitecode',
		'value' => function ($model) {
		    $sql = "SELECT all_hospital_thai.hcode, 
			    all_hospital_thai.`name`, 
			    all_hospital_thai.tambon, 
			    all_hospital_thai.amphur, 
			    all_hospital_thai.province, 
			    all_hospital_thai.code5
		    FROM all_hospital_thai
		    WHERE all_hospital_thai.hcode = :code";
		
		$sitecode = $model->userProfile->sitecode;
		$data = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();
		if($data){
		    $title = "{$data['name']} ต.{$data['tambon']} อ.{$data['amphur']} จ.{$data['province']}";
		    return '<a data-toggle="tooltip" title="'.$title.'">'.$sitecode.'</a>';
		}
		   return $sitecode;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
	    [
		'header' => 'ใบ/บัตร',
		'value' => function ($model) {
		   $userProfile = $model->userProfile;
		   $secret_file = !empty($userProfile->secret_file)?'y':'n';
		   $citizenid_file = !empty($userProfile->citizenid_file)?'y':'n';
		   return $secret_file.'/'.$citizenid_file;
		},
		//'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:70px;text-align: center;'],
	    ],	
		[
		'header' => 'เวลา',
		'value' => function ($model) {
		    $date = Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d H:i:s');
		    $str = common\lib\sdii\components\utils\SDdate::differenceTimer($date);
		    
		    return $str;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px;text-align: center;'],
	    ],		
	    [
		'attribute'=>'created_at',
		'header' => 'วันที่สมัคร',
		'value' => function ($model) {
		    Yii::$app->formatter->locale = 'th';

		   return Yii::$app->formatter->asDate($model->created_at, 'short');
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
			[
		'header' => 'Enroll key',
		'value' => function ($model) {
		    return $model->userProfile->enroll_key;
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;'],//text-align: center;
	    ],
	[
		'header' => 'อนุญาต',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('manager', $model->id);
		    $approved = $model->userProfile->approved;
		    $icon = 'style=" color:#333;"';
		    $css = 'heart';
		    $title = 'ยังไม่มีการตรวจสอบ';
		    if($approved===0){
			$icon = 'style="color:#d73925;"';
			$title = 'ไม่อนุมัติ';
		    } elseif ($approved===1) {
			$icon = 'style="color:#008d4c;"';
			$title = 'อนุมัติ';
		    } elseif ($approved===2) {
			$icon = 'style="color:#e08e0b;"';
			$title = 'รอเอกสาร';
		    } elseif ($approved===3) {
			$icon = 'style="color:#d73925;"';
			$css = 'remove';
			$title = 'มอบสิทธิ์โดย Site admin';
		    } elseif ($approved===4) {
			$icon = 'style="color:#e8889e;"';
			$title = 'อัพโหลดไฟล์ใหม่';
		    }
//		    elseif ($approved===5) {
//			$icon = 'style="color:#3c8dbc;"';
//			$css = 'earphone';
//			$title = 'โทรติดต่อแล้ว';
//		    }
		    $approved = ' '.Html::a('<i class="glyphicon glyphicon-'.$css.'" '.$icon.'></i>', yii\helpers\Url::to(['update', 'id' => $model->id, 'readonly' => 1]), [
                                    'title' => Yii::t('app', $title),
                                    'class'=>'btn btn-default btn-xs',
			    ]);
		    
		    if (isset($auth->roleName)) {
			
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'manager'])
			]) . $approved;
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'manager'])
			]) . $approved;
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px;text-align: center;'],
		
	    ],		
	    [
		'header' => 'Admin site',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('adminsite', $model->id);
		    if (isset($auth->roleName)) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'adminsite'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'adminsite'])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],
	    
	    [
		'header' => 'จัดการหน้าแรก',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('frontend', $model->id);
		    if (isset($auth->roleName)) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'frontend'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'frontend'])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px;text-align: center;'],
	    ],
	    [
		'header' => 'Active',
		'value' => function ($model) {
		    
		    if ($model->status==1) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model->id])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model->id])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:60px;text-align: center;'],
	    ],
            //'email:email',
//            [
//                'class' => \common\grid\EnumColumn::className(),
//                'attribute' => 'status',
//                'enum' => User::getStatuses(),
//                'filter' => User::getStatuses()
//            ],
            //'created_at:datetime',
            //'logged_at:datetime',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการผู้ใช้',
                'template' => '{update} {delete}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			if(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')){
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			}
                    },
                    'delete' => function ($url, $model) {
			if(Yii::$app->user->can('administrator') ){
			    if($model->id != Yii::$app->user->getId()){
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			    }
			}
                    },
                ],
                'contentOptions' => ['style' => 'width:130px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end() ?>
</div>

<?php  $this->registerJs("

$('#user-grid-pjax').on('click', '.manager-btn', function(){
    updateAttr($(this).attr('data-url'));
});

function updateAttr(url) {
    $.post(
	url
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    $.pjax.reload({container:'#user-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    }).fail(function(){
	console.log('server error');
    });
}
");?>