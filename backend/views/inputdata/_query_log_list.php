<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 5/4/2559
 * Time: 12:21
 * E-mail: kongvut@gmail.com
 */
?>


<a href="<?=\yii\helpers\Url::to(['/query-request/view', 'id'=>$model->id])?>" class="list-group-item">
    <h4 class="list-group-item-heading"><?php echo $model->note; ?></h4>
    <p class="list-group-item-text">สถานะ
        <?php
        if($model->status == 1){
            $model->status = 'Waiting';
        }else if($model->status ==2){
            $model->status = 'Resolve with some change';
        }else if($model->status ==3){
            $model->status = 'Resolve without any change';
        }else if($model->status ==4){
            $model->status = 'Unresolvable';
        }else if($model->status ==5){
            $model->status = 'Remark';
        }
        echo $model->status;?></p>
</a>

