<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use backend\models\Dynamic;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use common\lib\sdii\widgets\SDModalForm;
use yii\web\JsExpression;
//use common\lib\sdii\widgets\SDModalForm;
//use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ezforms\models\EzformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'บันทึกข้อมูล');
//$this->params['subtitle'] = 'Description';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss('
.tooltip-inner {
    max-width: 600px;
    padding: 3px 8px;
    color: #fff;
    background-color: #000;
    border-radius: 4px;
    text-align: left;
    font-size: medium;
    white-space: pre;
}
');

$this->registerCssFile('/css/ezform.css');
$this->registerJsFile('/jqueryloading/jloading.js',['depends' => [\yii\web\JqueryAsset::className()]]);
//set values
$xsourcex = Yii::$app->user->identity->userProfile->sitecode;
$xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
$xdepartmentx_sql = $xdepartmentx > 0 ? ("AND xdepartmentx ='".$xdepartmentx."'") : null;
$input_ezf_id = Yii::$app->request->get('ezf_id');
$input_target= Yii::$app->request->get('target');
$input_comp_target =Yii::$app->request->get('comp_id_target');
$input_dataid =Yii::$app->request->get('dataid');
?>

<div class="inputdata-index">
    <a id="watch-video" href="#" style="display: <?=(Yii::$app->keyStorage->get('frontend.domain')!='thaipalliative.org' ? 'none' : ''); ?>" data-url="/inputdata/watch-video/?type=palliative"><p class="pull-right text-bold"><span class="h4">VDO สอนการใช้งาน</span> <i class="fa fa-4x fa-youtube-play text-danger"></i></p></a>
    <div class="sdbox-header"><div class="pull-right"><a href="/inputdata/consult" class="btn btn-success"><span class="fa fa-comments-o"></span> ระบบขอคำปรึกษา</a></div>
        <h3><i class="fa fa-file-text" aria-hidden="true"></i> ขั้นที่ 1. เลือกฟอร์มเพื่อทำงาน</h3>
        <?php echo Html::a("ท่านมีสิทธิ์ใช้งานฟอร์มทั้งหมด (".$ezfAllCanUse.") ฟอร์ม [<li class=\"fa fa-hand-o-right\"></li> คลิกที่นี่เพื่อเลือกฟอร์มอื่นๆ]", '#', ['class'=>'h4 text text-info', 'id'=>'add-form-fav']); ?>

    </div>


    <?php
    $this->registerJs("
$('#add-form-fav').on('click', function(){
    modalQueryRequest('/inputdata/favorite-form');
    $('#modal-query-request').on('hidden.bs.modal', function () {
        // do something…
        runBlockUI('รอสักครู่กำลังจัดรายการฟอร์มใหม่...');
        location.reload(true);
    })
});
");

    echo SDModalForm::widget([
        'id' => 'modal-query-request',
        'size' => 'modal-lg',
        'tabindexEnable' => false
    ]);

    //
    if(!$input_ezf_id) {
        $totalCount = $dataProvider->totalCount;
        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'ชื่อฟอร์ม',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->ezf_name;
                },
                'filterType' => GridView::FILTER_COLOR,
                'vAlign' => 'middle',
                'format' => 'raw',
                'width' => '300px',
                'noWrap' => true
            ],
            [
                'attribute' => 'All',
                'value' => function ($model, $key, $index, $widget) use ($xsourcex, $xdepartmentx_sql) {
                    $res = Yii::$app->db->createCommand("SELECT count(*) as total FROM " . ($model->ezf_table) . " WHERE (rstat <> '3') AND xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
                    return Html::a(Html::encode($res['total']), ['step4', 'comp_id_target' => $model->comp_id_target, 'ezf_id' => $model->ezf_id, 'target' => 'all', 'tabstep3' => '1'], ['class' => 'open-form badge', 'data-pjax' => 0]);
                },
                'hAlign' => 'right',
                'filterType' => GridView::FILTER_COLOR,
                'vAlign' => 'middle',
                'format' => 'raw',
                'width' => '30px',
                'noWrap' => true
            ],
            [
                'attribute' => 'Submitted / Draft / New',
                'value' => function ($model, $key, $index, $widget) use ($xsourcex, $xdepartmentx_sql) {

                    $res = Yii::$app->db->createCommand("SELECT SUM(`rstat` = 2) as submitted, SUM(`rstat` = 1) as draft, SUM(`rstat` = 0) as newrecord FROM " . ($model->ezf_table) . " WHERE xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
                    $html = Html::a(Html::encode($res['submitted'] + 0), ['step4', 'comp_id_target' => $model->comp_id_target, 'ezf_id' => $model->ezf_id, 'target' => 'all', 'rstat' => 'submitted', 'tabstep3' => '1'], ['class' => 'open-form badge', 'data-pjax' => 0]);
                    $html .= ' / ' . Html::a(Html::encode($res['draft'] + 0), ['step4', 'comp_id_target' => $model->comp_id_target, 'ezf_id' => $model->ezf_id, 'target' => 'all', 'rstat' => 'draft', 'tabstep3' => '1'], ['class' => 'open-form badge', 'data-pjax' => 0]);
                    $html .= ' / ' . Html::a(Html::encode($res['newrecord'] + 0), ['step4', 'comp_id_target' => $model->comp_id_target, 'ezf_id' => $model->ezf_id, 'target' => 'all', 'rstat' => 'newrecord', 'tabstep3' => '1'], ['class' => 'open-form badge', 'data-pjax' => 0]);

                    return $html;
                },
                'hAlign' => 'right',
                'filterType' => GridView::FILTER_COLOR,
                'vAlign' => 'middle',
                'format' => 'raw',
                'width' => '40px',
                'noWrap' => true
            ],
            [
                'attribute' => 'ไม่มีเป้าหมาย',
                'value' => function ($model, $key, $index, $widget) use ($xsourcex, $xdepartmentx_sql) {
                    $res = Yii::$app->db->createCommand("SELECT count(*) as total FROM " . $model->ezf_table . " WHERE rstat <> 3 AND target IS NULL AND xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
                    return Html::a(Html::encode($res['total']), ['step4', 'comp_id_target' => $model->comp_id_target, 'ezf_id' => $model->ezf_id, 'target' => 'skip', 'tabstep3' => '1'], ['class' => 'open-form badge', 'data-pjax' => 0]);
                },
                'hAlign' => 'right',
                'filterType' => GridView::FILTER_COLOR,
                'vAlign' => 'middle',
                'format' => 'raw',
                'width' => '30px',
                'noWrap' => true
            ],
            [
                'attribute' => 'ข้อมูลฉันบันทึก',
                'value' => function ($model, $key, $index, $widget) use ($xsourcex, $xdepartmentx_sql) {
                    $res = Yii::$app->db->createCommand("SELECT count(*) as total FROM " . $model->ezf_table . " WHERE (rstat <> '3')  AND user_create ='" . Yii::$app->user->id . "' AND xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
                    return Html::a(Html::encode($res['total']), ['step4', 'comp_id_target' => $model->comp_id_target, 'ezf_id' => $model->ezf_id, 'target' => 'byme', 'tabstep3' => '1'], ['class' => 'open-form badge', 'data-pjax' => 0]);
                },
                'hAlign' => 'right',
                'filterType' => GridView::FILTER_COLOR,
                'vAlign' => 'middle',
                'format' => 'raw',
                'width' => '30px',
                'noWrap' => true
            ],
            [
                'attribute' => 'ถูกลบ',
                'value' => function ($model, $key, $index, $widget) use ($xsourcex, $xdepartmentx_sql) {
                    $res = Yii::$app->db->createCommand("SELECT count(*) as total FROM " . $model->ezf_table . " WHERE rstat = '3' AND xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
                    return Html::a(Html::encode($res['total']), null, ['class' => 'badge', 'data-pjax' => 0]);
                },
                'hAlign' => 'right',
                'filterType' => GridView::FILTER_COLOR,
                'vAlign' => 'middle',
                'format' => 'raw',
                'width' => '30px',
                'noWrap' => true
            ],
            [
                'attribute' => 'Action',
                'value' => function ($model, $key, $index, $widget) {
                    if (0) {
                        return Html::a(Html::encode("เริ่มบันทึก"), ['special-target1', 'ezf_id' => $model->ezf_id], ['class' => 'btn btn-success', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'เริ่มบันทึกข้อมูล')]);
                    } else {
                        return Html::a(Html::encode("เริ่มบันทึก"), [($model->comp_id_target ? 'step2' : 'step4'), 'comp_id_target' => $model->comp_id_target, 'target' => ($model->comp_id_target ? '' : 'skip'), 'ezf_id' => $model->ezf_id], ['data-pjax' => 0, 'class' => 'open-form btn btn-success', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'เริ่มบันทึกข้อมูล')]).
                            ' '.Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['ezforms/ezform/viewonline', 'action'=>'inputdata', 'id'=>$model->ezf_id], ['data-pjax' => 0, 'class' => 'open-form btn btn-primary', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ดูตัวอย่างฟอร์ม')]);
                    }
                },
                'hAlign' => 'center',
                'filterType' => GridView::FILTER_COLOR,
                'vAlign' => 'middle',
                'format' => 'raw',
                'width' => '100px',
                'noWrap' => true
            ],
            [
                'attribute' => 'Level',
                'value' => function ($model, $key, $index, $widget) use ($totalCount) {
                    $html = '';
                    if (Yii::$app->params['input_forder'] == 0) {
                        Yii::$app->params['input_forder'] = 1;
                        $html = '<a class="fa fa-arrow-down" href="' . Url::to(['inputdata/favorite-order', 'userid' => Yii::$app->user->id, 'ezfid' => $model->ezf_id, 'action' => 'down']) . '"></a>';
                    } else {
                        Yii::$app->params['input_forder'] += 1;
                        if (Yii::$app->params['input_forder'] == $totalCount) {
                            $html = '<a class="fa fa-arrow-up" href="' . Url::to(['inputdata/favorite-order', 'userid' => Yii::$app->user->id, 'ezfid' => $model->ezf_id, 'action' => 'up']) . '"></a>';
                        } else {
                            $html = '<a class="fa fa-arrow-up" href="' . Url::to(['inputdata/favorite-order', 'userid' => Yii::$app->user->id, 'ezfid' => $model->ezf_id, 'action' => 'up']) . '"></a>';
                            $html .= '<a class="fa fa-arrow-down" href="' . Url::to(['inputdata/favorite-order', 'userid' => Yii::$app->user->id, 'ezfid' => $model->ezf_id, 'action' => 'down']) . '"></a>';
                        }
                    }
                    $totalCount;
                    return $html;
                },
                'filterType' => GridView::FILTER_COLOR,
                'hAlign' => 'center',
                'vAlign' => 'middle',
                'format' => 'raw',
                'width' => '30px',
                'noWrap' => true
            ],
        ];

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'grid_step1',
            'columns' => $gridColumns,
            'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            'pjax' => true,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-form',], 'enablePushState' => false],
            'bordered' => true,
            'striped' => true,
            'condensed' => true,
            'responsiveWrap' => false,
            'responsive' => true,
            'hover' => True,
            'showPageSummary' => false,
            'showFooter' => false,
            'resizableColumns' => true,
            //'floatHeader' => true,
            //'floatHeaderOptions' => ['scrollingTop' => '0'],
            'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'footerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => '',
                'after' => false,
                'afterOptions' => false,
            ],
            'persistResize' => false,
        ]);
    }
    else{

        $res = Yii::$app->db->createCommand("SELECT count(*) as total FROM " . $modelEzform->ezf_table . " WHERE (rstat <> '3') AND xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
        $html = Html::a('<i class="fa fa-inbox"></i> All ('.($res['total']+0).')', ['step4', 'comp_id_target' => $input_comp_target, 'ezf_id' => $input_ezf_id, 'target' => 'all', 'tabstep3' => '1'], ['class' => 'open-form btn btn-danger btn-xs', 'data-pjax'=>0]);

        $res = Yii::$app->db->createCommand("SELECT SUM(`rstat` = 2) as submitted, SUM(`rstat` = 1) as draft, SUM(`rstat` = 0) as newrecord FROM " . $modelEzform->ezf_table . " WHERE xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
        $html .= ' '.Html::a('<i class="fa fa-check-square"></i> Submitted ('.($res['submitted']+0).')', ['step4', 'comp_id_target' => $input_comp_target, 'ezf_id' => $input_ezf_id, 'target' => 'all', 'rstat' => 'submitted', 'tabstep3' => '1'], ['class' => 'open-form btn btn-success btn-xs', 'data-pjax'=>0]);
        $html .= ' '.Html::a('<i class="fa fa-edit"></i> Save draft ('.($res['draft']+0).')', ['step4', 'comp_id_target' => $input_comp_target, 'ezf_id' => $input_ezf_id, 'target' => 'all', 'rstat' => 'draft', 'tabstep3' => '1'], ['class' => 'open-form btn btn-warning btn-xs', 'data-pjax'=>0]);
        $html .= ' '.Html::a('<i class="fa fa-certificate"></i> New record ('.($res['newrecord']+0).')', ['step4', 'comp_id_target' => $input_comp_target, 'ezf_id' => $input_ezf_id, 'target' => 'all', 'rstat' => 'newrecord', 'tabstep3' => '1'], ['class' => 'open-form btn btn-info btn-xs', 'data-pjax'=>0]);

        $res = Yii::$app->db->createCommand("SELECT count(*) as total FROM " . $modelEzform->ezf_table . " WHERE (rstat <> '3')  AND user_create ='".Yii::$app->user->id."' AND xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
        $html .= ' '.Html::a('<i class="fa fa-user"></i> ข้อมูลฉันบันทึก ('.($res['total']).')', ['step4', 'comp_id_target' => $input_comp_target, 'ezf_id' => $input_ezf_id, 'target' => 'byme', 'tabstep3' => '1'], ['class' => 'open-form btn btn-primary btn-xs', 'data-pjax'=>0, 'style'=> 'margin: 10px 0px 10px 0px;']);

        $res = Yii::$app->db->createCommand("SELECT count(*) as total FROM " . $modelEzform->ezf_table . " WHERE rstat = '1' AND target IS NULL AND xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
        $html .= ' '.Html::a('<i class="fa fa-times"></i> ไม่มีเป้าหมาย ('.($res['total']).')', ['step4', 'comp_id_target' => $input_comp_target, 'ezf_id' => $input_ezf_id, 'target' => 'skip', 'tabstep3' => '1'], ['class' => 'open-form btn btn-default btn-xs', 'data-pjax'=>0]);

        $res = Yii::$app->db->createCommand("SELECT count(*) as total FROM " . $modelEzform->ezf_table . " WHERE rstat = '3' AND xsourcex = :xsourcex ".$xdepartmentx_sql.";", [':xsourcex'=>$xsourcex, ])->queryOne();
        $html .= ' '. Html::a('<i class="fa fa-trash"></i> ถูกลบ ('.($res['total']).')', null, ['class' => 'btn btn-default btn-xs', 'data-pjax'=>0]);

        echo '<div class="bg bg-success" style="padding-left: 10px; padding-bottom: 10px; padding-top: 10px; padding-right: 10px;">
                <a href="/inputdata/?session=yes" class="open-form btn btn-danger btn-md pull-right" data-pjax="0"><i class="fa fa-list-ul"></i> แสดงฟอร์มทั้งหมด</a>
                <h3><i class="fa fa-file-text fa-2x text-info"></i> ท่านกำลังทำงานกับฟอร์ม : <span class="text-bold">'.$modelEzform->ezf_name.'</span></h3>
                 '.$html.'
              </div>';
    }
    ?>

    <div id='input-step2' class="sdbox-header">
        <h3><i class="fa fa-users" aria-hidden="true"></i> ขั้นที่ 2. เลือกเป้าหมายเพื่อกรอกข้อมูล<?php if ($input_target == 'skip') echo ' <code>(ข้ามไม่ระบุ)</code>'; ?></h3>
        <?php if ($input_comp_target) { ?>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <?php if($ezform_comp['special'] <= 0) { /* ?>
                            <input type="radio" <?php if ($input_target == 'skip') echo'checked'; ?> id="overstep2-1" name="overstep2" style="width: 22px; height: 22px;"> ไม่ระบุเป้าหมาย(ข้าม) |
                            <input type="radio" <?php if ($input_target == 'all') echo'checked'; ?> id="overstep2-2" name="overstep2" style="width: 22px; height: 22px;"> เลือกทุกเป้าหมาย(ข้าม)
                        <?php */ } else {
                            if(in_array(Yii::$app->keyStorage->get('frontend.domain'), ['thaipalliative.org', 'thaicarecloud.org', 'yii2-starter-kit.dev'])){
                                echo ' ', Html::button('<span class="fa fa-plus"></span> ลงทะเบียนชาวต่างชาติ', [
                                    'class' => 'btn btn-danger btn-md',
                                    'id' => 'btn-GenForeigner',
                                    'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ลงทะเบียนชาวต่างชาติ'),
                                ]);
                                $this->registerJS("$('#btn-GenForeigner').on('click',function(){
                                                                var url = '" . Url::to(['step2-confirm', 'task' => 'gen-foreigner', 'comp_id' => $ezform_comp['comp_id']]) . "';
                                                                modalQueryRequest(url);
                                                             });");
                            }
                        }?>
                    </h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->

                <div class="box-body" style="display: block;">
                    <?php
                    $concatLabel = \backend\controllers\InputdataController::setFieldSearch($ezform_comp);
                    $select2Option = [
                        'name' => 'select2Target',
                        'id' => 'select2Target',
                        'value'=> $modelSelectTarget['text'] ? $modelSelectTarget['text'] : null,
                        'size' => Select2::MEDIUM,
                        'language' => 'th',
                        'options' => ['placeholder' => 'สามารถค้นได้จากรายการดังนี้ '.$concatLabel['concat_label'], 'multiple' => ($dataComponent['tagMultiple']) ? $dataComponent['tagMultiple'] : FALSE],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'minimumInputLength' => 0,
                            'ajax' => [
                                'url' => Url::to(['lookup-target']),
                                'type' => 'POST',
                                'delay' => 500,
                                'dataType' => 'json',
                                'data' => new JsExpression("function(params) { $(this).text(''); return {q:params.term, search:'" . base64_encode($dataComponent['sqlSearch']) . "', ezf_id : '".$input_ezf_id."', ezf_comp_id : '".$ezform_comp['comp_id']."'}; }"),
                                //'results' => new JsExpression('function(data) { return {results:data}; }'),
                            ],
                        ],
                    ];


                    echo Select2::widget($select2Option);

                    $this->registerJS("$('#overstep2-1').on('click',function(){
                                                        var url = 'step4?comp_id_target={$input_comp_target}&ezf_id={$input_ezf_id}&target=skip';
                                                        $(location).attr('href',url);
                                                     });");
                    $this->registerJS("$('#overstep2-2').on('click',function(){
                                                        var url = 'step4?comp_id_target={$comp_id_target}&ezf_id={$input_ezf_id}&target=all';
                                                        $(location).attr('href',url);
                                                     });");
                    ?>

                    <?php if ($dataComponent['tagMultiple']) { ?>
                        <hr>
                        <div class="pull-right">
                            *เลือกได้หลายคน <button class="btn btn-warning btn-md" role="button">เลือกเป้าหมาย</button>
                        </div>
                        <?php
                    } else {
                        //เลือกได้คนเดียว
                        $this->registerJS("$('#select2Target').on('change',function(){
                                                        var target = $('#select2Target option:selected').val();
                                                        var target_text = $('#select2-select2Target-results').on('.select2-results__option--highlighted').text();
                                                        //เลือกแบบ special
                                                        //เพิ่มกลุ่มเสี่ยงใหม่ใน site ของท่าน (หา site ไหนก็ไม่เจอ)
                                                        if(target == '-990'){
                                                            //runBlockUI('รอสักครู่กำลังเพิ่มข้อมูลใหม่ ...');
                                                            var url = '" . Url::to(['step2-confirm', 'task' => 'add-new', 'comp_id' => $ezform_comp['comp_id']]) . "&target='+encodeURIComponent($.trim(target_text));
                                                            modalQueryRequest(url);
                                                            $('#select2Target option:selected').prop(\"selected\", false);
                                                            $('#select2-select2Target-container').text($('#select2Target option:selected').text());
                                                        //เพิ่มกลุ่มเสี่ยงใหม่ใน site ของท่าน (copy จาก site อื่น)
                                                        }else if(target == '-991'){
                                                            //runBlockUI('รอสักครู่กำลังเพิ่มข้อมูลใหม่ ...');
                                                            var url = '" . Url::to(['step2-confirm', 'task' => 'add-from-site', 'comp_id' => $ezform_comp['comp_id']]) . "&target='+encodeURIComponent($.trim(target_text));
                                                            modalQueryRequest(url);
                                                            $('#select2Target option:selected').prop(\"selected\", false);
                                                            $('#select2-select2Target-container').text($('#select2Target option:selected').text());
                                                        //เป้าหมายปกติ หาไม่เจอเพิ่มข้อมูลใหม่
                                                        }else if(target == '-992'){
                                                            runBlockUI('รอสักครู่กำลังเพิ่มข้อมูลใหม่ ...');
                                                            var url = '" . Url::to(['redirect-page', 'ezf_id' => $input_ezf_id, 'comp_id_target' => $input_comp_target ]) . "';
                                                            $(location).attr('href',url);
                                                        //กรณีเลขบัตรไม่ถูกต้อง
                                                        }else if(target == '-999'){
                                                            alert('ไม่พบข้อมูล! ลองค้นหาจากคำอื่นๆ');
                                                        }else{
                                                        //เลือกข้อมูลแบบปกติ
                                                        runBlockUI('รอสักครู่กำลังเปิดแฟ้มข้อมูล ...');
                                                            $.post('" . Url::to(['gen-target']) . "' , { target: target, ezf_id : '".$input_ezf_id."' })
                                                             .done(function( data ) {
                                                               var chk = ".(Yii::$app->request->get('addnext') ? 'true' : 'null').";
                                                               if(chk){
                                                                    var ezf_id = '".$input_ezf_id."';
                                                                    var comp_id_target = '".$input_comp_target."';
                                                                    var addnext = '".Yii::$app->request->get('addnext')."';
                                                                    var rurl = '".Yii::$app->request->get('rurl')."';
                                                                    $('#addNewRecord').prop(\"disabled\", true);
                                                                    $('#addNextRecord').prop(\"disabled\", true);
                                                                    
                                                                    runBlockUI('รอสักครู่กำลังเปิดฟอร์มใหม่ ...');

                                                                    $.post(\"".Url::to(['insert-record'])."\", {ezf_id: ezf_id, target: $.trim(data), comp_id_target: comp_id_target, addnext : addnext, rurl : rurl}, function(result){
                                                                        $(location).attr('href',result);
                                                                    });
                                                               }else{
                                                                    var url = '" . Url::to(['step4', 'comp_id_target' => $input_comp_target, 'ezf_id' => $input_ezf_id, 'dataset'=>$_GET['dataset']]) . "&target='+$.trim(data);
                                                                    $(location).attr('href',url);
                                                               }
                                                             });
                                                         }
                                                     });");
                    } ?>
                </div><!-- /.box-body -->

            </div>
        <?php } // end step2  ?>
    </div>

    <div class="sdbox-header">
        <h3><i class="fa fa-archive" aria-hidden="true"></i> ขั้นที่ 3. ดูแฟ้มข้อมูลทั้งหมดของเป้าหมาย <?php echo $modelSelectTarget['text'] ? ': '.$modelSelectTarget['text'] : null; ?></h3>
        <?php if ($input_target) { ?>
            <?php
            //\yii\helpers\VarDumper::dump($dataProvidertarget, 10, TRUE);
            $gridColumnstarget = [
                ['class' => 'kartik\grid\SerialColumn'],
                [
                    'attribute' => 'ชื่อเป้าหมาย',
                    'value' => function ($model, $key, $index, $widget) use ($dataComponent) {
                        if ($model->ptid || $model->target) {
                            //$user = common\models\UserProfile::findOne($model->target);
                            //echo $arr_comp_desc_field_name; echo'<hr>'; print_r($arr_comp_desc_field_name); exit;
                            $comp_name = new Dynamic();
                            $comp_name->setTableName($dataComponent['ezf_table_comp']);
                            if ($dataComponent['comp_id'] == 100000 OR $dataComponent['comp_id'] == 100001) {
                                $comp_name = $comp_name::find()->where('user_id = :user_id', [':user_id'=>$model->target])->one();
                            } else if($dataComponent['special']) {
                                $comp_name = $comp_name::find()->where('rstat <>3 AND xsourcex = :xsourcex AND ptid = :ptid', [':xsourcex' => $model->xsourcex, ':ptid'=>$model->ptid])->one();
                            }else{
                                $comp_name = $comp_name::find()->where('rstat <>3 AND xsourcex = :xsourcex AND id = :target', [':xsourcex' => $model->xsourcex, ':target'=>$model->target])->one();
                            }
                            $str = '';
                            foreach ($dataComponent['arr_comp_desc_field_name'] as $val) {
                                $str .= $comp_name->$val . ' ';
                            }
                            return $str;

                            //return $model->target;
                        } else
                            return 'ไม่ระบุเป้าหมาย';
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '150px',
                    'noWrap' => true
                ],
                [
                    'attribute' => 'ลักษณะหลัก',
                    'value' => function ($model, $key, $index, $widget) use ($modelEzform) {
                        if (strlen(trim($modelEzform->field_detail))) {
                            $arr_desc = explode(',', $modelEzform->field_detail);
                            $str_desc = '';
                            foreach ($arr_desc as $val) {
                                $str_desc .= $val . ', ';
                            }
                            $str_desc = substr($str_desc, 0, -2);
                            try {
                                $res = Yii::$app->db->createCommand("SELECT ".$str_desc." FROM `" . ($modelEzform->ezf_table) . "` WHERE id = '".$model->id."';")->queryOne();
                            } catch (\yii\db\Exception $e){
                                try {
                                    $res = Yii::$app->db->createCommand("SELECT ".$str_desc." FROM `" . ($modelEzform->ezf_table) . "` WHERE user_id = '".$model->id."';")->queryOne();
                                } catch (\yii\db\Exception $e){

                                }
                            }
                            $str_desc = '';
                            foreach ($res as  $val) {
                                $str_desc .= $val. ' ';
                            }
                        } else {
                            $str_desc = 'ไม่ได้ระบุ';
                        }
                        return $str_desc;

                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '250px',
                    'noWrap' => true
                ],
                [
                    'attribute' => 'หน่วยงาน',
                    'value' => function ($model, $key, $index, $widget) {
                        $hospital = \backend\modules\ezforms\components\EzformQuery::getHospital($model->xsourcex);
                        $html = '<span class="label label-success" data-toggle="tooltip" data-original-title="'.($hospital['name']. ' ต.'. $hospital['tambon']. ' อ.'. $hospital['amphur']. ' จ.'. $hospital['province']).'">'.$hospital['hcode'].'</span>';
                        return $html;
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '50px',
                    'noWrap' => true
                ],
                [
                    'attribute' => 'update_date',
                    'label' => 'วันที่แก้ไขล่าสุด',
                    'value' => function ($model, $key, $index, $widget) {
                        //return Html::a(Html::encode(""), '#');
                        if ($model->update_date) {
                            $date = new DateTime($model->update_date);
                            return $date->format('d/m/Y (H:i:s)');
                        }

                        $date = new DateTime($model->create_date);
                        return $date->format('d/m/Y (H:i:s)');
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '100px',
                    'noWrap' => true
                ],
                [
                    'attribute' => 'บันทึกโดย',
                    'value' => function ($model, $key, $index, $widget) {
                        if ($model->user_update == "") {
                            $user = common\models\UserProfile::findOne($model->user_create);
                        } else {
                            $user = common\models\UserProfile::findOne($model->user_update);
                        }
                        return '<span class="text-center fa fa-2x fa-user text-warning" data-toggle="tooltip" data-original-title="บันทึกล่าสุดโดย : '.$user->firstname . ' ' . $user->lastname.'"></span>';

                        //return $model->firstname.' '.$model->lastname;
                        //return $model->target;
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'hAlign' => 'center',
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '50px',
                    'noWrap' => true
                ],
                [
                    'attribute' => 'สถานะ',
                    'value' => function ($model, $key, $index, $widget) {
                        if ($model->rstat == 0) {
                            return Html::a('<i class="fa fa-pencil-square-o"></i> New Record', null, ['data-pjax' => 0, 'class' => 'text-default', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'รอการกรอกข้อมูล')]);
                        }
                        else if ($model->rstat == 1) {
                            return Html::a('<i class="fa fa-pencil-square-o"></i> Waiting', null, ['data-pjax' => 0, 'class' => 'text-warning', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ข้อมูลยังไม่ถูกส่งเข้าระบบด้วยการคลิก Submitted')]);
                        } else if ($model->rstat == 2 || $model->rstat >= 4){
                            return Html::a('<i class="fa fa-send"></i> Submitted', null, ['data-pjax' => 0, 'class' => 'text-success', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว')]);
                        }

                        //return $model->firstname.' '.$model->lastname;
                        //return $model->target;
                    },
                    'filterType' => GridView::FILTER_COLOR,
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '50px',
                    'noWrap' => true
                ],
                [
                    'attribute' => 'Action',
                    'value' => function ($model, $key, $index, $widget) use ($xsourcex, $input_comp_target, $input_ezf_id, $input_target) {
                        if($model->xsourcex ==$xsourcex){
                            $html = Html::a('<i class="fa fa-edit"></i> ดู / แก้ไข', 'step4?comp_id_target=' . $input_comp_target . '&ezf_id=' . $input_ezf_id . '&target=' . $input_target . '&dataid=' . $model->id, ['data-pjax' => 0, 'class' => 'open-form btn btn-primary', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'แก้ไขข้อมูลนี้')]);
                        }else{
                            $html = Html::a('<i class="fa fa-view"></i> อ่านอย่างเดียว', 'step4?comp_id_target=' . $input_comp_target . '&ezf_id=' . $input_ezf_id . '&target=' . $input_target . '&dataid=' . $model->id . '&action=read-only' , ['data-pjax' => 0, 'class' => 'open-form btn btn-warning', 'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'แสดงข้อมูลนี้')]);
                        }
                        return $html;
                    },
                    'hAlign' => 'center',
                    'filterType' => GridView::FILTER_COLOR,
                    'vAlign' => 'middle',
                    'format' => 'raw',
                    'width' => '50px',
                    'noWrap' => true
                ],

            ];
            //\yii\helpers\VarDumper::dump(ArrayHelper::map($dataProvider->models, 'ezf_id', 'ezf_name'),10,true);
            $gridTargetFrom = GridView::widget([
                'dataProvider' => $dataProvidertarget,
                'columns' => $gridColumnstarget,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'toolbar' => [
                    ['content' => '<button type="button" class="btn btn-success btn-flat">ฟอร์มอื่นๆ <li class="fa fa-hand-o-right"></li></button>'],
                    ['content' => Html::dropDownList('nextForm', $_GET['ezf_id'], ArrayHelper::map($dataProvider->models, 'ezf_id', 'ezf_name'), [
                        'class'=>'form-control',
                        'onchange'=>'step3ChangeForm($(this).val());'])
                    ],
                    '{export}',
                    '{toggleData}'
                ],
                'pjax'=>true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-gridTargetFrom',], 'enablePushState' => false],
                'bordered' => true,
                'striped' => true,
                'condensed' => true,
                'responsiveWrap' => false,
                'responsive' => true,
                'hover' => True,
                'showPageSummary' => false,
                'showFooter' => false,
                'resizableColumns' => true,
                'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'footerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '',
                    'after' => false,
                    'afterOptions' => false,
                ],
                'persistResize' => false,
            ]);

// show EMR for component
            if($input_comp_target) {
                $gridColumnstargetEMR = [
                    ['class' => 'kartik\grid\SerialColumn'],
                    [
                        'attribute' => 'ezf_id',
                        'label' => 'ชื่อฟอร์ม',
                        'value' => function ($model, $key, $index, $widget) {
                            $ezform = \backend\modules\ezforms\models\Ezform::find()->select('ezf_name')->where('ezf_id = :ezf_id', [':ezf_id' => $model->ezf_id])->one();
                            return $ezform->ezf_name;
                        },
                        'filterType' => GridView::FILTER_COLOR,
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'width' => '150px',
                        'noWrap' => true
                    ],
                    [
                        'attribute' => 'ลักษณะหลัก',
                        'value' => function ($model, $key, $index, $widget) {
                            $field_detail = Yii::$app->db->createCommand("SELECT ezf_table, field_detail FROM ezform WHERE ezf_id='" . $model->ezf_id . "';")->queryOne();
                            if (strlen(trim($field_detail['field_detail']))) {
                                $arr_desc = explode(',', $field_detail['field_detail']);
                                $str_desc = '';
                                foreach ($arr_desc as $val) {
                                    $str_desc .= $val . ', ';
                                }
                                $str_desc = substr($str_desc, 0, -2);
                                try {
                                    $res = Yii::$app->db->createCommand("SELECT " . $str_desc . " FROM " . $field_detail['ezf_table'] . " WHERE id = '" . $model->data_id . "';")->queryOne();
                                } catch (\yii\db\Exception $e) {
                                    try {
                                        $res = Yii::$app->db->createCommand("SELECT " . $str_desc . " FROM " . $field_detail['ezf_table'] . " WHERE user_id = '" . $model->data_id . "';")->queryOne();
                                    } catch (\yii\db\Exception $e) {

                                    }
                                }
                                $str_desc = '';
                                foreach ($res as $val) {
                                    $str_desc .= $val . ' ';
                                }
                            } else {
                                $str_desc = 'ไม่ได้ระบุ';
                            }
                            return $str_desc;

                        },
                        'filterType' => GridView::FILTER_COLOR,
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'width' => '250px',
                        'noWrap' => true
                    ],
                    [
                        'attribute' => 'หน่วยงาน',
                        'value' => function ($model, $key, $index, $widget) {
                            $hospital = \backend\modules\ezforms\components\EzformQuery::getHospital($model->xsourcex);
                            $html = '<span class="label label-success" data-toggle="tooltip" data-original-title="' . ($hospital['name'] . ' ต.' . $hospital['tambon'] . ' อ.' . $hospital['amphur'] . ' จ.' . $hospital['province']) . '">' . $hospital['hcode'] . '</span>';
                            return $html;
                        },
                        'filterType' => GridView::FILTER_COLOR,
                        'hAlign' => 'center',
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'width' => '50px',
                        'noWrap' => true
                    ],
                    [
                        'attribute' => 'update_date',
                        'label' => 'วันที่แก้ไขล่าสุด',
                        'value' => function ($model, $key, $index, $widget) {
                            if ($model->update_date) {
                                $date = new DateTime($model->update_date);
                                return $date->format('d/m/Y (H:i:s)');
                            }

                            $date = new DateTime($model->create_date);
                            return $date->format('d/m/Y (H:i:s)');
                        },
                        'filterType' => GridView::FILTER_COLOR,
                        'hAlign' => 'center',
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'width' => '50px',
                        'noWrap' => true
                    ],
                    [
                        'attribute' => 'บันทึกโดย',
                        'value' => function ($model, $key, $index, $widget) {
                            if ($model->user_update == "") {
                                $user = common\models\UserProfile::findOne($model->user_create);
                            } else {
                                $user = common\models\UserProfile::findOne($model->user_update);
                            }
                            return '<span class="text-center fa fa-2x fa-user text-warning" data-toggle="tooltip" data-original-title="บันทึกล่าสุดโดย : ' . $user->firstname . ' ' . $user->lastname . '"></span>';

                            //return $model->firstname.' '.$model->lastname;
                            //return $model->target;
                        },
                        'filterType' => GridView::FILTER_COLOR,
                        'hAlign' => 'center',
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'width' => '50px',
                        'noWrap' => true
                    ],
                    [
                        'attribute' => 'สถานะ',
                        'value' => function ($model, $key, $index, $widget) {
                            if ($model->rstat == 0) {
                                return Html::a('<i class="fa fa-pencil-square-o"></i> New Record', null, ['data-pjax' => 0, 'class' => 'text-default', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'รอการกรอกข้อมูล')]);
                            } else if ($model->rstat == 1) {
                                return Html::a('<i class="fa fa-pencil-square-o"></i> Waiting', null, ['data-pjax' => 0, 'class' => 'text-warning', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ข้อมูลยังไม่ถูกส่งเข้าระบบด้วยการคลิก Submitted')]);
                            } else if ($model->rstat == 2 || $model->rstat >= 4) {
                                return Html::a('<i class="fa fa-send"></i> Submitted', null, ['data-pjax' => 0, 'class' => 'text-success', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว')]);
                            }

                            //return $model->firstname.' '.$model->lastname;
                            //return $model->target;
                        },
                        'filterType' => GridView::FILTER_COLOR,
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'width' => '80px',
                        'noWrap' => true
                    ],
                    [
                        'attribute' => 'Action',
                        'value' => function ($model, $key, $index, $widget) use ($xsourcex, $input_comp_target, $input_target) {
                            if ($model->xsourcex == $xsourcex) {
                                $html = Html::a('<i class="fa fa-edit"></i> ดู / แก้ไข', 'step4?comp_id_target=' . $input_comp_target . '&ezf_id=' . $model->ezf_id . '&target=' . $input_target . '&dataid=' . $model->data_id, ['data-pjax' => 0, 'class' => 'open-form btn btn-primary', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'แก้ไขข้อมูลนี้')]);
                            } else {
                                $html = Html::a('<i class="fa fa-view"></i> อ่านอย่างเดียว', 'step4?comp_id_target=' . $input_comp_target . '&ezf_id=' . $model->ezf_id . '&target=' . $input_target . '&dataid=' . $model->data_id . '&action=read-only', ['data-pjax' => 0, 'class' => 'open-form btn btn-warning', 'data-toggle' => 'tooltip', 'data-original-title' => Yii::t('kvgrid', 'แสดงข้อมูลนี้')]);
                            }
                            return $html;
                        },
                        'hAlign' => 'center',
                        'filterType' => GridView::FILTER_COLOR,
                        'vAlign' => 'middle',
                        'format' => 'raw',
                        'width' => '50px',
                        'noWrap' => true
                    ],

                ];

                $gridTargetFromEMR = GridView::widget([
                    'dataProvider' => $dataProvidertargetEMR,
                    'columns' => $gridColumnstargetEMR,
                    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                    'toolbar' => [
                        ['content' => '<button type="button" class="btn btn-success btn-flat">ฟอร์มอื่นๆ <li class="fa fa-hand-o-right"></li></button>'],
                        ['content' => Html::dropDownList('nextForm', $_GET['ezf_id'], ArrayHelper::map($dataProvider->models, 'ezf_id', 'ezf_name'), [
                            'class' => 'form-control',
                            'onchange' => 'step3ChangeForm($(this).val());'])
                        ],
                        '{export}',
                        '{toggleData}'
                    ],
                    'pjax' => true,

                    'bordered' => true,
                    'striped' => true,
                    'condensed' => true,
                    'responsiveWrap' => false,
                    'responsive' => true,
                    'hover' => True,
                    'showPageSummary' => false,
                    'showFooter' => false,
                    'resizableColumns' => true,
                    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'footerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '',
                        'after' => false,
                        'afterOptions' => false,
                    ],
                    'persistResize' => false,
                ]);
//EMR grid
            }//end if

                //set default tab
                $tabStep3_1 = false;
                $tabStep3_2 = true;

                //items for grid step3
                $items = [
                    'items' => [
                        [
                            'label' => '<i class="fa fa-list-alt"></i> ข้อมูลของเป้าหมายทั้งหมดจากฟอร์มที่เลือก',
                            'encode' => false,
                            'content' => '<div style="margin-top: 10px;">' . $gridTargetFrom . '</div>',
                            'active' => $tabStep3_1,
                        ],
                    ],
                ];

                if ($input_comp_target && $input_target != 'all' && $input_target != 'byme' && $input_target != 'skip') {
                    $items['items'][1]['label'] = '<i class="fa fa-list-ul"></i> ' . ($ezform_comp['special'] ? 'ข้อมูล EMR ของเป้าหมาย' : 'ข้อมูลจากทุกแฟ้มของเป้าหมาย');
                    $items['items'][1]['encode'] = false;
                    $items['items'][1]['content'] = '<div style="margin-top: 10px;">' . $gridTargetFromEMR . '</div>';
                    $items['items'][1]['active'] = $tabStep3_2;
                }

            echo \yii\bootstrap\Tabs::widget($items);

            ?>

            <?php
            //START sdii editor
            if($dataProvidertargetEMR) {
                $ezform = [];
                $getModelsEMR = $dataProvidertargetEMR->getModels();
                foreach ($getModelsEMR as $ezf_key => $ezf_value) {
                    $keyExists = ArrayHelper::keyExists($ezf_value['ezf_id'], $ezform);
                    if (!$keyExists) {
                        $ezform[$ezf_value['ezf_id']] = $ezf_value['ezf_id'];
                    }
                }
                echo $this->render('@backend/modules/ezforms2/views/ezform-config/_report_show_widget', [
                    'ezform' => $ezform,
                    'target' => base64_decode($input_target),
                ]);
            }

            //END sdii editor
        } ?>



    </div>
    <div  id='input-step4' class="sdbox-header">
        <?php
        if($input_dataid){
            $this->registerJs("$().ready(function(){
                                                        $('html,body').animate({scrollTop: $('#input-step4').offset().top},'slow');
                                                      });");
        }
        else if ($input_target OR $input_target == 'skip' OR $input_ezf_id) {
            $this->registerJs('$().ready(function(){
                                                    $(\'html,body\').animate({scrollTop: $(\'#input-step2\').offset().top},\'slow\');
                                                  });');
        }
        if($modelSelectTarget['text']){
            $this->registerJS('
                                                (function blink() {
                                                    $(\'#blink-target\').fadeOut(300).fadeIn(500, blink);
                                                })();
                                                ');
        }
        ?>

        <?php
        echo '<h3><i class="fa fa-edit" aria-hidden="true"></i> ขั้นที่ 4. ลงมือบันทึกข้อมูลใน EzForm <span class="label label-primary">' . (isset($modelEzform->ezf_name) ? $modelEzform->ezf_name : '') . '</span></h3>';
        if(isset($modelEzform->ezf_name)) {
            //\yii\helpers\VarDumper::dump($modelSelectTarget['text'],10,true); exit;
            if($modelSelectTarget['chk'] != 1 && $modelSelectTarget['text'] !='' && $input_target != 'all' && $input_target != 'skip' && $input_target != 'byme'){
                echo '<div class="alert alert-warning"><span class="h2"><li class="fa fa-exclamation-triangle"></li> ข้อมูลนี้เป็นข้อมูลของหน่วยบริการอื่น ท่านจึงไม่สามารถเพิ่มข้อมูลได้ (อ่านอย่างเดียว)</span></div>';
            }
            else if($modelEzform->unique_record == 9){
                echo '<div class="alert alert-info"><span class="h2"><li class="fa fa-exclamation-triangle"></li> '. $modelEzform->ezf_name.' สามารถเพิ่มข้อมูลได้เป้าหมายละ 1 ครั้งเท่านั้น</span></div>';
            }
            else if(($ezform_comp['special']  && $input_target != 'all' && $input_target != 'skip' && $input_target != 'byme' && $ezform_comp['ezf_id'] <> $input_ezf_id && $modelSelectTarget['text'] !='') || ($ezform_comp['special']+0) ==0){
                echo '<hr>'.Html::button('<i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูลใหม่ใน ' . $modelEzform->ezf_name, ['data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'เพิ่มข้อมูลใหม่'), 'class' => 'btn btn-lg btn-success', 'id' => 'addNewRecord']);
                if($input_dataid)
                    echo ' ',Html::a('<i class="fa fa-arrow-circle-o-right"></i> เพิ่มข้อมูลใหม่สำหรับคนถัดไป ',  [($modelEzform->comp_id_target ? 'step2' : 'step4'), 'comp_id_target' => $modelEzform->comp_id_target, 'target' => ($modelEzform->comp_id_target ? '' : 'skip'), 'ezf_id' => $modelEzform->ezf_id, 'addnext' => $input_dataid], ['data-toggle' =>'tooltip', 'data-original-title' => Yii::t('kvgrid', 'เพิ่มข้อมูลใหม่สำหรับคนถัดไป'), 'class' => 'btn btn-lg btn-default pull-right', 'id' => 'addNextRecord']);
                echo '<hr>';
            }
        } ?>
        <?php if ($input_target) { ?>
            <div class="alert alert-danger">
                <span class="h2"><b>เป้าหมายที่เลือกคือ <span id="blink-target" onclick='$(this).removeAttr("id");'><?php echo $modelSelectTarget['text']; ?></span></b></span>
                <?php echo Html::a('<span class="fa fa-print fa-2x"></span>', ['/inputdata/printform', 'dataid' =>$input_dataid, 'id' => $input_ezf_id, 'print' => 1], ['target'=>'_blank', 'class' =>'pull-right']); ?>
            </div>
        <?php } ?>

        <?php if ($input_target AND ($input_dataid OR Yii::$app->request->get('ezfinput')=='new') AND $datamodel_table->id != '') { ?>
        <div id="ezformview" class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <?php if ($input_dataid)
                        echo '<i class="fa fa-pencil-square-o"></i> แก้ไขข้อมูลเดิม';
                    else
                        echo '<i class="fa fa-pencil-square-o"></i> บันทึกข้อมูลใหม่';
                    ?></h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body" style="display: block;">
                <?php
                if ($input_comp_target && 0) {
                    ?>
                    <div id="display-form" class='row' style="display: <?php echo $ezform_comp['special'] >0 ? 'none' : NULL; ?>">
                        <div class="col-md-12">
                            <h4>เป้าหมายที่เลือกคือ : </h4>
                            <?php
                            $select2Option['id']= 'select2Target2';
                            $select2Option['name']= 'select2Target2';
                            echo Select2::widget($select2Option);
                            $this->registerJS('
                                                                        $(\'#select2Target2\').on(\'change\',function(){
                                                                          var select_target = $(\'#select2Target2 option:selected\').val();
                                                                          $(\'#txt_target\').val(select_target);
                                                                        });
                                                                        ');
                            ?>
                            <hr>
                        </div>
                    </div>
                <?php } ?>

                <div class='row'>
                    <div class='col-lg-12'>
                        <h2><?= $modelEzform->ezf_name; ?></h2>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-12'>
                        <h4><?= $modelEzform->ezf_detail; ?></h4>
                    </div>
                </div>

                <hr>
                <?php if($_SESSION['save_status']) echo \yii\bootstrap\Alert::widget(Yii::$app->session->getFlash('save_status')); ?>

                <?php if ($datamodel_table->error && $datamodel_table->rstat != 0) { ?>
                    <div class="alert alert-danger text-left">
                        <span class="h2">Data Validation</span><hr><h3>กรุณาระบุข้อต่อไปนี้ (ถ้ากรอกครบแล้ว ให้กดปุ่ม [Save draft] อีกครั้งเพื่อตรวจสอบเงื่อนไขใหม่) :<br><b><?php echo $datamodel_table->error; ?></b></h3>
                    </div>
                    <hr>
                <?php }else if($datamodel_table->rstat == 3){ ?>
                    <div class="alert alert-warning text-left">
                        <span class="h2">ข้อความแจ้งเตือน</span><hr><h3>ข้อมูลในหน้านี้ถูกลบแล้ว ดังนั้นจะไม่สามารถแก้ไขข้อมูลนี้ได้ (อ่านอย่างเดียว)</h3>
                    </div>
                    <hr>
                <?php } ?>
                <?php if ($sql_announce) { ?>
                    <div class="alert alert-warning text-left">
                        <span class="h2">Data Announce</span><hr><h3>พบการตรวจสอบเงื่อนไขดังนี้ :<br><b><?php echo $sql_announce; ?></b></h3>
                    </div>
                    <hr>
                <?php } ?>

                <?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action' => '/ezforms/ezform/savedata2','options'=>['name'=> 'frmSaveData', 'enctype'=>'multipart/form-data']]); ?>
                <input type='hidden' name='ezf_id' id='ezf_id' value='<?php echo $input_ezf_id; ?>'>
                <?php if ($input_dataid) { ?>
                    <input type='hidden' name='id' id='id' value='<?php echo $input_dataid; ?>'>
                <?php } ?>
                <input type='hidden' name='url_redirect' id='url_redirect' value='<?php echo $_GET['rurl'] ? $_GET['rurl'] : base64_encode(Yii::$app->request->url); ?>'>
                <?php if ($input_target) { ?>
                    <input type='hidden' id ='txt_target' name='target' value='<?php echo $input_target; ?>'>
                    <input type='hidden' id ='comp_id_target' name='comp_id_target' value='<?php echo $input_comp_target; ?>'>
                <?php } ?>
                <div class="row dad" id="formPanel">
                    <?php
                    //                                                                $textbox = "<input type='text'>";
                    //                                                                echo $form->field($model_gen, 'var1')->radioList(array('1'=>'One '.$textbox,2=>'Two'));

                    if (isset($modelfield)) {
                    foreach ($modelfield as $field) {

                        //reference field
                        $options_input = [];
                        if($field->ezf_field_type == 18) {
                            //ถ้ามีการกำหนดเป้าหมาย

                            if ($field->ezf_field_val == 1) {
                                //แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)
                                $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                                //set form disable
                                $options_input['readonly'] = true;
                                $form->attributes['rstat'] = 0;
                            } else if ($field->ezf_field_val == 2) {
                                //ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง
                                $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_id);
                                $form->attributes['rstat'] = $datamodel_table->rstat;
                            } else if ($field->ezf_field_val == 3) {
                                //แก้ไขเฉพาะตารางปลายทาง
                                $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                                //\yii\helpers\VarDumper::dump($ezfField, 10, true);
                                //echo $field->ezf_field_ref_field,'-';
                                $options_input['readonly'] = true;
                                $form->attributes['rstat'] = 0;
                            }
                            $ezfTable = $modelDynamic->ezf_table;

                            $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_ref_field])->one();
                            if(!$ezfField->ezf_field_name){
                                $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->one();
                                $html = '<h1>Error</h1><hr>';
                                $html .= '<h3>เนื่องการเชื่อมโยงของประเภทคำถาม Reference field ผิดพลาด การแก้ไขคือ ลบคำถามออกแล้วสร้างใหม่</h3>';
                                $html .= '<h4>คำถามที่ผิดพลาดคือ : </h4>'. $ezfField->ezf_field_name.' ('. $ezfField->ezf_field_label.')';
                                echo $html;
                                Yii::$app->end();
                            }
                            $modelDynamic = new backend\modules\ezforms\models\EzformDynamic($ezfTable);

                            //กำหนดตัวแปร
                            $field_name = $field->ezf_field_name; //field name ต้นทาง
                            $field_name_ref = $ezfField->ezf_field_name; //field name ปลายทาง

                            //ดูตาราง Ezform ฟิลด์ target ที่มีเป้าหมายเดียวกัน
                            if ($input_target == 'byme' || $input_target == 'skip' || $input_target == 'all') {
                                $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($input_ezf_id, $input_dataid);
                                $target = $target['ptid'];
                            } else {
                                $target = base64_decode($input_target);
                            }
                            //หา target ใน site ตัวเองก่อน (ก็ไม่แนะนำ)
                            $res = $modelDynamic->find()->select($field_name_ref)->where('ptid = :target AND ' . $field_name_ref . ' <> "" AND xsourcex = :xsourcex AND rstat <>3', [':target' => $target, ':xsourcex' => $datamodel_table->xsourcex])->orderBy('create_date DESC');

                            //echo $target.' -';
                            //หาที่ target ก่อน
                            if ($res->count()) {
                                $model_table = $res->One();
                                if ($field->ezf_field_val == 1) {
                                    //echo 'target 1-1<br>';
                                    //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                    $model_gen->$field_name = $model_table->$field_name_ref;
                                    $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                                } else if ($field->ezf_field_val == 2) {

                                } else if ($field->ezf_field_val == 3) {
                                    if ($input_dataid) {
                                        //echo 'target 1<br>';
                                        $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                                        //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                                        if ($res[$field_name_ref]) {
                                            $model_table->$field_name_ref = $res[$field_name_ref];
                                        }
                                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                        $model_gen->$field_name = $model_table->$field_name_ref;
                                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                                    } else {
                                        //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                                        // echo 'target else 1<br>';
                                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                        $model_gen->$field_name = $model_table->$field_name_ref;
                                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                                    }
                                }

                            } else {
                                //กรณีหาไม่เจอ ให้ดู primary key
                                $res = $modelDynamic->find()->where('id = :id', [':id' => $target]);
                                if ($res->count()) {
                                    $model_table = $res->One();

                                    if ($field->ezf_field_val == 1) {
                                        //echo 'target 2-1<br>';
                                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                        $model_gen->$field_name = $model_table->$field_name_ref;
                                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                                    } else if ($field->ezf_field_val == 2) {

                                    } else if ($field->ezf_field_val == 3) {
                                        if ($input_dataid) {
                                            //echo 'target 2<br>';
                                            $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                                            if ($res[$field_name_ref]) {
                                                $model_table->$field_name_ref = $res[$field_name_ref];
                                            }
                                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                            $model_gen->$field_name = $model_table->$field_name_ref;
                                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                                        } else {
                                            //echo 'target else 2<br>';
                                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);

                                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                            $model_gen->$field_name = $model_table->$field_name_ref;
                                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                                        }
                                    }
                                }
                            }

                            // yii\helpers\VarDumper::dump($model_table, 10, true);
                            //Yii::$app->end();
                        }else if($modelEzform->query_tools == 2 && Yii::$app->keyStorage->get('frontend.domain') == "cascap.in.th" && ($input_ezf_id == "1437377239070461301" || $input_ezf_id == "1437377239070461302")){
                            //not doing every thing
                        }else if($modelEzform->query_tools == 2){
                            $form->attributes['rstat'] = $datamodel_table->rstat;
                        }
                        //end reference field

                        echo backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $field, $form, $options_input);
                    }

                    foreach($modelfield as $value){
                        $inputId = Html::getInputId($model_gen, $value['ezf_field_name']);
                        $inputValue = Html::getAttributeValue($model_gen, $value['ezf_field_name']);

                        $dataCond = backend\modules\ezforms\components\EzformQuery::getCondition($value['ezf_id'], $value['ezf_field_name']);
                        if($dataCond){
                            //Edit Html
                            $fieldId = Html::getInputId($model_gen, $value['ezf_field_name']);
                            if($value['ezf_field_type']==4){
                                $fieldId = $value['ezf_field_name'];
                            }

                            $enable = TRUE;
                            foreach ($dataCond as $index => $cvalue) {
                                //if($inputValue == $cvalue['ezf_field_value'] || $inputValue == ''){
                                $dataCond[$index]['cond_jump'] = json_decode($cvalue['cond_jump']);
                                $dataCond[$index]['cond_require'] = json_decode($cvalue['cond_require']);


                                if($value['ezf_field_type']=='4' || $value['ezf_field_type']=='6'){
                                    if($inputValue == $cvalue['ezf_field_value'] || $inputValue == ''){
                                        if ($enable){
                                            $enable = false;
                                            $jumpArr = json_decode($cvalue['cond_jump']);
                                            if(is_array($jumpArr)) {
                                                foreach ($jumpArr as $j => $jvalue) {
                                                    $this->registerJs("
										    var fieldIdj = '" . $jvalue . "';
										    var inputIdj = '" . $fieldId . "';
										    var valueIdj = '" . $inputValue . "';
										    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
										    var fTypej = '" . $value['ezf_field_type'] . "';
										    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'none');
									    ");
                                                }
                                            }

                                            $requireArr = json_decode($cvalue['cond_require']);
                                            if(is_array($requireArr)) {
                                                foreach ($requireArr as $r => $rvalue) {
                                                    $this->registerJs("
										    var fieldIdr = '" . $rvalue . "';
										    var inputIdr = '" . $fieldId . "';
										    var valueIdr = '" . $inputValue . "';
										    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
										    var fTyper = '" . $value['ezf_field_type'] . "';
										    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'block');
									    ");
                                                }
                                            }
                                        }
                                    }
                                } else {

                                    $jumpArr = json_decode($cvalue['cond_jump']);
                                    if(is_array($jumpArr)) {
                                        foreach ($jumpArr as $j => $jvalue) {
                                            $this->registerJs("
									    var fieldIdj = '" . $jvalue . "';
									    var inputIdj = '" . $fieldId . "';
									    var valueIdj = '" . $inputValue . "';
									    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
									    var fTypej = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'block');
								    ");
                                        }
                                    }

                                    $requireArr = json_decode($cvalue['cond_require']);
                                    if(is_array($requireArr)) {

                                        foreach ($requireArr as $r => $rvalue) {

                                            $this->registerJs("
									    var fieldIdr = '" . $rvalue . "';
									    var inputIdr = '" . $fieldId . "';
									    var valueIdr = '" . $inputValue . "';
									    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
									    var fTyper = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'none');
									    
								    ");
                                        }
                                    }
                                }




//						    } else {
//							$dataCond[$index]['cond_jump'] = json_decode($cvalue['cond_jump']);
//							$dataCond[$index]['cond_require'] = json_decode($cvalue['cond_require']);
//						    }
                            }
                            //Add Event
                            if($value['ezf_field_type']==20 || $value['ezf_field_type']==0 || $value['ezf_field_type']==16){
                                $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputId = '".$inputId."';
		    eventCheckBox(inputId, dataCond);
		    setCheckBox(inputId, dataCond);
		");
                            }else if($value['ezf_field_type']==6){
                                $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputId = '".$inputId."';
		    eventSelect(inputId, dataCond);
		    setSelect(inputId, dataCond);
		");
                            } else if($value['ezf_field_type']==4){
                                $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputName = '".$value['ezf_field_name']."';
		    eventRadio(inputName, dataCond);
		    setRadio(inputName, dataCond);
		");
                            }

                        }
                    }
                    backend\assets\EzfGenAsset::register($this);
                    backend\assets\AutoSaveAsset::register($this);
                    ?>

                </div>
                <?php if (isset($modelfield)) { ?>
                <div class="row" id="ezform-btn-saveform">
                    <div class='col-lg-12 text-right'>
                        <div id="div-savedraft"></div>
                        <hr>
                        <?php
                        //for doctor
                        if (Yii::$app->user->can('doctorcascap')) {
                            echo 'ท่านได้รับสิทธิ์ผู้เชี่ยวชาญ (Role) Doctor of CASCAP สามารถเปลี่ยนแปลงข้อมูลนี้โดยกดที่ปุ่ม <code><span class="fa fa-send"></span> Save</code> ซึ่งไม่กระทบสถานะข้อมูลอื่นๆ (rstat)<br>';
                            //$datamodel_table->rstat ==9 //เพื่อให้แก้ไขค่าได้
                            echo '&nbsp;&nbsp;' . Html::button('<span class="fa fa-send"></span> Save',  [
                                    'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'บันทึกผล'),
                                    'class' => 'btn btn-success btn-lg',
                                    'id' => 'final-save',
                                    'type' => 'submit',
                                ]);
                            if($datamodel_table->xsourcex == $xsourcex)
                                echo '<hr><br>ปุ่มอื่นๆสำหรับการบันทึกข้อมูล<hr>';
                        }


                        if($datamodel_table->xsourcex == $xsourcex) {

                            if($_SESSION['input_rurl']){
                                echo '&nbsp;&nbsp;', Html::a('<span class="fa fa-reply-all"></span> Backward', base64_decode($_SESSION['input_rurl']), [
                                    'class' => 'btn btn-success btn-lg',
                                    'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ย้อนกลับรายการก่อนหน้านี้'),
                                ]);
                            }

                            if (($datamodel_table->rstat == 0 || $datamodel_table->rstat == 1 || ($modelEzform->query_tools == 1 || $modelEzform->query_tools == 0) || Yii::$app->user->can('doctorcascap'))) {
                                if ($modelEzform->query_tools == 2) {
                                    echo '&nbsp;&nbsp;', Html::button('<span class="fa fa-pencil-square-o"></span> Save draft', [
                                            'id' => 'save-draft',
                                            'class' => 'btn btn-info btn-lg',
                                            'type' => 'button',
                                            'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'บันทึกข้อมูลเพื่อแก้ไขในภายหลัง'),
                                            'style'=> 'margin: 10px 0px 10px 0px;'
                                        ]).'';
                                }
                                if ($input_dataid) {
                                    if ($modelEzform->query_tools == 3 || ($datamodel_table->error == '' && $modelEzform->query_tools == 2) && ($datamodel_table->rstat == 0 || $datamodel_table->rstat == 1)) {
                                        echo '&nbsp;&nbsp;', Html::button('<span class="fa fa-send"></span> Submit', [
                                            'id' => 'save-submit',
                                            'class' => 'btn btn-primary btn-lg',
                                            'type' => 'button',
                                            'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ส่งข้อมูลนี้เข้าระบบฐานข้อมูล'),
                                            'style'=> 'margin: 10px 0px 10px 0px;',
                                            //'data-confirm' => '',
                                        ]);
                                    } else if ($modelEzform->query_tools == 1 || $modelEzform->query_tools == 0) {
                                        echo '&nbsp;&nbsp;', Html::button('<span class="fa fa-send"></span> Submit', [
                                            'id' => 'save-submit2',
                                            'class' => 'btn btn-primary btn-lg',
                                            'type' => 'button',
                                            'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ส่งข้อมูลนี้เข้าระบบฐานข้อมูล'),
                                            'style'=> 'margin: 10px 0px 10px 0px;',
                                        ]);
                                    }

                                    if ($ezform_comp['ezf_id'] <> $input_ezf_id) {
                                        echo '&nbsp;&nbsp;', Html::a('<span class="fa fa-times"></span> Clear', ['insert-record', 'ezf_id' => $input_ezf_id, 'target' => $input_target, 'comp_id_target' => $input_comp_target, 'dataid' => $input_dataid], [
                                            'class' => 'btn btn-warning btn-lg',
                                            'type' => 'button',
                                            'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ล้างข้อมูลในฟอร์มนี้'),
                                            'data-confirm' => '<div class = "alert alert-danger h4">คุณต้องการล้าง(รีเช็ต) Record นี้?</div>',
                                        ]);
                                    }

                                    //การลบข้อมูลกรณี ฟอร์มลงทะเบียน
                                    if($ezform_comp['ezf_id'] == $input_ezf_id) {
                                        echo '&nbsp;&nbsp;' . Html::button('<span class="fa fa-trash-o"></span> Delete', [
                                                'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ลบ Record นี้ทิ้งไป'),
                                                'class' => 'btn btn-danger btn-lg',
                                                'data-url'=>Url::to(['inputdata/remove-data', 'ezf_id' => $input_ezf_id, 'data_id' => $input_dataid, 'target' => $input_target]),
                                                'id' =>'btn-remove-data',
                                            ]);
                                        //กรณีฟอร์มทั่วไป
                                    }else{
                                        echo '&nbsp;&nbsp;' . Html::a('<span class="fa fa-trash-o"></span> Delete', ['step4', 'ezf_id' => $input_ezf_id, 'target' => $input_target, 'del_dataid' => $input_dataid], [
                                                'class' => 'btn btn-danger btn-lg',
                                                'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ลบ Record นี้ทิ้งไป'),
                                                'data-confirm' => '<div class = "alert alert-danger h4">คุณต้องการลบ Record นี้?</div>',
                                            ]);
                                    }

                                }

                            }
                        }

                        //re save draft
                        if (($datamodel_table->rstat == 2 || $datamodel_table->rstat == 4) && $modelEzform->query_tools == 2) {
                            //$queryManager = \backend\models\QueryManager::find()->where('user_id = :user_id AND user_group = :user_group', [':user_id' => (Yii::$app->user->id), ':user_group' => 1])->one();
                            if (Yii::$app->user->can('administrator')||(Yii::$app->user->can('adminsite')&&((strtotime("now")-strtotime($datamodel_table->update_date))/60/60/24) <=30)) {
                                echo '&nbsp;&nbsp;' . Html::a('<span class="fa fa-repeat"></span> Re-Save Draft', ['resave-draft', 'ezf_id' => $input_ezf_id, 're_dataid' => $input_dataid], [
                                        'class' => 'btn btn-warning btn-lg',
                                        'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'Re-Save Draft ที่ Record นี้'),
                                        'data-confirm' => "<div class = 'alert alert-danger h4'>คุณต้องการ Re-Save Draft ที่ Record นี้?</div>",
                                    ]);
                            }
                        }
                        /*
                        else if($modelEzform->query_tools == 2){

                            echo ' ', Html::button('<span class="fa fa-send"></span> Query', [
                                'class' => 'btn btn-success btn-lg',
                                'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ส่ง Query'),
                                'id' => 'ezform-btn-query',
                                'data-url'=>Url::to(['query-request/create', 'ezf_id' => $input_ezf_id, 'data_id' => $input_dataid]),
                            ]);
                            echo ' ', Html::button('<span class="fa fa-send"></span> Change', [
                                'class' => 'btn btn-primary btn-lg',
                                'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ส่งคำขอแก้ไขข้อมูล'),
                                'id' => 'ezform-btn-change',
                                'data-url'=>Url::to(['query-request/create', 'ezf_id' => $input_ezf_id, 'data_id' => $input_dataid]),
                            ]);


                        }*/
                        ?>

                    </div>
                </div>
                <?php if($modelEzform->query_tools==2) echo \backend\controllers\InputdataController::queryLog($input_ezf_id, $input_dataid); ?>
            </div><!-- /.box-body -->
            <?php
            } ?>
            <?php backend\modules\ezforms\components\EzActiveForm::end(); ?>
        </div><!-- /.end step4-body -->
    </div>
<?php } ?>
    <?php if($modelEzform->reply_tools==2) { ?>
    <div id="ezformview" class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">แสดงความคิดเห็น</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body" style="display: block;">

            <div class="media">
                <div class="media-left"><br>
                    <img class="media-object" src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" data-holder-rendered="true" style="width: 64px; height: 64px;">
                    <?php echo Yii::$app->user->identity->userProfile->getFullName();?>
                </div>
                <div class="media-body">
                    <?php \yii\widgets\Pjax::begin(['enablePushState' => false]);?>
                    <?php
                    $modelEzformReply = new \backend\modules\ezforms\models\EzformReply();
                    $form = ActiveForm::begin(['id'=>$modelEzformReply->formName(), 'action' => ['/inputdata/ezf-reply'], 'options' => ['enctype' => 'multipart/form-data']]); ?>
                    <?= $form->field($modelEzformReply, 'ezf_comment')->textarea(['rows' => 5, ]) ?>
                    <?= $form->field($modelEzformReply, 'file_upload[]')->widget(\kartik\widgets\FileInput::classname(),[
                        'pluginOptions' => [
                            'allowedFileExtensions'=>['pdf','png','jpg','jpeg'],
                            'showRemove' => false,
                            'showUpload' => false,
                        ],
                        'options' => ['multiple' => true]
                    ])->label('File Upload'); ?>
                    <?= $form->field($modelEzformReply, 'ezf_id')->hiddenInput(['value'=>$input_ezf_id])->label(false) ?>
                    <?= $form->field($modelEzformReply, 'data_id')->hiddenInput(['value'=>$input_dataid])->label(false) ?>
                    <?php
                    if(Yii::$app->keyStorage->get('frontend.domain') == "cascap.in.th" && $input_ezf_id == "1437619524091524800") {
                        $items = [
                            '1' => 'แสดงความคิดเห็น (note)',
                            '2' => 'ขอรับคำปรึกษากับผู้เชี่ยวชาญ (อจ. หมอ)',
                            '3' => 'ขอคำปรึกษาด่วน (SOS) กับผู้เชี่ยวชาญ (อจ. หมอ)',
                        ];
                    }else{
                        $items = [
                            '1' => 'แสดงความคิดเห็น (note)',
                            '2' => 'ขอรับคำปรึกษา',
                            '3' => 'ขอคำปรึกษาด่วน (SOS)',
                        ];
                    }?>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-9">
                            <?php
                            echo $form->field($modelEzformReply, 'type')->dropDownList($items, ['id' => 'ezf-reply-type', 'style'=>''])->label('ประเภทคำถาม');
                            ?>

                            <?= Html::submitButton('Comment', ['id' => 'btn-query', 'class' => 'btn btn-primary btn-lg open-form']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php \yii\widgets\Pjax::end();?>
                </div>
            </div>

            <?php echo \backend\controllers\InputdataController::renderEzfReplyList($input_ezf_id, $input_dataid); ?>
        </div>
    </div>

</div><!-- /.end input -->
<?php } ?>
<?php
}
?>

<?php
echo SDModalForm::widget([
    'id' => 'modal-ezform',
    'size' => 'modal-lg',
]);
?>

<?php
if($modelEzform->query_tools == 2 && $datamodel_table->rstat <> 9 && ($datamodel_table->rstat ==2 || $datamodel_table->rstat >=4)){
    $this->registerJs('$(function() { $("form[name=\'frmSaveData\']").find(\'input,select,textarea\').prop(\'disabled\', true); });');
}else{
    $this->registerJs("
                                            $(\"form[name='frmSaveData']\").find('input[type=\"text\"], textarea').focusout(function(e) {
                                                //console.log(e.currentTarget.value);
                                                //console.log(e.currentTarget.name);
                                                autoSaveForm(e.currentTarget.value, e.currentTarget.name);
                                            });
                                            
                                            $(\"form[name='frmSaveData']\").find('input[type=\"radio\"], select').change(function(e) {
                                                //console.log(e.currentTarget.value);
                                                //console.log(e.currentTarget.name);
                                                autoSaveForm(e.currentTarget.value, e.currentTarget.name);
                                            });
                                            $(\"form[name='frmSaveData']\").find('input[type=\"checkbox\"]').click(function(e) {
                                                //console.log(e.currentTarget.value);
                                                //console.log(e.currentTarget.name);
                                                var val = $(this).prop('checked') ? 1 : 0;
                                                autoSaveForm(val, e.currentTarget.name);
                                            });
                                            function autoSaveForm(value, ezf_name){
                                                var ezf_id = $('#ezf_id').val();
                                                var data_id = $('#id').val();
                                                $.post(\"" . Url::to(['auto-save-form']) . "\", {ezf_id: ezf_id, data_id:data_id, value: value, ezf_name: ezf_name}, function(result){
                                                    " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . "
                                                });
                                            }
                                            ");
}
if($modelEzform->js) $this->registerJs($modelEzform->js);
$this->registerJs("
                                        function runBlockUI(text){
                                       $.blockUI({
                                                    message : '<span style=\"font-size : 25px; font-color:#ffffff;\">'+text+'</span>',
                                                    css: {
                                                        border: 'none',
                                                        padding: '15px',
                                                        backgroundColor: '#000',
                                                        '-webkit-border-radius': '10px',
                                                        '-moz-border-radius': '10px',
                                                        opacity: 1,
                                                        color: '#fff'
                                                    }});
                                       }
                                         function step3ChangeForm(val){
                                            runBlockUI('รอสักครู่กำลังเปิดฟอร์มใหม่ ...');
                                            var url = \"" .Yii::$app->request->url . "&ezf_id=\"+$.trim(val); $(location).attr(\"href\",url);
                                       }
                                      
                                        ", \yii\web\View::POS_HEAD);

$this->registerJs("
               $('.file-preview-image').click(function(){
                    var img = $(this).attr('src');
		    var filename = $(this).attr('data-filename');
                    modalEzform('" . Url::to(['inputdata/viewimg', 'img' => '']) . "'+filename);
               });
               
                $( \".open-form\" ).on( \"click\", function() {
                    runBlockUI('รอสักครู่กำลังเรียกข้อมูล ...');
                });
                $( \"#save-draft\" ).on( \"click\", function() {
                    runBlockUI('รอสักครู่กำลังบันทึกข้อมูล ...');
                    $('form[name=frmSaveData]').submit();
                });
                //submit for lock
                $( \"#save-submit\" ).on( \"click\", function() {
                    bootbox.confirm({
                        title: \"ยืนยันข้อตกลง ?\",
                        message: \"<div class = 'alert alert-danger h3'>เมื่อกดที่ปุ่ม Submit แล้ว ท่านจะไม่สามารถแก้ไขข้อมูลได้ด้วยตนเอง หากจำเป็นต้องการแก้ไข ต้องขอเสนอแก้ไขข้อมูลต่อไป?</div>\",
                        buttons: {
                            cancel: {
                                label: '<i class=\"fa fa-times\"></i> Cancel'
                            },
                            confirm: {
                                label: '<i class=\"fa fa-check\"></i> Confirm'
                            }
                        },
                        callback: function (result) {
                            console.log('This was logged in the callback: ' + result);
                            
                            if(result){
                                runBlockUI('รอสักครู่กำลังบันทึกข้อมูล ...');
                                $('#div-savedraft').html('".Html::textInput('txtSubmit', '1', ['id' => 'txtSubmit', 'type' => 'hidden'])."');
                                $('form[name=frmSaveData]').submit();
                            }
                        }
                    });
                });
                //submit no lock
                 $( \"#save-submit2\" ).on( \"click\", function() {
                    runBlockUI('รอสักครู่กำลังบันทึกข้อมูล ...');
                    $('#div-savedraft').html('".Html::textInput('txtSubmit', '1', ['id' => 'txtSubmit', 'type' => 'hidden'])."');
                    $('form[name=frmSaveData]').submit();
                });
                $( \"#final-save\" ).on( \"click\", function() {
                    runBlockUI('รอสักครู่กำลังบันทึกข้อมูล ...');
                    $('#div-savedraft').html('".Html::textInput('finalSave', '1', ['id' => 'finalSave', 'type' => 'hidden'])."');
                    $('form[name=frmSaveData]').submit();
                });

                $( \"#addNewRecord\" ).on( \"click\", function() {
                     var ezf_id = '".$input_ezf_id."';
                    var target = '".$input_target."';
                    var rurl = '".Yii::$app->request->get('rurl')."';
                    var comp_id_target = '".$input_comp_target."';
                    var dataset = '".$_GET['dataset']."';
                    $(this).prop(\"disabled\", true);
                    runBlockUI('รอสักครู่กำลังเพิ่มฟอร์มใหม่ ...');

                    $.post(\"".Url::to(['insert-record'])."\", {ezf_id: ezf_id, target: target, comp_id_target: comp_id_target, rurl : rurl, dataset : dataset}, function(result){
                        $(location).attr('href',result);
                    });
                });
               function modalEzform(url) {
                    $('#modal-ezform .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
                    $('#modal-ezform').modal('show')
                    .find('.modal-content')
                    .load(url);
                }

                $('.inputdata-index').on('click', '#watch-video', function(){
                        modalQueryRequest($(this).attr('data-url'));
                });
                $('#formPanel').on('click', '.ezform-btn-change', function(){
                        modalQueryRequest($(this).attr('data-url'));
                });
                $('#ezform-btn-saveform').on('click', '#btn-remove-data', function(){
                        modalQueryRequest($(this).attr('data-url'));
                });
                function modalQueryRequest(url) {
                    $('#modal-query-request .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
                    $('#modal-query-request').modal('show')
                    .find('.modal-content')
                    .load(url);
                }

              ");

?>
