<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 5/22/2016 AD
 * Time: 23:25
 */
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
//

//set values
$xsourcex = Yii::$app->user->identity->userProfile->sitecode;


$gridColumnstarget = [
    ['class' => 'kartik\grid\SerialColumn'],
    [
        'attribute' => 'ชื่อฟอร์ม',
        'value' => function ($model, $key, $index, $widget) {
            $ezform = \backend\modules\ezforms\models\Ezform::find()->select('ezf_name')->where('ezf_id = :ezf_id', [':ezf_id' => $model->ezf_id])->one();
            return $ezform->ezf_name;
        },
        'filterType' => GridView::FILTER_COLOR,
        'vAlign' => 'middle',
        'format' => 'raw',
        'width' => '150px',
        'noWrap' => true
    ],
    [
        'attribute' => 'รายละเอียด',
        'value' => function ($model, $key, $index, $widget) {
            $field_detail = Yii::$app->db->createCommand("SELECT ezf_table, field_detail FROM ezform WHERE ezf_id='" . $model->ezf_id . "';")->queryOne();
            if (strlen(trim($field_detail['field_detail']))) {
                $arr_desc = explode(',', $field_detail['field_detail']);
                $str_desc = '';
                foreach ($arr_desc as $val) {
                    $str_desc .= $val . ', ';
                }
                $str_desc = substr($str_desc, 0, -2);
                try {
                    $res = Yii::$app->db->createCommand("SELECT ".$str_desc." FROM " . $field_detail['ezf_table'] . " WHERE id = '".$model->data_id."';")->queryOne();
                } catch (\yii\db\Exception $e){
                    try {
                        $res = Yii::$app->db->createCommand("SELECT ".$str_desc." FROM " . $field_detail['ezf_table'] . " WHERE user_id = '".$model->data_id."';")->queryOne();
                    } catch (\yii\db\Exception $e){

                    }
                }
                $str_desc = '';
                foreach ($res as  $val) {
                    $str_desc .= $val. ' ';
                }
            } else {
                $str_desc = 'ไม่ได้ระบุ';
            }
            return $str_desc;

        },
        'filterType' => GridView::FILTER_COLOR,
        'vAlign' => 'middle',
        'format' => 'raw',
        'width' => '250px',
        'noWrap' => true
    ],
    [
        'attribute' => 'หน่วยงาน',
        'value' => function ($model, $key, $index, $widget) {
            $hospital = \backend\modules\ezforms\components\EzformQuery::getHospital($model->xsourcex);
            $html = '<span class="label label-success" title="'.($hospital['name']. ' ต.'. $hospital['tambon']. ' อ.'. $hospital['amphur']. ' จ.'. $hospital['province']).'">'.$hospital['hcode'].'</span>';
            return $html;
        },
        'filterType' => GridView::FILTER_COLOR,
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'format' => 'raw',
        'width' => '50px',
        'noWrap' => true
    ],
    [
        'attribute' => 'วันที่แก้ไขล่าสุด',
        'value' => function ($model, $key, $index, $widget) {
            if ($model->update_date) {
                $date = new DateTime($model->update_date);
                return $date->format('d/m/Y');
            }

            $date = new DateTime($model->create_date);
            return $date->format('d/m/Y');
        },
        'filterType' => GridView::FILTER_COLOR,
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'format' => 'raw',
        'width' => '50px',
        'noWrap' => true
    ],
    [
        'attribute' => 'บันทึกโดย',
        'value' => function ($model, $key, $index, $widget) {
            if ($model->user_update == "") {
                $user = common\models\UserProfile::findOne($model->user_create);
            } else {
                $user = common\models\UserProfile::findOne($model->user_update);
            }
            return '<span class="text-center fa fa-2x fa-user text-warning" title="บันทึกล่าสุดโดย : '.$user->firstname . ' ' . $user->lastname.'"></span>';

            //return $model->firstname.' '.$model->lastname;
            //return $model->target;
        },
        'filterType' => GridView::FILTER_COLOR,
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'format' => 'raw',
        'width' => '50px',
        'noWrap' => true
    ],
    [
        'attribute' => 'สถานะ',
        'value' => function ($model, $key, $index, $widget) {
            if ($model->rstat == 0) {
                return Html::a('<i class="fa fa-pencil-square-o"></i> New Record', null, ['data-pjax' => 0, 'class' => 'text-default', 'title' => Yii::t('kvgrid', 'รอการกรอกข้อมูล')]);
            }
            else if ($model->rstat == 1) {
                return Html::a('<i class="fa fa-pencil-square-o"></i> Waiting', null, ['data-pjax' => 0, 'class' => 'text-warning', 'title' => Yii::t('kvgrid', 'ข้อมูลยังไม่ถูกส่งเข้าระบบด้วยการคลิก Submitted')]);
            } else if ($model->rstat == 2 || $model->rstat >= 4){
                return Html::a('<i class="fa fa-send"></i> Submitted', null, ['data-pjax' => 0, 'class' => 'text-success', 'title' => Yii::t('kvgrid', 'ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว')]);
            }

            //return $model->firstname.' '.$model->lastname;
            //return $model->target;
        },
        'filterType' => GridView::FILTER_COLOR,
        'vAlign' => 'middle',
        'format' => 'raw',
        'width' => '80px',
        'noWrap' => true
    ],
    [
        'attribute' => 'Action',
        'value' => function ($model, $key, $index, $widget) use ($xsourcex, $totalCountEMR, $ezform_comp) {
            if ($model->xsourcex == $xsourcex) {
                $html = Html::a('<i class="fa fa-edit"></i>', 'redirect-page' . '/?ezf_id=' . $model->ezf_id  . '&dataid=' . $model->data_id, ['data-pjax' => 0, 'class' => 'open-form btn btn-primary', 'title' => Yii::t('kvgrid', 'แก้ไขข้อมูลนี้')]);
                $html_del = Html::a(' <span class="btn btn-danger"><span class="fa fa-trash-o"></span> ลบ</span>', 'step4/?ezf_id=' . $model->ezf_id . '&target=' . base64_encode($model->target_id) . '&del_dataid=' . $model->data_id, [
                    'title' => Yii::t('app', 'ลบ Record นี้ทิ้งไป'),
                    'data-confirm' => '<div class = "alert alert-danger h4">คุณต้องการลบ Record นี้?</div>',
                ]);
                if($ezform_comp['special']==1) {
                    //กรณี ลบจากหน่วยงานต้นทาง
                    if ($ezform_comp['ezf_id'] == $model->ezf_id && $model->target_id == $model->data_id && $model->rstat != 2) {
                        if ($totalCountEMR['mysite'] == 1 && $totalCountEMR['othersite'] == 0 && $ezform_comp['ezf_id'] == $model->ezf_id) {
                            $html .= $html_del;
                        }
                        //กรณี ลบจากหน่วยงานที่ 2 ขึ้นไป
                    } else if ($ezform_comp['ezf_id'] == $model->ezf_id && $model->target_id != $model->data_id && $model->rstat != 2) {
                        if ($totalCountEMR['mysite'] == 1 && $ezform_comp['ezf_id'] == $model->ezf_id) {
                            $html .= $html_del;
                        }
                    } else if ($model->rstat != 2) {
                        $html .= $html_del;
                    }
                }else{
                    //กรณี ลบจากหน่วยงานต้นทาง
                    if ($ezform_comp['ezf_id'] == $model->ezf_id && $model->target_id == $model->data_id) {
                        if ($totalCountEMR['mysite'] == 1 && $totalCountEMR['othersite'] == 0 && $ezform_comp['ezf_id'] == $model->ezf_id) {
                            $html .= $html_del;
                        }
                        //กรณี ลบจากหน่วยงานที่ 2 ขึ้นไป
                    } else if ($ezform_comp['ezf_id'] == $model->ezf_id && $model->target_id != $model->data_id ) {
                        if ($totalCountEMR['mysite'] == 1 && $ezform_comp['ezf_id'] == $model->ezf_id) {
                            $html .= $html_del;
                        }
                    } else {
                        $html .= $html_del;
                    }
                }
            } else {
                $html = Html::a('<i class="fa fa-view"></i> อ่านอย่างเดียว', 'redirect-page' . '/?ezf_id=' . $model->ezf_id . '&dataid=' . $model->data_id . '&action=read-only', ['data-pjax' => 0, 'class' => 'open-form btn btn-warning', 'title' => Yii::t('kvgrid', 'แสดงข้อมูลนี้')]);
            }
            return $html;
        },
        'hAlign' => 'left',
        'filterType' => GridView::FILTER_COLOR,
        'vAlign' => 'middle',
        'format' => 'raw',
        'width' => '50px',
        'noWrap' => true
    ],

];

$gridTargetFromMysite = GridView::widget([
    'dataProvider' => $dataProvidertargetMysite,
    'columns' => $gridColumnstarget,
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'toolbar' => [
        '{export}',
        '{toggleData}'
    ],
    'pjax'=>true,

    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsiveWrap' => false,
    'responsive' => true,
    'hover' => True,
    'showPageSummary' => false,
    'showFooter' => false,
    'resizableColumns' => true,
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'footerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '',
        'after' => false,
        'afterOptions' => false,
    ],
    'persistResize' => false,
]);

$gridTargetFromOthersite = GridView::widget([
    'dataProvider' => $dataProvidertargetOthersite,
    'columns' => $gridColumnstarget,
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'toolbar' => [
        '{export}',
        '{toggleData}'
    ],
    'pjax'=>true,

    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsiveWrap' => false,
    'responsive' => true,
    'hover' => True,
    'showPageSummary' => false,
    'showFooter' => false,
    'resizableColumns' => true,
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'footerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '',
        'after' => false,
        'afterOptions' => false,
    ],
    'persistResize' => false,
]);
?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="ModalUser"><i class="fa fa-check" aria-hidden="true"></i> พบข้อมูลของเป้าหมายดังนี้</h3>
    </div>

    <!-- content -->
    <div class="modal-body">
    <p class="alert alert-danger well-sm no-shadow h4" style="margin-top: 10px;">
        Note :
        หากท่านต้องการลบข้อมูลการลงทะเบียนนี้ออก ท่านจำเป็นต้องลบข้อมูลจากฟอร์มอื่นๆของเป้าหมายให้หมดก่อนจึงจะสามารถลบฟอร์มลงทะเบียนนี้ได้
    </p>
    <p class="h4">ข้อมูลทั้งหมดของเป้าหมายในหน่วยงานท่าน</p>
    <?php echo $gridTargetFromMysite; ?>

    <p class="h4">ข้อมูลทั้งหมดของเป้าหมายในหน่วยงานอื่นๆ</p>
    <?php echo $gridTargetFromOthersite; ?>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> ยกเลิก</button>
    </div>
</div>