<?php

use yii\helpers\Html;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= $modelform->ezf_name; ?></title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        td div { width: 100px; overflow-y: hidden; }
        th, td { padding: 3px; border-style:dotted; }
    </style>

</head>
<body>
<div class="container" >

    <div class='row'>
        <div class='col-lg-12'>
            <h2><?= $modelform->ezf_name; ?></h2>
        </div>
    </div>

    <hr>
    <table border="1px" width="100%" style="font-size: medium;">
    <div class='row'>
        <?php
        //\yii\helpers\VarDumper::dump($modelfield, 10, true);
        //\yii\helpers\VarDumper::dump($model_gen->attributes, 10, true); exit;
        $dataid = Yii::$app->request->get('dataid');
        $index=1; foreach($modelfield as $key=>$field){
            $ezf_field_name = $field->ezf_field_name;
            if($index==1)
                echo '<tr style="vertical-align: top;">';

            if($field->ezf_field_type == 1  || $field->ezf_field_type == 3 || $field->ezf_field_type == 4 || $field->ezf_field_type == 6 || $field->ezf_field_type == 7 || $field->ezf_field_type==8 || $field->ezf_field_type==9 || $field->ezf_field_type==12) {
                echo '<td><b>' . $field->ezf_field_label . '</b> : ' . $model_gen->$ezf_field_name . '<br></td>';
                $index++;
            }else if($field->ezf_field_type == 21) {
                echo '<td>'.$model_gen->$ezf_field_name . '<br></td>';
                $index++;
            }
            else if($field->ezf_field_type == 11 || $field->ezf_field_type == 14 || $field->ezf_field_type == 17 ||  $field->ezf_field_type == 28 || $field->ezf_field_type == 27) {
                echo '<td><b>' . $field->ezf_field_label . '</b> : ' . $model_gen->$ezf_field_name . '<br></td>';
                $index++;
            }else if($field->ezf_field_type == 10) {
                echo '<td><b>' . $field->ezf_field_label . '</b> : ' . $model_gen->$ezf_field_name . '<br></td>';
                $index++;
            }else if($field->ezf_field_type == 16 || $field->ezf_field_type == 19) {
                if($model_gen->$ezf_field_name) {
                    echo '<td><b>' . $field->ezf_field_label . '</b> : ใช่<br></td>';
                }else{
                    echo '<td><b>' . $field->ezf_field_label . '</b> : ...<br></td>';
                }
                $index++;
            }else if($field->ezf_field_type==18){
                $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_ref_field])->one();
                $field_name_ref =$ezfField->ezf_field_name; //field name ปลายทาง
                $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $dataid, $field_name_ref);
                if($res[$field_name_ref]) {
                    echo '<td><b>' . $field->ezf_field_label . ' : </b>' . $res[$field_name_ref] . '<br></td>';
                }else{
                    echo '<td><b>' . $field->ezf_field_label . '</b> : ' . $model_gen->$ezf_field_name . '<br></td>';
                }
                $index++;
            }else if($field->ezf_field_type == 25) {
                $modelchoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$field->ezf_field_id])->all();
                $modelquestion = \backend\modules\ezforms\models\EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[':ezf_field_id'=>$field->ezf_field_id])->all();

                $html = "<td>";
                $html .= "<b>".$field->ezf_field_label."</b><br>";
                $html .= $field->ezf_field_help;

                foreach($modelquestion as $question){
                    $modelvalue = \backend\modules\ezforms\models\EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[":ezf_field_id"=>$question["ezf_field_id"]])->all();
                    if($question["ezf_field_label"]) {
                        $html .= '<span style="padding-right:3px;">' . $question["ezf_field_label"] . '</span>';
                    }
                    $j=0;
                    foreach($modelvalue as $value){
                        $ezf_field_name = $value["ezf_field_name"];
                        $html .= $modelchoice[$j]['ezf_choicelabel'];
                        if($value["ezf_field_type"]=="251" || $value["ezf_field_type"]=="252"){
                            $html .= "<span style=\"padding-left:3px; padding-right:3px;\">(". $model_gen->$ezf_field_name.")</span>";
                        }else if($value["ezf_field_type"]=="253"){
                            $date = $model_gen->$value["ezf_field_name"];
                            if($date) {
                                $explodeDate = explode('-', $date);
                                $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
                                $model_gen->$value["ezf_field_name"] = $formateDate;
                            }
                            $html .= "<span style=\"padding-left:3px; padding-right:3px;\">(". $model_gen->$ezf_field_name.")</span>";
                        }else if($value["ezf_field_type"]=="254"){
                            if($model_gen->$ezf_field_name) {
                                $html .= "<span style=\"padding-left:3px; padding-right:3px;\">(ใช่)</span>";
                            }else{
                                $html .= "";
                            }
                        }
                        $j++;
                    }
                    $html .= '<hr style="margin-top:2px; margin-bottom: 0px;">';
                }


                $html .= "</td>";
                echo $html;
                $index++;
            }else if($field->ezf_field_type == 23) {
                $modelchoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->all();
                $modelsubfield = \backend\modules\ezforms\models\EzformFields::find()->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id' => $field->ezf_field_id])->all();
                $html = "<td>";
                $html .= "<b>" . $field->ezf_field_label . "</b><br>";
                $html .= $field->ezf_field_help;

                $i = 1;
                foreach($modelsubfield as $subfield){
                    $modelsubchoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$subfield->ezf_field_sub_id])->all();


                    $html .= '<span style="padding-right:3px;">'.$i.". ".$subfield["ezf_field_label"].'</span>';

                    $j = 0;
                    foreach($modelsubchoice as $subchoice){
                        $ezf_field_name = $subfield["ezf_field_name"];
                        $html .=($subchoice["ezf_choicevalue"]==$model_gen->$ezf_field_name ? '('.$modelchoice[$j]['ezf_choicelabel'].')' : '');
                        $j++;
                    }
                    $html .= '<br>';
                    $i++;

                }
                $html .= "</td>";
                $index++;
                echo $html;
            }


            if($index==4){
                echo '</tr>';
                $index=1;
            }

        } ?>
    </div>
    </table>

</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script>
window.onload = function () {
    window.print();
}
</script>
</body>
</html>





