<?php
use yii\bootstrap\Html;
use yii\helpers\Url;

//set values
$xsourcex = Yii::$app->user->identity->userProfile->sitecode;
$input_ezf_id = Yii::$app->request->get('ezf_id');
$input_target= Yii::$app->request->get('target');
$input_comp_target =Yii::$app->request->get('comp_id_target');
$input_dataid =Yii::$app->request->get('dataid');

?>

<div class='row'>
    <div class='col-lg-12'>
        <h2><?= $modelEzform->ezf_name; ?></h2>
    </div>
</div>
<div class='row'>
    <div class='col-lg-12'>
        <h4><?= $modelEzform->ezf_detail; ?></h4>
    </div>
</div>

    <hr>
<?php if($_SESSION['save_status']) echo \yii\bootstrap\Alert::widget(Yii::$app->session->getFlash('save_status')); ?>

<?php if ($datamodel_table->error && $datamodel_table->rstat != 0) { ?>
    <div class="alert alert-danger text-left">
        <span class="h2">Data Validation</span><hr><h3>กรุณาระบุข้อต่อไปนี้ (ถ้ากรอกครบแล้ว ให้กดปุ่ม [Save draft] อีกครั้งเพื่อตรวจสอบเงื่อนไขใหม่) :<br><b><?php echo $datamodel_table->error; ?></b></h3>
    </div>
    <hr>
<?php } ?>
<?php if ($sql_announce) { ?>
    <div class="alert alert-warning text-left">
        <span class="h2">Data Announce</span><hr><h3>พบการตรวจสอบเงื่อนไขดังนี้ :<br><b><?php echo $sql_announce; ?></b></h3>
    </div>
    <hr>
<?php } ?>

<?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action' => '/ezforms/ezform/savedata2','options'=>['name'=> 'frmSaveData', 'enctype'=>'multipart/form-data']]); ?>
    <input type='hidden' name='ezf_id' id='ezf_id' value='<?php echo $input_ezf_id; ?>'>
<?php if ($input_dataid) { ?>
    <input type='hidden' name='id' id='id' value='<?php echo $input_dataid; ?>'>
<?php } ?>
    <input type='hidden' name='url_redirect' id='url_redirect' value='<?php echo $_GET['rurl'] ? $_GET['rurl'] : base64_encode(Yii::$app->request->url); ?>'>
<?php if ($input_target) { ?>
    <input type='hidden' id ='txt_target' name='target' value='<?php echo $input_target; ?>'>
    <input type='hidden' id ='comp_id_target' name='comp_id_target' value='<?php echo $input_comp_target; ?>'>
<?php } ?>

    <!-- content -->
    <div class="row" id="formPanel">
        <?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action' => '/ezform/savedata2','options'=>['name'=> 'frmSaveData', 'enctype'=>'multipart/form-data']]); ?>
        <?php
        foreach ($modelfield as $field) {

            //reference field
            $options_input = [];
            if($field->ezf_field_type == 18) {
                //ถ้ามีการกำหนดเป้าหมาย

                if ($field->ezf_field_val == 1) {
                    //แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                    //set form disable
                    $options_input['readonly'] = true;
                    $form->attributes['rstat'] = 0;
                } else if ($field->ezf_field_val == 2) {
                    //ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_id);
                    $form->attributes['rstat'] = $datamodel_table->rstat;
                } else if ($field->ezf_field_val == 3) {
                    //แก้ไขเฉพาะตารางปลายทาง
                    $modelDynamic = \backend\modules\ezforms\components\EzformQuery::getFormTableName($field->ezf_field_ref_table);
                    //\yii\helpers\VarDumper::dump($ezfField, 10, true);
                    //echo $field->ezf_field_ref_field,'-';
                    $options_input['readonly'] = true;
                    $form->attributes['rstat'] = 0;
                }
                $ezfTable = $modelDynamic->ezf_table;

                $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_ref_field])->one();
                if(!$ezfField->ezf_field_name){
                    $ezfField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $field->ezf_field_id])->one();
                    $html = '<h1>Error</h1><hr>';
                    $html .= '<h3>เนื่องการเชื่อมโยงของประเภทคำถาม Reference field ผิดพลาด การแก้ไขคือ ลบคำถามออกแล้วสร้างใหม่</h3>';
                    $html .= '<h4>คำถามที่ผิดพลาดคือ : </h4>'. $ezfField->ezf_field_name.' ('. $ezfField->ezf_field_label.')';
                    echo $html;
                    Yii::$app->end();
                }
                $modelDynamic = new backend\modules\ezforms\models\EzformDynamic($ezfTable);

                //กำหนดตัวแปร
                $field_name = $field->ezf_field_name; //field name ต้นทาง
                $field_name_ref = $ezfField->ezf_field_name; //field name ปลายทาง

                //ดูตาราง Ezform ฟิลด์ target ที่มีเป้าหมายเดียวกัน
                if ($input_target == 'byme' || $input_target == 'skip' || $input_target == 'all') {
                    $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($input_ezf_id, $input_dataid);
                    $target = $target['ptid'];
                } else {
                    $target = base64_decode($input_target);
                }
                //หา target ใน site ตัวเองก่อน (ก็ไม่แนะนำ)
                $res = $modelDynamic->find()->select($field_name_ref)->where('ptid = :target AND ' . $field_name_ref . ' <> "" AND xsourcex = :xsourcex AND rstat <>3', [':target' => $target, ':xsourcex' => $datamodel_table->xsourcex])->orderBy('create_date DESC');

                //echo $target.' -';
                //หาที่ target ก่อน
                if ($res->count()) {
                    $model_table = $res->One();
                    if ($field->ezf_field_val == 1) {
                        //echo 'target 1-1<br>';
                        //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                        $model_gen->$field_name = $model_table->$field_name_ref;
                        $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                    } else if ($field->ezf_field_val == 2) {

                    } else if ($field->ezf_field_val == 3) {
                        if ($input_dataid) {
                            //echo 'target 1<br>';
                            $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                            if ($res[$field_name_ref]) {
                                $model_table->$field_name_ref = $res[$field_name_ref];
                            }
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        } else {
                            //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);
                            // echo 'target else 1<br>';
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        }
                    }

                } else {
                    //กรณีหาไม่เจอ ให้ดู primary key
                    $res = $modelDynamic->find()->where('id = :id', [':id' => $target]);
                    if ($res->count()) {
                        $model_table = $res->One();

                        if ($field->ezf_field_val == 1) {
                            //echo 'target 2-1<br>';
                            //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                            $model_gen->$field_name = $model_table->$field_name_ref;
                            $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                        } else if ($field->ezf_field_val == 2) {

                        } else if ($field->ezf_field_val == 3) {
                            if ($input_dataid) {
                                //echo 'target 2<br>';
                                $res = \backend\modules\ezforms\components\EzformQuery::getReferenceData($field->ezf_id, $field->ezf_field_id, $input_dataid, $field_name_ref);
                                if ($res[$field_name_ref]) {
                                    $model_table->$field_name_ref = $res[$field_name_ref];
                                }
                                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                $model_gen->$field_name = $model_table->$field_name_ref;
                                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                            } else {
                                //echo 'target else 2<br>';
                                //\yii\helpers\VarDumper::dump($model_table->attributes, 10,true);

                                //เปลี่ยนค่า field name ระหว่างต้นทาง และปลายทาง
                                $model_gen->$field_name = $model_table->$field_name_ref;
                                $model_gen->attributes[$field_name] = $model_table->$field_name_ref;
                            }
                        }
                    }
                }

                // yii\helpers\VarDumper::dump($model_table, 10, true);
                //Yii::$app->end();
            }else if($modelEzform->query_tools == 2 && Yii::$app->keyStorage->get('frontend.domain') == "cascap.in.th" && ($input_ezf_id == "1437377239070461301" || $input_ezf_id == "1437377239070461302")){
                //not doing every thing
            }else if($modelEzform->query_tools == 2){
                $form->attributes['rstat'] = $datamodel_table->rstat;
            }
            //end reference field

            echo backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $field, $form, $options_input);
            //echo 'zzz<br>';
        }
        ?>

    </div>

    <div class="row" id="ezform-btn-saveform">
        <div class='col-lg-12 text-right'>
            <div id="div-savedraft"></div>
            <hr>
            <?php
            //for doctor
            if (Yii::$app->user->can('doctorcascap')) {
                echo 'ท่านได้รับสิทธิ์ผู้เชี่ยวชาญ (Role) Doctor of CASCAP สามารถเปลี่ยนแปลงข้อมูลนี้โดยกดที่ปุ่ม <code><span class="fa fa-send"></span> Save</code> ซึ่งไม่กระทบสถานะข้อมูลอื่นๆ (rstat)<br>';
                //$datamodel_table->rstat ==9 //เพื่อให้แก้ไขค่าได้
                echo '&nbsp;&nbsp;' . Html::button('<span class="fa fa-send"></span> Save',  [
                        'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'บันทึกผล'),
                        'class' => 'btn btn-success btn-lg',
                        'id' => 'final-save',
                        'type' => 'submit',
                    ]);
                if($datamodel_table->xsourcex == $xsourcex)
                    echo '<hr><br>ปุ่มอื่นๆสำหรับการบันทึกข้อมูล<hr>';
            }


            if($datamodel_table->xsourcex == $xsourcex) {

                if($_SESSION['input_rurl']){
                    echo '&nbsp;&nbsp;', Html::a('<span class="fa fa-reply-all"></span> Backward', base64_decode($_SESSION['input_rurl']), [
                        'class' => 'btn btn-success btn-lg',
                        'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ย้อนกลับรายการก่อนหน้านี้'),
                    ]);
                }

                if (($datamodel_table->rstat == 0 || $datamodel_table->rstat == 1 || ($modelEzform->query_tools == 1 || $modelEzform->query_tools == 0) || Yii::$app->user->can('doctorcascap'))) {
                    if ($modelEzform->query_tools == 2) {
                        echo '&nbsp;&nbsp;', Html::button('<span class="fa fa-pencil-square-o"></span> Save draft', [
                                'id' => 'save-draft',
                                'class' => 'btn btn-info btn-lg',
                                'type' => 'button',
                                'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'บันทึกข้อมูลเพื่อแก้ไขในภายหลัง'),
                                'style'=> 'margin: 10px 0px 10px 0px;'
                            ]).'';
                    }
                    if ($input_dataid) {
                        if ($modelEzform->query_tools == 3 || ($datamodel_table->error == '' && $modelEzform->query_tools == 2) && ($datamodel_table->rstat == 0 || $datamodel_table->rstat == 1)) {
                            echo '&nbsp;&nbsp;', Html::button('<span class="fa fa-send"></span> Submit', [
                                'id' => 'save-submit',
                                'class' => 'btn btn-primary btn-lg',
                                'type' => 'button',
                                'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ส่งข้อมูลนี้เข้าระบบฐานข้อมูล'),
                                'style'=> 'margin: 10px 0px 10px 0px;',
                                //'data-confirm' => '',
                            ]);
                        } else if ($modelEzform->query_tools == 1 || $modelEzform->query_tools == 0) {
                            echo '&nbsp;&nbsp;', Html::button('<span class="fa fa-send"></span> Submit', [
                                'id' => 'save-submit2',
                                'class' => 'btn btn-primary btn-lg',
                                'type' => 'button',
                                'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ส่งข้อมูลนี้เข้าระบบฐานข้อมูล'),
                                'style'=> 'margin: 10px 0px 10px 0px;',
                            ]);
                        }

                        if ($ezform_comp['ezf_id'] <> $input_ezf_id) {
                            echo '&nbsp;&nbsp;', Html::a('<span class="fa fa-times"></span> Clear', ['insert-record', 'ezf_id' => $input_ezf_id, 'target' => $input_target, 'comp_id_target' => $input_comp_target, 'dataid' => $input_dataid], [
                                'class' => 'btn btn-warning btn-lg',
                                'type' => 'button',
                                'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ล้างข้อมูลในฟอร์มนี้'),
                                'data-confirm' => '<div class = "alert alert-danger h4">คุณต้องการล้าง(รีเช็ต) Record นี้?</div>',
                            ]);
                        }

                        //การลบข้อมูลกรณี ฟอร์มลงทะเบียน
                        if($ezform_comp['ezf_id'] == $input_ezf_id) {
                            echo '&nbsp;&nbsp;' . Html::button('<span class="fa fa-trash-o"></span> Delete', [
                                    'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ลบ Record นี้ทิ้งไป'),
                                    'class' => 'btn btn-danger btn-lg',
                                    'data-url'=>Url::to(['inputdata/remove-data', 'ezf_id' => $input_ezf_id, 'data_id' => $input_dataid, 'target' => $input_target]),
                                    'id' =>'btn-remove-data',
                                ]);
                            //กรณีฟอร์มทั่วไป
                        }else{
                            echo '&nbsp;&nbsp;' . Html::a('<span class="fa fa-trash-o"></span> Delete', ['step4', 'ezf_id' => $input_ezf_id, 'target' => $input_target, 'del_dataid' => $input_dataid], [
                                    'class' => 'btn btn-danger btn-lg',
                                    'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ลบ Record นี้ทิ้งไป'),
                                    'data-confirm' => '<div class = "alert alert-danger h4">คุณต้องการลบ Record นี้?</div>',
                                ]);
                        }

                    }

                }
            }

            //re save draft
            if (($datamodel_table->rstat == 2 || $datamodel_table->rstat == 4) && $modelEzform->query_tools == 2) {
                //$queryManager = \backend\models\QueryManager::find()->where('user_id = :user_id AND user_group = :user_group', [':user_id' => (Yii::$app->user->id), ':user_group' => 1])->one();
                if (Yii::$app->user->can('administrator')||(Yii::$app->user->can('adminsite')&&((strtotime("now")-strtotime($datamodel_table->update_date))/60/60/24) <=30)) {
                    echo '&nbsp;&nbsp;' . Html::a('<span class="fa fa-repeat"></span> Re-Save Draft', ['resave-draft', 'ezf_id' => $input_ezf_id, 're_dataid' => $input_dataid], [
                            'class' => 'btn btn-warning btn-lg',
                            'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'Re-Save Draft ที่ Record นี้'),
                            'data-confirm' => "<div class = 'alert alert-danger h4'>คุณต้องการ Re-Save Draft ที่ Record นี้?</div>",
                        ]);
                }
            }
            /*
            else if($modelEzform->query_tools == 2){

                echo ' ', Html::button('<span class="fa fa-send"></span> Query', [
                    'class' => 'btn btn-success btn-lg',
                    'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ส่ง Query'),
                    'id' => 'ezform-btn-query',
                    'data-url'=>Url::to(['query-request/create', 'ezf_id' => $input_ezf_id, 'data_id' => $input_dataid]),
                ]);
                echo ' ', Html::button('<span class="fa fa-send"></span> Change', [
                    'class' => 'btn btn-primary btn-lg',
                    'data-toggle' =>'tooltip', 'data-original-title' => Yii::t('app', 'ส่งคำขอแก้ไขข้อมูล'),
                    'id' => 'ezform-btn-change',
                    'data-url'=>Url::to(['query-request/create', 'ezf_id' => $input_ezf_id, 'data_id' => $input_dataid]),
                ]);


            }*/
            ?>

        </div>
    </div>

<?php backend\modules\ezforms\components\EzActiveForm::end(); ?>


<?php
foreach($modelfield as $value){
    $inputId = Html::getInputId($model_gen, $value['ezf_field_name']);
    $inputValue = Html::getAttributeValue($model_gen, $value['ezf_field_name']);

    $dataCond = backend\modules\ezforms\components\EzformQuery::getCondition($value['ezf_id'], $value['ezf_field_name']);
    if($dataCond){
        //Edit Html
        $fieldId = Html::getInputId($model_gen, $value['ezf_field_name']);
        if($value['ezf_field_type']==4){
            $fieldId = $value['ezf_field_name'];
        }

        $enable = TRUE;
        foreach ($dataCond as $index => $cvalue) {
            //if($inputValue == $cvalue['ezf_field_value'] || $inputValue == ''){
            $dataCond[$index]['cond_jump'] = json_decode($cvalue['cond_jump']);
            $dataCond[$index]['cond_require'] = json_decode($cvalue['cond_require']);


            if($value['ezf_field_type']=='4' || $value['ezf_field_type']=='6'){
                if($inputValue == $cvalue['ezf_field_value'] || $inputValue == ''){
                    if ($enable){
                        $enable = false;
                        $jumpArr = json_decode($cvalue['cond_jump']);
                        if(is_array($jumpArr)) {
                            foreach ($jumpArr as $j => $jvalue) {
                                $this->registerJs("
										    var fieldIdj = '" . $jvalue . "';
										    var inputIdj = '" . $fieldId . "';
										    var valueIdj = '" . $inputValue . "';
										    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
										    var fTypej = '" . $value['ezf_field_type'] . "';
										    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'none');
									    ");
                            }
                        }

                        $requireArr = json_decode($cvalue['cond_require']);
                        if(is_array($requireArr)) {
                            foreach ($requireArr as $r => $rvalue) {
                                $this->registerJs("
										    var fieldIdr = '" . $rvalue . "';
										    var inputIdr = '" . $fieldId . "';
										    var valueIdr = '" . $inputValue . "';
										    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
										    var fTyper = '" . $value['ezf_field_type'] . "';
										    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'block');
									    ");
                            }
                        }
                    }
                }
            } else {

                $jumpArr = json_decode($cvalue['cond_jump']);
                if(is_array($jumpArr)) {
                    foreach ($jumpArr as $j => $jvalue) {
                        $this->registerJs("
									    var fieldIdj = '" . $jvalue . "';
									    var inputIdj = '" . $fieldId . "';
									    var valueIdj = '" . $inputValue . "';
									    var fixValuej = '" . $cvalue['ezf_field_value'] . "';
									    var fTypej = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'block');
								    ");
                    }
                }

                $requireArr = json_decode($cvalue['cond_require']);
                if(is_array($requireArr)) {

                    foreach ($requireArr as $r => $rvalue) {

                        $this->registerJs("
									    var fieldIdr = '" . $rvalue . "';
									    var inputIdr = '" . $fieldId . "';
									    var valueIdr = '" . $inputValue . "';
									    var fixValuer = '" . $cvalue['ezf_field_value'] . "';
									    var fTyper = '" . $value['ezf_field_type'] . "';
									    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'none');
									    
								    ");
                    }
                }
            }




//						    } else {
//							$dataCond[$index]['cond_jump'] = json_decode($cvalue['cond_jump']);
//							$dataCond[$index]['cond_require'] = json_decode($cvalue['cond_require']);
//						    }
        }
        //Add Event
        if($value['ezf_field_type']==20 || $value['ezf_field_type']==0 || $value['ezf_field_type']==16){
            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputId = '".$inputId."';
		    eventCheckBox(inputId, dataCond);
		    setCheckBox(inputId, dataCond);
		");
        }else if($value['ezf_field_type']==6){
            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputId = '".$inputId."';
		    eventSelect(inputId, dataCond);
		    setSelect(inputId, dataCond);
		");
        } else if($value['ezf_field_type']==4){
            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputName = '".$value['ezf_field_name']."';
		    eventRadio(inputName, dataCond);
		    setRadio(inputName, dataCond);
		");
        }

    }
}

//
if($modelEzform->js) $this->registerJs($modelEzform->js);
//

$this->registerJs("
$('.file-preview-image').click(function(){
    var img = $(this).attr('src');
var filename = $(this).attr('data-filename');
    modalEzform('" . Url::to(['inputdata/viewimg', 'img' => '']) . "'+filename);
});

$( \".open-form\" ).on( \"click\", function() {
});
$( \"#save-draft\" ).on( \"click\", function() {
    $('form[name=frmSaveData]').submit();
});
//submit for lock
$( \"#save-submit\" ).on( \"click\", function() {
    bootbox.confirm({
        title: \"ยืนยันข้อตกลง ?\",
        message: \"<div class = 'alert alert-danger h3'>เมื่อกดที่ปุ่ม Submit แล้ว ท่านจะไม่สามารถแก้ไขข้อมูลได้ด้วยตนเอง หากจำเป็นต้องการแก้ไข ต้องขอเสนอแก้ไขข้อมูลต่อไป?</div>\",
        buttons: {
            cancel: {
                label: '<i class=\"fa fa-times\"></i> Cancel'
            },
            confirm: {
                label: '<i class=\"fa fa-check\"></i> Confirm'
            }
        },
        callback: function (result) {
            console.log('This was logged in the callback: ' + result);
            
            if(result){
                $('#div-savedraft').html('".Html::textInput('txtSubmit', '1', ['id' => 'txtSubmit', 'type' => 'hidden'])."');
                $('form[name=frmSaveData]').submit();
            }
        }
    });
});
//submit no lock
 $( \"#save-submit2\" ).on( \"click\", function() {
    $('#div-savedraft').html('".Html::textInput('txtSubmit', '1', ['id' => 'txtSubmit', 'type' => 'hidden'])."');
    $('form[name=frmSaveData]').submit();
});
$( \"#final-save\" ).on( \"click\", function() {
    $('#div-savedraft').html('".Html::textInput('finalSave', '1', ['id' => 'finalSave', 'type' => 'hidden'])."');
    $('form[name=frmSaveData]').submit();
});

$( \"#addNewRecord\" ).on( \"click\", function() {
     var ezf_id = '".$input_ezf_id."';
    var target = '".$input_target."';
    var rurl = '".Yii::$app->request->get('rurl')."';
    var comp_id_target = '".$input_comp_target."';
    var dataset = '".$_GET['dataset']."';
    $(this).prop(\"disabled\", true);

    $.post(\"".Url::to(['insert-record'])."\", {ezf_id: ezf_id, target: target, comp_id_target: comp_id_target, rurl : rurl, dataset : dataset}, function(result){
        $(location).attr('href',result);
    });
});
function modalEzform(url) {
    $('#modal-ezform .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ezform').modal('show')
    .find('.modal-content')
    .load(url);
}

$('.inputdata-index').on('click', '#watch-video', function(){
        modalQueryRequest($(this).attr('data-url'));
});
$('#formPanel').on('click', '.ezform-btn-change', function(){
        modalQueryRequest($(this).attr('data-url'));
});
$('#ezform-btn-saveform').on('click', '#btn-remove-data', function(){
        modalQueryRequest($(this).attr('data-url'));
});
function modalQueryRequest(url) {
    $('#modal-query-request .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-query-request').modal('show')
    .find('.modal-content')
    .load(url);
}

");
?>