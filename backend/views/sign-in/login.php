<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\models\LoginForm */

$this->title = Yii::t('backend', 'Sign In');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';

?>
<div class="login-box">
    <div class="login-logo">
        <?php echo Html::encode($this->title) ?>
    </div><!-- /.login-logo -->
    <div class="header"></div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="body">
            <?php if(Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){?>
                <div class="col-md-12"><div class="alert alert-success h4">สมาชิก CASCAP Tools สามารถใช้ Username, Password เดิม</div></div>
            <?php } ?>
            <?php echo $form->field($model, 'username') ?>
            <?php echo $form->field($model, 'password')->passwordInput() ?>
            <?php echo $form->field($model, 'rememberMe')->checkbox(['class'=>'simple']) ?>
	    
	    <div style="color:#999;margin:1em 0">
		<?php echo Yii::t('frontend', '<a href="{link}">Forgot your password, click here.</a>', [
		    'link'=>Yii::getAlias('@frontendUrl'). '/user/sign-in/request-password-reset'
		]) ?>
	    </div>
	    <div class="form-group">
		<?php echo Html::a(Yii::t('frontend', 'Need an account? Sign up.'), Yii::getAlias('@frontendUrl'). '/user/sign-in/signup') ?>
	    </div>
        </div>
        <div class="footer">
            <?php echo Html::submitButton(Yii::t('backend', 'Sign me in'), [
                'class' => 'btn btn-primary btn-flat btn-block',
                'name' => 'login-button'
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <br>
        <center><a href="<?php echo Yii::getAlias('@frontendUrl'); ?>">กลับหน้าหลัก</a>
    </div>

</div>