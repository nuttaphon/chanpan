<?php
use yii\bootstrap\Html;
?>
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ModalUser">User Info</h4>
    </div>
    <div class="modal-body">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 text-center">
                        <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" alt="" class="center-block img-thumbnail img-responsive">
                        <br>
                    </div><!--/col-->

                    <div class="col-xs-12 col-sm-6 col-md-6">


                        <h3><?php echo Yii::$app->user->identity->userProfile->getFullName(); ?></h3>
                        <p><strong>About: </strong> <?php echo $about['code'].' - '.$about['name']; ?></p>
                        <p><strong>Member since: </strong><?php echo Yii::t('backend', '{0, date, full}', Yii::$app->user->identity->created_at); ?></p>
                        <p><strong>Groups: </strong>
                            <?php
                            foreach($usergroup as $val){
                                echo '<span class="label label-success tags">'.$val['item_name'].'</span> ';
                            }
                            ?>
                        </p>
                    </div><!--/col-->
                </div><!--/row-->

                <div class="row" style="margin-bottom: 5px;">
                    <div class="col-xs-12 col-sm-6">
                        <!--                            <h2><strong> 20,7K </strong></h2>-->
                        <!--                            <p><small>Followers</small></p>-->
                        <?php echo Html::a('<span class="fa fa-user"></span> ตั้งค่าผู้ใช้', ['/sign-in/profile'], ['class'=>'btn btn-success btn-block']) ?>
                    </div><!--/col-->
                    <div class="col-xs-12 col-sm-6">
			<?php //echo Html::a('<span class="glyphicon glyphicon-ban-circle"></span> ยกเลิกการเป็นสมาชิก', ['/site/canceluser'], ['data-confirm' => Yii::t('yii', 'คุณต้องการยกเลิกการเป็นสมาชิก หรือไม่?'), 'class'=>'btn btn-warning btn-block', 'data-method' => 'post']) ?>
                        
                    </div><!--/col-->
		    
                </div>

		<div class="row">
		    <div class="col-xs-12 col-sm-6">
                        <?php echo Html::a('<span class="fa fa-edit"></span> เปลี่ยนรหัสผู้ใช้', ['/sign-in/account'], ['class'=>'btn btn-primary btn-block']) ?>
                    </div><!--/col-->
                    <div class="col-xs-12 col-sm-6">
                        <?php echo Html::a('<span class="fa fa-sign-out"></span> ออกจากระบบ', ['/sign-in/logout'], ['class'=>'btn btn-danger btn-block', 'data-method' => 'post']) ?>
                    </div><!--/col-->
		</div>
            </div>
        </div>

    </div>

</div>