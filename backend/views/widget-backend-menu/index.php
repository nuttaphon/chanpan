<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\WidgetBackendMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Widget Backend Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-backend-menu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Widget Backend Menu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'titlename',
            [
                'attribute' => 'URL Link',
                'format'=>'raw',
                'value'=>function ($dataProvider, $index, $widget){
                    return Html::a(
                        $dataProvider->url,                     //link text
                        $dataProvider->url, //link url to some route
                        [                                 // link options
                            'title'=>'Go!',
                            'target'=> ($dataProvider->target == '_blank') ? '_blank' : '_self',
                        ]
                    );
                }
            ],
            [
                'attribute' => 'ลักษณะการ Link',
                'format'=>'raw',
                'value'=>function ($dataProvider, $index, $widget){

                    $target = '';
                    if($dataProvider->target == '_blank'){
                        $target = 'New Tab';
                    }else if($dataProvider->target == 'none' OR $dataProvider->target == ''){
                        $target = 'Current Page';
                    }
                    return $target;
                }
            ],
            'type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
