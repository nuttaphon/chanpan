<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\WidgetBackendMenu */

$this->title = 'Update Widget Backend Menu: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Widget Backend Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="widget-backend-menu-update">
    <?= Html::a('< Back', ['index'], ['class' => 'btn btn-success']); ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
