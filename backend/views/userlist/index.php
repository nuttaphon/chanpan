<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\lib\sdii\components\helpers\SDNoty;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'รายชื่อผู้ใช้งานในหน่วยบริการ');
$this->params['breadcrumbs'][] = $this->title;


?>
<ul class="nav nav-tabs">
    <li role="presentation" class="active"><a href="<?=  Url::to(['userlist/index'])?>">บุคลากร</a></li>
  <li role="presentation"><a href="<?=  Url::to(['userlist/volunteer'])?>">อาสามาสมัคร</a></li>
  <li role="presentation"><a href="#">ประชาชน</a></li>
</ul>

<div class="user-index" >
    <div class="text-right" style="margin-top: 10px;" >
    <?php $form = ActiveForm::begin([
	'id' => 'jump_menu',
        'action' => ['index'],
        'method' => 'get',
	'layout' => 'horizontal',
    ]); ?>
	
	<?php // Html::dropDownList('site', $sitecode, \yii\helpers\ArrayHelper::map($sitelist, 'code', 'name'), ['class'=>'form-control pull-right', 'onChange'=>'$("#jump_menu").submit()'])
	if (Yii::$app->user->can('administrator')) {
	?>
	<div class="row">
	    <div class="col-lg-12 col-md-12 col-sm-12">
		<div class="text-left" style="width: 250px; display: inline-block;">
		    <?=	kartik\widgets\Select2::widget([
			'name' => 'pcode',
			'data' => yii\helpers\ArrayHelper::map(backend\models\ConstProvince::find()->all(), 'PROVINCE_CODE', 'PROVINCE_NAME'),
			'value'=>$pcode,
			'pluginOptions' => [
			    'allowClear' => true
			],
			'options' => [
			    'class'=>'pull-right',
			    'prompt'=>'All',
			    'onChange'=>'$("#jump_menu").submit()'
			],
		    ]);?>
		</div>
		<div class="text-left" style="width: 550px; display: inline-block; ">

		    <?=	kartik\widgets\Select2::widget([
			'name' => 'site',
			'initValueText' => $sitelist['name'],
			'value'=>$sitecode,
			'options' => [
			    //'placeholder' => 'Select ...',
			    'class'=>'pull-right',
			    'onChange'=>'$("#jump_menu").submit()'
			],
			'pluginOptions' => [
			    'minimumInputLength' => 0,
			    'ajax' => [
				'url' => \yii\helpers\Url::to(['site-list', 'pcode'=>$pcode]),
				'dataType' => 'json',
				'data' => new JsExpression('function(params) { return {q:params.term}; }')
			    ],
			    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			    'templateResult' => new JsExpression('function(data) { return data.text; }'),
			    'templateSelection' => new JsExpression('function (data) { return data.text; }'),
		    ],
		    ]);?>
		</div>
	    </div>
	    
	</div>
	
	<?php 
	}
	$authSite = $sitecode==Yii::$app->user->identity->userProfile->sitecode;
	 ActiveForm::end(); ?>
    </div>
    </p>
    
<?php Pjax::begin(['id'=>'user-grid-pjax']) ?>
    <?php echo \common\lib\sdii\widgets\SDGridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
	    'class' => 'yii\grid\SerialColumn',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:50px;text-align: center;'],
	    ],
	    [
		'header' => 'ชื่อ-สกุล',
		'value' => function ($model) {
		    return $model->userProfile->firstname.' '.$model->userProfile->lastname;
		},
		'contentOptions'=>['style'=>'width:190px;'],
		//'headerOptions'=>['style'=>'text-align: center;'],
		//'contentOptions'=>['style'=>'width:190px;'],//text-align: center;
	    ],
	
	    [
		'header' => 'Sitecode',
		'value' => function ($model) {
		    $sql = "SELECT all_hospital_thai.hcode, 
			    all_hospital_thai.`name`, 
			    all_hospital_thai.tambon, 
			    all_hospital_thai.amphur, 
			    all_hospital_thai.province, 
			    all_hospital_thai.code5
		    FROM all_hospital_thai
		    WHERE all_hospital_thai.hcode = :code";

		$sitecode = $model->userProfile->sitecode;
		$data = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();
		if($data){
		    $title = "{$data['name']} ต.{$data['tambon']} อ.{$data['amphur']} จ.{$data['province']}";
		    return '<a data-toggle="tooltip" title="'.$title.'">'.$sitecode.'</a>';
		}
		   return $sitecode;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
	    [
		'header' => 'ใบ/บัตร',
		'value' => function ($model) {
		   $userProfile = $model->userProfile;
		   $secret_file = !empty($userProfile->secret_file)?'y':'n';
		   $citizenid_file = !empty($userProfile->citizenid_file)?'y':'n';
		   return $secret_file.'/'.$citizenid_file;
		},
		//'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:70px;text-align: center;'],
	    ],
			[
		'header' => 'เวลา',
		'value' => function ($model) {
		    $date = Yii::$app->formatter->asDate($model->created_at, 'php:Y-m-d H:i:s');
		    $str = common\lib\sdii\components\utils\SDdate::differenceTimer($date);
		    
		    return $str;
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px;text-align: center;'],
	    ],	
	    [
		'attribute'=>'created_at',
		'header' => 'วันที่สมัคร',
		'value' => function ($model) {
		    Yii::$app->formatter->locale = 'th';

		   return Yii::$app->formatter->asDate($model->created_at, 'short');
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
	    ],	
			[
		'header' => 'Enroll key',
		'value' => function ($model) {
		    return $model->userProfile->enroll_key;
		},
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;'],//text-align: center;
	    ],
	[
		'header' => 'อนุญาต',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('manager', $model->id);
		    if (isset($auth->roleName)) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'manager'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'manager'])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite))	
	    ],			
	    [
		'header' => 'Admin site',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('adminsite', $model->id);
		    if (isset($auth->roleName)) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'adminsite'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'adminsite'])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite))
	    ],
	    
	    [
		'header' => 'จัดการหน้าแรก',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('frontend', $model->id);
		    if (isset($auth->roleName)) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'frontend'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'frontend'])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite))
	    ],
	    [
		'header' => 'Active',
		'value' => function ($model) {
		    
		    if ($model->status==1) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model->id])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model->id])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite))
	    ],
            //'email:email',
//            [
//                'class' => \common\grid\EnumColumn::className(),
//                'attribute' => 'status',
//                'enum' => User::getStatuses(),
//                'filter' => User::getStatuses()
//            ],
            //'created_at:datetime',
            //'logged_at:datetime',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการผู้ใช้',
                'template' => '{view} {update} {delete}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			$authSite = ($model->userProfile->sitecode==Yii::$app->user->identity->userProfile->sitecode);
			if(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite)){
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			}
                    },
                    'delete' => function ($url, $model) {
			$authSite = ($model->userProfile->sitecode==Yii::$app->user->identity->userProfile->sitecode);
			if(Yii::$app->user->can('administrator') || (Yii::$app->user->can('adminsite') && $authSite)){
			   if($model->id != Yii::$app->user->getId()){
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			    }
			}
                    },
                ],
                'contentOptions' => ['style' => 'width:190px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end() ?>
    <h3>รายชื่อผู้ใช้จากหน่วยงานอื่น</h3>
<p style="padding-top: 10px;">
    <span class="label label-primary">คำอธิบาย</span>
    <?= Yii::t('app', 'ผู้ใช้งานจากหน่วยบริการอื่นดังต่อไปนี้ สามารถเข้าใช้งานในหน่วยบริการของท่านได้.') ?>
</p>
<?php
if ($authSite = (Yii::$app->user->identity->userProfile->sitecode == Yii::$app->user->identity->userProfile->department)) {
    $panel = Html::a(" ตั้งค่าการอนุญาตผู้ใช้งานจากหน่วยบรการอื่น", [Url::to(['user-allow/index'])], ['class' => 'btn btn-success btn-sm']);
}
?>
<?= \appxq\sdii\widgets\GridView::widget([
	'id' => 'user-allow-grid',
	'panelBtn' => $panel,
	'dataProvider' => $userallowdata,
	//'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            [
                'header' => 'ชื่อ - สกุล',
                'value' => function ($model) {
                    $user = \backend\models\UserProfile::find()->where(['user_id'=>$model->user_id])->one();
                    return $user->firstname . " " . $user->lastname;
                }
            ],
            [
                'header' => 'สังกัด',
                'value' => function ($model) {
                    $user = \backend\models\UserProfile::find()->where(['user_id'=>$model->user_id])->one();
                    $hospital = app\modules\adddoctor\models\AllHospitalThai::find()->where(['hcode'=>$user->department])->one();
                    return $user->department . " " . $hospital->name;
                }
            ],    
            [
                'header' => 'เข้ามาล่าสุด',
                'value' => function ($model) {
                    return null;
                }
            ],                        

        ],
    ]); 
?>
</div>

<?php  $this->registerJs("

$('#user-grid-pjax').on('click', '.manager-btn', function(){
    updateAttr($(this).attr('data-url'));
});

function updateAttr(url) {
    $.post(
	url
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    $.pjax.reload({container:'#user-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    }).fail(function(){
	console.log('server error');
    });
}
");?>