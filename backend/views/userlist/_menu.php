<?php
use yii\helpers\Url;

/**
 * _menu file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 18 ธ.ค. 2559 14:10:15
 * @link http://www.appxq.com/
 */
?>

<ul class="nav nav-tabs">
    <li role="presentation" class="active"><a href="<?=  Url::to(['userlist/index'])?>">บุคลากร</a></li>
  <li role="presentation"><a href="<?=  Url::to(['userlist/volunteer'])?>">อาสามาสมัคร</a></li>
  <li role="presentation"><a href="#">ประชาชน</a></li>
</ul>