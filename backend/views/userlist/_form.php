<?php

use common\models\User;
use common\models\UserProfile;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\UserForm */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $roles yii\rbac\Role[] */
/* @var $permissions yii\rbac\Permission[] */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-3 ">
		    <?php echo $form->field($modelProfile, 'picture')->widget(\trntv\filekit\widget\Upload::classname(), [
			'url' => ['/sign-in/avatar-upload']
		    ]) ?>
		</div>
	    <div class="col-md-9 sdbox-col">
		
		<?php echo $form->field($model, 'username') ?>
        <?php echo $form->field($model, 'email') ?>
        <?php echo $form->field($model, 'password')->passwordInput() ?>
        <?php echo $form->field($model, 'status')->label(Yii::t('backend', 'Active'))->checkbox() ?>
    
	<div class="box box-solid box-success " id="personalForm2" >
    <div class="box-header">
        ชื่อและข้อมูลการติดต่อ
    </div>
    <br>
    <div class="box-body">
        <div class='form-horizontal'>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label">คำนำหน้าชื่อ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'title')->dropDownList(['0'=>'นาย','1'=>'นาง','2'=>'นางสาว'])->label(false)
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> ชื่อ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'firstname')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> นามสกุล</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'lastname')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label">เพศ</label>
                <div class="col-sm-8">
                    <?php
                    echo $form->field($modelProfile,'gender')->radioList(['1'=>'ชาย','2'=>'หญิง'])->label(false)
                    ?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> อีเมล์</label>
                <div class="col-sm-8">
                    <?php 
			echo $form->field($modelProfile,'email')->textInput()->label(false);
//			echo $form->field($modelProfile,'email')->widget(MaskedInput::className(),[
//                        'clientOptions'=>[
//                            'alias'=>'email'
//                        ]
//                    ])->label(false);
			?>
                </div>
            </div>
            <div class='form-group'>
                <label for="inputEmail3" class="col-sm-2 control-label"><code>*</code> เบอร์โทรศัพท์</label>
                <div class="col-sm-8">
                    <?php echo $form->field($modelProfile,'telephone')->widget(MaskedInput::className(),[
                        'mask'=>'9999999999'
                    ])->label(false);
                    ?>
                </div>
            </div>
	    
        </div>

    </div>
    
    
        <!-- <div class="box box-footer"  >
            <button type="button" id='btnForm2'  style="float:right;" class="btn btn-success">ถัดไป</button>
        </div> -->
</div>
    
    <div class="box box-solid box-success  " id="personalForm3" >
    <div class="box-header">
        เลือกบทบาท
    </div>
    <br>
        <div class="box-body">
            <?php
	    echo $form->field($modelProfile, 'inout')->radioList(['หน่วยงานบริการสุขภาพ', 'หน่วยงานอื่นๆ'])->label(FALSE);
		  
	    ?>
            <div class='form-group <?=(strlen($modelProfile->sitecode)<10)?'has-error':''?>' id="rowDepartment" >
                  <label><code>*</code> เลือกหน่วยงาน</label>
                  <?php
		  echo Html::activeHiddenInput($modelProfile, 'cid');
		  echo Html::activeHiddenInput($modelProfile, 'department');
		  echo yii\jui\AutoComplete::widget([
		    'model' =>$modelProfile,
		    'attribute' => 'sitecode',
		    'clientOptions' => [
			'minChars' => 1,
			'source' => new JsExpression("function(request, response) {
			    $.getJSON('".yii\helpers\Url::to(['/user/querys'])."', {
				q: request.term
			    }, response);
			}"),
			'select' => new JsExpression("function( event, ui ) {
			    $('#userprofile-department').val(ui.item.id);
			 }"),
			'change' => new JsExpression("function( event, ui ) {
			    if (!ui.item) {
				$('#userprofile-department').val('');
			    }
			 }"),
			],
		     'options'=>['class'=>'form-control', 'disabled'=>true] 
		]);
		  
		  ?>
		  <p class="help-block">กรุณาค้นด้วยเลขห้าหลัก หรือส่วนหนึ่งของชื่อแล้วคลิกเลือกรายการที่ปรากฏ</p>
            </div>
            <div class="form-group">
                <label>เลือกบทบาท (สามารถเลือกได้มากกว่าหนึ่งบทบาท)</label>
                <div class="checkbox">
		    <?=  Html::checkbox('tmpStatus', true, ['label'=>'ผู้รับบริการหรือบุคคลทั่วไป'])?>
                    
                </div>
               
            </div>
	    <?= $form->field($modelProfile,'status')->checkbox(array('label'=>'บุคลากร'))->label(false);?>
            <div id="status-personal-form" style="<?=($modelProfile->status=='0')?'display:none;':''?>">
            <div class="row" id="rowPersonal">
                <div class="col-lg-6">
                    <label>เลือกบทบาทเฉพาะสังกัด</label>
                    <?php
                    echo $form->field($modelProfile,'status_personal')->dropDownList(
                        [
                            ''=>'-- เลือกบทบาท --',
                            '2'=>'ผู้ดูแลระบบฐานข้อมูล',
                            '3'=>'ผู้ให้บริการด้านการแพทย์และสาธารณสุข',
                            '4'=>'ผู้บริหาร',
                            '10'=>'นักวิจัย',
                            '11'=>'อื่นๆ ',

                        ])->label(false)
                    ?>
                </div>
                <div class="col-lg-6" id="rowManager" style="<?=($modelProfile->status_personal=='4')?'':'display:none;'?>">
                    <label>เลือกระดับ</label>
                    <?php
                      echo $form->field($modelProfile,'status_manager')->dropDownList([
                              ''=>'-- เลือกระดับหน่วยงาน --',
                              '5'=>'หน่วยงานระดับประเทศ',
                              '6'=>'หน่วยงานระดับเขต',
                              '7'=>'หน่วยงานระดับจังหวัด',
                              '8'=>'หน่วยงานระดับอำเภอ',
                              '9'=>'หน่วยงานทางการแพทย์และสาธารณสุข'
                              ])->label(false);
                    ?>
                </div>

                <div class="col-lg-6" id="rowStatusOther" style="<?=($modelProfile->status_personal=='11')?'':'display:none;'?>">
                    <label>บทบาทอื่นๆ</label>
                    <?php
                    echo $form->field($modelProfile,'status_other')->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowNation" style="<?=($modelProfile->status_manager=='5')?'':'display:none;'?>">
                <div class="col-lg-12">
                    <label>ชื่อหน่วยงานระดับประเทศ</label>
                    <?php
                    echo $form->field($modelProfile,'department_area_text')->textInput(['disabled'=>$modelProfile->status_manager!='5'])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowArea" style="<?=($modelProfile->status_manager=='6')?'':'display:none;'?>">
                <div class="col-lg-6">
                    <label>หน่วยงานระดับเขต</label>
                    <?php
                    echo $form->field($modelProfile,'department_area')->dropDownList([''=>'-- เลือกเขตบริการ --',
                        '1'=>'เขตบริการที่ 1','2'=>'เขตบริการที่ 2','3'=>'เขตบริการที่ 3','4'=>'เขตบริการที่ 4','5'=>'เขตบริการที่ 5','6'=>'เขตบริการที่ 6','7'=>'เขตบริการที่ 7',
                        '8'=>'เขตบริการที่ 8','9'=>'เขตบริการที่ 9','10'=>'เขตบริการที่ 10','11'=>'เขตบริการที่ 12','13'=>'เขตบริการที่ 13','14'=>'เขตบริการที่ 14'
                    ])->label(false)
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงานระดับเขต</label>
                    <?php
                    echo $form->field($modelProfile,'department_area_text')->textInput(['disabled'=>$modelProfile->status_manager!='6'])->label(false);
                    ?>
                </div>
            </div>
            <div class="row" id="rowProvince" style="<?=($modelProfile->status_manager=='7')?'':'display:none;'?>">
                <div class="col-lg-6">
                    <label>หน่วยงานระดับจังหวัด</label>
                    <?php
                    echo Select2::widget([
                        'options' => ['placeholder' => 'จังหวัด','id'=>'province','value'=>'test'],
                        'data' => ArrayHelper::map($dataProvince,'PROVINCE_CODE','PROVINCE_NAME'),
                        'model' =>$modelProfile,
                        'attribute'=>'department_province',
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงาน</label>
                    <?php
                        echo $form->field($modelProfile,'department_province_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
            <div class="row"  id="rowAmphur" style="<?=($modelProfile->status_manager=='8')?'':'display:none;'?>">
                <div class="col-lg-6"  >
                    <label>หน่วยงานระดับอำเภอ</label>
                    <?php
                    echo $form->field($modelProfile,'department_amphur')->widget(DepDrop::classname(),[
                        'type'=>  DepDrop::TYPE_SELECT2,
                        'options'=>['id'=>'amphur'],
                        'model'=>$modelProfile,
                        'attribute'=>'department_amphur',
                        'pluginOptions'=>[
                            'depends'=>['province'],
                            'placeholder'=>'อำเภอ',
                            'url'=>'/user/genamphur'
                        ]
                    ])->label(false);
                    ?>
                </div>
                <div class="col-lg-6">
                    <label>ชื่อหน่วยงานระดับอำเภอ</label>
                    <?php
                    echo $form->field($modelProfile,'department_amphur_text')->textInput()->label(false);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="box box-footer"  >
        <button type="button" id="btnForm3" class="btn btn-success" style="float:right;">ถัดไป</button>
    </div> -->
</div>
		
        <?php 
	    
	    $arrRoles = [];
	    foreach ($model->roles as $value) {
		$arrRoles[] = $roles[$value];
	    }
	    
	    echo '<label class="control-label" for="userform-roles">Roles</label>';
	    echo '<div class="form-group">'.Html::textInput('roles', implode(', ', $arrRoles), ['class'=>'form-control', 'readonly'=>true]).'</div>';
	    echo Html::activeHiddenInput($model,'roles');
	?>
	
	<div class='form-group'>
	    <label for="enroll_key" class="control-label"> Enroll key</label>
		<?php
		echo $form->field($modelProfile,'enroll_key')->textInput(['readonly'=>TRUE])->label(false);
		?>
	</div>	
    <label class="control-label" for="userprofile-secret_file">เอกสารรักษาความลับ</label>
    <?php
		$secret_file = '#';
		if($modelProfile->secret_file!=''){
		    $secret_file = Yii::getAlias('@storageUrl') . '/source/'.$modelProfile->secret_file;
		    $secret_file_old = $secret_file;
		    $ext = strtolower(pathinfo($modelProfile->secret_file, PATHINFO_EXTENSION));
		    if($ext=='pdf'){
			$secret_file = Yii::getAlias('@storageUrl').'/source/pdf_icon.png';
		    }
		?>
    <div class=""><a class="file-preview-image" data-filename="<?=$secret_file_old?>"><img src="<?=$secret_file?>"  height="100" ></a></div>
		<?php } ?>
		<br>
		<label class="control-label" for="userprofile-citizenid_file">สำเนาบัตรประชาชน</label>
		<?php
		$citizenid_file = '#';
		if($modelProfile->citizenid_file!=''){
		    $citizenid_file = Yii::getAlias('@storageUrl') . '/source/'.$modelProfile->citizenid_file;
		    $citizenid_file_old = $citizenid_file;
		    $ext = strtolower(pathinfo($modelProfile->citizenid_file, PATHINFO_EXTENSION));
		    if($ext=='pdf'){
			$citizenid_file = Yii::getAlias('@storageUrl').'/source/pdf_icon.png';
		    }
		?>
		<div class=""> <a class="file-preview-image" data-filename="<?=$citizenid_file_old?>"><img src="<?=$citizenid_file?>" height="100" ></a></div>
		<?php } ?>
		<br>
		<?php
	    echo Html::checkbox('reset_file', false, ['label'=>'เอกสารไม่ถูกต้อง คลิกเพื่อให้ผู้ใช้อัพโหลดเอกสารใหม่']);
		?>
        <div class="form-group">
            <?php echo Html::submitButton(Yii::t('backend', 'Save'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
	    </div>
	</div>
	
        
    <?php ActiveForm::end(); ?>

</div>
<?=	    common\lib\sdii\widgets\SDModalForm::widget([
		'id' => 'modal-move-units',
		'size'=>'modal-lg',
		'tabindexEnable' => false,
	    ]);
	    ?>
<?php
$this->registerJs(
    "	    $('body').tooltip('destroy');

            $('input:checkbox[name=\"UserProfile[status]\"]').on('click',function(){
                if($(this).is(':checked')){
                    $('#status-personal-form').show();  // checked
                } else {
                    $('#status-personal-form').hide();
		    $('#userprofile-status_personal').val('');
		    $('#userprofile-status_personal').trigger('change');
		}
            });

            $('#buttonCid').on('click',function(){
                var cid = $('#puser-cid').val();

                    $('#form2').attr('class','label label-success');
                    $('#personalForm2').attr('class','box box-solid box-success');
                    $('#hideCidForm').show();

            });

            $('#btnForm2').on('click',function(){
                    $('#form3').attr('class','label label-success');
                    $('#personalForm3').attr('class','box box-solid box-success');
            });

            $('#userprofile-status_personal').on('change',function(){
                var type = $(this).val();
                if(type==4){
		    $('#rowManager').show();
                    $('#rowStatusOther').hide();
                }else if(type==11){
                    $('#rowStatusOther').show();
		    $('#userprofile-status_manager').val('');
		    $('#userprofile-status_manager').trigger('change');
                    $('#rowManager').hide();
                }else{
                    $('#rowStatusOther').hide();
		    $('#userprofile-status_manager').val('');
		    $('#userprofile-status_manager').trigger('change');
                    $('#rowManager').hide();
                }
            });
            $('#userprofile-status_manager').on('change',function(){
                var type = $(this).val();

                if(type==5){
                  $('#rowNation').show();
                  $('#rowProvince').hide();
                  $('#rowArea').hide();
                  $('#rowAmphur').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', false);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', true);
                }else if(type==6){
                  $('#rowArea').show();
                  $('#rowNation').hide();
                  $('#rowProvince').hide();
                  $('#rowAmphur').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', true);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', false);
                }
                else if(type==7){
                  $('#rowProvince').show();
                  $('#rowNation').hide();
                  $('#rowArea').hide();
                  $('#rowAmphur').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', true);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', true);
                }else if(type==8){
                  $('#rowProvince').show();
                  $('#rowAmphur').show();
                  $('#rowNation').hide();
                  $('#rowArea').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', true);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', true);
                }else{
                  $('#rowArea').hide();
                  $('#rowNation').hide();
                  $('#rowProvince').hide();
                  $('#rowAmphur').hide();
		  $('#rowNation #userprofile-department_area_text').attr('disabled', true);
		  $('#rowArea #userprofile-department_area_text').attr('disabled', true);
                }
            });
            
            $('#btnAddMoreHospital').on('click',function(){
              var nameHospital = $('#addMoreHospital').val();
              if(nameHospital==''){
                $('#rowMoreHospital').attr('class','form-group has-error');
                $('.hospital-error').html('กรุณาระบุ');
              }else{
                $('#rowMoreHospital').attr('class','form-group');
                $('.hospital-error').html('');
                // $('#rowMoreHospital').hide();
                $.post('/user/formquery/addmorehospital',{nameHospital:nameHospital},function(result){
                    console.log(result);
                });
              }
            });
	    
	    $('.file-preview-image').on('click',function(){
		
		var filename = $(this).attr('data-filename');
                    modalMoveUnit('" . Url::to(['/sign-in/viewimg', 'img' => '']) . "'+filename);
	    });
	       
	       function modalMoveUnit(url) {
		    $('#modal-move-units .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
		    $('#modal-move-units').modal('show')
		    .find('.modal-content')
		    .load(url);
		}
    "
);
?>