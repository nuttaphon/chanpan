<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model frontend\modules\volunteer\models\VolunteerJob */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="volunteer-job-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">อนุมัติอาสาสมัคร</h4>
    </div>

    <div class="modal-body">
	
<?= $form->field($model, 'job_cid')->textInput(['maxlength' => true]) ?>
			    <?= $form->field($model, 'job_fname')->textInput(['maxlength' => true]) ?>
			    <?= $form->field($model, 'job_lname')->textInput(['maxlength' => true]) ?>
	
	<?php
		if($model->job_type==1){
		    echo $form->field($model, 'job_parent')->widget(kartik\select2\Select2::className(),[
				//'initValueText' => $model->job_parent, // set the initial display text
				'options' => ['placeholder' => 'ค้นหา อสม.'],
				'pluginOptions' => [
					'allowClear' => true,
					'minimumInputLength' => 0,
					'ajax' => [
					    'url' => \yii\helpers\Url::to(['/volunteer/volunteer-job/author-list']),
					    'dataType' => 'json',
					    'data' => new JsExpression('function(params) { return {q:params.term}; }')
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(author) { return author.text; }'),
					'templateSelection' => new JsExpression('function (author) { return author.text; }'),
				],
			    ]) ;
		} else {
		    echo $form->field($model, 'job_parent')->hiddenInput()->label(false);
		    
		}
		
		?>
		

	
	<?= $form->field($model, 'job_id')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'job_start')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'job_end')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'job_type')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'sitecode')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'job_status')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'job_active')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'created_by')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'updated_by')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton('อนุมัติ', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    $(document).find('#modal-volunteer-job').modal('hide');
		$.pjax.reload({container:'#volunteer-job-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>