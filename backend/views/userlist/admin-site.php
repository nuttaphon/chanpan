<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index" >

    <p>
        <?php echo Html::a(Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'User',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(['id'=>'user-grid-pjax']) ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
	    'class' => 'yii\grid\SerialColumn',
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'width:50px;text-align: center;'],
	    ],
	    'username',
            'userProfile.firstname',
	    'userProfile.lastname',
	    [
		'header' => 'บทบาท',
		'value' => function ($model) {
		    if($model->userProfile->status==0){
			return $model->userProfile->status;
		    } else {
			if($model->userProfile->status==4){
			    return $model->userProfile->status_manager;
			}
		    }
		    return $model->userProfile->status_personal;
		},
		//'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:280px;'],
	    ],
	    [
		'header' => 'Admin site',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('adminsite', $model->id);
		    if (isset($auth->roleName)) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'adminsite'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'adminsite'])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:90px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))
	    ],
	    [
		'header' => 'เจ้าหน้าที่',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('manager', $model->id);
		    if (isset($auth->roleName)) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'manager'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'manager'])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))
	    ],
	    [
		'header' => 'จัดการหน้าแรก',
		'value' => function ($model) {
		    $auth = Yii::$app->authManager->getAssignment('frontend', $model->id);
		    if (isset($auth->roleName)) {
			return Html::button('<i class="glyphicon glyphicon-ok"></i>', [
			    'class' => 'manager-btn btn btn-xs btn-primary',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'frontend'])
			]);
		    } else {
			return Html::button('<i class="glyphicon " style="padding-right: 6px; padding-left: 6px;"></i>',[
			    'class' => 'manager-btn btn btn-xs btn-default',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['manager', 'id' => $model->id, 'auth'=>'frontend'])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))
	    ],
	    [
		'header' => 'Active',
		'value' => function ($model) {
		    
		    if ($model->status==1) {
			return Html::button('Active', [
			    'class' => 'manager-btn btn btn-xs btn-success btn-block',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model->id])
			]);
		    } else {
			return Html::button('Disabled',[
			    'class' => 'manager-btn btn btn-xs btn-danger btn-block',
			    'data-id' => $model->id,
			    'data-url' => yii\helpers\Url::to(['status', 'id' => $model->id])
			]);
		    }
		},
		'format' => 'raw',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px;text-align: center;'],
		'visible'=>(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite'))
	    ],
            //'email:email',
//            [
//                'class' => \common\grid\EnumColumn::className(),
//                'attribute' => 'status',
//                'enum' => User::getStatuses(),
//                'filter' => User::getStatuses()
//            ],
            //'created_at:datetime',
            //'logged_at:datetime',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการผู้ใช้',
                'template' => '{view} {update} {delete}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) {
			
			    return Html::a('<span class="fa fa-eye"></span> View', $url, [
                                    'title' => Yii::t('app', 'View'),
                                    'class'=>'btn btn-info btn-xs',
			    ]);
			
                    },
                    'update' => function ($url, $model) {
			if(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')){
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-warning btn-xs',
			    ]);
			}
                    },
                    'delete' => function ($url, $model) {
			if(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite')){
			   return Html::a('<span class="fa fa-trash"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',                                    
			    ]); 
			}
                    },
                ],
                'contentOptions' => ['style' => 'width:200px;']                
            ],
        ],
    ]); ?>
<?php Pjax::end() ?>
</div>

<?php  $this->registerJs("

$('#user-grid-pjax').on('click', '.manager-btn', function(){
    updateAttr($(this).attr('data-url'));
});

function updateAttr(url) {
    $.post(
	url
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    $.pjax.reload({container:'#user-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    }).fail(function(){
	console.log('server error');
    });
}
");?>