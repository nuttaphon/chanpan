<?php
/**
 * Created by PhpStorm.
 * User: gundamx
 * Date: 11/27/2015 AD
 * Time: 2:02 PM
 */

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\QueryRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Query Submission';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="query-request-index">

    <?=
    \yii\widgets\Breadcrumbs::widget([
        'homeLink' => [
            'label' => Yii::t('yii', 'Home'),
            'url' => Yii::$app->homeUrl,
        ],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])
    ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?php //echo Html::a('< Back', Yii::$app->request->referrer, ['class' => 'btn btn-success'])
        echo Html::a('<span class="fa fa-list"></span> '.'CASCAP ICF', '?ezf_id=all&type=2', ['class' => 'btn btn-success']);
        echo ' '. Html::a('<span class="fa fa-list"></span> '.'Show All', '?ezf_id=all&type=1', ['class' => 'btn btn-success']);
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'ezf_id',
            [
                'label' =>"ชื่อฟอร์ม",
                'attribute' => 'ezf_id',
                'value'=>function($data){
                    return $data["ezf_name"];
                }
            ],
            [
                'label' =>"Waiting",
                'attribute' => 'status_waiting',
                'value'=>function($data){
                    return $data["status_waiting"];
                }
            ],
            [
                'label' =>"สิทธิ์",
                'attribute' => 'positions',
                'value'=>function($data){
                    return $data["positions"];
                }
            ],
            [
                'label' =>"Action",
                'format'=>'html',
                'value'=>function($data){
                    return Html::a(Html::encode("View"), ['view-by-form', 'ezf_id' => $data['ezf_id'],'type' => 1], ['class' => 'btn btn-primary', 'title' => 'แสดงรายการ Query ของฟอร์มนี้']);
                }
            ],
            //'positions',
            //'data_id',
            //'ezf_field_id',
            // 'new_val:ntext',
            // 'note:ntext',
            //'status',
            //'user_create',
            //'status',
            // 'user_update',
            //'time_create:date',
            // 'time_update',
            // 'approved_by',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>