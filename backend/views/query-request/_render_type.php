<?php
/**
 * Created by PhpStorm.
 * User: gundamx
 * Date: 12/18/2015 AD
 * Time: 1:41 PM
 */
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\EzformChoice;
use yii\helpers\ArrayHelper;
use backend\modules\ezforms\components\EzformWidget;
use common\lib\damasac\widgets\DMSDateWidget;
use common\lib\damasac\widgets\DMSTimeWidget;
use common\lib\damasac\widgets\DMSDatetimeWidget;
use yii\bootstrap\Html;
?>


<?php
$html = \yii\bootstrap\Html::label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')', 'queryrequest-new_val', ['class'=>"control-label", ]);
//
if($fields['ezf_field_type']==0 AND $fields['ezf_field_sub_id'] != ''){
    $EzformFields = EzformFields::find()->select('ezf_field_type, ezf_field_id, ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_sub_id']])->one();
    if($EzformFields->ezf_field_type==19){
        //$html = $form->field($modelForm, 'new_val')->checkbox()->label('ค่าใหม่ที่ต้องการแก้ไข <br>(' . $fields['ezf_field_label'] . ')');
        //$html .= Html::checkboxList($fieldName, $modelForm->new_val ? true : false, ['class' => 'form-control']);
        $html .= '<br>'.Html::activeCheckbox($modelForm, 'new_val', ['label' =>false]);
    }
}else if($fields['ezf_field_type']==1 || $fields['ezf_field_type']==251){
    //$html = $form->field($modelForm, 'new_val')->textInput()->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
    $html .= '<br>'.Html::activeTextInput($modelForm, 'new_val', ['label' =>false, 'class'=>'form-control']);
}else if($fields['ezf_field_type']==3 || $fields['ezf_field_type']==252){
    //$html = $form->field($modelForm, 'new_val')->textarea()->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
    $html .= '<br>'.Html::activeTextarea($modelForm, 'new_val', ['label' =>false, 'class'=>'form-control']);
}else if($fields['ezf_field_type']==4){
    $ezformChoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_id']])->all();
    $items = ArrayHelper::map($ezformChoice, 'ezf_choicevalue', 'ezf_choicelabel');
    //VarDumper::dump($ezformChoice, 10, true);
    //Yii::$app->end();
    //$html = $form->field($modelForm, 'new_val')->radioList($items)->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
    $html .= Html::activeRadioList($modelForm, 'new_val', $items);
}else if($fields['ezf_field_type']==6){
    $ezformChoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_id']])->all();
    $items = ArrayHelper::map($ezformChoice, 'ezf_choicevalue', 'ezf_choicelabel');
    //$html = $form->field($modelForm, 'new_val')->dropDownList($items)->label('ค่าใหม่ที่ต้องการแก้ไข <br>('.$fields['ezf_field_label'].')');
    $html .= Html::activeDropDownList($modelForm, 'new_val', $items, ['class'=>'form-control']);
}else if($fields['ezf_field_type']==7 || $fields['ezf_field_type']==253){
    $date = $res['text'];
    if($date) {
        $explodeDate = explode('-', $date);
        $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
        $modelForm->new_val = $formateDate;
    }
    //$html = $form->field($modelForm, 'new_val')->widget(DMSDateWidget::className(), [])->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
    $html .= DMSDateWidget::widget(['name' => $fieldName, 'value'=>$modelForm->new_val]);
}else if($fields['ezf_field_type']==8){
    //$html = $form->field($modelForm, 'new_val')->widget(DMSTimeWidget::classname(), [])->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
    $html .= DMSTimeWidget::widget(['name' => $fieldName, 'value'=>$modelForm->new_val]);
}else if($fields['ezf_field_type']==9){
    //$html = $form->field($modelForm, 'new_val')->widget(DMSDatetimeWidget::classname(), [])->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
    $html .= DMSDatetimeWidget::widget(['name' => $fieldName, 'value'=>$modelForm->new_val]);
}else if($fields['ezf_field_type']==10){
    $text=[];
    $text['ezf_field_label'] = 'ค่าใหม่ที่ต้องการแก้ไข ';
    $text["ezf_field_help"] = '('.$fields['ezf_field_label'].')';
    //$fields = (object)$fields;
    //\yii\helpers\VarDumper::dump($field->ezf_component, 10, true);
    //Yii::$app->end();
    //$html .= EzformWidget::componentValidate($form, $modelForm, 'new_val', $fields, null, $text);
    //$html = $form->field($modelForm, 'new_val')->widget(Select2::className(), [])->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
}else if($fields['ezf_field_type']==11){
    $text=[];
    $text['ezf_field_label'] = 'ค่าใหม่ที่ต้องการแก้ไข ';
    $text["ezf_field_help"] = '('.$fields['ezf_field_label'].')';
    //$fields = (object)$fields;
    //VarDumper::dump($fields, 10, true);
    //Yii::$app->end();
    //$html .= EzformWidget::snomedValidate($form, $modelForm, 'new_val', $fields, null, $text);
    //$html = $form->field($modelForm, 'new_val')->widget(Select2::className(), [])->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
}else if($fields['ezf_field_type']==12){
    //personal ID
    //$html = $form->field($modelForm, 'new_val')->textInput()->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
    $html .= '<br>'.Html::activeTextInput($modelForm, 'new_val', ['label' =>false, 'class'=>'form-control']);
}else if($fields['ezf_field_type']==21){
    //Province, amphur, tombon
    if($fields['ezf_field_label']==1) {
        //$ConstProvince = ConstProvince::find()->where('PROVINCE_CODE = :code', [':code'=>$modelForm->new_val])->all();
        //$html = $form->field($modelForm, 'new_val')->dropDownList($items)->label('ค่าใหม่ที่ต้องการแก้ไข (จังหวัด)');
    }
}else if($fields['ezf_field_type']==14){
    $text=[];
    $text['ezf_field_label'] = 'ค่าใหม่ที่ต้องการแก้ไข ';
    $text["ezf_field_help"] = '('.$fields['ezf_field_label'].')';
    $fields = (object)$fields;
    //VarDumper::dump($fields, 10, true);
    //Yii::$app->end();
    $html = EzformWidget::fileinputValidate($form, $modelForm, 'new_val', $fields, null, $text);
}else if($fields['ezf_field_type']==16 || $fields['ezf_field_type']==254){
    //$html = $form->field($modelForm, 'new_val')->checkbox()->label('ค่าใหม่ที่ต้องการแก้ไข <br>('.$fields['ezf_field_label'].')');
}else if($fields['ezf_field_type']==17){
    $text=[];
    $text['ezf_field_label'] = 'ค่าใหม่ที่ต้องการแก้ไข ';
    $text["ezf_field_help"] = '('.$fields['ezf_field_label'].')';
    $fields = (object)$fields;
    //VarDumper::dump($fields, 10, true);
    //Yii::$app->end();
    $html = EzformWidget::activeHospitalSave($form, $modelForm, 'new_val', $fields, null, $text);
}else if($fields['ezf_field_type']==231){
    //scale
    $ezformChoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_sub_id']])->all();
    $items = ArrayHelper::map($ezformChoice, 'ezf_choicevalue', 'ezf_choicelabel');
    //$html = $form->field($modelForm, 'new_val')->radioList($items)->label('ค่าใหม่ที่ต้องการแก้ไข <br>('.$fields['ezf_field_label'].')');
}else {
    //$html = $form->field($modelForm, 'new_val')->textInput()->label('ค่าใหม่ที่ต้องการแก้ไข ('.$fields['ezf_field_label'].')');
}
    echo $html;