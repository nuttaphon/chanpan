<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use common\lib\damasac\assets\date\DMSDateAsset;
//use kartik\select2\Select2;
//use kartik\select2\Select2Asset;
/* @var $this yii\web\View */
/* @var $model backend\models\QueryRequest */
/* @var $form yii\widgets\ActiveForm */
//DMSDateAsset::register($this);
//Select2Asset::register($this);
?>
<style>
    .datepicker{
        z-index: 3000 !important;
    }
</style>

<div class="query-request-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName(), 'action' => ['create'], 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h1 class="modal-title" id="itemModalLabel">Query change request</h1>
    </div>

    <?php
    ?>
    <div class="modal-body">
    <?php //Html::textInput('queryrequest-id', '', ['hidden'=>true]); ?>
    <?php echo $form->field($model, 'ezf_id')->textInput(['maxlength' => true,])->hiddenInput(['value' => Yii::$app->request->get('ezf_id')])->label(false); ?>

    <?php echo $form->field($model, 'target_id')->textInput(['maxlength' => true,])->hiddenInput(['value' => $modelValue->target_id])->label(false); ?>

    <?php echo $form->field($model, 'data_id')->textInput(['maxlength' => true,])->hiddenInput(['value' => Yii::$app->request->get('data_id')])->label(false); ?>
    <?php echo $form->field($model, 'xsourcex')->textInput(['maxlength' => true,])->hiddenInput(['value' =>Yii::$app->user->identity->userProfile->sitecode])->label(false); ?>

    <?php echo Html::hiddenInput('ezf_field_type', $ezf_field->ezf_field_type, []);?>
    <?php echo Html::hiddenInput('ezf_field_name', $ezf_field->ezf_field_name, []);?>

    <?//= $form->field($model, 'ezf_field_id')->dropDownList($ezf_field, ['prompt'=>'Select Field...',])->label('เลือกข้อคำถามที่ต้องการแก้ไข') ?>

    <?php $model->old_val = json_encode($modeQuery->attributes);  echo $form->field($model, 'old_val')->textarea(['style'=>"display:none;"])->label(false);?>

<!--
    <div id="div-list-ezf-field" style="display:<?php // echo !$model->isNewRecord ? '' : 'none'; ?>;">
        <div style="padding: 10px;" class="alert-success" id="list-ezf-field"></div><br>
    </div>

    <div id="queryrequest-label-note" style="display: <?php // echo !$model->isNewRecord ? '' : 'none'; ?>;">
    <?php //echo $form->field($model, 'new_val')->textarea(['rows' => 6, 'disabled' => true])->hiddenInput()->label(false); ?>

    <?//= $form->field($model, 'note')->textarea(['rows' => 6, ])->label('สาเหตุที่ต้องการแก้ไข') ?>
    </div>
-->
    <?php echo Html::label('ค่าใหม่ที่ต้องการแก้ไข') ?>
    <?php
    //\yii\helpers\VarDumper::dump($options_input);
    if($ezf_field->ezf_field_type != 14 && $ezf_field->ezf_field_type != 13)
    echo backend\modules\ezforms\components\EzformFunc::getTypeEform($modeQuery, $ezf_field, $form, $options_input);
    echo $form->field($model, 'ezf_field_id')->textInput()->hiddenInput(['value' => Yii::$app->request->get('ezf_field_id')])->label(false);
    //echo \backend\modules\ezforms\components\EzformWidget::activeDateInputSave($form, $modeQuery, 'new_val', $ezf_field, null);
     ?>

    <?php echo $form->field($model, 'note')->textarea(['rows' => 6, ])->label('สาเหตุที่ต้องการแก้ไข') ?>

    <?php
    if($queryManager->user_id || Yii::$app->user->can('adminsite')) {
        echo Html::label('ต้องการ Query ถึง...');
        echo \kartik\widgets\Select2::widget([
            'id' => 'queryUserTo',
            'name' => 'queryUserTo',
            'value' => [$modelValue->user_create], // initial value
            'data' => $userTo,
            'options' => ['placeholder' => 'Select user ...', 'multiple' => false],
            'pluginOptions' => [
                'allowClear' => true
                //'maximumInputLength' => 10
            ],
        ]);
    }
    ?>

    <?//= $form->field($model, 'user_create')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'user_update')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'time_create')->textInput() ?>

    <?//= $form->field($model, 'time_update')->textInput() ?>

    <?//= $form->field($model, 'approved_by')->textInput(['maxlength' => true, 'value' => $modelValue->user_create]) ?>

    <?php echo $form->field($model, 'userkey')->textInput(['maxlength' => true,])->hiddenInput(['value' => $modelValue->user_create])->label(false); ?>
    </div>

    <div class="modal-footer">
        <div class="form-group">
            <?php echo Html::submitButton($model->isNewRecord ? 'Edit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary btn-lg',]) ?>
        </div>
    </div>

    <?php ActiveForm::end();
    $this->registerJs("
    function getDataFromTable(ezf_field_id){

        var data_id = '".$model->data_id."';
        var url = '".\yii\helpers\Url::to(['/query-request/find-data-model',])."';
        $.ajax({
            type: 'POST',
            url: url,
            data: {data_id: data_id, ezf_field_id : ezf_field_id},
            dataType: 'json',
            success: function(data) {
                if(data.note || data.new_val){
                    //update
                    $(\"#queryrequest-note\").val(data.note);
                    //$(\"#queryrequest-new_val\").val(data.new_val);
                    $(\"[name='queryrequest-id']\").val(data.id);

                    url = '".\yii\helpers\Url::to(['/query-request/select-data-field',])."';
                    //var htmlx ='';
                    var query_id = data.id;
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {query_id: query_id, ezf_field_id : ezf_field_id},
                        //dataType: 'json',
                        success: function(data) {
                            // you can use data.blah, or if working with multiple rows
                            // of data, then you can use $.each()
                            //htmlx = '<h3>ข้อมูลค่าเดิม</h3>';
                            //htmlx += ''+data.text;
                            //htmlx += '<hr>';
                            $(\"#list-ezf-field\").html(data);
                            $(\"#div-list-ezf-field\").fadeIn();
                            $(\"#queryrequest-label-note\").fadeIn();
                            $(\".field-queryrequest-old_val\").html('');
                        }
                    });
                }else{
                    //create
                    url = '".\yii\helpers\Url::to(['/query-request/select-data-field',])."';
                    //var htmlx ='';
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {data_id: data_id, ezf_field_id : ezf_field_id},
                        //dataType: 'json',
                        success: function(data) {
                            // you can use data.blah, or if working with multiple rows
                            // of data, then you can use $.each()

                            //htmlx = '<h3>ข้อมูลค่าเดิม</h3>';
                            //htmlx += ''+data.text;
                            //htmlx += '<hr>';
                            //htmlx = data;
                            $(\"#list-ezf-field\").html(data);

                            if(data.text=='ไม่มีค่าข้อมูล'){
                                data.text=null;
                            }
                            $(\"#queryrequest-old_val\").val($(\"#queryrequest-new_val\").val());

                            $(\"[name='queryrequest-id']\").val(null);
                            $(\"#queryrequest-note\").val('');
                            //$(\"#queryrequest-new_val\").val('');
                            $(\"#div-list-ezf-field\").fadeIn();
                            $(\"#queryrequest-label-note\").fadeIn();
                        }
                    });
                }
            }
        });

    }
    $('#".$model->formName()."').on('change', '#queryrequest-ezf_field_id', function(){
        var ezf_field_id = $(this).val();
        getDataFromTable(ezf_field_id);
    });


/*
$(function() {
    var datastring = $(\"#QueryRequest\").serialize();
    var url = '".\yii\helpers\Url::to(['/query-request/encode-json-oldval',])."';
        $.ajax({
            type: \"POST\",
            url: url,
            data: datastring,
            success: function(data) {
                 //$(\"#queryrequest-old_val\").val(data);
            }
        });
});
*/

    ");

//    if(!$model->isNewRecord){
//        $this->registerJs("
//        getDataFromTable('".$model->ezf_field_id."');
//        ");
//    }

    ?>

</div>
