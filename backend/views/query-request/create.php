<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\QueryRequest */

$this->title = 'Create Query Request';
$this->params['breadcrumbs'][] = ['label' => 'Query Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="query-request-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelValue' => $modelValue,
        'modeQuery' => $modeQuery,
        'ezf_field' => $ezf_field,
        'options_input' => $options_input,
        'queryManager' => $queryManager,
        'userTo' =>$userTo
    ]) ?>

</div>
