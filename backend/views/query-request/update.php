<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\QueryRequest */

$this->title = 'Update Query Request: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Query Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="query-request-update">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'modelValue' => $modelValue,
        'modeQuery' => $modeQuery,
        'ezf_field' => $ezf_field,
    ]) ?>

</div>
