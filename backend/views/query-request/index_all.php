<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\QueryRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->registerCss('
//.formPanel  input[type=radio] {
//     transform: scale(2, 2);
//     -moz-transform: scale(2, 2);
//     -ms-transform: scale(2, 2);
//     -webkit-transform: scale(2, 2);
//     -o-transform: scale(2, 2);
//
//     margin-right: 10px !important;
//     margin-left: 10px !important;
//
// }
//.formPanel  input[type=checkbox] {
//    transform: scale(2, 2);
//    -moz-transform: scale(2, 2);
//    -ms-transform: scale(2, 2);
//    -webkit-transform: scale(2, 2);
//    -o-transform: scale(2, 2);
//    margin-right: 10px !important;
//
//
//}
//.formPanel .radio label, .checkbox label{
//    margin-left: 10px !important;
//    padding-right: 10px !important;
//
//}');
?>
<div class="query-request-index">
    <?php
    if(Yii::$app->request->get('ezf_id')=='all') {
        $this->title = 'Query Submission';
        $this->params['breadcrumbs'][] = 'Query Submission';

    }else {
        $this->title = 'Query Submission (view by form)';
        $this->params['breadcrumbs'][] = ['label' => 'Query Submission', 'url' => ['index']];
    }
    $this->params['breadcrumbs'][] = $this->title;
    ?>
    <?=
    \yii\widgets\Breadcrumbs::widget([
        'homeLink' => [
            'label' => Yii::t('yii', 'Home'),
            'url' => Yii::$app->homeUrl,
        ],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])
    ?>

    <?php if(Yii::$app->request->get('ezf_id')!='all') {?>
    <h2> Form : <?php echo $ezform->ezf_name; ?></h2><hr>
        <div class="row">
            <div class="col-md-6">
    <?php
        $num=1;
        echo '<h3 class="text-blod">Admin กลาง</h3>';
        foreach($queryManager as $val){
            if($val->user_group == 1) {
                if ($val->status == 1) {
                    $owner .=  $val->user_id . ', ';
                } else if ($val->status == 2) {
                    $codev .= $num++.'.'.$val->user_id . ', ';
                }
            }
        }
        echo '<h4><span class="">Owner : '.substr($owner,0,-2).' (Co-dev : '.substr($codev,0,-2).')</span>';
    ?>
        <h3><span class="label label-warning">Data manager : </span></h3>
    <?php
        echo \kartik\widgets\Select2::widget([
            'id' => 'data_manager',
            'name' => 'data_manager',
            'value' => $initiUserManager, // initial value
            'disabled'=> Yii::$app->user->id == $userOwner || Yii::$app->user->can('administrator') ? false : true,
            'data' => $userDataManager,
            'options' => ['placeholder' => 'Select user ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                //'maximumInputLength' => 10
            ],
        ]);
        $this->registerJs("$('#data_manager').on('change',function(){
            var val = $(this).val();
            var ezf_id = '".Yii::$app->request->get('ezf_id')."';
            $.post('".\yii\helpers\Url::to(['/query-request/save-data-manager?action=central'])."',{val:val, ezf_id:ezf_id},function(result){
                " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . "
            });
        });");

    ?>
        </div>
            <div class="col-md-6">
        <?php
        echo '<h3 class="text-blod">Admin หน่วยบริการ (Site)</h3>';
        echo '<h4><span class="">';
        $num=1;
        foreach($userDataManagerSiteAdmin as $val){
            echo $num++.'.'.$val.', ';
        }
        echo '</span></h4>';
        ?>
        <h3><span class="label label-warning">Data manager : </span></h3>
        <?php
        echo \kartik\widgets\Select2::widget([
            'id' => 'data_manager_site',
            'name' => 'data_manager_site',
            'value' => $initiUserManagerSite, // initial value
            'disabled'=> Yii::$app->user->id == $userOwner || Yii::$app->user->can('adminsite') ? false : true,
            'data' => $userDataManager,
            'options' => ['placeholder' => 'Select user ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                //'maximumInputLength' => 10
            ],
        ]);
        $this->registerJs("$('#data_manager_site').on('change',function(){
            var val = $(this).val();
            var ezf_id = '".Yii::$app->request->get('ezf_id')."';
            $.post('".\yii\helpers\Url::to(['/query-request/save-data-manager?action=site'])."',{val:val, ezf_id:ezf_id},function(result){
                " . \common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') . "
            });
        });");

        echo '</div></div>';
        }?>


    <?php

    //
    if(Yii::$app->request->get('query_by')=='1'){
        $query_by = 1;
    }else if(Yii::$app->request->get('query_by')=='2'){
        $query_by = 2;
    }else{
        $query_by = 3;
    }
    //
    if(Yii::$app->request->get('query_status')=='-9'){
        $query_status = -9;
    }else if(Yii::$app->request->get('query_status')=='1'){
        $query_status = 1;
    }else if(Yii::$app->request->get('query_status')=='2'){
        $query_status = 2;
    }else if(Yii::$app->request->get('query_status')=='3'){
        $query_status = 3;
    }else{
        $query_status = 9;
    }

    ?>

    <?php
    echo '<hr><div class="row">
    <form action="" method="GET">
    '.Html::hiddenInput('ezf_id', Yii::$app->request->get('ezf_id'), ['id'=>'ezf_id', 'class'=>'form-control', 'placeholder'=>'ezf_id']).'
    <div class="col-md-3">'.Html::textInput('txtSearch', Yii::$app->request->get('txtSearch'), ['id'=>'txtSearch', 'class'=>'form-control', 'placeholder'=>'ค้นหาจากหน่วยบริการ']).'</div>
    <div class="col-md-3">'.Html::dropDownList('query_by', $query_by, ['3' => 'All', '1'=>'Query by me', '2'=>'Query to me'], ['class'=> 'form-control', 'id' => 'query_by']).'</div>';
    echo '<div class="col-md-3">'.Html::dropDownList('query_status', $query_status, ['9' => 'All', '1'=>'Waiting (open)', '2'=>'Resolve with some change', '3'=>'Resolve without any change', '-9'=>'Close'], ['class'=> 'form-control', 'id' => 'query_status']).'</div>
    <div class="col-md-3">'.Html::button('<span class="fa fa-search"></span> Search query', ['id'=>'btnSearchQuery', 'class'=>'btn btn-primary btn-md', 'type'=>'submit']).'</div>
    </form></div><hr>';


    ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="pull-right">
        <?php //echo Html::a('< Back', Yii::$app->request->referrer, ['class' => 'btn btn-success'])
        echo Html::a('<span class="fa fa-list"></span> '.'Show by form', '/query-request', ['class' => 'btn btn-success'])
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'xsourcex',
            'ezf_id',
            'target_id',
            //'data_id',
            'ezf_field_id',
            // 'new_val:ntext',
            // 'note:ntext',
            //'status',
            'user_create',
            'status',
            // 'user_update',
             'time_create:date',
            // 'time_update',
            // 'approved_by',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'จัดการ',
                'template' => '{view} {update}',
                'buttons' => [

                    //view button
                    'view' => function ($url, $model) use ($ezf_id) {

                        return Html::a('<span class="fa fa-eye"></span> View', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class'=>'btn btn-info btn-xs',
                            'target'=>'_blank'
                        ]);

                    },
                    'update' => function ($url, $model) use ($ezf_id) {
                        return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class'=>'btn btn-warning btn-xs',
                            'target'=>'_blank'
                        ]);

                    },
                ],
                'headerOptions'=>['style'=>'text-align: center;'],
                'contentOptions' => ['style' => 'width:190px;text-align: center;']
            ],
        ],
    ]); ?>

</div>
