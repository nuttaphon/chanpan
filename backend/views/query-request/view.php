<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDModalForm;

/* @var $this yii\web\View */
/* @var $model backend\models\QueryRequest */

$this->title = 'Query Resolution';
$this->params['breadcrumbs'][] = ['label' => 'Query Submission', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Query Submission (view by form)', 'url' => ['view-by-form', 'ezf_id'=>$model->oldAttributes['ezf_id']]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="query-request-view" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<?=
\yii\widgets\Breadcrumbs::widget([
    'homeLink' => [
        'label' => Yii::t('yii', 'Home'),
        'url' => Yii::$app->homeUrl,
    ],
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
   ])
?>
    <div class="panel panel-primary">
        <div class="panel-heading"></div>
        <div class="panel-body">
            <table class="table table-striped">
        <?php
            foreach($model as $key => $val){
                if($key !='id' && $key != 'data_id' && $key != 'new_changed' && $key != 'userkey_status') {
                    if(($key == 'old_val' || $key == 'new_val') && $model->userkey_status == 2) {

                    }else{
                        echo '<tr><td>';

                        if ($key == 'old_val' || $key == 'new_val') echo '<form action="#" name="form-' . $key . '">';

                        echo '<span class="h4">' . Html::activeLabel($model, $key) . '</span> ';
                        echo $model->$key;

                        if ($key == 'old_val' || $key == 'new_val') echo '</form>';

                        echo '</td></tr>';
                    }
                }
            }
            ?>
            </table>
            <p id="query-btn" class="pull-right">
                <?php
                echo ' '.Html::a('<span class="fa fa-check-square-o"></span> View form', ['/inputdata/redirect-page', 'ezf_id' => $model->oldAttributes['ezf_id'], 'dataid'=>$model->data_id], [
                        'class' => 'btn btn-warning btn-lg',
                        'title' => Yii::t('app', 'ดูฟอร์มที่ขอแก้ไขข้อมูล'),
                    ]);
                if($model->oldAttributes['status']==1 && $model->user_create == Yii::$app->user->id) {
                    echo Html::a('<span class="fa fa-edit"></span> Update', ['update', 'id' => $model->id], [
                        'title' => Yii::t('app', 'แก้ไขข้อมูลที่ร้องขอ'),
                        'class' => 'btn btn-primary btn-lg'
                    ]);
                    echo ' '.Html::a('<span class="fa fa-times"></span> Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger btn-lg',
                        'title' => Yii::t('app', 'ลบคำร้องขอแก้ไขข้อมูล'),
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]);
                }?>
                <?php if($queryManager->user_id || Yii::$app->user->can('adminsite')) {
                    if ($model->open_status) {
                        echo ' ' . Html::a('<span class="fa fa-times"></span> Closed', null, [
                                'class' => 'btn btn-default btn-lg',
                                'title' => Yii::t('app', 'Query ถูกปิดแล้ว'),
                            ]);
                    } else {
                        echo ' ' . Html::a('<span class="fa fa-times"></span> Close topic', ['close-topic', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-lg',
                                'title' => Yii::t('app', 'ปิดการ Query หัวข้อนี้'),
                                'data' => [
                                    'confirm' => 'คุณต้องการที่จะปิดการ Query หัวข้อนี้?',
                                    'method' => 'post',
                                ],
                            ]);
                    }
                }
                    echo ' '. Html::button('<span class="fa fa-send"></span> Respond', [
                        'class' => 'btn btn-success btn-lg',
                        'title' => Yii::t('app', 'เปลี่ยนข้อมูลที่ร้องขอ'),
                        'id' => 'query-btn-change',
                        'data-url'=>Url::to(['/query-request/approved-query', 'id' => $model->id,]),
                    ]);
                ?>
            </p>
            <br><br>

            <hr>
            <div>
                <h3>รายการขอแก้ที่ผ่านมา</h3>
                <div class="list-group">
                    <?php foreach($modelQueryLog as $val) { ?>
                    <a href="<?=Url::to(['/query-request/view', 'id'=>$val->id])?>" class="list-group-item <?= $model->id == $val->id ? 'active' : ''; ?>">
                        <h4 class="list-group-item-heading"><?php echo Yii::$app->formatter->asDatetime($val->time_create); ?></h4>
                        <p class="list-group-item-text">สถานะ
                            <?php
                            if($val->status == 1){
                                $val->status = 'Waiting';
                            }else if($val->status ==2){
                                $val->status = 'Resolve with some change';
                            }else if($val->status ==3){
                                $val->status = 'Resolve without any change';
                            }else if($val->status ==4){
                                $val->status = 'Unresolvable';
                            }else if($val->status ==5){
                                $val->status = 'Remark';
                            }
                            echo $val->status;?></p>
                    </a>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>

</div>
<hr>
<?= \yii\widgets\ListView::widget([
    'dataProvider' => $modelReplyData,
    'itemOptions' => ['class' => 'article-item'],
    'options' => [
        'tag' => 'div',
        'class' => 'list-wrapper',
        'id' => 'list-wrapper',
    ],
    //'layout' => "{pager}\n{items}\n{summary}",
    'itemView' => '_view_reply',
    'pager' => [
        'firstPageLabel' => 'first',
        'lastPageLabel' => 'last',
        'nextPageLabel' => 'next',
        'prevPageLabel' => 'previous',
        'maxButtonCount' => 3,
    ],
]);
?>
<hr>

<?php
echo SDModalForm::widget([
    'id' => 'modal-query-resolution',
    'size' => 'modal-lg',
    'tabindexEnable' => false
]);

$this->registerJs("
$('#query-btn').on('click', '#query-btn-change', function(){
                        modalQueryResolution($(this).attr('data-url'));
                });

function modalQueryResolution(url) {
                    $('#modal-query-resolution .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
                    $('#modal-query-resolution').modal('show')
                    .find('.modal-content')
                    .load(url);
                }
                ");
?>

