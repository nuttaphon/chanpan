<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Dmp');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dmp-index">


    <?php
    $sql = " SELECT dep_member.`name`, 
	dep_member.`level`, 
	dep_member.username, 
	dep_member.user_id,
	dep_meeting.id, 
	dep_meeting.staff, 
	dep_meeting.staffid, 
	dep_meeting.type, 
	dep_meeting.bday, 
	dep_meeting.bmonth, 
	dep_meeting.byear, 
	dep_meeting.eday, 
	dep_meeting.emonth, 
	dep_meeting.eyear, 
	dep_meeting.heading, 
	dep_meeting.place, 
	dep_meeting.detail, 
	dep_meeting.price, 
	dep_meeting.borrow, 
	dep_meeting.moretext, 
	dep_meeting.dstatus, 
	dep_meeting.lupdate
FROM dep_meeting INNER JOIN dep_member ON dep_meeting.staffid = dep_member.id
	   ";
    $data = Yii::$app->dbsr->createCommand($sql)->queryAll();
    
    $num = 0;
    $all = 0;
    
    if($data){
	foreach ($data as $key => $value) {
	    $all++;
	    
	    $id = common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
	    
	    $sqlSave = Yii::$app->dbdmp->createCommand($sql)->insert('tbdata_1471503538085309100', [
		'id'=>$id,
		//'ptid'=>$id,
		'xsourcex'=>'93001',
		//'xdepartmentx'=>$id, แผนก
		'rstat'=>2,
		//'sitecode'=>$id,
		//'ptcode'=>$id,
		//'ptcodefull'=>$id,
		//'hptcode'=>$id,
		//'hsitecode'=>$id,
		'user_create'=>  Yii::$app->user->id,
		'create_date'=>new yii\db\Expression('NOW()'),
		'user_update'=>Yii::$app->user->id,
		'update_date'=>new yii\db\Expression('NOW()'),
		//'target'=>$id,
		//'error'=>$id,
		'nstaffid'=>$value['user_id'],
		'ntype'=>$value['type'],
		'nbdate'=>addZero($value['byear']).'-'.addZero($value['bmonth']).'-'.addZero($value['bday']),
		'nedate'=>addZero($value['eyear']).'-'.addZero($value['emonth']).'-'.addZero($value['eday']),
		'nheading'=> $value['heading'],
		'nplace'=>$value['place'],
		'ndetail'=>$value['detail'],
		'nprice'=>$value['price'],
		'nborrow'=>$value['borrow'],
		'nmoretext'=>$value['moretext'],
	    ])->rawSql;
	    //->execute()
	    \appxq\sdii\utils\VarDumper::dump($sqlSave);
	    if($sqlSave){
		echo $sqlSave.'<br><br>';
		$num++;
	    }
	}
	
	echo $num .'/'.$all;
    }
    
    function addZero($value, $num=2){
	$len = strlen($value);
	$txt = $value;
	
	if($len==$num){
	    return $txt;
	} elseif ($len<$num) {
	    $sum = $num-$len;
	    for($i=0;$i<$sum;$i++){
		$txt = '0'.$txt;
	    }
	    return $txt;
	} else {
	    return '00';
	}
    }
    ?>

</div>
