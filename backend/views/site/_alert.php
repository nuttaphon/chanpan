<div class="modal-header ">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 class="modal-title" id="itemModalLabel">กรุณาอัพโหลดเอกสารรักษาความลับและสำเนาบัตรประชาชน</h3>
</div>

<div class="modal-body text-center" style="font-size: 18px;">
    
    <p>
    กรุณาอัพโหลดเอกสารรักษาความลับและสำเนาบัตรประชาชน โปรดอัพโหลดภายใน 30 วัน หลังจากที่ท่านสมัครสมาชิก
    <br>
    เหลือเวลา : (<code><strong><?=$time?></strong></code>)
    <br>    <div>ดาวน์โหลดเอกสารรักษาความลับและสำเนาบัตรประชาชน เพื่อให้ผู้บริหารอนุมัติ<a href="<?php echo Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' ? 'http://www1.cascap.in.th/v4cascap/central_docs/siteadmin20140210_01.pdf' : '/imgfile/agreement 20160503.pdf'; ?>" target="_blank" > Download</a></div>
    หรือ <?=\yii\helpers\Html::a('Upload เอกสารรักษาความลับและสำเนาบัตรประชาชนที่นี่', ['/sign-in/profile'])?>
    
    <div class="alert alert-danger" role="alert"><?=$comment?></div>
    </p>
    <img width="300" src="<?=  Yii::getAlias('@web').'/img/alert_img.jpg'?>">
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
</div>