<?php
\backend\assets\ImageAsset::register($this);

?>
<!--<div class="btn-group" role="group" style="position: absolute;top: 0;z-index: 9999;right: 0;">
    <button class="btn btn-primary" type="button"><i class="fa fa-undo lrotate"></i></button>
    <button class="btn btn-primary" type="button"><i class="fa fa-repeat rrotate"></i></button>
</div>-->

<?php
echo \yii\helpers\Html::tag('img', '', ['src'=>$img, 'width'=>880, 'class'=>'img-rotate']);

Yii::$app->view->registerJs("
    var imgangel = 0;
//    $('.rrotate').click(function(){
//	imgangel += 90;
//	if(imgangel==360){
//	    imgangel = 0;
//	}
//	$('img.img-rotate').rotate(imgangel);
//    });
//    
//    $('.lrotate').click(function(){
//	imgangel -= 90;
//	if(imgangel==0){
//	    imgangel = 360;
//	}
//	$('img.img-rotate').rotate({angle:imgangel});
//    });
    
    $('img.img-rotate').rotate({ 
	bind: 
	  { 
	     click: function(){
		 imgangel +=90;
		 
		 $(this).rotate({ animateTo:imgangel})
	     }
	  } 

     });
");
?>