<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/9/2016 AD
 * Time: 18:16
 */
use appxq\sdii\widgets\GridView;

echo GridView::widget([
    'dataProvider'=>$dataProviderReport,
    //'filterModel'=>$searchModel,
    // 'columns'=>$gridColumns,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'columns'=>[
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'format' => 'text',
            'label' => 'จังหวัด',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                return $model['province'];
            }
        ],
        [
            'format' => 'text',
            'label' => 'บัตรทอง',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['val1'];
            }
        ],
        [
            'label' => 'ข้าราชการ',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['val2'];
            }
        ],
        [
            'label' => 'ประกันสังคม',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['val3'];
            }
        ],
        [
            'label' => 'รัฐวิสาหกิจ',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['val4'];
            }
        ],
        [
            'label' => 'จ่ายเอง',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['val5'];
            }
        ],
        [
            'label' => 'อื่นๆ',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['val6'];
            }
        ],
        [
            'label' => 'ไม่ระบุ',
            'headerOptions' => ['class' => 'text-center bg-danger'],
            'contentOptions' => ['class' => 'text-center bg-danger'],
            'value' => function($model){
                return $model['space'];
            }
        ],
        [
            'label' => 'ทั้งหมด',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                return $model['total'];
            }
        ],
    ],

]);