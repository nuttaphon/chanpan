<?php
    use yii\helpers\Html;


    if(!isset($proofAllZone))exit();

    $lastCal = explode(' ',$lastcal);
    $lastCalDate = explode('-',$lastCal[0]);

    $fromDate = ($fiscalYear-1).'-10-01';
    $toDate = ($fiscalYear).'-09-30';
    if(strtotime($fromDate) < strtotime('2013-02-09'))$fromDate = '2013-02-09';
    if(strtotime($toDate) > strtotime(date('Y-m-d H:i:s')))$toDate = date('Y-m-d');

    $fd = explode('-',$fromDate);
    $td = explode('-',$toDate);

//    $userDetail['level'] = 8;
//    $userDetail['sitecode'] = '11095';

    if(
        $userDetail['level']==3 ||
        $userDetail['level']==4 ||
        $userDetail['level']==8 ||
        $userDetail['level']==13 ||
        $userDetail['level']==17 ||
        $userDetail['level']==18
    )
        $localSanitarium = true;
    else
        $localSanitarium = false;


    echo '<br/>';
    echo '<div class="col-md-12" style="text-align: center;" id="allproofrefreshdiv">';

    echo '<div id="excel-allproof-header-range">';
        echo 'แสดงข้อมูลในช่วงปีงบประมาณ '.($fiscalYear+543);
        echo ' (ระหว่างวันที่ '.intval($fd[2]).' '.$thaimonth[intval($fd[1])]['full'].' '.(intval($fd[0])+543);
        echo ' ถึงวันที่ '.intval($td[2]).' '.$thaimonth[intval($td[1])]['full'].' '.(intval($td[0])+543).')';
    echo '</div>';

    echo '<br/><br/>';

    echo '<div id="excel-allproof-header-lastcal">';
        echo 'ยอดสรุป ณ วันที่ '.intval($lastCalDate[2]).' '.$thaimonth[intval($lastCalDate[1])]['full'].' '.(intval($lastCalDate[0])+543).' เวลา '.$lastCal[1];
    echo '</div>';

    echo '<br/><br/>';
    echo '<a id="allproofrefresh" title="">(หากต้องการรายงาน ณ ปัจจุบัน คลิกที่นี่ ซึ่งอาจใช้เวลาหลายนาที)</a><br/><br/>';
    echo '</div>';
    echo '<br/><br/>';



    echo '<div style="text-align: left;display: inline-block;">';
        echo '<select class="form-control" id="allproof-report-table-show">';
            if($localSanitarium){
                echo '<option value="2">แสดงเฉพาะหน่วยบริการที่สังกัด</option>';
            }
            echo '<option value="1">แสดงเฉพาะข้อมูลที่เกี่ยวข้อง</option>';
            echo '<option value="0">แสดงทั้งหมด</option>';
        echo '</select>';
        echo '<br/>';


    echo '</div>';
    echo '<br/>';

    echo '<table class="table table-hover table-responsive" id="allproof-report-table" style="">';

        echo '<tbody>';
        foreach($proofAllZone as $key => $row){


            if(preg_match('/^zone_/',$key)) {

                echo '<tr show="me">';
            }else if(isset($row['label']) && $row['label']=='รวม'){

                echo '<tr style="color: blue;"'.($showing?' show="me" ':'').'>';

            }else if( isset($row['section']) ){

                if($row['section'] == 'เขตบริการสุขภาพ'){
                    $showing = true;
                }
                if($row['section'] == 'รายจังหวัด'){
                    $showing = false;
                }

                echo '<tr show="me" '.($row['section'] == 'ระดับสถานบริการ'?'mysite="true"':'').'>';

            }else if( isset($row['separator_zone']) ){
                $sep_zone = $row['separator_zone'];

                if($sep_zone == $userDetail['zone_code']){
                    $showing = true;
                }else{
                    $showing = false;
                }

                if($showing){
                    echo '<tr show="me">';
                }

            }else if( isset($row['separator_province']) ){
                $sep_pro = $row['separator_province'];

                if($sep_pro == $userDetail['provincecode']){
                    $showing = true;
                }else{
                    $showing = false;
                }

                if($showing){
                    echo '<tr show="me">';
                }

            }else if( isset($row['separator_amphur']) ){

                $sep_amp = explode('_',$row['separator_amphur']);
                // 3 || 4 || 8 || 13 || 17 || 18 // รพสต

                if($sep_amp[0] == $userDetail['provincecode'] && $sep_amp[1] == $userDetail['amphurcode']){
                    $showing = true;
                }else{
                    $showing = false;
                }

                if($showing){
                    echo '<tr show="me">';
                }

            }else if(isset($row['sitecode'])){

                echo '<tr '.($showing?'show="me" ':'').' '.($row['sitecode']==$userDetail['sitecode']?'mysite="true" ':'').'>';
            }else{
                echo '<tr '.($showing?'show="me" ':'').'>';
            }


            foreach($row as $colIndex => $col){
                if(
                    $colIndex == 'separator_amphur' ||
                    $colIndex == 'separator_province' ||
                    $colIndex == 'separator_zone' ||
                    $colIndex == 'provincecode' ||
                    $colIndex == 'pamphurcode' ||
                    $colIndex == 'sitecode'
                )continue;

                if(preg_match('/Pct$/',$colIndex)) {
                    $col = number_format( ($col == null || floatval($col)==0 ? 0 : $col),2);
                }else if(preg_match('/registerCount|cca02Count|suspected|ctmriCount|ccaCount/',$colIndex)){
                    $col = number_format( ($col == null || intval($col)==0 ? 0 : $col),0);
                }



                if( preg_match('/heading/',$colIndex ) ){
                    echo '<td class="unit-heading">'.$col.'</td>';
                    echo '<td class="unit-heading">เป้าหมาย</td>';
                    echo '<td class="unit-heading">ผลงาน</td>';
                    echo '<td class="unit-heading">ร้อยละ</td>';
                    echo '<td class="unit-heading">พบ Liver mass, Dilate mass</td>';
                    echo '<td class="unit-heading">ส่ง CT/MRI</td>';
                    echo '<td class="unit-heading">พบเป็น CCA</td>';
                    echo '<td class="unit-heading">ร้อยละ</td>';

                }
                else if( preg_match('/section/',$colIndex ) ){
                    echo '<th class="unit-section" colspan="8">'.$col.'</th>';
                }
                else if( preg_match('/separator/',$colIndex ) ){
                    echo '<td class="unit-separator" colspan="8">'.$col.'</td>';
                }
                else if( preg_match('/typesplit/',$colIndex ) ){
                    echo '<td class="unit-typesplit" colspan="8">'.$col.'</td>';
                }
                else if( preg_match('/zone|province|amphur|name/',$colIndex ) ){
                    $col = str_replace('โรงพยาบาลส่งเสริมสุขภาพตำบล','รพ.สต.',$col);
                    $col = str_replace('โรงพยาบาล','รพ.',$col);
                    echo '<td class="unit-name">'.$col.'</td>';
                }
                else if(isset($row['summation'])){
                    echo '<td class="unit-sum'.($colIndex == 'summation'?' name':'').'">'.$col.'</td>';
                }else{
                    echo '<td>'.$col.'</td>';
                }

            }
            echo '</tr>';
        }
        echo '</tbody>';
    echo '</table>';

    $this->registerJs('
        $("#allproofrefresh").click(function(){
            allProofFormData["refresh"] = "true";
            getAllProofCca();
        });
    ');

    $this->registerJs(
        '$("#allproof-report-table-show")[0].selectedIndex = '.($localSanitarium?'1':'0').';'
    );

    $this->registerJs('

        $("#allproof-report-table tr").hide();
        $("#allproof-report-table [show=\'me\']").show()

        $("#allproof-report-table-show").change(function(){
            var v = $(this).val();
            if(v==0){
                 $("#allproof-report-table tr").show();
            }else if(v==1){
                $("#allproof-report-table tr").hide();
                $("#allproof-report-table [show=\'me\']").show();
            }else if(v==2){ // localSanitarium
                if( $("#allproof-report-table [mysite]:last:has(.unit-name)").length==0 )return false;
                $("#allproof-report-table tr").hide();
                var myNodeLast = $("#allproof-report-table [mysite]:last");
                var myHeadingNode = myNodeLast.prevAll(":has(\'.unit-heading\')");
                myHeadingNode.eq(0).show();


                myNodeLast.prevAll(":has(\'.unit-typesplit\')").eq(0).show();
                myNodeLast.prevAll(":has(\'.unit-section\')").eq(0).show();

                $("#allproof-report-table tr[mysite]").show();
            }
        });
        /*
        $("#allproof-report-table-showme").click(function(){
            var showstate = $(this).attr("show");
            if(showstate==0){
                $(this).html("แสดงเฉพาะข้อมูลที่เกี่ยวข้อง");
                $(this).attr("show",1);
                $("#allproof-report-table tr").show();
            }else if(showstate==1){
                $(this).html("แสดงทั้งหมด");
                $(this).attr("show",0);
                $("#allproof-report-table tr").hide();
                $("#allproof-report-table [show=\'me\']").show()
            }
        });
        */

        $("#allproof-report-table tr>td:nth-child(4)").attr("title","เทียบกับ CCA-01");
    ');



