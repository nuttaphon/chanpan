<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 3/3/2016
 * Time: 12:52 PM
 */

    use miloschuman\highcharts\Highcharts;

    if(is_null($query['count']) || is_null($query3['count']))exit();

    echo '<div class="col-md-6" >';
        echo Highcharts::widget([
            'setupOptions' => [
                'lang' => [
                    'thousandsSep' => ',',
                    'loading' => 'Loading...',
                ],
            ],
            'scripts' => [
                'modules/exporting',
                'themes/default',
            ],
            'options' => $query['options'],
            'id'=>'cca01-no-select'
        ]);
    echo '</div>';

    echo '<div class="col-md-6" >';
        echo Highcharts::widget([
            'setupOptions' => [
                'lang' => [
                    'thousandsSep' => ',',
                    'loading' => 'Loading...',
                ],
            ],
            'scripts' => [
                'modules/exporting',
                'themes/default',
            ],
            'options' => $query3['options'],
            'id'=>'cca02-no-select'
        ]);
    echo '</div>';