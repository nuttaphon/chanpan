<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 7/9/2016 AD
 * Time: 18:16
 */
use appxq\sdii\widgets\GridView;

global $ptt_register, $ptt_treatment, $ptt_followup, $ptt_refer;
?>

<?php
echo GridView::widget([
    'id' => 'palliative-report1',
    'dataProvider'=>$dataProviderReport,
    //'filterModel'=>$searchModel,
    // 'columns'=>$gridColumns,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'columns'=>[
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'format' => 'text',
            'label' => 'ชื่อสถานบริการ',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                $sql="SELECT hcode, name, tambon, amphur, province FROM all_hospital_thai WHERE hcode=:hcode";
                $hosp = \Yii::$app->db->createCommand($sql,[':hcode' => $model['xsourcex']])->queryOne();
                $hospStr = 'จ.'.$hosp['province'];
                return $hosp['hcode'].' : '.$hosp['name'] .' ('.$hospStr.')';
            }
        ],
        [
            'label' => 'Register',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                global $ptt_register;
                $ptt_register += $model['register'];
                return $model['register'];
            }
        ],
        [
            'label' => 'Treatment',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                global $ptt_treatment;
                $ptt_treatment += $model['treatment'];
                return $model['treatment'];
            }
        ],
        [
            'label' => 'Follow-up',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                global $ptt_followup;
                $ptt_followup += $model['followup'];
                return $model['followup'];
            }
        ],
        [
            'label' => 'Refer',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                global $ptt_refer;
                $ptt_refer += $model['refer'];
                return $model['refer'];
            }
        ],
    ],

]);
?>
<h5>Total Register = <?=$ptt_register;?>, Total Treatment = <?=$ptt_treatment;?>, Total Follow-up = <?=$ptt_followup;?>, Total Refer = <?=$ptt_refer;?></h5>
