<?php
    use yii\helpers\Html;

    if(!isset($dataAllZone))exit();

    //echo $userDetail['level'];
//    var_dump($dataAllZone);
//    exit();

    $lastCal = explode(' ',$lastcal);
    $lastCalDate = explode('-',$lastCal[0]);
//
//    $fromDate = ($fiscalYear-1).'-10-01';
//    $toDate = ($fiscalYear).'-09-30';
//    if(strtotime($fromDate) < strtotime('2013-02-09'))$fromDate = '2013-02-09';
//    if(strtotime($toDate) > strtotime(date('Y-m-d H:i:s')))$toDate = date('Y-m-d');
//
    $fd = explode('-',$getFromDate);
    $td = explode('-',$getToDate);

//    $userDetail['level'] = 8;
//    $userDetail['sitecode'] = '11095';

    if(
        $userDetail['level']==3 ||
        $userDetail['level']==4 ||
        $userDetail['level']==8 ||
        $userDetail['level']==13 ||
        $userDetail['level']==17 ||
        $userDetail['level']==18
    )
        $localSanitarium = true;
    else
        $localSanitarium = false;




    echo '<br/>';
    echo '<div class="col-md-12" style="text-align: center;" id="datarefreshdiv">';

    if($getType!=null){
        echo '<div id="excel-data-header-range">';
        if($getType == 'year'){
            echo 'แสดงข้อมูลในช่วงปีงบประมาณ '.(intval($str)+543).' (';
            $this->registerJs('

            ');
        }else if($getType == 'all'){
            echo 'แสดงข้อมูลทั้งหมด '.$str.' (';
        }else if($getType == 'day'){
            echo 'แสดงข้อมูล'.$str;
        }
    }

    echo 'ระหว่างวันที่ '.intval($fd[2]).' '.$thaimonth[intval($fd[1])]['full'].' '.(intval($fd[0])+543);
    echo ' ถึงวันที่ '.intval($td[2]).' '.$thaimonth[intval($td[1])]['full'].' '.(intval($td[0])+543);
    if($getType!=null){
        if($getType == 'year' || $getType == 'month'){
            echo ')';
        }
        echo '</div>';
    }



    echo '<br/><br/>';
    echo '<div id="excel-data-header-lastcal">';
        echo 'ยอดสรุป ณ วันที่ '.intval($lastCalDate[2]).' '.$thaimonth[intval($lastCalDate[1])]['full'].' '.(intval($lastCalDate[0])+543).' เวลา '.$lastCal[1];
    echo '</div>';
    echo '<br/><br/>';
    echo '<a id="datarefresh" title="">(หากต้องการรายงาน ณ ปัจจุบัน คลิกที่นี่ ซึ่งอาจใช้เวลาหลายนาที)</a><br/><br/>';
    echo '</div>';
    echo '<br/><br/>';

//    var_dump($dataAllZone);
//    exit();

    $sectionColspan = intval((sizeof($allMonthList)*8)+2);
    $headingString = '';
    foreach ($allMonthList as $key => $eachMonth) {
        $showMonth = explode('_', $eachMonth['Ym']);
        $headingString.='<td colspan="8" class="unit-heading">'.
            $thaimonth[intval($showMonth[1])]['full'].' '.(543+intval($showMonth[0])).
        '</td>';
    }



    /*if(
        $userDetail['level']==3 ||
        $userDetail['level']==4 ||
        $userDetail['level']==8 ||
        $userDetail['level']==13 ||
        $userDetail['level']==17 ||
        $userDetail['level']==18
    ){*/
        echo '<div style="text-align: left;display: inline-block;">';
            echo '<select class="form-control" id="data-report-table-show">';
            	if($localSanitarium){
            		echo '<option value="2">แสดงเฉพาะหน่วยบริการที่สังกัด</option>';
            	}
                echo '<option value="1">แสดงเฉพาะข้อมูลที่เกี่ยวข้อง</option>';
                echo '<option value="0">แสดงทั้งหมด</option>';
            echo '</select>';
            echo '<br/>';

            /*echo '<button class="btn btn-success" type="submit" id="data-report-table-showme" show="0">';
                echo 'แสดงทั้งหมด';
            echo '</button> ';
            if($localSanitarium){
                echo '<button class="btn btn-success" type="submit" id="data-report-table-mysite" show="0">';
                    echo 'แสดงเฉพาะหน่วยงานที่สังกัด';
                echo '</button>';
            }*/
        echo '</div>';
        echo '<br/>';
    //}



    echo '<table class="table table-hover table-responsive" id="data-report-table" style="" border="1">';

        echo '<tbody>';
        foreach($dataAllZone as $key => $row){
            //$showing = true;
            //echo $key.'<br/>';
            if(preg_match('/^zone_/',$key)){

                echo '<tr show="me">';
                /*
                $sep_site = explode('_',$key)[1];
                //echo 'asdasd'.$sep_site.'_'.$userDetail['sitecode'];
                echo '<tr mysite="true" '.$sep_site.' '.$key.''.$userDetail['sitecode'].'>';
                if($sep_site==$userDetail['sitecode']){
                    //echo 'ffffff';
                    echo '<tr mysite="true">';
                }else{
                    echo '<tr>';
                }
                */
            }else if(isset($row['label']) && $row['label']=='รวม'){

                echo '<tr style="color: blue;"'.($showing?' show="me" ':'').'>';

            }else if( isset($row['section']) ){

                /*if(
                    $userDetail['level']==3 ||
                    $userDetail['level']==4 ||
                    $userDetail['level']==8 ||
                    $userDetail['level']==13 ||
                    $userDetail['level']==17 ||
                    $userDetail['level']==18
                ){*/
                    //echo '<tr '.($row['section'] == 'ระดับสถานบริการ'?'show="me"':'').'>';

                    if($row['section'] == 'เขตบริการสุขภาพ'){
                        $showing = true;
                    }
                    if($row['section'] == 'รายจังหวัด'){
                        $showing = false;
                    }

                    echo '<tr show="me" '.($row['section'] == 'ระดับสถานบริการ'?'mysite="true"':'').'>';

                //}


            }else if( isset($row['separator_zone']) ){
                $sep_zone = $row['separator_zone'];
                /*if(
                    $userDetail['level']==3 ||
                    $userDetail['level']==4 ||
                    $userDetail['level']==8 ||
                    $userDetail['level']==13 ||
                    $userDetail['level']==17 ||
                    $userDetail['level']==18
                ){*/
                    if($sep_zone == $userDetail['zone_code']){
                        $showing = true;
                    }else{
                        $showing = false;
                    }

                    if($showing){
                        echo '<tr show="me">';
                    }
                //}
            }else if( isset($row['separator_province']) ){
                $sep_pro = $row['separator_province'];
                /*if(
                    $userDetail['level']==3 ||
                    $userDetail['level']==4 ||
                    $userDetail['level']==8 ||
                    $userDetail['level']==13 ||
                    $userDetail['level']==17 ||
                    $userDetail['level']==18
                ){*/
                    if($sep_pro == $userDetail['provincecode']){
                        $showing = true;
                    }else{
                        $showing = false;
                    }

                    if($showing){
                        echo '<tr show="me">';
                    }
                //}
            }else if( isset($row['separator_amphur']) ){

                $sep_amp = explode('_',$row['separator_amphur']);
                // 3 || 4 || 8 || 13 || 17 || 18 // รพสต
                /*if(
                    $userDetail['level']==3 ||
                    $userDetail['level']==4 ||
                    $userDetail['level']==8 ||
                    $userDetail['level']==13 ||
                    $userDetail['level']==17 ||
                    $userDetail['level']==18
                ){*/
                    if($sep_amp[0] == $userDetail['provincecode'] && $sep_amp[1] == $userDetail['amphurcode']){
                        $showing = true;
                    }else{
                        $showing = false;
                    }

                    if($showing){
                        echo '<tr show="me">';
                    }

                //}

            }else{
                echo '<tr '.($showing?'show="me" ':'').' '.($key=='sitecode_'.$userDetail['sitecode']?'mysite="true" ':'').'>';
            }

            foreach($row as $colIndex => $col){
                if($colIndex=='separator_amphur' || $colIndex=='separator_province' || $colIndex== 'separator_zone' )continue;


                if( preg_match('/heading/',$colIndex ) ){
                    echo '<td rowspan="2" class="unit-heading">'.$col.'</td>';
                    echo '<td rowspan="2" class="unit-heading">'.Total.'</td>';
                        echo $headingString;
                    echo '</tr>';
                    echo '<tr'.($showing?' show="me" ':'').'>';

                    foreach ($allMonthList as $key => $eachMonth) {

                        echo '<td class="unit-heading" style="border-left: 1px solid #AAA">Register</td>';
                        echo '<td class="unit-heading">ICF</td>';
                        echo '<td class="unit-heading">CCA-01</td>';
                        echo '<td class="unit-heading">CCA-02</td>';
                        echo '<td class="unit-heading">CCA-02.1</td>';
                        echo '<td class="unit-heading">CCA-03</td>';
                        //echo '<td class="unit-heading">CCA-03.1</td>';
                        echo '<td class="unit-heading">CCA-04</td>';
                        echo '<td class="unit-heading">CCA-05</td>';
                    }

                }
                else if( preg_match('/section/',$colIndex ) ){
                    echo '<th class="unit-section" colspan="'.$sectionColspan.'" style="text-align:left;">'.$col.'</th>';
                }
                else if( preg_match('/separator/',$colIndex ) ){
                    echo '<td class="unit-separator" colspan="'.$sectionColspan.'">'.$col.'</td>';
                }
                else if( preg_match('/typesplit/',$colIndex ) ){
                    echo '<td class="unit-typesplit" colspan="'.$sectionColspan.'">'.$col.'</td>';
                }
                else if( preg_match('/label/',$colIndex ) ){
                    $col = str_replace('โรงพยาบาลส่งเสริมสุขภาพตำบล','รพ.สต.',$col);
                    $col = str_replace('โรงพยาบาล','รพ.',$col);

                    echo '<td class="unit-name" style="border-left: 1px solid #AAA;">'.$col.'</td>';


                }else if( preg_match('/sumRegis/',$colIndex )){
                    echo '<td style="border-left: 1px solid #AAA;">'.number_format($col).'</td>';
                }
                else if(isset($row['summation'])){
                    echo '<td class="unit-sum'.($colIndex == 'summation'?' name':'').'">'.$col.'</td>';
                }else if($col==null){
                    echo '<td style="border-left: 1px solid #AAA;">0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                    echo '<td>0</td>';
                }else{

                    foreach($col as $keyData => $data){
                        if(preg_match('/provincecode|amphurcode|sitecode|cca031Count/',$keyData ))continue;
                        if(preg_match('/registerCount/',$keyData)){
                            echo '<td style="border-left: 1px solid #AAA;">'.number_format($data).'</td>';
                        }else{
                            echo '<td>'.number_format($data).'</td>';
                        }

                    }

                }

            }
            echo '</tr>';
        }
        echo '</tbody>';
    echo '</table>';

    $this->registerJs('
        $("#datarefresh").click(function(){
            dataMonitorFormData["refresh"] = "true";
            setCheckedZone();
        });
    ');

    /*if(
        $userDetail['level']==3 ||
        $userDetail['level']==4 ||
        $userDetail['level']==8 ||
        $userDetail['level']==13 ||
        $userDetail['level']==17 ||
        $userDetail['level']==18
    ){*/
        $this->registerJs(
            '$("#data-report-table-show")[0].selectedIndex = '.($localSanitarium?'1':'0').';'
        );
        $this->registerJs('

            $("#data-report-table tr").hide();
            $("#data-report-table [show=\'me\']").show();



            $("#data-report-table-show").change(function(){
                var v = $(this).val();
                if(v==0){
                     $("#data-report-table tr").show();
                }else if(v==1){
                    $("#data-report-table tr").hide();
                    $("#data-report-table [show=\'me\']").show();
                }else if(v==2){ // localSanitarium
                    if( $("#data-report-table [mysite]:last:has(.unit-name)").length==0 )return false;
                    $("#data-report-table tr").hide();
                    var myNodeLast = $("#data-report-table [mysite]:last");
                    var myHeadingNode = myNodeLast.prevAll(":has(\'.unit-heading\')");
                    myHeadingNode.eq(0).show();
                    myHeadingNode.eq(1).show();

                    myNodeLast.prevAll(":has(\'.unit-typesplit\')").eq(0).show();
                    myNodeLast.prevAll(":has(\'.unit-section\')").eq(0).show();

                    $("#data-report-table tr[mysite]").show();
                }
            });
            /*
            $("#data-report-table-showme").click(function(){
                var showstate = $(this).attr("show");
                if(showstate==0){
                    $(this).html("แสดงเฉพาะข้อมูลที่เกี่ยวข้อง");
                    $(this).attr("show",1);
                    $("#data-report-table tr").show();
                }else if(showstate==1){
                    $(this).html("แสดงทั้งหมด");
                    $(this).attr("show",0);
                    $("#data-report-table tr").hide();
                    $("#data-report-table [show=\'me\']").show()
                }
            });

            $("#data-report-table-mysite").click(function(){
                var showstate = $(this).attr("show");
                if(showstate==1){
                    $(this).html("แสดงเฉพาะหน่วยงานที่สังกัด");
                    $(this).attr("show",0);
                    $("#data-report-table tr").show();
                }else if(showstate==0){
                    $(this).html("แสดงทั้งหมด");
                    $(this).attr("show",1);
                    $("#data-report-table tr").hide();
                    $("#data-report-table tr[mysite]").show();
                    var mynode = $("[mysite]:last").prevAll(":has(\'.unit-heading\')");
                    mynode.eq(0).show();
                    mynode.eq(1).show();
                    $("[mysite]:last").prevAll(":has(\'.unit-typesplit\')").eq(0).show();
                    $("[mysite]:last").prevAll(":has(\'.unit-section\')").eq(0).show();
                }
            });
            */
        ');
    //}


