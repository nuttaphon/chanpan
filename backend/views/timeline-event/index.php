<?php
/**
 * Eugine Terentev <eugine@terentev.net>
 * @var $this \yii\web\View
 * @var $model \common\models\TimelineEvent
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\web\JsExpression;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
//
if($siteconfig->sitemode==1){
    $mode = "Faculty Mode";
}else if($siteconfig->sitemode==2){
    $mode = "Patient Mode";
}else{
    $mode = "Mixed Mode";
}
//$this->title = Yii::t('backend', $siteconfig->sitename." (".$mode.")");
$icons = [
    'user'=>'<i class="fa fa-user bg-blue"></i>'
];

$model = Yii::$app->user->identity->userProfile->sitecode;
$sqlHospital = "SELECT `hcode`,`name` FROM all_hospital_thai WHERE hcode='".($model)."' ";
$dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();

$sqlSiteURL = "SELECT code,`url`,`name` FROM site_url WHERE code='".(is_numeric($model) ? $model+0 : $model)."' ";
$dataSiteURL = Yii::$app->db->createCommand($sqlSiteURL)->queryOne();
if(!$dataSiteURL){
    $controller = new \yii\web\Controller();
    $controller->refresh();
}
$frontendUrl=$dataSiteURL['url'].".".Yii::$app->keyStorage->get('frontend.domain');
if (Yii::$app->keyStorage->get('ssl.enable')=="1") {
    $http="https";
}else{
    $http="http";
}

?>
    
<?php

    //if(Yii::$app->keyStorage->get('frontend.domain')!='cascap.in.th'){
	
	
       // echo '<div class="row">';
        echo '<div class="box" style="padding: 15px 30px;">';
        echo '<span class="h3"><li class="fa fa-building"></li> ';

        echo $model ." : " . $dataHospital['name']." ";
        echo '<il class="fa fa-globe"></il> ';
        echo \yii\helpers\BaseHtml::a("{$http}://".$frontendUrl, "{$http}://".$frontendUrl).'</span>';        

        Modal::begin([
            'id' => 'modal-move-site',
            'header' => '<h2>เปลี่ยนหน่วยบริการ</h2>',
            'toggleButton' => ['label' => 'เข้าทำงานในหน่วยบริการอื่น','class' => 'btn btn-info btn-sm pull-right'],
            'closeButton' => ['label' => 'ยกเลิก','class' => 'btn btn-success btn-sm pull-right'],
        ]);
        
            echo \appxq\sdii\widgets\GridView::widget([
                'id' => 'user-allow-grid',
                'dataProvider' => $userallowdata,
                //'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'headerOptions' => ['style'=>'text-align: center;'],
                        'contentOptions' => ['style'=>'width:60px;text-align: center;'],
                    ],

                    [
                        'header' => 'หน่วยงานที่สามารถเข้าไปทำงาน',
                        'value' => function ($model) {
                            $hospital = app\modules\adddoctor\models\AllHospitalThai::find()->where(['hcode'=>$model->sitecode])->one();
                            return $model->sitecode . " " . $hospital->name;
                        }
                    ],    
                    [
                        'class' => 'appxq\sdii\widgets\ActionColumn',
                        'contentOptions' => ['style'=>'width:80px;text-align: center;'],
                        'template' => '{changesite}',
                        'buttons' => [
                            'changesite' => function ($url, $model) {
                                   return Html::a('<span class="glyphicon glyphicon-log-in"></span> เข้าสู่การทำงาน', $url, [
                                            'title' => Yii::t('app', 'เข้าสู่การทำงาน'),
                                            'class'=>'btn btn-info btn-xs',
                                            'data-confirm' => Yii::t('yii', 'คุณแน่ใจหรือไม่ที่จะเข้าสู่การทำงานในหน่วยงานดังกล่าว?'),
                                            'data-method' => 'post',    
                                            'data-action' => ['sitecode'=>$model->sitecode],
                                            'data-pjax'=>'0',
                                    ]); 
                            },
                        ],                
                    ],                     

                ],
            ]); 
            echo "<div>ท่านสามารถแจ้งผู้ดูแลระบบของแต่ละหน่วยบริการเพื่อเพิ่มท่านเข้าไปในรายชื่อที่อนุญาตให้เข้าใช้งานได้ ที่เมนู ตั้งค่า -> สมาชิกในหน่วยงาน </div>";
        Modal::end(); 

        echo '</div>';
        //echo '</div>';
	
	if(!in_array(Yii::$app->keyStorage->get('frontend.domain'), ['thaipalliative.org'])){
	    echo $this->render('/tccbots/_mym');
	}
   // }
	
?>


    <?php
           
           // echo '<div class="row">';
                echo '<div class=" box" style="padding-top: 15px;">';
		?>
    <div class="modal-header" style="margin-bottom: 15px;">
    <h3 class="modal-title" id="itemModalLabel">My EzForms <?=  \yii\helpers\Html::a('<i class="glyphicon glyphicon-plus"></i> นำเข้าจาก Favorite Form', ['/inputdata/index'], ['class'=>'btn btn-link btn-sm'])?></h3>
</div>
    <?php
    echo '<div class="alert" id="report-overview-info">';
                        
                    echo '<div class="alert" id="report-overview-info">';
		    if(Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th') {
                       echo Html::a('<label class="btn btn-danger btn-lg" role="button">ผลการตรวจสอบความถูกต้องของเอกสาร เพื่อคำนวณค่าตอบแทน</label>',
                            Url::to('/icf/total'),
                            [
                                'target'=>'_self'
                            ]);
			    echo '<br><br>';
		    }
                        echo '<label id="report-overview-lastcal"></label> ';
                        echo '<button class="btn btn-primary" id="report-overview-refresh" sitecode="'.$mysitecode.'">Refresh <i class="fa fa-refresh"></i></button>';
                        echo '<br/><br/>';
                        echo '<label id="report-overview-daterange"></label> ';

                    echo '</div>';
                    echo '<div class="alert table-responsive" id="report-overview-container">';
                        echo $table;
                    echo '</div>';
                echo '</div>';
          //  echo '</div>';
	?>
	
	
    
	<?php
        $this->registerJs('
            $("#report-overview-refresh").off().click(function(){

                $(this).children("i").addClass("fa-spin");
                $("#overview-report-table").addClass("fadeout");

                $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.Url::to('/timeline-event/report-overview-refresh/').'",
                    data    : {
                        sitecode: $(this).attr("sitecode"),
			module: "'.$module.'",
                    },
                    success  : function(response) {
                        $("#report-overview-container").html(response);
                        $("#overview-report-table").removeClass("fadeout");
                        $("#report-overview-refresh i").removeClass("fa-spin");
                    },
                    error : function(){
                        $("#report-overview-refresh i").removeClass("fa-spin");
                    }
                });
            });
        ');
    ?>

<?php
    if(Yii::$app->keyStorage->get('frontend.domain')=='thaipalliative.org') { ?>
<br>
    <div class="box">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    ตั้งค่ารูปแบบรายงาน
                </h4>
            </div>
            <div role="tabpanel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <b>เลือกเขตบริการสุขภาพ</b>
                            <?php echo Html::dropDownList('tpc_province', null, [
                                 '01'=>'เขตบริการสุขภาพที่ 1',
                                 '02'=>'เขตบริการสุขภาพที่ 2',
                                 '03'=>'เขตบริการสุขภาพที่ 3',
                                 '04'=>'เขตบริการสุขภาพที่ 4',
                                 '05'=>'เขตบริการสุขภาพที่ 5',
                                 '06'=>'เขตบริการสุขภาพที่ 6',
                                 '07'=>'เขตบริการสุขภาพที่ 7',
                                 '08'=>'เขตบริการสุขภาพที่ 8',
                                 '09'=>'เขตบริการสุขภาพที่ 9',
                                 '10'=>'เขตบริการสุขภาพที่ 10',
                                 '11'=>'เขตบริการสุขภาพที่ 11',
                                 '12'=>'เขตบริการสุขภาพที่ 12',
                            ],
                                [
                                    'prompt'=>'- ทุกเขตบริการสุขภาพ -', 'class'=>'form-control',
                                    'id' => 'tpc_area_zone',
                                ]);
                            ?>
                            <?php
                            $this->registerJs("$('#tpc_area_zone').on('change',function(){
                                  var val = $(this).val();
                                  $.getJSON( '".Url::to(['ajax/list-province', 'zone' =>''])."'+val, function( data ) {
                                      $('#tpc_province').html('<option value>- ทุกจังหวัด -</option>');
                                      $.each( data, function( key, val ) {
                                            $('#tpc_province').append('<option value=\"'+val.provincecode+'\">'+val.province+'</option>');
                                      });
                                });

                                });");
                            ?>
                        </div>
                        <div class="col-md-3 form-group"><b>จาก</b>
                            <?=Html::textInput('cccc', null, ['id' => 'palliative_date_start', 'type'=>date, 'class'=>'form-control']);?>
                        </div><div class="col-md-3 form-group"><b>ถึง</b>
                            <?=Html::textInput('ccccx', null, ['id'=>'palliative_date_end', 'type'=>date, 'class'=>'form-control']);?>
                        </div><div class="col-md-6 form-group">
                            <?=Html::dropDownList('tpc_province', null, \yii\helpers\ArrayHelper::map($dataProviderReport2->models, 'provincecode', 'province'),
                                [
                                    'prompt'=>'- ทุกจังหวัด -', 'class'=>'form-control',
                                    'id' => 'tpc_province',
                                ]);
                            echo '</div><div class="col-md-6 form-group">';
                            echo Html::dropDownList('tpc_pay_right', null, [
                                '0' =>'ทุกสิทธิ์การรักษา',
                                '1' =>'บัตรทอง',
                                '2' => 'ข้าราชการ',
                                '3' => 'ประกันสังคม',
                                '4' => 'รัฐวิสาหกิจ',
                                '5' => 'จ่ายเอง',
                                '6' => 'อื่นๆ',
                            ], [
                                'class'=>'form-control',
                                'id' => 'tpc_pay_right',
                                'onchange' => 'palliativeReport3($(this).val())'
                            ]);
                            ?>
                        </div>
                    </div><!-- end col and row-->

                </div>
            </div>
        </div> <!-- end panel-->

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-primary">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            รายงานการนำเข้าข้อมูล
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="h4">รายงานระหว่างวันที่ </span> <span class="h4 date_start"></span> ถึง <span class="h4 date_end"></span><br>
                                <?=Html::button('<span class="fa fa-search"></span> แสดงรายงาน', [
                                    'id' => 'btn-palliative-report1',
                                    'class' => 'btn btn-success btn-md',
                                ]);?>
                                <?php
                                $this->registerJs("$('#btn-palliative-report1').on('click',function(){
                                    var date_start = $('#palliative_date_start').val();
                                    var date_end = $('#palliative_date_end').val();
                                    //
                                    $('.date_start').text(date_start);
                                    $('.date_end').text(date_end);
                                    
                                    if(date_start==''){
                                        alert('กรุณาเลือกช่วงเวลา เริ่มแสดงรายงาน');
                                    }
                                    else if(date_end==''){
                                        alert('กรุณาเลือกช่วงเวลา สรุปแสดงรายงาน');
                                    }else{
                                        $('#report-palliative1').html('<i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i><span class=\"sr-only\">Loading...</span>');
                                        $(this).prop('disabled',true);
                                        $.post('".Url::to(['timeline-event/palliative-report', 'type'=>'report1'])."',{date_start:date_start, date_end:date_end},function(result){
                                            $('#report-palliative1').html(result);
                                            $('#btn-palliative-report1').prop('disabled',false);
                                        });
                                    }
                                });");
                                ?>
                            </div>
                        </div>
                        <br>
                        <div id="report-palliative1"></div>
                    </div>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            รายงานแยกตามสิทธิ์การรักษา (ไม่นับเลขบัตรที่ลงทะเบียนซ้ำ)
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="h4">รายงานระหว่างวันที่ </span> <span class="h4 date_start"></span> ถึง <span class="h4 date_end"></span><br>
                                <?=Html::button('<span class="fa fa-search"></span> แสดงรายงาน', [
                                    'id' => 'btn-palliative-report2',
                                    'class' => 'btn btn-success btn-md',
                                ]);?>
                                <?php
                                $this->registerJs("$('#btn-palliative-report2').on('click',function(){
                                    var date_start = $('#palliative_date_start').val();
                                    var date_end = $('#palliative_date_end').val();
                                    //
                                     $('.date_start').text(date_start);
                                    $('.date_end').text(date_end);
                                    
                                    if(date_start==''){
                                        alert('กรุณาเลือกช่วงเวลา เริ่มแสดงรายงาน');
                                    }
                                    else if(date_end==''){
                                        alert('กรุณาเลือกช่วงเวลา สรุปแสดงรายงาน');
                                    }else{
                                        $('#report-palliative2').html('<i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i><span class=\"sr-only\">Loading...</span>');
                                        $(this).prop('disabled',true);
                                        $.post('".Url::to(['timeline-event/palliative-report', 'type'=>'report2'])."',{date_start:date_start, date_end:date_end},function(result){
                                            $('#report-palliative2').html(result);
                                            $('#btn-palliative-report2').prop('disabled',false);
                                        });
                                    }
                                });");
                                ?>
                            </div>
                        </div>
                        <br>
                        <div id="report-palliative2"></div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            รายงานข้อมูลเครือข่าย (เลือกจังหวัด จำแนกตามสิทธิ์การรักษา)
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="h4">รายงานระหว่างวันที่ </span> <span class="h4 date_start"></span> ถึง <span class="h4 date_end"></span><br>
                                <?=Html::button('<span class="fa fa-search"></span> แสดงรายงาน', [
                                    'id' => 'btn-palliative-report3',
                                    'class' => 'btn btn-success btn-md',
                                ]);?>
                                <?php
                                $this->registerJs("$('#btn-palliative-report3').on('click',function(){
                                    var date_start = $('#palliative_date_start').val();
                                    var date_end = $('#palliative_date_end').val();
                                    var province = $('#tpc_province').val();
                                    var pay_right = $('#tpc_pay_right').val();
                                    //
                                    $('.date_start').text(date_start);
                                    $('.date_end').text(date_end);
                                    
                                    if(date_start==''){
                                        alert('กรุณาเลือกช่วงเวลา เริ่มแสดงรายงาน');
                                    }
                                    else if(date_end==''){
                                        alert('กรุณาเลือกช่วงเวลา สรุปแสดงรายงาน');
                                    }else if($('#tpc_area_zone').val()==''){
                                        alert('กรุณาเลือกเขตสถานบริการ');
                                    }else if(province==''){
                                        alert('กรุณาเลือกจังหวัด');
                                    }else{
                                        $('#report-palliative3').html('<i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i><span class=\"sr-only\">Loading...</span>');
                                        $(this).prop('disabled',true);
                                        $.post('".Url::to(['timeline-event/palliative-report', 'type'=>'report3'])."',{date_start:date_start, date_end:date_end, province:province, pay_right:pay_right},function(result){
                                            $('#report-palliative3').html(result);
                                            $('#btn-palliative-report3').prop('disabled',false);
                                        });
                                    }
                                });");
                                ?>
                            </div>
                        </div>
                        <br>
                        <div id="report-palliative3"></div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading" role="tab" id="heading4">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            รายงานสรุปตามเขตบริการสุขภาพ
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="h4">รายงานระหว่างวันที่ </span> <span class="h4 date_start"></span> ถึง <span class="h4 date_end"></span><br>
                                <?=Html::button('<span class="fa fa-search"></span> แสดงรายงาน', [
                                    'id' => 'btn-palliative-report4',
                                    'class' => 'btn btn-success btn-md',
                                ]);?>
                                <?php
                                $this->registerJs("$('#btn-palliative-report4').on('click',function(){
                                    var date_start = $('#palliative_date_start').val();
                                    var date_end = $('#palliative_date_end').val();
                                    var zone_code = $('#tpc_area_zone').val();
                                    var pay_right = $('#tpc_pay_right').val();
                                    
                                    //
                                    $('.date_start').text(date_start);
                                    $('.date_end').text(date_end);
                                    
                                    if(date_start==''){
                                        alert('กรุณาเลือกช่วงเวลา เริ่มแสดงรายงาน');
                                    }
                                    else if(date_end==''){
                                        alert('กรุณาเลือกช่วงเวลา สรุปแสดงรายงาน');
                                    }else if(zone_code==''){
                                        alert('กรุณาเลือกเขตสถานบริการ');
                                    }else{
                                        $('#report-palliative4').html('<i class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></i><span class=\"sr-only\">Loading...</span>');
                                        //$(this).prop('disabled',true);
                                        $.post('".Url::to(['timeline-event/palliative-report', 'type'=>'report4'])."',{date_start:date_start, date_end:date_end, zone_code:zone_code, pay_right:pay_right},function(result){
                                            $('#report-palliative4').html(result);
                                            $('#btn-palliative-report4').prop('disabled',false);
                                        });
                                    }
                                });");
                                ?>
                            </div>
                        </div>
                        <br>
                        <div id="report-palliative4"></div>
                    </div>
                </div>
            </div>

        </div><!--//end col-->


    <?php
    }//end palliative

    //for all site
    $this->registerJs('
     $("#watch-video").on("click", function() {
        modalUserPanel($(this).attr(\'data-url\'));
    });',  yii\web\View::POS_END);

    $loadIconData = '\'<i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i>\'';

//    if(Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th') {
//        echo '<div class="container" >';
//	
//	
//	
//            echo '<div class="row">';
//                echo '<div class="col-md-12 box" style="padding: 15px 30px;">';
//                    echo '<span class="h3"><li class="fa fa-building"></li> ';
//
//                    echo $model ." : " . $dataHospital['name']." ";
//                    echo '<il class="fa fa-globe"></il> ';
//                    echo \yii\helpers\BaseHtml::a("{$http}://".$frontendUrl, "{$http}://".$frontendUrl).'</span>';
//
//                    Modal::begin([
//                        'id' => 'modal-move-site',
//                        'header' => '<h2>เปลี่ยนหน่วยบริการ</h2>',
//                        'toggleButton' => ['label' => 'เข้าทำงานในหน่วยบริการอื่น','class' => 'btn btn-info btn-sm pull-right'],
//                        'closeButton' => ['label' => 'ยกเลิก','class' => 'btn btn-success btn-sm pull-right'],
//                    ]);
//
//                        echo \appxq\sdii\widgets\GridView::widget([
//                            'id' => 'user-allow-grid',
//                            'dataProvider' => $userallowdata,
//                            //'filterModel' => $searchModel,
//                            'columns' => [
//                                [
//                                    'class' => 'yii\grid\SerialColumn',
//                                    'headerOptions' => ['style'=>'text-align: center;'],
//                                    'contentOptions' => ['style'=>'width:60px;text-align: center;'],
//                                ],
//
//                                [
//                                    'header' => 'หน่วยงานที่สามารถเข้าไปทำงาน',
//                                    'value' => function ($model) {
//                                        $hospital = app\modules\adddoctor\models\AllHospitalThai::find()->where(['hcode'=>$model->sitecode])->one();
//                                        return $model->sitecode . " " . $hospital->name;
//                                    }
//                                ],
//                                [
//                                    'class' => 'appxq\sdii\widgets\ActionColumn',
//                                    'contentOptions' => ['style'=>'width:80px;text-align: center;'],
//                                    'template' => '{changesite}',
//                                    'buttons' => [
//                                        'changesite' => function ($url, $model) {
//                                               return Html::a('<span class="glyphicon glyphicon-log-in"></span> เข้าสู่การทำงาน', $url, [
//                                                        'title' => Yii::t('app', 'เข้าสู่การทำงาน'),
//                                                        'class'=>'btn btn-info btn-xs',
//                                                        'data-confirm' => Yii::t('yii', 'คุณแน่ใจหรือไม่ที่จะเข้าสู่การทำงานในหน่วยงานดังกล่าว?'),
//                                                        'data-method' => 'post',
//                                                        'data-action' => ['sitecode'=>$model->sitecode],
//                                                        'data-pjax'=>'0',
//                                                ]);
//                                        },
//                                    ],
//                                ],
//
//                            ],
//                        ]);
//                        echo "<div>ท่านสามารถแจ้งผู้ดูแลระบบของแต่ละหน่วยบริการเพื่อเพิ่มท่านเข้าไปในรายชื่อที่อนุญาตให้เข้าใช้งานได้ ที่เมนู ตั้งค่า -> สมาชิกในหน่วยงาน </div>";
//                    Modal::end();
//
//                    echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="watch-video" href="#" data-url="/inputdata/watch-video/?type=cascap" class="btn btn-default btn-sm" href="#"><i class="fa fa-youtube-play"></i> VDO สอนการใช้งานเบี้องต้น</a>';
//                    //echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="watch-video" href="#" data-url="/inputdata/watch-video/?type=cascap" class="btn btn-default" style="margin: 10px 0px 0px 0px;"><il class="fa fa-2x fa-youtube-play text-danger"></il> <span  class="h4">VDO สอนการใช้งานเบี้องต้น</span></a>';
//                echo '</div>';
//            echo '</div>';
//	    echo $this->render('/tccbots/_mym');
//            echo '<div class="row">';
//                echo '<div class="col-md-12 box" style="padding-top: 15px;">';
//                    echo '<div class="alert" id="report-overview-info">';
//                        echo Html::a('<label class="btn btn-danger btn-lg" role="button">ผลการตรวจสอบความถูกต้องของเอกสาร เพื่อคำนวณค่าตอบแทน</label>',
//                            Url::to('/icf/total'),
//                            [
//                                'target'=>'_self'
//                            ]);
//                        echo Html::a(
//                            '<label class="text-primary" style="cursor: pointer;"><i class="fa fa-check"></i> กำกับงาน CASCAP</label>',
//                            Url::to('/icf/total'),
//                            [
//                                'target'=>'_blank'
//                            ]);
//                        echo '<br/><br/>';
//                        echo '<label id="report-overview-lastcal"></label> ';
//                        echo '<button class="btn btn-primary" id="report-overview-refresh" sitecode="'.$mysitecode.'">Refresh <i class="fa fa-refresh"></i></button>';
//                        echo '<br/><br/>';
//                        echo '<label id="report-overview-daterange"></label> ';
//
//                    echo '</div>';
//                    echo '<div class="alert table-responsive" id="report-overview-container">';
//                        echo $table;
//                    echo '</div>';
//                echo '</div>';
//            echo '</div>';
//        echo '</div>';
//        $this->registerJs('
//            $("#report-overview-refresh").off().click(function(){
//
//                $(this).children("i").addClass("fa-spin");
//                $("#overview-report-table").addClass("fadeout");
//
//                $.ajax({
//                    type    : "GET",
//                    cache   : false,
//                    url     : "'.Url::to('/timeline-event/report-overview-refresh/').'",
//                    data    : {
//                        sitecode: $(this).attr("sitecode"),
//                    },
//                    success  : function(response) {
//                        $("#report-overview-container").html(response);
//                        $("#overview-report-table").removeClass("fadeout");
//                        $("#report-overview-refresh i").removeClass("fa-spin");
//                    },
//                    error : function(){
//                        $("#report-overview-refresh i").removeClass("fa-spin");
//                    }
//                });
//            });         
//        ');
//    }
?>
<img style="display: none;" src="<?=  Url::to(['site/send-mail']) ?>">
