<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Ezform */

$this->title = 'Create Ezform';
$this->params['breadcrumbs'][] = ['label' => 'Ezforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
