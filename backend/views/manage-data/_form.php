<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Ezform */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ezform-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ezf_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ezf_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ezf_detail')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ezf_table')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_create')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'user_update')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'update_date')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'shared')->textInput() ?>

    <?= $form->field($model, 'public_listview')->textInput() ?>

    <?= $form->field($model, 'public_edit')->textInput() ?>

    <?= $form->field($model, 'public_delete')->textInput() ?>

    <?= $form->field($model, 'comp_id_target')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
