<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Ezform */

$this->title = $model->ezf_id;
$this->params['breadcrumbs'][] = ['label' => 'Ezforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ezf_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ezf_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ezf_id',
            'ezf_name',
            'ezf_detail:ntext',
            'ezf_table',
            'user_create',
            'create_date',
            'user_update',
            'update_date',
            'status',
            'shared',
            'public_listview',
            'public_edit',
            'public_delete',
            'comp_id_target',
        ],
    ]) ?>

</div>
