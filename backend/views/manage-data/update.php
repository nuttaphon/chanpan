<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Ezform */

$this->title = 'Update Ezform: ' . ' ' . $model->ezf_id;
$this->params['breadcrumbs'][] = ['label' => 'Ezforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ezf_id, 'url' => ['view', 'id' => $model->ezf_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ezform-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
