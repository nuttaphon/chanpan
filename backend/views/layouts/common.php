<?php
/**
 * @var $this yii\web\View
 */
use backend\widgets\Menu;
use common\models\TimelineEvent;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\models\SiteUrl;
use common\lib\sdii\components\helpers\SDNoty;
$siteconfig = \app\models\SiteConfig::find()->One();

$js = <<< JS
$("#btn-user-info").on("click", function() {
    modalUserPanel($(this).attr('data-url'));
});
$("#btn-notification").on("click", function() {
    modalUserPanel($(this).attr('data-url'));
     $("#btn-notification").stop().fadeOut();
     $("#btn-notification").stop().fadeIn();
});
$("#btn-help-desk").on("click", function() {
    modalUserPanel($(this).attr('data-url'));
});
function modalUserPanel(url) {
    $('#modal-user-panel .modal-content').html('<div class="sdloader "><i class="sdloader-icon"></i></div>');
    $('#modal-user-panel').modal('show')
    .find('.modal-content')
    .load(url);
}
(function blink() {
    $('#btn-notificationx').fadeOut(300).fadeIn(300, blink);
})();
/*
$(function() {
    habla_link_div();
    function habla_link_div() {
        setTimeout(habla_link_div,4000);
        $('#tawktoLink').remove();
    }
})
*/
JS;
// register your javascript
$this->registerJs($js, \yii\web\View::POS_END);
?>
<?php
//check_notify
$user_id=Yii::$app->user->identity->id;
$check_total= backend\modules\notification\controllers\NotificationController::CheckNotify($user_id);
$jsNotify = <<< JS
var total_count=parseInt($("#Notify_Count").attr('total_count'));
//alert(total_count);
if(total_count>0){
$("#btn-notification").attr( "class"," btn btn-xs btn-danger");

}
JS;
// register your javascript
$this->registerJs($jsNotify);
?>
<?php

if(Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){
    $this->registerJs("

    $.ajax({
	method: 'POST',
	url: '".Url::to(['/site/checkfile'])."',
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $('#modal-alert .modal-content').html(result.message);
	    $('#modal-alert').modal('show');
	}
    });


    ");
}


?>

<?php $this->beginContent('@backend/views/layouts/base.php'); ?>
<div class="wrapper" >

        <!-- header logo: style can be found in header.less -->
        <header class="main-header">
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
          <?php
            

            NavBar::begin([
                'id' => 'top-navbar',
                'brandLabel' => '<span class="fa fa-home"></span>  <b>'.$siteconfig->sitename.'</b> '. (Yii::$app->user->can('administrator') ?
                    '<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only"></span>
                </a>' : '<a href="#" role="button"></a>'). '&nbsp;&nbsp; <button id="btn-notification" title="เปิดข้อความ" data-url="/notification/notification/list-notify" style="margin-top : 14px;" class="btn btn-xs btn-default"><i class="fa fa-envelope"></i> <span id="Notify_Count" total_count="'.$check_total.'">(<blink>'.$check_total.'</blink>)</span></button> <button id="btn-help-desk" title="ขอความช่วยเหลือ" data-url="/forum/forum-topic/help-desk" style="margin-top : 14px;" class="btn btn-xs btn-info"><i class="fa fa-heart"></i> <span>Help</span></button>',  //Yii::$app->name,
                'brandUrl' =>Yii::getAlias('@frontendUrl'),
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
                'innerContainerOptions' => ['class'=>'container-fluid'],
            ]);?>


                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">

                        </li>
                    </ul>
                </div>
        <?php
        $menu_items = Yii::$app->db->createCommand("select id, titlename as label, url, target from widget_backend_menu where type = 'report';")->queryAll();
        foreach($menu_items as $key => $val){
            if($val['target']=='_blank'){
                $menu_items[$key]['linkOptions'] = array('target' => '_blank');
            }
        }
        if(Yii::$app->user->can('administrator')) {
            array_push($menu_items,
                [
                    'label' => '+ Add Menu',
                    'url' => Yii::getAlias('@backendUrl') . '/widget-backend-menu/create',
                ]
            );
        }
        //set module name
        $moduleName1 = Yii::$app->controller->id;
        $moduleName2 = Yii::$app->controller->action->id;
        $moduleName3 = Yii::$app->controller->module->id;
        //echo $moduleName1.'/'; echo $moduleName2.'/'; echo $moduleName3; exit;
        //\yii\helpers\VarDumper::dump($menu_items, 10, true);
        echo Nav::widget([
        'id' => 'top-nav',
        'options' => ['class' => 'nav navbar-nav navbar-right'],
        'items' => [
//            [
//                'label' => 'Demo', 'url' => 'https://clouddemo.cascap.in.th',
//                'encode' => false,
//                'visible' => getenv('BACKEND_URL') == 'https://cloudbackend.cascap.in.th' ? true : false,
//            ],
//            [
//                'label' => 'CASCAP หลัก', 'url' => 'https://cloudbackend.cascap.in.th',
//                'encode' => false,
//                'visible' => getenv('BACKEND_URL') == 'https://clouddemo.cascap.in.th' ? true : false,
//            ],
            ['label' => Yii::t('backend', '<span class="fa fa-university"></span> หน้าหลัก'), 'url' => ['/'], 'encode' => false, 'active'=> $moduleName1 == 'timeline-event' ? true : false],
			['label' => Yii::t('backend', '<span class="fa fa-book"></span> งานวิจัย'), 'url' => ['/article/research/index'], 'encode' => false, 'active'=>$moduleName3 == 'article' ? true : false],
	    
            ['label' => Yii::t('backend', '<span class="fa fa-archive"></span> Module'), 'url' => ['/tccbots/my-module'], 'encode' => false, 'active'=>$moduleName2 == 'my-module' ? true : false],
            ['label' => Yii::t('backend', '<span class="fa fa-pencil-square-o"></span> บันทึกข้อมูล'), 'url' => ['/inputdata'], 'encode' => false, 'active'=> $moduleName1 == 'inputdata' ? true : false,
//                'items'=> [
//                    [
//                        'label' => '<span class="fa fa-pencil-square-o"></span>ระบบบันทึกข้อมูล',
//                        'url' => ['/inputdata/index'],
//                        'active'=> $moduleName1 == 'inputdata/index' ? true : false,
//                        'encode' => false,
//                    ],
//                    [
//                        'label' => '<span class="fa fa-comments-o"></span>ระบบขอคำปรึกษา',
//                        'url' => ['/inputdata/consult'],
//                        'active'=> $moduleName2 == 'consult' ? true : false,
//                        'encode' => false,
//                    ],
//                ]
            ],
            [
                'label' => Yii::t('backend', '<span class="fa fa-table"></span> จัดการข้อมูล'),
                'active'=> ($moduleName1 == 'query-request' || $moduleName3 =='managedata' || $moduleName1 == 'map' || $moduleName3 =='icf' || $moduleName3 =='appointments')  ? true : false,
                'encode' => false,
		'dropDownOptions'=>['id'=>'manager_drop'],
                'items'=> [
                    [
                        'label' => '<span class="fa fa-database"></span>'.Yii::t('backend', 'จัดการข้อมูล'),
                        'url' => ['/managedata/managedata'],
                        'encode' => false,
                    ],
//                    [
//                        'label' => '<span class="fa fa-check-square-o"></span>'.Yii::t('backend', 'Validation tools'),
//                        'visible'=> (Yii::$app->keyStorage->get('frontend.domain')=='thaipalliative.org' ? false : true),
//                        'url' => ['/query-request'],
//                        'encode' => false,
//                    ],
		    [
                        'label' => '<span class="glyphicon glyphicon-hourglass"></span>'.Yii::t('backend', 'Validation tools'),
                        'url' => ['/script-validate/index'],
                        'encode' => false,
                        'visible'=>Yii::$app->user->can('administrator')
                    ],
		    [
                        'label' => '<span class="glyphicon glyphicon-list-alt"></span>'.Yii::t('backend', 'Results'),
                        'url' => ['/script-validate/group'],
                        'encode' => false,
                        'visible'=>Yii::$app->user->can('administrator')
                    ],
		    [
                        'label' => '<span class="glyphicon glyphicon-share"></span>'.Yii::t('backend', 'Ezform Exports'),
                        'url' => ['/ezforms2/ezform-export/index'],
                        'encode' => false,
                        'visible'=>Yii::$app->user->can('administrator')
                    ],
                    [
                        'label' => '<span class="glyphicon glyphicon-ok"></span>'.Yii::t('backend', 'กำกับงาน CASCAP'),
                        'visible'=> (Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' ? true : false),
                        'url' => ['/icf/total'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-file-o"></span>'.Yii::t('backend', 'เพิ่มรายชื่อแพทย์'),
                        'visible'=> (Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' ? true : false),
                        'url' => ['/adddoctor'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-calendar"></span>'.Yii::t('backend', 'ติดตามผล / นัดหมาย'),
                        'visible'=> (Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th' ? true : false),
                        'url' => ['/appointments'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-file-o"></span>'.Yii::t('backend', 'Query tools'),
                        'url' => ['/query-request'],
                        'encode' => false,
                    ],
		            [
                        'label' => '<span class="fa fa-map-marker"></span>'.Yii::t('backend', 'Map'),
                        'url' => ['/map'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<span class="glyphicon glyphicon-bed"></span>'.Yii::t('backend', 'Refer'),
                        'visible'=> (Yii::$app->keyStorage->get('frontend.domain')=='thaipalliative.org' ? true : false),
                        'url' => ['/refer/refer'],
                        'encode' => false,
                    ],

                ]
            ],
            [
                'label' => Yii::t('backend', '<span class="fa fa-file-text-o"></span> รายงาน'),
                'active'=> ($moduleName1 == 'researcher-cohort' || $moduleName1 == 'verification')  ? true : false,
                'encode' => false,
                'items'=> $menu_items,
		'dropDownOptions'=>['id'=>'report_drop'],

            ],
            [
                'label'=>Yii::t('backend', '<span class="fa fa-cogs"></span> ตั้งค่า'),
                'encode' => false,
                'active' => in_array($moduleName1, ['tbl-tree', 'ezform', 'userlist', 'user', 'move-units']) ?  true : false,
		'dropDownOptions'=>['id'=>'setting_drop'],
                'items'=>[
                    [
                        'label' => '<span class="fa fa-send"></span>'.Yii::t('backend', 'จัดกิจกรรม'),
                        'url' => ['/tbl-tree'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-file-text"></span>'.Yii::t('backend', 'จัดการฟอร์ม'),
                        'url' => ['/ezforms/ezform'],
                        'visible'=>Yii::$app->user->can('manager'),
                        'encode' => false,
                    ],
		    [
                        'label' => '<span class="fa fa-cube"></span>'.Yii::t('backend', 'จัดการโมดูล'),
                        'url' => ['/inv/inv-gen/index'],
                        'visible'=>Yii::$app->user->can('manager'),
                        'encode' => false,
                    ],
		    [
                        'label' => '<span class="glyphicon glyphicon-thumbs-up"></span>'.Yii::t('backend', 'อนุมัติโมดูล'),
                        'url' => ['/inv/inv-gen/auth'],
                        'visible'=>Yii::$app->user->can('administrator'),
                        'encode' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-link"></span>'.Yii::t('backend', 'จัดการ Site'),
                        'url' => ['/userlist/page-setting'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-user-secret"></span>'.Yii::t('backend', 'สมาชิกในหน่วยงาน'),
                        'url' => ['/userlist/index'],
                        'encode' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-users"></span>'.Yii::t('backend', 'สมาชิกทั้งหมด'),
                        'url' => ['/user/index'],
                        'encode' => false,
                        'visible'=>Yii::$app->user->can('administrator'),
                    ],
		    [
                        'label' => '<span class="glyphicon glyphicon-random"></span>'.Yii::t('backend', 'สมาชิก cascap tools'),
                        'url' => ['/user/tool'],
                        'encode' => false,
                        'visible'=>Yii::$app->user->can('administrator') && (Yii::$app->keyStorage->get('frontend.domain')!='cascap.in.th' ? false : true),
                    ],
                    [
                        'label' => '<span class="fa fa-exchange"></span>'.Yii::t('backend', 'อนุมัติการขอย้ายหน่วยงาน'),
                        'url' => ['/move-units'],
                        'encode' => false,
                        'visible'=>Yii::$app->user->can('administrator')
                    ],
		    
                ]

            ],
            '<li id="btn-user-info" style="cursor:pointer; margin: 5px 15px 5px 10px;" data-url="/sign-in/show-user-panel">'.
            Html::img(Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg', ['class'=>'img-rounded', 'height' => '40', 'width' => '40',])
            .'</li>',
        ],
    ]);

	?>

    <?php NavBar::end(); ?>
            </nav>
            <?php
                function myBrowserInfo(){
                    $ua = $_SERVER['HTTP_USER_AGENT'];
                    $os_platform = "unknown";
                    $os_array = array(
                        '/windows nt 6.2/i' => 'Windows 8',
                        '/windows nt 6.1/i' =>  'Windows 7',
                        '/windows nt 6.0/i' =>  'Windows Vista',
                        '/windows nt 5.2/i' =>  'Windows Server 2003/XP x64',
                        '/windows nt 5.1/i' =>  'Windows XP',
                        '/windows xp/i'     =>  'Windows XP',
                        '/windows nt 5.0/i' =>  'Windows 2000',
                        '/windows me/i'     =>  'Windows ME',
                        '/win98/i'          =>  'Windows 98',
                        '/win95/i'          =>  'Windows 95',
                        '/win16/i'          =>  'Windows 3.11',
                        '/macintosh|mac os x/i' =>  'Mac OS X',
                        '/mac_powerpc/i'    =>  'Mac OS 9',
                        '/linux/i'          =>  'Linux',
                        '/ubuntu/i'         =>  'Ubuntu',
                        '/iphone/i'         =>  'iPhone',
                        '/ipod/i'           =>  'iPod',
                        '/ipad/i'           =>  'iPad',
                        '/android/i'        =>  'Android',
                        '/blackberry/i'     =>  'BlackBerry',
                        '/webos/i'          =>  'Mobile'
                    );
                    foreach ($os_array as $regex => $value) {
                        if (preg_match($regex, $ua)) {
                            $os_platform    =   $value;
                        }
                    }
                    $os = 'unknown';
                    if( preg_match('/windows/i', $os_platform ) ){
                        $os = 'windows';
                    }else if( in_array( $os_platform, array('iPhone','iPod','iPad','Mac OS X','Mac OS 9') ) ){
                        $os = 'apple';
                    }else if($os_platform == 'Android'){
                        $os = 'android';
                    }
                    $browser = 'unknown';
                    if(preg_match('/chrome/i', $ua)){
                        $browser = 'Chrome';
                        $browserName = 'Chrome';
                    }else if(preg_match('/CriOS/i', $ua)){
                        $browser = 'CriOS';
                        $browserName = 'Chrome';
                    }else if(preg_match('/firefox/i', $ua)){
                        $browser = 'Firefox';
                        $browserName = 'Mozilla Firefox';
                    }else if(preg_match('/msie/i', $ua)){
                        $browser = 'MSIE';
                        $browserName = 'Internet Explorer';
                    }else if(preg_match('/safari/i', $ua)){
                        $browser = 'Safari';
                        $browserName = 'Safari';
                    }
                    $delimiter = $browser == 'MSIE' ? ';' : ' ';
                    $spliter = $browser == 'MSIE' ? ' ' : '/';
                    $startv = strpos($ua, $browser);
                    $endv = strpos($ua, $delimiter, $startv);
                    if(!$endv || $endv >= strlen($ua)){
                        $endv = strlen($ua);
                    }
                    $browserdata = substr($ua, $startv, $endv-$startv);
                    $browserVersion = explode($spliter, $browserdata)[1];
                    $browserVersion = (!isset($browserVersion) || $browserVersion == '') ? 'unknown' : $browserVersion ;
                    $download = false;
                    $chromeurl ='https://www.google.com/intl/th/chrome/browser/';
                    if( !( ($browser=='Chrome' || $browser=='CriOS') && (explode('.', $browserVersion)[0]) >= 49 ) ){
                        $download = true;
                        if($os=='apple'){
                            $chromeurl = 'https://itunes.apple.com/app/chrome/id535886823/';
                        }else if($os=='android'){
                            $chromeurl = 'https://play.google.com/store/apps/details?id=com.android.chrome/';
                        }
                    }
                    return [
                        'ua'=>$ua,
                        'os'=>$os_platform,
                        'vendor'=>$os,
                        'browser'=>$browser,
                        'name'=>$browserName,
                        'version'=>$browserVersion,
                        'versiondec'=>explode('.', $browserVersion)[0].'.'.implode('', array_slice(explode('.', $browserVersion), 1)),
                        'downloadrecommend'=>$download,
                        'chromeurl'=>$chromeurl,
                    ];
                }

                $bif = myBrowserInfo();
                if(Yii::$app->request->get('testchrome')=='yes' || $bif['downloadrecommend'] && (!Yii::$app->session->has('chrome-alert') || Yii::$app->session->get('chrome-alert')!='dismissed' )) {
                    echo '<div id="chrome-alert">
                        <label class="chrome-alert-invis chrome-alert-medium">คุณกำลังใช้งาน Isan Cohort ด้วย '.$bif['name'].' (V. '.$bif['version'].')</label>
                        <label><span class="chrome-alert-small">เพื่อการแสดงผลที่ถูกต้อง </span>กรุณาใช้ <a target="_blank" href="'.$bif['chromeurl'].'">
                            <img src="/img/chromelogo.png" id="chrome-logo-png"> <span class="chrome-alert-invis">Google Chrome</span></a> <span class="chrome-alert-invis">(V. 49 ขึ้นไป)</span>
                        </label>
                        <button class="btn btn-danger" id="chrome-alert-dismiss">
                            <i class="fa fa-close"></i> <span class="chrome-alert-invis"> Dismiss</span>
                        </button>
                    </div>';
                    $this->registerCss('
                        #chrome-alert{

                            display: none;
                            width: 100%;
                            height: 0px;

                            position: fixed;
                            top: 51px;

                            background-color: #f39c12;
                            border-bottom: 1px solid #F44336;

                            overflow: hidden;
                            text-align: center;
                            vertical-align: middle;
                            line-height: 50px;

                            transition: height 0.3s ease-in-out;

                            z-index: 1000;
                        }
                        #chrome-alert-dismiss{
                            position: absolute;
                            margin: 8px 0;
                            right: 5%;
                            transition: width 0.3s ease-in-out;
                        }
                        #chrome-alert > label{
                            margin-bottom: 0;
                            color: #FFF;
                        }
                        #chrome-logo-png{
                            width: 34px;
                            height: 34px;
                            margin-left: 10px;
                        }
                        @media screen and (max-width: 347px){
                            .chrome-alert-small{
                                display: none;
                            }
                        }
                        @media screen and (max-width: 1200px){
                            .chrome-alert-medium{
                                display: none;
                            }
                        }
                        @media screen and (max-width: 768px){
                            #chrome-alert{
                                position: relative;
                                top: 0px;
                                line-height: 50px;
                                padding : 0 5%;
                                text-align: left;
                            }
                            #chrome-alert-dismiss{
                                position: absolute;



                                transition: width 0.3s ease-in-out;
                            }


                            #chrome-alert > label{
                                margin-bottom: 0;

                            }
                            .chrome-alert-invis{
                                display:none;
                            }


                        }
                    ');
                    $this->registerJs('
                        setTimeout(function(){
                            $("#chrome-alert").show().delay(1).queue(function(){
                                $(this).css("height","50px").dequeue();
                            });
                            $("aside.content-wrapper").css("margin-top","50px");
                        },750);
                    ', 5);
                    $this->registerJs('
                        $("#chrome-alert-dismiss").click(function(){
                            $("#chrome-alert").css("height","0px").delay(300).queue(function(){
                                $(this).hide().dequeue();
                            });
                            $("aside.content-wrapper").css("margin-top","0px");

                            $.ajax({
                                type    : "GET",
                                cache   : false,
                                url     : "'.Url::to('/timeline-event/dismiss-chrome-alert/').'",
                                data    : {
                                    dismiss  :   1,
                                },
                                success  : function(response) {
                                    //console.log(response);
                                    //$("#report84-modal-body").html(response);
                                },
                                error : function(){
                                    //$("#report84-modal-body").html("การเรียกดูข้อมูลผิดพลาด");
                                }
                            });
                        });
                    ');
                }
            ?>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php echo Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ?>" class="img-circle" />
                    </div>
                    <div class="pull-left info">
                        <p><?php echo Yii::t('backend', '{username}', ['username'=> Yii::$app->user->identity->getPublicIdentity()]) ?></p>
                        <a href="<?php echo Url::to(['/sign-in/profile']) ?>">
                            <i class="fa fa-circle text-success"></i>
                            <?php echo Yii::$app->formatter->asDatetime(time()) ?>
                        </a>
                    </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?php echo Menu::widget([
                    'options'=>['class'=>'sidebar-menu'],
                    'labelTemplate' => '<a href="#">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                    'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                    'submenuTemplate'=>"\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                    'activateParents'=>true,
                    'items'=>[
                        [
                            'label'=>Yii::t('backend', 'Timeline'),
                            'icon'=>'<i class="fa fa-bar-chart-o"></i>',
                            'url'=>['/timeline-event/index'],
                            'badge'=> TimelineEvent::find()->today()->count(),
                            'badgeBgClass'=>'label-success',
                        ],
                        [
                            'label'=>Yii::t('backend', 'Content'),
                            'icon'=>'<i class="fa fa-edit"></i>',
                            'options'=>['class'=>'treeview'],
                            'items'=>[
                                ['label'=>Yii::t('backend', 'Static pages'), 'url'=>['/page/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'Articles'), 'url'=>['/article/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'Article Categories'), 'url'=>['/article-category/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'Text Widgets'), 'url'=>['/widget-text/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'Menu Widgets'), 'url'=>['/widget-menu/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'Carousel Widgets'), 'url'=>['/widget-carousel/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                            ]
                        ],
                        [
                            'label'=>Yii::t('backend', 'Users'),
                            'icon'=>'<i class="fa fa-users"></i>',
                            'url'=>['/user/index'],
                            'visible'=>Yii::$app->user->can('administrator')
                        ],
			[
                            'label'=>Yii::t('backend', 'Restore Users'),
                            'icon'=>'<i class="glyphicon glyphicon-repeat"></i>',
                            'url'=>['/user/index2'],
                            'visible'=>Yii::$app->user->can('administrator')
                        ],
//			[
//                            'label'=>Yii::t('backend', 'Admin Site'),
//                            'icon'=>'<i class="fa fa-users"></i>',
//                            'url'=>['/user/admin-site'],
//			    'visible'=>(Yii::$app->user->identity->userProfile->sitecode!='')
//                        ],
						[
                            'label'=>Yii::t('backend', 'Authentication'),
                            'icon'=>'<i class="fa fa-street-view"></i>',
                            'url'=>['/admin/assignment'],
                            'visible'=>Yii::$app->user->can('administrator')
                        ],
                        [
                            'label'=>Yii::t('backend', 'System'),
                            'icon'=>'<i class="fa fa-cogs"></i>',
                            'options'=>['class'=>'treeview'],
                            'items'=>[
                                [
                                    'label'=>Yii::t('backend', 'i18n'),
                                    'icon'=>'<i class="fa fa-flag"></i>',
                                    'options'=>['class'=>'treeview'],
                                    'items'=>[
                                        ['label'=>Yii::t('backend', 'i18n Source Message'), 'url'=>['/i18n/i18n-source-message/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                        ['label'=>Yii::t('backend', 'i18n Message'), 'url'=>['/i18n/i18n-message/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                    ]
                                ],
				['label'=>Yii::t('backend', 'EzForm Input'), 'url'=>['/ezforms2/ezform-input/index'], 'icon'=>'<i class="glyphicon glyphicon-grain"></i>', 'visible'=>Yii::$app->user->can('administrator')],
                                ['label'=>Yii::t('backend', 'Config Mode'), 'url'=>['/configmode/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'Key-Value Storage'), 'url'=>['/key-storage/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'File Storage'), 'url'=>['/file-storage/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'Cache'), 'url'=>['/cache/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                ['label'=>Yii::t('backend', 'File Manager'), 'url'=>['/file-manager/index'], 'icon'=>'<i class="fa fa-angle-double-right"></i>'],
                                [
                                    'label'=>Yii::t('backend', 'System Information'),
                                    'url'=>['/system-information/index'],
                                    'icon'=>'<i class="fa fa-angle-double-right"></i>'
                                ],
                                [
                                    'label'=>Yii::t('backend', 'Logs'),
                                    'url'=>['/log/index'],
                                    'icon'=>'<i class="fa fa-angle-double-right"></i>',
                                    //'badge'=>\backend\models\SystemLog::find()->count(),
                                    'badgeBgClass'=>'label-danger',
                                ],
                            ]
                        ]
                    ]
                ]) ?>
            </section>
            <!-- /.sidebar -->
        </aside>

        <?php
        echo Yii::$app->keyStorage->get('tawk');
        echo \common\lib\sdii\widgets\SDModalForm::widget([
            'id' => 'modal-user-panel',
            'size' => 'modal-md',
            'tabindexEnable' => false
        ]);

	 echo \common\lib\sdii\widgets\SDModalForm::widget([
            'id' => 'modal-alert',
            'size' => 'modal-lg',
	     'options'=>['data-backdrop'=>'false']
        ]);
        ?>

        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <?php echo $this->title ?>
                    <?php if (isset($this->params['subtitle'])): ?>
                        <small><?php echo $this->params['subtitle'] ?></small>
                    <?php endif; ?>
                </h1>

                <?php echo Breadcrumbs::widget([
                    'tag'=>'ol',
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>

            <!-- Main content -->
            <section class="content">
                <?php
                    $profile = Yii::$app->user->identity->userProfile;
                    if ($profile->sitecode<>$profile->department) {
                ?>
                <div class="alert alert-warning" role="alert">ท่านกำลังทำงานอยู่ภายใต้หน่วยบริการอื่น [ <?php echo $profile->sitecode;?> ] <?= Html::a('กลับหน่วยงานเดิม', ['/sign-in/goback'], ['class'=>'btn btn-primary btn-xs']) ?></div>
                <?php
                    }
                ?>
                <?php if (Yii::$app->session->hasFlash('alert')):?>
                    <?php echo \yii\bootstrap\Alert::widget([
                        'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                        'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                    ])?>
                <?php endif; ?>
                <?php echo $content ?>
            </section><!-- /.content -->
        </aside><!-- /.right-side -->
    </div><!-- ./wrapper -->

<?php $this->endContent(); ?>
