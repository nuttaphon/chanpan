<?php
use kartik\tabs\TabsX;
/**
 * @var $this yii\web\View
 */
        $active[$this->context->activetab]=true;
        
        $items = [
            [
                'label'=>'<i class="fa fa-home"></i> หน้าแรก',
                'active'=>$active['home'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckd/default/redirect?tab='])]                           
            ],
            [
                'label'=>'<i class="fa fa-check-square-o"></i> กำกับงาน',
                'active'=>$active['inv'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckd/default/redirect?tab=inv'])]                
            ],
            [
                'label'=>'<i class="fa fa-male"></i> EMR',
                'active'=>$active['emr'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckd/default/redirect?tab=emr'])]
            ],
            [
                'label'=>'<i class="fa fa-area-chart"></i> รายงาน',
                'active'=>$active['report'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckd/default/redirect?tab='])]                
            ],
            [
                'label'=>'<i class="fa fa-map-marker"></i> แผนที่',
                'active'=>$active['map'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckd/default/redirect?tab='])]                
            ],
            [
                'label'=>'<i class="fa fa-table"></i> Matrix',
                'active'=>$active['matrix'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckd/default/redirect?tab='])]
            ],
        ];        
        $tab = Yii::$app->request->get('tab');
?>
<?php $this->beginContent('@backend/views/layouts/common.php'); ?>
    <div class="box">
        <div class="box-body">
            <?php
            echo TabsX::widget([
                'items'=>$items,
                'position'=>TabsX::POS_ABOVE,
                'encodeLabels'=>false
            ]);
            ?>            
            <?php echo $content ?>
        </div>
    </div>
<?php $this->endContent(); ?>