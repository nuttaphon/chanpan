<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\QueryReply */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="query-reply-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'query_id')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'query_comment')->textarea(['rows' => 6]) ?>

    <?php echo $form->field($model, 'create_by')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'create_at')->textInput() ?>

    <?php echo $form->field($model, 'update_at')->textInput() ?>

    <?php echo $form->field($model, 'rstat')->textInput() ?>

    <?php echo $form->field($model, 'file_upload')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'bookmark')->textInput() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
