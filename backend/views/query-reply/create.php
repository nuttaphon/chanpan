<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\QueryReply */

$this->title = 'Create Query Reply';
$this->params['breadcrumbs'][] = ['label' => 'Query Replies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="query-reply-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
