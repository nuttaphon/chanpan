<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ReportDynamic */

$this->title = 'Create Dynamics Report';
$this->params['breadcrumbs'][] = ['label' => 'Report Dynamics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-dynamic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
