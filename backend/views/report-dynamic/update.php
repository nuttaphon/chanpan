<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ReportDynamic */

$this->title = 'Update Dynamics Report: ' . ' ' . $model->titlename;
$this->params['breadcrumbs'][] = ['label' => 'Report Dynamics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->titlename, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="report-dynamic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
