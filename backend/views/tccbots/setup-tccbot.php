<?php
/**
 * Created by PhpStorm.
 * User: iam
 * Date: 22/1/2559
 * Time: 12:21
 */
use yii\helpers\Html;
use kartik\grid\GridView;
?>
    <div class="container-fluid">
        <span class="text-bold" style="font-size: 30px;">Thai Database Connector (TDC)</span>&nbsp; <a href="https://backend.thaicarecloud.org/tccadmin/buffe-data-table" class="btn btn-primary pull-right"><li class="fa fa-cogs"></li> View & Trigger setup</a><hr>
        <div class="jumbotron">
            <span class="text-bold" style="font-size: 35px;">เงื่อนไขการใช้งาน</span>
            <p>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thai Care Cloud เป็นระบบ web application ที่พัฒนาขึ้นภายใต้ข้อตกลงความร่วมมือระหว่างกรมการแพทย์ กระทรวงสาธารณสุข กับมหาวิทยาลัยขอนแก่น เพื่อเป็นฐานข้อมูลสุขภาพ ที่มุ่งเอื้อให้บริการทางการแพทย์และสาธารณสุข มีประสิทธิภาพ โดยที่มีการรักษาความลับของผู้ป่วย ด้วยการเข้ารหัสก่อนส่งผ่านข้อมูลด้วยระบบ Secure Sockets Layer (SSL) 128 bits นอกจากนั้น ยังมีการเข้ารหัสเลขประจำตัวประชาชน ชื่อ และนามสกุลผู้รับบริการอีกชั้นหนึ่ง ที่มีเฉพาะหน่วยบริการเจ้าของข้อมูลเท่านั้นที่สามารถถอดรหัสได้
            </p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ดังนั้น หากมีการรั่วไหลของข้อมูล รหัสเลขประจำตัวประชาชน ชื่อ และนามสกุลผู้รับบริการ ถือเป็นความรับผิดชอบของหน่วยบริการสาธารณสุขที่เข้าร่วมโครงการ และเป็นเรื่องที่นอกเหนือความรับผิดชอบของคณะผู้พัฒนาระบบ Thai Care Cloud </p>
<!--            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การที่ท่านคลิก [ยอมรับ] หมายถึงท่านได้ยอมรับเงื่อนไขการใช้งานข้างต้น </p><hr>-->
            <div class="pull-right"><?php //echo \yii\bootstrap\Html::button('ยอมรับ', ['class' => 'btn btn-success btn-lg', 'onclick'=>'$("#setup").fadeIn(); $(this).attr("disabled", true); $("#dismiss").attr("disabled", true);']); echo ' '.\yii\bootstrap\Html::button('ไม่ยอมรับ', ['id'=>'dismiss','class' => 'btn btn-danger btn-lg']);?></div><br>
        </div>

        <hr>


        <h3>Source Code</h3>
        Webservice : <?php echo \yii\bootstrap\Html::a('https://github.com/damasac/buffe_webservice', 'https://github.com/damasac/buffe_webservice', ['target'=>'_blank']); ?><br>
        TCC-Bot application : <?php echo \yii\bootstrap\Html::a('https://github.com/damasac/buffe_webservice', 'https://github.com/damasac/buffe-application', ['target'=>'_blank']); ?><br>
        <hr>

        <h3>ตัวติดตั้ง</h3>
        <iframe  src="https://storage.thaicarecloud.org/source/app/v2/" height="500" width="100%" scrolling="no" marginheight="0" marginwidth="0" frameborder="0"></iframe>
        <hr>



    </div>
