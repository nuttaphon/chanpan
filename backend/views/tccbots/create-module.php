

        <h1 class="modal-title" id="itemModalLabel">สร้าง Module</h1>
        <span class="pull-left"><?php echo \yii\bootstrap\Html::button('< Back', [
                'class' => 'tcc-btn-add btn btn-success btn-md',
                'title' => Yii::t('app', 'สร้างโมดูลใหม่'),
                'data-url'=>\yii\helpers\Url::to(['tccbots/modules', ]),
                'onclick' => 'location.href=\''.\yii\helpers\Url::to(['tccbots/modules', ]).'\';'
            ]);?></span>
<br><br>
        <form class="form-horizontal">
            <fieldset>

                <!-- Form Name -->
<hr>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Name</label>
                    <div class="col-md-4">
                        <input id="textinput" name="textinput" type="text" placeholder="placeholder" class="form-control input-md">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">URL</label>
                    <div class="col-md-4">
                        <input id="textinput" name="textinput" type="text" placeholder="placeholder" class="form-control input-md">

                    </div>
                </div>

                <!-- File Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="filebutton">Icon</label>
                    <div class="col-md-4">
                        <input id="filebutton" name="filebutton" class="input-file" type="file">
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textarea">Detail</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="textarea" name="textarea"></textarea>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <button id="singlebutton" name="singlebutton" class="btn btn-primary">Submit</button>
                    </div>
                </div>

            </fieldset>
        </form>


