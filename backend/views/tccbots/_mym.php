    <div class=" box">   
<div class="modal-header" style="margin-bottom: 15px;">
    <h3 class="modal-title" id="itemModalLabel">My Modules <?=  \yii\helpers\Html::a('<i class="glyphicon glyphicon-plus"></i> นำเข้าจาก Public Modules', ['/tccbots/my-module', 'public'=>1], ['class'=>'btn btn-link btn-sm'])?></h3>
</div>
<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
	$arrayFav = array_keys($arrayMap);
	?>
	    
	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModuleList($userId);

	if($dataModules){
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a href="<?=$linkModule?>">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong></h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } else {
	    if(count($arrayFav)==0){
		echo '<div class="col-md-4"><cite>ไม่พบโมดูล</cite></div>';
	    }
	}
	?>
    </div>
</div>
</div>