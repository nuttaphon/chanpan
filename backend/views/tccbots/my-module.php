<?php
use yii\helpers\Url;
use common\lib\sdii\components\helpers\SDNoty;

$public = isset($_GET['public'])?$_GET['public']:0;
?>
<?= \yii\bootstrap\Nav::widget([
    'items' => [
	[
	    'label' => 'My Modules',
	    'url' => Url::to(['/tccbots/my-module']),
	    'active'=>$public==0,
	],
        [
	    'label' => 'Assigned Modules',
	    'url' => Url::to(['/tccbots/my-module', 'public'=>2]),
	    'active'=>$public==2,
	],
	[
	    'label' => 'Public Modules',
	    'url' => Url::to(['/tccbots/my-module', 'public'=>1]),
	    'active'=>$public==1,
	],
    ],
    'options' => ['class'=>'nav nav-tabs'],
]);
?>
<div id="main-app">
<?php if($public==0):?>
<div class="modal-header" style="margin-bottom: 15px;">
    <h3 class="modal-title" id="itemModalLabel">My Modules <?=  \yii\helpers\Html::a('<i class="glyphicon glyphicon-plus"></i> นำเข้าจาก Public Modules', ['/tccbots/my-module', 'public'=>1], ['class'=>'btn btn-link btn-sm'])?></h3>
</div>
<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
	$arrayFav = array_keys($arrayMap);
	?>
	    
	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModuleList($userId);

	if($dataModules){
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a href="<?=$linkModule?>">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong> <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>" class="info-app"><i class="glyphicon glyphicon-info-sign"></i></a></h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } else {
	    if(count($arrayFav)==0){
		echo '<div class="col-md-4"><cite>ไม่พบโมดูล</cite></div>';
	    }
	}
	?>
    </div>
</div>


<div class="modal-header" style="margin-bottom: 15px;">
    <h3 class="modal-title" id="itemModalLabel">System Modules</h3>
</div>
<div class="modal-body">
    <div class="row">

	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModuleListSys();

	if($dataModules){
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a href="<?=$linkModule?>">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?= $gname?></strong> <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>" class="info-app"><i class="glyphicon glyphicon-info-sign"></i></a></h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } 
	?>
	
    </div>
</div>
<?php elseif($public==2):?>

<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	
//	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
//	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
//	$arrayFav = array_keys($arrayMap);

	?>
	
	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModuleAssign($userId);
	//appxq\sdii\utils\VarDumper::dump($dataModules);
	if($dataModules){
	    
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>" class="info-app">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong> </h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } 
	?>
	
    </div>
</div>
    <?php else:?>

<div class="modal-body">
    <div class="row">
	<?php
	$userId = Yii::$app->user->id;
	
//	$dataFav = backend\modules\inv\classes\InvQuery::getModuleListFavorite($userId);
//	$arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'user_id');
//	$arrayFav = array_keys($arrayMap);

	?>
	
	<?php

	$dataModules = backend\modules\inv\classes\InvQuery::getModulePublic($userId);
	//appxq\sdii\utils\VarDumper::dump($dataModules);
	if($dataModules){
	    
	?>
	<?php foreach ($dataModules as $key => $value):?>
	<div class="col-xs-3 col-md-2" style="margin-bottom: 20px;">
		<div class="media-left">
		    <?php
			$linkModule = '';
			if($value['gtype']==1){
//			    $pos = strpos($value['glink'], 'http');
//			    if ($pos === false) {
//				$linkModule = \yii\helpers\Url::to($value['glink']);
//			    } else {
//				$linkModule = $value['glink'];
//			    }
			    $linkModule = \yii\helpers\Url::to($value['glink']);
			} else {
			    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
			}
			
			$gname = yii\helpers\Html::encode($value['gname']);
			$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
			$len = 12;
			if($checkthai!=''){
			    $len = $len*3;
			}
			if(strlen($gname)>$len){
			    $gname = substr($gname, 0, $len).'...';
			}
		    ?>
		    <a style="cursor: pointer;" data-url="<?=Url::to(['/inv/inv-person/info-app', 'gid'=>$value['gid']])?>" class="info-app">
			<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
		    </a>
		    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?=  $gname?></strong> </h4>
		</div>
		
	    </div>
	<?php endforeach;?>
	<?php } 
	?>
	
    </div>
</div>
<?php endif;?>
</div>

<?=    \appxq\sdii\widgets\ModalForm::widget([
    'id' => 'modal-app',
    //'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
    
$('#main-app').on('click', '.info-app', function() {
    modalApp($(this).attr('data-url'));
});    

function modalApp(url) {
    $('#modal-app .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-app').modal('show')
    .find('.modal-content')
    .load(url);
}




");?>
