
<div class="row">
    <div class="col-md-12"><span class="h3 text-bold">Modules เสริม</span>
        <span class="pull-right"><?php echo \yii\bootstrap\Html::button('<i class=\'fa fa-plus\'></i> สร้าง Module', [
    'class' => 'tcc-btn-add btn btn-success btn-lg',
    'title' => Yii::t('app', 'สร้างโมดูลใหม่'),
    'data-url'=>\yii\helpers\Url::to(['tccbots/create-module', ]),
                'onclick' => 'location.href=\''.\yii\helpers\Url::to(['tccbots/create-module', ]).'\';'
]);?></span>
        <br><br><hr><br></div>
    <div class="col-md-4">
        <div class="media-left">
            <a href="#" data-toggle="modal" data-target="#myModal">
                <span class="fa-stack fa-lg fa-5x text-primary">
                  <i class="fa fa-square-o fa-stack-2x"></i>
                  <i class="fa fa-archive fa-stack-1x"></i>
                </span>
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading text-bold">Nemo Care</h4>
            Nemo Care Application...<br><i class="fa fa-download"></i> [install]
        </div>
    </div>
    <div class="col-md-4">
        <div class="media-left">
            <a href="#">
                <span class="fa-stack fa-lg fa-5x text-warning">
                  <i class="fa fa-square-o fa-stack-2x"></i>
                  <i class="fa fa-area-chart fa-stack-1x"></i>
                </span>
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading text-bold">Cancer Care Cloud</h4>
            Cancer Care Cloud...<br><i class="fa fa-download"></i> [install]
        </div>
    </div>
    <div class="col-md-4">
        <div class="media-left">
            <a href="#">
                <span class="fa-stack fa-lg fa-5x text-danger">
                  <i class="fa fa-square-o fa-stack-2x"></i>
                  <i class="fa fa-bullseye fa-stack-1x"></i>
                </span>
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading text-bold">CKD</h4>
            CKD...<br><i class="fa fa-download"></i> [install]
        </div>
    </div>
    <div class="col-md-4">
        <div class="media-left">
            <a href="#">
                <span class="fa-stack fa-lg fa-5x text-success">
                  <i class="fa fa-square-o fa-stack-2x"></i>
                  <i class="fa fa-cubes fa-stack-1x"></i>
                </span>
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading text-bold">Epidemiology</h4>
            Epidemiology...<br><i class="fa fa-download"></i> [install]
        </div>
    </div>
    <div class="col-md-4">
        <div class="media-left">
            <a href="#">
		<div style="margin: 10px;"><img src="../img/linei.png" width="110"></div>
                
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading text-bold">Line</h4>
            Line connect...<br><i class="fa fa-download"></i> [install]
        </div>
    </div>

    <div class="col-md-4">
        <div class="media-left">
            <a href="https://storage.thaicarecloud.org/source/app/" target="_blank">
                <span class="fa-stack fa-lg fa-5x text-info">
                  <i class="fa fa-square-o fa-stack-2x"></i>
                  <i class="fa fa-retweet fa-stack-1x"></i>
                </span>
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading text-bold">TCC Bot</h4>
            TCC Bot Application...<br><i class="fa fa-download"></i> [<a target="_blank" href="https://storage.thaicarecloud.org/source/app/">install</a>]
        </div>
    </div>

</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nemo Care</h4>
            </div>
            <div class="modal-body">
                <div class="media-left">
                    <a href="#" data-toggle="modal" data-target="#myModal">
                <span class="fa-stack fa-lg fa-5x text-primary">
                  <i class="fa fa-square-o fa-stack-2x"></i>
                  <i class="fa fa-archive fa-stack-1x"></i>
                </span>
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading text-bold">Nemo Care</h4>
                    Nemo Care Application...
                </div>
                <hr>
                <h4>Assignment</h4>
                <select id="ezform-assign" class="form-control select2-hidden-accessible" name="Ezform[assign][]" multiple="" size="4" data-krajee-select2="select2_62593e16" style="display:" tabindex="-1" aria-hidden="true">
                    <option value="1435745159010041100" selected="">Administrator DAMASAC (webmaster)</option>
                    <option value="1435745159010042200">ผู้จัดการระบบ  (manager)</option>
                    <option value="1435745159010043300">ผู้ใช้ธรรมดา  (user)</option>
                    <option value="1435745159010043301">ภาณุวัฒน์ ประทุมขำ (panuwat)</option>
                    <option value="1435745159010043302">  (codeerror)</option>
                    <option value="1435745159010043303">Kavin Thinkhamrop (kavin)</option>
                    <option value="1435745159010043304">อรวรรณ ดีสุรกุล (orawan)</option>
                    <option value="1435745159010043305">Sorasak Ura (Sorasak)</option>
                    <option value="1435745159010043306">Jidapa Phlangwan (jidapa)</option>
                    <option value="1435745159010043307">ยุวดี เพชระ (yuwadee)</option>
                    <option value="1435745159010043308">  (karuna)</option>
                    <option value="1435745159010043309">ภาณุวัฒน์ ประทุมขำ (nudamasac)</option>
                    <option value="1435745159010043310">Weerin Khanthatiwong (weerin)</option>
                    <option value="1435745159010043311">Pornpimon Kayota (Pornpimon)</option>
                    <option value="1435745159010043312">  (prasat)</option>
                    <option value="1435745159010043313">Bandit Thinkhamrop (bandit)</option>
                    <option value="1435745159010043314">Khanitthakul Koonmueang (khanitthakul)</option>
                    <option value="1435745159010043315">Supannee Promthet (supannee)</option>
                    <option value="1435745159010043316">  (nichapa)</option>
                    <option value="1435745159010043317">Jiamjit Saengsuwan (jiasae)</option>
                    <option value="1435745159010043318">Mr.kumrai simsan kumrai simsan (skumra)</option>
                    <option value="1435745159010043319">supanan  promkhonyang (pooky)</option>
                    <option value="1435745159010043320">  (Theppamorn)</option>
                    <option value="1435745159010043321">  (radda)</option>
                    <option value="1435745159010043322">Jiraporn Boonjung (Jiraporn)</option>
                    <option value="1435745159010043323">  (pranee)</option>
                    <option value="1435745159010043324">  (bandit1)</option>
                    <option value="1435745159010043325">  (kongvut)</option>
                    <option value="1435745159010043326">  (krit)</option>
                    <option value="1435745159010043329">ณัฏฐาพร  ปิยะสาธุกิจ (2129900017183)</option>
                    <option value="1435745159010043330">ผดุงพษ์ แสนทวีสุข (3339900193522)</option>
                    <option value="1435745159010043331">ธณาวุฒิ โยธะการี (1449900193941)</option>
                    <option value="1435745159010043332">ธรรมรัช เรืองปรัชญากุล (1319900200964)</option>
                    <option value="1435745159010043333">ณัชชา  บุญวัชรพันธ์ (1409900565155)</option>
                    <option value="1435745159010043334">ปิติพงษ์ สิทธิอำนวย (5101200138678)</option>
                    <option value="1435745159010043335">นรนนท์  บุญยืน (1539900243874)</option>
                    <option value="1435745159010043336">กตัญญู สว่างศรี (1449900163359)</option>
                    <option value="1435745159010043337">ชนาธิป อำนวย (1451500032331)</option>
                    <option value="1435745159010043339">ฐิติ ศิวะชิตพงศ์ (1101700092909)</option>
                    <option value="1435745159010043340">วิชชา ทุมมาลา (1449900150681)</option>
                    <option value="1435745159010043341">เสฐียรพงษ์ จันทวิบูลย์ (1639900107783)</option>
                    <option value="1435745159010043342">ระวีวัชร์ บรรณรักษ์ณา (1101700081290)</option>
                    <option value="1435745159010043343">ปฏิเวช สุคนธ์ขจร (1409900702641)</option>
                    <option value="1435745159010043344">ธนรรจน์ ปวรางกูล (1409900595046)</option>
                    <option value="1435745159010043345">กัญญารัตน์ เลิศวาจา (1909800270426)</option>
                    <option value="1435745159010043347">อารีนา อับดุลเลาะ (1969900068297)</option>
                    <option value="1435745159010043348">กันต์ชนินทร์ บุณย์ศุภา (1419900200394)</option>
                    <option value="1435745159010043349">อลิสรา ศรีนิลทา (1409900559465)</option>
                    <option value="1435745159010043350">วันปิติ พบลาภ (1349900332615)</option>
                    <option value="1435745159010043351">พิชเยนทร์ ดวงทองพล (3410101285923)</option>
                    <option value="1435745159010043352">วศิน ชคัตตรัยกุล (1100701105876)</option>
                    <option value="1435745159010043353">อรรถพล  ติตะปัญ (3409900423993)</option>
                    <option value="1435745159010043354">จักรี ดอนเตาเหล็ก (1361000124458)</option>
                    <option value="1435745159010043355">จุติรัตน์ เสาวงค์ (1459900138109)</option>
                    <option value="1435745159010043356">ธันวา มาโสม (1419900177074)</option>
                    <option value="1435745159010043357">test test (3720300448860)</option>
                    <option value="1435745159010043369">ภานุพงศ์ ศรีศุภเดชะ (1409900619107)</option>
                    <option value="1435745159010043370">ฐิติ ศรีแก้ว (3349900855827)</option>
                    <option value="1435745159010043371">สันติ ต๊อดแก้ว (1509901263445)</option>
                    <option value="1435745159010043374">ภาณุวัฒน์ ประทุมขำ (3332222211111)</option>
                    <option value="1435745159010043375">ชัยวัฒน์ ทะวะรุ่งเรือง (3410100805885)</option>
                    <option value="1435745159010043376">รัชนีย์ ชนะวงศ์ (3409800011189)</option>
                    <option value="1435745159010043377">จารุวรรณ เถื่อนมั่น (1440300163911)</option>
                    <option value="1435745159010043378">รติรัตน์ รุ่งเรือง (3419900745354)</option>
                    <option value="1435745159010043379">ปนัดดา ลีนาลาด (2419900008106)</option>
                    <option value="1435745159010043380">ขวัญศิริ ปริปุณโณ (3410100044264)</option>
                    <option value="1435745159010043381">ธันย์ชนก แสงโชติ (1321200062693)</option>
                    <option value="1435745159010043382">ศิโรรัตน์ สิทธิศักดิ์ (1309900313220)</option>
                    <option value="1435745159010043383">นุชกานต์ คำงาม (1349900458929)</option>
                    <option value="1435745159010043386">ศศิธร โคตรสิงห์ (1409900788740)</option>
                    <option value="1435745159010043387">สุทธิพร ทองสอดแสง (1489900113131)</option>
                    <option value="1435745159010043388">สำนักงานสาธารณสุขอำเภอสร้างคอม  (00411u2)</option>
                    <option value="1435745159010043389">รศ.พญ.นิตยา ฉมาดล (nittaya)</option>
                    <option value="1435745159010043390">kongvut sangkla (1350100190818)</option>
                    <option value="1435745159010043391">ภาณุวัฒน์ ประทุมขำ (5440900036297)</option>
                    <option value="1435745159010043392">บัณฑิต ถิ่นคำรพ (5409999022496)</option>
                    <option value="1435745159010043393">พรรณิภา ดีจะมาลา (1414070045737)</option>
                    <option value="1435745159010043394">คาระวะ ปรัชญา (1409901548386)</option>
                    <option value="1435745159010043395">ณัฐพล โยธา (1409900761485)</option>
                    <option value="1435745159010043396">สินใจ สินสอด (7744247838840)</option>
                    <option value="1435745159010043397">สมใจ สมสุข (4156606186325)</option>
                    <option value="1435745159010043398">ณฐาภพ ชัยชญา (3720400032978)</option>
                    <option value="1435745159010043399">ทดสอบ สุ่มเลขบัตร (2488431117560)</option>
                    <option value="1435745159010043400">วิลัยพร ถิ่นคำรพ (3450800201702)</option>
                    <option value="1435745159010043403">เอื้อมพร สุ่มมาตย์ (3451100529722)</option>
                    <option value="1435745159010043405">สุปราณี วรพันธุ์ (3460700009842)</option>
                    <option value="1435745159010043406">ศุภัทร์ มณีวงศ์ (3430600427081)</option>
                    <option value="1435745159010043407">TEST TESTING (6571178074018)</option>
                    <option value="1435745159010043408">test2 test2 (3248821012851)</option>
                    <option value="1435745159010043410">จันทร์ อังคาร (1213478046021)</option>
                    <option value="1435745159010043411">ใจ ดี (5376502245542)</option>
                    <option value="1435745159010043412">ใจ ดี (8374408702300)</option>
                    <option value="1435745159010043413">fsdfsdfds fsdfsdf (7508822446421)</option>
                    <option value="1435745159010043414">เมตตา กรุณา (1642411502781)</option>
                    <option value="1435745159010043415">ศรุตยา ศรีมารัตน์ (1409900315540)</option>
                    <option value="1435745159010043416">ผดารณัช พลไชยมาตย์ (1411400030077)</option>
                    <option value="1435745159010043417">วชิญญา  มังธานี (1470800013543)</option>
                    <option value="1435745159010043418">อัญศักดิ์ สายธิไชย (1471100007070)</option>
                    <option value="1435745159010043419">เนติพล พรหมจักร (1470400120431)</option>
                    <option value="1435745159010043420">ธนกฤต บัวสกุล (5470500035285)</option>
                    <option value="1435745159010043421">เพ็ญนภา เมืองเหนือ (1440900135306)</option>
                    <option value="1435745159010043422">สุปัญญา ไชยราช (3440700276042)</option>
                    <option value="1435745159010043423">วีระพงษ์ เวียงพรหมมา (1471100047870)</option>
                    <option value="1435745159010043424">ศรัญญา โพธิ์พันธ์ (1470400086268)</option>
                    <option value="1435745159010043425">ณัฐพล ฤาเดช (1471100075814)</option>
                    <option value="1435745159010043426">ฐิติมา แสนโสภา (1470800116253)</option>
                    <option value="1435745159010043427">จำปาทอง ทิพย์ม้อม (3471100159101)</option>
                    <option value="1435745159010043428">ศุภชัย ช่วยรักษา (1471100051265)</option>
                    <option value="1435745159010043429">มณีรัตน์ อินทร์พิมพ์ (1350700058368)</option>
                    <option value="1435745159010043430">ชไมรัตน์ ดุลบดี (3470800787120)</option>
                    <option value="1435745159010043431">รัตณา คนเพียร (3471100266314)</option>
                    <option value="1435745159010043432">กลีบผกา ไชยสุรินทร์ (3471100200348)</option>
                    <option value="1435745159010043433">จักรพันธ์ อรรคฮาด (1470800134642)</option>
                    <option value="1435745159010043434">สร้อยสุรีย์ นมนวน (1471300031136)</option>
                    <option value="1435745159010043435">อนุชา สีหาเพชร (1470300067016)</option>
                    <option value="1435745159010043436">วาทิต ศรีสร้อย (1470500032828)</option>
                    <option value="1435745159010043437">วิญญูพล ปรางค์ทอง (5470800051804)</option>
                    <option value="1435745159010043438">user  (user@gmail.com)</option>
                    <option value="1435745159010043439">สุดารัตน์ อาแพงพันธ์ (1470800116580)</option>
                    <option value="1435745159010043440">พิชัย พลจิตต์ (3470800024100)</option>
                    <option value="1435745159010043441">โสภาภรณ์ พลจางวาง (3470400353882)</option>
                    <option value="1435745159010043442">ภาณุพงษ์ ชาเหลา (1471100078996)</option>
                    <option value="1435745159010043443">สันติ ต๊อดแก้ว (nbermm)</option>
                    <option value="1435745159010043444">ปริญญา มาแสวง (3359900068558)</option>
                    <option value="1435745159010043445">สมศักดิ์ นามฤทธิื (3409900482361)</option>
                    <option value="1435745159010043446">ประยงค์ โพธิ์กลาง (3310200073232)</option>
                    <option value="1435745159010043447">โรงพยาบาลรัตนบุรี 2.2 ทุติยภูมิระดับกลาง (10920)</option>
                    <option value="1435745159010043448">มรกต มูลสาร (3350300565498)</option>
                    <option value="1435745159010043449">พนมพรณ์ อร่ามพงษ์พันธ์ (3440600258832)</option>
                    <option value="1435745159010043450">โรงพยาบาลสมเด็จพระยุพราชบ้านดุง 2.1 ทุติยภูมิระดับต้น (11446)</option>
                    <option value="1435745159010043451">จันทร์ฉาย เวชกามา (3341400118192)</option>
                    <option value="1435745159010043452">Administrator SRISONGKRAM (1480800076600)</option>
                    <option value="1435745159010043453">กฤษฎา ถิตย์วิลาศ (1460300129623)</option>
                    <option value="1435745159010043454">กัณทิมา แก้วดวงดี (3480100088377)</option>
                    <option value="1435745159010043455">แม้นเขียน ชัญถาวร (3329900190817)</option>
                    <option value="1435745159010043456">กฤษณ์ หวังกีรติกานต์ (1329900167673)</option>
                    <option value="1435745159010043457">ศุภรัตน์ อมรพรเจริญ (1100700025798)</option>
                    <option value="1435745159010043458">ชุติมา สายรัตน์ (3320700840038)</option>
                    <option value="1435745159010043459">สถาพร ลาภจิตร (3320300872773)</option>
                    <option value="1435745159010043460">วันทนีย์ ระดาฤทธิ์ (3460400066727)</option>
                    <option value="1435745159010043461">ศิราณี โพธิ์ศรี (3359900185724)</option>
                    <option value="1435745159010043462">จันทร์ทิมา ภูสิทธิเดช (1329900108715)</option>
                    <option value="1435745159010043463">ฉัตฑริกรณ์ หานะพันธ์ (1409901012435)</option>
                    <option value="1435745159010043464">วิไลพร สาศิริ (3140500223622)</option>
                    <option value="1435745159010043465">เตือนจิต พุทธานุ (3459900211937)</option>
                    <option value="1435745159010043466">ณัฐปรัชญาปกรณ์ สีแพง (1470400102484)</option>
                    <option value="1435745159010043467">มาตุภูมิ ใครบุตร (3460100321128)</option>
                    <option value="1435745159010043468">นวนันท์ เย็นวัฒนา (3420800019891)</option>
                    <option value="1435745159010043469">กรุง เย็นวัฒนา (3410300140381)</option>
                    <option value="1435745159010043470">โรงพยาบาลบ้านผือ 2.2 ทุติยภูมิระดับกลาง (11023)</option>
                    <option value="1435745159010043471">อานนท์ แสงจันทร์ (1440500126628)</option>
                    <option value="1435745159010043473">บัณฑิต ภารวงษ์ (1440800043001)</option>
                    <option value="1435745159010043474">สวาสดิ์ สิมมา (3320300305488)</option>
                    <option value="1435745159010043475">xxxx yyyyy (3333333390991)</option>
                    <option value="1435745159010043476">อภิชาติ จิระวุฒิพงศ์ (3409900355645)</option>
                    <option value="1435745159010043477">Admin  (skaball69@gmail.com)</option>
                    <option value="1435745159010043478">ศักดิ์ชัย เคหาบาล (11076)</option>
                    <option value="1435745159010043479">ภาคภูมิ ขามพิทักษ์ (1409900456011)</option>
                    <option value="1435745159010043480">บุญธง ธานี (3470800672268)</option>
                    <option value="1435745159010043481">ฉันทนา ชาวดร (4411700002324)</option>
                    <option value="1435745159010043482">รัตติกาล ดรุณ (5620690005569)</option>
                    <option value="1435745159010043483">โรงพยาบาลเขื่องใน 2.1 ทุติยภูมิระดับต้น (10946)</option>
                    <option value="1435745159010043484">Jirawat Maubkhuntod (1300800201146)</option>
                    <option value="1435745159010043485">Jirawat Maubkhuntod (6138237552715)</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
            </div>
        </div>
    </div>
</div>

