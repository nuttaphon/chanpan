<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\helpdesk\models\Tester */

$this->title = 'Add Tester';
//$this->params['breadcrumbs'][] = ['label' => 'Testers', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tester-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
