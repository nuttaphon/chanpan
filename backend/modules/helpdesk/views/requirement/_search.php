<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\helpdesk\models\RequirementSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="requirement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_assign') ?>

    <?= $form->field($model, 'requirement') ?>

    <?= $form->field($model, 'department') ?>

    <?= $form->field($model, 'ownership') ?>

    <?php // echo $form->field($model, 'assign_date') ?>

    <?php // echo $form->field($model, 'deadline_date') ?>

    <?php // echo $form->field($model, 'left_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
