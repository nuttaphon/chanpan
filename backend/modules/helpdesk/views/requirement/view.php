<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

//use kartik\widgets\DatePicker;
//use yii\bootstrap\Tabs;
use kartik\tabs\TabsX;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model backend\modules\helpdesk\models\Requirement */
/* @var $newCommentModel backend\modules\helpdesk\models\Requirement */
/* @var $dataTesterProvider backend\modules\helpdesk\models\Tester */
/* @var $dataCommentProvider backend\modules\helpdesk\models\Comment */
/* @var $dateAssignProvider backend\modules\helpdesk\models\DateAssign */


$this->title = $model->requirement;
$this->params['breadcrumbs'][] = ['label' => 'Requirements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="requirement-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            [
                'label' => 'ผู้มอบหมายงาน',
                'value' => $model->userAssign->username
            ],
            [
                'label' => 'รายงานความผิดพลาด',
                'value' => $model->requirement
            ],
            [
                'label' => 'โครงการ',
                'value' => $model->department0->department_name
            ],
            [
                'label' => 'ผู้รับงาน',
                'value' => $model->ownership0->username
            ],
            [
                'label' => 'วันมอบหมาย',
                'attribute' => 'assign_date',
                'format' => ['date', 'php:d-m-Y']
            ],
            [
                'label' => 'วันส่งงาน',
                'attribute' => 'deadline_date',
                'format' => ['date', 'php:d-m-Y']
            ],
            [
                'label' => 'วันที่เหลือ',
                'attribute' => 'left_date',
            ],
            [
                'label' => 'สถานะ',
                'value' => $model->status0->status_name
            ]
        ],
    ]) ?>
    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    $gridTester = GridView::widget([
        'dataProvider' => $dataTesterProvider,
        'tableOptions' => [
            'class' =>'table table-hover'
        ],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'ผู้ทดสอบ',
                'value' => 'userProperties.username'
            ],
            [
                'class'=>'kartik\grid\BooleanColumn',
                'attribute'=>'status_approve',
                'label' => 'สถานะ',
                'vAlign'=>'middle',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions'=>['class'=>'kartik-sheet-style'],
                'template' =>'{edit_button} {delete_button}',
                'buttons' =>[
                    'edit_button' =>function ($url, $model, $key) {
                        return Html::a('แก้ไข', ['update-tester', 'id'=>$model->id,'requirement_id'=>$model->requirement_id],['class' => 'btn btn-primary']);
                    },
                    'delete_button' =>function ($url, $model, $key) {
                        return Html::a('ลบ', ['delete-tester', 'id'=>$model->id,'requirement_id'=>$model->requirement_id],['class' => 'btn btn-danger']);
                    }
                ]
            ],
        ],
    ]);
    $listComment =ListView::widget([
        'dataProvider' => $dataCommentProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' =>function($modelCommentList){
            return '<div class="list-group">
                  <a  class="list-group-item">
                    <h4 class="list-group-item-heading">'.Html::encode($modelCommentList->comment).'</h4>
                    <img id="imageresource" src='.Url::to(Yii::$app->request->baseUrl.'/'.$modelCommentList->files_upload).' class="img-responsive" width="300px" height="100px">
                    <p class="list-group-item-text">'."Post by : ".$modelCommentList->postBy->username.'</p>
                  </a>
            </div>';
        },
//        'pager' => [
//            'maxButtonCount' => 5,
//        ],
    ]);
    $gridDateAssign = GridView::widget([
        'dataProvider' => $dateAssignProvider,
        'tableOptions' => [
            'class' =>'table table-hover'
        ],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'assign_date',
                'label' => 'วันมอบหมาย',
                'vAlign'=>'middle',
            ],
            [
                'attribute'=>'deadline_date',
                'label' => 'วันส่งงาน',
                'vAlign'=>'middle',
            ],
            [
                'attribute'=>'requirement_log',
                'label' => 'ข้อผิดพลาด',
                'vAlign'=>'middle',
            ]
        ]
    ]);
    Pjax::begin();
    $form = ActiveForm::begin(['id' => 'comment-form',
        'method'=>'post',
        'action' =>'add-comment',
        'options' =>[
            'enctype' => 'multipart/form-data'
        ]
    ]);

    echo TabsX::widget([
        'position' => TabsX::POS_ABOVE,
        'align' => TabsX::ALIGN_LEFT,
        'items' => [
            [
                'label' => 'การทดสอบ',
                'content' =>$gridTester,
                'headerOptions' => ['style'=>'font-weight:bold'],
            ],
            [
                'label' => 'Comment',
                'content' =>$this->render('_comment', ['model' => $newCommentModel, 'form' => $form]),
                'headerOptions' => ['style'=>'font-weight:bold'],
            ],
            [
                'label' => 'View Comment',
                'content' =>$listComment,
                'headerOptions' => ['style'=>'font-weight:bold'],
            ],
            [
                'label' => 'Time log',
                'content' =>$gridDateAssign,
                'headerOptions' => ['style'=>'font-weight:bold'],
            ],
        ],
    ]);
    ActiveForm::end();
    Pjax::end();
    ?>
</div>
