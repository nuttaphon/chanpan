<?php
use yii\grid\GridView;
?>
<?php
echo GridView::widget([
    'dataProvider' => $dataTesterProvider,
    'tableOptions' => [
        'class' =>'table table-hover'
    ],
    'layout' => "{items}\n{pager}",
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        //['resizableColumns'=>true,]
        //['class' => 'yii\grid\CheckboxColumn'],
        //'id',
        [
            //'attribute' =>'Tester',
            'label' => 'ผู้ทดสอบ',
            'value' => 'userProperties.username'
        ],
        [
            'class' =>'yii\grid\CheckboxColumn',
            'header' => 'ผลการทดสอบ',

//                'class' => 'yii\grid\CheckboxColumn',
//                'checkboxOptions' => function($model, $key, $index, $column) {
//                    return ['value' => $model->status_approve];
//                }

            //     'attribute' =>'status_approve',
            //'attribute' => 'name',
            // 'format' => 'text',
            // 'label' => 'ผลการทดสอบ',
//                'value' => Html::checkBox('status_approve', false, [
//                    'class' => 'select-on-check-all',
//                    'label' => 'Check All',
//                ]),
            //'class' => 'yii\grid\CheckboxColumn'
            //'class' => 'yii\grid\CheckboxColumn',
            //'value' => "typeStatus"
//                'value' => function ($model, $index, $widget) {
//                    return Html::checkbox('foo[]', $model->status_approve, ['value' => $index, 'disabled' => true]);
//                },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions'=>['class'=>'kartik-sheet-style'],
            'template' =>'{edit_button} {delete_button}',
            'buttons' =>[
                'edit_button' =>function ($url, $model, $key) {
                    return Html::a('แก้ไข', ['update-tester', 'id'=>$model->id,'requirement_id'=>$model->requirement_id],['class' => 'btn btn-primary']);
                },
                'delete_button' =>function ($url, $model, $key) {
                    return Html::a('ลบ', ['delete-tester', 'id'=>$model->id,'requirement_id'=>$model->requirement_id],['class' => 'btn btn-danger']);
                }
            ]
        ],
    ],
]);
?>