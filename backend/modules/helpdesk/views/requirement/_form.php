<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartik\widgets\DatePicker;
use yii\jui\DatePicker;
//use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\modules\helpdesk\models\Requirement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="requirement-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= Html::activeHiddenInput($model, 'user_assign') ;?>
    <?= $form->field($model, 'requirement')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'department')->dropDownList($model->getDepartmentList()) ?>

   <?=
   $form->field($model, 'ownership')->widget(Select2::classname(), [
       'data' => $model->getOwnershipList(),
       'language' => 'de',
       'options' => ['placeholder' => 'เลือกผู้รับงาน'],
       'pluginOptions' => [
           'allowClear' => true
       ],
   ])
   ?>
    <h5><b>Assign Date</b></h5>
    <?php
        echo DatePicker::widget([
            'name'  => 'assign_date',
            'model' => $model,
            'attribute' => 'assign_date',
            'value' =>$model->assign_date,
            'language' => 'th',
            'dateFormat' => 'yyyy-MM-dd',
            //'dateFormat' => 'dd-MM-yyyy',
            'inline' => false
        ]);
    ?>
    <h5><b>Deadline Date</b></h5>
    <?php
        echo DatePicker::widget([
            'name'  => 'deadline_date',
            'model' => $model,
            'attribute' => 'deadline_date',
            'value' =>$model->deadline_date,
            'language' => 'th',
            'dateFormat' => 'yyyy-MM-dd',
            'inline' => false
        ]);
    ?>

    <?= $form->field($model, 'left_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatusList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
