<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\helpdesk\models\RequirementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Requirements';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="requirement-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Requirement', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' =>'user_assign',
                'label' => 'ผู้มอบหมายงาน',
                'value' => 'userAssign.username'
            ],
            [
                'attribute' =>'requirement',
                'label' => 'รายงานความผิดพลาด',
            ],
            [
                'attribute' =>'department',
                'label' => 'โครงการ',
                'value' => 'department0.department_name'
            ],
            [
                'attribute' =>'ownership',
                'label' => 'ผู้รับงาน',
                'value' => 'ownership0.username'
            ],
            [
                'label' => 'วันมอบหมาย',
                'attribute' => 'assign_date',
                'format' => ['date', 'php:d-m-Y']
            ],
            [
                'label' => 'วันเสร็จสิ้น',
                'attribute' => 'deadline_date',
                'format' => ['date', 'php:d-m-Y']
            ],
            [
                'label' => 'วันที่เหลือ',
                'attribute' => 'left_date',
            ],
            [
                'attribute' =>'status',
                'label' => 'สถานะ',
                'value' => 'status0.status_name'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions'=>['class'=>'kartik-sheet-style'],
                'template' => '{view} {update}{delete}'
            ],
        ],
    ]); ?>

</div>
