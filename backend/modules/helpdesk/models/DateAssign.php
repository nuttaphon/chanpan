<?php

namespace backend\modules\helpdesk\models;

use Yii;

/**
 * This is the model class for table "help_desk_datetime".
 *
 * @property integer $id
 * @property integer $requirement_id
 * @property string $assign_date
 * @property string $deadline_date
 * @property string $requirement_log
 *
 * @property HelpDeskRequirement $requirement
 */
class DateAssign extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_desk_datetime';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requirement_id'], 'required'],
            [['requirement_id'], 'integer'],
            [['assign_date', 'deadline_date'], 'safe'],
            [['requirement_log'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requirement_id' => 'Requirement ID',
            'assign_date' => 'Assign Date',
            'deadline_date' => 'Deadline Date',
            'requirement_log' => 'Requirement Log',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequirement()
    {
        return $this->hasOne(HelpDeskRequirement::className(), ['id' => 'requirement_id']);
    }
}
