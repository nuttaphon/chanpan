<?php

namespace backend\modules\helpdesk\models;

use Yii;

/**
 * This is the model class for table "help_desk_status".
 *
 * @property integer $id
 * @property string $status_name
 *
 * @property HelpDeskRequirement[] $helpDeskRequirements
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_desk_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_name'], 'required'],
            [['status_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_name' => 'Status Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelpDeskRequirements()
    {
        return $this->hasMany(HelpDeskRequirement::className(), ['status' => 'id']);
    }
}
