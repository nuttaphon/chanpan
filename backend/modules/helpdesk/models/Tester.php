<?php

namespace backend\modules\helpdesk\models;

use Yii;
use common\models\User;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "help_desk_tester".
 *
 * @property integer $id
 * @property integer $requirement_id
 * @property string $user_id
 * @property integer $status_approve
 *
 * @property HelpDeskRequirement $requirement
 */
class Tester extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_desk_tester';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requirement_id', 'user_id', 'status_approve'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requirement_id' => 'Requirement ID',
            'user_id' => 'ผู้ทดสอบระบบ',
            'status_approve' => 'Status Approve',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getRequirement0()
//    {
//       // return $this->hasOne(Requirement::className(), ['id' => 'requirement_id']);
//    }
    public function getUserProperties()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getTesterList()
    {
        $list =User::find()->orderBy('id')->all();
        return ArrayHelper::map($list,'id','username');
    }
    public function getTypeStatus()
    {
        if($this->status_approve == 0)
            return "No";
        else
            return "Yes";

    }
}
