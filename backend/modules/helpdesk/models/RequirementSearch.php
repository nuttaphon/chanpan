<?php

namespace backend\modules\helpdesk\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\helpdesk\models\Requirement;

/**
 * RequirementSearch represents the model behind the search form about `backend\modules\helpdesk\models\Requirement`.
 */
class RequirementSearch extends Requirement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_assign', 'department', 'ownership', 'status'], 'integer'],
            [['requirement', 'assign_date', 'deadline_date', 'left_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Requirement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_assign' => $this->user_assign,
            'department' => $this->department,
            'ownership' => $this->ownership,
            'assign_date' => $this->assign_date,
            'deadline_date' => $this->deadline_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'requirement', $this->requirement])
            ->andFilterWhere(['like', 'left_date', $this->left_date]);

        return $dataProvider;
    }
}
