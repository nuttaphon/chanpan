<?php

namespace backend\modules\helpdesk\controllers;

use backend\models\UserProfile;
use backend\modules\helpdesk\models\Comment;
use backend\modules\helpdesk\models\DateAssign;
use backend\modules\helpdesk\models\Tester;
use common\models\User;
use Yii;
use backend\modules\helpdesk\models\Requirement;
use backend\modules\helpdesk\models\RequirementSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use yii\db\Query;
/**
 * RequirementController implements the CRUD actions for Requirement model.
 */
class RequirementController extends Controller
{
    //private $_old_requirement;
    private $_deadline_date;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Requirement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequirementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Requirement model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelCommentList = Comment::find()
            ->where(['requirement_id' => $model->id])
            ->orderBy('id DESC')
            ->all();
        $dataCommentProvider = new ActiveDataProvider([
            'query' => Comment::find()->where(['requirement_id' =>$model->id])->orderBy('id DESC'),
//            'pagination' => [
//                'pageSize' => 5,
//            ],
        ]);
        $dataTesterProvider = new ActiveDataProvider([
            'query' => Tester::find()->andFilterWhere([
                'requirement_id' => $model->id
            ]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $testerModel = Tester::find()
                        ->where(['requirement_id'=>$model->id])
                        ->all();
        $dateAssignProvider = new ActiveDataProvider(
            [
                'query' => DateAssign::find()->where(['requirement_id'=>$model->id])->orderBy('id DESC'),
//                'pagination' => [
//                    'pageSize' => 20,
//                ],
            ]
        );
        $newCommentModel = new Comment();
        $newCommentModel->requirement_id = $model->id;
        $newCommentModel->post_by =Yii::$app->user->identity->id;
        //$newCommentModel->post_datetime =
        return $this->render('view', [
            'model' => $model,
            'modelCommentList' => $modelCommentList,
            'dataTesterProvider'=> $dataTesterProvider,
            'newCommentModel' =>$newCommentModel,
            'testerModel' =>$testerModel,
            'dataCommentProvider' => $dataCommentProvider,
            'dateAssignProvider' =>$dateAssignProvider
        ]);
    }

    /**
     * Creates a new Requirement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Requirement();
        $model->user_assign = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->addDateAssign($model->id, $model->assign_date, $model->deadline_date,$model->requirement);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing Requirement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($this->_deadline_date < $model->deadline_date) {

                $this->addDateAssign($model->id, $model->assign_date, $model->deadline_date,$model->requirement);
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
           // $this->_old_requirement = $model->requirement;
            //VarDumper::dump($this->_old_requirement);
            //exit();
            //$_deadline_date =$model->deadline_date;
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Requirement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Requirement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Requirement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Requirement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddComment()
    {
        $model = new Comment();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->file = UploadedFile::getInstance($model,'file');

            $save_file='';

            if($model->file){

                $image_path = 'uploads/comments/';
                $model->files_upload = $image_path.$model->file->name;
                $save_file =1;

                if($model->save()){
                    if($save_file){
                        $model->file->saveAs($model->files_upload);
                    }
                    return $this->redirect(['/helpdesk/requirement/view', 'id' => $model->requirement_id]);
                }
            }
//            if($model->save()){
//                if($save_file){
//                    $model->file->saveAs($model->files_upload);
//                }
//                return $this->redirect(['/helpdesk/requirement/view', 'id' => $model->requirement_id]);
//            }
        } else {
            return $this->redirect(['/helpdesk/requirement/view', 'id' => $model->requirement_id]);
//            $model->requirement_id =Yii::$app->request->post('requirement_id');
//            $model->post_by = Yii::$app->request->post('post_by');
//            // $model->requirement_id = $requirement_id;
//            // $model->post_by = $post_by;
//            return $this->render('create', [
//                'model' => $model,
//            ]);
        }
    }
    public function actionUpdateComment($id)
    {
        $modelComment = Comment::find()->where(['id' => $id])->one();
        if($modelComment->load(Yii::$app->request->post())){
            $modelComment->file = UploadedFile::getInstance($modelComment,'file');
            $save_file ='';
            if($modelComment->file){
                $save_file=1;
                if($modelComment->save()){
                    if($save_file){
                        $modelComment->file->saveAs($modelComment->files_upload);
                    }
                    return $this->redirect(['/helpdesk/requirement/view','id' =>$modelComment->requirement_id]);
                }
            }
        }
//        else{
//            return $this->render('create', [
//                'model' => $modelComment,
//            ]);
//        }
    }
    public  function actionDateAssign()
    {
        $requriement_id = Yii::$app->request->post('requirement_id');
        $dateAssignProvider = new ActiveDataProvider([
            'query'=>DateAssign::find()
                        ->where(['requirement_id' => $requriement_id])
        ]);
    }
    private function addDateAssign($requirement_id, $assign_date, $deadline_date, $requirement_log)
    {
        $model = new DateAssign();
        $model->requirement_id = $requirement_id;
        $model->assign_date = $assign_date;
        $model->deadline_date = $deadline_date;
        $model->requirement_log = $requirement_log;
        $model->save();
    }
    public function actionDeleteTester($id,$requirement_id)
    {
        $tester = Tester::find()->where(['id' => $id])->one();
        $tester->delete();
        return $this->redirect(['/helpdesk/requirement/view','id' =>$requirement_id]);

    }
    public function  actionUpdateTester($id, $requirement_id)
    {
        $model = Tester::find()->where(['id' => $id])->one();

        if ($model->load(Yii::$app->request->post())&& $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['view', 'id' => $requirement_id]);
        } else {
            return $this->render('update_tester', [
                'model' => $model,
            ]);
        }
    }
}
