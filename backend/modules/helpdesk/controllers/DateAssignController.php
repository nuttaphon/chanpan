<?php

namespace backend\modules\helpdesk\controllers;

use backend\modules\helpdesk\models\DateAssign;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\VarDumper;

class DateAssignController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $requriement_id = \Yii::$app->request->post('requirement_id');
       // $model = DateAssign::find()
//            ->where(['requirement_id' => $requriement_id])
//            ->all();
        //VarDumper::dump($requriement_id);
        //exit();
       // $query = new Query();
        $dataProvider = new ActiveDataProvider([
           // 'query' =>$model,
           // 'query' =>
            'pagination' => [
                'pageSize' => 20,
            ],
            'query' => DateAssign::find()
        ]);


        return $this->render('index',[
            'dataProvider' => $dataProvider
        ]);
    }

}
