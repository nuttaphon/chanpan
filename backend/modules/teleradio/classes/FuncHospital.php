<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\classes;

#use yii\helpers\VarDumper;
#use yii\web\Controller;
use Yii;

class FuncHospital {
    //put your code here
    public static function getHospitalByHCODE($hcode) {
        $sql = "select * from all_hospital_thai
                where hcode=:hcode ";
        
        $out = Yii::$app->db->createCommand($sql, [':hcode'=>$hcode])->queryOne();
        return $out;
    }
}