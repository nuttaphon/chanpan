<?php

namespace backend\modules\teleradio\models;

use Yii;

/**
 * This is the model class for table "tele_radio_diag".
 *
 * @property string $id
 * @property integer $var1a1
 * @property integer $var3a1
 * @property integer $var3a9
 * @property integer $var2a1
 * @property integer $var4a1
 * @property integer $var4a3
 * @property integer $var2a2
 * @property integer $var4a4
 * @property string $textdet
 * @property string $ref_consult_id
 * @property string $user_create
 * @property string $create_date
 * @property string $xsourcex
 */
class TeleRadioDiag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tele_radio_diag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['var1a1', 'var3a1', 'var3a9', 'var2a1', 'var4a1', 'var4a3', 'var2a2', 'var4a4', 'ref_consult_id', 'user_create'], 'integer'],
            [['textdet'], 'string'],
            [['create_date'], 'safe'],
            [['xsourcex'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'var1a1' => 'Var1a1',
            'var3a1' => 'Var3a1',
            'var3a9' => 'Var3a9',
            'var2a1' => 'ยืนยันผลการส่งต่อ',
            'var4a1' => 'Var4a1',
            'var4a3' => 'Var4a3',
            'var2a2' => 'Var2a2',
            'var4a4' => 'Var4a4',
            'textdet' => 'Textdet',
            'ref_consult_id' => 'Ref Consult ID',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'xsourcex' => 'Xsourcex',
        ];
    }
}
