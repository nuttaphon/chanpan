<?php

namespace app\modules\teleradio\models;

use Yii;

/**
 * This is the model class for table "history_us_site".
 *
 * @property string $No
 * @property string $hospitalname
 * @property string $hcode
 * @property string $dateatsite
 * @property integer $man_c
 * @property integer $time_c
 * @property integer $suspected_c
 * @property integer $suspectedtime_c
 * @property integer $pdf_c
 * @property integer $pdftime_c
 * @property integer $sos_c
 * @property integer $sostime_c
 * @property integer $consult_c
 * @property integer $consulttime_c
 */
class HistoryUsSite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history_us_site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hcode'], 'required'],
            [['dateatsite'], 'safe'],
            [['man_c', 'time_c', 'suspected_c', 'suspectedtime_c', 'pdf_c', 'pdftime_c', 'sos_c', 'sostime_c', 'consult_c', 'consulttime_c'], 'integer'],
            [['No', 'hcode'], 'string', 'max' => 10],
            [['hospitalname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'No' => 'แถวที่',
            'hospitalname' => 'ชื่อหน่วยบริการ',
            'hcode' => 'รหัสหน่วยบริการ',
            'dateatsite' => 'วันที่วางเครื่อง',
            'man_c' => 'จำนวนคน',
            'time_c' => 'จำนวนครั้ง',
            'suspected_c' => 'Suspected คน (ครั้ง)',
            'suspectedtime_c' => 'Suspected ครั้ง',
            'pdf_c' => 'PDF คน (ครั้ง)',
            'pdftime_c' => 'PDF ครั้ง',
            'sos_c' => 'SOS',
            'sostime_c' => 'SOS จำนวนครั้ง',
            'consult_c' => 'ขอคำปรึกษา',
            'consulttime_c' => 'ขอคำปรึกษา ครั้ง',
        ];
    }
}
