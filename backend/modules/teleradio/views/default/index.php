<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 17/4/2559
 * Time: 19:34
 * E-mail: kongvut@gmail.com
 */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

$this->title = 'Tele-Radiology';

echo GridView::widget([
    'dataProvider'=>$dataProvider,
    //'filterModel'=>$searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'resizableColumns'=>false,
    'columns'=>[
        [
            'class' => 'kartik\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'format' => 'html',
            'label' => 'สถานบริการ',
            'width' => '100px',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                $hospital = \backend\modules\ezforms\components\EzformQuery::getHospital($model['xsourcex']);
                return  '<span class="label label-success" title="'.($hospital['name']. ' ต.'. $hospital['tambon']. ' อ.'. $hospital['amphur']. ' จ.'. $hospital['province']).'">'.$hospital['hcode'].'</span>';;
            }
        ],
        [
            'format' => 'text',
            'label' => 'ชื่อฟอร์ม',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                $sql="SELECT ezf_name FROM ezform WHERE ezf_id=:ezf_id";
                $hosp = \Yii::$app->db->createCommand($sql,[':ezf_id' => $model['ezf_id']])->queryOne();
                return $hosp['ezf_name'];
            }
        ],
        [
            'label' => 'เรื่อง',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                return mb_substr($model['ezf_comment'], 0,50,'UTF-8').'...';
            }
        ],
        [
            'label' => 'ประเภท',
            'format' => 'text',
            'width' => '90px',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $text = '';
                if($model['type'] == 1)
                    $text =  'แสงความคิดเห็นทั่วไป';
                else if($model['type'] == 2)
                    $text = 'ขอคำปรึกษา';
                else if($model['type'] == 3)
                    $text = 'SOS';
                else if($model['type'] == 9 || $model['type'] == 0)
                    $text = 'บันทึกข้อมูล';

                return $text;
            }
        ],
        [
            'label' => 'เมื่อ',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $date = new DateTime($model['create_date']);
                return $date->format('d/m/Y');
            }
        ],
        [
            'label' => 'โดย',
            'width' => '150px',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                $user = common\models\UserProfile::findOne($model['user_create']);
                return $user->firstname . ' ' . $user->lastname;
            }
        ],
        [
            'format' => 'raw',
            'width' => '70px',
            'headerOptions' => ['class' => 'text-center'],
            'label' => 'Link',
            'value' => function($model){
                return Html::a('<span class="fa fa-file-text-o"></span> ดูข้อมูล', ['consult-view', 'id'=>$model['id'], ], ['class'=>'btn btn-success btn-sm', 'data-pjax' => 0, 'target'=> '_blank']);
            }
        ],
    ],

    'headerRowOptions'=>['class'=>'kartik-sheet-style'],

    'pjax'=>true, // pjax is set to always true for this demo
    'pjaxSettings' => ['options' => ['id' => 'pjax-page-consult',], 'enablePushState' => false],
    // set your toolbar
    'toolbar'=> [
        ['content' => Html::dropDownList('typeReply', ($_GET['type'] ? $_GET['type'] : 2), [ '1' => 'แสดงความคิดเห็น', '2' => 'ขอคำปรึกษา', '3' => 'SOS', 'all' => 'ทั้งหมด'], [
            'class'=>'form-control',
            'onchange'=>'changeType($(this).val());'])
        ],
        '{export}',
        '{toggleData}',
    ],
    // set export properties
    'export'=>[
        'fontAwesome'=>true
    ],
    // parameters from the demo form
    'bootstrap'=>true,
    'bordered'=>true,
    'striped'=>true,
    'condensed'=>true,
    'responsiveWrap' => false,
    'responsive' => true,
    'showPageSummary'=>false,
    'pageSummaryRowOptions'=>['class' => 'text-center bg-warning text-bold'],
    'hover'=>false,
    'panel'=>[
        'type'=>GridView::TYPE_PRIMARY,
        'heading'=>'รายการทั้งหมด',
    ],
    'persistResize'=>false,
]);

$this->registerJs("
function changeType(val){
    var url = \"?type=\"+$.trim(val); $(location).attr(\"href\",url);
}
",\yii\web\View::POS_END);