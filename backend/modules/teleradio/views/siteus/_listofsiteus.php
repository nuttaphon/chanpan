<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if( 0 ){
    echo "<pre align='left'>";
    print_r($lsSiteUs);
    echo "</pre>";
}
?>
<div>
    <label >Teleradio: แสดงภาพรวม</label>
    กลับสู่ US Finding <a href="/usfinding">(Click)</a>
</div>
<div style="overflow-x:auto;">
    <table class="kv-grid-table table table-bordered table-striped">
        <thead>
            <tr>
                <th rowspan="1" style="min-width:80px; text-align:right; ">#</th>
                <th width="500px" style="min-width:250px;text-align:left; font-size:12px;" rowspan="2"><a href="#table_secshow3"
                                                                                            id='sort01' data-sort='asc'
                                                                                            fromDate='<?php echo $datadate[fromDate] ?>'
                                                                                            toDate='<?php echo $datadate[toDate] ?>'
                                                                                            sec='3' colum='hosname'>ชื่อโรงพยาบาล</a>
                </th>
                <th style="min-width:100px;text-align:right; font-size:12px;" rowspan="1"><a href="#table_secshow3" id='sort02'
                                                                              data-sort='asc'
                                                                              fromDate='<?php echo $datadate[fromDate] ?>'
                                                                              toDate='<?php echo $datadate[toDate] ?>'
                                                                              sec='3' colum='resgis'>คน</a></th>                                                             
                <th style="min-width:120px;text-align:right; font-size:12px;" rowspan="1"><a href="#table_secshow3" id='sort03'
                                                                              data-sort='desc'
                                                                              fromDate='<?php echo $datadate[fromDate] ?>'
                                                                              toDate='<?php echo $datadate[toDate] ?>'
                                                                              sec='3' colum='treat'>ครั้ง</a></th>
                <th colspan="1" style="min-width:150px; text-align:right; font-size:12px;">suspected คน (ครั้ง)</th>
                <th colspan="1" style="min-width:150px; text-align:right;font-size:12px;">PDF คน (ครั้ง)</th>
                <th colspan="1" style="min-width:150px; text-align:right;font-size:12px;">SOS</th>
                <th colspan="1" style="min-width:150px; text-align:right;font-size:12px;">ขอคำปรึกษา</th>
                <th colspan="1" style="min-width:150px; text-align:right;font-size:14px;">วันที่วางเครื่อง</th>

            </tr>
        </thead>
    <?php
    $irow=1;
    if( count($lsSiteUs)>0 ){
    ?>
        <tbody>
            <?php
            foreach($lsSiteUs as $key => $value ){
            ?>
                <tr>
                    <td class="kv-align-center kv-align-middle"
                        style="text-align:right;">
                            <?php echo $irow; ?>
                    </td>
                    <td class="kv-align-center kv-align-middle"
                        style="text-align:left;">
                            <?php echo $value[hospitalname] ?>
                    </td>
                    <td class="kv-align-middle"
                        style="text-align:right;">
                            <?php echo number_format($lsSiteUsResult[$value[hcode]]['patient'],0,'.',','); ?>
                    </td>
                    <td class="kv-align-middle"
                        style="text-align:right;">
                            <?php echo number_format($lsSiteUsResult[$value[hcode]]['dvisit'],0,'.',','); ?>
                    </td>
                    <td class="kv-align-middle"
                        style="text-align:right;">
                            <?php 
                            echo number_format($lsSiteUsResult[$value[hcode]]['patient_sus'],0,'.',','); 
                            echo " (";
                            echo number_format($lsSiteUsResult[$value[hcode]]['dvisit_sus'],0,'.',','); 
                            echo ")";
                            ?>
                    </td>
                    <td class="kv-align-middle"
                        style="text-align:right;">
                            <?php 
                            echo number_format($lsSiteUsResult[$value[hcode]]['patient_pdf'],0,'.',','); 
                            echo " (";
                            echo number_format($lsSiteUsResult[$value[hcode]]['dvisit_pdf'],0,'.',','); 
                            echo ")";
                            ?>
                    </td>
                    <td class="kv-align-middle"
                        style="text-align:right;">
                            <?php 
                            echo number_format($lsSiteUsResult[$value[hcode]]['patient_sos'],0,'.',','); 
                            //echo " (";
                            //echo number_format($lsSiteUsResult[$value[hcode]]['dvisit_consult'],0,'.',','); 
                            //echo ")";
                            ?>
                    </td>
                    <td class="kv-align-middle"
                        style="text-align:right;">
                            <?php 
                            echo number_format($lsSiteUsResult[$value[hcode]]['patient_consult'],0,'.',','); 
                            //echo " (";
                            //echo number_format($lsSiteUsResult[$value[hcode]]['dvisit_consult'],0,'.',','); 
                            //echo ")";
                            ?>
                    </td>
                    <td class="kv-align-middle"
                        style="text-align:right;">
                            <?php 
                            echo $value[dateatsite]; 
                            ?>
                    </td>
                </tr>
            <?php
                $sumpatient=$sumpatient+$lsSiteUsResult[$value[hcode]]['patient'];
                $sumdvisit=$sumdvisit+$lsSiteUsResult[$value[hcode]]['dvisit'];
                $sumpatient_sus=$sumpatient_sus+$lsSiteUsResult[$value[hcode]]['patient_sus'];
                $sumdvisit_sus=$sumdvisit_sus+$lsSiteUsResult[$value[hcode]]['dvisit_sus'];
                $sumpatient_pdf=$sumpatient_pdf+$lsSiteUsResult[$value[hcode]]['patient_pdf'];
                $sumdvisit_pdf=$sumdvisit_pdf+$lsSiteUsResult[$value[hcode]]['dvisit_pdf'];
                $sumpatient_sos=$sumpatient_sos+$lsSiteUsResult[$value[hcode]]['patient_sos'];
                $sumpatient_consult=$sumpatient_consult+$lsSiteUsResult[$value[hcode]]['patient_consult'];
                $irow++;
            }
            ?>
                <tr>
                    <td class="kv-align-center kv-align-middle">&nbsp;</td>
                    <td class="kv-align-middle" style="text-align:center;">รวม</td>
                    <td style="text-align:right;"><?= number_format($sumpatient) ?></td>
                    <td style="text-align:right;"><?= number_format($sumdvisit) ?></td>
                    <td style="text-align:right;"><?= number_format($sumpatient_sus) ?></td>
                    <!--td style="text-align:right;"><?= number_format($sumdvisit_sus) ?></td-->
                    <td style="text-align:right;"><?= number_format($sumpatient_pdf) ?></td>
                    <!--td style="text-align:right;"><?= number_format($sumdvisit_pdf) ?></td-->
                    <td style="text-align:right;"><?= number_format($sumpatient_sos) ?></td>
                    <td style="text-align:right;"><?= number_format($sumpatient_consult) ?></td>
                    <td></td>
                </tr>
        </tbody>
        
    <?php
    }
    ?>
    </table>
</div>
<?php
$jsAdd = <<< JS
var sort="$sort";
switch (sort) {
    case "asc":
        var data_sortadd=$("#$div").attr("data-sort","desc");
        break;
    case "desc":
        var data_sortadd=$("#$div").attr("data-sort","asc");
}
JS;
$this->registerJs($jsAdd);
?>
