<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Yii;
//use yii\helpers\Html;
use kartik\grid\GridView;
use app\modules\teleradio\models\HistoryUsSite;

$this->title = Yii::t('', 'รายชื่อหน่วยบริการที่ได้รับเครื่อง');


# barmenu

$reccord = HistoryUsSite::find()->count();
$perpage = $dataProvider->getPagination()->pageSize;

if( 0 ){
    echo "<pre align=left>";
    print_r($tabdet);
    echo "</pre>";
}
echo $this->render('_listofbutton',
        [
            'reccord'=>$reccord,
            'perpage'=>$perpage,
            'tabdet' => $tabdet,
        ]);


echo GridView::widget([
    'caption'=>'<h2>รายชื่อหน่วยบริการที่ได้รับการสนับสนุนเครื่องอัลตราซาวจากโครงการ CASCAP</h2>',
                'showHeader'=>true,
                'layout' => "{summary}\n{items}\n{pager}",
                'options' => array('class' => 'grid-view'),
                'tableOptions' => array('class' => 'table table-striped table-bordered'),
//              'pager'=>'yii\widgets\LinkSorter',
      'dataProvider' => $dataProvider,
      //'filterModel' => $searchModel,
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
          'attribute' => 'hcode',
          'value'=>function($model){
            return $model->hcode.': '.$model->hospitalname;
          }
        ],
        [
          'attribute' => 'man_c',
          'headerOptions' => ['class' => 'text-right'],
          'contentOptions' => ['class' => 'text-right'],
          'value'=>function($model){
            return number_format($model->man_c,0,'.',',');
          }
        ], 
        [
          'attribute' => 'time_c',
          'headerOptions' => ['class' => 'text-right'],
          'contentOptions' => ['class' => 'text-right'],
          'value'=>function($model){
            return number_format($model->time_c,0,'.',',');
          }
        ], 
        [
          'attribute' => 'pdf_c',
          'headerOptions' => ['class' => 'text-right'],
          'contentOptions' => ['class' => 'text-right'],
          'value'=>function($model){
            return number_format($model->pdf_c,0,'.',',').' ('.number_format($model['pdftime_c'],0,'.',',').')';
          }
        ], 
        [
          'attribute' => 'suspected_c',
          'headerOptions' => ['class' => 'text-right'],
          'contentOptions' => ['class' => 'text-right'],
          'value'=>function($model){
            return number_format($model->suspected_c,0,'.',',').' ('.number_format($model['suspectedtime_c'],0,'.',',').')';
          }
        ], 
        [
          'attribute' => 'sos_c',
          'headerOptions' => ['class' => 'text-right'],
          'contentOptions' => ['class' => 'text-right'],
          //'style'=>['color'=>'green','text-align'=>'right'],
          'value'=>function($model){
            return number_format($model->sos_c,0,'.',',');
          }
        ], 
        [
          'attribute' => 'consult_c',
          'headerOptions' => ['class' => 'text-right'],
          'contentOptions' => ['class' => 'text-right'],
          'value'=>function($model){
            return number_format($model->consult_c,0,'.',',');
          }
        ], 
        [
          'attribute' => 'dateatsite',
          'headerOptions' => ['class' => 'text-right'],
          'contentOptions' => ['class' => 'text-right'],
          'value'=>function($model){
            return $model->dateatsite;
          }
        ],         
      ],
  ]);