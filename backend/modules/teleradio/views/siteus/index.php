<?php
/* @var $this yii\web\View */
    $this->title = Yii::t('', 'Tele Radio: Site ที่วางเครื่อง');

          
    echo $this->render('/siteus/_listofsiteus', [
            'lsSiteUs' => $lsSiteUs,
            'lsSiteUsResult' => $lsSiteUsResult,
        ]);

?>


<!-- Modal -->
<?php
$jsAdd = <<< JS
$(document).on("click", "*[id^=sort]", function() {
    var data_sort=$(this).attr("data-sort");
    var fromDate=$(this).attr("fromDate");
    var toDate=$(this).attr("toDate");
    var sec=$(this).attr("sec");
    var colum=$(this).attr("colum");
    var div=$(this).attr('id');


    //alert("sort="+data_sort+" fromDate="+fromDate+" toDate="+toDate+" sec="+sec+" colum="+colum);
    switch (data_sort) {
        case "asc":
            var data_sortadd=$(this).attr("data-sort","desc");
            break;
        case "desc":
            var data_sortadd=$(this).attr("data-sort","asc");
    }
    var url = "/project84/report/get-data-sort";
    $.get(url, {
        sort:data_sort,
        fromDate:fromDate,
        toDate:toDate,
        sec:sec,
        colum:colum,
        div:div,
    })
    .done(function( data ) {
        switch (sec) {
            case "1":
                $("#table_sec1").empty();
                $("#table_sec1").html(data);
                break;
            case "2":
                $("#table_sec2").empty();
                $("#table_sec2").html(data);
                break;
            case "3":
                $("#table_sec3").empty();
                $("#table_sec3").html(data);
                break;
        }
    });
});
        
JS;
$this->registerJs($jsAdd);
?>