<?php

namespace backend\modules\teleradio\controllers;
use Yii;
use yii\data\ActiveDataProvider;
//use yii\data\SqlDataProvider;
use backend\modules\teleradio\classes\QueryTableDet;
use app\modules\teleradio\models\HistoryUsSite;

class SiteusController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $lsSiteUs = SiteusListHospitalController::getList();
        $lsSiteUsResult = SiteusListHospitalController::getListUsAtSite();
        return $this->render('index', [
            'lsSiteUs' => $lsSiteUs,
            'lsSiteUsResult' => $lsSiteUsResult,
        ]);
    }
    
    public function actionList(){
        
        $query = HistoryUsSite::find();
        
        $tabdet = QueryTableDet::getDetail('cascapcloud', 'history_us_site');
        //$param = Yii::$app->request->queryParams;
        
//        if(!isset($param['sort'])){
//            $query->orderBy(['dateatsite'=>'asc']);
//        }
        //$query->is_null(hsitecode);
        $dataProvider = new ActiveDataProvider([
              'query' => $query,
              'pagination' => [
                  'pageSize' => 20,
              ],
              'sort'=> ['defaultOrder' => ['dateatsite'=>SORT_ASC]]
          ]);
            
//        \appxq\sdii\utils\VarDumper::dump($dataProvider->query->createCommand()->rawSql);
        return $this->render('list',[
            'dataProvider' => $dataProvider,
            'tabdet' => $tabdet,
        ]);
    }
    
    public function actionCheckNewRecord()
    { 
        // clear table suspected
        $cca02_ezformid='1437619524091524800';
        
        $sql ="update history_us_site a left join ";
        $sql.= "( ";
        $sql.= "select cca02.hsitecode ";
        $sql.= ",count(distinct cca02.ptid) as 'patient' ";
        $sql.= ",count(distinct cca02.ptid,cca02.f2v1) as 'dvisit' ";
        $sql.= ",count(distinct if(cca02.f2v6a3b1='1',cca02.ptid,NULL)) as 'patient_sus' ";
        $sql.= ",count(distinct if(cca02.f2v6a3b1='1',concat(cca02.ptid,'-',cca02.f2v1),NULL)) as 'dvisit_sus' ";
        $sql.= ",count(distinct if(cca02.f2v2a1b2='1' or cca02.f2v2a1b2='2' or cca02.f2v2a1b2='3',cca02.ptid,NULL)) as 'patient_pdf' ";
        $sql.= ",count(distinct if(cca02.f2v2a1b2='1' or cca02.f2v2a1b2='2' or cca02.f2v2a1b2='3',concat(cca02.ptid,'-',cca02.f2v1),NULL)) as 'dvisit_pdf' ";
        $sql.= ",count(distinct if(ezform_reply.type='2',cca02.ptid,NULL)) as 'patient_consult' ";
        $sql.= ",count(distinct if(ezform_reply.type='2',concat(cca02.ptid,'-',cca02.f2v1),NULL)) as 'dvisit_consult' ";
        $sql.= ",count(distinct if(ezform_reply.type='3',cca02.ptid,NULL)) as 'patient_sos' ";
        $sql.= ",count(distinct if(ezform_reply.type='3',concat(cca02.ptid,'-',cca02.f2v1),NULL)) as 'divist_sos' ";
        $sql.= "from tb_data_3 cca02 ";
        $sql.= "inner join history_us_site ussite on ussite.hcode=cca02.hsitecode ";
        $sql.= "left join ezform_reply on ezform_reply.data_id=cca02.id and ezform_reply.`ezf_id`='".$cca02_ezformid."' ";
        $sql.= "where cca02.rstat<>'3' ";
        $sql.= "and length(ussite.hcode)>0 ";
        $sql.= "and cca02.f2v1>=ussite.dateatsite ";
        $sql.= "group by cca02.hsitecode ";
        $sql.= "order by ussite.dateatsite ";
        $sql.= ") ";
        $sql.= "b on a.hcode=b.hsitecode ";
        $sql.= "set a.man_c=b.patient ";
        $sql.= ",a.time_c=b.dvisit ";
        $sql.= ",a.suspected_c=b.patient_sus ";
        $sql.= ",a.suspectedtime_c=b.dvisit_sus ";
        $sql.= ",a.pdf_c=b.patient_pdf ";
        $sql.= ",a.pdftime_c=b.dvisit_pdf ";
        $sql.= ",a.sos_c=b.divist_sos ";
        $sql.= ",a.consult_c=b.patient_consult ";
        
        //echo $sql;
        Yii::$app->db->createCommand($sql)->execute();
        // Redirect page
        //
        return $this->redirect(['list']);
  
    }


    public function actionSuspected(){
        return $this->render('suspected');
    }
    

}
