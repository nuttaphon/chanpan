<?php

namespace backend\modules\teleradio\controllers;

use Yii;
use backend\modules\ezforms\models\EzformReply;
use backend\modules\teleradio\models\TeleRadioDiag;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        if($_GET['type']=='all')
            $type = "type<> 9";
        else if($_GET['type']=='')
            $type ="type =2";
        else
            $type = "type = ".$_GET['type'];

        $type .= " AND ezf_id = '1437619524091524800'";

        $dataProvider = new ActiveDataProvider([
            'query' => EzformReply::find()->select('id, ezf_id, data_id, ezf_comment, file_upload, user_create, create_date, update_date, xsourcex, rstat, type')->where($type)->groupBy('data_id')->orderBy('create_date DESC'),
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionConsultView($id){
        $model = EzformReply::find()->where('id=:id', [':id'=>$id])->one();
        //->xsourcex
        $sql="SELECT hcode, name, tambon, amphur, province FROM all_hospital_thai WHERE hcode=:hcode";
        $hosp = \Yii::$app->db->createCommand($sql,[':hcode' => $model->xsourcex])->queryOne();
        $model->xsourcex  = $hosp['hcode'].' : '.$hosp['name'].' '.$hosp['province'];
        //
        $date = new \DateTime($model->create_date);
        $model->create_date = $date->format('d/m/Y');
        //
        if($model->type == 1)
            $model->type = 'แสงความคิดเห็นทั่วไป';
        else if($model->type == 2)
            $model->type = 'ขอคำปรึกษา';
        else if($model->type == 3)
            $model->type = 'SOS';
        //
        $model->ezf_comment = mb_substr($model->ezf_comment, 0,50,'UTF-8').'...';
        //
        $sql="SELECT ezf_name FROM ezform WHERE ezf_id=:ezf_id";
        $hosp = \Yii::$app->db->createCommand($sql,[':ezf_id' => $model->ezf_id])->queryOne();
        $model->ezf_id = $hosp['ezf_name'];
        //
        $modelTeleList = TeleRadioDiag::find()->where(['ref_consult_id' => $id]);
        $modelTele = new TeleRadioDiag();

        return $this->render('consult-view', [
            'model' => $model,
            'modelTele' => $modelTele,
            'modelTeleList' => $modelTeleList,
        ]);
    }

    public function actionDiagReply()
    {
        $model = new TeleRadioDiag();
        if($model->load(Yii::$app->request->post())){
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->xsourcex = Yii::$app->user->identity->userProfile->sitecode;
            if($model->save()){
                return $this->redirect(Url::to(Yii::$app->request->referrer));
            }
        }
    }

    public function renderDiagReplyList($id)
    {
        $modelReplyData = new ActiveDataProvider([
            'query' => TeleRadioDiag::find()->where(['ref_consult_id' => $id])->orderBy('create_date DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $view = Yii::$app->getView();
        return $view->render('@backend/modules/teleradio/views/default/diag_reply', ['modelReplyData'=>$modelReplyData]);
    }

}
