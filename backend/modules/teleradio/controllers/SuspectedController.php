<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\teleradio\controllers;

use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
//use yii\helpers\VarDumper;
//use yii\web\Controller;
use backend\modules\teleradio\classes\QueryTableDet;
use app\modules\teleradio\models\SuspectedCca;
use Yii;

class SuspectedController extends \yii\web\Controller {
    
    public function actionIndex()
    { 
        $tabdet = QueryTableDet::getDetail('cascapcloud', 'tb_data_3_suspected');
        // manage data provider
        $dataProvider = new ActiveDataProvider([
              'query' => SuspectedCca::find(),
              'pagination' => [
                  'pageSize' => 20,
              ],
              'sort'=> ['defaultOrder' => ['f2v1'=>SORT_DESC]]
          ]);
        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'tabdet' => $tabdet,
        ]);
    }
    public function actionPdfList()
    { 
        // manage data provider
        $dataProvider = new ActiveDataProvider([
              'query' => SuspectedCca::find()->where(['f2v6a3b1'=>'1']),
              'pagination' => [
                  'pageSize' => 20,
              ]
          ]);
        return $this->render('pdfList',[
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCheckNewRecord()
    { 
        // clear table suspected
        $sql = "truncate tb_data_3_suspected ";
        Yii::$app->db->createCommand($sql)->execute();
        // insert to suspected
        $sql = "insert into tb_data_3_suspected ";
        $sql.= "select cca02.*,'','','','','','','','','','','' from tb_data_3 cca02 where cca02.f2v6a3b1='1' ";
        $sql.= "order by f2v1 desc ";
        Yii::$app->db->createCommand($sql)->execute();
        // update name from register
        $sql = "update tb_data_3_suspected susp inner join tb_data_1 reg on reg.ptid=susp.ptid "; 
        $sql.= "set susp.name=reg.name ";
        $sql.= ",susp.surname=reg.surname ";
        $sql.= ",susp.title=reg.title ";
        $sql.= "where length(reg.name)>0 ";
        Yii::$app->db->createCommand($sql)->execute();
        // update abnormal
        $sql = "update tb_data_3_suspected  ";
        $sql.= "set usabnormal= ";
        $sql.= "case 1 when f2v2a1='0' then '0: Normal'  ";
        $sql.= "when f2v2a1b1='1' then '1a) Mild fatty liver'  ";
        $sql.= "when f2v2a1b1='2' then '1b) Moderate fatty liver'  ";
        $sql.= "when f2v2a1b1='3' then '1c) Severe fatty liver'  ";
        $sql.= "when f2v2a1b2='1' then '2a) PDF 1'  ";
        $sql.= "when f2v2a1b2='2' then '2b) PDF 2'  ";
        $sql.= "when f2v2a1b2='3' then '2c) PDF 3'  ";
        $sql.= "when f2v2a1b3='1' then '3a) Cirrhosis'  ";
        $sql.= "when f2v2a1b4='1' then '4a) Parenchymal change'  ";
        $sql.= "else '-'  ";
        $sql.= "end ";
        Yii::$app->db->createCommand($sql)->execute();
        $sql = "delete from tb_data_3_suspected ";
        $sql.= "where (hsitecode like '91%' or hsitecode like '92%' or hsitecode like 'A%' or hsitecode like 'Z%' or hsitecode like 'z%') ";
        Yii::$app->db->createCommand($sql)->execute();
        // update suspected
        $sql = "update tb_data_3_suspected  ";
        $sql.= "set ussuspected= ";
        $sql.= "case 1 when f2v2a2b5c1='1' or f2v2a2b6c1='1' or f2v2a2b7c1='1' or f2v2a3b1='1' or f2v2a3b2='1' or f2v2a3b3='1' then '<span style=\"color: blue\">Suspected</span>'  ";
        $sql.= "else '<span style=\"color: blue\">Suspected</span> <span style=\"color: red\">[มี Error]</span>'  ";
        $sql.= "end ";
        Yii::$app->db->createCommand($sql)->execute();
        // Confirm
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_4 ctmri on ctmri.ptid=sus.ptid ";
        $sql.= "set ctmri_result= ";
        $sql.= "case 1 when ctmri.f2p1v3='0' then '<span style=\"color: black\">- Normal</span>'  ";
        $sql.= "when ctmri.f2p1v3='1' then '<span style=\"color: blue\">+ Intrahepatic</span>'  ";
        $sql.= "when ctmri.f2p1v3='2' then '<span style=\"color: blue\">+ Perihilar</span>'  ";
        $sql.= "when ctmri.f2p1v3='3' then '<span style=\"color: blue\">+ Distal</span>'  ";
        $sql.= "when ctmri.f2p1v3='4' then '<span style=\"color: black\">- Not CCA</span>'  ";
        //$sql.= "else '<span style=\"color: red\">...</span>'  ";
        $sql.= "end ";
        $sql.= "where ctmri.rstat<>'3' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-03
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_7 cca03 on cca03.ptid=sus.ptid ";
        $sql.= "set treat_result= ";
        $sql.= "case 1 when cca03.f3v5a1='1' then '<span style=\"color: black\">Surgery</span>'  ";
        $sql.= "when cca03.f3v5a1='0' then '<span style=\"color: black\">- xxx</span>'  ";
        //$sql.= "else '<span style=\"color: red\">...</span>'  ";
        $sql.= "end ";
        $sql.= "where cca03.rstat<>'3' ";
        $sql.= "and f3v5a1='1' ";
        Yii::$app->db->createCommand($sql)->execute();
        // CCA-04
        $sql = "update tb_data_3_suspected sus ";
        $sql.= "inner join tb_data_8 cca04 on cca04.ptid=sus.ptid ";
        $sql.= "set patho_result= ";
        $sql.= "case 1 when cca04.f4v4='1' then '<span style=\"color: blue\">+  Intra</span>'  ";
        $sql.= "when cca04.f4v4='2' then '<span style=\"color: blue\">+ Perihilar</span>'  ";
        $sql.= "when cca04.f4v4='3' then '<span style=\"color: blue\">+ Distal</span>'  ";
        $sql.= "when cca04.f4v4='4' then '<span style=\"color: black\">+ Other</span>'  ";
        $sql.= "when cca04.f4v4='5' then '<span style=\"color: black\">+ not done</span>'  ";
        //$sql.= "else '<span style=\"color: red\">...</span>'  ";
        $sql.= "end ";
        $sql.= "where cca04.rstat<>'3' ";
        $sql.= "and (f4v4='1' or f4v4='2' or f4v4='3' or f4v4='4' or f4v4='5') ";
        Yii::$app->db->createCommand($sql)->execute();
        // Redirect page
        //
        return $this->redirect(['index']);
    }
    
    // ไม่ได้ถูกเรียกใช้
    public function getListSuspected(){
        $count = Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM tb_data_3_suspected WHERE f2v6a3b1=:suspected
        ', [':suspected' => 1])->queryScalar();

        $provider = new SqlDataProvider([
            'sql' => 'SELECT * FROM tb_data_3_suspected WHERE f2v6a3b1=:suspected',
            'params' => [':suspected' => 1],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'hsitecode',
                    'hptcode',
                    'f2v1',
                ],
            ],
        ]);

        // returns an array of data rows
        //$models = $provider->getModels;
        return $provider;
    }
    
    public function createTableSuspected(){
        $userId = \Yii::$app->user->identity->id;
        $tempTableName = "sespected_".$userId.str_replace('.','',microtime(true));
        // create tem table
        $sqlCreateTable = "CREATE TEMPORARY TABLE ".$tempTableName." ";
        $sqlCreateTable.= "select * from tb_data_3 where rstat<>'3' and f2v6a3b1='1' ";
        Yii::$app->db->createCommand($sqlCreateTable)->execute();
        
        $count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM tb_data_3 ')->queryScalar();
        
        $provider = new SqlDataProvider([
            'sql' => 'SELECT * FROM '.$sqlCreateTable.' order by hsitecode,hptcode,f2v1 ',
            'params' => [':suspected' => 1],
            'totalCount' => $count,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'hsitecode',
                    'hptcode',
                    'f2v1',
                ],
            ],
        ]);
        
        
        
        return $provider;
    }
    
    
}

