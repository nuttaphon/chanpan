<?php

namespace backend\modules\ezforms\controllers;

use backend\modules\ezforms\models\EzformDataLog;
use backend\modules\ezforms\models\EzformSqlLog;
use backend\modules\ezforms\models\EzformReply;
use NumberFormatter;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use \yii\web\UploadedFile;
use backend\models\EzformTarget;
use backend\modules\ezforms\models\EzformDynamic;
use trntv\filekit\widget\Upload;
use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\models\EzformChoice;
use backend\modules\component\models\EzformComponent;
use yii\helpers\ArrayHelper;
use backend\models\EzformCoDev;
use backend\models\EzformAssign;
use yii\helpers\Url;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\components\EzformFunc;
use yii\widgets\ActiveForm;

/**
 * EzformController implements the CRUD actions for Ezform model.
 */
class EzformController extends Controller
{

    public $layout = '@backend/views/layouts/common';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'undo'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        //Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Yii::getAlias('@storageUrl') . '/form-upload', // Directory URL address, where files are stored.
                'path' => '@app/../storage/web/form-upload', // Or absolute path to directory where files are stored.
                //'type' => GetAction::TYPE_IMAGES,
            ],
        ];
        //use vova07/imperavi/actions/UploadAction
    }

    public function actionFixfield()
    {
        $modelform = Ezform::find()->where('status<>0')->all();

        foreach ($modelform as $form) {
            $ezf_id = $form->ezf_id;
            $modelfield = new EzformFields();
            $modelfield->ezf_field_id = GenMillisecTime::getMillisecTime();
            $modelfield->ezf_id = $ezf_id;
            $modelfield->ezf_field_name = "id";
            $modelfield->ezf_field_label = "ID ข้อมูล";
            $modelfield->ezf_field_type = "0";
            $modelfield->save();
        }

    }

    public function beforeAction($action)
    {
        if($_POST['onmobile']) {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)) {
            if (in_array($action->id, array('create', 'update'))) {

            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all Ezform models.
     * @return mixed
     */
    public function actionRandomDrug($id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
	    $data = EzformFunc::getRandomDrug($id);
	    
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong>Success!</strong> ' . Yii::t('app', 'get drug'),
		'data'=>$data,
            ];
            return $result;
        }
    }
    
    public function actionFormautosave()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($_POST['id']);
            $field = explode('-', $_POST['field']);

            $model->{$field[1]} = $_POST['value'];
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

    public function actionUpdateformstatus()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($_POST['id']);
            $model->shared = $_POST["shared"];
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }
    
    public function actionUpdateformcomptype()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($_POST['id']);
            $model->comp_type = $_POST["comp_type"];
            if ($_POST["comp_type"]=="0") {
                $model->comp_id_target="";
            }else if ($_POST["comp_type"]=="1") {
                $model->comp_id_target= Yii::$app->keyStorage->get('personprofile.comp_id');
            }
            $_SESSION['showsetting']=1;
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }    

    public function actionUseEzformOption()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($_POST['id']);
            if($_POST['action']) {
                $model->{$_POST['action']} = $_POST["status"];
            }
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

    public function actionUpdatepublic1()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($_POST['id']);
            $model->public_listview = $_POST["public_listview"];
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

    public function actionUpdatepublic2()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($_POST['id']);
            $model->public_edit = $_POST["public_edit"];
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

    public function actionViewform($id, $message = '')
    {
        $modelform = $this->findModel($id);
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        return $this->render('viewform', [
            'modelform' => $modelform,
            'modelfield' => $modelfield,
            'message' => $message,
            'model_gen' => $model_gen]);
    }

    public function actionSavedata()
    {

        $file = $_FILES;

        $dataid = GenMillisecTime::getMillisecTime();
        $filelist = "";


        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            if (isset($_FILES)) {
                foreach ($_FILES as $key => $val) {
                    $filelist = "";
                    $file = \yii\web\UploadedFile::getInstanceByName($key);
                    if (isset($file)) {
                        $filename = $file->name;
                        $filenameEx = explode(".", $filename);
                        $filenameNew = GenMillisecTime::getMillisecTime();
                        $filenameFull = $filenameNew . "." . $filenameEx[1];
                        if (!file_exists(Yii::$app->basePath . "/web/uploads/imgform/" . $_POST["ezf_id"])) {
                            mkdir(Yii::$app->basePath . "/web/uploads/imgform/" . $_POST["ezf_id"], 0777, true);
                        }
                        $path = Yii::$app->basePath . "/web/uploads/imgform/" . $_POST["ezf_id"] . "/" . $_POST['id'] . "_" . $filenameFull;
                        $file->saveAs($path, $deleteTempFile = true);
                        $filelist .= $filenameFull;
                        $fvalue[$key] = $filelist;
                    }
                }
            }
            $user = Yii::$app->user->id;
            $fields = '';
            $values = '';
            foreach ($_POST as $key => $value) {
                if ($key == '_csrf')
                    continue;
                if ($key == 'ezf_id')
                    continue;
                if ($key == 'id')
                    continue;
                if ($key == 'url_redirect')
                    continue;
                if ($key == 'target') {
                    if ($value == 'skip')
                        continue;
                }
                if (is_array($value)) {
                    $arrayValue = implode(',', $value);
                    unset($value);
                    $value = $arrayValue;
                }
                $fields .= ',' . $key;
                $values .= ',\'' . $value . '\'';
            }
            $user = Yii::$app->user->id;

            $sql = "REPLACE INTO `tbdata_" . $_POST['ezf_id'] . "` (id,rstat,user_create,create_date" . $fields . ") VALUES "
                . "('" . $_POST['id'] . "','2','" . $user . "',NOW()" . $values . ") ";

            Yii::$app->db->createCommand($sql)->execute();
            foreach ($_FILES as $key => $value) {

                if (isset($fvalue[$key])) {

                    $sqlUpload = "UPDATE `tbdata_" . $_POST['ezf_id'] . "` "
                        . "SET $key='$fvalue[$key]' WHERE id='" . $id . "' ";

                    Yii::$app->db->createCommand($sqlUpload)->execute();
                }
            }
            if (Yii::$app->request->post('url_redirect')) {
                $url_redirect = base64_decode(Yii::$app->request->post('url_redirect'));
                return $this->redirect($url_redirect, 302);
            } else
                return $this->actionViewform($_POST["ezf_id"], 'บันทึกข้อมูลเรียบร้อย');
        } else {
            if (isset($_FILES)) {
                foreach ($_FILES as $key => $val) {
                    $filelist = "";
                    $file = \yii\web\UploadedFile::getInstanceByName($key);
                    if (isset($file)) {
                        $filename = $file->name;
                        $filenameEx = explode(".", $filename);
                        $filenameNew = GenMillisecTime::getMillisecTime();
                        $filenameFull = $filenameNew . "." . $filenameEx[1];
                        if (!file_exists(Yii::$app->basePath . "/web/uploads/imgform/" . $_POST["ezf_id"])) {
                            mkdir(Yii::$app->basePath . "/web/uploads/imgform/" . $_POST["ezf_id"], 0777, true);
                        }
                        $path = Yii::$app->basePath . "/web/uploads/imgform/" . $_POST["ezf_id"] . "/" . $dataid . "_" . $filenameFull;
                        $file->saveAs($path, $deleteTempFile = true);
                        $filelist .= $filenameFull;
                        $fvalue[$key] = $filelist;
                    }
                }
            }

            $fields = '';
            $values = '';
            foreach ($_POST as $key => $value) {
                if ($key == '_csrf')
                    continue;
                if ($key == 'ezf_id')
                    continue;
                if ($key == 'id')
                    continue;
                if ($key == 'url_redirect')
                    continue;
                if ($key == 'target') {
                    if ($value == 'skip')
                        continue;
                }

                if (is_array($value)) {
                    $arrayValue = implode(',', $value);
                    unset($value);
                    $value = $arrayValue;
                }
                $fields .= ',' . $key;
                $values .= ',\'' . $value . '\'';
            }
            $user = Yii::$app->user->id;

            $sql = "REPLACE INTO `tbdata_" . $_POST['ezf_id'] . "` (id,rstat,user_create,create_date" . $fields . ") VALUES "
                . "('" . $dataid . "','2','" . $user . "',NOW()" . $values . ") ";


            Yii::$app->db->createCommand($sql)->execute();
            foreach ($_FILES as $key => $val) {
                if (isset($fvalue[$key])) {
                    $sqlUpload = "UPDATE `tbdata_" . $_POST['ezf_id'] . "` "
                        . "SET $key='$fvalue[$key]' WHERE id='" . $dataid . "' ";
                    Yii::$app->db->createCommand($sqlUpload)->execute();

                }
                if (Yii::$app->request->post('url_redirect')) {
                    $url_redirect = base64_decode(Yii::$app->request->post('url_redirect'));
                    return $this->redirect($url_redirect, 302);
                } else
                    return $this->actionViewform($_POST["ezf_id"], 'บันทึกข้อมูลเรียบร้อย');
            }
            if (Yii::$app->request->post('target')) {
                $sql = "REPLACE INTO `tbdata_target` (`ezf_id`, `data_id`, `target_id`) VALUES ('" . $_POST['ezf_id'] . "', '" . $dataid . "', '" . $_POST['target'] . "');";
                Yii::$app->db->createCommand($sql)->execute();
            }
            if (Yii::$app->request->post('url_redirect')) {
                $url_redirect = base64_decode(Yii::$app->request->post('url_redirect'));
                return $this->redirect($url_redirect, 302);
            } else
                return $this->actionViewform($_POST["ezf_id"], 'บันทึกข้อมูลเรียบร้อย');
        }
    }

    public function actionUpdatepublic3()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($_POST['id']);
            $model->public_delete = $_POST["public_delete"];
            $model->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

    public function actionSaveassign()
    {
        if (Yii::$app->getRequest()->isAjax) {

            $ezf_id = Yii::$app->request->post('ezf_id');
            Yii::$app->db->createCommand("DELETE FROM ezform_assign WHERE ezf_id='$ezf_id';")->query();
            if(Yii::$app->request->post('val') != '') {
                $user_id = Yii::$app->request->post('val');
                $values = '';
                $userlist = '';
                foreach ($user_id as $k => $val) {
                    $values .= "('$ezf_id', '$val'), ";
                    $userlist .= ',' . $val;
                }
                $values = substr($values, 0, -2);
                $userlist = substr($userlist, 1);

                $sql = "INSERT INTO ezform_assign (`ezf_id`, `user_id`) VALUES" . $values;
                Yii::$app->db->createCommand($sql)->query();
            }else{
                $userlist = null;
            }

            $sql = "UPDATE ezform set assign='$userlist' WHERE ezf_id='$ezf_id'";
            Yii::$app->db->createCommand($sql)->query();
            //
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

    public function actionSaveConsultant($type=null)
    {
        if (Yii::$app->getRequest()->isAjax) {

            //VarDumper::dump(),10,true);exit;
            if($type=='consultant') {
                Yii::$app->db->createCommand()->update('ezform', ['consultant_users' => implode(',', $_POST['val']),], ['ezf_id' => $_POST['ezf_id']])->execute();
            }else if($type =='telegram'){
                Yii::$app->db->createCommand()->update('ezform', ['telegram' => $_POST['val'],], ['ezf_id' => $_POST['ezf_id']])->execute();
            }
            //
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ',
            ];
            return $result;
        }
    }

    public function actionSavefielddetail()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $ezf_id = Yii::$app->request->post('ezf_id');
            $fields = Yii::$app->request->post('val');

            //\yii\helpers\VarDumper::dump($fields);

            $fieldlist = '';

            if ('' != $fields) {
                if (count($fields) > 0) {
                    foreach ($fields AS $val) {
                        $fieldlist .= ',' . $val;
                    }
                }
            }

            $fieldlist = substr($fieldlist, 1);
            //$fieldlist = $fields;
            $sql = "UPDATE ezform set field_detail='$fieldlist' WHERE ezf_id='$ezf_id'";
            Yii::$app->db->createCommand($sql)->query();
            //
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'บันทึกรายละเอียดแล้ว'),
            ];
            return $result;
        }
    }

    public function actionIndex()
    {
	$tab = isset($_GET['tab'])?$_GET['tab']:4;
	
        $searchModel = new EzformSearch();
	if($tab==2){
	    $dataProvider = $searchModel->searchMyFormPublic(Yii::$app->request->queryParams);
	} elseif ($tab==3) {
	    $dataProvider = $searchModel->searchMyForm(Yii::$app->request->queryParams);
	} elseif ($tab==4) {
	    $dataProvider = $searchModel->searchMyFormAssign(Yii::$app->request->queryParams);
	} elseif ($tab==5) {
	    $dataProvider = $searchModel->searchMyFormDelete(Yii::$app->request->queryParams);
	} elseif ($tab==6) {
	    $dataProvider = $searchModel->searchMyFormTdc(Yii::$app->request->queryParams);
	} else {
	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	}
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'tab'=>$tab,
        ]);
    }

    /**
     * Displays a single Ezform model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        echo $id;
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($id);

            $users = User::findOne($model->user_create);

            $model->username = $users->username;

            return $this->renderAjax('view', [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Creates a new Ezform model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionViewonline($id, $message = '')
    {
        $modelform = $this->findModel($id);
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        return $this->render('viewform_public', [
            'modelform' => $modelform,
            'modelfield' => $modelfield,
            'message' => $message,
            'model_gen' => $model_gen]);
    }

    public function actionValueupdate($tableid, $recordid)
    {
        $modelfield = EzformFields::find()->where("ezf_id = :ezf_id", [':ezf_id' => $tableid])->all();
        $arrayValue = array();
        foreach ($modelfield as $field) {
            $sql = "SELECT * FROM tbdata_" . $tableid . " WHERE id='" . $recordid . "' ";
            $modelvalue = Yii::$app->db->createCommand($sql)->queryOne();
            $fieldname = $field->ezf_field_name;
            $value = $modelvalue[$fieldname];
            array_push($arrayValue, $value);
        }
        print_r($arrayValue);
    }

    public function actionCreate()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = new Ezform();
	    $model->ezf_version = 'v1';
	    
            $ezformComponents = EzformComponent::find()
                ->where(['user_create' => Yii::$app->user->id])
                ->orWhere(['shared' => 1])
                ->orWhere('comp_id<1000000')
                ->all();
            $ezformComponent_maps = ArrayHelper::map($ezformComponents, 'comp_id', 'comp_name');

            $ezf_id = GenMillisecTime::getMillisecTime();

            $model->ezf_id = $ezf_id;
            $model->ezf_table = 'tbdata_' . $ezf_id;
            $model->user_create = Yii::$app->user->id;
            $model->user_update = Yii::$app->user->id;
            $model->status = 1;
            $model->assign = Yii::$app->user->id;

            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                //$tmp = Yii::$app->request->post();
                //\yii\helpers\VarDumper::dump($tmp);
                //Yii::app()->end();

                if ($model->save()) {

                    Yii::$app->db->createCommand('CREATE TABLE tbdata_' . $ezf_id . ' LIKE tbdata_init')
                        ->execute();

                    $model_co_dev = new EzformCoDev;
                    $model_co_dev->ezf_id = $ezf_id;
                    $model_co_dev->user_co = Yii::$app->user->id;
                    $model_co_dev->status = 0;
                    $model_co_dev->save();

                    $model_assign = new EzformAssign;
                    $model_assign->ezf_id = $ezf_id;
                    $model_assign->user_id = Yii::$app->user->id;
                    $model_assign->status = 1;
                    $model_assign->save();
                    //favorite
                    $userid = Yii::$app->user->id;
                    Yii::$app->db->createCommand("REPLACE INTO ezform_favorite (`ezf_id`, `userid`, `forder`) VALUES ('$ezf_id', '$userid', '999');")->execute();

                    $modelfield = new EzformFields;
                    $modelfield->ezf_field_id = GenMillisecTime::getMillisecTime();
                    $modelfield->ezf_id = $ezf_id;
                    $modelfield->ezf_field_name = "id";
                    $modelfield->ezf_field_label = "ID ข้อมูล";
                    $modelfield->ezf_field_type = "0";
                    $modelfield->save();

                    $result = [
                        'status' => 'success',
                        'action' => 'create',
                        'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
                        'data' => $model,
                    ];
                    return $result;
                } else {
                    $result = [
                        'status' => 'error',
                        'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
                        'data' => $model,
                    ];
                    return $result;
                }
            } else {
                $model->shared = 0;
                $model->public_listview = false;
                $model->public_edit = false;
                $model->public_delete = false;

                return $this->renderAjax('create', [
                    'model' => $model,
                    'ezformComponents' => $ezformComponent_maps
                ]);
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }
    public function actionAnnotated ($ezf_id) {
        $modelcompfield = EzformFields::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->orderBy(['ezf_field_order' => SORT_ASC])->all();
        
        $modelcompezform = Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
        
        $modelcompgen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelcompfield);

        $prefix = '<div class="panel panel-primary">
            <div class="panel-heading clearfix" id="panel-ezform-box">
                <h3 class="panel-title">ฟอร์มเป้าหมาย '.$modelform['ezf_name'].'</h3>
        </div>
            <div class="panel-body" id="panel-ezform-body">';        

        return $prefix.$annotated = $this->renderAjax('annotated',[
                'modelfield' => $modelcompfield,
                'model_gen' => $modelcompgen,
                'modelezform' => $modelcompezform,            
        ]).'</div></div>';        
    }

    /**
     * Updates an existing Ezform model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model1 = $this->findModel($id);

        $co_devs = EzformCoDev::find()
            ->where(['ezf_id' => $id, 'user_co'=>Yii::$app->user->id])
            ->scalar();
        if($model1->user_create != Yii::$app->user->id && (!Yii::$app->user->can('administrator')) && !$co_devs){
            return $this->renderContent('You don\'t have permission for access form.');
        }

        $model3 = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $model1->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();

        $ezformComponents = EzformComponent::find()
            ->where('(user_create=:user_create OR shared =1) and status<>3', [':user_create'=>Yii::$app->user->id])
            ->all();
        $ezformComponent_maps = ArrayHelper::map($ezformComponents, 'comp_id', 'comp_name');


        $userlist = EzformQuery::getIntUserAll(); //explode(",", $model1->assign);

        $userlist = ArrayHelper::map($userlist, 'id', 'text');
	$model1->assign = explode(',', $model1->assign);

        $co_devs = EzformCoDev::find()
            ->where(['ezf_id' => $id])
            ->all();

        $model1->field_detail = explode(",", $model1->field_detail);

        $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($model3);

        //get table from tcc bot
        $tccTables = [];
        try {
            $sql = "select id, CONCAT(tbname, ' (',IF(LENGTH(`title`),`title`,'no detail'),')') as text from `tdc_webservice`.`buffe_data_tables`;";
            $tccTables = Yii::$app->dbsvr1->createCommand($sql)->queryAll();
            $tccTables = ArrayHelper::map($tccTables, 'id', 'text');
        }catch (Exception $e){

        }

	$configModel = new \backend\modules\ezforms2\models\EzformConfigSearch();
	$dpReport = $configModel->search(Yii::$app->request->queryParams, $id, 'report');
        
        $comp_id = $model1->comp_id_target;
        
        $ezfcomp = EzformComponent::findOne(['comp_id'=>$comp_id]);
        
        $modelcompfield = EzformFields::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezfcomp->ezf_id])->orderBy(['ezf_field_order' => SORT_ASC])->all();

        $modelcompezform = Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezfcomp->ezf_id])->one();
        
        $modelcompgen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelcompfield);
        
        $annotated = $this->renderAjax('annotated',[
                'modelfield' => $modelcompfield,
                'model_gen' => $modelcompgen,
                'modelezform' => $modelcompezform,            
        ]);

        return $this->render("update", ['model1' => $model1,
            'model3' => $model3,
            'ezf_id' => $id,
            'ezformComponents' => $ezformComponent_maps,
            'co_devs' => $co_devs,
            'model_gen' => $model_gen,
	        'userlist' => $userlist,
            'tccTables' => $tccTables,
	    'dpReport'=>$dpReport,
            'ezfcomp'=>$ezfcomp->ezf_id,
        ]);
    }

    public function actionTccBotMapping(){
        $sql = "select id, tbname from `buffe_webservice`.`buffe_data_table` WHERE id=:id;";
        $tccTables = Yii::$app->dbbot->createCommand($sql, [':id'=>$_POST['val']])->queryOne();
        $dbname=  Yii::$app->dbbot->createCommand("SELECT DATABASE();")->queryScalar();
        $sql="SELECT '{$tccTables['tbname']}' as tbname,COLUMN_NAME as fname, COLUMN_TYPE as ftype, IS_NULLABLE as isnull,'' as caption,'' as description,COLUMN_KEY as pk FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{$tccTables['tbname']}' AND table_schema = '{$dbname}'";
        $model = \Yii::$app->dbbot->createCommand($sql)->queryAll();
        $dataprovider = new ArrayDataProvider([
            'allModels'=>$model,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        //list field
        $modelField = Yii::$app->db->createCommand("SELECT `ezf_id`, `ezf_field_id`, `ezf_field_name`, `ezf_field_label`, `ezf_field_type`, CONCAT(ezf_field_name, ' (', IF(LENGTH(`ezf_field_label`), `ezf_field_label`, 'no detail'), ')') AS `text` FROM `ezform_fields` WHERE ezf_id=:ezf_id ORDER BY `ezf_field_order`", [':ezf_id'=>$_POST['ezf_id']])->queryAll();
        $listField = ArrayHelper::map($modelField, 'ezf_field_name', 'text');

        return $this->renderAjax('_tcc_bot_mapping', [
            'listField' => $listField,
            'dataprovider' => $dataprovider,
            'ezf_id' => $_POST['ezf_id'],
        ]);
    }

    public function actionTccBotMappingField(){
        $sql = "select tbname from `buffe_webservice`.`buffe_data_table` where id=:id;";
        $tccTables = Yii::$app->dbbot->createCommand($sql, ['id'=>$_POST['tcc_tb']])->queryOne();

        if($_POST['type']=='mapping'){
            Yii::$app->db->createCommand()->delete('ezform_maping_tcc', ['ezf_id' => $_POST['ezf_id'], 'field_ezf' => $tccTables['field_ezf'],])->execute();
            Yii::$app->db->createCommand()->insert('ezform_maping_tcc', [
                'ezf_id' => $_POST['ezf_id'],
                'tcc_tb' => $tccTables['tbname'],
                'field_tcc' => $_POST['field_tcc'],
                'field_ezf' => $_POST['field_ezf'],
            ])->execute();
        }else if($_POST['type']=='un-mapping'){
            Yii::$app->db->createCommand()->delete('ezform_maping_tcc', ['ezf_id' => $_POST['ezf_id'], 'field_ezf' => $tccTables['field_ezf']])->execute();
        }

        //
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [
            'status' => 'success',
            'message' => '<strong><i class="fa fa-check"></i> Success!</strong> ',
        ];
        return $result;
    }

    public function actionTccBotFieldDecode(){
        $sql = "select tbname from `buffe_webservice`.`buffe_data_table` where id=:id;";
        $tccTables = Yii::$app->dbbot->createCommand($sql, ['id'=>$_POST['tcc_tb']])->queryOne();

        Yii::$app->db->createCommand()->update('ezform_maping_tcc', ['decode'=>$_POST['decode']], ['ezf_id' => $_POST['ezf_id'], 'tcc_tb' => $tccTables['tbname'], 'field_tcc' => $_POST['field_tcc'],])->execute();

        //
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [
            'status' => 'success',
            'message' => '<strong><i class="fa fa-check"></i> Success!</strong> ',
        ];
        return $result;
    }

    public  function actionFixForm(){
        $id = $_POST['id'];
        $strSqlForm = 'SELECT * FROM ezform WHERE ezf_id="'.$id.'"';
        $qryAllFormDetail = Yii::$app->db->createCommand($strSqlForm)->queryAll();

        $strSqlFieldSchema = 'SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = "'.$qryAllFormDetail[0]['ezf_table'].'" AND table_schema = "'.explode('=', getenv('DB_DSN'))['3'].'"';
        $qryAllFieldSchema = Yii::$app->db->createCommand($strSqlFieldSchema)->queryAll();
        $allFieldSchema = array();
        foreach($qryAllFieldSchema as $fieldKey => $fieldValue ){
            array_push($allFieldSchema, $fieldValue['COLUMN_NAME']);
        }

        $strSqlField = 'SELECT * FROM ezform_fields WHERE ezf_id="'.$id.'"';
        $qryAllField = Yii::$app->db->createCommand($strSqlField)->queryAll();


        foreach($qryAllField as $fieldKey => $fieldValue ){
            if(!in_array($fieldValue['ezf_field_name'], $allFieldSchema)){
                $dataType = EzformController::checkDataType($fieldValue['ezf_field_type']);
                $fieldName = $fieldValue['ezf_field_name'];
                $strSqlAlter = 'ALTER TABLE '.$qryAllFormDetail[0]['ezf_table'].' ADD COLUMN `'.$fieldName.'` '.$dataType.' NULL;';
                try{
                    Yii::$app->db->createCommand($strSqlAlter)->execute();
                }catch(\yii\db\Exception $e){

                }
            }
        }

        $strSqlTbdata_Init = 'SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME = "tbdata_init" AND table_schema = "'.explode('=', getenv('DB_DSN'))['3'].'"';
        $qryAllTbdata_Init = Yii::$app->db->createCommand($strSqlTbdata_Init)->queryAll();
        foreach($qryAllTbdata_Init as $fieldKey => $fieldValue ){
            if(!in_array($fieldValue['COLUMN_NAME'], $allFieldSchema)){
                $dataType = $fieldValue['COLUMN_TYPE'];
                $fieldName = $fieldValue['COLUMN_NAME'];
                $strSqlAlter = 'ALTER TABLE '.$qryAllFormDetail[0]['ezf_table'].' ADD COLUMN `'.$fieldName.'` '.$dataType.' NULL;';
                try{
                    Yii::$app->db->createCommand($strSqlAlter)->execute();
                }catch(\yii\db\Exception $e){

                }
            }
            array_push($allFieldSchema, $fieldValue['COLUMN_NAME']);
        }


        Yii::$app->response->format = Response::FORMAT_JSON;
        $message = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Fix table success!</strong> ' . Yii::t('app', ''),
        ];
        return $message;
    }

    public function checkDataType($input_id){
        $dataType = 'varchar(100)';
        if( $input_id == 1 ){ //    Textbox
            $dataType = 'varchar(100)';
        }else if( $input_id == 2 ){ //  Heading Text
            $dataType = '';
        }else if( $input_id == 3 ){ //  Paragraph Text
            $dataType = 'text';
        }else if( $input_id == 4 ){ //  Radio box (Multiple choice)
            $dataType = 'varchar(10)';
        }else if( $input_id == 6 ){ //	Dropdown List (Choose from list)
            $dataType = 'varchar(10)';
        }else if( $input_id == 7 ){ //	Date
            $dataType = 'DATE';
        }else if( $input_id == 8 ){ //	Time
            $dataType = 'varchar(20)';
        }else if( $input_id == 9 ){ //	Date Time
            $dataType = 'DATETIME';
        }else if( $input_id == 10 ){ // Component
        }else if( $input_id == 11 ){ //	SNOMED-CT
        }else if( $input_id == 12 ){ //	Personal ID
            $dataType = 'varchar(20)';
        }else if( $input_id == 13 ){ //	Province Amphur Tumbon
            $dataType = 'varchar(50)';
        }else if( $input_id == 14 ){ //	File Upload
        }else if( $input_id == 15 ){ //	Question Text
            $dataType = '';
        }else if( $input_id == 16 ){ //	Single checkbox (Boolean or Toggle)
            $dataType = 'int';
        }else if( $input_id == 17 ){ //	Hospital list
        }else if( $input_id == 18 ){ //	Reference field
        }else if( $input_id == 31 ){ //	TDC field
        }else if( $input_id == 19 ){ //	Checkbox (Multiple-item response)
            $dataType = 'int';
        }else if( $input_id == 23 ){ //	Scale
            $dataType = '';
        }else if( $input_id == 24 ){ //	Drawing
        }else if( $input_id == 25 ){ //	Grid
            $dataType = '';
        }else if( $input_id == 27 ){ //icd9 list
        }else if( $input_id == 28 ){ //icd10 list
        }else if( $input_id == 231 ){ //	Scale
            $dataType = 'varchar(10)';
        }
        else if( $input_id == 251 ){ //	Grid
            $dataType = 'varchar(100)';
        }
        else if( $input_id == 252 ){ //	Grid
            $dataType = 'TEXT';
        }
        else if( $input_id == 253 ){ //	Grid
            $dataType = 'DATE';
        }
        else if( $input_id == 253 ){ //	Grid
            $dataType = 'DATE';
        }
        return $dataType;
    }

    public function actionFormadd($id)
    {
        $model2 = new EzformFields();

        if (Yii::$app->request->isAjax && $model2->load($_POST)) {
            Yii::$app->response->format = "json";
            return ActiveForm::validate($model2);
        }
        $model1 = $this->findModel($id);
        $model2->ezf_id = $model1->ezf_id;
        $numId = EzformFields::find()->where('ezf_id = :ezf_id', [':ezf_id' => $id])->andWhere('ezf_field_sub_id is NULL')->count();
        $numId + 1;
        return $this->renderAjax('_addpanel', ['model2' => $model2, 'numId' => 'var' . $numId]);
    }

    public function actionFormupdateorder()
    {
        $position = $_POST['positionArray'];
        print_r($position);
        $i = 0;
        foreach ($position as $p) {
            $sql = "UPDATE `ezform_fields` SET `ezf_field_order`='" . $i . "' WHERE `ezf_field_id`='" . $p . "' ";
            Yii::$app->db->createCommand($sql)->query();
            $i++;

        }
    }

    public function actionRenderfield($idtype, $labelvalue, $nameValue)
    {
        if ($idtype == "1") {
            return $this->renderAjax("exfield/text.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "2") {
            return $this->renderAjax("exfield/heading.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "3") {
            $modelField = EzformFields::find()->select('ezf_field_text_html, ezf_field_rows')->where(['ezf_field_id'=>$_GET['ezf_field_id']])->one();
            return $this->renderAjax("exfield/textarea.php", ['nameValue' => $nameValue, 'modelField' => $modelField]);
        } else if ($idtype == "4") {
            if (isset($_GET['ezf_field_id'])) {
                $model2 = $this->findFieldModel($_GET['ezf_field_id']);
                $modelradio = EzformChoice::find()
                    ->where('ezf_field_id = :ezf_field_id', ['ezf_field_id' => $model2->ezf_field_id])
                    ->andWhere('ezf_choiceetc is NULL');

                if ($modelradio->count()) {
                    $modelradio = $modelradio->all();
                    return $this->renderAjax("exfield/radio.php", ['nameValue' => $nameValue, 'modelform' => $model2, 'modelradio' => $modelradio,]);
                } else {
                    return $this->renderAjax("exfield/radio.php", ['nameValue' => $nameValue,]);
                }

            } else {
                return $this->renderAjax("exfield/radio.php", ['nameValue' => $nameValue,]);
            }
        } else if ($idtype == "19") {
            return $this->renderAjax("exfield/checkbox.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "6") {
            if (isset($_GET['ezf_field_id'])) {
                $model2 = $this->findFieldModel($_GET['ezf_field_id']);
                $modelselect = EzformChoice::find()
                    ->where('ezf_field_id = :ezf_field_id', ['ezf_field_id' => $model2->ezf_field_id])
                    ->andWhere('ezf_choiceetc is NULL');

                if ($modelselect->count()) {
                    $modelselect = $modelselect->all();
                    return $this->renderAjax("exfield/select.php", ['nameValue' => $nameValue, 'modelform' => $model2, 'modelselect' => $modelselect]);
                } else {
                    return $this->renderAjax("exfield/select.php", ['nameValue' => $nameValue,]);
                }
            } else {
                return $this->renderAjax("exfield/select.php", ['nameValue' => $nameValue,]);
            }
        } else if ($idtype == "7") {
            return $this->renderAjax("exfield/date.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "8") {
            return $this->renderAjax("exfield/time.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "9") {
            return $this->renderAjax("exfield/datetime.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "10") {
            $ezformComponents = EzformComponent::find()->all();
            $ezformComponent_maps = ArrayHelper::map($ezformComponents, 'comp_id', 'comp_name');
            return $this->renderAjax("exfield/component.php", [
                'nameValue' => $nameValue,
                'ezformComponent_maps' => $ezformComponent_maps
            ]);
        } else if ($idtype == "12") {
            return $this->renderAjax("exfield/personal.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "11") {
            return $this->renderAjax("exfield/snomed.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "13") {
            return $this->renderAjax("exfield/province.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "14") {
            return $this->renderAjax("exfield/fileinput.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "16") {
            return $this->renderAjax("exfield/scheckbox.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "15") {
            return $this->renderAjax("exfield/question.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "17") {
            return $this->renderAjax("exfield/hospital.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "18") {
            //Ez-Form ของตัวเอง
            $table1 = Yii::$app->db->createCommand("SELECT ezf_id, ezf_name, 'Ez-Form ของตัวเอง' as class FROM ezform WHERE status <> 3 AND user_create='" . Yii::$app->user->id . "';")->queryAll();
            //Ez-Form ที่เป็นผู้สร้างร่วม
            $table2 = Yii::$app->db->createCommand("SELECT a.ezf_id, a.ezf_name, 'Ez-Form ที่เป็นผู้สร้างร่วม' as class FROM ezform as a INNER JOIN ezform_co_dev as b ON a.ezf_id = b.ezf_id WHERE a.status <> 3 AND b.user_co = '" . Yii::$app->user->id . "';")->queryAll();
            //Ez-Form ที่มีเป้าหมายเดียวกัน
            if (Yii::$app->request->post('ezf_id')) {
                $ezf_id = Yii::$app->request->post('ezf_id');
                $model = $this->findModel($ezf_id);
                $table3 = Yii::$app->db->createCommand("SELECT ezf_id, ezf_name, 'Ez-Form ที่มีเป้าหมายเดียวกัน' as class FROM ezform WHERE status <> 3 AND comp_id_target='" . $model->comp_id_target . "' AND ezf_id<>'" . $ezf_id . "' AND comp_id_target IS NOT NULL;")->queryAll();
            } else {
                $table3 = [];
            }
            return $this->renderAjax("exfield/reffield.php",
                [
                    'nameValue' => $nameValue,
                    'ezf_id' => $ezf_id,
                    'table1' => $table1,
                    'table2' => $table2,
                    'table3' => $table3,
                ]);
        } else if ($idtype == "23") {
            return $this->renderAjax("exfield/scale.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "24") {
            return $this->renderAjax("exfield/drawing.php", ['nameValue' => $nameValue]);
        }else if ($idtype == "30") {
            return $this->renderAjax("exfield/audiorec.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "25") {
            return $this->renderAjax("exfield/grid.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "26") {
            return $this->renderAjax("exfield/map.php", ['nameValue' => $nameValue]);
        }
        else if ($idtype == "27") {
            return $this->renderAjax("exfield/icd9.php", ['nameValue' => $nameValue]);
        }
        else if ($idtype == "28") {
            return $this->renderAjax("exfield/icd10.php", ['nameValue' => $nameValue]);
        } else if ($idtype == "31") {

            $table1 = Yii::$app->db->createCommand("SELECT ezf_id, ezf_name, 'Ez-Form TDC' as class FROM ezform WHERE ezf_id IN (select ezf_id from ezform_map_f43) ORDER BY ezf_name;")->queryAll();
            if (Yii::$app->request->post('ezf_id')) {
                $ezf_id = Yii::$app->request->post('ezf_id');
                $model = $this->findModel($ezf_id);
                $table3 = Yii::$app->db->createCommand("SELECT ezf_id, ezf_name, 'Ez-Form ที่มีเป้าหมายเดียวกัน' as class FROM ezform WHERE status <> 3 AND comp_id_target='" . $model->comp_id_target . "' AND ezf_id<>'" . $ezf_id . "' AND comp_id_target IS NOT NULL;")->queryAll();
            } else {
                $table3 = [];
            }


            return $this->renderAjax("exfield/ref43field.php",
                [
                    'nameValue' => $nameValue,
                    'ezf_id' => $ezf_id,
                    'table1' => $table1,
                ]);
        }
    }

    /**
     * Deletes an existing Ezform model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            if (3 == $model->status && $model->user_create == Yii::$app->user->id) {

                Yii::$app->db->createCommand("DELETE FROM `ezform_target` WHERE `data_id` = :ezf_id;", [':ezf_id'=>$id])->execute();
                Yii::$app->db->createCommand("DELETE FROM `ezform_data_relation` WHERE `target_ezf_data_id` = :ezf_id;", [':ezf_id'=>$id])->execute();
                Yii::$app->db->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :ezf_id;", [':ezf_id'=>$id])->execute();
                Yii::$app->db->createCommand("DELETE FROM `query_request` WHERE `data_id` = :ezf_id;", [':ezf_id'=>$id])->execute();
                Yii::$app->db->createCommand("DELETE FROM `".($model->ezf_table)."`;")->execute();

                $this->findModel($id)->delete();

                $result = [
                    'status' => 'success',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                    'data' => $id,
                ];
                return $result;
            } else if($model->user_create == Yii::$app->user->id) {
                Yii::$app->db->createCommand("UPDATE `".($model->ezf_table)."` SET rstat='3', update_date = now();")->execute();
                Yii::$app->db->createCommand("UPDATE `ezform_target` SET rstat='3' WHERE `ezf_id` = :ezf_id;", [':ezf_id'=>$id])->execute();
                Yii::$app->db->createCommand("DELETE FROM `ezform_reply` WHERE `ezf_id` = :ezf_id;", [':ezf_id'=>$id])->execute();
                Yii::$app->db->createCommand("DELETE FROM `query_request` WHERE `ezf_id` = :ezf_id;", [':ezf_id'=>$id])->execute();
                $model->status = 3;
            }

            if ($model->save()) {
                $result = [
                    'status' => 'success',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                    'data' => $id,
                ];
                return $result;
            } else {
                $result = [
                    'status' => 'error',
                    'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not delete the data.'),
                    'data' => $id,
                ];
                return $result;
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Finds the Ezform model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ezform the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ezform::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUndo($id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);

            $model->status = 1;

            if ($model->save()) {
                $result = [
                    'status' => 'success',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                    'data' => $id,
                ];
                return $result;
            } else {
                $result = [
                    'status' => 'error',
                    'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not delete the data.'),
                    'data' => $id,
                ];
                return $result;
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionUpdateComponentTarget()
    {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $ezf_id = Yii::$app->request->post('form_id');
            $comp_id = Yii::$app->request->post('comp_id');

            $model = $this->findModel($ezf_id);

            $model->comp_id_target = $comp_id;
            $_SESSION['showsetting']=1;
            if ($model->save()) {
                $result = [
                    'status' => 'success',
                    'action' => 'update',
                    'message' => '<strong><i class="fa fa-check"></i> Success!</strong> ' . Yii::t('app', 'Updated completed.'),
                ];

                return $result;
            } else {
                $result = [
                    'status' => 'error',
                    'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not update the data.'),
                ];

                return $result;
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionUpdateCategory()
    {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $ezf_id = Yii::$app->request->post('form_id');
            $category_id = Yii::$app->request->post('category_id');

            $model = $this->findModel($ezf_id);

            $model->category_id = $category_id;

            if ($model->save()) {
                $result = [
                    'status' => 'success',
                    'action' => 'update',
                    'message' => '<strong><i class="fa fa-check"></i> Success!</strong> ' . Yii::t('app', 'Updated completed.'),
                ];

                return $result;
            } else {
                $result = [
                    'status' => 'error',
                    'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not update the data.'),
                ];

                return $result;
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionSaveCoDev()
    {
        if (Yii::$app->getRequest()->isAjax) {
            $ezf_id = Yii::$app->request->post('ezf_id');
            $fields = Yii::$app->request->post('val');

            $co_dev_lists = explode(",", $fields);

            $sql = "DELETE FROM ezform_co_dev WHERE ezf_id = '" . $ezf_id . "';";
            Yii::$app->db->createCommand($sql)->query();

            //delete for query data manager
            $sql = "DELETE FROM query_manager WHERE ezf_id = '" . $ezf_id . "' AND status = '2' AND user_group = '1';";
            Yii::$app->db->createCommand($sql)->query();

            foreach ($co_dev_lists as $value) {
                $sql = "INSERT INTO ezform_co_dev (ezf_id, user_co, status) VALUES ('" . $ezf_id . "', '" . $value . "', 0);";
                Yii::$app->db->createCommand($sql)->query();

                //
                $sql = "INSERT INTO query_manager (ezf_id, user_id, status, user_group) VALUES ('" . $ezf_id . "', '" . $value . "', 2, 1);";
                Yii::$app->db->createCommand($sql)->query();
            }
            //
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'บันทึกรายละเอียดแล้ว'),
            ];
            return $result;
        }
    }

    public function actionFindComponent(){
        $data = Yii::$app->db->createCommand(base64_decode($_POST['q_str'])." LIKE '%".$_POST['q']."%' LIMIT 0,50")->queryAll();
        $out = ['results' => ['id' => '', 'text' => '']];
        $out['results'] = array_values($data);
        return json_encode($out);
    }

    public function saveDataLog($ezf_json_new, $ezf_json_old, $ezf_id, $dataid, $ptid, $rstat){
        //save data log
        //
        //$arrDiffOld = [];
        //$arrDiffNew = [];
        $arrInit = [
            'id',
            'xsourcex',
            'rstat',
            'sitecode',
            'ptid',
            'ptcode',
            'ptcodefull',
            'hsitecode',
            'hptcode',
            'user_create',
            'create_date',
            'user_update',
            'update_date',
            'target',
            'error'
        ];
        $ezf_val_new = json_decode($ezf_json_new, true);
        $ezf_val_old = json_decode($ezf_json_old, true);
        $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
        $user = Yii::$app->user->id;
        //
        $arrData = array();
        foreach ($ezf_val_new as $key => $val) {
            if (in_array($key, $arrInit))
                continue;
            else if (is_array($val))
                continue;
            //กรณีมีการเปลี่ยน แปลงค่า
            if ($ezf_val_old[$key] != $val) {

                //for check box
                $EzformFields = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_type, ezf_field_id, ezf_field_name, ezf_field_label, ezf_field_sub_id')->where('ezf_id = :ezf_id AND ezf_field_name = :ezf_field_name', [':ezf_id' => $ezf_id, ':ezf_field_name' => $key])->one();
                if($EzformFields->ezf_field_type == 16 || $EzformFields->ezf_field_type == 19 || ($EzformFields->ezf_field_type == 0 && $EzformFields->ezf_field_sub_id)){
                    $ezf_val_old[$key] = $ezf_val_old[$key]+0;
                    $val = $val+0;
                    if($ezf_val_old[$key] != $val){
                        $arrData[$key]['dataid'] = $dataid;
                        $arrData[$key]['ptid'] = $ptid;
                        $arrData[$key]['rstat'] = $rstat;
                        $arrData[$key]['ezf_id'] = $ezf_id;
                        $arrData[$key]['ezf_field_name'] = $key;
                        $arrData[$key]['xsourcex'] = $xsourcex;
                        $arrData[$key]['user_create'] = $user;
                        $arrData[$key]['create_date'] = date('Y-m-d H:i:s');
                        //แสดงค่าใหม่
                        //$arrDiffNew[$key] = $val;
                        $arrData[$key]['change'] = $val;

                        //แสดงค่าเก่า
                        //$arrDiffOld[$key] = $ezf_val_old[$key];
                        $arrData[$key]['old'] = $ezf_val_old[$key];
                    }else{
                        continue;
                    }
                }else {
                    $arrData[$key]['dataid'] = $dataid;
                    $arrData[$key]['ptid'] = $ptid;
                    $arrData[$key]['rstat'] = $rstat;
                    $arrData[$key]['ezf_id'] = $ezf_id;
                    $arrData[$key]['ezf_field_name'] = $key;
                    $arrData[$key]['xsourcex'] = $xsourcex;
                    $arrData[$key]['user_create'] = $user;
                    $arrData[$key]['create_date'] = date('Y-m-d H:i:s');
                    //แสดงค่าใหม่
                    //$arrDiffNew[$key] = $val;
                    $arrData[$key]['change'] = $val;

                    //แสดงค่าเก่า
                    //$arrDiffOld[$key] = $ezf_val_old[$key];
                    $arrData[$key]['old'] = $ezf_val_old[$key];
                }
            }
        }

        if(count($arrData))
            Yii::$app->db->createCommand()->batchInsert('ezform_data_log', ['dataid', 'ptid', 'rstat', 'ezf_id', 'ezf_field_name', 'xsourcex', 'user_create', 'create_date', 'change', 'old'], $arrData)->execute();
        return ;
    }

    public static function MailNotify($data, $emailConsult, $template){

        //มีเมลเท่านั้นถึงจะส่ง
        if(count($emailConsult)) {

            try {
                $mailer = Yii::$app->mailer->compose($template, [
                    'data' => $data,
                ])
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')])
                    ->setTo($emailConsult)
                    ->setSubject($data['title']);
                $mailer->send();

                return true;
                //end send email
            }catch (\yii\base\Exception $ex){
                return false;
            }
        }

    }

    public function actionMailing($ezf_id=null, $dataid=null){
        //GeCiCCA Enrollment Report
        if($ezf_id == '1474424530033118700' && Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){

            //mailing enroll report
            $sql = "SELECT * FROM tbdata_1474424530033118700 WHERE `id`= :dataid AND mailing_enroll_report is null;";
            $model = Yii::$app->db->createCommand($sql, [':dataid' => $dataid])->queryOne();
            if($model['f1v46']=='1' && $model['confirm']=='1') {
                $sql = "SELECT `email` FROM mailing_list WHERE `group`='1';";
                $email = Yii::$app->db->createCommand($sql)->queryAll();

                $emailConsult = [];
                //กรณีส่งอีเมลจาก mail list
                if (count($email) > 0) {
                    //$users = \common\models\UserProfile::find()->select('email')->where('user_id in(' . $ezform['consultant_users'] . ') and (email is not null or LENGTH(email)>0)')->all();

                    foreach ($email as $val) {
                        $emailConsult[] = $val['email'];
                    }
                }

                $data = [];
                $data['random'] = $model['random'];
                $data['update_date'] = date_format(date_create($model['update_date']),"Y/m/d H:i:s");
                $data['create_date'] = date_format(date_create($model['create_date']),"Y/m/d H:i:s");
                $data['hptcode'] = $model['hptcode'];
                $data['f1v45_x'] = $model['f1v45_x'];
                $data['f1_initial'] = $model['f1_initial'];
                $data['f1v3'] = $model['f1v3'];
                $data['f1v5'] = $model['f1v5']=='1' ? 'Male' : 'Female';
                $data['chemo_drugs'] = $model['chemo_drugs']=='1' ? 'Gemcitabine' : 'Gemcitabine/Cisplatin';
                $users = \common\models\UserProfile::find()->select('firstname, lastname')->where('user_id in(' . ($model['user_create']) . ');')->one();
                $data['user_create'] = $users['firstname'] . ' ' . $users['lastname'];
                $data['sender'] = Yii::$app->keyStorage->get('sender');
                $data['title'] = 'GeCiCCA Enrollment Report : ' . $data['random'];

                if(self::MailNotify($data, $emailConsult, '@backend/mail/gecicca_report_enroll')){
                    //send success
                    Yii::$app->db->createCommand("UPDATE tbdata_1474424530033118700 SET mailing_enroll_report=1 WHERE id = :dataid", [':dataid'=>$dataid])->execute();
                }

            }

            //mailing enroll case
            $sql = "SELECT * FROM tbdata_1474424530033118700 WHERE `id`= :dataid AND mailing_enroll_case is null;";
            $model = Yii::$app->db->createCommand($sql, [':dataid' => $dataid])->queryOne();
            if($model['f1v46']=='1' && $model['confirm']=='1') {
                $sql = "SELECT `email` FROM mailing_list WHERE `group`='2';";
                $email = Yii::$app->db->createCommand($sql)->queryAll();

                $emailConsult = [];
                //กรณีส่งอีเมลจาก mail list
                if (count($email) > 0) {
                    //$users = \common\models\UserProfile::find()->select('email')->where('user_id in(' . $ezform['consultant_users'] . ') and (email is not null or LENGTH(email)>0)')->all();

                    foreach ($email as $val) {
                        $emailConsult[] = $val['email'];
                    }
                }

                $data = [];
                $data['random'] = $model['random'];
                $data['create_date'] = date_format(date_create($model['create_date']),"Y/m/d H:i:s");
                $data['hptcode'] = $model['hptcode'];
                $data['dataid'] = $dataid;
                $users = \common\models\UserProfile::find()->select('firstname, lastname')->where('user_id in(' . ($model['user_create']) . ');')->one();
                $data['user_create'] = $users['firstname'] . ' ' . $users['lastname'];
                $data['sender'] = Yii::$app->keyStorage->get('sender');
                $sql = "SELECT subjectID FROM tbdata_1474424530033118700_subjectID WHERE `dataID`= :dataid;";
                $modelSubjectID = Yii::$app->db->createCommand($sql, [':dataid' => $dataid])->queryOne();
                $nf = new NumberFormatter('en_US', NumberFormatter::ORDINAL);
                $data['num_ordinal'] = $nf->format($modelSubjectID['subjectID']+0);
                $data['title'] = 'Randomizing... The '.($data['num_ordinal']).' subject has been enrolled at '.date_format(date_create($model['create_date']),"Y/m/d H:i:s");

                if(self::MailNotify($data, $emailConsult, '@backend/mail/gecicca_report_enroll_case')){
                    //send success
                    Yii::$app->db->createCommand("UPDATE tbdata_1474424530033118700 SET mailing_enroll_case=1 WHERE id = :dataid", [':dataid'=>$dataid])->execute();
                }
            }

        }//end if
        return;
    }
    
        public function actionSavedata2()
    {

        if (isset($_POST["id"])) {
            $model_fields = EzformQuery::getFieldsByEzf_id($_POST["ezf_id"]);
            $model_form = EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = EzformQuery::getFormTableName($_POST["ezf_id"]);
            if(Yii::$app->request->post('txtSubmit')){
                //save submit
                $rstat = 2;
            }else{
                //save draft
                $rstat = 1;
            }

            if ($model_form->load(Yii::$app->request->post())) {
                $model_form->attributes = $_POST['SDDynamicModel'];

                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
                $modelOld = $model->find()->where('id = :id', ['id' => $_POST['id']])->One();

                //if click final save (rstat not change)
                if(Yii::$app->request->post('finalSave')){
                    $rstat = $modelOld->rstat;
                    $xsourcex = $modelOld->xsourcex;
                }else{
                    $xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
                }
                //VarDumper::dump($modelOld->attributes,10,true);
                //VarDumper::dump($model_form->attributes,10,true);
                $model->attributes = $model_form->attributes;
                $model->id = $_POST['id']; //ห้ามเอาออก ไม่งั้นจะ save ไม่ได้
                $model->rstat = $rstat;
                $model->xsourcex = $xsourcex;
                //$model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                //echo $rstat;

                if (Yii::$app->request->post('target') <> 'skip' AND Yii::$app->request->post('target') <> 'all') {
                    //จริงๆถ้าไม่มีการเปลี่ยนเป้าหมาย ให้ไปอัพเดจใน ezform_target ด้วย
                    $ezform = EzformQuery::checkIsTableComponent(Yii::$app->request->post('ezf_id'));
                    if ($ezform['special']) {
                        // something
                    } else {
                        $model->target = isset($_POST['target']) ? base64_decode($_POST['target']) : NULL;
                    }
                }

                foreach ($model_fields as $key => $value) {
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {
                        // set Data Sql Format
                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = new Expression('NULL');
                        }
                    }
                    else if ($value['ezf_field_type'] == 10) {
                        if (count($model->{$value['ezf_field_name']}) > 1)
                            $model->{$value['ezf_field_name']} = implode(',', $model->{$value['ezf_field_name']});
                    }
                    else if ($value['ezf_field_type'] == 14) {
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']] !='' && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
                            $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
                            $fileType = $_FILES['SDDynamicModel']['type'][$value['ezf_field_name']];
                            $newFileItems = [];
                            $action = false;
                            foreach ($fileItems as $i => $fileItem) {
                                if($fileItem!=''){
                                    $action = true;
                                    $fileArr = explode('/', $fileType[$i]);
                                    if (isset($fileArr[1])) {
                                        $fileBg = $fileArr[1];

                                        //$newFileName = $fileName;
                                        $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.$model->id.'_'.date("Ymd_His").(microtime(true)*10000) . '.' . $fileBg;
                                        //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                                        $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
                                        $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

                                        $file = UploadedFile::getInstanceByName($fieldname);
                                        $file->saveAs($fullPath);

					if($file){
					    $newFileItems[] = $newFileName;

					    //add file to db
					    $file_db = new \backend\modules\ezforms\models\FileUpload();
					    $file_db->tbid = $model->id;
					    $file_db->ezf_id = $value['ezf_id'];
					    $file_db->ezf_field_id = $value['ezf_field_id'];
					    $file_db->file_active = 0;
					    $file_db->file_name = $newFileName;
					    $file_db->file_name_old = $fileItem;
					    $file_db->target = ($model->ptid ? $model->ptid :  $model->target).'';
					    $file_db->save();
					}
                                    }
                                }
                            }
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            if($action){
                                $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

//				if($res){
//				    $res_items = explode(',', $res['filename']);
//
//				    foreach ($res_items as $dataTmp) {
//					@unlink(Yii::$app->basePath . '/../backend/web/fileinput/' . $dataTmp);
//				    }
//				}
                            } else {
                                if($res){
                                    $model->{$value['ezf_field_name']} = $res['filename'];
                                }
                            }

                        } else {
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            $model->{$value['ezf_field_name']} = $res['filename'];
                        }
                    }
                    else if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            //set data Drawing
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            $nameEdit = false;
                            $bgEdit = false;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $nameEdit = true;
                                $newFileName = date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $bgEdit = true;
                                $newFileBg = 'bg_' . date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                            $modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

                            if (isset($modelTmp['id'])) {
                                $fileArr = explode(',', $modelTmp[$value['ezf_field_name']]);
                                if (count($fileArr) > 1) {
                                    $fileName = $fileArr[0];
                                    $fileBg = $fileArr[1];
                                    if ($nameEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/data/' . $fileName);
                                    }
                                    if ($bgEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileBg);
                                    }
                                }
                            }
                        }
                    } else if ($value['ezf_field_type'] == 30) {
			$fileName = $model[$value['ezf_field_name']];
			$newFileName = $fileName;
                        $nameEdit = false;
			
			$foder = 'sddynamicmodel-'.$value['ezf_field_name'].'_'.Yii::$app->user->id;
			
			if (stristr($fileName, 'fileNameAuto') == TRUE) {
			    $nameEdit = true;
			    $newFileName = date("Ymd_His").(microtime(true)*10000) . '.mp3';
			    @copy(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName, Yii::$app->basePath . '/../storage/web/audio/' . $newFileName);
			    @unlink(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName);
			}
			
			$model[$value['ezf_field_name']] = $newFileName;
			
			$modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

			if (isset($modelTmp['id'])) {
			    
				$fileName = $modelTmp[$value['ezf_field_name']];
				//sddynamicmodel-
				if ($nameEdit) {
				    @unlink(Yii::$app->basePath . "/../storage/web/audio/" . $fileName);
				}
				
			    
			}
		    }
                }

                //
                $EzformReply = new EzformReply();
                $EzformReply->data_id = $_POST['id'];
                $EzformReply->ezf_id = $_POST['ezf_id'];
                $EzformReply->ezf_comment = 'บันทึกข้อมูล';
                $EzformReply->ezf_json_new = json_encode($model->attributes);
                $rst = Yii::$app->db->createCommand("select ezf_json_old from ezform_reply_temp where ezf_id=:ezf_id and data_id=:data_id;", [':ezf_id'=>$_POST['ezf_id'], ':data_id'=>$_POST['id']])->queryOne();
                $EzformReply->ezf_json_old = $rst['ezf_json_old'];
                $EzformReply->type= $modelOld->rstat == 0 ? 0 : 9 ;
                $EzformReply->create_date=new Expression('NOW()');
                $EzformReply->user_create=Yii::$app->user->id;
                $EzformReply->xsourcex = $model->xsourcex;
                $EzformReply->save();
                //end track change

                //send email to user key
                if(\backend\controllers\InputdataController::checkUserConsultant(Yii::$app->user->id, $_POST['ezf_id']) && $modelOld->user_create) {
                    \backend\controllers\InputdataController::consultNotify($EzformReply, $modelOld->user_create);
                }

                if ($model->update()) {
                    //save reverse
                    if(Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org'){
                        $res = Yii::$app->db->createCommand("SELECT * FROM ezform_sync WHERE ezf_local = :ezf_id", [':ezf_id'=>$_POST['ezf_id']])->queryOne();
                        if($res['ezf_local']){
                            //ดึงข้อมูลที่ Remote server
                            $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id'=>$res['ezf_remote']])->queryOne();
                            $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `".($table_name->ezf_table)."` WHERE  id = :id", [':id'=>$_POST['id']])->queryOne();

                            if($model['id']) {
                                //กรณีพบข้อมูลที่ Remote server
                                //ดึงข้อมูลใน local
                                $model = Yii::$app->db->createCommand("SELECT * FROM `".($table_name->ezf_table)."` WHERE  id = :id", [':id'=>$_POST['id']])->queryOne();

                                $id = $model['id'];
                                unset($model['id']);

                                Yii::$app->dbcascaputf8->createCommand()->update($ezform['ezf_table'], $model, ['id'=>$id])->execute();

                            }else{
                                //ดึงข้อมูลใน local
                                $model = Yii::$app->db->createCommand("SELECT * FROM `".($table_name->ezf_table)."` WHERE  id = :id", [':id'=>$_POST['id']])->queryOne();
                                $values = ''; $keys = '';
                                foreach ($model as $key=>$val){
                                   $values  .= "'{$val}', ";
                                   $keys  .= "`{$key}`, ";
                                }
                                $values = substr($values, 0, -2);
                                $keys = substr($keys, 0, -2);

                                Yii::$app->dbcascaputf8->createCommand("insert into `" . $ezform['ezf_table'] . "` (".$keys.") VALUES (".$values.");")->execute();
                            }

                            //save EMR
                            $model = Yii::$app->db->createCommand("SELECT * FROM `ezform_target` WHERE  data_id=:data_id AND ezf_id=:ezf_id;", [':data_id'=>$_POST['id'], ':ezf_id'=>$_POST['ezf_id']])->queryOne();
                            if($model['data_id'])
                                $values = ''; $keys = '';
                                foreach ($model as $key=>$val){
                                    $values  .= "'{$val}', ";
                                    $keys  .= "`{$key}`, ";
                                }
                                $values = substr($values, 0, -2);
                                $keys = substr($keys, 0, -2);

                                Yii::$app->dbcascaputf8->createCommand("replace into `ezform_target` (".$keys.") VALUES (".$values.");")->execute();
                        }
                    }
                    //save EMR
                    $modelx = EzformTarget::find()->where('data_id = :data_id AND ezf_id = :ezf_id ', [':data_id' => $_POST["id"], ':ezf_id' => $_POST["ezf_id"]])->one();
                    if($modelx->data_id) {
                        $modelx->user_update = Yii::$app->user->id;
                        $modelx->rstat = $rstat;
                        $modelx->update_date = new Expression('NOW()');
                        $modelx->save();
                    }
                    else{
                        //save EMR
                        $modelx = new EzformTarget();
                        $modelx->ezf_id = $_POST['ezf_id'];
                        $modelx->data_id = $model->id;
                        isset($_POST['target']) ? $modelx->target_id = base64_decode($_POST['target']) : NULL;
                        isset($_POST['comp_id_target']) ? $modelx->comp_id = $_POST['comp_id_target'] : NULL;
                        $modelx->user_create = $model->user_create;
                        $modelx->create_date = $model->create_date;
                        $modelx->user_update = Yii::$app->user->id;
                        $modelx->update_date = new Expression('NOW()');
                        $modelx->rstat = $model->rstat;
                        $modelx->xsourcex = $model->xsourcex;
                        $modelx->save();
                    }
                    //save sql log
                    $sqlLog = new EzformSqlLog();
                    $sqlLog->data_id = $model->id;
                    $sqlLog->ezf_id = $_POST['ezf_id'];
                    $sqlLog->sql_log =  (Yii::$app->db->createCommand()->update($table_name->ezf_table, $model->attributes, ['id'=>$_POST['id']])->rawSql);
                    $sqlLog->user_id = Yii::$app->user->id;
                    $sqlLog->create_date = new Expression('NOW()');
                    $sqlLog->rstat = $model->rstat;
                    $sqlLog->xsourcex = $model->xsourcex;
                    $sqlLog->save();
                    //save track change
                    self::saveDataLog($EzformReply->ezf_json_new, $EzformReply->ezf_json_old, $_POST['ezf_id'], $model->id, $modelx->target_id, $modelx->rstat);

                    //mailing
                    self::actionMailing($_POST['ezf_id'], $model->id);

                    //update register for cascap
                    if($modelx->ezf_id == '1437377239070461301' && Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){
                        //update cca-01
                        $table_name = EzformQuery::getFormTableName('1437377239070461302');
                        try {
                            if($modelx->target_id)
                            Yii::$app->db->createCommand()->update($table_name->ezf_table, ['f1v2' => $model->v2, 'f1v3' => $model->v3], 'ptid=:ptid', [':ptid' => $modelx->target_id])->execute();
                        }catch (Exception $e){

                        }
                    }
                    //update cca01 for cascap
                    else if($modelx->ezf_id == '1437377239070461302' && Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){
                        //update register
                        $table_name = EzformQuery::getFormTableName('1437377239070461301');
                        try {
                            if($modelx->target_id)
                            Yii::$app->db->createCommand()->update($table_name->ezf_table, ['v2' => $model->f1v2, 'v3' => $model->f1v3], 'ptid=:ptid', [':ptid' => $modelx->target_id])->execute();
                        }catch (Exception $e){

                        }
                    }

                }

                Yii::$app->getSession()->setFlash('save_status', [
                    'body'=> '<span class="h3" style="text-align: center;"><b>บันทึกข้อมูลสำเร็จ!</b></span>',
                    'options'=>['class'=>'alert-success']
                ]);
                if (Yii::$app->request->post('url_redirect')) {
                    $url_redirect = base64_decode(Yii::$app->request->post('url_redirect'));
                    return $this->redirect($url_redirect, 302);
                }else {
                    return $this->redirect(Yii::$app->request->referrer, 302);
                }
            }
        } else {
            $model_fields = EzformQuery::getFieldsByEzf_id($_POST["ezf_id"]);
            $model_form = EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = EzformQuery::getFormTableName($_POST["ezf_id"]);
            if(Yii::$app->request->post('txtSaveDraft')){
                //save draft
                $rstat = 1;
            }else{
                //save submit
                $rstat = 2;
            }

            if ($model_form->load(Yii::$app->request->post())) {

                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);

                $model->attributes = $model_form->attributes;

                $dataid = GenMillisecTime::getMillisecTime();

                $model->id = $dataid;
                $model->rstat = $rstat;
                $model->user_create = Yii::$app->user->id;
                $model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                //$model->target = isset($_POST['target']) ? $_POST['target'] : NULL;
//                $value = "";
//                foreach($_POST["SDDynamicModel"] as $key=>$v1){
//                    if(is_array($v1)){
//                        $attribute = $key;
//                        foreach($v1 as $v2){
//                            $value .= $v2.",";
//                        }
//						$model->$attribute = $value;
//                    }
//                    continue;
//                }
                if (Yii::$app->request->post('target') <> 'skip' AND Yii::$app->request->post('target') <> 'all') {
                    $ezform = EzformQuery::checkIsTableComponent(Yii::$app->request->post('ezf_id'));
                    if ($ezform['special']) {
                        $taget = isset($_POST['target']) ? base64_decode($_POST['target']) : '1';
                        $ezform = EzformQuery::getTablenameFromComponent(Yii::$app->request->post('comp_id_target'));
                        $taget = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $taget);
                        $model->target = $taget['ptid_key'];
                    } else {
                        $model->target = isset($_POST['target']) ? base64_decode($_POST['target']) : NULL;
                    }
                }


                $model->xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
                //
                foreach ($model_fields as $key => $value) {

                    if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $newFileName = date("Ymd_His") . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $newFileBg = 'bg_' . date("Ymd_His") . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                        }
                    }
                    if ($value['ezf_field_type'] == 14) {
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
			    $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
			    $newFileItems = [];
			    foreach ($fileItems as $i => $fileItem) {
				$fileArr = explode('/', $fileType[$i]);
				if (isset($fileArr[1])) {
				    $fileBg = $fileArr[1];

				    $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.date("Ymd_His") . '.' . $fileBg;
				    //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
				    $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
				    $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

				    $file = UploadedFile::getInstanceByName($fieldname);
				    $file->saveAs($fullPath);

				    if($file){
					$newFileItems[] = $newFileName;

					//add file to db
					$file_db = new \backend\modules\ezforms\models\FileUpload();
					$file_db->tbid = $model->id;
					$file_db->ezf_id = (int)$value['ezf_id'];
					$file_db->ezf_field_id = (int)$value['ezf_field_id'];
					$file_db->file_active = 0;
					$file_db->file_name = $newFileName;
					$file_db->file_name_old = $fileItem;
					$file_db->target = isset($model->target)?$model->target:'';
					$file_db->save();
				    }
				}
			    }
                            $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

                        }

                    }
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {

                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = '2011-01-01';
                        }

                    }
                    if ($value['ezf_field_type'] == 18) {
                        $target = Yii::$app->request->post('target');
                        //EzformQuery::saveReferenceFields($ezf_field_ref_field, $ezf_field_ref_table, $target, $ezf_id, $ezf_field_id, $dataid);
                        EzformQuery::saveReferenceFields($value['ezf_field_ref_field'], $value['ezf_field_ref_table'], $target, $value['ezf_id'], $value['ezf_field_id'], $dataid);
                    }
                    if ($value['ezf_field_type'] == 31) {
                        $target = Yii::$app->request->post('target');
                        //EzformQuery::saveReferenceFields($ezf_field_ref_field, $ezf_field_ref_table, $target, $ezf_id, $ezf_field_id, $dataid);
                        EzformQuery::saveReference43Fields($value['ezf_field_ref_field'], $value['ezf_field_ref_table'], $target, $value['ezf_id'], $value['ezf_field_id'], $dataid);
                    }                    

                }
                //VarDumper::dump(Yii::$app->request->post(),10,true);
                if ($model->save()) {
                    //save EMR
                    $model = new EzformTarget();
                    $model->ezf_id = Yii::$app->request->post('ezf_id');
                    $model->data_id = $dataid;
                    isset($_POST['target']) ? $model->target_id = base64_decode($_POST['target']) : NULL;
                    $model->user_create = Yii::$app->user->id;
                    $model->create_date = new Expression('NOW()');
                    $model->user_update = Yii::$app->user->id;
                    $model->update_date = new Expression('NOW()');
                    $model->rstat = $rstat;
                    $model->xsourcex = Yii::$app->user->identity->userProfile->sitecode;
                    $model->save();
                    //
                    if (Yii::$app->request->post('url_redirect')) {
                        $url_redirect = base64_decode(Yii::$app->request->post('url_redirect')) . '&dataid=' . $dataid;
                        return $this->redirect($url_redirect, 302);
                    }
                }
            }
        }
    }

    public function actionSavedatam()
    {

        if (isset($_POST["id"])) {
            $model_fields = EzformQuery::getFieldsByEzf_id($_POST["ezf_id"]);
            $model_form = EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = EzformQuery::getFormTableName($_POST["ezf_id"]);
            if(Yii::$app->request->post('txtSubmit')){
                //save submit
                $rstat = 2;
            }else{
                //save draft
                $rstat = 1;
            }

            if ($model_form->load(Yii::$app->request->post())) {
                $model_form->attributes = $_POST['SDDynamicModel'];

                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
                $modelOld = $model->find()->where('id = :id', ['id' => $_POST['id']])->One();

                //if click final save (rstat not change)
                if(Yii::$app->request->post('finalSave')){
                    $rstat = $modelOld->rstat;
                    $xsourcex = $modelOld->xsourcex;
                }else{
                    $xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
                }
                //VarDumper::dump($modelOld->attributes,10,true);
                //VarDumper::dump($model_form->attributes,10,true);
                $model->attributes = $model_form->attributes;
                $model->id = $_POST['id']; //ห้ามเอาออก ไม่งั้นจะ save ไม่ได้
                $model->rstat = $rstat;
                $model->xsourcex = $xsourcex;
                $model->hsitecode = $xsourcex;
                $model->target = $_POST['target'];
                $model->ptid = $_POST['ptid'];
                $model->sitecode = $_POST['sitecode'];
                $model->hptcode = $_POST['hptcode'];


                //$model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                //echo $rstat;

                if (Yii::$app->request->post('target') <> 'skip' AND Yii::$app->request->post('target') <> 'all') {
                    //จริงๆถ้าไม่มีการเปลี่ยนเป้าหมาย ให้ไปอัพเดจใน ezform_target ด้วย
                    $ezform = EzformQuery::checkIsTableComponent(Yii::$app->request->post('ezf_id'));
                    if ($ezform['special']) {
                        // something
                    } else {
                        $model->target = isset($_POST['target']) ? base64_decode($_POST['target']) : NULL;
                    }
                }

                foreach ($model_fields as $key => $value) {
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {
                        // set Data Sql Format
                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = new Expression('NULL');
                        }
                    }
                    else if ($value['ezf_field_type'] == 10) {
                        if (count($model->{$value['ezf_field_name']}) > 1)
                            $model->{$value['ezf_field_name']} = implode(',', $model->{$value['ezf_field_name']});
                    }
                    else if ($value['ezf_field_type'] == 14) {
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']] !='' && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
                            $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
                            $fileType = $_FILES['SDDynamicModel']['type'][$value['ezf_field_name']];
                            $newFileItems = [];
                            $action = false;
                            foreach ($fileItems as $i => $fileItem) {
                                if($fileItem!=''){
                                    $action = true;
                                    $fileArr = explode('/', $fileType[$i]);
                                    if (isset($fileArr[1])) {
                                        $fileBg = $fileArr[1];

                                        //$newFileName = $fileName;
                                        $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.$model->id.'_'.date("Ymd_His").(microtime(true)*10000) . '.' . $fileBg;
                                        //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                                        $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
                                        $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

                                        $file = UploadedFile::getInstanceByName($fieldname);
                                        $file->saveAs($fullPath);

					if($file){
					    $newFileItems[] = $newFileName;

					    //add file to db
					    $file_db = new \backend\modules\ezforms\models\FileUpload();
					    $file_db->tbid = $model->id;
					    $file_db->ezf_id = $value['ezf_id'];
					    $file_db->ezf_field_id = $value['ezf_field_id'];
					    $file_db->file_active = 0;
					    $file_db->file_name = $newFileName;
					    $file_db->file_name_old = $fileItem;
					    $file_db->target = ($model->ptid ? $model->ptid :  $model->target).'';
					    $file_db->save();
					}
                                    }
                                }
                            }
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            if($action){
                                $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

//				if($res){
//				    $res_items = explode(',', $res['filename']);
//
//				    foreach ($res_items as $dataTmp) {
//					@unlink(Yii::$app->basePath . '/../backend/web/fileinput/' . $dataTmp);
//				    }
//				}
                            } else {
                                if($res){
                                    $model->{$value['ezf_field_name']} = $res['filename'];
                                }
                            }

                        } else {
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            $model->{$value['ezf_field_name']} = $res['filename'];
                        }
                    }
                    else if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            //set data Drawing
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            $nameEdit = false;
                            $bgEdit = false;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $nameEdit = true;
                                $newFileName = date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $bgEdit = true;
                                $newFileBg = 'bg_' . date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                            $modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

                            if (isset($modelTmp['id'])) {
                                $fileArr = explode(',', $modelTmp[$value['ezf_field_name']]);
                                if (count($fileArr) > 1) {
                                    $fileName = $fileArr[0];
                                    $fileBg = $fileArr[1];
                                    if ($nameEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/data/' . $fileName);
                                    }
                                    if ($bgEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileBg);
                                    }
                                }
                            }
                        }
                    } else if ($value['ezf_field_type'] == 30) {
			$fileName = $model[$value['ezf_field_name']];
			$newFileName = $fileName;
                        $nameEdit = false;
			
			$foder = 'sddynamicmodel-'.$value['ezf_field_name'].'_'.Yii::$app->user->id;
			
			if (stristr($fileName, 'fileNameAuto') == TRUE) {
			    $nameEdit = true;
			    $newFileName = date("Ymd_His").(microtime(true)*10000) . '.mp3';
			    @copy(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName, Yii::$app->basePath . '/../storage/web/audio/' . $newFileName);
			    @unlink(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName);
			}
			
			$model[$value['ezf_field_name']] = $newFileName;
			
			$modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

			if (isset($modelTmp['id'])) {
			    
				$fileName = $modelTmp[$value['ezf_field_name']];
				//sddynamicmodel-
				if ($nameEdit) {
				    @unlink(Yii::$app->basePath . "/../storage/web/audio/" . $fileName);
				}
				
			    
			}
		    }
                }

                //
                $EzformReply = new EzformReply();
                $EzformReply->data_id = $_POST['id'];
                $EzformReply->ezf_id = $_POST['ezf_id'];
                $EzformReply->ezf_comment = 'บันทึกข้อมูล';
                $EzformReply->ezf_json_new = json_encode($model->attributes);
                $rst = Yii::$app->db->createCommand("select ezf_json_old from ezform_reply_temp where ezf_id=:ezf_id and data_id=:data_id;", [':ezf_id'=>$_POST['ezf_id'], ':data_id'=>$_POST['id']])->queryOne();
                $EzformReply->ezf_json_old = $rst['ezf_json_old'];
                $EzformReply->type= $modelOld->rstat == 0 ? 0 : 9 ;
                $EzformReply->create_date=new Expression('NOW()');
                $EzformReply->user_create=Yii::$app->user->id;
                $EzformReply->xsourcex = $model->xsourcex;
                $EzformReply->save();
                //end track change

                //send email to user key
                if(\backend\controllers\InputdataController::checkUserConsultant(Yii::$app->user->id, $_POST['ezf_id']) && $modelOld->user_create) {
                    \backend\controllers\InputdataController::consultNotify($EzformReply, $modelOld->user_create);
                }

                if ($model->update()) {
                    //save reverse
                    if(Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org'){
                        $res = Yii::$app->db->createCommand("SELECT * FROM ezform_sync WHERE ezf_local = :ezf_id", [':ezf_id'=>$_POST['ezf_id']])->queryOne();
                        if($res['ezf_local']){
                            //ดึงข้อมูลที่ Remote server
                            $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id'=>$res['ezf_remote']])->queryOne();
                            $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `".($table_name->ezf_table)."` WHERE  id = :id", [':id'=>$_POST['id']])->queryOne();

                            if($model['id']) {
                                //กรณีพบข้อมูลที่ Remote server
                                //ดึงข้อมูลใน local
                                $model = Yii::$app->db->createCommand("SELECT * FROM `".($table_name->ezf_table)."` WHERE  id = :id", [':id'=>$_POST['id']])->queryOne();

                                $id = $model['id'];
                                unset($model['id']);

                                Yii::$app->dbcascaputf8->createCommand()->update($ezform['ezf_table'], $model, ['id'=>$id])->execute();

                            }else{
                                //ดึงข้อมูลใน local
                                $model = Yii::$app->db->createCommand("SELECT * FROM `".($table_name->ezf_table)."` WHERE  id = :id", [':id'=>$_POST['id']])->queryOne();
                                $values = ''; $keys = '';
                                foreach ($model as $key=>$val){
                                   $values  .= "'{$val}', ";
                                   $keys  .= "`{$key}`, ";
                                }
                                $values = substr($values, 0, -2);
                                $keys = substr($keys, 0, -2);

                                Yii::$app->dbcascaputf8->createCommand("insert into `" . $ezform['ezf_table'] . "` (".$keys.") VALUES (".$values.");")->execute();
                            }

                            //save EMR
                            $model = Yii::$app->db->createCommand("SELECT * FROM `ezform_target` WHERE  data_id=:data_id AND ezf_id=:ezf_id;", [':data_id'=>$_POST['id'], ':ezf_id'=>$_POST['ezf_id']])->queryOne();
                            if($model['data_id'])
                                $values = ''; $keys = '';
                                foreach ($model as $key=>$val){
                                    $values  .= "'{$val}', ";
                                    $keys  .= "`{$key}`, ";
                                }
                                $values = substr($values, 0, -2);
                                $keys = substr($keys, 0, -2);

                                Yii::$app->dbcascaputf8->createCommand("replace into `ezform_target` (".$keys.") VALUES (".$values.");")->execute();
                        }
                    }
                    //save EMR
                    $modelx = EzformTarget::find()->where('data_id = :data_id AND ezf_id = :ezf_id ', [':data_id' => $_POST["id"], ':ezf_id' => $_POST["ezf_id"]])->one();
                    if($modelx->data_id) {
                        $modelx->user_update = Yii::$app->user->id;
                        $modelx->rstat = $rstat;
                        $modelx->update_date = new Expression('NOW()');
                        $modelx->save();
                    }
                    else{
                        //save EMR
                        $modelx = new EzformTarget();
                        $modelx->ezf_id = $_POST['ezf_id'];
                        $modelx->data_id = $model->id;
                        isset($_POST['target']) ? $modelx->target_id = base64_decode($_POST['target']) : NULL;
                        isset($_POST['comp_id_target']) ? $modelx->comp_id = $_POST['comp_id_target'] : NULL;
                        $modelx->user_create = $model->user_create;
                        $modelx->create_date = $model->create_date;
                        $modelx->user_update = Yii::$app->user->id;
                        $modelx->update_date = new Expression('NOW()');
                        $modelx->rstat = $model->rstat;
                        $modelx->xsourcex = $model->xsourcex;
                        $modelx->save();
                    }
                    //save sql log
                    $sqlLog = new EzformSqlLog();
                    $sqlLog->data_id = $model->id;
                    $sqlLog->ezf_id = $_POST['ezf_id'];
                    $sqlLog->sql_log =  (Yii::$app->db->createCommand()->update($table_name->ezf_table, $model->attributes, ['id'=>$_POST['id']])->rawSql);
                    $sqlLog->user_id = Yii::$app->user->id;
                    $sqlLog->create_date = new Expression('NOW()');
                    $sqlLog->rstat = $model->rstat;
                    $sqlLog->xsourcex = $model->xsourcex;
                    $sqlLog->save();
                    //save track change
                    self::saveDataLog($EzformReply->ezf_json_new, $EzformReply->ezf_json_old, $_POST['ezf_id'], $model->id, $modelx->target_id, $modelx->rstat);

                    //mailing
                    self::actionMailing($_POST['ezf_id'], $model->id);

                    //update register for cascap
                    if($modelx->ezf_id == '1437377239070461301' && Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){
                        //update cca-01
                        $table_name = EzformQuery::getFormTableName('1437377239070461302');
                        try {
                            if($modelx->target_id)
                            Yii::$app->db->createCommand()->update($table_name->ezf_table, ['f1v2' => $model->v2, 'f1v3' => $model->v3], 'ptid=:ptid', [':ptid' => $modelx->target_id])->execute();
                        }catch (Exception $e){

                        }
                    }
                    //update cca01 for cascap
                    else if($modelx->ezf_id == '1437377239070461302' && Yii::$app->keyStorage->get('frontend.domain')=='cascap.in.th'){
                        //update register
                        $table_name = EzformQuery::getFormTableName('1437377239070461301');
                        try {
                            if($modelx->target_id)
                            Yii::$app->db->createCommand()->update($table_name->ezf_table, ['v2' => $model->f1v2, 'v3' => $model->f1v3], 'ptid=:ptid', [':ptid' => $modelx->target_id])->execute();
                        }catch (Exception $e){

                        }
                    }

                }

                Yii::$app->getSession()->setFlash('save_status', [
                    'body'=> '<span class="h3" style="text-align: center;"><b>บันทึกข้อมูลสำเร็จ!</b></span>',
                    'options'=>['class'=>'alert-success']
                ]);
                if (Yii::$app->request->post('url_redirect')) {
                    $url_redirect = base64_decode(Yii::$app->request->post('url_redirect'));
                    return $this->redirect($url_redirect, 302);
                }else {
                    return $this->redirect(Yii::$app->request->referrer, 302);
                }
            }
        } else {
            $model_fields = EzformQuery::getFieldsByEzf_id($_POST["ezf_id"]);
            $model_form = EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = EzformQuery::getFormTableName($_POST["ezf_id"]);
            if(Yii::$app->request->post('txtSaveDraft')){
                //save draft
                $rstat = 1;
            }else{
                //save submit
                $rstat = 2;
            }

            if ($model_form->load(Yii::$app->request->post())) {

                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);

                $model->attributes = $model_form->attributes;

                $dataid = GenMillisecTime::getMillisecTime();

                $model->id = $dataid;
                $model->rstat = $rstat;
                $model->user_create = Yii::$app->user->id;
                $model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                //$model->target = isset($_POST['target']) ? $_POST['target'] : NULL;
//                $value = "";
//                foreach($_POST["SDDynamicModel"] as $key=>$v1){
//                    if(is_array($v1)){
//                        $attribute = $key;
//                        foreach($v1 as $v2){
//                            $value .= $v2.",";
//                        }
//						$model->$attribute = $value;
//                    }
//                    continue;
//                }
                if (Yii::$app->request->post('target') <> 'skip' AND Yii::$app->request->post('target') <> 'all') {
                    $ezform = EzformQuery::checkIsTableComponent(Yii::$app->request->post('ezf_id'));
                    if ($ezform['special']) {
                        $taget = isset($_POST['target']) ? base64_decode($_POST['target']) : '1';
                        $ezform = EzformQuery::getTablenameFromComponent(Yii::$app->request->post('comp_id_target'));
                        $taget = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $taget);
                        $model->target = $taget['ptid_key'];
                    } else {
                        $model->target = isset($_POST['target']) ? base64_decode($_POST['target']) : NULL;
                    }
                }


                $model->xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
                //
                foreach ($model_fields as $key => $value) {

                    if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $newFileName = date("Ymd_His") . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $newFileBg = 'bg_' . date("Ymd_His") . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                        }
                    }
                    if ($value['ezf_field_type'] == 14) {
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
			    $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
			    $newFileItems = [];
			    foreach ($fileItems as $i => $fileItem) {
				$fileArr = explode('/', $fileType[$i]);
				if (isset($fileArr[1])) {
				    $fileBg = $fileArr[1];

				    $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.date("Ymd_His") . '.' . $fileBg;
				    //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
				    $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
				    $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

				    $file = UploadedFile::getInstanceByName($fieldname);
				    $file->saveAs($fullPath);

				    if($file){
					$newFileItems[] = $newFileName;

					//add file to db
					$file_db = new \backend\modules\ezforms\models\FileUpload();
					$file_db->tbid = $model->id;
					$file_db->ezf_id = (int)$value['ezf_id'];
					$file_db->ezf_field_id = (int)$value['ezf_field_id'];
					$file_db->file_active = 0;
					$file_db->file_name = $newFileName;
					$file_db->file_name_old = $fileItem;
					$file_db->target = isset($model->target)?$model->target:'';
					$file_db->save();
				    }
				}
			    }
                            $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

                        }

                    }
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {

                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = '2011-01-01';
                        }

                    }
                    if ($value['ezf_field_type'] == 18) {
                        $target = Yii::$app->request->post('target');
                        //EzformQuery::saveReferenceFields($ezf_field_ref_field, $ezf_field_ref_table, $target, $ezf_id, $ezf_field_id, $dataid);
                        EzformQuery::saveReferenceFields($value['ezf_field_ref_field'], $value['ezf_field_ref_table'], $target, $value['ezf_id'], $value['ezf_field_id'], $dataid);
                    }
                    if ($value['ezf_field_type'] == 31) {
                        $target = Yii::$app->request->post('target');
                        //EzformQuery::saveReferenceFields($ezf_field_ref_field, $ezf_field_ref_table, $target, $ezf_id, $ezf_field_id, $dataid);
                        EzformQuery::saveReference43Fields($value['ezf_field_ref_field'], $value['ezf_field_ref_table'], $target, $value['ezf_id'], $value['ezf_field_id'], $dataid);
                    }                    

                }
                //VarDumper::dump(Yii::$app->request->post(),10,true);
                if ($model->save()) {
                    //save EMR
                    $model = new EzformTarget();
                    $model->ezf_id = Yii::$app->request->post('ezf_id');
                    $model->data_id = $dataid;
                    isset($_POST['target']) ? $model->target_id = base64_decode($_POST['target']) : NULL;
                    $model->user_create = Yii::$app->user->id;
                    $model->create_date = new Expression('NOW()');
                    $model->user_update = Yii::$app->user->id;
                    $model->update_date = new Expression('NOW()');
                    $model->rstat = $rstat;
                    $model->xsourcex = Yii::$app->user->identity->userProfile->sitecode;
                    $model->save();
                    //
                    if (Yii::$app->request->post('url_redirect')) {
                        $url_redirect = base64_decode(Yii::$app->request->post('url_redirect')) . '&dataid=' . $dataid;
                        return $this->redirect($url_redirect, 302);
                    }
                }
            }
        }
    }

    public function actionIcfList($recordid=null){
        if($recordid==null)return false;
        if (1||Yii::$app->getRequest()->isAjax) {
            //$recordid = base64_decode($recordid);

            $getIdSql = 'SELECT id,confirm FROM tb_data_1 WHERE ptid = (SELECT ptid FROM tb_data_1 WHERE id = "'.$recordid.'" LIMIT 1) AND id != "'.$recordid.'"';
            $getIdQry = Yii::$app->db->createCommand($getIdSql)->queryAll();

            if($getIdQry == null){
                return $this->renderAjax('_icf_list',[
                    'hasRow'=>false
                ]);
            }

            $fileSet = [];

            foreach($getIdQry as $key => $row){

                $fileUploadSql = 'SELECT file_name,file_name_old FROM file_upload WHERE tbid="'.$row['id'].'" ORDER BY fid DESC';
                $fileUploadQry = Yii::$app->db->createCommand($fileUploadSql)->queryAll();

                $arrWithConfirm = [
                    'confirm'=>$row['confirm']=='null'||intval($row['confirm'])==0?false:true,
                    'files'=>[]
                ];

                foreach($fileUploadQry as $keyFile => $singleFile){
                    array_push($arrWithConfirm['files'],[
                        'name'=>$singleFile['file_name'],
                        'old'=>$singleFile['file_name_old']
                    ]);
                }
                array_push($fileSet,$arrWithConfirm);
            }

            return $this->renderAjax('_icf_list',[
                'hasRow'=>true,
                'getIdQry'=>$getIdQry,
                'fileSet'=>$fileSet
            ]);
        }
    }

    public function actionGetZipCode($tamboncode=null){
        if($tamboncode==null)return false;
        if (1||Yii::$app->getRequest()->isAjax) {
            $getZipSql = 'SELECT zipcode FROM const_zipcodes WHERE district_code = "'.$tamboncode.'"';
            $getZipQry = Yii::$app->db->createCommand($getZipSql)->queryOne();
            return $getZipQry['zipcode'];
        }
    }
}
