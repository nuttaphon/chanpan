<?php
namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\SqlDataProvider;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\GenForm;
use common\lib\ezform\EzformQuery;
use yii\helpers\Json;

class MapController extends Controller{
    //put your code here
    public function actionInsertfield($forder){
        
        $modelfield = new EzformFields();
        $modelfield->ezf_field_options = json_encode($_POST['ezf_field_options']);

        if($modelfield->load(Yii::$app->request->post())){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $modelform = \backend\models\Ezform::find()->where(['ezf_id'=>$modelfield->ezf_id])->One();
            $data = array();
            $result = array();
            array_push($data,array($_POST["valuelat"],'1'));
            array_push($data,array($_POST["valuelng"],'2'));
            
            for($i=0;$i<count($data);$i++){
                $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                            . "WHERE TABLE_NAME = '".$modelform["ezf_table"]."' "
                            . "AND COLUMN_NAME = '".$data[$i][0]."'  ";
                $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                array_push($result, $num);
            }
            if(in_array("1",$result)=="1"){
                        $result = [
                            'status' => 'warning',
                            'action' => 'update',
                            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                        ];
                        return $result;
            }else{
                $modelfield->ezf_field_id = GenMillisecTime::getMillisecTime();
                $modelfield->ezf_field_name = 'SYSCHECK';
                $modelfield->ezf_field_head_label = 1;
                $modelfield->ezf_field_order = $forder;
                if($modelfield->save()){
                    $ezf_id = $modelfield->ezf_id;
                    $field_id = $modelfield->ezf_field_id;
                    foreach($data as $item){
                        $modelfield2 = new EzformFields;
                        $modelfield2->ezf_id = $ezf_id;
                        $modelfield2->ezf_field_id = GenMillisecTime::getMillisecTime();
                        $modelfield2->ezf_field_sub_id = $field_id;
                        $modelfield2->ezf_field_name = $item[0];
                        $modelfield2->ezf_field_label = $item[1];
                        $modelfield2->ezf_field_type = 21;
                        $modelfield2->save();
                        $sql = "ALTER TABLE `".$modelform["ezf_table"]."`"
                                         . "ADD COLUMN $modelfield2->ezf_field_name VARCHAR(100)";
                        Yii::$app->db->createCommand($sql)->execute();
                    }
                }
                return \backend\modules\ezforms\components\EzformFunc::saveInput($modelfield);
            }
        }
    }
    
    public function actionFormdelete($id){
        if (Yii::$app->getRequest()->isAjax) {
            $fieldid = $this->findFieldModel($id);
            $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$fieldid->ezf_id])->One();
            if($fieldid->delete()){
                $fieldSubItem = EzformFields::find()->where('ezf_field_sub_id = :subid',[':subid'=>$fieldid->ezf_field_id])->all();
                foreach($fieldSubItem as $SubItem){
                    $sql2 = "ALTER TABLE `".$modelform->ezf_table."` DROP $SubItem->ezf_field_name ";
                    Yii::$app->db->createCommand($sql2)->execute();
                }
                $deleteItem = EzformFields::deleteAll('ezf_field_sub_id = :subid ',[':subid'=>$fieldid->ezf_field_id]);
                Yii::$app->response->format = Response::FORMAT_JSON;
                $result = [
                    'status' => 'warning',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                    'data' => $id,
                ];
                return $result;
            }
        }
    }
    public function actionFormupdate($id){
            $model2 = $this->findFieldModel($id);
            $modelfield = EzformFields::find()->where('ezf_field_sub_id = :subid',[':subid'=>$id])->all();    
            $nameValue = '';
            return $this->renderAjax('/ezform/_editpanel',['model2'=>$model2,'modelfield'=>$modelfield,'nameValue'=>$nameValue]);
    }
    public function actionUpdatefield($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $field = $this->findFieldModel($id);
            //ezf_field_options
            $jsonOld = json_decode($field->ezf_field_options, true);
            if(count($jsonOld)) {
                foreach ($jsonOld as $key => $val) {
                    $jsonOld[$key] = $_POST['ezf_field_options'][$key];
                }
                $field->ezf_field_options = json_encode($jsonOld);
            }
            //
            $modelform = \backend\models\Ezform::find()->where(['ezf_id'=>$field->ezf_id])->One();
            $delete = $this->actionFormdelete($id);
            $data = array();
            $result = array();
            array_push($data,array($_POST["valuelat"],'1'));
            array_push($data,array($_POST["valuelng"],'2'));
            
	    $forder = $field->ezf_field_order;
	    
            for($i=0;$i<count($data);$i++){
                $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                            . "WHERE TABLE_NAME = '".$modelform["ezf_table"]."' "
                            . "AND COLUMN_NAME = '".$data[$i][0]."'  ";
                $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                array_push($result, $num);
            }
            if(in_array("1",$result)=="1"){
                $result = [
                            'status' => 'warning',
                            'action' => 'update',
                            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                        ];
                return $result;
            }else{
                $modelfield = new EzformFields;
                if($modelfield->load(Yii::$app->request->post())){
                    $modelfield->ezf_field_id = GenMillisecTime::getMillisecTime();
                    $modelfield->ezf_field_name = 'SYSCHECK';
		            $modelfield->ezf_field_head_label = 1;
		            $modelfield->ezf_field_order = $forder;
                    if($modelfield->save()){
                        $ezf_id = $modelfield->ezf_id;
                        $field_id = $modelfield->ezf_field_id;
                        foreach($data as $item){
                            $modelfield2 = new EzformFields;
                            $modelfield2->ezf_id = $ezf_id;
                            $modelfield2->ezf_field_id = GenMillisecTime::getMillisecTime();
                            $modelfield2->ezf_field_sub_id = $field_id;
                            $modelfield2->ezf_field_name = $item[0];
                            $modelfield2->ezf_field_label = $item[1];
                            $modelfield2->ezf_field_type = 21;
                            $modelfield2->save();
                            $sql = "ALTER TABLE `".$modelform["ezf_table"]."`"
                                             . "ADD COLUMN $modelfield2->ezf_field_name VARCHAR(100)";
                            Yii::$app->db->createCommand($sql)->execute();
                        }
                        $result = [
                                 'status' => 'success',
                                   ];
                        return $result;
                    }else{
                        echo 'error';
                    }
                }
            }
            
//                    $ezf_id = $modelfield->ezf_id;
//                    $modelfield->ezf_field_id = GenMillisecTime::getMillisecTime();
//                    $modelfield->ezf_field_province = $_POST["valueprovince"];
//                    $modelfield->ezf_field_amphur = $_POST["valueamphur"];
//                    $modelfield->ezf_field_tumbon = $_POST["valuetumbon"];
//                    $modelfield->ezf_field_lenght = $_POST["ezf_field_lenght"];
//                    $value = array();
//                    if($modelfield->save()){
//                        $value[0] = $modelfield->ezf_field_province;
//                        $value[1] = $modelfield->ezf_field_amphur;
//                        $value[2] = $modelfield->ezf_field_tumbon;
//                        foreach($value as $val){
//                                if($val!="0"){
//                                    $sqlAlter = "ALTER TABLE `tbdata_".$ezf_id."` "
//                                            . "ADD COLUMN ".$val." INT(11) ";
//                                    Yii::$app->db->createCommand($sqlAlter)->execute();
//                                }
//                            }
//                            Yii::$app->response->format = Response::FORMAT_JSON;
//                            $result = [
//                                    'status' => 'success',
//                                    'action' => 'update'
//                                ];
//                            return $result;
//                }
            }
        
    }
    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
