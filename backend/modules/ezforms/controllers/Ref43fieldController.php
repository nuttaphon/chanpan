<?php

namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\EzformChoice;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\lib\codeerror\helpers\GenMillisecTime;

class Ref43fieldController extends Controller {

    public function actionInsertfield($forder) {
        if (Yii::$app->getRequest()->isAjax) {
            $model2 = new EzformFields();

            $model2->ezf_field_order = $forder;
            $model2->ezf_field_id = GenMillisecTime::getMillisecTime();

            $model2->ezf_field_val = Yii::$app->request->post('ref_field_set');
            $model2->ezf_field_ref_table = Yii::$app->request->post('select_ezf');
            $model2->ezf_field_ref_field = Yii::$app->request->post('select_field');
//               \yii\helpers\VarDumper::dump($model2->attributes, 10, TRUE);
//            //echo Yii::$app->user->id;
//            Yii::$app->end();

            if ($model2->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                //1=แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)
                //2=ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง
                //3=แก้ไขเฉพาะตารางปลายทาง
                if(Yii::$app->request->post('ref_field_set')<=3) {
                    $model2->ezf_field_val = Yii::$app->request->post('ref_field_set');
                }

                //หา ref table name
                $ref_ezf_id = Yii::$app->request->post('select_ezf');
                $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ref_ezf_id';")->queryOne();
                $ref_ezf_table = $ezform['ezf_table'];

                //หา table name ตัวปัจจุบัน
                $ezf_id = Yii::$app->request->post('ezf_id');
                $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id';")->queryOne();
                $ezf_table = $ezform['ezf_table'];

                $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                    . "WHERE TABLE_NAME = '".$ezf_table."' "
                    . "AND COLUMN_NAME = '".$model2->ezf_field_name."'  ";
                $num = Yii::$app->db->createCommand($sqlColumn)->execute();
                if($num==1 && ($model2->ezf_field_name != 'hsitecode' && $model2->ezf_field_name != 'hptcode')) {
                    $result = [
                        'status' => 'warning',
                        'action' => 'update',
                        'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                    ];
                    return $result;
                } else {
                    //$sql = "ALTER TABLE `tbdata_" . $model2->ezf_id . "`"
                    //        . "ADD COLUMN $model2->ezf_field_name VARCHAR(255)";
                    //Yii::$app->db->createCommand($sql)->execute();
                    //return GenForm::saveSelect($model2);

                    //save ค่า ref field
                    $model2->save();

                    //ขั้นตอน Alter table
                    $ref_field = Yii::$app->request->post('select_field');
                    $model = $this->findFieldModel($ref_field);
                    //ถ้ามี condition
                    //check box
                    if($model->ezf_field_type == 5){
                        $ezf_choice = Yii::$app->db->createCommand("SELECT ezf_choicevalue FROM ezform_choice WHERE ezf_field_id='$ref_field';")->queryColumn();
                        //print_r($ezf_choice);
                        foreach ($ezf_choice as $val){
                            $val ='ref_'.$val;
                            Yii::$app->db->createCommand("ALTER TABLE `$ezf_table` ADD COLUMN $val VARCHAR(10)")->execute();
                        }
                    }
                    //province
                    else if($model->ezf_field_type == 13){
                        $ezf_choice = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_sub_id='$ref_field';")->queryColumn();
                        //print_r($ezf_choice);
                        foreach ($ezf_choice as $val){
                            $val ='ref_'.$val;
                            Yii::$app->db->createCommand("ALTER TABLE `$ezf_table` ADD COLUMN $val VARCHAR(50)")->execute();
                        }
                    }
                    //text
                    else if($model->ezf_field_type == 3){
                        Yii::$app->db->createCommand("ALTER TABLE `$ezf_table` ADD COLUMN ".$model2->ezf_field_name." TEXT")->execute();
                    }
                    else{
                        try {
                            Yii::$app->db->createCommand("ALTER TABLE `$ezf_table` ADD COLUMN " . $model2->ezf_field_name . " VARCHAR(255)")->execute();
                        }catch (Exception $e){

                        }
                    }

                    //$ezf_choice = Yii::$app->db->createCommand("SELECT * FROM ezform_fields WHERE ezf_field_id='$ref_field';")->queryOne();
                    //print_r($ezf_choice);
                    $model2 = EzformFields::find()->where(['ezf_field_id' => $model2->ezf_field_id])->one();
                    return \backend\modules\ezforms\components\EzformFunc::saveInput($model2);

                }
            }
        }
    }

    public function actionFormdelete($id) {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findFieldModel($id);
            $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$model->ezf_id])->One();
            $model->delete();
            $sql = "ALTER TABLE `" . $modelform->ezf_table . "`"
                    . "DROP $model->ezf_field_name ";
            Yii::$app->db->createCommand($sql)->execute();
            $result = [
                'status' => 'warning',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
            ];
            return $result;
        }
    }

    public function actionFormupdate($id) {

    }

    public function actionSelecttable() {
        $ezf_id = Yii::$app->request->post('ezf_id');
        $ezf_fields= Yii::$app->db->createCommand("SELECT ezf_field_id, CONCAT(ezf_field_name, ' : ', ezf_field_label) as ezf_field_name FROM ezform_fields WHERE (ezf_field_type <> 0 AND ezf_id='$ezf_id');")->queryAll();
        //print_r($ezf_fields);

        return $this->renderAjax("/ezform/exfield/ref43field_select.php",
            [
                'ezf_fields'=>$ezf_fields,
            ]);
    }

    public function actionUpdatefield($id, $ezf_id) {
        if (Yii::$app->getRequest()->isAjax) {

            $sql = "ALTER TABLE `tbdata_" . $modelfield->ezf_id . "`"
                        . "CHANGE COLUMN `$model->ezf_field_name` `$modelfield->ezf_field_name` VARCHAR(100)";
            Yii::$app->db->createCommand($sql)->execute();

        }
    }

    protected function findFieldModel($id) {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

?>
