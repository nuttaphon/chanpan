<?php

namespace backend\modules\ezforms\controllers;

use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\SqlDataProvider;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\GenForm;
use common\lib\ezform\EzformQuery;

class HeadingController extends Controller
{
    public function actionInsertfield($forder){
        if (Yii::$app->getRequest()->isAjax) {
	    $model2 = new EzformFields();
	    $model2->ezf_field_order = $forder;
	    $model2->ezf_field_id = GenMillisecTime::getMillisecTime();
	    $model2->ezf_field_head_label = 1;
	    $model2->ezf_field_options = json_encode($_POST['ezf_field_options']);
	    if ($model2->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		
		return  \backend\modules\ezforms\components\EzformFunc::saveInput($model2);
	    }
        }
    }
    public function actionFormdelete($id){
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $this->findFieldModel($id)->delete();
            $result = [
                'status' => 'warning',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
            ];
            return $result;
        }

    }
    public function actionFormupdate($id){
            $model2 = $this->findFieldModel($id);
	    
	    
            return $this->renderAjax('/ezform/_editpanel',['model2'=>$model2]);
            

    }
    public function actionUpdatefield($id){
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findFieldModel($id);
            //ezf_field_options
            $jsonOld = json_decode($model->ezf_field_options, true);
            foreach($jsonOld as $key => $val){
                $jsonOld[$key] = $_POST['ezf_field_options'][$key];
            }
            $model->ezf_field_options = json_encode($jsonOld);
            //
            $modelform = Ezform::find()
                    ->where(['ezf_id'=>$model->ezf_id])
                    ->one();
            if($model->load(Yii::$app->request->post())){
                
                Yii::$app->response->format = Response::FORMAT_JSON;
		
		
                $model->save();
                $result =   [
                                'status' => 'success',
                            ];
                return $result;
            }
        }
    }
    protected function findFieldModel($id)
    {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
?>
