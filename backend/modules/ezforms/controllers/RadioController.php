<?php

namespace backend\modules\ezforms\controllers;

use backend\modules\ezforms\components\EzformQuery;
use Yii;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformSearch;
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\EzformChoice;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\SqlDataProvider;
use common\models\User;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\components\GenForm;

class RadioController extends Controller {

    public function actionInsertfield($forder) {
        if (Yii::$app->getRequest()->isAjax) {
            $model2 = new EzformFields;
            //$modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$_POST['EzformFields']['ezf_id']])->One();
            $model2->ezf_field_order = $forder;
            $model2->ezf_field_options = json_encode($_POST['ezf_field_options']);
            $model2->ezf_field_id = GenMillisecTime::getMillisecTime();
            $ezf_tablename = EzformQuery::getFormTableName($_POST['EzformFields']['ezf_id']);

//            \yii\helpers\VarDumper::dump($_POST['radioVar'], 10, TRUE);
//            exit();

            if ($model2->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $exists = EzFormFields::find()->
                                where([ 'ezf_field_name' => $model2->ezf_field_name])
                                ->andWhere(['ezf_id' => $model2->ezf_id])->exists();
                if ($exists == 1) {
                    $result = [
                        'status' => 'warning',
                        'action' => 'update',
                        'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                    ];
                    return $result;
                } else {

                    $strlenght = count($_POST['radioVar']);
                    //check fields ซ้ำ
                    for ($i = 2; $i < $strlenght; $i+=3) {
                        $ezf_choicelabel = $_POST['radioVar'][$i];
                        if($ezf_choicelabel <> '') {
                            $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                                . "WHERE TABLE_NAME = '" . $ezf_tablename->ezf_table . "' "
                                . "AND COLUMN_NAME = '" . $ezf_choicelabel . "'  ";

                            $num = Yii::$app->db->createCommand($sqlColumn)->query()->count();
                            //VarDumper::dump($sqlColumn);
                            //Yii::$app->end();
                            if ($num) {
                                $result = [
                                    'status' => 'warning',
                                    'action' => 'update',
                                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ตัวแปร ('.$ezf_choicelabel.') นี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                                ];
                                return $result;
                                break;
                            }
                        }
                    }//end check
                    $model2->ezf_field_default = $_POST['ezf_field_default'];

                    if($model2->save()){
                        //$modelchoice = new EzformChoice;
                        //$modelchoice->ezf_choice_id = GenMillisecTime::getMillisecTime();

                        for ($i = 0; $i < $strlenght; $i++) {
                            $ezf_choice_id = GenMillisecTime::getMillisecTime();
                            $ezf_choicevalue = $_POST['radioVar'][$i];
                            $i++;
                            $ezf_choicelabel = $_POST['radioVar'][$i];

                            $sql = "INSERT INTO `ezform_choice` (`ezf_choice_id`, `ezf_field_id`, `ezf_choicevalue`, `ezf_choicelabel`) "
                                    . "VALUES ('$ezf_choice_id', '$model2->ezf_field_id', '$ezf_choicevalue', '$ezf_choicelabel');";
                            Yii::$app->db->createCommand($sql)->execute();

                            //etc text
                            $i++;
                            $ezf_choicelabel = $_POST['radioVar'][$i];
                            if($ezf_choicelabel <> ''){
                                //insert field etc
                                $ezf_fields_id_etc = GenMillisecTime::getMillisecTime();
                                $sql = "INSERT INTO `ezform_fields` (`ezf_field_id`, `ezf_id`, `ezf_field_name`, `ezf_field_label`,`ezf_field_type`, `ezf_field_ref`, `ezf_field_ref_field`) "
                                    . "VALUES ('$ezf_fields_id_etc', '$model2->ezf_id', '$ezf_choicelabel', 'ETC Text', '0', '$ezf_choice_id', '$model2->ezf_field_id');";
                                Yii::$app->db->createCommand($sql)->execute();

                                //add column table
                                $sql = "ALTER TABLE `" . $ezf_tablename->ezf_table . "`"
                                    . "ADD COLUMN $ezf_choicelabel VARCHAR(255)";
                                Yii::$app->db->createCommand($sql)->execute();
                            }// end

                        }
                        $sql = "ALTER TABLE `" . $ezf_tablename->ezf_table . "`"
                                . "ADD COLUMN $model2->ezf_field_name VARCHAR(10)";
                        Yii::$app->db->createCommand($sql)->execute();

                        $model2->ezf_field_default = $_POST['ezf_field_default'];

			            \backend\modules\ezforms\components\EzformFunc::saveCondition($_POST['conditionFields']);

                        return \backend\modules\ezforms\components\EzformFunc::saveInput($model2);

                    }


                }
            }
        }
    }

    public function actionFormdelete($id) {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model2 = $this->findFieldModel($id);

            $ezf_tablename = EzformQuery::getFormTableName($model2->ezf_id);
            \backend\modules\ezforms\components\EzformFunc::deleteCondition($model2);

            $model2->delete();
            $sql = "ALTER TABLE `" . $ezf_tablename->ezf_table . "`"
                    . " DROP $model2->ezf_field_name ";
            Yii::$app->db->createCommand($sql)->execute();

            //drop etc field
            $model_etc = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_ref_field='$id';")->queryColumn();
            foreach($model_etc as $key => $val ){
                $sql = "ALTER TABLE `" . $ezf_tablename->ezf_table . "`"
                    . " DROP $val ";
                Yii::$app->db->createCommand($sql)->execute();
            }
            Yii::$app->db->createCommand("DELETE FROM ezform_fields WHERE ezf_field_ref_field='$id';")->execute();
            //drop etc field end


            //remove other field
//            $sql = " SELECT ezf_choicevalue  FROM `ezform_choice` WHERE ezf_field_id='".($id)."' AND ezf_choiceetc='1' ORDER BY ezf_choice_id";
//            $valueField = \Yii::$app->db->createCommand($sql)->queryOne();
//
//
//                $sql = "ALTER TABLE `tbdata_" . $model2->ezf_id . "`"
//                    . " DROP ".$valueField['ezf_choicevalue'];
//                Yii::$app->db->createCommand($sql)->execute();


            $result = [
                'status' => 'warning',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
            ];
            return $result;
        }
    }

    public function actionFormupdate($id) {
        $model2 = $this->findFieldModel($id);
        $modelradio = EzformChoice::find()
                ->where('ezf_field_id = :ezf_field_id', ['ezf_field_id' => $model2->ezf_field_id])
                ->andWhere('ezf_choiceetc is NULL')
                ->all();

        $nameValue = $model2->ezf_field_name;
        return $this->renderAjax('/ezform/_editpanel', ['model2' => $model2,
                    'modelradio' => $modelradio,
                    'nameValue' => $nameValue]);
    }

    public function actionTransformfield($ezf_field_id, $ezf_id) {
//        $model = new EzformFields();
//        $model->ezf_field_id = $ezf_field_id;
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        return  GenForm::typeSelectOnload($model2);

        $ezform_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$ezf_field_id';")->queryOne();
        $ezf_field_name = $ezform_fields['ezf_field_name'];
        $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id';")->queryOne();
        $ezf_table = $ezform['ezf_table'];

        //\yii\helpers\VarDumper::dump($ezform_fields, 10, TRUE);
        //exit();

        try {
            $sql = "SELECT DISTINCT " . $ezf_field_name . " FROM `" . $ezf_table . "` WHERE LENGTH(" . $ezf_field_name . ")>0;";

            //return \yii\helpers\VarDumper::dump($ezform, 10, TRUE);
            if(Yii::$app->db->createCommand($sql)->query()->count()) {
                $ezform = Yii::$app->db->createCommand($sql)->queryColumn();
                return $this->renderAjax('/ezform/exfield/radio.php', ['modelComponent' => $ezform]);
            }else{
                return $this->renderAjax('/ezform/exfield/radio.php');
            }
        } catch (\yii\db\Exception $e) {
            return $this->renderAjax('/ezform/exfield/radio.php');
        }
    }

    public function actionUpdatefield($id, $ezf_id) {
        if (Yii::$app->getRequest()->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            //$strlenght = count($_POST['radioVar']);
            //check fields ซ้ำ
            /*
            for ($i = 2; $i < $strlenght; $i+=3) {
                $ezf_choicelabel = $_POST['radioVar'][$i];
                if($ezf_choicelabel <> '') {
                    $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id';")->queryOne();
                    $ezf_table = $ezform['ezf_table'];

                    $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                        . "WHERE TABLE_NAME = '" . $ezf_table . "' "
                        . "AND COLUMN_NAME = '" . $ezf_choicelabel . "'  ";

                    $num = Yii::$app->db->createCommand($sqlColumn)->query()->count();
                    //VarDumper::dump($sqlColumn);
                    //Yii::$app->end();
                    if ($num) {
                        $result = [
                            'status' => 'warning',
                            'action' => 'update',
                            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ตัวแปร ('.$ezf_choicelabel.') นี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                        ];
                        return $result;
                        break;
                    }
                }
            }//end check
            */

            if (isset($_POST['transformfield'])) {

                $ezform_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$id';")->queryOne();
                $ezf_field_name = $ezform_fields['ezf_field_name'];
                $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id';")->queryOne();
                $ezf_table = $ezform['ezf_table'];
                //$sql = "SELECT id, ".$ezf_field_name." FROM `".$ezf_table."`;";
                //$ezform_tb = Yii::$app->db->createCommand($sql)->queryAll();
                //$sql = "SELECT DISTINCT ".$ezf_field_name." FROM `".$ezf_table."` WHERE LENGTH(".$ezf_field_name.")>0;";
                //$ezform_tb = Yii::$app->db->createCommand($sql)->queryColumn();

                //กรณี หาก Var ของตัวเลือกมีการแก้ไข
                $model = $this->findFieldModel($id);
                $modelfield = $this->findFieldModel($id);


                if ($modelfield->load(Yii::$app->request->post())) {

                    //ezf_field_options
                    $jsonOld = json_decode($modelfield->ezf_field_options, true);
                    if(count($jsonOld)) {
                        foreach ($jsonOld as $key => $val) {
                            $jsonOld[$key] = $_POST['ezf_field_options'][$key];
                        }
                        $modelfield->ezf_field_options = json_encode($jsonOld);
                    }
                    //
                    $modelfield->ezf_field_default = $_POST['ezf_field_default'];
		    
                    if ($modelfield->save()) {

                            $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                                . "WHERE TABLE_NAME = '" . $ezf_table . "' "
                                . "AND COLUMN_NAME = '" . $modelfield->ezf_field_name . "'  ";

                            $num = Yii::$app->db->createCommand($sqlColumn)->query()->count();

                            if ($num) {
                                $sql = "ALTER TABLE `" . $ezf_table . "`"
                                    . "CHANGE COLUMN `$model->ezf_field_name` `$modelfield->ezf_field_name` VARCHAR(100)";
                                Yii::$app->db->createCommand($sql)->execute();
                            }//end check
                            else{
                                $sql = "ALTER TABLE `" . $ezf_table . "`"
                                    . "ADD COLUMN `$modelfield->ezf_field_name` VARCHAR(100)";

                                Yii::$app->db->createCommand($sql)->execute();
                            }
			    
                        \backend\modules\ezforms\components\EzformFunc::saveCondition($_POST['conditionFields']);

//                        $sql2 = "ALTER TABLE `".$ezf_table."`"
//                        . "ADD $modelfield->ezf_field_name VARCHAR(255)";
//                        Yii::$app->db->createCommand($sql2)->execute();
                    }
//                    if(Yii::$app->request->post('radioValueOther')){
//                        $sql = "ALTER TABLE `" . $ezf_table . "`"
//                            . "ADD COLUMN valother_".$_POST['radioValueOther']." VARCHAR(255)";
//                        Yii::$app->db->createCommand($sql)->execute();
//                    }
                }



                //\yii\helpers\VarDumper::dump($ezform_tb, 10);

                //select choice and delete
                $sql = "SELECT ezf_choice_id FROM ezform_choice WHERE ezf_field_id ='$id';";
                $res = Yii::$app->db->createCommand($sql)->queryAll();
                foreach($res AS $val){
                    Yii::$app->db->createCommand("DELETE FROM ezform_fields WHERE ezf_field_ref ='".$val['ezf_choice_id']."';")->execute();
                }
                $sql = "DELETE FROM ezform_choice WHERE ezf_field_id ='$id';";
                Yii::$app->db->createCommand($sql)->execute();
                //
                $strlenght = count($_POST['radioVar']);
                $indexorg = 0;
                for ($i = 0; $i < $strlenght; $i++) {
                    $ezf_choice_id = GenMillisecTime::getMillisecTime();
                    $ezf_choicevalue = $_POST['radioVar'][$i];

                    $ezf_orglabel = $_POST['orglabel'][$indexorg];
                    $indexorg++;
                    $i++;
                    $ezf_choicelabel = $_POST['radioVar'][$i];

                    $sql = "UPDATE `$ezf_table` SET `$ezf_field_name` = '$ezf_choicevalue' WHERE `$ezf_field_name`='$ezf_orglabel';";
                    Yii::$app->db->createCommand($sql)->execute();

                    $sql = "INSERT INTO `ezform_choice` (`ezf_choice_id`, `ezf_field_id`, `ezf_choicevalue`, `ezf_choicelabel`) "
                            . "VALUES ('$ezf_choice_id', '$modelfield->ezf_field_id', '$ezf_choicevalue', '$ezf_choicelabel');";
                    Yii::$app->db->createCommand($sql)->execute();

                    //etc text
                    $i++;
                    $ezf_choicelabel = $_POST['radioVar'][$i];
                    if($ezf_choicelabel <> ''){
                        //insert field etc
                        $ezf_fields_id_etc = GenMillisecTime::getMillisecTime();
                        $sql = "INSERT INTO `ezform_fields` (`ezf_field_id`, `ezf_id`, `ezf_field_name`, `ezf_field_label`, `ezf_field_ref`, `ezf_field_ref_field`) "
                            . "VALUES ('$ezf_fields_id_etc', '$modelfield->ezf_id', '$ezf_choicelabel', 'ETC Text', '$ezf_choice_id', '$modelfield->ezf_field_id');";
                        Yii::$app->db->createCommand($sql)->execute();

                        //add column table
                        $sql = "ALTER TABLE `" . $ezf_table . "`"
                            . "ADD COLUMN $ezf_choicelabel VARCHAR(255)";
                        Yii::$app->db->createCommand($sql)->execute();
                    }// end
                }
                //add other field
                /*
                if(Yii::$app->request->post('radioValueOther')){
                    $ezf_choice_id = GenMillisecTime::getMillisecTime();
                    $ezf_choicevalue = 'valother_'.$_POST['radioValueOther'];
                    $ezf_choicelabel = 'อื่นๆ';
                    $sql = "INSERT INTO `ezform_choice` (`ezf_choice_id`, `ezf_field_id`, `ezf_choicevalue`, `ezf_choicelabel`, `ezf_choiceetc`) "
                                . "VALUES ('$ezf_choice_id', '$modelfield->ezf_field_id', '$ezf_choicevalue', '$ezf_choicelabel', '1');";
                    Yii::$app->db->createCommand($sql)->execute();
                }
                 */
                $result = [
                            'status' => 'success',
                            'action' => 'update',
                        ];
                        return $result;
            } else {
                //select choice and delete
                $sql = "SELECT ezf_choice_id FROM ezform_choice WHERE ezf_field_id ='$id';";
                $res = Yii::$app->db->createCommand($sql)->queryAll();
                foreach($res AS $val){
                    Yii::$app->db->createCommand("DELETE FROM ezform_fields WHERE ezf_field_ref ='".$val['ezf_choice_id']."';")->execute();
                }

                $sql = "DELETE FROM ezform_choice WHERE ezf_field_id ='$id';";
                Yii::$app->db->createCommand($sql)->execute();

                $model = $this->findFieldModel($id);
                $modelfield = $this->findFieldModel($id);

                if ($modelfield->load(Yii::$app->request->post())) {

                    //ezf_field_options
                    $jsonOld = json_decode($modelfield->ezf_field_options, true);
                    if(count($jsonOld)) {
                        foreach ($jsonOld as $key => $val) {
                            $jsonOld[$key] = $_POST['ezf_field_options'][$key];
                        }
                        $modelfield->ezf_field_options = json_encode($jsonOld);
                    }
                    //
                    $modelfield->ezf_field_default = $_POST['ezf_field_default'];
		    
                    $ezf_tablename = EzformQuery::getFormTableName($modelfield->ezf_id);

                    $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                        . "WHERE TABLE_NAME = '" . $ezf_tablename->ezf_table . "' "
                        . "AND COLUMN_NAME = '" . $modelfield->ezf_field_name . "'  ";

                    $num = Yii::$app->db->createCommand($sqlColumn)->query()->count();

                    if ($num) {
                        $sql = "ALTER TABLE `" . $ezf_tablename->ezf_table . "`"
                            . "CHANGE COLUMN `$model->ezf_field_name` `$modelfield->ezf_field_name` VARCHAR(100)";
                        Yii::$app->db->createCommand($sql)->execute();
                    }//end check
                    else{
                        $sql = "ALTER TABLE `" . $ezf_tablename->ezf_table . "`"
                            . "ADD COLUMN `$modelfield->ezf_field_name` VARCHAR(100)";
                        Yii::$app->db->createCommand($sql)->execute();
                    }

                    $sqlUpdate = "UPDATE `ezform_fields` "
                            . "SET `ezf_field_lenght`='".$_POST["ezf_field_lenght"]."' "
                            . "WHERE `ezf_field_id`='".$modelfield->ezf_field_id."' ";
                    Yii::$app->db->createCommand($sqlUpdate)->execute();

                    if ($modelfield->save()) {
						\backend\modules\ezforms\components\EzformFunc::deleteCondition($modelfield);
                        $strlenght = count($_POST['radioVar']);

                        for ($i = 0; $i < $strlenght; $i++) {
                            $ezf_choice_id = GenMillisecTime::getMillisecTime();
                            $ezf_choicevalue = $_POST['radioVar'][$i];
                            $i++;
                            $ezf_choicelabel = $_POST['radioVar'][$i];
                            $sql = "INSERT INTO `ezform_choice` (`ezf_choice_id`, `ezf_field_id`, `ezf_choicevalue`, `ezf_choicelabel`) "
                                    . "VALUES ('$ezf_choice_id', '$modelfield->ezf_field_id', '$ezf_choicevalue', '$ezf_choicelabel');";
                            $dataField = Yii::$app->db->createCommand($sql)->execute();

                            //etc text
                            $i++;
                            $ezf_choicelabel = $_POST['radioVar'][$i];
                            if($ezf_choicelabel <> ''){
                                //insert field etc
                                $ezf_fields_id_etc = GenMillisecTime::getMillisecTime();
                                $sql = "INSERT INTO `ezform_fields` (`ezf_field_id`, `ezf_id`, `ezf_field_name`, `ezf_field_label`, `ezf_field_type`, `ezf_field_ref`, `ezf_field_ref_field`) "
                                    . "VALUES ('$ezf_fields_id_etc', '$modelfield->ezf_id', '$ezf_choicelabel', 'ETC Text', '0', '$ezf_choice_id', '$modelfield->ezf_field_id');";
                                Yii::$app->db->createCommand($sql)->execute();

                                //add column table
                                $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                                    . "WHERE TABLE_NAME = '" . $ezf_tablename->ezf_table . "' "
                                    . "AND COLUMN_NAME = '" . $ezf_choicelabel . "'  ";

                                $num = Yii::$app->db->createCommand($sqlColumn)->query()->count();
                                //มีไม่สนใจ ไม่มีเพิ่มให้
                                if (!$num) {
                                    $sql = "ALTER TABLE `" . $ezf_tablename->ezf_table . "`"
                                        . "ADD COLUMN $ezf_choicelabel VARCHAR(100)";
                                    Yii::$app->db->createCommand($sql)->execute();
                                }

                            }// end

                        }
			            \backend\modules\ezforms\components\EzformFunc::saveCondition($_POST['conditionFields']);
                        //add other field
                        /*
                        if(Yii::$app->request->post('radioValueOther')){
                            $ezf_choice_id = GenMillisecTime::getMillisecTime();
                            $ezf_choicevalue = 'valother_'.$_POST['radioValueOther'];
                            $ezf_choicelabel = 'อื่นๆ';
                            $sql = "INSERT INTO `ezform_choice` (`ezf_choice_id`, `ezf_field_id`, `ezf_choicevalue`, `ezf_choicelabel`, `ezf_choiceetc`) "
                                        . "VALUES ('$ezf_choice_id', '$modelfield->ezf_field_id', '$ezf_choicevalue', '$ezf_choicelabel', '1');";
                            Yii::$app->db->createCommand($sql)->execute();
                        }*/
                        $result = [
                            'status' => 'success',
                            'action' => 'update',
                        ];
                        return $result;
                    } else {
                        echo "FALSE";
                    }
                }
            }
        }
    }

    protected function findFieldModel($id) {
        if (($model = EzformFields::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

?>
