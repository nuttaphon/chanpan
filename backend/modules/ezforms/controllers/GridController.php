<?php
/**
 * Created by PhpStorm.
 * User: balz
 * Date: 11/9/2558
 * Time: 11:12
 */

namespace backend\modules\ezforms\controllers;

use backend\modules\ezforms\models\EzformChoice;
use common\lib\codeerror\helpers\GenMillisecTime;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Response;
use backend\modules\ezforms\components\EzformQuery;

class GridController extends Controller
{
    private $resultValidate = [];
    private $ezf_field;
    private $ezf_table;
    private $arrayGrid;
    private $arrayLabel;
    private $order;
    private $field_id;
    private $form_id;

    public function init(){
        parent::init();

        if(isset($_POST["EzformFields"]["ezf_id"])){
            $ezf_table = Ezform::find('ezf_table')->where('ezf_id = :ezf_id',['ezf_id'=>$_POST["EzformFields"]["ezf_id"]])->One();
            $this->form_id = $_POST["EzformFields"]["ezf_id"];
            $this->ezf_table = $ezf_table["ezf_table"];
        }else if(isset($_GET["id"])){
            $field = EzformFields::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$_GET["id"]])->One();
            $form = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$field["ezf_id"]])->One();
            $this->ezf_field = $field["ezf_field_id"];
            $this->ezf_table = $form["ezf_table"];
        }
    }

    public function test(){
	VarDumper::dump($_POST,10,true);
	exit();
    }
    
    public function actionInsertfield($forder){
//        VarDumper::dump($_POST['grid'],10,true);
//        exit;
        $this->arrayGrid = $_POST["grid"];
        $this->arrayLabel = $_POST["labelGrid"];
        $this->order = $forder;
        $this->validateValue();
        $result = $this->setArrayUnique();
        if($result == 0) {
            $result = [
                'status' => 'uniquevalue',
                'action' => 'alert',
                'message' => '<span style="font-size: 20px;"><strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าของตัวแปรในสเกลห้ามซ้ำกัน' . '</span>'),
            ];
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $result;
        }else{
            if(in_array("1",$this->resultValidate)=="1"){
                $result = [
                    'status' => 'warning',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                ];
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;

            }else{
                $this->insertColumn();
                $result = [
                    'status' => 'success',
                ];
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }
        }
    }
    private function insertColumn(){
        $nfield = new EzformFields();
        if($nfield->load(Yii::$app->request->post())){
            $nfield->ezf_field_id = GenMillisecTime::getMillisecTime();
            $nfield->ezf_field_head_label = 1;
            $nfield->ezf_field_order = $this->order;
            if($nfield->save()){
                $this->field_id = $nfield->ezf_field_id;
                foreach($this->arrayLabel as $label){
                    $nchoice = new EzformChoice();
                    $nchoice->ezf_choice_id = GenMillisecTime::getMillisecTime();
                    $nchoice->ezf_field_id = $this->field_id;
                    $nchoice->ezf_choicelabel = $label;
                    $nchoice->ezf_choicevalue = 'grid';
                    $nchoice->save();
                }
            }
        }

        foreach($this->arrayGrid as $grid) {
            $qfield = new EzformFields();
            $qfield->ezf_field_id = GenMillisecTime::getMillisecTime();
            $qfield->ezf_id = $this->form_id;
            $qfield->ezf_field_name = 'questiongrid';
            $qfield->ezf_field_label = $grid["title"];
            $qfield->ezf_field_head_label = 1;
            $qfield->ezf_field_type = 250;
            $qfield->ezf_field_sub_id = $this->field_id;
            if ($qfield->save()) {
                foreach ($grid["value"] as $value) {
                    $vfield = new EzformFields();
                    $vfield->ezf_field_id = GenMillisecTime::getMillisecTime();
                    $vfield->ezf_id = $this->form_id;
                    if($value[2]==254){
                        $vfield->ezf_field_label = $value[1];
                    }else{
                        $vfield->ezf_field_label = '';
                    }
                    $vfield->ezf_field_name = $value[0];
                    $vfield->ezf_field_type = $value[2];
                    $vfield->ezf_field_sub_id = $qfield->ezf_field_id;
                    if($vfield->save()){

                    }
                }
            }

        }
        $this->alterColumn();
    }
    public function actionDeletegrid($id){
        $nfield = EzformFields::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$id])->One();
        $nfield->delete();
        $qfield = EzformFields::find()->Where('ezf_field_sub_id = :ezf_field_sub_id',[':ezf_field_sub_id'=>$id])->all();

        foreach($qfield as $q){

            $vfield = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[':ezf_field_id'=>$q["ezf_field_id"]])->all();
            foreach($vfield as $v){
                $this->dropColumn($v);
            }
            EzformFields::deleteAll('ezf_field_sub_id = :ezf_field_id',[':ezf_field_id'=>$q["ezf_field_id"]]);
        }
        EzformFields::deleteAll('ezf_field_sub_id = :ezf_field_sub_id',[':ezf_field_sub_id'=>$id]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $result = [
            'status' => 'warning',
            'action' => 'update',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
            'data' => $this->ezf_field,
        ];
        return $result;

    }
    public function actionUpdatefield($id){
        $this->arrayGrid = $_POST["grid"];
        $this->arrayLabel = $_POST["labelGrid"];
        $result = $this->setArrayUnique($_POST["grid"]);
	
        foreach($this->arrayGrid as $g){
            
            foreach($g["value"] as $v){

            }
        }
//	VarDumper::dump($_POST,10,true);
//	exit();
//	
	$updateAction = \backend\modules\ezforms\components\EzformFunc::ezformUpdate($id);
	if($updateAction){
	    return $updateAction;
	}
	
	if($result == 0){
	    $result = [
		'status' => 'uniquevalue',
		'action' => 'alert',
		'message' => '<span style="font-size: 20px;"><strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าของตัวแปรในสเกลห้ามซ้ำกัน'.'</span>'),
	    ];
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    return $result;
	}else{
	    $field = EzformFields::find()->where('ezf_field_type IN (251,252,253,254)')->all();
	    $countfield = count($field);
	    $count = array();
	    foreach($this->arrayGrid as $grid){

		$countvalue = sizeof($grid["value"])*sizeof($this->arrayGrid);
		array_push($count,$countvalue);
	    }
	    $countvalue = $count[0];
	    $checkField = array();
	    foreach($this->arrayGrid as $g) {
		foreach($g["value"] as $v){
		    $result = EzformFields::find()->where('ezf_field_name = :ezf_field_name ',['ezf_field_name'=>$v[0]])->count();
		    array_push($checkField,$result);
		}
	    }
	    if(in_array(0,$checkField)=="1" || $countvalue < $countfield) {
		$modelfield = EzformFields::find()->where('ezf_field_id = :ezf_field_id',[":ezf_field_id"=>$id])->One();
		$forder = $modelfield->ezf_field_order;
		$field_id = $modelfield->ezf_field_id;
		$this->actionDeletegrid($field_id);
		$this->actionInsertfield($forder);
		$result = [
		    'status' => 'success',
		];
		Yii::$app->response->format = Response::FORMAT_JSON;
		return $result;
	    }else{
		$modelfield = EzformFields::find()->where('ezf_field_id = :ezf_field_id',[":ezf_field_id"=>$id])->One();
		$questionfield = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[":ezf_field_id"=>$id])->all();

		if($modelfield->load(Yii::$app->request->post())){
		    if($modelfield->save()){
			$choice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[":ezf_field_id"=>$id])->all();
			$k=0;
			foreach($choice as $c){
			    $sql = "UPDATE `ezform_choice` SET `ezf_choicelabel`='".$this->arrayLabel[$k]."' WHERE ezf_choice_id='".$c["ezf_choice_id"]."' ";
			    Yii::$app->db->createCommand($sql)->execute();
			    $k++;
			}
			$i=1;
			foreach($questionfield as $question){
			    $sql = "UPDATE `ezform_fields` SET `ezf_field_label`='".$this->arrayGrid[$i]["title"]."' WHERE ezf_field_id='".$question["ezf_field_id"]."' ";
			    Yii::$app->db->createCommand($sql)->execute();

			    $value = EzformFields::find()
				->where('ezf_field_sub_id = :ezf_field_id',[':ezf_field_id'=>$question["ezf_field_id"]])
				->all();
			    $j=1;
			    foreach($value as $v){
				$sql = "UPDATE `ezform_fields` SET `ezf_field_type`='".$this->arrayGrid[$i]["value"][$j][2]."' WHERE ezf_field_id='".$v["ezf_field_id"]."' ";
//                                echo $sql."<br>";
				Yii::$app->db->createCommand($sql)->execute();

				$j++;
			    }
			    $i++;
			}
			$result = [
			    'status' => 'success',
			];
			Yii::$app->response->format = Response::FORMAT_JSON;
			return $result;
		    }
		}
	    }
	    $this->validateValue();
	}
	
	
        

    }
    
    public function actionTypegrid($row){

        return $this->renderAjax("_grid.php",['row'=>$row]);
    }
    public function actionFormupdate($id){

        $model2 = EzformFields::find()
            ->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$id])
            ->andWhere('ezf_field_head_label = :ezf_field_head_label',[':ezf_field_head_label'=>1])
            ->One();

        $nameValue = $model2->ezf_field_name;

        $choice = EzformChoice::find()
            ->where('ezf_field_id = :field_id',[':field_id'=>$id])
            ->all();
        $question = EzformFields::find()
            ->where('ezf_field_sub_id = :ezf_field_sub_id',[':ezf_field_sub_id'=>$id])
            ->all();

        return $this->renderAjax('/ezform/_editpanel',
            [
                'nameValue'=>$nameValue,
                'model2'=>$model2,
                'choice'=>$choice,
                'question'=>$question,
            ]
        );
    }
    public function actionInputgridchoice(){
        $fieldid = GenMillisecTime::getMillisecTime();
        $arrayData = array();

        foreach($_POST['radio'] as $value){
            $i=0;
            foreach($value as $key){
                $arrayData[$i][] = $key;
                $i++;
            }
        }
        $field = new EzformFields();
        $field->ezf_field_id = $fieldid;
        $field->ezf_field_name = 'null';
        $field->save();
        foreach($arrayData as $data){
            $choice = new EzformChoice();
            $choice->ezf_choice_id = GenMillisecTime::getMillisecTime();
            $choice->ezf_field_id = $fieldid;
            $choice->ezf_choicevalue = $data[0];
            $choice->ezf_choicelabel = $data[1];
            $choice->save();
        }
        return $fieldid;
    }
    private function setArrayUnique(){
        $arrayUnique = [];
        foreach($this->arrayGrid as $value){
            foreach($value["value"] as $v){

                array_push($arrayUnique,$v[0]);
            }
        }
        $unique = array_unique($arrayUnique);
        if(count($arrayUnique)!=count($unique)){
            $result = 0;
        }else{
            $result = 1;
        }

        return $result;
    }
    private function dropColumn($v){
        $sql = "ALTER TABLE `".$this->ezf_table."`"
            . " DROP COLUMN ".$v['ezf_field_name']." ";
        Yii::$app->db->createCommand($sql)->execute();
    }
    private function alterColumn(){
        foreach($this->arrayGrid as $grid){
            foreach($grid["value"] as $value){
                $sql = "ALTER TABLE `".$this->ezf_table."`"
                    . " ADD COLUMN $value[0] VARCHAR(100)";
                Yii::$app->db->createCommand($sql)->execute();
            }
        }
    }

    private function validateValue(){
        foreach($this->arrayGrid as $value){
            foreach($value["value"] as $val){

                $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                    . "WHERE TABLE_NAME = '".$this->ezf_table."' "
                    . "AND COLUMN_NAME = '".$val[0]."'  ";

                $num = Yii::$app->db->createCommand($sqlColumn)->execute();

                array_push($this->resultValidate, $num);
            }
        }
    }

}
