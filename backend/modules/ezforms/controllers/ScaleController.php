<?php
/**
 * Created by PhpStorm.
 * User: balz
 * Date: 3/9/2558
 * Time: 14:03
 */

namespace backend\modules\ezforms\controllers;

use backend\models\Ezform;
use backend\modules\ezforms\models\EzformChoice;
use Yii;
use backend\modules\ezforms\models\EzformFields;
use yii\helpers\VarDumper;
use yii\web\Controller;
use common\lib\codeerror\helpers\GenMillisecTime;
use yii\web\Response;

class ScaleController extends Controller
{
    private $arrayQuestion = [];

    private $ezf_table;

    private $ezf_field;

    private $arrayValidate = [];

    private $arrayScale = [];

    public $fieldpost;

    public $order;

    public function init(){
        parent::init();

        if(isset($_POST["EzformFields"]["ezf_id"])){
            $ezf_table = Ezform::find('ezf_table')->where('ezf_id = :ezf_id',['ezf_id'=>$_POST["EzformFields"]["ezf_id"]])->One();
            $this->ezf_table = $ezf_table["ezf_table"];
        }else if(isset($_GET["id"])){
            $field = EzformFields::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$_GET["id"]])->One();
            $form = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$field["ezf_id"]])->One();
            $this->ezf_field = $field["ezf_field_id"];
            $this->ezf_table = $form["ezf_table"];
        }
    }
    public function actionDeletescale(){
        $field = EzformFields::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$this->ezf_field])->One();
        if($field->delete()){

            $fieldalter = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_sub_id',[':ezf_field_sub_id'=>$this->ezf_field])->all();
            foreach($fieldalter as $alter){
                $this->altertDropTable($alter["ezf_field_name"]);
            }

            $subfield = EzformFields::deleteAll('ezf_field_sub_id = :ezf_field_sub_id',[':ezf_field_sub_id'=>$this->ezf_field]);
            if($subfield){
                Yii::$app->response->format = Response::FORMAT_JSON;
                $result = [
                    'status' => 'warning',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                    'data' => $this->ezf_field,
                ];
                return $result;
            }

        }
    }

    public function actionInsertfield($forder){

        //set Value;
        $this->fieldpost = Yii::$app->request->post();
        $this->order = $forder;

        //insert Ezform_field And Alter Table;
        $Question = $_POST["question"];
        //insert Ezform_choice;
        $Scale = $_POST["scale"];

        foreach($Question as $val){
            $i=0;
            foreach($val as $key){
                $this->arrayQuestion[$i][] = $key;
                $i++;
            }
        }
        foreach($Scale as $val){
            $i=0;
            foreach($val as $key){
                $this->arrayScale[$i][] = $key;
                $i++;
            }
        }
        $this->validateColumn();
        $result1 = $this->setArrayUnique($Question);
        $result2 = $this->setArrayUnique($Scale);
        if($result1==0 || $result2==0){
            $result = [
                'status' => 'uniquevalue',
                'action' => 'alert',
                'message' => '<span style="font-size: 20px;"><strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าของตัวแปรในสเกลห้ามซ้ำกัน'.'</span>'),
            ];
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $result;
        }else {
            if (in_array("1", $this->arrayValidate) == "1") {
                $result = [
                    'status' => 'warning',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณาลองใหม่'),
                ];
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            } else {
                $this->insertColumn();
                $result = [
                    'status' => 'success',
                ];
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }
        }
    }
    public function actionUpdatefield($id){
        $Question = $_POST["question"];
        $Scale = $_POST["scale"];
        $result1 = $this->setArrayUnique($Question);
        $result2 = $this->setArrayUnique($Scale);

        if($result1==0 || $result2==0){
            $result = [
                'status' => 'uniquevalue',
                'action' => 'alert',
                'message' => '<span style="font-size: 20px;"><strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ค่าของตัวแปรในสเกลห้ามซ้ำกัน'.'</span>'),
            ];
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $result;
        }else{
            foreach($Question as $val){
                $i=0;
                foreach($val as $key){
                    $this->arrayQuestion[$i][] = $key;
                    $i++;
                }
            }
            foreach($Scale as $val){
                $i=0;
                foreach($val as $key){
                    $this->arrayScale[$i][] = $key;
                    $i++;
                }
            }
            $field = EzformFields::find()->where('ezf_field_sub_id = :ezf_field_id',[":ezf_field_id"=>$id])->all();
            $countfield = count($field);
            $checkField = array();
            foreach($this->arrayQuestion as $question) {
                $result = EzformFields::find()->where('ezf_field_name = :ezf_field_name ',['ezf_field_name'=>$question[1]])->count();
                array_push($checkField,$result);
            }
            $countCheckfield = count($checkField);
            if(in_array(0,$checkField)=="1" || $countCheckfield < $countfield){
                $modelfield = EzformFields::find()->where('ezf_field_id = :ezf_field_id',[":ezf_field_id"=>$id])->One();
                $forder = $modelfield->ezf_field_order;
                $this->ezf_field = $id;
                $this->actionDeletescale();
                $this->actionInsertfield($forder);
                $result = [
                    'status' => 'success',
                ];
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }else{

                $modelfield = EzformFields::find()->where('ezf_field_id = :ezf_field_id',[":ezf_field_id"=>$id])->One();
                $field_id = $modelfield->ezf_field_id;
                $ezf_id = $modelfield->ezf_id;

                if($modelfield->load(Yii::$app->request->post())){

                    if($modelfield->save()) {

                        EzformFields::deleteAll('ezf_field_sub_id = :sub_id',[':sub_id'=>$field_id]);
                        EzformChoice::deleteAll('ezf_field_id = :field_id',[':field_id'=>$field_id]);

                        foreach($this->arrayQuestion as $question){
                            $subfield = new EzformFields();
                            $subfield->ezf_field_id = GenMillisecTime::getMillisecTime();
                            $subfield->ezf_id = $ezf_id;
                            $subfield->ezf_field_name = $question[1];
                            $subfield->ezf_field_label = $question[0];
                            $subfield->ezf_field_type = 231;
                            $subfield->ezf_field_sub_id = $field_id;
                            $subfield->save();
                        }

                        foreach($this->arrayScale as $scale){
                            $choice = new EzformChoice();
                            $choice->ezf_choice_id = GenMillisecTime::getMillisecTime();
                            $choice->ezf_field_id = $field_id;
                            $choice->ezf_choicevalue = $scale[1];
                            $choice->ezf_choicelabel = $scale[0];
                            $choice->save();
                        }

                        $result = [
                            'status' => 'success',
                        ];
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return $result;
                    }
                }
            }
        }
    }

    public function actionFormupdate($id){

        $model2 = EzformFields::find()
            ->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$id])
            ->andWhere('ezf_field_head_label = :ezf_field_head_label',[':ezf_field_head_label'=>1])
            ->One();
        $nameValue = $model2->ezf_field_name;
        $modelquestion = EzformFields::find()->where(['ezf_field_sub_id'=>$model2->ezf_field_id])->all();
        $modelchoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id',[':ezf_field_id'=>$model2->ezf_field_id])->all();
        return $this->renderAjax('/ezform/_editpanel',
            [
                'model2'=>$model2,
                'modelquestion'=>$modelquestion,
                'modelchoice'=>$modelchoice,
                'nameValue'=>$nameValue,
            ]);

    }
    protected function setArrayUnique($value){

        $unique = array_unique($value["value"]);
        if(count($value["value"])!=count($unique)){
            $result = 0;
        }else{
            $result = 1;
        }

        return $result;

    }
    protected function validateColumn(){
        foreach($this->arrayQuestion as $key=>$val){
            $sqlColumn = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS "
                . "WHERE TABLE_NAME = '".$this->ezf_table."' "
                . "AND COLUMN_NAME = '".$val[1]."'  ";
            $num = Yii::$app->db->createCommand($sqlColumn)->execute();
            array_push($this->arrayValidate, $num);
        }
    }
    protected function insertColumn(){
        $field = new EzformFields();
        if ($field->load($this->fieldpost)) {
            $field->ezf_field_id = GenMillisecTime::getMillisecTime();
            $field->ezf_field_order = $this->order;
            $field->ezf_field_head_label = 1;
            if($field->save()){
                foreach($this->arrayScale as $scale){
                    $choice = new EzformChoice();
                    $choice->ezf_choice_id = GenMillisecTime::getMillisecTime();
                    $choice->ezf_field_id = $field->ezf_field_id;
                    $choice->ezf_choicevalue = $scale[1];
                    $choice->ezf_choicelabel = $scale[0];
                    $choice->save();
                }
                foreach($this->arrayQuestion as $question){
                    $subfield = new EzformFields();
                    $subfield->ezf_field_id = GenMillisecTime::getMillisecTime();
                    $subfield->ezf_id = $field->ezf_id;
                    $subfield->ezf_field_name = $question[1];
                    $subfield->ezf_field_label = $question[0];
                    $subfield->ezf_field_type = 231;
                    $subfield->ezf_field_sub_id = $field->ezf_field_id;
                    $subfield->save();
                }
                $this->alterInsertTable();
            }
        }
    }

    protected function altertDropTable($value){
        $sql = "ALTER TABLE `".$this->ezf_table."`"
            . " DROP COLUMN $value ";
        Yii::$app->db->createCommand($sql)->execute();
    }
    protected function alterInsertTable(){
        foreach($this->arrayQuestion as $question){
            $sql = "ALTER TABLE `".$this->ezf_table."`"
                . " ADD COLUMN $question[1] VARCHAR(10)";
            Yii::$app->db->createCommand($sql)->execute();
        }
    }

}