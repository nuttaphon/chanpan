<?php

namespace backend\modules\ezforms\models;

use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\Ezform;

class EzformDynamic extends \yii\db\ActiveRecord {

    private static $_tablename;

    public function __construct($tablename = '', $config = []) {
	if ($tablename != '') {
	    self::$_tablename = $tablename;
	}
	parent::__construct($config);
    }

    public static function tableName() {
	return self::$_tablename;
    }

    public function attributes() {
	$fields = $this->getFields();
	$attribute = [
			'id'=>'',
			'xsourcex'=>'',
			'xdepartmentx'=>'',
			'rstat'=>'',
			'sitecode'=>'',
			'ptid'=>'',
			'ptcode'=>'',
			'ptcodefull'=>'',
			'hsitecode'=>'',
			'hptcode'=>'',
			'user_create'=>'',
			'create_date'=>'',
			'user_update'=>'',
			'update_date'=>'',
			'target'=>'',
			'error'=>''
	    	    ];// fix attribute
	
	foreach ($fields as $key => $value) {
	    $attribute[$value['ezf_field_name']] = '';
	}

	return array_keys($attribute);

    }

    public function rules() {
	$fields = $this->getFields();
	$safe = [];
	
	foreach ($fields as $key => $value) {
	    $safe[] = $value['ezf_field_name'];
	}

	$safe[] ='ptid';
	$safe[] ='sitecode';
	$safe[] ='ptcode';
	$safe[] ='ptcodefull';
	$safe[] ='hsitecode';
	$safe[] ='hptcode';
	return [
	    [$safe, 'safe']
	];
    }

    public function attributeLabels() {

		$fields = $this->getFields();
		$labels = [];
		foreach ($fields as $key => $value) {
			$labels[$value['ezf_field_name']] = isset($value['ezf_field_label']) ? $value['ezf_field_label'] : $value['ezf_field_name'];
		}
		return $labels;

	}

    public function getFields() {
		$table = Ezform::find()->where(['ezf_table' => self::$_tablename])->one();
		$fields = EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $table->ezf_id])
			->andWhere('ezf_field_head_label IS NULL ')
			->andWhere('ezf_field_type is NOT NULL')->all();

		return $fields;
    }
    
    public static function updateAll($attributes, $condition = '', $params = [])
    {
        $command = static::getDb()->createCommand();
	$condition[static::primaryKey()[0]] = $attributes[static::primaryKey()[0]];
        $command->update(static::tableName(), $attributes, $condition, $params);
	
        return $command->execute();
    }
}