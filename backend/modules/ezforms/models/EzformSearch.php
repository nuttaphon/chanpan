<?php

namespace backend\modules\ezforms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ezforms\models\Ezform;

/**
 * EzformSearch represents the model behind the search form about `backend\modules\ezforms\models\Ezform`.
 */
class EzformSearch extends Ezform
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'user_create', 'user_update', 'status', 'shared', 'public_listview', 'public_edit', 'public_delete'], 'integer'],
            [['ezf_name', 'ezf_detail', 'ezf_table', 'create_date', 'update_date', 'username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ezform::find()
                        ->select('ezform.*,`user`.`username`')
                        ->leftJoin('user', '`ezform`.`user_create` = `user`.`id`')
                        ->where('ezf_id > :ezf_id', [':ezf_id' => 100000])
                        ->andWhere(['user_create' => Yii::$app->user->id])
                        //->andWhere(['ezform.status' => 1])
                        ->andWhere('ezform.status <> :status', [':status' => 3])->orderBy('create_date DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['username', 'ezf_id', 'ezf_name', 'ezf_detail', 'shared', 'public_listview', 'public_edit', 'public_delete', 'create_date']],
	    'pagination'=>[
		'pageSize'=>50,
	    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ezf_id' => $this->ezf_id,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
            'status' => $this->status,
            'shared' => $this->shared,
            'public_listview' => $this->public_listview,
            'public_edit' => $this->public_edit,
            'public_delete' => $this->public_delete,
        ]);

        $query->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_detail', $this->ezf_detail])
            ->andFilterWhere(['like', 'username', $this->ezf_detail])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table]);

        return $dataProvider;
    }

    public function searchMyForm($params)
    {
        $query = Ezform::find()
                        ->select('ezform.*,`user`.`username`')
                        ->leftJoin('user', '`ezform`.`user_create` = `user`.`id`')
                        ->where('ezf_id > :ezf_id', [':ezf_id' => 100000])
                        ->andWhere(['user_create' => Yii::$app->user->id])
                        ->orWhere('ezf_id in (SELECT ezf_id FROM ezform_co_dev WHERE user_co = :user_id)', [':user_id' => Yii::$app->user->id])
                        ->andWhere('ezform.status <> :status', [':status' => 3]);
        //\yii\helpers\VarDumper::dump($query->createCommand()->sql, 10, TRUE);
        //exit();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['username', 'ezf_id', 'ezf_name', 'ezf_detail', 'shared', 'public_listview', 'public_edit', 'public_delete', 'create_date']],
	    'pagination'=>[
		'pageSize'=>50,
	    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ezf_id' => $this->ezf_id,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
            'status' => $this->status,
            'shared' => $this->shared,
            'public_listview' => $this->public_listview,
            'public_edit' => $this->public_edit,
            'public_delete' => $this->public_delete,
        ]);

        $query->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_detail', $this->ezf_detail])
            ->andFilterWhere(['like', 'username', $this->ezf_detail])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table]);

        return $dataProvider;
    }

    public function searchMyFormDelete($params)
    {
        $query = Ezform::find()
                        ->select('ezform.*,`user`.`username`')
                        ->leftJoin('user', '`ezform`.`user_create` = `user`.`id`')
                        ->where(['user_create' => Yii::$app->user->id])
                        ->andWhere(['ezform.status' => 3]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['username', 'ezf_id', 'ezf_name', 'ezf_detail', 'shared', 'public_listview', 'public_edit', 'public_delete', 'create_date']],
	    'pagination'=>[
		'pageSize'=>50,
	    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ezf_id' => $this->ezf_id,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
            'status' => $this->status,
            'shared' => $this->shared,
            'public_listview' => $this->public_listview,
            'public_edit' => $this->public_edit,
            'public_delete' => $this->public_delete,
        ]);

        $query->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_detail', $this->ezf_detail])
            ->andFilterWhere(['like', 'username', $this->ezf_detail])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table]);

        return $dataProvider;
    }

    public function searchMyFormAssign($params)
    {
        $query = Ezform::find()->where('ezform_favorite.userid=\''.Yii::$app->user->id.'\'')->andWhere('ezform.`status` <> :status', [':status' => 3]);
        $query->innerJoin('ezform_favorite', 'ezform_favorite.ezf_id = ezform.ezf_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['username', 'ezf_id', 'ezf_name', 'ezf_detail', 'shared', 'public_listview', 'public_edit', 'public_delete', 'create_date']],
	    'pagination'=>[
		'pageSize'=>50,
	    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ezf_id' => $this->ezf_id,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
            'status' => $this->status,
            'shared' => $this->shared,
            'public_listview' => $this->public_listview,
            'public_edit' => $this->public_edit,
            'public_delete' => $this->public_delete,
        ]);

        $query->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_detail', $this->ezf_detail])
            ->andFilterWhere(['like', 'username', $this->ezf_detail])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table]);

        return $dataProvider;
    }
    public function searchMyFormPublic($params)
    {
        $query = Ezform::find()->where('shared=1');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['username', 'ezf_id', 'ezf_name', 'ezf_detail', 'shared', 'public_listview', 'public_edit', 'public_delete', 'create_date']],
	    'pagination'=>[
		'pageSize'=>50,
	    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ezf_id' => $this->ezf_id,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
            'status' => $this->status,
            'shared' => $this->shared,
            'public_listview' => $this->public_listview,
            'public_edit' => $this->public_edit,
            'public_delete' => $this->public_delete,
        ]);

        $query->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_detail', $this->ezf_detail])
            ->andFilterWhere(['like', 'username', $this->ezf_detail])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table]);

        return $dataProvider;
    }
    public function searchMyFormTdc($params)
    {
        $query = Ezform::find()->where("shared=99");
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['username', 'ezf_id', 'ezf_name', 'ezf_detail', 'shared', 'public_listview', 'public_edit', 'public_delete', 'create_date']],
	    'pagination'=>[
		'pageSize'=>60,
	    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ezf_id' => $this->ezf_id,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
            'status' => $this->status,
            'shared' => $this->shared,
            'public_listview' => $this->public_listview,
            'public_edit' => $this->public_edit,
            'public_delete' => $this->public_delete,
        ]);

        $query->andFilterWhere(['like', 'ezf_name', "("]);
        
              $query->orderBy('ezf_name');
        //\appxq\sdii\utils\VarDumper::dump($query->createCommand()->rawSql);
        return $dataProvider;
    }    
}
