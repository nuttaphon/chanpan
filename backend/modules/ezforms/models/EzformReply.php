<?php

namespace backend\modules\ezforms\models;

use Yii;

/**
 * This is the model class for table "ezform_reply".
 *
 * @property string $id
 * @property string $ezf_id
 * @property string $data_id
 * @property string $ezf_comment
 * @property string $ezf_json_old
 * @property string $ezf_json_new
 * @property string $user_create
 * @property string $create_date
 * @property string $update_date
 * @property string $xsourcex
 * @property integer $rstat
 * @property integer $bookmark
 * @property string $file_upload
 * @property integer $type
 */
class EzformReply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'data_id', 'ezf_comment', 'user_create', 'create_date'], 'required'],
            [['ezf_id', 'data_id', 'user_create', 'rstat', 'bookmark', 'type'], 'integer'],
            [['ezf_comment', 'ezf_json_old', 'ezf_json_new'], 'string'],
            [['file_upload', 'create_date', 'update_date', 'xsourcex'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'primary key',
            'ezf_id' => 'Ezform ID',
            'data_id' => 'Data ID',
            'ezf_comment' => 'รายละเอียด',
            'ezf_json_old' => 'EzFrom Json Old',
            'ezf_json_new' => 'EzFrom Json New',
            'user_create' => 'โดย',
            'create_date' => 'เวลา',
            'update_date' => 'เวลาที่ปรับปรุงล่าสุด',
            'xsourcex' => 'xsourcex',
            'rstat' => 'rstat',
            'bookmark' => 'Bookmark',
            'type' => 'ประเภท',
        ];
    }
}
