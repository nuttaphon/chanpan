<?php

namespace backend\modules\ezforms\models;

use Yii;

/**
 * This is the model class for table "ezform_input".
 *
 * @property integer $input_id
 * @property string $input_name
 * @property string $input_class
 * @property string $input_function
 * @property string $input_class_validate
 * @property string $input_function_validate
 * @property string $input_option
 * @property double $input_order
 */
class EzformInput extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_input';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['input_name', 'input_class', 'input_function'], 'required'],
            [['input_option'], 'string'],
            [['input_order'], 'number'],
            [['input_name'], 'string', 'max' => 20],
            [['input_class', 'input_class_validate'], 'string', 'max' => 80],
            [['input_function', 'input_function_validate'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'input_id' => Yii::t('app', 'ID'),
            'input_name' => Yii::t('app', 'Name'),
            'input_class' => Yii::t('app', 'Class'),
            'input_function' => Yii::t('app', 'Function'),
            'input_class_validate' => Yii::t('app', 'class validate'),
            'input_function_validate' => Yii::t('app', 'function validate'),
            'input_option' => Yii::t('app', 'Option'),
            'input_order' => Yii::t('app', 'Order'),
        ];
    }
}
