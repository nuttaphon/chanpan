<?php

namespace backend\modules\ezforms\models;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use Yii;

/**
 * This is the model class for table "ezform".
 *
 * @property string $ezf_id
 * @property string $ezf_name
 * @property string $ezf_version
 * @property string $ezf_detail
 * @property string $ezf_table
 * @property string $user_create
 * @property string $create_date
 * @property string $user_update
 * @property string $update_date
 * @property integer $status
 * @property integer $shared
 * @property integer $public_listview
 * @property integer $public_edit
 * @property integer $public_delete
 * @property string $sql
 * @property string $error
 * @property string $js
 * @property EzformFields[] $ezformFields
 */
class Ezform extends \yii\db\ActiveRecord
{
    public $username;
    public $num_form;
    public $comp_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['ezf_id', 'ezf_name', 'ezf_table', 'user_update', 'status'], 'required'],
            [['ezf_id',  'user_update', 'status', 'shared', 'public_listview', 'public_edit', 'public_delete', 'comp_id_target', 'category_id'], 'integer'],
            [['ezf_detail'], 'string'],
	    [['ezf_date_order','ezf_version', 'telegram'], 'string', 'max' => 50],
            [['sql_condition', 'sql_announce',  'error','js', 'create_date', 'update_date'], 'safe'],
            [['ezf_name', 'ezf_table'], 'string', 'max' => 100]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'update_date',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_id' => Yii::t('app', 'รหัสฟอร์ม'),
            'ezf_name' => Yii::t('app', 'ชื่อฟอร์ม'),
            'ezf_detail' => Yii::t('app', 'รายละเอียด'),
            'ezf_table' => Yii::t('app', 'ตาราง'),
            'user_create' => Yii::t('app', 'ผู้สร้าง'),
            'create_date' => Yii::t('app', 'วันที่สร้าง'),
            'user_update' => Yii::t('app', 'ผู้แก้ไข'),
            'update_date' => Yii::t('app', 'วันที่แก้ไข'),
            'status' => Yii::t('app', 'สถานะ'),
            'shared' => Yii::t('app', 'การแชร์ฟอร์ม'),
            'public_listview' => Yii::t('app', 'อนุญาตให้ view List Data table ของฟอร์ม'),
            'public_edit' => Yii::t('app', 'อนุญาตให้ Edit/Update List Data table ของฟอร์ม'),
            'public_delete' => Yii::t('app', 'อนุญาตให้ Remove List Data table ของฟอร์ม'),
            'co_dev' =>  Yii::t('app', 'ผู้สร้างร่วม'),
            'assign' =>  Yii::t('app', 'ผู้ใช้ฟอร์มนี้'),
            'comp_id_target' =>  Yii::t('app', 'เลือกเป้าหมาย'),
            'category_id' => Yii::t('app', 'หมวดกิจกรรม'),
            'field_detail' => Yii::t('app', 'เลือกตัวแปรเพื่อแสดงลักษณะหลักของฟอร์ม'),
	        'sql_announce' => Yii::t('app', 'SQL Announce'),
            'sql_condition' => Yii::t('app', 'SQL Condition'),
            'error' => Yii::t('app', 'ERROR'),
            'js' => Yii::t('app', 'JS'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformFields()
    {
        return $this->hasMany(EzformFields::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @inheritdoc
     * @return EzformQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EzformQuery(get_called_class());
    }
}
