<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ezforms2\models\EzformConfigSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



?>
<div class="ezform-config-index">

    <?php  Pjax::begin(['id'=>'ezform-config-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'ezform-config-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['/ezforms2/ezform-config/create', 'ezf_id'=>$ezf_id, 'type'=>'report']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform-config']),
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
        'columns' => [
	    
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
//
//            'config_id',
//            'config_type',
//            'ezf_id',
	    [
		'attribute'=>'config_name',
		'value'=>function ($data){ return \backend\modules\ezforms2\classes\EzfFunc::itemAlias('reportItems', $data['config_name']); },
	    ],
	    [
		'attribute'=>'config_value',
		'value'=>function ($data){ return $data['config_value']==1?'แสดง':'ไม่แสดง'; },
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center;'],
	    ],
            // 'config_options:ntext',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-ezform-config',
    //'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#ezform-config-grid-pjax').on('click', '#modal-addbtn-ezform-config', function() {
    modalEzformConfig($(this).attr('data-url'));
});

$('#ezform-config-grid-pjax').on('click', '#modal-delbtn-ezform-config', function() {
    selectionEzformConfigGrid($(this).attr('data-url'));
});

$('#ezform-config-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#ezform-config-grid').yiiGridView('getSelectedRows');
	disabledEzformConfigBtn(key.length);
    },100);
});

$('#ezform-config-grid-pjax').on('click', '.selectionEzformConfigIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledEzformConfigBtn(key.length);
});

$('#ezform-config-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalEzformConfig('".Url::to(['/ezforms2/ezform-config/update', 'id'=>''])."'+id);
});	

$('#ezform-config-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalEzformConfig(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ezform-config-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledEzformConfigBtn(num) {
    if(num>0) {
	$('#modal-delbtn-ezform-config').attr('disabled', false);
    } else {
	$('#modal-delbtn-ezform-config').attr('disabled', true);
    }
}

function selectionEzformConfigGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionEzformConfigIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ezform-config-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalEzformConfig(url) {
    $('#modal-ezform-config .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ezform-config').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>