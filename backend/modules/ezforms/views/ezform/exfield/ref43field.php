<?php
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use common\lib\sdii\components\helpers\SDNoty;

//print_r($table);
$ref_table1 = array();
$ref_table1 = ArrayHelper::map($table1, 'ezf_id', 'ezf_name', 'class');

?>
<div class='row'>
    <div class="col-md-6">
        <?php
        
        echo Select2::widget([
            'id' => 'select_ezf',
            'name' => 'select_ezf',
            //'value' => $target, // initial value
            'data' => $ref_table1,
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'เลือกตาราง TDC', 'required' => 'required', 'class' =>'js-sin' /*'multiple' => isset($tagMultiple) ? $tagMultiple : FALSE*/],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        
        $this->registerJS("
            $('#select_ezf').on('change',function(){
              var select_ezf = $('#select_ezf option:selected').val();
              var labelValue = 'เลือก '+$('#select_ezf :selected').html();
              $.post('/ezforms/ref43field/selecttable',{ ezf_id: select_ezf }
                ).done(function(result){
                   $('#select_field_div').html(result);
                   ".SDNoty::show('labelValue', '"success"') . "
                }).fail(function(){
                    console.log('server error');
                });
            });
        ");
         
        ?>
    </div>
    <div id="select_field_div" class="col-md-6">
        <?php
        //if(empty($ezf_fields)) $ezf_fields=array('a'=>'xx');
        
        echo Select2::widget([
            'id' => 'select_field',
            'name' => 'select_field',
            //'value' => $target, // initial value
            //'data' => $ezf_fields,
            'size' => Select2::MEDIUM,
            'options' => ['placeholder' => 'เลือก TDC Field', 'required' => 'required', /*'multiple' => isset($tagMultiple) ? $tagMultiple : FALSE*/],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>
</div>

<hr>

<div class='row'>
    <div class="col-md-12">
        <?php
        $items = [
            '1' => 'แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)',
            //'2' => 'ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง',
            '3' => 'แก้ไขเฉพาะตารางปลายทาง',
        ];
        echo \yii\bootstrap\BaseHtml::radio('ref_field_set', true, ['label' =>$items['1'], 'value' => '1']).'<br>';
        //echo \yii\bootstrap\BaseHtml::radio('ref_field_set', false, ['label' =>$items['2'], 'value' => '2']).'<br>';
        echo \yii\bootstrap\BaseHtml::radio('ref_field_set', false, ['label' =>$items['3'], 'value' => '3']);
        ?>
        <input type="hidden" value="<?php echo $ezf_id; ?>" name="ezf_id">
    </div>
</div>