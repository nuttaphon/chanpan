<div class='row'>
    <div class="col-md-12">
        <?php
        $items = [
            '1' => 'แสดงอย่างเดียวแก้ไขไม่ได้ (Read-only)',
            //'2' => 'ถ้ามีการแก้ไขค่า จะอัพเดจค่านั้นทั้งตารางต้นทาง - ปลายทาง',
            '3' => 'แก้ไขเฉพาะตารางปลายทาง',
        ];
        echo \yii\bootstrap\BaseHtml::radio('ref_field_set', true, ['label' =>$items['1'], 'value' => '1']).'<br>';
        //echo \yii\bootstrap\BaseHtml::radio('ref_field_set', false, ['label' =>$items['2'], 'value' => '2']).'<br>';
        echo \yii\bootstrap\BaseHtml::radio('ref_field_set', false, ['label' =>$items['3'], 'value' => '3']);
        ?>
    </div>
</div>