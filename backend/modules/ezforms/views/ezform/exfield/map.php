<style>
    .deleteIcon{
        
        position: absolute;
        bottom: 22px;
        right: 5px;
        color: red;
        font-size: 20px;

    }
</style>
<?php 
    if(isset($modelfield)){
        $dataValue = array();
        $i=0;
        foreach($modelfield as $key=>$value){
            $dataValue[$i] = $value->ezf_field_name;
            $i++;
        }
        if(isset($dataValue[2])){
            echo $dataValue[2];
        }
        
    }else{
        $valueLat = $nameValue.'_Lat';
        $valueLng = $nameValue.'_Lng';
    }
?>

<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <div class="col-md-3">
            <label>ตัวแปร<label>
        </div>
        <div class="col-md-3">
            <label>พิกัด<label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <div class="col-md-3">
            <input type="text" class="form-control" id="valuelat" name="valuelat" value="<?php if(isset($modelfield)){echo $dataValue[0];}else{echo $valueLat;}?>">
        </div>
        <div class="col-md-3">
                Latitude
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <div class="col-md-3">
            <input type="text" class="form-control" id="valuelng" name="valuelng" value="<?php if(isset($modelfield)){echo $dataValue[1];}else{echo $valueLng;}?>">
        </div>
        <div class="col-md-3">
                Longitude
        </div>
    </div>
</div>