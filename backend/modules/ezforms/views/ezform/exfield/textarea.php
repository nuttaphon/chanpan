<?php
use yii\helpers\Html;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<label>Textarea</label>
<textarea class="form-control" rows="8" disabled>Detail...</textarea>
<div class="row">
    <div class="col-md-6">
        <label>จำนวนแถว</label>
        <input type="number" name="field_row" id="field_row" class="form-control" value="<?php echo $modelField->ezf_field_rows ? $modelField->ezf_field_rows : '8'; ?>">
    </div>
    <div class="col-md-6">
        <input type="checkbox" name="field_text_html" id="field_text_html" value="1"  <?php echo $modelField->ezf_field_text_html ? 'checked' : null; ?>>
        <label>HTML Editor</label>
    </div>
</div>