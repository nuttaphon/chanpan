<?php

use yii\helpers\Html;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php $this->registerCssFile("/timepicker/css/bootstrap-timepicker.min.css"); ?>
<?php $this->registerJsFile("/timepicker/js/bootstrap-timepicker.min.js"); ?>
<label>เวลา</label>
<div class='bootstrap-timepicker'>
    <input id='timepicker1' class="form-control" type='text' class='input-small' readonly placeholder="__:__">
</div>
<?php
$this->registerJs("$('#timepicker1').timepicker({
                minuteStep: 5,
                showInputs: false,
                disableFocus: true,
                showMeridian: false
            });")
?>
