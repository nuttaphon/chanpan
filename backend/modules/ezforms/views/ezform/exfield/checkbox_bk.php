<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


?>

<?php if(isset($modelform)){?>
<?php $i=1;foreach($modelcheck as $check){ 

    ?>
    <div class='row' id="addFieldCheckbox">
        <div class="col-md-10 col-md-offset-2" style='padding:10px;' checkbox-id='<?php echo $check->ezf_field_id;?>'>
            <div class='col-md-2'>
            <input type='text' class='form-control' value='<?php echo $check->ezf_field_name?>' id='checkboxValue_<?php echo $i;?>' name="checkboxValue[]">
            </div>
            <div class='col-md-5'>
            <input type='text' class='form-control' value='<?php echo $check->ezf_field_label?>' id='checkboxLabel_<?php echo $i;?>' name="checkboxLabel[]">
            </div>
            <div class='col-md-5'>
            <i onclick="deleteCheckbox('<?php echo $check->ezf_field_id?>')" item-id='<?php echo $check->ezf_field_id?>'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>
            </div>
        </div>
    </div>
<?php $i++; }?>
    <?php if(isset($modelcheckEtc)){?>
    <div class='row' id='addFieldCheckbox'>
        
    </div>    
    <div class='row' id="addEtcFieldCheckbox">
        <div class='col--10 col-md-offset-2' style='padding:10px;' checkbox-etc-id='1'>
            <div class='col-md-2'>
                <input type='text' class='form-control' value='<?php echo $modelcheckEtc->ezf_feld_name?>' id='checkboxValueOther' name="checkboxEtc">
            </div>
            <div class='col-md-5'>
                <input type='text' class='form-control' value='คำตอบอื่นๆ' id='1' readonly>
            </div>
            <div class='col-md-5'>
                <i onclick='deleteCheckboxEtc("1")' item-id='"+numCheckbox+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>
            </div>
        </div>
    </div>
    <?php }?>
    <div class='row' id='addEtcFieldCheckbox'>
    </div>
<?php }else{?>
<div class="row"  id="inputCheckbox" >
    <div class="col-md-10 col-md-offset-2" style='padding:10px;' checkbox-id='1'>
        
        <div class='col-md-2'>
        <label>ตัวแปร</label>
        <input type='text' class='form-control' value='<?php echo $nameValue;?>_1' id='checkboxValue_1' name="checkboxEtc">
        </div>
        <div class='col-md-5'>
        <label>คำอธิบาย</label>
        <input type='text' class='form-control' value='ตัวเลือกที่ 1' id='checkboxLabel_1'>
        </div>
    </div>
</div>
<div class='row' id='addFieldCheckbox' >
</div>
<div class='row' id='addEtcFieldCheckbox'>
</div>
<?php }?>
<div class='row' id='buttonAddCheckbox'>
    <div class="col-md-10 col-md-offset-2" style='padding:10px;'>
        <div class='col-md-2'>
            <input type='text' class='form-control' value='+' id='addCheckbox' style='border:1px #eee;cursor:pointer;'readonly>
        </div>
        <div class='col-md-5'>
            <input type='text' class='form-control' value='คลิ้กเพื่อเพิ่มตัวเลือก' id='addCheckbox' style='border:1px #eee;cursor:pointer;' readonly>
        </div>
        <div class='col-md-4'>
            <p><span>หรือ </span><span style='color:#5656F5;cursor:pointer;' onclick='addCheckboxEtc();'>เพิ่ม "อื่นๆ"</span></p>
        </div>
    </div>
</div>
<script>
    $('.row').on('click','#addCheckbox',function(){
        var nameCheckbox = '<?php echo $nameValue;?>';
        var numCheckbox = $('div[checkbox-id]').length+1;
        var numCheckboxEtc = $('div[checkbox-etc-id]').length;
        var checkboxHtml  = "<div class='col-md-10 col-md-offset-2' style='padding:10px;' checkbox-id='"+numCheckbox+"'>";
       

         checkboxHtml += "<div class='col-md-2'>";
         checkboxHtml += "<input type='text' class='form-control' value='"+nameCheckbox+"_"+numCheckbox+"' id='checkboxValue_"+numCheckbox+"' name='checkboxValue[]'>";
         checkboxHtml += "</div>";
         checkboxHtml += "<div class='col-md-5'>";
         checkboxHtml += "<input type='text' class='form-control' value='ตัวเลือกที่ "+numCheckbox+"' id='checkboxLabel_"+numCheckbox+"' name='checkboxLabel[]'>";
         checkboxHtml += "</div>";
         checkboxHtml += "<div class='col-md-2'>";
         checkboxHtml += "<i onclick='deleteCheckbox("+numCheckbox+")' item-id='"+numCheckbox+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>";
         checkboxHtml += "</div>";
         checkboxHtml += "</div>";
//         if(numCheckboxEtc==0){
            $('#addFieldCheckbox').after().append(checkboxHtml);
//        }
    });
    function addCheckboxEtc(){
        var nameCheckbox = '<?php echo $nameValue;?>';
        var numCheckbox = $('div[checkbox-id]').length+1;
        var numCheckboxEtc = $('div[checkbox-etc-id]').length;
        var checkboxHtml  = "<div class='col-md-10 col-md-offset-2' style='padding:10px;' checkbox-etc-id='"+numCheckbox+"'>";
         checkboxHtml += "<div class='col-md-2'>";
         checkboxHtml += "<input type='text' class='form-control' value='"+nameCheckbox+"_"+numCheckbox+"_other' id='checkboxValueOther'  name='checkboxEtc'>";
         checkboxHtml += "</div>";
         checkboxHtml += "<div class='col-md-5'>";
         checkboxHtml += "<input type='text' class='form-control' value='คำตอบอื่นๆ' id='"+numCheckbox+"' readonly>";
         checkboxHtml += "</div>";
         checkboxHtml += "<div class='col-md-5'>";
         checkboxHtml += "<i onclick='deleteCheckboxEtc("+numCheckbox+")' item-id='"+numCheckbox+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>";
         checkboxHtml += "</div>";
         checkboxHtml += "</div>";
        if(numCheckboxEtc==0){
            $('#addEtcFieldCheckbox').append(checkboxHtml);
        }
	renderTap();
    }
    
    function deleteCheckbox(numCheckbox){
        $("div[checkbox-id="+numCheckbox+"]").remove();
	renderTap();
    };
    function deleteCheckboxEtc(numCheckbox){
        $("div[checkbox-etc-id="+numCheckbox+"]").remove();
        $("#buttonAddCheckbox").show();
	renderTap();
    };
</script>