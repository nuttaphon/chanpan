<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script>
    var numData = $('div[radio-id]').length;
    $('.row').on('click', '#addRadiox', function () {
        var nameRadio = '<?php echo $nameValue;?>';
        if(numData >=1){
            numData++;
        }
        var numRadio = numData;
        //var numRadioEtc = $('div[radio-etc-id]').length;

        var radioHtml = "<div class='col-md-10 col-md-offset-2' style='padding:10px;' radio-id='" + numRadio + "'>";
        radioHtml += "<div class='col-md-2'>";
        radioHtml += "<input type='text' class='form-control' value='" + numRadio + "' id='radioVar_" + numRadio + "' name='radioVar[]'>";
        radioHtml += "</div>";
        radioHtml += "<div class='col-md-3'>";
        radioHtml += "<input type='text' class='form-control' value='ตัวเลือกที่ " + numRadio + "' id='radioLabel_" + numRadio + "' name='radioVar[]'>";
        radioHtml += "</div>";

        radioHtml += "<div class='col-md-3'>";
        radioHtml += "<div id='divRadioText' radio-text-id='"+numRadio+"'>";
        radioHtml += "<span style='color:blue;cursor: pointer;font-style: oblique;' onclick=addRadioText('"+nameRadio+"','"+numRadio+"')>+ คำถามเพิ่มเติม</span>";
        radioHtml += "<input type='hidden' class='form-control' value='' id='radioVar[]' name='radioVar[]'/>";
        radioHtml += "</div>";
        radioHtml += "</div>";

        radioHtml += "<div class='col-md-2'>";
        radioHtml += "<i onclick='deleteRadio(" + numRadio + ")' item-id='" + numRadio + "'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>";
        radioHtml += "</div>";
        radioHtml += "</div>";
        $('#addFieldRadio').append(radioHtml);
//        if(numRadioEtc==0){
//            $('#addFieldRadio').append(radioHtml);
//        }
    });
    /*
     function addRadioEtc(){
     var numRadio = $('div[radio-id]').length+1;
     var numRadioEtc = $('div[radio-etc-id]').length;
     var radioHtml  = "<div class='col-md-10 col-md-offset-2' style='padding:10px;' radio-etc-id='"+numRadio+"'>";
     radioHtml += "<div class='col-md-2'>";
     radioHtml += "<input type='text' class='form-control' value='"+numRadio+"' name='radioVarOther' id='radioVarOther'>";
     radioHtml += "</div>";
     radioHtml += "<div class='col-md-5'>";
     radioHtml += "<input type='text' class='form-control' value='คำตอบอื่นๆ' id='radioVar_"+numRadio+"' readonly>";
     radioHtml += "</div>";
     radioHtml += "<div class='col-md-5'>";
     radioHtml += "<i onclick='deleteRadioEtc("+numRadio+")' item-id='"+numRadio+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>";
     radioHtml += "</div>";
     radioHtml += "</div>";
     if(numRadioEtc==0){
     $('#addFieldRadio').prepend().append(radioHtml);
     $("#buttonAddRadio").hide();
     }
     }*/
    function deleteRadio(numRadio) {
        $("div[radio-id=" + numRadio + "]").remove();
        renderTap('div[radio-id]');
    }
    function addRadioText(value,order, action){
        if(action == 'update'){
            var radioText = "<input type='text' class='form-control' id='radioText_"+order+"' name='radioVar[]' value='"+value+"' style='width:150px;'>";
            radioText += "<i class='fa fa-close' style='position:relative;left: 119px;top:-27px;cursor:pointer;color:#9F9F9F;' onClick=removeRadioText('"+value+"','"+order+"')></i>";
            $("#divRadioText[radio-text-id='"+order+"']").html(radioText);
        }else {
            var radioText = "<input type='text' class='form-control' id='radioText_" + order + "' name='radioVar[]' value='" + value + "_" + order + "_text' style='width:150px;'>";
            radioText += "<i class='fa fa-close' style='position:relative;left: 119px;top:-27px;cursor:pointer;color:#9F9F9F;' onClick=removeRadioText('" + value + "','" + order + "')></i>";
            $("#divRadioText[radio-text-id='" + order + "']").html(radioText);
        }
    }
    function removeRadioText(value,order){
        var buttonText = "<span style='color:blue;cursor: pointer;font-style: oblique;' onclick=addRadioText('"+value+"','"+order+"')>+ คำถามเพิ่มเติม</span>";
        buttonText += "<input type='hidden' class='form-control' value='' id='radioText_"+order+"' name='radioVar[]'/>";
        $("#divRadioText[radio-text-id='"+order+"']").html(buttonText);
    }
    /*
     function deleteRadioEtc(numRadio){
     $("div[radio-etc-id="+numRadio+"]").remove();
     $("#buttonAddRadio").show();
     }*/
</script>

<div class='row' id='radioLayout'>
  <div class="col-md-10 col-md-offset-2" style='padding: 10px;'>
      <label >การจัดเรียงของตัวเลือก </label>
      <div class='radio-inline'>
        <label>
          <input type="radio" name="ezf_field_default" id='ezf_field_default' value="1" checked> แนวตั้ง
        </label>
      </div>
      <div class='radio-inline'>
          <label>
            <input type="radio" name="ezf_field_default" id='ezf_field_default' value="2" > แนวนอน
          </label>
        </div>
      </div>
    </div>


<?php
//จากการสร้างปกติ (จะแสดงเมื่อ กด update)
if (isset($modelform)) { ?>
    <div class='row'>
        <div class="col-md-10 col-md-offset-2" style='padding:10px;' >
            <div class='col-md-2'>
                <label>ค่า</label>
            </div>
             <div class='col-md-3'>
                <label>ตัวเลือก</label>
            </div>
            <div class='col-md-3'>
                <label>คำถามเพิ่มเติม</label>
            </div>
        </div>
    </div>
    <?php $i=1; foreach ($modelradio as $radio) { ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-2" style='padding:10px;' radio-id='<?php echo $radio->ezf_choice_id ?>'>
                <div class='col-md-2'>
                    <!--<label>ค่า</label>-->
                    <input type='text' class='form-control' value='<?php echo $radio->ezf_choicevalue; ?>' id='radioVar_<?php echo $i; ?>' name='radioVar[]'>
                </div>

                <div class='col-md-3'>
                    <!--<label>ตัวเลือก</label>-->
                    <input type='text' class='form-control' value='<?php echo $radio->ezf_choicelabel; ?>' id='radioLabel_<?php echo $i; ?>' name='radioVar[]'>
                </div>
                <div class='col-md-3'>
                    <div id='divRadioText' radio-text-id='<?php echo $radio->ezf_choicevalue; ?>'>
                        <span style='color:blue;cursor: pointer;font-style: oblique;' onclick="addRadioText('<?php echo $nameValue; ?>','<?php echo $radio->ezf_choicevalue; ?>')">+ คำถามเพิ่มเติม</span>
                        <input type='hidden' class='form-control' value=''id=radioText_<?php echo $i; ?>' name='radioVar[]'/>
                    </div>
                </div>
                <div class='col-md-2'>
                    <i onclick="deleteRadio('<?php echo $radio->ezf_choice_id ?>')" item-id='"+numRadio+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>
                </div>
            </div>
        </div>
        <?php
        $etc = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_name')->where('ezf_field_ref = :ezf_field_ref', [':ezf_field_ref' => $radio->ezf_choice_id]);
        if($etc->count()){ $etc = $etc->one(); ?>
        <script>
           // $(function() {
                addRadioText('<?php echo $etc->ezf_field_name; ?>', '<?php echo $radio->ezf_choicevalue; ?>', 'update');
            //});
        </script>
        <?php } //end ?>

    <?php
        $i++;
    }
    //เมื่อมีการแปลงฟิลด์
} else if (isset($modelComponent)) {
    ?>
    <div class='row'>
        <div class="col-md-10 col-md-offset-2" style='padding:10px;' >
            <div class='col-md-2'>
                <label>ค่า</label>
            </div>
             <div class='col-md-3'>
                <label>ตัวเลือก</label>
            </div>
            <div class='col-md-3'>
                <label>คำถามเพิ่มเติม</label>
            </div>
        </div>
    </div>
    <?php
    if (count($modelComponent)) {
        foreach ($modelComponent AS $key => $val) {
            ?>
            <div class="col-md-10 col-md-offset-2" style='padding:10px;' radio-id='<?= $key + 1; ?>'>
                <div class='col-md-2'>
                    <!--<label>ค่า</label>-->
                    <input type='text' class='form-control' value='<?= $key + 1; ?>' id='radioVar_<?= $key + 1; ?>' name='radioVar[]'>
                </div>

                <div class='col-md-3'>
                    <!--<label>ตัวเลือก</label>-->
                    <input type='text' class='form-control' value='<?= $val; ?>' id='radioLabel_<?= $key + 1; ?>' name='radioVar[]'>
                    <input type='hidden' class='form-control' value='<?= $val; ?>' name='orglabel[]'>
                </div>
                <div class='col-md-3'>
                    <div id='divRadioText' radio-text-id='<?= $key + 1; ?>'>
                        <span style='color:blue;cursor: pointer;font-style: oblique;' onclick="addRadioText('<?php echo $nameValue; ?>','<?= $key + 1; ?>')">+ คำถามเพิ่มเติม</span>
                        <input type='hidden' class='form-control' value=''id=radioText_<?= $key + 1; ?>' name='radioVar[]'/>
                    </div>
                </div>
                <div class='col-md-2'>
                    <i onclick="deleteSelect('<?= $key + 1; ?>');" item-id='"+numRadio+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>
                </div>
            </div>

        <?php }
    } else { //ไม่มีข้อมูลสร้างปกติ ?>
        <div class="row"  id="inputRadio" >
            <div class="col-md-10 col-md-offset-2" style='padding:10px;' radio-id='1'>
                <div class='col-md-2'>
                    <label>ค่า</label>
                    <input type='text' class='form-control' value='1' id='radioVar_1' name='radioVar[]'>
                </div>

                <div class='col-md-3'>
                    <label>ตัวเลือก</label>
                    <input type='text' class='form-control' value='ตัวเลือกที่ 1' id='radioLabel_1' name='radioVar[]'>
                </div>

                <div class='col-md-3'>
                    <label>คำถามเพิ่มเติม</label>
                    <div id='divRadioText' radio-text-id='1'>
                        <span style='color:blue;cursor: pointer;font-style: oblique;' onclick="addRadioText('<?php echo $nameValue?>','1')">+ คำถามเพิ่มเติม</span>
                        <input type='hidden' class='form-control' value=''id=radioText_1' name='radioVar[]'/>
                    </div>
                </div>

            </div>
        </div>
    <?php } ?>
    <input type='hidden' class='form-control' value='1' name='transformfield'>
<?php } else { //กรณีสร้างปกติ ?>
    <div class="row"  id="inputRadio" >
        <div class="col-md-10 col-md-offset-2" style='padding:10px;' radio-id='1'>
            <div class='col-md-2'>
                <label>ค่า</label>
                <input type='text' class='form-control' value='1' id='radioVar_1' name='radioVar[]'>
            </div>
            <div class='col-md-3'>
                <label>ตัวเลือก</label>
                <input type='text' class='form-control' value='ตัวเลือกที่ 1' id='radioLabel_1' name='radioVar[]'>
            </div>
            <div class='col-md-3'>
                <label>คำถามเพิ่มเติม</label>
                <div id='divRadioText' radio-text-id='1'>
                    <span style='color:blue;cursor: pointer;font-style: oblique;' onclick="addRadioText('<?php echo $nameValue?>','1')">+ คำถามเพิ่มเติม</span>
                    <input type='hidden' class='form-control' value=''id=radioText_1' name='radioVar[]'/>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class='row' id='addFieldRadio' ></div>
<div class='row' id='buttonAddRadio'>
    <div class="col-md-10 col-md-offset-2" style='padding:10px;'>
        <div class='col-md-2'>
            <input type='text' class='form-control' value='+' id='addRadiox' style='border:1px #eee;cursor:pointer;'readonly>
        </div>
        <div class='col-md-3'>
            <input type='text' class='form-control' value='คลิ้กเพื่อเพิ่มตัวเลือก' id='addRadiox' style='border:1px #eee;cursor:pointer;' readonly>
        </div>

<!--                <div class='col-md-5'>
                    <p><span>หรือ </span><span style='color:#5656F5;cursor:pointer;' onclick='addRadioEtc();'>เพิ่ม "อื่นๆ"</span></p>
                </div>-->
    </div>
</div>
