<style>
    .deleteIcon{
        
        position: absolute;
        bottom: 22px;
        right: 5px;
        color: red;
        font-size: 20px;

    }
</style>
<?php 
    if(isset($modelfield)){
        $dataValue = array();
        $i=0;
        foreach($modelfield as $key=>$value){
            $dataValue[$i] = $value->ezf_field_name;
            $i++;
        }
        
    }
        $valueProvince = $dataValue[0] ? $dataValue[0] :  $nameValue.'_province';
        $valueAmphur = $dataValue[1] ? $dataValue[1] :  $nameValue.'_amphur';
        $valueTumbon = $dataValue[2] ? $dataValue[2] :  $nameValue.'_tumbon';
        $nameValue = $nameValue;

?>
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <div class="checkbox checkbox-primary">
            <input type="checkbox" id="checkbox" name="checkbox" value="1" <?php if(isset($dataValue[2])){echo 'checked';}else{echo '';}?>>
            <label for="checkbox">ตำบล</label>
        </div>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <div class="col-md-3">
            <label>ตัวแปร<label>
        </div>
        <div class="col-md-3">
            <label>ตัวเลือก<label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <div class="col-md-3">
            <input type="text" class="form-control" id="valueprovince" name="valueprovince" value="<?php echo $valueProvince;?>">
        </div>
        <div class="col-md-3">
                <select class="form-control" id="formprovince" readonly disabled="disabled">
                    <option>- เลือกจังหวัด -</option>
                </select>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <div class="col-md-3">
            <input type="text" class="form-control" id="valueamphur" name="valueamphur" value="<?php echo $valueAmphur;?>">
        </div>
        <div class="col-md-3">
                <select class="form-control" id="formanphur" readonly disabled="disabled">
                    <option>- เลือกอำเภอ -</option>
                </select>
        </div>
    </div>
</div>
<br>

<div class="row" id="divTumbon">
    <div class="col-md-10 col-md-offset-2" id="formtumbon">
        <div class="col-md-3">
            <input type="text" class="form-control" id="valuetumbon" name="valuetumbon" value="<?php echo $valueTumbon;?>">
        </div>
        <div class="col-md-3">
                <select class="form-control"  disabled="disabled" readonly>
                    <option>- เลือกตำบล -</option>
                </select>
        </div>
    </div>
</div>
<br>

<script>
    $(function(){
        if($('#checkbox').is(':checked')){
            $('#divTumbon').show();
        }else{
            $('#divTumbon').hide();
        }
        $("#checkbox").change(function(){
            var valueDefault = '<?php echo $nameValue?>';
            if($("#checkbox").is(":checked")){
                $("#divTumbon").show();
                $("#valuetumbon").val(valueDefault+"_tumbon");
            }else{
                $("#valuetumbon").val(0);
                $("#divTumbon").hide();
            }
        });
    });
</script>