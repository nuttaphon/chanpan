<?php

use yii\helpers\Html;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<label>วันที่</label>
<div class="row">
    <div class="col-md-4">
        <input class='input-medium form-control' type='text' data-provide='datepicker' data-date-language='th-th' placeholder="__/__/_____" readonly>
    </div>
</div>


<?php
$this->registerJs("$('.datepicker').datepicker()");
?>
