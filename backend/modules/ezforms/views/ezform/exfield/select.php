<?php

use yii\helpers\Html;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php if (isset($modelform)) { ?>
    <div class='row'>
        <div class="col-md-10 col-md-offset-2" style='padding:10px;' >
            <div class='col-md-2'>
                <label>ค่า</label>
            </div>
             <div class='col-md-3'>
                <label>ตัวเลือก</label>
            </div>
        </div>
    </div>
    <?php $i=1; foreach ($modelselect as $select) { ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-2" style='padding:10px;' radio-id='<?php echo $select->ezf_choice_id; ?>'>
                <div class='col-md-2'>
                    <!--<label>ค่า</label>-->
                    <input type='text' class='form-control' value='<?php echo $select->ezf_choicevalue; ?>' id='radioValue_<?php echo $i; ?>' name='selectValue[]'>
                </div>
                <div class='col-md-3'>
                    <!--<label>ตัวเลือก</label>-->
                    <input type='text' class='form-control' value='<?php echo $select->ezf_choicelabel; ?>' id='radioLabel_<?php echo $i; ?>' name='selectValue[]'>
                </div>
                <div class='col-md-5'>
                    <i onclick='deleteSelect(<?php echo $select->ezf_choice_id; ?>)' item-id='"+numRadio+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>
                </div>
            </div>
        </div>
    <?php
        $i++;
    }
} else if (isset($modelComponent)) {
    ?>
    <div class='row'>
        <div class="col-md-10 col-md-offset-2" style='padding:10px;' >
            <div class='col-md-2'>
                <label>ค่า</label>
            </div>
             <div class='col-md-3'>
                <label>ตัวเลือก</label>
            </div>
        </div>
    </div>
    <?Php
    if (count($modelComponent)) {
        foreach ($modelComponent AS $key => $val) {
            ?>
            <div class="col-md-10 col-md-offset-2" style='padding:10px;' radio-id='<?= $key + 1; ?>'>
                <div class='col-md-2'>
                    <input type='text' class='form-control' value='<?= $key + 1; ?>' id='radioValue_<?= $key + 1; ?>' name='selectValue[]'>
                </div>
                <div class='col-md-3'>
                    <input type='text' class='form-control' value='<?= $val; ?>' id='radioLabel_<?= $key + 1; ?>' name='selectValue[]'>
                    <input type='hidden' class='form-control' value='<?= $val; ?>' name='orglabel[]'>
                </div>
                <div class='col-md-5'>
                    <i onclick="deleteSelect('<?= $key + 1; ?>');" item-id='"+numRadio+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>
                </div>
            </div>
        <?php }
    } else { ?>
        <div class="row"  id="inputRadio" >
            <div class="col-md-10 col-md-offset-2" style='padding:10px;' radio-id='1'>
                <div class='col-md-2'>
                    <label>ค่า</label>
                    <input type='text' class='form-control' value='1' id='radioValue_1' name='selectValue[]'>
                </div>
                <div class='col-md-3'>
                    <label>ตัวเลือก</label>
                    <input type='text' class='form-control' value='ตัวเลือกที่ 1' id='radioLabel_1' name='selectValue[]'>
                </div>
            </div>
        </div>
    <?php } ?>
    <input type='hidden' class='form-control' value='1' name='transformfield'>
<?php } else { ?>
    <div class="row"  id="inputRadio" >
        <div class="col-md-10 col-md-offset-2" style='padding:10px;' radio-id='1'>
            <div class='col-md-2'>
                <label>ค่า</label>
                <input type='text' class='form-control' value='1' id='radioValue_1' name='selectValue[]'>
            </div>
            <div class='col-md-3'>
                <label>ตัวเลือก</label>
                <input type='text' class='form-control' value='ตัวเลือกที่ 1' id='radioLabel_1' name='selectValue[]'>
            </div>
        </div>
    </div>

<?php } ?>
<div class='row' id='addFieldRadio' ></div>
<div class='row' id='buttonAddRadio'>
    <div class="col-md-10 col-md-offset-2" style='padding:10px;'>
        <div class='col-md-2'>
            <input type='text' class='form-control' value='+' id='addRadio' style='border:1px #eee;cursor:pointer;'readonly>
        </div>
        <div class='col-md-3'>
            <input type='text' class='form-control' value='คลิ้กเพื่อเพิ่มตัวเลือก' id='addRadio' style='border:1px #eee;cursor:pointer;' readonly>
        </div>

    </div>
</div>
<script>
    var numData = '<?php echo ($i >=1) ? $i : '2';?>';
    $('.row').on('click', '#addRadio', function () {
        var numRadio = numData++;

        //var numRadioEtc = $('div[radio-etc-id]').length;
        var radioHtml = "<div class='col-md-10 col-md-offset-2' style='padding:10px;' radio-id='" + numRadio + "'>";
        radioHtml += "<div class='col-md-2'>";
        radioHtml += "<input type='text' class='form-control' value='" + numRadio + "' id='radioValue_" + numRadio + "' name='selectValue[]'>";
        radioHtml += "</div>";
        radioHtml += "<div class='col-md-3'>";
        radioHtml += "<input type='text' class='form-control' value='ตัวเลือกที่ " + numRadio + "' id='radioLabel_" + numRadio + "' name='selectValue[]'>";
        radioHtml += "</div>";
        radioHtml += "<div class='col-md-5'>";
        radioHtml += "<i onclick='deleteSelect(" + numRadio + ")' item-id='" + numRadio + "'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>";
        radioHtml += "</div>";
        radioHtml += "</div>";
        //if(numRadioEtc==0){
        $('#addFieldRadio').append(radioHtml);
        //}
    });
//    function addRadioEtc(){
//        var numRadio = $('div[radio-id]').length+1;
//        var numRadioEtc = $('div[radio-etc-id]').length;
//        var radioHtml  = "<div class='col-md-10 col-md-offset-2' style='padding:10px;' radio-etc-id='"+numRadio+"'>";
//         radioHtml += "<div class='col-md-2'>";
//         radioHtml += "<input type='text' class='form-control' value='value"+numRadio+"_other' id='radioValueOther'>";
//         radioHtml += "</div>";
//         radioHtml += "<div class='col-md-5'>";
//         radioHtml += "<input type='text' class='form-control' value='คำตอบอื่นๆ' id='radioValue_"+numRadio+"' readonly>";
//         radioHtml += "</div>";
//         radioHtml += "<div class='col-md-5'>";
//         radioHtml += "<i onclick='deleteSelectEtc("+numRadio+")' item-id='"+numRadio+"'class='fa fa-close' style='color:red;font-size:25px;cursor:pointer;'></i>";
//         radioHtml += "</div>";
//         radioHtml += "</div>";
//        if(numRadioEtc==0){
//            $('#addFieldRadio').prepend().append(radioHtml);
//            $("#buttonAddRadio").hide();
//        }
//    }
    function deleteSelect(numRadio) {
        $("div[radio-id=" + numRadio + "]").remove();
	renderTap('div[radio-id]');
    }
    ;
//    function deleteSelectEtc(numRadio){
//        $("div[radio-etc-id="+numRadio+"]").remove();
//        $("#buttonAddRadio").show();
//    };
</script>