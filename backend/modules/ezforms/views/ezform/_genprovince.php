<?php 
    use yii\helpers\Html;
?>
    <div class='col-md-4'>
        <label>จังหวัด</label>
        <select class='form-control' data-placeholder="- จังหวัด -" 
                data-id="province_<?php echo $modelfield->ezf_field_id?>"
                id='<?php echo $modelfield->ezf_field_province?>' 
                name='<?php echo $modelfield->ezf_field_province?>' >
            <option></option>
        <?php foreach($dataProvince as $province){?>
            <?php echo $data[$modelfield->ezf_field_province];?>
            <?php if($data[$modelfield->ezf_field_province]==$province["PROVINCE_CODE"]){?>
            <?php $select="selected"?>
            <?php }else{
                $select="";
            }?>
            <option value='<?php echo $province["PROVINCE_CODE"]?>'<?Php echo $select;?>><?php echo $province["PROVINCE_NAME"];?></option>
        <?php } ?>
        </select>
    </div>
    <div class='col-md-4'>
        <label>อำเภอ</label>
        <select class='form-control' data-placeholder="- อำเภอ -" 
                data-id="amphur_<?php echo $modelfield->ezf_field_id?>"
                id='<?php echo $modelfield->ezf_field_amphur?>' 
                name='<?php echo $modelfield->ezf_field_amphur?>' >
            <?php 
                if($data[$modelfield->ezf_field_amphur]!=""){
                    $sqlAmphur = "SELECT * FROM `const_amphur` WHERE `AMPHUR_CODE` LIKE '".$data[$modelfield->ezf_field_province]."%'";
                    $dataAmphur = Yii::$app->db->createCommand($sqlAmphur)->queryAll();
                    foreach($dataAmphur as $amphur){
                        if($data[$modelfield->ezf_field_amphur]==$amphur["AMPHUR_CODE"]){
                            $select2 = "selected";
                        }else{
                            $select2 = "";
                        }
                        ?>
                <option value="<?php echo $amphur["AMPHUR_CODE"]?>" <?php echo $select2;?>><?php echo $amphur["AMPHUR_NAME"]?></option>
            <?php
                    }
                }
            ?>
        </select>
    </div>
<?php if(isset($modelfield->ezf_field_tumbon)){?>
    <div class='col-md-4'>
        <label>ตำบล</label>
        <select class='form-control' data-placeholder="- ตำบล -" 
                data-id="tumbon_<?php echo $modelfield->ezf_field_id?>"
                id='<?php echo $modelfield->ezf_field_tumbon?>' 
                name='<?php echo $modelfield->ezf_field_tumbon?>' >
            <?php 
                if($data[$modelfield->ezf_field_amphur]!=""){
                    $sqlTumbon = "SELECT * FROM `const_district` WHERE `DISTRICT_CODE` LIKE '".$data[$modelfield->ezf_field_amphur]."%'";
                    $dataTumbon = Yii::$app->db->createCommand($sqlTumbon)->queryAll();
                    foreach($dataTumbon as $tumbon){
                        if($data[$modelfield->ezf_field_tumbon]==$tumbon["DISTRICT_CODE"]){
                            $select3 = "selected";
                        }else{
                            $select3 = "";
                        }
                        ?>
                <option value="<?php echo $tumbon["DISTRICT_CODE"]?>" <?php echo $select3;?>><?php echo $tumbon["DISTRICT_NAME"]?></option>
            <?php
                    }
                }
            ?>
        </select>
    </div>
<?php }?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type='text/javascript'>
        var province = '#<?php echo $modelfield->ezf_field_province?>';
        var amphur = '#<?php echo $modelfield->ezf_field_amphur?>';
        var tumbon = '#<?php echo $modelfield->ezf_field_tumbon?>';
        $(province).select2();
        $(amphur).select2();
        $(tumbon).select2();
        $(province).on("change",function(){
           var provincecode = $(this).val();
           $("[data-id=amphur_<?Php echo $modelfield->ezf_field_id;?>]").html('<option></option>');
           $("[data-id=tumbon_<?Php echo $modelfield->ezf_field_id;?>]").html('<option></option>');      
           $.post(<?php echo yii\helpers\Url::to('/ezforms/')?>'genprovince/amphur',{provincecode:provincecode},function(result){
               $.each(result,function(key,value){
                   $("[data-id=amphur_<?Php echo $modelfield->ezf_field_id;?>]").append("<option value='"+value.AMPHUR_CODE+"'>"+value.AMPHUR_NAME+"</option>");
               })
           });
        });
        $(amphur).on("change",function(){
           var amphurcode = $(this).val();
           $("[data-id=tumbon_<?Php echo $modelfield->ezf_field_id;?>]").html('<option></option>');
           $.post(<?php echo yii\helpers\Url::to('/ezforms/')?>'genprovince/tumbon',{amphurcode:amphurcode},function(result){
               $.each(result,function(key,value){
                   $("[data-id=tumbon_<?Php echo $modelfield->ezf_field_id;?>]").append("<option value='"+value.DISTRICT_CODE+"'>"+value.DISTRICT_NAME+"</option>");
               })
           });
        });
</script>
