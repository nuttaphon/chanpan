

<div class='col-md-<?php echo $modelfield->ezf_field_lenght;?>'>
    <label><?php echo $modelfield->ezf_field_label;?></label>
    <select     class='form-control' 
                data-placeholder="- เลือกโรงพยาบาล -" 
                data-id="hospital_<?php echo $modelfield->ezf_field_id?>"
                id='<?php echo $modelfield->ezf_field_name?>' 
                name='<?php echo $modelfield->ezf_field_name?>' >
    <option></option>
    </select>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script>
    var hospital = '#<?php echo $modelfield->ezf_field_name?>';
    $(hospital).select2({
        ajax: {
        url: <?php echo Url::to('/ezforms/')?>"hospital/query",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params.term, // search term
            page: params.page
          };
        },
        processResults: function (data, page) {
          // parse the results into the format expected by Select2.
          // since we are using custom formatting functions we do not need to
          // alter the remote JSON data
//          console.log(data);
          return {
            results: data.items
          };
        },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
      minimumInputLength: 1,
    //  templateResult: formatRepo, // omitted for brevity, see the source of this page
    //  templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    
</script>
