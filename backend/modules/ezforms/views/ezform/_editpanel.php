<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use backend\modules\ezforms\components\EzformQuery;
?>
<div id='editPanel' style='display:none;'>
</div>
<div id='showPanel' >
    <div id='addPanel' >
        <?php
        $form2 = ActiveForm::begin([
                    'id' => $model2->formName(),
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => 'col-sm-offset-2',
                            'wrapper' => 'col-sm-10'
                        ]
                    ],
                    'layout' => 'horizontal'
        ]);
        ?>
        <?php
        if (isset($modelselect) || isset($modelradio)) {
            if (count($modelselect) || count($modelradio)) echo "<input type='hidden' class='form-control' value='1' id='num_modelselect'>";
        }else{
            echo "<input type='hidden' class='form-control' value='1' id='num_modelselect'>";
        }
	?>
        <?= Html::activeHiddenInput($model2, 'ezf_id'); ?>
        <?= Html::activeHiddenInput($model2, 'ezf_field_id'); ?>

        <?= $form2->field($model2, 'ezf_field_label')->textInput(['maxlength' => true]) ?>
	<?= $form2->field($model2, 'ezf_field_color')->widget(kartik\widgets\ColorInput::className(),[
	    'options' => ['readonly' => true]
	]) ?>
	<?= $form2->field($model2, 'ezf_field_icon')->checkbox()?>

        <?php echo $form2->field($model2, 'ezf_field_help')->widget(vova07\imperavi\Widget::className(), [
                'settings' => [
                    'lang' => 'th',
                    'minHeight' => 100,
                    //'imageManagerJson' => Url::to(['ezform/images-get']),
                    'imageUpload' => Url::to(['ezform/image-upload']),
                    'plugins' => [
                        'fullscreen',
                        'imagemanager'
                    ]
                ]
            ]);
        echo $form2->field($model2, 'ezf_field_val')->checkbox(['value' => '99'])->label('แสดงข้อความช่วยเหลือ');
        $fieldOption = json_decode($model2->ezf_field_options);
        ?>
        <div class="form-group field-ezformfields-ezf_field_options">
            <div class="col-sm-10 col-sm-offset-2">
                <div class="checkbox">
                    <label for="ezformfields-ezf_field_options">
                        <input type="checkbox" id="ezformfields-ezf_field_options" name="ezf_field_options[remember_field]" value="1" <?php echo $fieldOption->remember_field ? 'checked' : null ?>>
                        Remember Fields
                    </label>
                </div>
                <div class="help-block help-block-error"></div>
            </div>
        </div>
        <?= $form2->field($model2, 'ezf_field_lenght')->dropDownList(['3' => '25 %', '6' => '50 %', '9' => '75%','12'=>'100%']); ?>
        <?php
            if($model2->ezf_field_type!=19 && $model2->ezf_field_type!=13){
        ?>
        <?= $form2->field($model2, 'ezf_field_name')->textInput(['maxlength' => true, 'style' => 'width:150px;']) ?>
        <?php
            }
        ?>

        <?php
        if($model2->ezf_field_type!=19 && $model2->ezf_field_type!=13 && $model2->ezf_field_type!=18 && $model2->ezf_field_type!=23 && $model2->ezf_field_type!=25){
        echo
        $form2->field($model2, 'ezf_field_type')->dropDownList(ArrayHelper::map(EzformQuery::getInputAllEdit(), 'input_id', 'input_name'));
        }else{
            echo "<input type='hidden' name='nameValue' id='nameValue' value='".$nameValue."' >";
            echo Html::activeHiddenInput($model2, 'ezf_field_type');
        }
        ?>
	<?php
	if($model2->ezf_field_type==5 || $model2->ezf_field_type==19 || $model2->ezf_field_type==16 || $model2->ezf_field_type==23){

	} else {
	    echo $form2->field($model2, 'ezf_field_required')->checkbox();
	}
	?>
        <div class='well' id='showExample'>
            <?php if ($model2->ezf_field_type == 1) { ?>
                <label>คำถาม</label>
                <input type='text' class='form-control' value='คำตอบ' id='exText' name='exText' disabled='disabled'>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 2) { ?>
                <?= $this->render('exfield/heading') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 3) {
                $modelField = \backend\modules\ezforms\models\EzformFields::find()->select('ezf_field_text_html, ezf_field_rows')->where(['ezf_field_id'=>$_GET['id']])->one();
                ?>
                <?= $this->render('exfield/textarea', ['modelField'=>$modelField]) ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 4) { ?>
                <?=
                $this->render('exfield/radio', ['modelform' => $model2,
                    'modelradio' => $modelradio,
                    'nameValue' => $nameValue])
                ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 19) { ?>
                <?=
                $this->render('exfield/checkbox', ['modelform' => $model2,
                    'modelcheck' => $modelcheck,
                    'nameValue' => $nameValue,
                ])
                ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 6) { ?>
                <?=
                $this->render('exfield/select', ['modelform' => $model2,
                    'modelselect' => $modelselect,
                    'nameValue' => $nameValue])
                ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 7) { ?>
                <?= $this->render('exfield/date') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 8) { ?>
                <?= $this->render('exfield/time') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 10) { ?>
                <?=
                $this->render('exfield/component', [
                    'ezformComponent_maps' => $ezformComponent_maps
                ])
                ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 11) { ?>
                <?= $this->render('exfield/snomed') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 12) { ?>
                <?= $this->render('exfield/personal') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 13) { ?>
                <?= $this->render('exfield/province',['model2'=>$model2,'modelfield'=>$modelfield,'nameValue'=>$nameValue]) ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 14) { ?>
                <?= $this->render('exfield/fileinput') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 15) { ?>
                <?= $this->render('exfield/question') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 16) { ?>
                <?= $this->render('exfield/scheckbox') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 17) { ?>
                <?= $this->render('exfield/hospital') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 18) { ?>
                <?= $this->render('exfield/reffield_update') ?>
            <?php } ?>
            <?php if($model2->ezf_field_type == 23){?>
                <?= $this->render('exfield/scale.php',['model2'=>$model2,'modelquestion'=>$modelquestion,'modelchoice'=>$modelchoice,'nameValue'=>$nameValue])?>
            <?php }?>
            <?php if($model2->ezf_field_type == 24){?>
                <?= $this->render('exfield/drawing.php',['model2'=>$model2,'modelquestion'=>$modelquestion,'modelchoice'=>$modelchoice,'nameValue'=>$nameValue])?>
            <?php }?>
	    <?php if($model2->ezf_field_type == 30){?>
                <?= $this->render('exfield/audiorec.php',['model2'=>$model2,'modelquestion'=>$modelquestion,'modelchoice'=>$modelchoice,'nameValue'=>$nameValue])?>
            <?php }?>
            <?php if($model2->ezf_field_type == 25){ ?>
                <?= $this->render('exfield/grid.php',['model2'=>$model2,'choice'=>$choice,'question'=>$question,'nameValue'=>$nameValue])?>
            <?php }?>
	    <?php if($model2->ezf_field_type == 26){ ?>
                <?= $this->render('exfield/map.php',['model2'=>$model2,'modelfield'=>$modelfield,'nameValue'=>$nameValue])?>
            <?php }?>
            <?php if ($model2->ezf_field_type == 27) { ?>
                <?= $this->render('exfield/icd9') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 28) { ?>
                <?= $this->render('exfield/icd10') ?>
            <?php } ?>
            <?php if ($model2->ezf_field_type == 31) { ?>
                <?= $this->render('exfield/ref43field_update') ?>
            <?php } ?>                
        </div>
        <input type='hidden' class='form-control' name='fieldMainType' id='fieldMainType' value='<?php echo $model2->ezf_field_type?>'>
        <input type='hidden' class='form-control' name='fieldMainId' id='fieldMainId' value='<?php echo $model2->ezf_field_id?>'>
        <input type='hidden' class='form-control' name='fieldMainName' id='fieldMainName' value='<?php echo $model2->ezf_field_id?>'>
		<?= Html::hiddenInput('conditionFields', '', ['id'=>'conditionFields'])?>

        <br>
        <div class='row'>
            <div class='col-lg-12'>
                <p style='cursor:pointer' id='btnSetting'><i class='fa fa-cog'></i>  แสดงการตั่งค่าขั้นสูง</p>
            </div>
        </div>
        <div id='advanceSetting' style='padding:20px;background-color: #f5f5f5;border:1px solid #e3e3e3;border-radius: 4px;'>

	    <br>
		<div id="drawingOption" class="row disabledDisplay">
		<div class="col-sm-12">
		    <a class="fileUpload btn btn-success" >อัพโหลดรูปพื้นหลัง
			<?= $fileUpload = \dosamigos\fileupload\FileUpload::widget([
				'id' => 'option-bg-id',
				'name' => 'option-bg',
				'url' => Url::to(['//ezforms/drawing/option-image', 'name'=>'test111']),
				'plus' => true,
				'options' => ['accept' => 'image/*', 'class'=>'upload'],
				'clientOptions' => [
				'maxFileSize' => 3000000
				],
				// Also, you can specify jQuery-File-Upload events
				// see: https://github.com/blueimp/jQuery-File-Upload/wiki/Options#processing-callback-options
				'clientEvents' => [
				'fileuploaddone' => "function(e, data) {
							var bgsize = 'auto auto';
							if(data.result.files.width > data.result.files.height){
								bgsize = '300px auto';
							} else {
								bgsize = 'auto 200px';
							}
							$('input[name=\"option-bg\"]').attr('data-url', data.result.files.newurl);
							$('input[name=\"option-bg\"]').fileupload({'maxFileSize':3000000,'url':$('input[name=\"option-bg\"]').attr('data-url')});

							$('#ezformfields-ezf_field_default').val(data.result.files.name);

							$('#showImg').css('background-image', 'url('+data.result.files.url+')');
							$('#showImg').css('background-size', bgsize);
							$('#showImg').css('background-position', 'center center');
							$('#showImg').css('background-repeat', 'no-repeat');
							}",
				'fileuploadfail' => "function(e, data) {
							console.log(e);
							console.log(data);
							}",
				],
			]); ?>
			</a>
			<?= $form2->field($model2, 'ezf_field_default')->hiddenInput()->label(false) ?>
			<?= Html::checkbox('allow_bg', ($model2->ezf_field_options!=''), ['label'=> 'อนุญาติให้เปลี่ยนรูปพื้นหลัง']); ?>
			<div id="showImg"></div>
		</div>
	    </div>
	    <br>
	    <div class="row">
		<div class="col-sm-12">
		    <div id="conditionBox">

		    </div>
		</div>
	    </div>
        </div>
        <span id="errorAddField"></span><br>
<?= Html::submitButton($model2->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'แก้ไขคำถาม'), ['class' => $model2->isNewRecord ? 'btn btn-primary' : 'btn btn-primary'], ['id' => 'editQuestionButton']) ?>
<?php ActiveForm::end(); ?>
    </div>
</div>
<?Php
if($model2->ezf_field_default!='' AND $model2->ezf_field_default != 1 AND $model2->ezf_field_default != 2){
	list($width, $height, $type, $attr) = getimagesize(Yii::$app->basePath . '/../storage/web/drawing/bg/'.$model2->ezf_field_default);

	$this->registerJs("
				var bgsize = 'auto auto';
				if({$width} > {$height}){
				bgsize = '300px auto';
				} else {
				bgsize = 'auto 200px';
				}
				$('#showImg').css('background-image', 'url(".Url::to(Yii::getAlias('@storageUrl').'/drawing/bg/'.$model2->ezf_field_default).")');
				$('#showImg').css('background-size', bgsize);
				$('#showImg').css('background-position', 'center center');
				$('#showImg').css('background-repeat', 'no-repeat');
			");
}


$this->registerJS("
    $('#advanceSetting').hide();
    $('#btnSetting').on('click',function(){
        $('#advanceSetting').toggle();
    });

    newLoad = true;
    if($('#ezformfields-ezf_field_type').val()==19){
	renderTap('div[checkbox-id]');
    } else if($('#ezformfields-ezf_field_type').val()==4 || $('#ezformfields-ezf_field_type').val()==6){
	renderTap('div[radio-id]');
    } else if($('#ezformfields-ezf_field_type').val()==16){
	renderTap('div.col-md-10.col-md-offset-2');
    } else {
	$('#conditionBox').html('');
    }
    if($('#ezformfields-ezf_field_type').val() == 24){
		$('#drawingOption').removeClass('disabledDisplay');
	}

    $('#ezformfields-ezf_field_type').on('change',function(){
        var idValue = $(this).val();
	if(idValue == 5){
            idValue = 19;
        }

		if(idValue == 24){
			$('#drawingOption').removeClass('disabledDisplay');
		} else {
			$('#ezformfields-ezf_field_default').val('');
			$('#drawingOption').addClass('disabledDisplay');
		}

        var labelValue = $('#ezformfields-ezf_field_type :selected').html();
        var nameValue = $('#ezformfields-ezf_field_name').val();
        var ezf_field_id = $('#ezformfields-ezf_field_id').val();
        var url = '".Url::to(['ezform/renderfield','idtype'=>''])."'+idValue+'&labelvalue='+labelValue+'&nameValue='+nameValue+'&ezf_field_id='+ezf_field_id;
        var num_modelselect = parseInt($('#num_modelselect').val());
        //var num_modelradio = parseInt($('#num_modelradio').val());
        //num_modelselect = 0 คือไม่มี choice ใดๆ
        if(idValue == '6' && !num_modelselect){
            var ezf_id = $('#ezformfields-ezf_id').val();
            var ezf_field_id = $('#ezformfields-ezf_field_id').val();
            url = '".Url::to(['select/transformfield','ezf_id'=>''])."'+ezf_id+'&ezf_field_id='+ezf_field_id;
        }
        else if(idValue == '4' && !num_modelselect){
            var ezf_id = $('#ezformfields-ezf_id').val();
            var ezf_field_id = $('#ezformfields-ezf_field_id').val();
            url =  '".Url::to(['radio/transformfield','ezf_id'=>''])."'+ezf_id+'&ezf_field_id='+ezf_field_id;
        }
        $.post(
		url
	    ).done(function(result){
		$('#showExample').html(result);
		newLoad = true;
		if(idValue==19){
		    renderTap('div[checkbox-id]');
		} else if(idValue==4 || idValue==6){
		    renderTap('div[radio-id]');
		} else if(idValue==16){
		    renderTap('div.col-md-10.col-md-offset-2');
		} else {
		    $('#conditionBox').html('');
		}
	    }).fail(function(){
		console.log('server error');
	    });
        " . SDNoty::show('labelValue', '"success"') . "
    });
    $('form#{$model2->formName()}').on('beforeSubmit', function(e){
//        $('button[type=submit]').attr('disabled','disabled');
//        $('button[type=submit]').html('Generating ...');
        var ezf_field_type = $('#ezformfields-ezf_field_type').val();
        var ezf_id = $('#ezformfields-ezf_field_id').val();
        var field_row = $('#field_row').val();
        var form = $(this);
        $.blockUI({
            message : 'กำลังแก้ไขคำถาม ...',
            css: {
               border: 'none',
               padding: '15px',
               backgroundColor: '#000',
               '-webkit-border-radius': '10px',
               '-moz-border-radius': '10px',
               opacity: .5,
               color: '#fff',
               'z-index': '3000'
            }});
        if(ezf_field_type==1){
            updateTextField(ezf_id,form);
        }else if(ezf_field_type==2){
            updateHeadField(ezf_id,form);
        }else if(ezf_field_type==3){
            updateTextAreaField(ezf_id, form, field_row);
        }else if(ezf_field_type==4){
            var ezf_field_id = $('#ezformfields-ezf_field_id').val();
            ezf_id = $('#ezformfields-ezf_id').val();
            updateRadioField(ezf_id, ezf_field_id, form);
        }else if(ezf_field_type==19){
            updateCheckboxField(ezf_id,form);
        }else if(ezf_field_type==6){
            var ezf_field_id = $('#ezformfields-ezf_field_id').val();
            ezf_id = $('#ezformfields-ezf_id').val();
            updateSelectField(ezf_id, ezf_field_id, form);
        }else if(ezf_field_type==7){
            updateDateField(ezf_id,form);
        }else if(ezf_field_type==8){
            updateTimeField(ezf_id,form);
        }else if(ezf_field_type==9){
            updateDateTimeField(ezf_id,form);
        } else if (ezf_field_type == 10) {
            updateComponentField(ezf_id, form);
        }
        else if (ezf_field_type == 11) {
            updateSnomedField(ezf_id, form);
        }else if (ezf_field_type == 12) {
            updatePersonalField(ezf_id, form);
        }
        else if (ezf_field_type == 13) {
            updateProvinceField(ezf_id, form);
        }
        else if (ezf_field_type == 14) {
            updateFileinputField(ezf_id, form);
        }
        else if (ezf_field_type == 15) {
            updateQuestionField(ezf_id, form);
        }
        else if (ezf_field_type == 16) {
            updateScheckboxField(ezf_id, form);
        }
         else if (ezf_field_type == 17) {
            updateHospitalField(ezf_id, form);
        }else if (ezf_field_type == 23){

            updateScaleField(ezf_id,form);
        }else if (ezf_field_type == 24){

            updateDrawingField(ezf_id,form);
        }else if (ezf_field_type == 30){

            updateAudioField(ezf_id,form);
        }else if(ezf_field_type == 25){
            updateGridField(ezf_id,form);
        }else if(ezf_field_type == 26){
            updateMapField(ezf_id,form);
        }
        else if(ezf_field_type == 27){
            updateIcd9Field(ezf_id,form);
        }
        else if(ezf_field_type == 28){
            updateIcd10Field(ezf_id,form);
        }
        return false;
    });



    ")
?>
