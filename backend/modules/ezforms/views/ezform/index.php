<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use common\lib\codeerror\helpers\CheckOwn;
use backend\models\Dynamic;
use backend\modules\managedata\classes\ManagerQuery;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ezforms\models\EzformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'EzForm');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-index">

    <div class="sdbox-header">

    </div>
    <?php
    $items = [
        [
	    'label' => 'ฟอร์มในหน้าบันทึกข้อมูลของฉัน',
	    'url' => Url::to(['/ezforms/ezform/index', 'tab' => 4]),
	    'active' => $tab == 4,
	],
	[
	    'label' => 'ฟอร์มที่ฉันสร้าง',
	    'url' => Url::to(['/ezforms/ezform/index', 'tab' => 1]),
	    'active' => $tab == 1,
	],
	[
	    'label' => 'ฟอร์มสาธารณะ',
	    'url' => Url::to(['/ezforms/ezform/index', 'tab' => 2]),
	    'active' => $tab == 2,
	],
	[
	    'label' => 'ฟอร์มที่ฉันมีส่วนร่วมสร้าง',
	    'url' => Url::to(['/ezforms/ezform/index', 'tab' => 3]),
	    'active' => $tab == 3,
	],
	
	[
	    'label' => 'ฟอร์มที่ถูกลบ',
	    'url' => Url::to(['/ezforms/ezform/index', 'tab' => 5]),
	    'active' => $tab == 5,
	],
	[
	    'label' => 'ฟอร์มจาก TDC',
	    'url' => Url::to(['/ezforms/ezform/index', 'tab' => 6]),
	    'active' => $tab == 6,
	],        
    ];
    ?>

    <?=
    \yii\bootstrap\Nav::widget([
	'items' => $items,
	'options' => ['class' => 'nav nav-tabs'],
    ]);
    ?>
    <?php Pjax::begin(['id' => 'ezform-grid-pjax', 'timeout' => 5000]); ?>
    <div class="nav-tabs-custom">

	<div class="tab-content">
	    <div class="tab-pane active" >
		<?php
		if ($tab == 2) {
		    echo SDGridView::widget([
			'id' => 'ezform-grid2',
			//'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span> Add'), ['data-url'=>Url::to(['ezform/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform']),
			'dataProvider' => $dataProvider,
			'columns' => [
			    ['class' => 'yii\grid\SerialColumn'],
				[
					'label' => 'ชื่อฟอร์ม',
					'attribute' => 'ezf_name',
					'format' => 'text',
					'contentOptions'=>['style'=>'min-width:100; text-align: left;'],
				],
				[
					'label' => 'ชื่อฟอร์ม',
					'attribute' => 'ezf_name',
					'format' => 'text',
					'contentOptions'=>['style'=>'min-width:120; text-align: left;'],
				],
				[
					'attribute' => 'username',
					'contentOptions'=>['style'=>'min-width:50; text-align: center;'],
					'label' => Yii::t('username', 'ผู้สร้าง'),
				],
				[
					'attribute' => 'create_date',
					'label' => 'สร้างเมื่อ',
					'contentOptions'=>['style'=>'min-width:30px; text-align: center;'],
					'format' => ['date', 'php:d/m/Y'],
				],

			    [
				'label' => 'จำนวนข้อมูล',
				'attribute' => 'count_data',
				'format' => 'raw',
				'value' => function ($data) {
				    $dynamic = new Dynamic();

				    $dynamic->setTableName($data->ezf_table);

				    try {
					$conut = $dynamic->find()->count();
				    } catch (yii\db\Exception $e) {
					$conut = 0;
				    }
				    return $conut;
				}
			    ],
				/*
			    [
				'header' => 'Submitted',
				//'format'=>'raw',
				'value' => function ($data) {
				    $dynamic = new Dynamic();

				    $dynamic->setTableName($data->ezf_table);

				    try {
					$conut = $dynamic->find()->where('rstat=4')->count();
				    } catch (yii\db\Exception $e) {
					$conut = 0;
				    }

				    return $conut;
				}
			    ],
				*/
			    ['class' => 'common\lib\sdii\widgets\SDActionColumn', 'template' => '{view}', 'contentOptions' => ['style' => 'width:200px;'],
				'buttons' => [
				    'view' => function ($url, $model) {
					$url = '/ezforms/ezform/viewonline?id=' . $model->ezf_id;
					return Html::a('<span class="glyphicon glyphicon-eye-open"></span> ดูฟอร์ม', $url, [
						    'data-action' => 'update',
						    'title' => Yii::t('yii', 'View Online'),
						    'class' => 'btn btn-primary btn-xs',
						    'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
					]);
				    }],
				    ],
				],
			    ]);
			} elseif ($tab == 3) {
			    echo SDGridView::widget([
				'id' => 'ezform-grid',
				//'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span> Add'), ['data-url'=>Url::to(['ezform/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform']),
				'dataProvider' => $dataProvider,
				'columns' => [
				    ['class' => 'yii\grid\SerialColumn'],
					[
						'label' => 'ชื่อฟอร์ม',
						'attribute' => 'ezf_name',
						'format' => 'text',
						'contentOptions'=>['style'=>'min-width:100; text-align: left;'],
					],
					[
						'label' => 'ชื่อฟอร์ม',
						'attribute' => 'ezf_name',
						'format' => 'text',
						'contentOptions'=>['style'=>'min-width:120; text-align: left;'],
					],
					[
						'attribute' => 'username',
						'contentOptions'=>['style'=>'min-width:50; text-align: center;'],
						'label' => Yii::t('username', 'ผู้สร้าง'),
					],
					[
						'attribute' => 'create_date',
						'label' => 'สร้างเมื่อ',
						'contentOptions'=>['style'=>'min-width:30px; text-align: center;'],
						'format' => ['date', 'php:d/m/Y'],
					],
				    [
					'label' => 'จำนวนข้อมูล',
					'attribute' => 'count_data',
					'format' => 'raw',
					'value' => function ($data) {
					    $dynamic = new Dynamic();

					    $dynamic->setTableName($data->ezf_table);

					    try {
						$conut = $dynamic->find()->count();
					    } catch (yii\db\Exception $e) {
						$conut = 0;
					    }
					    return $conut;
					}
				    ],
					/*
				    [
					'header' => 'Submitted',
					//'format'=>'raw',
					'value' => function ($data) {
					    $dynamic = new Dynamic();

					    $dynamic->setTableName($data->ezf_table);

					    try {
						$conut = $dynamic->find()->where('rstat=4')->count();
					    } catch (yii\db\Exception $e) {
						$conut = 0;
					    }

					    return $conut;
					}
				    ],
					*/
				    [
					'class' => 'common\lib\sdii\widgets\SDActionColumn',
					'template' => '{update}',
					'buttons' => [
					    'update' => function ($url, $model) {
						//if (0 < CheckOwn::checkOwnForm($model->ezf_id)) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span> จัดการฟอร์ม', $url, [
							    'data-action' => 'update',
							    'title' => Yii::t('yii', 'Update'),
							    'class' => 'btn btn-warning btn-xs',
							    'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
						]);
						//}
					    }
						]
					    ],
					],
				    ]);
				} elseif ($tab == 4) {
                                    ?>
                <div style="margin-bottom: 10px;">
                                        <?php
                                    echo Html::button('<i class="glyphicon glyphicon-star"></i> คลิกที่นี่เพื่อเลือกฟอร์มอื่นๆ', ['id'=>'add-form-fav', 'class'=>'btn btn-warning']) . '</div> ';
				    echo SDGridView::widget([
					'id' => 'ezform-grid',
					//'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span> Add'), ['data-url'=>Url::to(['ezform/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform']),
					'dataProvider' => $dataProvider,
					'columns' => [
					    ['class' => 'yii\grid\SerialColumn'],
						[
							'label' => 'ชื่อฟอร์ม',
							'attribute' => 'ezf_name',
							'format' => 'text',
							'contentOptions'=>['style'=>'min-width:100; text-align: left;'],
						],
						[
							'label' => 'ชื่อฟอร์ม',
							'attribute' => 'ezf_name',
							'format' => 'text',
							'contentOptions'=>['style'=>'min-width:120; text-align: left;'],
						],
						[
							'attribute' => 'username',
							'contentOptions'=>['style'=>'min-width:50; text-align: center;'],
							'label' => Yii::t('username', 'ผู้สร้าง'),
						],
						[
							'attribute' => 'create_date',
							'label' => 'สร้างเมื่อ',
							'contentOptions'=>['style'=>'min-width:30px; text-align: center;'],
							'format' => ['date', 'php:d/m/Y'],
						],
					    [
						'label' => 'จำนวนข้อมูล',
						'attribute' => 'count_data',
						'format' => 'raw',
						'value' => function ($data) {
						    $dynamic = new Dynamic();

						    $dynamic->setTableName($data->ezf_table);

						    try {
							$conut = $dynamic->find()->count();
						    } catch (yii\db\Exception $e) {
							$conut = 0;
						    }
						    return $conut;
						}
					    ],
						/*
					    [
						'header' => 'Submitted',
						//'format'=>'raw',
						'value' => function ($data) {
						    $dynamic = new Dynamic();

						    $dynamic->setTableName($data->ezf_table);

						    try {
							$conut = $dynamic->find()->where('rstat=4')->count();
						    } catch (yii\db\Exception $e) {
							$conut = 0;
						    }

						    return $conut;
						}
					    ],
						*/
					    ['class' => 'common\lib\sdii\widgets\SDActionColumn', 'template' => '{view}', 'contentOptions' => ['style' => 'width:200px;'],
						'buttons' => [
						    'view' => function ($url, $model) {
//                                if (0 < CheckOwn::checkOwnForm($model->ezf_id)) {
							return Html::a('<span class="glyphicon glyphicon-eye-open"></span> บันทึกข้อมูล', $url, [
								    'data-action' => '/inputdata/step2?comp_id_target=100000&ezf_id=' . $model->ezf_id,
								    'title' => Yii::t('yii', 'View'),
								    'class' => 'btn btn-info btn-xs',
								    'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
							]);
//                                }
						    }],
						    ],
						],
					    ]);
					} elseif ($tab == 5) {
					    echo SDGridView::widget([
						'id' => 'ezform-grid',
						//'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span> Add'), ['data-url'=>Url::to(['ezform/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform']),
						'dataProvider' => $dataProvider,
						'columns' => [
						    ['class' => 'yii\grid\SerialColumn'],
							[
								'label' => 'ชื่อฟอร์ม',
								'attribute' => 'ezf_name',
								'format' => 'text',
								'contentOptions'=>['style'=>'min-width:100; text-align: left;'],
							],
							[
								'label' => 'ชื่อฟอร์ม',
								'attribute' => 'ezf_name',
								'format' => 'text',
								'contentOptions'=>['style'=>'min-width:120; text-align: left;'],
							],
							[
								'attribute' => 'username',
								'contentOptions'=>['style'=>'min-width:50; text-align: center;'],
								'label' => Yii::t('username', 'ผู้สร้าง'),
							],
							[
								'attribute' => 'create_date',
								'label' => 'สร้างเมื่อ',
								'contentOptions'=>['style'=>'min-width:30px; text-align: center;'],
								'format' => ['date', 'php:d/m/Y'],
							],
						    [
							'label' => 'จำนวนข้อมูล',
							'attribute' => 'count_data',
							'format' => 'raw',
							'value' => function ($data) {
							    $dynamic = new Dynamic();

							    $dynamic->setTableName($data->ezf_table);

							    try {
								$conut = $dynamic->find()->count();
							    } catch (yii\db\Exception $e) {
								$conut = 0;
							    }
							    return $conut;
							}
						    ],
							/*
						    [
							'header' => 'Submitted',
							//'format'=>'raw',
							'value' => function ($data) {
							    $dynamic = new Dynamic();

							    $dynamic->setTableName($data->ezf_table);

							    try {
								$conut = $dynamic->find()->where('rstat=4')->count();
							    } catch (yii\db\Exception $e) {
								$conut = 0;
							    }

							    return $conut;
							}
						    ],
							*/
						    ['class' => 'common\lib\sdii\widgets\SDActionColumn', 'template' => '{undo} {delete}', 'contentOptions' => ['style' => 'width:200px;']],
						],
					    ]);
					} elseif ($tab == 6) {
					    echo SDGridView::widget([
                                                'id' => 'managedata-grid',
                                                'panelBtn' => Html::a('<span class="glyphicon glyphicon-tint"></span> Purify', 'http://tools.cascap.in.th/purify/download/index.php', [
                                                                    'title' => Yii::t('yii', 'Purification Tools'),
                                                                    'target'=>'_blank',
                                                                    'class'=>'btn btn-warning btn-xs',
                                                                ]),
                                                'dataProvider' => $dataProvider,
                                                //  'filterModel' => $searchModel,
                                                'columns' => [
                                                    [
                                                        'class' => 'yii\grid\SerialColumn',
                                                        'headerOptions' => ['style'=>'text-align: center;'],
                                                        'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
                                                    ],

                                                    //'ezf_id',
                                                    'ezf_name',
                                                    //'ezf_detail:ntext',
                                                    //'ezf_table',
                                                    [
                                                        'label'=>'ข้อมูลทั้งหมด',
                                                        'header'=>'ข้อมูลทั้งหมด',
                                                        //'format'=>'raw',
                                                        'value' => function ($data)
                                                        {
                                                            try {
                                                                //$sitecode = Yii::$app->user->identity->userProfile->sitecode;
                                                                $conut = ManagerQuery::getDataCountAll($data['ezf_id']);
                                                            } catch (yii\db\Exception $e) {
                                                                $conut = 0;
                                                            }
                                                            return number_format($conut);
                                                        }
                                                    ],
                                                    [
                                                        'label'=>'เฉพาะไซต์ตัวเอง',
                                                        'header'=>'เฉพาะไซต์ตัวเอง',
                                                        //'format'=>'raw',
                                                        'value' => function ($data)
                                                        {

                                                            try {
                                                                $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                                                                $table = ManagerQuery::getMapTable($data['ezf_id']);

                                                                $conut = ManagerQuery::getDataCountSite($sitecode, $table);
                                                            } catch (yii\db\Exception $e) {
                                                                $conut = 0;
                                                            }
                                                            return number_format($conut);
                                                        }
                                                    ],
                                                    //'user_create',
                                                    // 'create_date',
                                                    // 'user_update',
                                                    // 'update_date',
                                                    // 'status',
                                                    // 'shared',
                                                    // 'public_listview',
                                                    // 'public_edit',
                                                    // 'public_delete',

                                                    [
                                                        'class' => 'common\lib\sdii\widgets\SDActionColumn' ,
                                                        'template' => '{export} {edat} {annotated} {datadic}',
                                                        'contentOptions' => ['style'=>'width:380px;'],
                                                        'buttons' => [
                                                            'export' => function ($url, $model){
                                                                if (0 < CheckOwn::checkOwnForm($model['ezf_id'])) {
                                                                    $table = ManagerQuery::getMapTable($model['ezf_id']);
                                                                    return Html::a('<span class="glyphicon glyphicon-share-alt"></span> View/Export', ['/managedata/managedata/view-tdc', 'id' => $model['ezf_id'], 'table'=>$table], [
                                                                        'data-action' => 'viewexport',
                                                                        'title' => Yii::t('yii', 'View/Export'),
                                                                        'class'=>'btn btn-primary btn-xs',
                                                                        //'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
                                                                    ]);
                                                                }
                                                            },

                                                            'edat' => function ($url, $model){

                                                                return Html::a('<span class="glyphicon glyphicon-tasks"></span> EDAT', ['/edat', 'id' => $model['ezf_id'], 'table'=>$model['ezf_table']], [
                                                                    'data-action' => 'edat',
                                                                    'title' => Yii::t('yii', 'Exploratory Data Analysis Tools'),
                                                                    'class'=>'btn btn-success btn-xs',
                                                                    'url' => ['/managedata/tb-data1'],
                                                                ]);

                                                            },
                                                            'annotated' => function ($url, $model){

                                                                return Html::a('<span class="glyphicon glyphicon-book"></span> Annotated CRF', ['/managedata/annotated', 'ezf_id' => $model['ezf_id'],], [
                                                                    'data-action' => 'anotated',
                                                                    'title' => Yii::t('yii', 'Annotated case report form'),
                                                                    'class'=>'btn btn-danger btn-xs',
                                                                ]);

                                                            },
                                                            'datadic' => function ($url, $model){

                                                                return Html::a('<span class="glyphicon glyphicon-book"></span> Dictionary', ['/managedata/dictionary', 'ezf_id' => $model['ezf_id']], [
                                                                    'data-action' => 'datadict',
                                                                    'title' => Yii::t('yii', 'Data Dictionary'),
                                                                    'class'=>'btn btn-info btn-xs',
                                                                ]);

                                                            }


                                                        ]
                                                    ],
                                                ],
                                            ]); 
					} else {
					    echo SDGridView::widget([
						'id' => 'ezform-managedata-grid',
						'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span> เพิ่มฟอร์มใหม่'), ['data-url' => Url::to(['ezform/create']), 'class' => 'btn btn-success btn-sm', 'id' => 'modal-addbtn-ezform']),
						'dataProvider' => $dataProvider,
						'columns' => [
						    ['class' => 'yii\grid\SerialColumn'],
							[
								'label' => 'ชื่อฟอร์ม',
								'attribute' => 'ezf_name',
								'format' => 'text',
								'contentOptions'=>['style'=>'min-width:100; text-align: left;'],
							],
							[
								'label' => 'ชื่อฟอร์ม',
								'attribute' => 'ezf_name',
								'format' => 'text',
								'contentOptions'=>['style'=>'min-width:120; text-align: left;'],
							],
							[
								'attribute' => 'username',
								'contentOptions'=>['style'=>'min-width:50; text-align: center;'],
								'label' => Yii::t('username', 'ผู้สร้าง'),
							],
							[
								'attribute' => 'create_date',
								'label' => 'สร้างเมื่อ',
								'contentOptions'=>['style'=>'min-width:30px; text-align: center;'],
								'format' => ['date', 'php:d/m/Y'],
							],
						    [
							'label' => 'จำนวนข้อมูล',
							'attribute' => 'count_data',
							'format' => 'raw',
							'value' => function ($data) {
							    $dynamic = new Dynamic();

							    $dynamic->setTableName($data->ezf_table);

							    try {
								$conut = $dynamic->find()->count();
							    } catch (yii\db\Exception $e) {
								$conut = 0;
							    }
							    return $conut;
							}
						    ],
							/*
						    [
							'header' => 'Submitted',
							//'format'=>'raw',
							'value' => function ($data) {
							    $dynamic = new Dynamic();

							    $dynamic->setTableName($data->ezf_table);

							    try {
								$conut = $dynamic->find()->where('rstat=4')->count();
							    } catch (yii\db\Exception $e) {
								$conut = 0;
							    }

							    return $conut;
							}
						    ],
							*/
						    [
							'class' => 'common\lib\sdii\widgets\SDActionColumn',
							'template' => '{update} {delete}',
							'buttons' => [
							    'update' => function ($url, $model) {
								if (0 < CheckOwn::checkOwnForm($model->ezf_id)) {
								    $url = '/ezforms/ezform/update?id=' . $model->ezf_id;
								    if ($model->ezf_version == 'v2') {
									$url = '/ezforms2/ezform/update?id=' . $model->ezf_id;
								    }

								    return Html::a('<span class="glyphicon glyphicon-pencil"></span> จัดการฟอร์ม', $url, [
										'data-action' => 'update',
										'title' => Yii::t('yii', 'Update'),
										'class' => 'btn btn-warning btn-xs',
										'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
								    ]);
								}
							    },
								    'delete' => function ($url, $model) {
								if (0 < CheckOwn::checkOwnForm($model->ezf_id)) {

								    return Html::a('<span class="glyphicon glyphicon-trash"></span> ลบ', $url, [
										'data-action' => 'delete',
										'title' => Yii::t('yii', 'Delete'),
										'class' => 'btn btn-danger btn-xs',
										'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
										'data-method' => 'post',
										'data-pjax' => isset($this->pjax_id) ? $this->pjax_id : '0',
								    ]);
								}
							    }
								]
							    ],
							],
						    ]);
						}
						?>

				            </div><!-- /.tab-pane -->

					</div><!-- /.tab-content -->
				    </div>


						<?php Pjax::end(); ?>

				</div>
						<?=
						SDModalForm::widget([
						    'id' => 'modal-ezform',
						    'size' => 'modal-lg',
						]);
						?>

				<?php $this->registerJs("
                    $('#ezform-grid-pjax').on('click', '#modal-addbtn-ezform', function(){
                        modalEzform($(this).attr('data-url'));
                    });

$('#ezform-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    //modalEzform('" . Url::to(['ezform/update', 'id' => '']) . "'+id);
});

$('#ezform-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');
    console.log(url);
    if(action == 'view'){
       modalEzform(url);
   }else if(action == 'viewexport'){
    location.href=url;
   }else if(action == 'edat'){
    location.href=url;
   }else if(action == 'anotated'){
    location.href=url;
   }else if(action == 'datadict'){
    location.href=url;
   }else if(action == 'update'){
    location.href=url;
}
else if(action === 'delete') {
	yii.confirm('" . Yii::t('app', 'คุณต้องการลบฟอร์มใช้หรือไม่?') . "', function(){
       $.post(
          url
          ).done(function(result){
              if(result.status == 'success'){
                  " . SDNoty::show('result.message', 'result.status') . "
                  $.pjax.reload({container:'#ezform-grid-pjax'});
              } else {
                  " . SDNoty::show('result.message', 'result.status') . "
              }
          }).fail(function(){
              console.log('server error');
          });
})
} else if (action === 'undo') {
    yii.confirm('" . Yii::t('app', 'คุณต้องการนำฟอร์มมาใช้ใหม่หรือไม่?') . "', function(){
        $.post(
            url
            ).done(function(result){
                if(result.status == 'success'){
                    " . SDNoty::show('result.message', 'result.status') . "
                    $.pjax.reload({container:'#ezform-grid-pjax'});
                } else {
                    " . SDNoty::show('result.message', 'result.status') . "
                }
            }).fail(function(){
                console.log('server error');
            });
})
}
return false;
});

$('#add-form-fav').on('click', function(){
    modalEzform('/inputdata/favorite-form');
    $('#modal-ezform').on('hidden.bs.modal', function () {
        // do something…
        //runBlockUI('รอสักครู่กำลังจัดรายการฟอร์มใหม่...');
        location.reload(true);
    })
});

function modalEzform(url) {
    $('#modal-ezform .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ezform').modal('show')
    .find('.modal-content')
    .load(url);
}

"); ?>
