<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms\models\Ezform */

$this->title = Yii::t('backend', 'Create Ezform');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-create">

    <?= $this->render('_form', [
        'model' => $model,
        'ezformComponents' => $ezformComponents
    ]) ?>

</div>
