<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;
use common\lib\ezform\components\helpers\GenForm;
use kartik\widgets\Select2;
use kartik\widgets\InputWidget;
use yii\widgets\MaskedInput;
use backend\modules\ezforms\components\GenViewForm;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = Yii::t('backend', 'ฟอร์ม ', [
        'modelClass' => 'Ezform',
    ]) . ':: ' . $modelform->ezf_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezforms'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model1->ezf_name, 'url' => ['view', 'id' => $model1->ezf_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'ดูฟอร์มออนไลน์');

$this->registerCssFile('/css/ezform.css');
//$this->registerCssFile('/checkbox/demo/build.css');
?>
<div class="ezform-update" >
    <br>
    <div style='float:right'>
        <?php
        $url = $_GET['action']=='inputdata' ? '/inputdata': '/ezforms/ezform/';
        echo Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;'.($_GET['action']=='inputdata' ? 'กลับไปหน้าบันทึกข้อมูล' : 'กลับไปหน้าเลือกฟอร์ม'), $url, ['class' => 'btn btn-warning btn-flat']); ?>
    </div>
    <br>
    <br>
    <div class="box">
        <div class="box-body">
            <div class='row'>
                <div class='col-lg-12'>
                    <h2><?= $modelform->ezf_name;?></h2>
                </div>
            </div>
            <div class='row'>
                <div class='col-lg-12'>
                    <h4><?= $modelform->ezf_detail;?></h4>
                </div>
            </div>


            <hr>
            <div class="row dad" id="formPanel">
                <?php $form = backend\modules\ezforms\components\EzActiveForm::begin(['action'=>'/ezforms/ezform/savedata', 'options' => ['enctype'=>'multipart/form-data']]); ?>
                <input type='hidden' name='ezf_id' id='ezf_id' value='<?php echo $modelform->ezf_id;?>'>
                <input type='hidden' name='id' id='id' value=''>
                <?php
                foreach($modelfield as $value){
                    $target='';
                    echo \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $value, $form);
                }

                foreach($modelfield as $value){
                    $inputId = Html::getInputId($model_gen, $value['ezf_field_name']);
                    $inputValue = Html::getAttributeValue($model_gen, $value['ezf_field_name']);

                    $dataCond = backend\modules\ezforms\components\EzformQuery::getCondition($value['ezf_id'], $value['ezf_field_name']);
                    if($dataCond){
                        //Edit Html
                        $fieldId = Html::getInputId($model_gen, $value['ezf_field_name']);
                        if($value['ezf_field_type']==4){
                            $fieldId = $value['ezf_field_name'];
                        }
                        foreach ($dataCond as $index => $cvalue) {
                            $jumpArr = explode(',', $cvalue['cond_jump']);
                            foreach ($jumpArr as $j => $jvalue) {
                                $this->registerJs("
			    var fieldIdj = '".$jvalue."';
			    var inputIdj = '".$fieldId."';
			    var valueIdj = '".$inputValue."';
			    var fixValuej = '".$cvalue['ezf_field_value']."';
			    var fTypej = '".$value['ezf_field_type']."';
			    domHtml(fieldIdj, inputIdj, valueIdj, fixValuej, fTypej, 'block');
			");
                            }

                            $requireArr = explode(',', $cvalue['cond_require']);
                            foreach ($requireArr as $r => $rvalue) {
                                $this->registerJs("
			    var fieldIdr = '".$rvalue."';
			    var inputIdr = '".$fieldId."';
			    var valueIdr = '".$inputValue."';
			    var fixValuer = '".$cvalue['ezf_field_value']."';
			    var fTyper = '".$value['ezf_field_type']."';
			    domHtml(fieldIdr, inputIdr, valueIdr, fixValuer, fTyper, 'none');
			");
                            }
                        }
                        //Add Event
                        if($value['ezf_field_type']==20 || $value['ezf_field_type']==0 || $value['ezf_field_type']==16){
                            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputId = '".$inputId."';
		    eventCheckBox(inputId, dataCond);
		");
                        }else if($value['ezf_field_type']==6){
                            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputId = '".$inputId."';
		    eventSelect(inputId, dataCond);
		");
                        } else if($value['ezf_field_type']==4){
                            $this->registerJs("
		    var dataCond = '".yii\helpers\Json::encode($dataCond)."';
		    var inputName = '".$value['ezf_field_name']."';
		    eventRadio(inputName, dataCond);
		");
                        }

                    }
                }
                backend\assets\EzfGenAsset::register($this);
                ?>
                <div class='col-lg-12'>
                    <p style='color:green;font-size: 15px;'><?php echo $message;?></p>
                </div>
                <?php backend\modules\ezforms\components\EzActiveForm::end();?>
            </div
        </div>
    </div>


