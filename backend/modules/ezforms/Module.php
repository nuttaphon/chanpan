<?php

namespace backend\modules\ezforms;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ezforms\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
