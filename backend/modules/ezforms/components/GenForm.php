<?php

namespace backend\modules\ezforms\components;

use Yii;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\models\EzformFields;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use kartik\widgets\FileInput;
use backend\modules\component\models\EzformComponent;
use backend\modules\ezforms\models\EzformField;
use backend\modules\ezforms\models\Ezform;
use yii\helpers\ArrayHelper;

class GenForm
{
    public static function getTypeEform($model2){
        if($model2->ezf_field_type == 18){
            //เก็บ temp ไว้
            $ezf_field_order = $model2->ezf_field_order;
            $ezf_field_id = $model2->ezf_field_id;
            //
            $model2 = EzformFields::find()->where(['ezf_field_id' => $model2->ezf_field_ref_field])->one();
            $model2->ezf_field_order = $ezf_field_order;
            $model2->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
        }

        if ($model2->ezf_field_type == 0) {
           return self::typeHiddenfield($model2);
        }
        if($model2->ezf_field_type==1){
           return self::typeText($model2);
        }
        if($model2->ezf_field_type==2){
           return self::typeHeading($model2);
        }
        if($model2->ezf_field_type==3){
           return self::typeTextArea($model2);
        }
         if($model2->ezf_field_type==4){
           return self::typeRadioOnload($model2);
        }
        if($model2->ezf_field_type==5){
           return self::typeCheckboxOnload($model2);
        }
        if($model2->ezf_field_type==6){
           return self::typeSelectOnload($model2);
        }
        if($model2->ezf_field_type==7){
           return self::typeDate($model2);
        }
        if($model2->ezf_field_type==9){
           return self::typeDateTime($model2);
        }
        if($model2->ezf_field_type==8){
           return self::typeTime($model2);
        }
        if ($model2->ezf_field_type == 10) {
           return self::typeComponent($model2);
        }
        if ($model2->ezf_field_type == 12) {
           return self::typePersonal($model2);
        }
        if ($model2->ezf_field_type == 11) {
           return self::typeSnomed($model2);
        }
        if ($model2->ezf_field_type == 14) {
           return self::typeFileinput($model2);
        }
        if($model2->ezf_field_type == 13){
           return self::typeProvince($model2);
        }
        if($model2->ezf_field_type == 15){
           return self::typeQuestion($model2);
        }
        if($model2->ezf_field_type == 16){
           return self::typeScheckbox($model2);
        }
        if($model2->ezf_field_type == 17){
           return self::typeHospital($model2);
        }
        if($model2->ezf_field_type == 27){
           return self::typeIcd9($model2);
        }
        if($model2->ezf_field_type == 28){
           return self::typeIcd10($model2);
        }
    }

    public static function saveText($model2){
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeText($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }

    public static function saveHeading($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeHeading($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }

    public static function saveTextArea($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeTextArea($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }

    public static function saveDateTime($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeDateTime($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }

    public static function saveComponent($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeComponent($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }

    public static function typeText($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . "<input type='text' class='form-control'>"
        . "</div>"
        . "</div>";
    }

    public static function typeHeading($model2)
    {

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-12' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . "<h3>$model2->ezf_field_label<h3><hr>"
        . "</div>"
        . "</div>";
    }

    public static function typeTextArea($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;


        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . "<textarea class='form-control' rows='$model2->ezf_field_rows'></textarea>"
        . "</div>"
        . "</div>";
    }
    public static function typeDateTime($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . "<input type='text' class='form-control' placeholder='dd/mm/YYYY 00:00'>"
        . "</div>"
        . "</div>";
    }

    public static function typeComponent($model2)
    {

        $ezformComponents = EzformComponent::find()
                                ->where(['comp_id' => $model2->ezf_component])
                                ->one();

        /**  Get field key name. */
        $field_key = EzformFields::find()
                            ->where(['ezf_field_id' => $ezformComponents->field_id_key])
                            ->one();

        /** Get field desc name. */
        if (!empty($ezformComponents->field_id_desc)) {
            $field_desc_array = explode(',', $ezformComponents->field_id_desc);
        }


        $field_desc_list = '';

        foreach ($field_desc_array as $value) {

            if (!empty($value)) {
                $field_desc = EzformFields::find()
                                ->where(['ezf_field_id' => $value])
                                ->one();

                $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
            }

        }

        $field_desc_list = substr($field_desc_list, 0 ,strlen($field_desc_list)-6);

        /** Get table name. */
        $ezform = Ezform::find()
                    ->where(['ezf_id' => $ezformComponents->ezf_id])
                    ->one();

        $dynamic_datas = ArrayHelper::map(Yii::$app->db->createCommand('SELECT *, CONCAT('.$field_desc_list.') AS field_desc FROM '.$ezform->ezf_table)
                            ->queryAll(), $field_key->ezf_field_name, 'field_desc');


        //\yii\helpers\VarDumper::dump($dynamic_datas, 10, true);

        $select = '<select class="form-control js-sin">';

        foreach ($dynamic_datas as $key => $dynamic_data) {
            $select .= "<option value='".$key."'>".$dynamic_data."</option>";
        }

        $select .= '</select>';

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-12' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . "<label>".$model2->ezf_field_label."</label>"
        .$select
        . "</div>"
        . "</div>";
    }

        //---------------
    public static function saveSelect($model2){
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeSelectOnsave($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function typeRadioOnsave($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }
        $sql = "SELECT * FROM `ezform_fields` WHERE `ezf_field_id`='".$model2->ezf_field_id."';";
        $dataField= \Yii::$app->db->createCommand($sql)->queryAll();

        $formradio = $label;

        $ezf_choice_id = GenMillisecTime::getMillisecTime();

        //selectValue
        $strlenght = count($_POST['radioValue']);

        for($i=0;$i<$strlenght;$i++){
        $ezf_choice_id = GenMillisecTime::getMillisecTime();
        $ezf_choicevalue = $_POST['radioValue'][$i]; $i++;
        $ezf_choicelabel = $_POST['radioValue'][$i];
        $sql = "INSERT INTO `ezform_choice` (`ezf_choice_id`, `ezf_field_id`, `ezf_choicevalue`, `ezf_choicelabel`) "
                . "VALUES ('$ezf_choice_id', '$model2->ezf_field_id', '$ezf_choicevalue', '$ezf_choicelabel');";
        $dataField = Yii::$app->db->createCommand($sql)->execute();
        }

        $sql = " SELECT count(*) as total  FROM `ezform_choice` WHERE ezf_field_id='".($model2->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();


        $strlenght = $dataField[0]['total'];
        $sql = " SELECT ezf_choicevalue, ezf_choicelabel FROM `ezform_choice` WHERE ezf_field_id='".($model2->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        for($i=0;$i<$strlenght;$i++){
            $formradio .= "<div class='radio'><label><input type='radio' name='".$model2->ezf_field_id."' value='".$dataField[$i]['ezf_choicevalue']."'>".$dataField[$i]['ezf_choicelabel']."</label></div>";
        }
        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $formradio
        . "</div>"
        . "</div>";
    }
    public static function typeRadioOnload($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }
        //$sql = "SELECT * FROM `ezform_fields` WHERE `ezf_field_id`='".$model2->ezf_field_id."';";
        //$dataField= \Yii::$app->db->createCommand($sql)->queryAll();

        $formradio = $label;
        /*
        $sql = " SELECT ezf_choicevalue, ezf_choicelabel FROM `ezform_choice` WHERE ezf_field_id='".($model2->ezf_field_id)."' ORDER BY ezf_choice_id";
        $strlenght = \Yii::$app->db->createCommand($sql)->query()->count();
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        for($i=0;$i<$strlenght;$i++){
            $formradio .= "<div class='radio'><label><input type='radio' name='".$model2->ezf_field_id."' value='".$dataField[$i]['ezf_choicevalue']."'>".$dataField[$i]['ezf_choicelabel']."</label></div>";
        }
        */
        $sql = "SELECT ezf_choice_id, ezf_choicevalue, ezf_choicelabel, ezf_choiceetc FROM `ezform_choice` WHERE ezf_field_id='" . ($model2->ezf_field_id) . "' AND ezf_choiceetc IS NULL ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        $strlenght = \Yii::$app->db->createCommand($sql)->query()->count();
        for ($i = 0; $i < $strlenght; $i++) {
            $formradio .= "<div class='radio'><label><input type='radio' class='form-control' name='" . $model2->ezf_field_id . "' value='" . $dataField[$i]['ezf_choicevalue'] . "'>" . $dataField[$i]['ezf_choicelabel']."";
            $sql = " SELECT ezf_choicevalue, ezf_choicelabel, ezf_choiceetc FROM `ezform_choice` WHERE ezf_choiceetc ='".$dataField[$i]['ezf_choice_id']."' ORDER BY ezf_choice_id";
            $formradio .= "</label></div>";
            if(\Yii::$app->db->createCommand($sql)->query()->count()>0){
              $dataOther = \Yii::$app->db->createCommand($sql)->queryOne();
              $formradio .= "<div style='margin-left:45px;'><input type='text' placeholder='".$dataOther['ezf_choicelabel']."' class='form-control' id='".$dataOther['ezf_choicevalue']."' name='".$dataOther['ezf_choicevalue']."' value=''></div>";
            }
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght col-md-offset-$model2->ezf_margin_col' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $formradio
        . "</div>"
        . "</div>";
    }
    //---------------

     //---------------
    public static function saveRadio($model2){
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeRadioOnsave($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function typeCheckboxOnload($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        $ezf_field_id =$model2->ezf_field_id;
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        $html = "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>";
        $html .= "<div class='buttonItem'>";
        $html .= "<button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>";
        $html .= "<button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>";
        $html .= "</div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>";
        $html .= $label;

        //set id กลับคืน
        $model2->ezf_field_id = $ezf_field_id;

        $sql = "SELECT * FROM `ezform_choice` WHERE `ezf_field_id`='".$model2->ezf_field_id."' AND `ezf_choiceetc` is NULL ORDER BY `ezf_choice_id`";
        $dataField= \Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataField as $value){
            $html .= "<div class='checkbox'>";
            $html .= "<label><input type='checkbox' name='".$value["ezf_choicevalue"]."' id='".$value["ezf_choicevalue"]."' value='".$value["ezf_choicelabel"]."'>".$value["ezf_choicelabel"]."</label>";
            $html .= "<br></div>";
        }
        $sql2 = "SELECT * FROM `ezform_choice` WHERE `ezf_field_id`='".$model2->ezf_field_id."' AND `ezf_choiceetc`='1' ";
        $data2 = \Yii::$app->db->createCommand($sql2)->queryOne();
        if(($data2)!=""){
                $html .= "<div class='checkbox'><label>";
		$html .= "<input type='checkbox' name='".$model2->ezf_field_name."[]' id='".$model2->ezf_field_name."[]' value='".$model2->ezf_field_name."'>".$data2["ezf_choicelabel"]."";
		$html .= "<input type='text' class='form-control' name='".$data2["ezf_choicevalue"]."' id='".$data2["ezf_choicevalue"]."'/></label></div>";
        }
        $html .= "</div>";
        $html .= "</div>";

        return $html;
    }
    public static function typeCheckboxOnsave($modelfieldid){
        if($modelfieldid->ezf_field_label!=""){
            $label = "<label>$modelfieldid->ezf_field_label</label>";
        }else{
            $label = "";
        }
        $html = "<div class='col-md-$modelfieldid->ezf_field_lenght' id='rowItem' item-id='$modelfieldid->ezf_field_id' data-dad-id='$modelfieldid->ezf_field_order'>";
        $html .= "<div class='buttonItem'>";
        $html .= "<button class='btn btn-default btn-sm btn-edit' type-item='$modelfieldid->ezf_field_type'  data-id='$modelfieldid->ezf_field_id' ><i class='fa fa-pencil'></i></button>";
        $html .= "<button class='btn btn-default btn-sm btn-delete'type-item='$modelfieldid->ezf_field_type' data-id='$modelfieldid->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>";
        $html .= "</div><div class='draggable' id='changeItem' citem-id='$modelfieldid->ezf_field_id'>";

        $html .= $label;
        $sql = "SELECT * FROM `ezform_choice` WHERE `ezf_field_id`='".$modelfieldid->ezf_field_id."' AND `ezf_choiceetc` is NULL ORDER BY `ezf_choice_id`";
        $dataField= \Yii::$app->db->createCommand($sql)->queryAll();
        foreach($dataField as $value){
            $html .= "<div class='checkbox'>";
            $html .= "<label><input type='checkbox' name='".$value["ezf_choicevalue"]."' id='".$value["ezf_choicevalue"]."' value='".$value["ezf_choicelabel"]."'>".$value["ezf_choicelabel"]."</label>";
            $html .= "<br></div>";
        }
        $sql2 = "SELECT * FROM `ezform_choice` WHERE `ezf_field_id`='".$modelfieldid->ezf_field_id."' AND `ezf_choiceetc`='1' ";
        $data2 = \Yii::$app->db->createCommand($sql2)->queryOne();
        if(($data2)!=""){
                $html .= "<div class='checkbox'><label>";
		$html .= "<input type='checkbox' name='".$modelfieldid->ezf_field_name."[]' id='".$modelfieldid->ezf_field_name."[]' value='".$modelfieldid->ezf_field_name."'>".$data2["ezf_choicelabel"]."";
		$html .= "<input type='text' class='form-control' name='".$data2["ezf_choicevalue"]."' id='".$data2["ezf_choicevalue"]."'/></label></div>";
        }
        $html .= "</div>";
        $html .= "</div>";
        $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => $html
        ];
        return $result;
    }
    public static function typeSelectOnsave($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }
        $sql = "SELECT * FROM `ezform_fields` WHERE `ezf_field_id`='".$model2->ezf_field_id."';";
        $dataField= \Yii::$app->db->createCommand($sql)->queryAll();

        $formselect = $label."<br><br>";
        $formselect .= "<select class='form-control' id='".($dataField[0]['ezf_field_name'])."' name='".($dataField[0]['ezf_field_name'])."' style='width:auto;'>";
        $ezf_choice_id = GenMillisecTime::getMillisecTime();

        //selectValue
        $strlenght = count($_POST['selectValue']);

        for($i=0;$i<$strlenght;$i++){
        $ezf_choice_id = GenMillisecTime::getMillisecTime();
        $ezf_choicevalue = $_POST['selectValue'][$i]; $i++;
        $ezf_choicelabel = $_POST['selectValue'][$i];
        $sql = "INSERT INTO `ezform_choice` (`ezf_choice_id`, `ezf_field_id`, `ezf_choicevalue`, `ezf_choicelabel`) "
                . "VALUES ('$ezf_choice_id', '$model2->ezf_field_id', '$ezf_choicevalue', '$ezf_choicelabel');";
        $dataField = Yii::$app->db->createCommand($sql)->execute();
        }

        $sql = " SELECT count(*) as total  FROM `ezform_choice` WHERE ezf_field_id='".($model2->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();


        $strlenght = $dataField[0]['total'];
        $sql = " SELECT ezf_choicevalue, ezf_choicelabel FROM `ezform_choice` WHERE ezf_field_id='".($model2->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        for($i=0;$i<$strlenght;$i++){
            $formselect .= "<option value='".$dataField[$i]['ezf_choicevalue']."'>".$dataField[$i]['ezf_choicelabel']."</option>";
        }
        $formselect .= "</select>";
        //echo $strlenght;
        //\yii\helpers\VarDumper::dump($dataField, 10, TRUE);
        //echo Yii::$app->user->id;
        //Yii::$app->end();

        //$formselect =$dataField;
        //$model2->ezf_field_type ='aa';
        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $formselect
        . "</div>"
        . "</div>";
    }
    public static function typeSelectOnload($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }
        $sql = "SELECT * FROM `ezform_fields` WHERE `ezf_field_id`='".$model2->ezf_field_id."';";
        $dataField= \Yii::$app->db->createCommand($sql)->queryAll();

        $formselect = $label."<br><br>";
        $formselect .= "<select class='form-control' id='".($dataField[0]['ezf_field_id'])."' name='".($dataField[0]['ezf_field_id'])."' style='width:auto;'>";


        $sql = " SELECT count(*) as total  FROM `ezform_choice` WHERE ezf_field_id='".($model2->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        $strlenght = $dataField[0]['total'];

        $sql = " SELECT ezf_choicevalue, ezf_choicelabel FROM `ezform_choice` WHERE ezf_field_id='".($model2->ezf_field_id)."' ORDER BY ezf_choice_id";
        $dataField = \Yii::$app->db->createCommand($sql)->queryAll();
        for($i=0;$i<$strlenght;$i++){
            $formselect .= "<option value='".$dataField[$i]['ezf_choicevalue']."'>".$dataField[$i]['ezf_choicelabel']."</option>";
        }
        $formselect .= "</select>";
        //echo $strlenght;
        //\yii\helpers\VarDumper::dump($dataField, 10, TRUE);
        //echo Yii::$app->user->id;
        //Yii::$app->end();

        //$formselect =$dataField;
        //$model2->ezf_field_type ='aa';

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $formselect
        . "</div>"
        . "</div>";
    }
    //---------------

    public static function saveDate($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeDate($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }

    public static function typeDate($model2){
         if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . "<input type='text' class='form-control' placeholder='dd/mm/YYYY'>"
        . "</div>"
        . "</div>";
    }
    //----------------

    public static function saveTime($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeTime($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function savePersonal($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typePersonal($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function saveSnomed($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeSnomed($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function saveFileinput($model2)
    {
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeFileinput($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function typeTime($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . "<input type='text' class='form-control' placeholder='00:00'>"
        . "</div>"
        . "</div>";
    }
    public static function typePersonal($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . "<input type='text' class='form-control' placeholder='_-____-_____-__-_'>"
        . "</div>"
        . "</div>";
    }
    public static function typeSnomed($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }
        $data = [
            "Snomed Code 2" => "Snomed Code 2",
            "Snomed Code 1" => "Snomed Code 1"
        ];
        $snomedHtml = "<input type='text' class='form-control' placeholder='Search for snomed ...'>";

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . $snomedHtml
        . "</div>"
        . "</div>";
    }
    public static function typeFileinput($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }
        $fielinput = FileInput::widget([
            'name' => 'attachment_50',
            'pluginOptions' => [
                'showPreview' => false,
                'showCaption' => true,
                'showRemove' => true,
                'showUpload' => false
            ]
        ]);

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . $fielinput
        . "</div>"
        . "</div>";
    }
    public static  function typeHiddenfield($model2){
        return "<input type='hidden' value='".$model2->ezf_id."' name='".$model2->ezf_field_label."' id='".$model2->ezf_field_label."'>";
    }
    public static function SaveProvince($model2){
        $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeProvince($model2),
            'data'=> $model2
            ];
        return $result;
    }

    public static function typeProvince($model2){
        if($model2->ezf_field_tumbon!="0"){
            $tumbon = "<div class='col-lg-3'><label>ตำบล</label><select class='form-control' style='width:250px;'><option value=''>- เลือกตำบล -</option></select></div>";
        }else{
            $tumbon = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght col-md-offset-".$model2->ezf_margin_col."'  id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . "<div class='row'><div class='col-lg-3'><label>จังหวัด</label><select class='form-control' style='width:250px;'><option>- เลือกจังหวัด -</option></select></div>"
        . "<div class='col-lg-3'><label>อำเภอ</label><select class='form-control' style='width:250px;'><option>- เลือกอำเภอ -</option></select></div>"
        . $tumbon
        . "</div></div>"
        . "</div>";
    }
    public static function SaveScheckbox($model2){
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeScheckbox($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function typeScheckbox($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label for='$model2->ezf_field_id'>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        return "<div class='col-md-$model2->ezf_field_lenght col-md-offset-".$model2->ezf_margin_col."' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . "<div class='checkbox checkbox-primary'>"
        . "<input type='checkbox' id='$model2->ezf_field_id' name='$model2->ezf_field_id' >"
        . $label
        . "</div>"
        . "</div>"
        . "</div>";
    }
    public static function saveQuestion($model2){
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeQuestion($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function typeQuestion($model2){
        if($model2->ezf_field_label!=""){
            $label = "<h4>$model2->ezf_field_label</h4><hr>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

       return "<div class='col-md-12' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'>"
        . $label
        . "</div>"
        . "</div>";
    }
    public static function SaveHospital($model2){
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeHospital($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function typeHospital($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        $hospitalHtml = "<input type='text' class='form-control' placeholder='Search for hospital ...'>";
        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'><div>"
        . $label
        . $hospitalHtml
        . "</div>"
        . "</div>"
        . "</div>";
    }
    //----------------

    //---------------

    public static function SaveIcd9($model2){
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeIcd9($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function typeIcd9($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        $icd9Html = "<input type='text' class='form-control' placeholder='Search for Icd9 ...'>";
        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'><div>"
        . $label
        . $icd9Html
        . "</div>"
        . "</div>"
        . "</div>";
    }
    //----------------

    //---------------
    public static function SaveIcd10($model2){
        if ($model2->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::typeIcd9($model2),
            'data'=> $model2
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model2,
            ];
            return $result;
        }
    }
    public static function typeIcd10($model2){
        if($model2->ezf_field_label!=""){
            $label = "<label>$model2->ezf_field_label</label>";
        }else{
            $label = "";
        }

        //ถ้าเป็น Reference field
        if($model2->ezf_field_ref_field)
            $model2->ezf_field_id = $model2->ezf_field_ref_field;

        $icd10Html = "<input type='text' class='form-control' placeholder='Search for Icd10 ...'>";
        return "<div class='col-md-$model2->ezf_field_lenght' id='rowItem' item-id='$model2->ezf_field_id' data-dad-id='$model2->ezf_field_order'>"
        . "<div class='buttonItem'>
                <button class='btn btn-default btn-sm btn-edit' type-item='$model2->ezf_field_type'  data-id='$model2->ezf_field_id' ><i class='fa fa-pencil'></i></button>
                <button class='btn btn-default btn-sm btn-delete'type-item='$model2->ezf_field_type' data-id='$model2->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
          </div><div class='draggable' id='changeItem' citem-id='$model2->ezf_field_id'><div>"
        . $label
        . $icd10Html
        . "</div>"
        . "</div>"
        . "</div>";
    }
    //----------------

    //---------------






    public static function saveReffield($model2){
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'html' => self::getTypeEform($model2),
            'data'=> $model2
            ];
            return $result;
    }

}
