<?php

namespace backend\modules\ezforms\components;

use Yii;
use yii\helpers\Html;
use common\lib\sdii\models\SDDynamicModel;
use backend\modules\ezforms\models\EzformFields;
use yii\helpers\Json;
use backend\modules\ezforms\models\EzformCondition;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\Ezform;

class EzformFunc {

	public static function setDynamicModelLimit($fields) {
		$attributes = [];
		$labels = [];
		$required = [];
		$rules = [];
		$rulesFields = [];
		$condFields = [];
		if (!empty($fields)) {
			foreach ($fields as $value) {
				//Attributes array
				$attributes[$value['ezf_field_name']] = ''; //$value['ezf_field_val']
				//Labels array
				$labels[$value['ezf_field_name']] = isset($value['ezf_field_label']) ? $value['ezf_field_label'] : $value['ezf_field_name'];
				//Rule array required
				if ($value['ezf_field_required'] == 1) {
					$required[] = $value['ezf_field_name'];
				}

				//Rule array validate
				$rulesFields['safe'][] = $value['ezf_field_name'];
				$rules['safe'] = ['safe'];
				
				$condFields[] = self::getCondition($value['ezf_id'], $value['ezf_field_name']);
			}
		}
		$model = new SDDynamicModel($attributes);
		foreach ($rules as $key => $value) {
			$options = isset($value[1]) ? $value[1] : [];
			$model->addRule($rulesFields[$key], $value[0], $options);
		}
		
		$js = '';
		foreach ($condFields as $key => $value) {
			if(!empty($value)){
				foreach ($required as $i => $v) {
					foreach ($value as $k => $data) {
						if((!empty($data['var_require']) && in_array($v, $data['var_require'])) ){//|| (!empty($data['var_jump']) && in_array($v, $data['var_jump']))
							$js .= "if(attribute.name == '$v') {
									var r = $('#sddynamicmodel-{$data['ezf_field_name']}:checked').val()=='{$data['ezf_field_value']}';
									//console.log(r);	
									return r;	
								}";
						}
					}
				}
			}
		}
//		VarDumper::dump($js, 10, true);
//		exit();
		
		$model->addRule($required, 'required', ['whenClient' => "function (attribute, value) {
				$js
			}"]);
		$model->addLabel($labels);
		return $model;
	}

	public static function getTypeEform($model_from, $model, $form = null, $options_input = null) {
		//ถ้าเป็น Reference field
		if ($model->ezf_field_type == 18) {
			//เก็บ temp ไว้
			$ezf_field_order = $model->ezf_field_order;
			$ezf_field_id = $model->ezf_field_id;
			$ezf_field_name = $model->ezf_field_name;
			$ezf_field_lenght = $model->ezf_field_lenght;
			//
//			VarDumper::dump($model, 10, true);
			//echo $model->ezf_field_ref_field;
//			Yii::$app->end();

			$model = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
			//VarDumper::dump($model, 10, true);
			//Yii::$app->end();
			$model->ezf_field_name = $ezf_field_name;
			$model->ezf_field_order = $ezf_field_order;
			$model->ezf_field_lenght = $ezf_field_lenght;
			$model->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
			//VarDumper::dump($model, 10, true);
		}
                if ($model->ezf_field_type == 31) {
			//เก็บ temp ไว้
			$ezf_field_order = $model->ezf_field_order;
			$ezf_field_id = $model->ezf_field_id;
			$ezf_field_name = $model->ezf_field_name;
			$ezf_field_lenght = $model->ezf_field_lenght;
			//
//			VarDumper::dump($model, 10, true);
			//echo $model->ezf_field_ref_field;
//			Yii::$app->end();

			$model = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
			//VarDumper::dump($model, 10, true);
			//Yii::$app->end();
			$model->ezf_field_name = $ezf_field_name;
			$model->ezf_field_order = $ezf_field_order;
			$model->ezf_field_lenght = $ezf_field_lenght;
			$model->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
			//VarDumper::dump($model, 10, true);
		}
		if ($form) {
			//VarDumper::dump($model_from, 10, true);
			return self::typeInputValidate($model_from, $model, $form, $options_input);
		} else {
			//VarDumper::dump($model, 10, true);
			return self::typeInput($model_from, $model);
		}
	}
	
	public static function getTypeEprintform($model_from, $model, $form = null, $options_input = null) {
		//ถ้าเป็น Reference field
		if ($model->ezf_field_type == 18) {
			//เก็บ temp ไว้
			$ezf_field_order = $model->ezf_field_order;
			$ezf_field_id = $model->ezf_field_id;
			$ezf_field_name = $model->ezf_field_name;
			$ezf_field_lenght = $model->ezf_field_lenght;
			//
//			VarDumper::dump($model, 10, true);
			//echo $model->ezf_field_ref_field;
//			Yii::$app->end();

			$model = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
			//VarDumper::dump($model, 10, true);
			//Yii::$app->end();
			$model->ezf_field_name = $ezf_field_name;
			$model->ezf_field_order = $ezf_field_order;
			$model->ezf_field_lenght = $ezf_field_lenght;
			$model->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
			//VarDumper::dump($model, 10, true);
		}
		if ($model->ezf_field_type == 31) {
			//เก็บ temp ไว้
			$ezf_field_order = $model->ezf_field_order;
			$ezf_field_id = $model->ezf_field_id;
			$ezf_field_name = $model->ezf_field_name;
			$ezf_field_lenght = $model->ezf_field_lenght;
			//
//			VarDumper::dump($model, 10, true);
			//echo $model->ezf_field_ref_field;
//			Yii::$app->end();

			$model = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
			//VarDumper::dump($model, 10, true);
			//Yii::$app->end();
			$model->ezf_field_name = $ezf_field_name;
			$model->ezf_field_order = $ezf_field_order;
			$model->ezf_field_lenght = $ezf_field_lenght;
			$model->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
			//VarDumper::dump($model, 10, true);
		}                
		return self::typeInputValidatePrint($model_from, $model, $form, $options_input);
	}

	public static function typeInput($model_from, $model) {
		$field_item = EzformQuery::getInputId($model->ezf_field_type);

		if ($field_item) {
			//['labelOptions'=>['class'=>'sdsd']
			
			
			$options = ['class' => 'form-control'];
			$fieldArray = @unserialize($field_item['input_option']);
			if (is_array($fieldArray)) {
				$options = $fieldArray;
			}

			//ตรวจสอบ field_help
			if ($model['ezf_field_val'] == 99) {
				$model["ezf_field_help"] = '<br>' . $model["ezf_field_help"];
			} else {
				$model["ezf_field_help"] = '';
			}

			$enableLabel = true;
			if (in_array($model['ezf_field_type'], [13])) {
			    $enableLabel = false;
			}
			$labelOptions = [];
			$label = $model["ezf_field_label"];
			if($model["ezf_field_color"]){
			    $labelOptions['style'] = 'color: '.$model["ezf_field_color"].';';
			}
			
			if(isset($model["ezf_field_icon"]) && $model["ezf_field_icon"]==1){
			    $label .= ' <i class="glyphicon glyphicon-star"></i>';
			    
			}
			if(!$enableLabel){
			    $label='';
			}
			$labelOptions['label'] = $label;
			
			$options['labelOptions'] = $labelOptions;
			$html = '';
			if ($field_item['input_function'] == 'widget') {
				unset($options['labelOptions']);
				$options['model'] = $model_from;
				$options['attribute'] = $model['ezf_field_name'];
				$options['options'] = ['field'=>$model];
				if($enableLabel){
				    eval("\$html = yii\\helpers\\Html::activeLabel(\$model_from, \$model['ezf_field_name'],\$labelOptions);");
				}
				eval("\$html .= {$field_item['input_class']}::{$field_item['input_function']}(\$options);");
				eval("\$html .= yii\\helpers\\Html::error(\$model_from, \$model['ezf_field_name']);");
			} else if ($field_item['input_class'] == 'EzformWidget') {
			    
				eval("\$html = \\backend\\modules\\ezforms\\components\\".$field_item['input_class']."::{$field_item['input_function']}(\$model_from, \$model['ezf_field_name'], \$model, \$options);");
			} else {
			    if($enableLabel){
				eval("\$html = yii\\helpers\\Html::activeLabel(\$model_from, \$model['ezf_field_name'],\$labelOptions);");
			    }
				eval("\$html .= {$field_item['input_class']}::{$field_item['input_function']}(\$model_from, \$model['ezf_field_name'], \$options);");
				eval("\$html .= yii\\helpers\\Html::error(\$model_from, \$model['ezf_field_name']);");
			}

			//ถ้าเป็น Reference field
			if ($model->ezf_field_ref_field) {
				//เอาไว้ id เวลากด update / delete
				$model->ezf_field_id = $model->ezf_field_ref_field;
			}

			return
					"<div class='col-md-$model->ezf_field_lenght col-md-offset-$model->ezf_margin_col'  id='rowItem' item-id='$model->ezf_field_id' data-dad-id='$model->ezf_field_order'>
			<div class='buttonItem'>
			    <button class='btn btn-default btn-sm btn-edit' type-item='$model->ezf_field_type'  data-id='$model->ezf_field_id' ><i class='fa fa-pencil'></i></button>
			    <button class='btn btn-default btn-sm btn-delete'type-item='$model->ezf_field_type' data-id='$model->ezf_field_id' onclick=''><i class='fa fa-trash'></i></button>
			</div>
			<div class='draggable' id='changeItem' citem-id='$model->ezf_field_id'>
			    $html
			</div>
		    </div>";
		} else if ($field_item['input_function'] == 'hidden') {
			return Html::activeHiddenInput($model_from, $model['ezf_field_name']);
		}
	}

	public static function typeInputValidatePrint($model_from, $model, $form, $options_input) {

		$field_item = EzformQuery::getInputId($model->ezf_field_type);
		
		
		if ($field_item) {
			
			
			$options = ['class' => 'form-control'];
			$fieldArray = @unserialize($field_item['input_option']);
			if (is_array($fieldArray)) {
				$options = $fieldArray;
			}

			//ตรวจสอบ field_help
			if ($model['ezf_field_val'] == 99) {
				$model["ezf_field_help"] = '<br>' . $model["ezf_field_help"];
			} else {
				$model["ezf_field_help"] = '';
			}

			$enableLabel = true;
			if ($model['ezf_field_type'] == 24 && $model['ezf_field_default'] != '') {

				$options['default_bg'] = $model['ezf_field_default'];
				$arr = @unserialize($model['ezf_field_options']);
				if(is_array($arr)){
					$options['allow_bg'] = $arr['allow_bg'];
				}
				
			}elseif (in_array($model['ezf_field_type'], [13])) {
			    $enableLabel = false;
			}else if($model['ezf_field_type'] == 3){
				$options = array_merge($options, ['rows'=>$model['ezf_field_rows']]);
			}
			
			if($options_input != null) {
				$options = array_merge($options, $options_input);
			}

			$labelOptions = [];
			$label = $model["ezf_field_label"];
			
			if($model["ezf_field_color"]){
			    $labelOptions['style'] = 'color: '.$model["ezf_field_color"].';';
			}
			
			if(isset($model["ezf_field_icon"]) && $model["ezf_field_icon"]==1){
			    $label .= ' <i class="glyphicon glyphicon-star"></i>';
			    
			}
			if(!$enableLabel){
			    $label='';
			}
			$labelOptions['label'] = $label;
			$options['labelOptions'] = $labelOptions;
			
			$html ='';
			if ($field_item['input_function_validate'] == 'widget') {
				unset($options['labelOptions']);
				$options['model'] = $model_from;
				$options['attribute'] = $model['ezf_field_name'];
				$options['options'] = ['field'=>$model, 'options_custom' => $form->attributes];
				eval("\$html = \$form->field(\$model_from, \$model['ezf_field_name'])->hint(\$model['ezf_field_hint'])->{$field_item['input_function_validate']}({$field_item['input_class_validate']}, \$options)->label(\$label, \$labelOptions);");
			} else if ($field_item['input_class_validate'] == 'EzformWidget') {
				eval("\$html = \\backend\\modules\\ezforms\\components\\".$field_item['input_class_validate']."::{$field_item['input_function_validate']}(\$form, \$model_from, \$model['ezf_field_name'], \$model, \$options);");
				//eval("\$html = \\backend\\modules\\ezforms\\components\\{$field_item['input_class_validate']}::{$field_item['input_function_validate']}(\$form, \$model_from, \$model['ezf_field_name'], \$model, \$options);");
			} else {
				eval("\$html = \$form->field(\$model_from, \$model['ezf_field_name'])->hint(\$model['ezf_field_hint'])->{$field_item['input_function_validate']}(\$options)->label(\$label, \$labelOptions);");
			}

			$message = "<div";
			if($options_input['query_tools_render']){
				$message .= " class='col-md-12'>";
			}else{
				$message.=" class='col-md-$model->ezf_field_lenght col-md-offset-$model->ezf_margin_col".($form->attributes['print']==1 ? ' visible-print-inline-block' : null). " id='rowItem' item-id='$model->ezf_field_id' data-dad-id='$model->ezf_field_order'>";
			}
			//change Query tools
			if($form->attributes['rstat']==2 && $model->ezf_field_type != 2 && $model->ezf_field_type != 15){
				$message .= Html::button('<i class=\'fa fa-pencil\'></i> ขอแก้ไข', [
					'class' => 'ezform-btn-change btn btn-primary btn-xs',
					'title' => Yii::t('app', 'ส่งคำขอแก้ไขข้อมูล'),
					'data-url'=>Url::to(['query-request/create', 'ezf_id' => Yii::$app->request->get('ezf_id'), 'dataid' => Yii::$app->request->get('dataid'), 'ezf_field_id' => $model->ezf_field_id]),
				]).'<br />';
			}else if((string)$form->attributes['rstat']=='annotated' && $model->ezf_field_type!=2 && $model->ezf_field_type!=15 && $model->ezf_field_type!=13 && $model->ezf_field_type!=19 && $model->ezf_field_type!=23 && $model->ezf_field_type!=26){
				$message .= '<code>'.$model->ezf_field_name.'</code><br>';
			}
			
			
//			if($model['ezf_field_type']==3){
//			    $html='
//				<div class="form-group field-'.$model['ezf_field_name'].'">
//				    <label class="control-label" for="'.$model['ezf_field_name'].'">'.$model['ezf_field_label'].'</label>
//				    <div style="border: 1px solid #ccc; padding: 0 10px;">'.(($model_from[$model['ezf_field_name']]!='')?$model_from[$model['ezf_field_name']]:'<br>').'</div>
//				</div>
//			    
//			    ';
//			}
			
			
			$message .= $html."</div>";
			
			return $message;
		} else if ($field_item['input_function'] == 'hidden') {
			return Html::activeHiddenInput($model_from, $model['ezf_field_name']);
		}
	}
	
	public static function typeInputValidate($model_from, $model, $form, $options_input) {

		$field_item = EzformQuery::getInputId($model->ezf_field_type);
		
		
		if ($field_item) {
			
			
			$options = ['class' => 'form-control'];
			$fieldArray = @unserialize($field_item['input_option']);
			if (is_array($fieldArray)) {
				$options = $fieldArray;
			}

			//ตรวจสอบ field_help
			if ($model['ezf_field_val'] == 99) {
				$model["ezf_field_help"] = '<br>' . $model["ezf_field_help"];
			} else {
				$model["ezf_field_help"] = '';
			}

			$enableLabel = true;
			if ($model['ezf_field_type'] == 24 && $model['ezf_field_default'] != '') {

				$options['default_bg'] = $model['ezf_field_default'];
				$arr = @unserialize($model['ezf_field_options']);
				if(is_array($arr)){
					$options['allow_bg'] = $arr['allow_bg'];
				}
				
			}elseif (in_array($model['ezf_field_type'], [13])) {
			    $enableLabel = false;
			}else if($model['ezf_field_type'] == 3){
				$options = array_merge($options, ['rows'=>$model['ezf_field_rows']]);
			}
			
			if($options_input != null) {
				$options = array_merge($options, $options_input);
			}

			$labelOptions = [];
			$label = $model["ezf_field_label"];
			
			if($model["ezf_field_color"]){
			    $labelOptions['style'] = 'color: '.$model["ezf_field_color"].';';
			}
			
			if(isset($model["ezf_field_icon"]) && $model["ezf_field_icon"]==1){
			    $label .= ' <i class="glyphicon glyphicon-star"></i>';
			    
			}
			if(!$enableLabel){
			    $label='';
			}
			$labelOptions['label'] = $label;
			$options['labelOptions'] = $labelOptions;
			
			$html ='';
			if ($field_item['input_function_validate'] == 'widget') {
				unset($options['labelOptions']);
				$options['model'] = $model_from;
				$options['attribute'] = $model['ezf_field_name'];
				$options['options'] = ['field'=>$model, 'options_custom' => $form->attributes];
				eval("\$html = \$form->field(\$model_from, \$model['ezf_field_name'])->hint(\$model['ezf_field_hint'])->{$field_item['input_function_validate']}({$field_item['input_class_validate']}, \$options)->label(\$label, \$labelOptions);");
			} else if ($field_item['input_class_validate'] == 'EzformWidget') {
				eval("\$html = \\backend\\modules\\ezforms\\components\\".$field_item['input_class_validate']."::{$field_item['input_function_validate']}(\$form, \$model_from, \$model['ezf_field_name'], \$model, \$options);");
				//eval("\$html = \\backend\\modules\\ezforms\\components\\{$field_item['input_class_validate']}::{$field_item['input_function_validate']}(\$form, \$model_from, \$model['ezf_field_name'], \$model, \$options);");
			} else {
				eval("\$html = \$form->field(\$model_from, \$model['ezf_field_name'])->hint(\$model['ezf_field_hint'])->{$field_item['input_function_validate']}(\$options)->label(\$label, \$labelOptions);");
			}

			$message = "<div";
			if($options_input['query_tools_render']){
				$message .= " class='col-md-12'>";
			}else{
				$message.=" class='col-md-$model->ezf_field_lenght col-md-offset-$model->ezf_margin_col".($form->attributes['print']==1 ? ' visible-print-inline-block' : null). " id='rowItem' item-id='$model->ezf_field_id' data-dad-id='$model->ezf_field_order'>";
			}
			//change Query tools
			if($form->attributes['rstat']==2 && $model->ezf_field_type != 2 && $model->ezf_field_type != 15){
				$message .= Html::button('<i class=\'fa fa-pencil\'></i> ขอแก้ไข', [
					'class' => 'ezform-btn-change btn btn-primary btn-xs',
					'title' => Yii::t('app', 'ส่งคำขอแก้ไขข้อมูล'),
					'data-url'=>Url::to(['query-request/create', 'ezf_id' => Yii::$app->request->get('ezf_id'), 'dataid' => Yii::$app->request->get('dataid'), 'ezf_field_id' => $model->ezf_field_id]),
				]).'<br />';
			}else if((string)$form->attributes['rstat']=='annotated' && $model->ezf_field_type!=2 && $model->ezf_field_type!=15 && $model->ezf_field_type!=13 && $model->ezf_field_type!=19 && $model->ezf_field_type!=23 && $model->ezf_field_type!=26){
				//$message .= '<code>'.$model->ezf_field_name.'</code><br>';
                                $from = '/'.preg_quote("</label>", '/').'/';
                                $html = preg_replace($from, '</label> <code>'.$model->ezf_field_name.'</code>', $html,1);
			}

			$message .= $html."</div>";

			return $message;
		} else if ($field_item['input_function'] == 'hidden') {
			return Html::activeHiddenInput($model_from, $model['ezf_field_name']);
		}
	}

	public static function saveInput($model) {

		if ($model->save()) {

			//ถ้าเป็น Reference field
			if ($model->ezf_field_type == 18) {
				//เก็บ temp ไว้
				$ezf_field_order = $model->ezf_field_order;
				$ezf_field_id = $model->ezf_field_id;
				$ezf_field_name = $model->ezf_field_name;
				$ezf_field_lenght = $model->ezf_field_lenght;
				$ezf_id = $model->ezf_id;
				//

				$model = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
				$model->ezf_field_name = $ezf_field_name;
				$model->ezf_field_order = $ezf_field_order;
				$model->ezf_field_lenght = $ezf_field_lenght;
				$model->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
				$model->ezf_id = $ezf_id;
				//VarDumper::dump($model, 10, true);
			}
                        
			if ($model->ezf_field_type == 31) {
				//เก็บ temp ไว้
				$ezf_field_order = $model->ezf_field_order;
				$ezf_field_id = $model->ezf_field_id;
				$ezf_field_name = $model->ezf_field_name;
				$ezf_field_lenght = $model->ezf_field_lenght;
				$ezf_id = $model->ezf_id;
				//

				$model = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_ref_field])->andWhere('ezf_field_type IS NOT NULL')->one();
				$model->ezf_field_name = $ezf_field_name;
				$model->ezf_field_order = $ezf_field_order;
				$model->ezf_field_lenght = $ezf_field_lenght;
				$model->ezf_field_ref_field = $ezf_field_id; //เก็บ id field ต้นทางไปด้วย
				$model->ezf_id = $ezf_id;
				//VarDumper::dump($model, 10, true);
			}                        
			$model_fields = EzformQuery::getFieldsByEzf_id($model->ezf_id);
			//VarDumper::dump($model_fields, 10, true);
			$model_form = self::setDynamicModelLimit($model_fields);

			$result = [
				'status' => 'success',
				'action' => 'create',
				'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
				'html' => self::typeInput($model_form, $model),
				'data' => $model
			];
			return $result;
		} else {
			$result = [
				'status' => 'error',
				'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
				'data' => $model,
			];
			return $result;
		}
	}

	public static function saveCondition($data) {
		$condArr = [];
		if (isset($data)) {
			$condArr = Json::decode($data);
			
			foreach ($condArr as $key => $value) {


				$model = EzformCondition::find()
						->where('ezf_id=:ezf_id AND ezf_field_name=:ezf_field_name AND ezf_field_value=:ezf_field_value', [':ezf_id' => $value['ezf_id'], ':ezf_field_name' => $value['ezf_field_name'], ':ezf_field_value' => $value['ezf_field_value']])
						->one();

				if ($model) {
					$model->cond_jump = Json::encode($value['cond_jump']);
					$model->cond_require = Json::encode($value['cond_require']);
				} else {
					
					$model = new EzformCondition();
					$model->ezf_id = (int)$value['ezf_id'];
					$model->ezf_field_name = (string)$value['ezf_field_name'];
					$model->ezf_field_value = (string)$value['ezf_field_value'];
					$model->cond_jump = ($value['cond_jump']!='')?Json::encode($value['cond_jump']):'';
					$model->cond_require = ($value['cond_require']!='')?Json::encode($value['cond_require']):'';
				}
				
				
						
				if ($model->isNewRecord) {
					if ($value['cond_jump'] != '' || $value['cond_require'] != '') {
						$action = $model->save();
					}
				} else {
					$action = $model->save();
				}
			}
			
		}

		//unset($_COOKIE['gen_condition']);
	}

	public static function deleteCondition($field) {
		$model = EzformCondition::find()
				->where('ezf_id=:ezf_id AND ezf_field_name=:ezf_field_name', [':ezf_id' => $field['ezf_id'], ':ezf_field_name' => $field['ezf_field_name']])
				->all();
		if ($model) {
			foreach ($model as $value) {
				$modelCond = EzformCondition::findOne(['cond_id' => $value['cond_id']]);
				$modelCond->delete();
			}
		}
	}
	public static function chekauth($roleName){
	    $role = Yii::$app->authManager->getAssignment($roleName, Yii::$app->user->id);
	    if($role){
		return true;
	    }
	    return false;
	}

	public static function getCondition($ezf_id, $ezf_field_name)
    {
			$model = EzformCondition::find()
				->where('ezf_id=:ezf_id AND ezf_field_name=:ezf_field_name', [':ezf_id'=>$ezf_id, ':ezf_field_name'=>$ezf_field_name])
				->all();
			
			$dataEzf = [];
			if($model){
				$k = 0;
				foreach ($model as $key => $value) {
					$arr_cond_jump = json_decode($value['cond_jump']);
					$arr_cond_require = json_decode($value['cond_require']);

					if(is_array($arr_cond_jump)) {
						$cond_jump = implode(',', $arr_cond_jump);
						$var_jump = ArrayHelper::getColumn(EzformQuery::getConditionFieldsName('ezf_field_name', $cond_jump), 'ezf_field_name');
					}

					if(is_array($arr_cond_require)) {
						$cond_require = implode(',', $arr_cond_require);
						$var_require = ArrayHelper::getColumn(EzformQuery::getConditionFieldsName('ezf_field_name', $cond_require), 'ezf_field_name');
					}
				
					$dataEzf[$k]['ezf_id'] = $ezf_id;
					$dataEzf[$k]['ezf_field_name'] = $value['ezf_field_name'];
					$dataEzf[$k]['ezf_field_value'] = $value['ezf_field_value'];
					$dataEzf[$k]['var_jump'] = isset($var_jump)?$var_jump:'';
					$dataEzf[$k]['var_require'] = isset($var_require)?$var_require:'';
					$k++;
				}
				
			}
			
			return $dataEzf;
    }
//	    \yii\helpers\VarDumper::dump($model_from,10,true);
//	    \Yii::$app->end();
    public static function ezformUpdate($id){
	$modelOldField = EzformFields::findOne($id);
	if($modelOldField){
	    $modelform = Ezform::find()->where('ezf_id = :ezf_id',[':ezf_id'=>$modelOldField->ezf_id])->One();
	    $valueOld = $modelOldField->ezf_field_name;
	    
	    if($_POST['EzformFields']['ezf_field_type']=='1'){
		$fieldsClass = '\backend\modules\ezforms\controllers\TextController';
		$baseFunc = new $fieldsClass;
		
	    } elseif ($_POST['EzformFields']['ezf_field_type']=='25') {
		$fieldsClass = '\backend\modules\ezforms\controllers\GridController';
		$baseFunc = new $fieldsClass;
		
		if($modelOldField->ezf_field_type!=25){
		    $field_order = $modelOldField->ezf_field_order;
		    
		    if(in_array($modelOldField->ezf_field_type, [1,2,3])){
			$modelOldField->delete();
			EzformQuery::AlterDropField($modelform->ezf_table, $modelOldField->ezf_field_name);
		    }
		    
		    $result = $baseFunc->actionInsertfield($field_order);
		    
		    return $result;
		}
	    }
	    
	}
	
	return false;
    }
    
    public static function insertFiles() {
	$sql = "SELECT tb_data_1.id, 
	tb_data_1.ptid_key, 
	tb_data_1.icf_upload1, 
	tb_data_1.icf_upload2, 
	tb_data_1.icf_upload3, 
	tb_data_1.target
FROM tb_data_1
WHERE tb_data_1.icf_upload1 IS NOT NULL OR tb_data_1.icf_upload2 IS NOT NULL OR tb_data_1.icf_upload3 IS NOT NULL";
	
	$data = Yii::$app->db->createCommand($sql)->queryAll();
	
	foreach ($data as $value) {
	    
	    if(!empty($value['icf_upload1'])){
		$file_db = new \backend\modules\ezforms\models\FileUpload();
		$file_db->tbid = $value['id'];
		$file_db->ezf_id = '1437377239070461301';
		$file_db->ezf_field_id = '1446652646071443400';
		$file_db->file_active = 0;
		$file_db->file_name = $value['icf_upload1'];
		$file_db->file_name_old = $value['icf_upload1'];
		$file_db->target = $value['target'];
		$file_db->save();
	    }
	    
	    if(!empty($value['icf_upload2'])){
		$file_db = new \backend\modules\ezforms\models\FileUpload();
		$file_db->tbid = $value['id'];
		$file_db->ezf_id = '1437377239070461301';
		$file_db->ezf_field_id = '1446652758038914800';
		$file_db->file_active = 0;
		$file_db->file_name = $value['icf_upload2'];
		$file_db->file_name_old = $value['icf_upload2'];
		$file_db->target = $value['target'];
		$file_db->save();
	    }
	    
	    if(!empty($value['icf_upload3'])){
		$file_db = new \backend\modules\ezforms\models\FileUpload();
		$file_db->tbid = $value['id'];
		$file_db->ezf_id = '1437377239070461301';
		$file_db->ezf_field_id = '1446652816009316600';
		$file_db->file_active = 0;
		$file_db->file_name = $value['icf_upload3'];
		$file_db->file_name_old = $value['icf_upload3'];
		$file_db->target = $value['target'];
		$file_db->save();
	    }
	    
	}
    }
    
    public static function getEmailSite() {
	$key = Yii::$app->keyStorage->get('admin-email');
	if(!empty($key)){
	    $key = str_replace(' ', '', $key);
	    return explode(',', $key);
	}
	return Yii::$app->params['adminEmail'];
    }
    
    public static function getSite($sitecode){
	
        $sqlHospital = "SELECT `hcode` as code,`name` FROM all_hospital_thai WHERE hcode=:sitecode ";
        $dataHospital = Yii::$app->db->createCommand($sqlHospital, [':sitecode'=>$sitecode])->queryOne();
	return $dataHospital;
    }
    
    public static function getTextBetweenTags($string, $tagname) {
	$pattern = "/<$tagname\s[^>]*>(.*)<\/$tagname>/siU";
	preg_match($pattern, $string, $matches);
	return $matches[1];
    }
    
    public static function getAttributeBetweenTags($string, $tagname, $attribute) {
	$pattern = "/<$tagname(?:\s+(?:$attribute=[\"\'](?P<$attribute>[^\"\'<>]+)[\"\']|\w+=[\"\'][^\"\'<>]+[\"\']))+/i";
	preg_match($pattern, $string, $matches);
	return $matches[$attribute];
    }
    
    public static function getLable($string) {
	$txtIcon = self::getTextBetweenTags($string, 'font');
	$txt = trim(str_replace('<i class="glyphicon glyphicon-star" ></i>', '', $txtIcon));
	$icon = trim(str_replace($txt, '', $txtIcon));
	$color = self::getAttributeBetweenTags($string, 'font', 'color');
	
	return [
	    'text'=>$txt,
	    'color'=>$color,
	    'icon'=>(!empty($icon))?1:0,
	];
    }
    
    public static function setLable($array) {
	$str = '';
	$icon = (isset($array['icon']) && $array['icon'])?'<i class="glyphicon glyphicon-star" ></i>':'';
	$color = (isset($array['color']) && !empty($array['color']))?$array['color']:'#000';
	$str = "<font color=\"$color\">{$array['text']} $icon</font>";
	
	return $str;
    }
    
    public static function getRandomDrug($id) {
	$arrId = ['S1443', 'S1408', 'S1713', 'S1963', 'S1360', 'S1673', 'S1831', 'S1523', 'S1778', 'S2001',
		'S1498', 'S1260', 'S1156', 'S1440', 'S1718', 'S1601', 'S2052', 'S1914', 'S2019', 'S1954',
		'S1242', 'S1816', 'S2113', 'S1814', 'S1931', 'S1208', 'S1441', 'S1494', 'S2029', 'S1211',
		'S2095', 'S1315', 'S1186', 'S1682',  'S1545', 'S2060',   'S1684', 'S1179', 'S1188', 'S2082', 'S1086', 'S1416',
		'S1330', 'S1366', 'S2088', 'S2134', 'S1489', 'S1687',
		'S1768','S1591','S1930','S1227','S2016','S1415','S1571','S1720','S1906','S1164','S1341','S1853','S1314','S1095','S1191',
	    'S1499','S1633','S1531','S1896','S2026','S1992','S1135','S1685','S2120','S1495','S1484','S1957','S1617','S1927','S1925',
	    'S1515','S1386','S2097','S1608','S1622','S1319','S1140','S2054','S1345','S1435','S2087','S1780','S1769','S1392','S1696',
	    'S1766','S1423','S2098','S1363','S1846','S1690','S1698','S1550','S1310','S1442','S1899','S2045','S1125','S2059','S1299',
	    'S1303','S1174','S2055','S1771','S1535','S2132','S2047','S1613','S1411','S1141','S1347','S1178','S1724','S1317','S1344',
	    'S1686','S2119','S1965','S1924','S1500','S2096','S1762','S2131','S1890','S1420','S1118','S1864','S1935','S2080','S2128',
	    'S1597','S1507','S1986','S2133','S1322','S1307','S1672','S1394','S1738','S1157','S1294','S1971','S1266','S1593','S1144',
	    'S1137','S1228','S1881','S1932','S2032','S2070','S1882','S2035','S1358','S1662','S1945','S1354','S1541','S1206','S1551',
	    'S1302','S1566','S1335','S1946','S1840','S1850','S1148'];
	
	$strDrug = 1;
	if(in_array($id, $arrId)){
	    $strDrug = 2;
	}
	
	return $strDrug;
    }

}
