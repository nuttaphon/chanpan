<?php
namespace backend\modules\ezforms\components;

class EzActiveForm extends \yii\bootstrap\ActiveForm{
    /**
     * @var string the default field class name when calling [[field()]] to create a new field.
     * @see fieldConfig
     */
    public $fieldClass = 'backend\modules\ezforms\components\ActiveField';
}
