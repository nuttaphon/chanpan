<?php

namespace backend\modules\mailing\models;

use Yii;

/**
 * This is the model class for table "mailing_list".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $email
 * @property integer $group
 * @property integer $user_create
 * @property string $create_date
 */
class MailingList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mailing_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'group', 'user_create'], 'integer'],
            [['create_date'], 'safe'],
            [['email'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'email' => 'Email',
            'group' => 'Group',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
        ];
    }
}
