<?php
/*
 * Module US Finding.
 * Developed by Mark.
 * Date : 2016-03-01
*/
namespace backend\modules\usfinding;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\usfinding\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
