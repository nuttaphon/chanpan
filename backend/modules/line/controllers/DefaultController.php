<?php

namespace backend\modules\line\controllers;

use yii\web\Controller;
use backend\modules\line\models\LineUser;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        if (!LineUser::findOne(['user_id'=>\Yii::$app->user->identity->id])) {
            while(true) {
                $pincode = str_pad(rand(10000, 99999), 5, '0', STR_PAD_LEFT);
                if (!LineUser::findOne(['pincode'=>$pincode])) {
                    $lineuser = new LineUser();
                    $lineuser->user_id = \Yii::$app->user->identity->id;
                    $lineuser->pincode = $pincode;
                    $lineuser->dupdate = date("Y-m-d H:i:s");
                    $lineuser->save();     
                    break;
                }
            }
        }else{
            $lineuser = LineUser::findOne(['user_id'=>\Yii::$app->user->identity->id]);
            $pincode = $lineuser->pincode;
            $lineid = $lineuser->line_id;
            $lineimg = $lineuser->line_image;
        }
        //$pincode= rand(10000, 99999);
        return $this->render('index',[
            'pincode'   => $pincode,
            'lineid'    => $lineid,
            'lineimg'    => $lineimg,
        ]);
    }
}
