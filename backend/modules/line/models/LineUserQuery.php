<?php

namespace backend\modules\line\models;

/**
 * This is the ActiveQuery class for [[LineUser]].
 *
 * @see LineUser
 */
class LineUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return LineUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return LineUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}