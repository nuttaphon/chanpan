<?php

namespace backend\modules\line;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\line\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
