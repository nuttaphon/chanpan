<?php

namespace backend\modules\cpay;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\cpay\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
