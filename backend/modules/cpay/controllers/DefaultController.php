<?php

namespace backend\modules\cpay\controllers;

use yii\web\Controller;
use Yii;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $userdet = self::userDetail( $_SESSION["__id"] );
        $_SALT='cpay'.date('Ymd');
        $uath = crypt( $userdet[0]['username'],$_SALT );
        
        if(strlen($_GET['act'])==0){
            $act='cpay';
        }else{
            $act=$_GET['act'];
        }
        
        if(strlen($_GET['act1'])==0){
            //$act1='cpay';
            $act1='ver2';
        }else{
            $act1=$_GET['act1'];
        }
        
        return $this->render('index',[
                'userdet' => $userdet,
                'uid' => $_SESSION["__id"],
                'act' => $act,
                'act1' => $act1,
                'act2' => $_GET['act2'],
                'act3' => $_GET['act3'],
                'tperiod' => $_GET['tperiod'],
                'zone' => $_GET['zone'],
                'prov' => $_GET['province'],
                'amphur' => $_GET['amphur'],
                'hosp' => $_GET['hosp'],
                'selectuser' => $_GET['selectuser'],
                'uath' => $uath,
                '_SALT' => $_SALT,
                'administrator' => Yii::$app->user->can('administrator'),
                'adminsite' => Yii::$app->user->can('adminsite'),
                'debug'=>true
            ]);
    }
    
    
    
    // sub function 
    public function userDetail($userid){
        //$userid=1;
        $sql = "select * from user where id=\"".addslashes($userid)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }
    
    public function userDetailFull($userid){
        
        //$userid=1;
        $sql = "select * from user_profile where user_id=\"".addslashes($userid)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
}
