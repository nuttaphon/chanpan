<?php
namespace backend\modules\tccbot\classes;

use Yii;
use yii\data\SqlDataProvider;
use backend\modules\ezforms\models\Ezform;
use backend\modules\inv\models\InvGen;

/**
 * OvccaQuery class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class TccBotQuery {
    public static function getBuffeDataCount($sitecode, $tbname) {
	
	try {
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    Yii::$app->session['db_zone'] = \backend\modules\managedata\classes\ManagerQuery::getZone($sitecode);
	
	    $sql = "SELECT COUNT(*)
		FROM $tbname
		WHERE hospcode = :hospcode
	    ";

	    return \backend\modules\managedata\classes\ManagerFunc::queryScalar(Yii::$app->session['db_zone'], $sql, [':hospcode'=>$sitecode]);

	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }
    
    public static function getColumnBuffeData($tbname) {
	
	try {
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    Yii::$app->session['db_zone'] = \backend\modules\managedata\classes\ManagerQuery::getZone($sitecode);
	
	    $sql = "SHOW COLUMNS FROM $tbname";

	    return \backend\modules\managedata\classes\ManagerFunc::queryAll(Yii::$app->session['db_zone'], $sql);

	} catch (\yii\db\Exception $e) {
	    return false;
	}
    }
    
}
