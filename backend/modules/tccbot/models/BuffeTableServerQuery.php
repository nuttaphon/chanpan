<?php

namespace backend\modules\tccbot\models;

/**
 * This is the ActiveQuery class for [[BuffeTableServer]].
 *
 * @see BuffeTableServer
 */
class BuffeTableServerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return BuffeTableServer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BuffeTableServer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}