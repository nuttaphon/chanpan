<?php

namespace backend\modules\tccbot\models;

/**
 * This is the ActiveQuery class for [[BuffeTriggers]].
 *
 * @see BuffeTriggers
 */
class BuffeTriggersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return BuffeTriggers[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BuffeTriggers|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}