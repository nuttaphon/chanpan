<?php

namespace backend\modules\tccbot\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\tccbot\models\BuffeTableServer;

/**
 * BuffeTableServerSearch represents the model behind the search form about `backend\modules\tccbot\models\BuffeTableServer`.
 */
class BuffeTableServerSearch extends BuffeTableServer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'table', 'progress'], 'safe'],
            [['record', 'qleft'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuffeTableServer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'record' => $this->record,
            'qleft' => $this->qleft,
        ]);

        $query->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'table', $this->table])
            ->andFilterWhere(['like', 'progress', $this->progress]);

        return $dataProvider;
    }
}
