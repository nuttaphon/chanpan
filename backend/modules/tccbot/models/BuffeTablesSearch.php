<?php

namespace backend\modules\tccbot\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\tccbot\models\BuffeTables;

/**
 * BuffeTablesSearch represents the model behind the search form about `backend\modules\tccbot\models\BuffeTables`.
 */
class BuffeTablesSearch extends BuffeTables
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'table', 'data', 'dadd', 'dupdate'], 'safe'],
            [['his_rows', 'server_rows'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuffeTables::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'his_rows' => $this->his_rows,
            'server_rows' => $this->server_rows,
            'dadd' => $this->dadd,
            'dupdate' => $this->dupdate,
        ]);
        
        $query->andFilterWhere(['like', 'sitecode', \Yii::$app->user->identity->userProfile->sitecode])
            ->andFilterWhere(['like', 'table', $this->table])
            ->andFilterWhere(['like', 'data', $this->data]);
        $query->orderBy('server_rows DESC,table ASC');
        return $dataProvider;
    }
}
