<?php

namespace backend\modules\tccbot\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\tccbot\models\BuffeConfig;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use backend\modules\tccbot\models\BuffeTableServer;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

/**
 * BuffeTriggersController implements the CRUD actions for BuffeTriggers model.
 */
class MonitorController extends Controller
{

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all BuffeTriggers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $delay=  isset($_GET['delay'])?(in_array($_GET['delay'], [5,10,15,30,60,300])?$_GET['delay']:15):15;
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	$model = BuffeConfig::find()->where('id=:sitecode', [':sitecode'=>$sitecode])->one();
	
	if(!$model){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'ท่านยังไม่ได้ติดตั้ง TDC '.\yii\helpers\Html::a('ท่านสามารถดาวน์โหลดได้ที่นี่', 'https://storage.thaicarecloud.org/source/app/v2/', ['class'=>'btn btn-warning']),
		'options'=>['class'=>'alert-info']
	    ]);
	    
	    return $this->redirect(['/tccbots/my-module']);
	} else {
	    $statusIcon = \common\lib\sdii\components\utils\SDdate::differenceTimerBot($model['last_ping']);
	    if($statusIcon==2){
		$model['cpu']=0;
		$model['ram']=0;
		$model['cpu_serv']=0;
		$model['ram_serv']=0;
		$model['ram_usage']=0;
		$model['ram_serv_usage']=0;
	    } else {
		$model['cpu']=round($model['cpu'], 1);
		$model['ram']=round($model['ram'], 1);
		$model['cpu_serv']=round($model['cpu_serv'], 1);
		$model['ram_serv']=round($model['ram_serv'], 1);
		$model['ram_usage']= number_format($model['ram_usage'],1);
		$model['ram_serv_usage']=  number_format($model['ram_serv_usage'],1);
				
	    }
	}
	
	//ประมวลผลแล้วบันทึกให้เสร็จเลย
	$buffeUpdate = \backend\modules\tccbot\classes\TccBotFunc::buffeUpdate($sitecode);
	
	$query = BuffeTableServer::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode]);
	
	$dataProvider = new ActiveDataProvider([
            'query' => $query,
	    'pagination' => [
		'pageSize' => 0,
	    ],
        ]);
	
        return $this->render('index', [
            'delay'=>$delay,
	    'model'=>$model,
	    'dataProvider'=>$dataProvider,
	    'sitecode'=>$sitecode,
        ]);
    }
    
    public function actionGetPoint(){
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	    $model = BuffeConfig::find()->where('id=:sitecode', [':sitecode'=>$sitecode])->one();
	    
	    if($model){
		$statusIcon = \common\lib\sdii\components\utils\SDdate::differenceTimerBot($model['last_ping']);
		if($statusIcon==2){
		    $model['cpu']=0;
		    $model['ram']=0;
		    $model['cpu_serv']=0;
		    $model['ram_serv']=0;
		    $model['ram_usage']=0;
		    $model['ram_serv_usage']=0;
		} else {
		    $model['cpu']=round($model['cpu'], 1);
		    $model['ram']=round($model['ram'], 1);
		    $model['cpu_serv']=round($model['cpu_serv'], 1);
		    $model['ram_serv']=round($model['ram_serv'], 1);
		    $model['ram_usage']= number_format($model['ram_usage'],1);
		    $model['ram_serv_usage']=  number_format($model['ram_serv_usage'],1);
		}
		$result = [
		    'status' => 'success',
		    'message' => Yii::t('app', 'Load completed.'),
		    'data'=>$model,
		];
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => Yii::t('app', 'Load Empty data.'),
		    'data'=>$model,
		];
	    }
	    
	    return $result;
	}
    }

    public function actionUpdate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    $model = BuffeConfig::find()->where('id=:sitecode', [':sitecode'=>$sitecode])->one();
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('_update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionViewProcess()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    //ประมวลผลแล้วบันทึกให้เสร็จเลย
	    $buffeUpdate = \backend\modules\tccbot\classes\TccBotFunc::buffeUpdate($sitecode);
	    
	    $query = BuffeTableServer::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode]);
	    
	    $dataProvider = new ActiveDataProvider([
		'query' => $query,
		'pagination' => [
		    'pageSize' => 0,
		],
	    ]);
	    
	    return $this->renderAjax('_process', [
		'dataProvider'=>$dataProvider,
		'sitecode'=>$sitecode,
	    ]);
	    
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionViewData()
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$table = isset($_GET['table'])?$_GET['table']:'';
	//ประมวลผลแล้วบันทึกให้เสร็จเลย
	$count = \backend\modules\tccbot\classes\TccBotQuery::getBuffeDataCount($sitecode, $table);
	
	if($count){
	    $sql = "SELECT *
		FROM $table
		WHERE hospcode = :hospcode
	    ";
	    
	    $dataProvider = new SqlDataProvider([
		'sql' => $sql,
		'db'=>'dbbot2',
		'params' => [':hospcode' => $sitecode],
		'totalCount' => $count,
		'pagination' => [
		    'pageSize' => 100,
		],
	    ]);
	    
	    return $this->render('view', [
		'dataProvider'=>$dataProvider,
		'data'=>$data,
		'sitecode'=>$sitecode,
		'table'=>$table,
	   ]);
	} else {
	    return $this->render('error', [
		'sitecode'=>$sitecode,
		'table'=>$table,
	   ]);
	}   
	
    }
}
