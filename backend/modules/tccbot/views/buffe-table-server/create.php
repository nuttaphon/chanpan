<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTableServer */

$this->title = 'Create Buffe Table Server';
$this->params['breadcrumbs'][] = ['label' => 'Buffe Table Servers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-table-server-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
