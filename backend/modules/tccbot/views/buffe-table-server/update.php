<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTableServer */

$this->title = 'Update Buffe Table Server: ' . ' ' . $model->sitecode;
$this->params['breadcrumbs'][] = ['label' => 'Buffe Table Servers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sitecode, 'url' => ['view', 'sitecode' => $model->sitecode, 'table' => $model->table]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="buffe-table-server-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
