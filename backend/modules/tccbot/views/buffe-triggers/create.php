<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTriggers */

$this->title = 'Create Buffe Triggers';
$this->params['breadcrumbs'][] = ['label' => 'Buffe Triggers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-triggers-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
