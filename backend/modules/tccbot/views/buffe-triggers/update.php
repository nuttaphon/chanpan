<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTriggers */

$this->title = 'Update Buffe Triggers: ' . ' ' . $model->sitecode;
$this->params['breadcrumbs'][] = ['label' => 'Buffe Triggers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sitecode, 'url' => ['view', 'sitecode' => $model->sitecode, 'Trigger' => $model->Trigger]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="buffe-triggers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
