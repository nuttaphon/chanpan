<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTriggers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buffe-triggers-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Buffe Triggers</h4>
	</div>

	<div class="modal-body">
		<?= $form->field($model, 'sitecode')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'Trigger')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'Event')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'Table')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'Statement')->textarea(['rows' => 6]) ?>

		<?= $form->field($model, 'Timing')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'Created')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'sql_mode')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'Definer')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'character_set_client')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'collation_connection')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'Database_Collation')->textInput(['maxlength' => true]) ?>

	</div>
	<div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create'){
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#buffe-triggers-grid-pjax'});
	    } else if(result.action == 'update'){
		$(document).find('#modal-buffe-triggers').modal('hide');
		$.pjax.reload({container:'#buffe-triggers-grid-pjax'});
	    }
	} else{
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function(){
	console.log('server error');
    });
    return false;
});

");?>