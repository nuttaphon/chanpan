<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTables */

$this->title = 'Update Buffe Tables: ' . ' ' . $model->sitecode;
$this->params['breadcrumbs'][] = ['label' => 'Buffe Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sitecode, 'url' => ['view', 'sitecode' => $model->sitecode, 'table' => $model->table]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="buffe-tables-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
