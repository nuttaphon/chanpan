<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTables */

$this->title = 'Create Buffe Tables';
$this->params['breadcrumbs'][] = ['label' => 'Buffe Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-tables-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
