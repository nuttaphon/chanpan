<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTablesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buffe-tables-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sitecode') ?>

    <?= $form->field($model, 'table') ?>

    <?= $form->field($model, 'data') ?>

    <?= $form->field($model, 'his_rows') ?>

    <?= $form->field($model, 'server_rows') ?>

    <?php // echo $form->field($model, 'dadd') ?>

    <?php // echo $form->field($model, 'dupdate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
