<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tccbot\models\BuffeTablesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buffe Tables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-tables-index">

    <div class="sdbox-header">
	<h3><?=  Html::encode($this->title) ?></h3>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php  Pjax::begin(['id'=>'buffe-tables-grid-pjax']);?>
    <?= SDGridView::widget([
	'id' => 'buffe-tables-grid',
	'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['buffe-tables/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-buffe-tables']),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'sitecode','filter'=>''],
            'table',
            ['attribute'=>'data','filter'=>''],
            
            ['attribute'=>'server_rows','filter'=>''],
            ['attribute'=>'qleft','filter'=>''],
            // 'dadd',
            // 'dupdate',

            ['class' => 'common\lib\sdii\widgets\SDActionColumn','template'=>'{view}'],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  SDModalForm::widget([
    'id' => 'modal-buffe-tables',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#buffe-tables-grid-pjax').on('click', '#modal-addbtn-buffe-tables', function(){
modalBuffeTable($(this).attr('data-url'));
});

$('#buffe-tables-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalBuffeTable('".Url::to(['buffe-tables/update', 'id'=>''])."'+id);
});	

$('#buffe-tables-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'view'){
	modalBuffeTable(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
	    $.post(
		url
	    ).done(function(result){
		if(result.status == 'success'){
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-tables-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function(){
		console.log('server error');
	    });
	})
    }
    return false;
});
	
function modalBuffeTable(url) {
    $('#modal-buffe-tables .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-buffe-tables').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>