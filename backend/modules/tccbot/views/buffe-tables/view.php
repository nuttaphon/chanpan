<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccbot\models\BuffeTables */

$this->title = 'Buffe Tables#'.$model->sitecode;
$this->params['breadcrumbs'][] = ['label' => 'Buffe Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-tables-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'sitecode',
		'table',
		'data:ntext',
		'his_rows',
		'server_rows',
		'dadd',
		'dupdate',
	    ],
	]) ?>
    </div>
</div>
