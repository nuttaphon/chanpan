<?php
use yii\helpers\Html;
Use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\bootstrap\ActiveForm;
use appxq\sdii\widgets\ModalForm;
use yii\widgets\Pjax;

$this->title = 'TDC Monitoring';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$gaugeOptions = [
    'chart'=> ['type'=>'solidgauge'],
    'title'=>null,
    'pane'=>[
	'center'=>['50%', '85%'],
	'size'=>'140%',
	'startAngle'=>-90,
	'endAngle'=>90,
	'background'=>[
	    'backgroundColor'=> new JsExpression("(Highcharts.theme && Highcharts.theme.background2) || '#EEE'"),
	    'innerRadius'=>'60%',
	    'outerRadius'=>'100%',
	    'shape'=>'arc'
	]
    ],
    'tooltip'=>['enabled'=>false],
    'yAxis'=>[
	'stops'=>[
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#DF5353'] // red
            ],
	'lineWidth'=>0,
	'minorTickInterval'=>null,
	'tickAmount'=>2,
	'title'=>['y'=>-70],
	'labels'=>['y'=>16],
    ],
    'plotOptions'=>[
	'solidgauge'=>[
	    'dataLabels'=>[
		'y'=>5,
		'borderWidth'=>0,
		'useHTML'=>true
	    ]
	]
    ]
];

$statusIcon = \common\lib\sdii\components\utils\SDdate::differenceTimerBot($model['last_ping']);
$iconColor = '#ddd';
$iconLabel = '';

if ($statusIcon==0){
    $iconColor = 'rgb(85, 191, 59)';
    $iconLabel = 'Connecting';
} elseif ($statusIcon==1) {
    $iconColor = 'rgb(222, 153, 48)';
    $iconLabel = 'Wait';
} elseif ($statusIcon==2) {
    $iconColor = 'rgb(223, 83, 83)';
    $iconLabel = 'Disconnect';
}
?>

<div class="row" id="tccbot-monitoring">
    <div class="col-md-5">
	<div class="panel panel-default ">
	    <div class="panel-heading">
		<div class="row">
		    <div class="col-lg-5 col-md-5"><h3 class="panel-title"><i class="glyphicon glyphicon-flash" data-toggle="tooltip" title="<?=$iconLabel?>" style="font-size: 20px; color: <?=$iconColor?>"></i> Hardware Monitoring</h3></div>
		<div class="col-lg-7 col-md-7 text-right">
		    <?php $form = ActiveForm::begin([
		'id' => 'jump_menu',
		'action' => ['index'],
		'method' => 'get',
		'layout' => 'inline',
		'options' => ['style'=>'display: inline-block;']	    
	    ]); 
    
	    ?>
		    <?= Html::dropDownList('delay', $delay, [5=>'5 วิ', 10=>'10 วิ', 15=>'15 วิ', 30=>'30 วิ', 60=>'60 วิ', 300=>'5 นาที'], ['class'=>'form-control input-sm', 'onChange'=>'$("#jump_menu").submit()'])?>
	    <?php ActiveForm::end(); ?>	
		    <?=  Html::button('<i class="glyphicon glyphicon-cog"></i>',['id'=>'config-btn','class'=>'btn btn-primary btn-sm ', 'data-toggle'=>'tooltip', 'title'=>'ตั้งค่า TDC', 'data-url'=>  Url::to(['/tccbot/monitor/update'])])?>
		    <?=  Html::a('<i class="glyphicon glyphicon-cloud-download"></i> ดาวน์โหลด TDC', 'https://storage.thaicarecloud.org/source/app/v2/', ['class'=>'btn btn-success btn-sm ', 'target'=>'_blank']); //https://storage.thaicarecloud.org/source/app/v2/ ?>
		</div>
		</div>
	    </div>
	    <div class="panel-body">
		<div class="row">
		    <div class="col-lg-6 col-md-6" style="padding-left: 5px;">
			<?= miloschuman\highcharts\Highcharts::widget([
			    'htmlOptions'=>[
				'id'=>'container-app-cpu',
				'style'=>'width: 265px; height: 200px;'
			    ],
			    'options' => ArrayHelper::merge($gaugeOptions, [
				'yAxis'=>[
				    'min'=>0,
				    'max'=>100,
				    'title'=>[
					'text'=>'CPU App'
				    ]
				],
				'credits'=>['enabled'=>false],
				'series'=>[[
				    'name'=>'CPU App',
				    'data'=>[floatval($model['cpu'])],
				    'dataLabels'=>[
					'format'=> new JsExpression("'<div style=\"text-align:center\"><span style=\"font-size:25px;color:' +
					    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '\">{y}</span><br/>' +
					       '<span style=\"font-size:12px;color:silver\">%</span></div>'")
				    ],
				    'tooltip'=>['valueSuffix'=>' %'],
				]]
			    ]),
			    'scripts'=>['highcharts-more', 'modules/solid-gauge']
			]);?>
		    </div>
		    <div class="col-lg-6 col-md-6" style="padding-left: 5px;">
			<?= miloschuman\highcharts\Highcharts::widget([
			    'htmlOptions'=>[
				'id'=>'container-app-ram',
				'style'=>'width: 265px; height: 200px;'
			    ],
			    'options' => ArrayHelper::merge($gaugeOptions, [
				'yAxis'=>[
				    'min'=>0,
				    'max'=>100,
				    'title'=>[
					'text'=>'RAM App '.$model['ram_usage'].' MB'
				    ]
				],
				'credits'=>['enabled'=>false],
				'series'=>[[
				    'name'=>'RAM App',
				    'data'=>[floatval($model['ram'])],
				    'dataLabels'=>[
					'format'=> new JsExpression("'<div style=\"text-align:center\"><span style=\"font-size:25px;color:' +
					    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '\">{y}</span><br/>' +
					       '<span style=\"font-size:12px;color:silver\">%</span></div>'")
				    ],
				    'tooltip'=>['valueSuffix'=>' %'],
				]]
			    ]),
			    'scripts'=>['highcharts-more', 'modules/solid-gauge']
			]);?>
			
		    </div>
		</div>
		<div class="row">
		    <div class="col-lg-6 col-md-6" style="padding-left: 5px;">
			<?= miloschuman\highcharts\Highcharts::widget([
			    'htmlOptions'=>[
				'id'=>'container-server-cpu',
				'style'=>'width: 265px; height: 200px;'
			    ],
			    'options' => ArrayHelper::merge($gaugeOptions, [
				'yAxis'=>[
				    'min'=>0,
				    'max'=>100,
				    'title'=>[
					'text'=>'CPU Server'
				    ]
				],
				'credits'=>['enabled'=>false],
				'series'=>[[
				    'name'=>'CPU Server',
				    'data'=>[floatval($model['cpu_serv'])],
				    'dataLabels'=>[
					'format'=> new JsExpression("'<div style=\"text-align:center\"><span style=\"font-size:25px;color:' +
					    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '\">{y}</span><br/>' +
					       '<span style=\"font-size:12px;color:silver\">%</span></div>'")
				    ],
				    'tooltip'=>['valueSuffix'=>' %'],
				]]
			    ]),
			    'scripts'=>['highcharts-more', 'modules/solid-gauge']
			]);?>
		    </div>
		    <div class="col-lg-6 col-md-6" style="padding-left: 5px;">
			
			<?= miloschuman\highcharts\Highcharts::widget([
			    'htmlOptions'=>[
				'id'=>'container-server-ram',
				'style'=>'width: 265px; height: 200px;'
			    ],
			    'options' => ArrayHelper::merge($gaugeOptions, [
				'yAxis'=>[
				    'min'=>0,
				    'max'=>100,
				    'title'=>[
					'text'=>'RAM Server '.$model['ram_serv_usage'].' MB'
				    ]
				],
				'credits'=>['enabled'=>false],
				'series'=>[[
				    'name'=>'RAM Server',
				    'data'=>[floatval($model['ram_serv'])],
				    'dataLabels'=>[
					'format'=> new JsExpression("'<div style=\"text-align:center\"><span style=\"font-size:25px;color:' +
					    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '\">{y}</span><br/>' +
					       '<span style=\"font-size:12px;color:silver\">%</span></div>'")
				    ],
				    'tooltip'=>['valueSuffix'=>' %'],
				]]
			    ]),
			    'scripts'=>['highcharts-more', 'modules/solid-gauge']
			]);?>
			
		    </div>
		</div>
	    </div>
	</div>
    </div>
    <div class="col-md-7">
	<div class="panel panel-default">
	    <div class="panel-heading">
		<h3 class="panel-title">Sync Progress</h3>
	    </div>
	    <?php  //Pjax::begin(['id'=>'buffe-table-server-pjax', 'timeout' => 5000]);?>
	    <div id="view-process" class="panel-body">
		<?php
		    echo $this->render('_process', [
			'dataProvider'=>$dataProvider,
			'sitecode'=>$sitecode,
		    ]);
		?>
	    </div>
	    <?php  //Pjax::end();?>
	</div>
    </div>
</div>

<?=  ModalForm::widget([
    'id' => 'modal-config',
    'size'=>'modal-sm',
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-view',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#view-process').on('click', '.btn-view-data', function() {
    location.href = $(this).attr('data-url');
});

$('#tccbot-monitoring').on('click', '#config-btn', function() {
    modalConfig($(this).attr('data-url'));
});

setInterval(function () {
        // App 
	getConfig('container-app-cpu', 'container-app-ram', 'app');
	
	// Server CPU
	getConfig('container-server-cpu', 'container-server-ram', 'serv');

//	$.pjax.reload({container:'#buffe-table-server-pjax'});
	
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/tccbot/monitor/view-process'])."',
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#view-process').html(result);
	    }
	});
	
    }, ".($delay*1000).");
    
var chart = $('#container-server-ram').highcharts();

function getConfig(cpu_id, ram_id, type) {
    $.ajax({
	method: 'POST',
	url: '".Url::to(['/tccbot/monitor/get-point'])."',
	dataType: 'JSON',
	success: function(result, textStatus) {
	    var chart = $('#'+cpu_id).highcharts(),
            point,
            newVal;
	    
	    var cpu = 0;
	    if(type=='app') {
		cpu = result.data.cpu;
	    } else {
		cpu = result.data.cpu_serv;
	    }
	    
	    if (chart) {
		point = chart.series[0].points[0];
		newVal = Number(cpu);
		
		point.update(newVal);
	    }  
	    
	    var chart = $('#'+ram_id).highcharts(),
            point,
            newVal;
	    
	    var ram = 0;
	    if(type=='app') {
		ram = result.data.ram;
		//$('#ram_usage').html(result.data.ram_usage+'MB');
		
		chart.yAxis[0].setTitle({text: 'RAM App '+result.data.ram_usage+' MB'});
	    } else {
		ram = result.data.ram_serv;
		//$('#ram_serv_usage').html(result.data.ram_serv_usage+'MB');
		
		chart.yAxis[0].setTitle({text: 'RAM Server '+result.data.ram_serv_usage+' MB'});
	    }

	    if (chart) {
		point = chart.series[0].points[0];
		newVal = Number(ram);
		
		point.update(newVal);
	    }
	}
    });
}

function modalConfig(url) {
    $('#modal-config .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-config').modal('show')
    .find('.modal-content')
    .load(url);
}

function modalView(url) {
    $('#modal-view .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-view').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>