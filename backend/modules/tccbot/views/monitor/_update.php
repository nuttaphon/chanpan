<?php
use yii\helpers\Html;
Use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/**
 * _update file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 3 ส.ค. 2559 14:23:01
 * @link http://www.appxq.com/
 */
?>

<div class="inv-filter-sub-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Buffe Config</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'config_delay', ['inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">วินาที</span></div>'])->textInput(['maxlength' => true, 'type'=>'number']) ?>
	<?= $form->field($model, 'command_delay', ['inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">วินาที</span></div>'])->textInput(['maxlength' => true, 'type'=>'number']) ?>
	<?= $form->field($model, 'constants_delay', ['inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">วินาที</span></div>'])->textInput(['maxlength' => true, 'type'=>'number']) ?>
	<?= $form->field($model, 'template_delay', ['inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">วินาที</span></div>'])->textInput(['maxlength' => true, 'type'=>'number']) ?>
	<?= $form->field($model, 'sync_delay', ['inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">วินาที</span></div>'])->textInput(['maxlength' => true, 'type'=>'number']) ?>
	<?= $form->field($model, 'sync_nrec', ['inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon">วินาที</span></div>'])->textInput(['maxlength' => true, 'type'=>'number']) ?>
	
    </div>
    
    <div class="modal-footer">
	<?= Html::submitButton(Yii::t('app', 'Update'), ['class' =>'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'update') {
		$(document).find('#modal-config').modal('hide');
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>