<?php
use appxq\sdii\widgets\GridView;
use Yii;
/**
 * _process file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 ส.ค. 2559 11:58:41
 * @link http://www.appxq.com/
 */
?>

<?= GridView::widget([
    'id' => 'buffe-table-server',
    'panel'=>false,
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'tableOptions'=>['class' => 'table table-striped table-hover'],
    'columns' => [
	[
	    'attribute'=>'table',
	    'header'=>'ชื่อตาราง',
	    'value'=>function ($data){
		
		if($data['record']>0){
		    return yii\helpers\Html::a($data['table'], '#', ['class'=>'btn-view-data', 'data-url'=>  \yii\helpers\Url::to(['/tccbot/monitor/view-data', 'table'=>$data['table']])]);
		} else {
		    return $data['table'];
		}
	    },
	    'format'=>'raw',
	    'contentOptions'=>['style'=>'min-width:100px;'],
	],
	[
	    'attribute'=>'record',
	    'header'=>'นำเข้าแล้ว',
	    'value'=>function ($data){
		return number_format($data['record']);
	    },
	    'headerOptions'=>['style'=>'text-align: right;'],
	    'contentOptions'=>['style'=>'min-width:80px; text-align: right;'],
	],
	[
	    'attribute'=>'qleft',
	    'header'=>'คิว',
	    'value'=>function ($data){
		return number_format($data['qleft']);
	    },
	    'headerOptions'=>['style'=>'text-align: right;'],
	    'contentOptions'=>['style'=>'min-width:80px; text-align: right;'],
	],
	[
	    'attribute'=>'progress',
	    'header'=>'Progress',
	    'value'=>function ($data){ 
		$record = (int)$data['record'];
		$qleft = (int)$data['qleft'];
		$total = $record+$qleft;

		$percen = floatval($data['progress']);

		if($total>0){
		    $percen = number_format(($record/$total)*100);
		} else {
		    return '';
		}

		return '<div class="progress">
		    <div class="progress-bar" role="progressbar" aria-valuenow="'.$percen.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$percen.'%;">
		      '.$percen.'%
		    </div>
		  </div>';
	    },
	    'format'=>'raw',    
	    'headerOptions'=>['style'=>'text-align: center;'],
	    'contentOptions'=>['style'=>'text-align: center;'],
	],
    ],
]); ?>