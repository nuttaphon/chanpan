<?php
use appxq\sdii\widgets\GridView;
use yii\widgets\Pjax;
use backend\modules\tccbot\classes\TccBotQuery;

/**
 * _process file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 ส.ค. 2559 11:58:41
 * @link http://www.appxq.com/
 */
$this->title = 'TDC Monitoring';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View data';

$dataCol = TccBotQuery::getColumnBuffeData($table);

$columns = [];

if($dataCol){
    foreach ($dataCol as $key => $value) {
	$columns[] = [
	    'attribute'=>$value['Field'],
	    'contentOptions'=>['style'=>'min-width:100px;'],
	];
    }
}


?>
<?=  yii\helpers\Html::a('<i class="glyphicon glyphicon-chevron-left"></i> TDC Monitoring', ['index'], ['class'=>'btn btn-default'])?>
<br><br>
<?php  Pjax::begin(['id'=>'buffe-view-data-pjax', 'timeout' => 10000]);?>
<?= GridView::widget([
    'id' => 'buffe-view-data',
    'panel'=>true,
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'tableOptions'=>['class' => 'table table-striped table-hover'],
    'columns' => $columns,
]); ?>
<?php  Pjax::end();?>