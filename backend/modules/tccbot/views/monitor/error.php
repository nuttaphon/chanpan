<?php

/**
 * error file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 ส.ค. 2559 16:02:17
 * @link http://www.appxq.com/
 */
$this->title = 'TCC Bot Monitoring';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View data';
?>
<div class="alert alert-danger" role="alert">
  ไม่พบข้อมูล
</div>