<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 9/3/2559
 * Time: 16:53
 * E-mail: kongvut@gmail.com
 */
?>
<!-- Modal -->

<div class="modal-content">

    <div class="modal-body">

        <div class="panel panel-primary">
            <div class="panel-heading"><li class="fa fa-envelope"></li> Notification <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
            <div class="panel-body">
                <?php \yii\widgets\Pjax::begin(['enablePushState' => false]);?>
                <?= \yii\widgets\ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'notify-item'],
                    'options' => [
                        'tag' => 'div',
                        'class' => 'list-wrapper',
                        'id' => 'list-wrapper',
                    ],
                    //'layout' => "{pager}\n{items}\n{summary}",
                    'itemView' => '_list_notofy',
                    'pager' => [
                        'firstPageLabel' => 'first',
                        'lastPageLabel' => 'last',
                        'nextPageLabel' => 'next',
                        'prevPageLabel' => 'previous',
                        'maxButtonCount' => 3,
                    ],
                ]);
                ?>
                <?php \yii\widgets\Pjax::end();?>
            </div>
        </div>

    </div>
</div>
