<?php
use yii\helpers\Html;
$url =['/notification/app/test', 'page' => 10, 'sort' => 'name'];
?>
<div class="modal-content">

    <div class="modal-body">

        <div class="panel panel-primary">
            <div class="panel-heading"><li class="fa fa-envelope"></li>
                Notification <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button></div>
            <div class="panel-body">
           <!--Notification message -->
         <?php
         $num=1;
        foreach ($model as $value) {
            $user = common\models\UserProfile::findOne($value[from_user_id]);
            if($value[read]==0){
            ?>
           <div class="row" style="border-bottom: 1px solid #dddddd;padding:5px;">
          <?php
        }else
        {
           ?>
            <div class="row" style="border-bottom: 1px solid #dddddd;padding:5px;background-color:#e6e0e0">
              <?php
            }
               ?>
                <div class="col-md-1">
                <img src="<?php echo $user->avatar_base_url . '/' . $user->avatar_path ?>" height="40" width="40">
                </div>
               <div class="col-md-11">
            <b><?php echo $user->firstname . ' ' . $user->lastname ?></b>
            <i><?php   echo substr($value[message],0,150)   ?>...</i> <div class="read_reply" ref_dataid="<?php   echo $value[ref_dataid]   ?>" module="<?php   echo $value[module]   ?>" controller="<?php   echo $value[controller]   ?>" action="<?php   echo $value[action]   ?>"><?= Html::a('<span>read more</span>', $value[link_to], []) ?></div>
             <span><?php   echo $value[create_at]   ?></span>
               </div>
           </div>
           <?php
           $num++;
        }

           ?>
           </div>


            </div>
            </div>
        </div>
<?php
$jsNotifyAddRead = <<< JS
  $(".read_reply").on("click", function() {
  var url = "http://backend.yii2-starter-kit.dev/notification/notifaication/read-notify";
  var ref_dataid=parseInt($(this).attr('ref_dataid'));
  var module=$(this).attr('module');
  var controller=$(this).attr('controller');
  var action=$(this).attr('action');
  $.get(url, {
      ref_dataid:ref_dataid,
      module:module,
      controller:controller,
      action:action,
  });
  });
JS;
// register your javascript
$this->registerJs($jsNotifyAddRead);
?>
