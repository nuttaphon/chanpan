<?php
/**
 * Created by PhpStorm.
 * User: Kongvut Sangkla
 * Date: 9/3/2559
 * Time: 16:53
 * E-mail: kongvut@gmail.com
 */
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\helpers\Url;
function getAvatar($avatar_base_url,$avatar_path){
if($avatar_base_url==""){
$avatar="/img/anonymous.jpg";
}else{
$avatar=$avatar_base_url . '/' . $avatar_path;
}
return  $avatar;
}
$domain=Url::home();
?>
<div class="modal-content">

    <div class="modal-body">

        <div class="panel panel-primary">
            <div class="panel-heading"><li class="fa fa-envelope"></li>
                Notification <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button></div>
            <div class="panel-body">
           <!--Notification message -->
         <?php
         $num=1;
        foreach ($model as $value) {
            $user = common\models\UserProfile::findOne($value[from_user_id]);
            if($value[read]==0){
            ?>
           <div class="row" style="border-bottom: 1px solid #dddddd;padding:5px;">
          <?php
        }else
        {
           ?>
            <div class="row" style="border-bottom: 1px solid #dddddd;padding:5px;background-color:#e6e0e0">
              <?php
            }
               ?>
                <div class="col-md-1">
                <img src="<?php echo getAvatar($user->avatar_base_url,$user->avatar_path) ?>" height="40" width="40">
                </div>
               <div class="col-md-11">
            <b><?php echo $user->firstname . ' ' . $user->lastname ?></b>
            <i><?php   echo substr($value[message],0,150)   ?>...</i> <div class="read_reply" ref_dataid="<?php   echo $value[ref_dataid]   ?>" module="<?php   echo $value[module]   ?>" controller="<?php   echo $value[controller]   ?>" action="<?php   echo $value[action]   ?>"><?= Html::a('<span>read more</span>', $value[link_to], []) ?></div>
             <span><?php   echo $value[create_at]   ?></span>
               </div>
           </div>
           <?php
           $num++;
        }

           ?>
           </div>


            </div>
            </div>
            <?php
            // display pagination
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>
        </div>
<?php
$jsNotifyAddRead = <<< JS
  $(".read_reply").on("click", function() {
  var url = "$domain/notification/notification/read-notify";
  var ref_dataid=parseInt($(this).attr('ref_dataid'));
  var module=$(this).attr('module');
  var controller=$(this).attr('controller');
  var action=$(this).attr('action');
  $.get(url, {
      ref_dataid:ref_dataid,
      module:module,
      controller:controller,
      action:action,
  });
  });
JS;
// register your javascript
$this->registerJs($jsNotifyAddRead);
?>
