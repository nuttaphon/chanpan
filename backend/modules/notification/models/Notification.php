<?php

namespace backend\modules\notification\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property string $id
 * @property string $ref_dataid
 * @property string $message
 * @property string $from_user_id
 * @property string $to_user_id
 * @property integer $status
 * @property string $create_at
 * @property string $read
 * @property string $link_to
 * @property string $module
 * @property string $controller
 * @property string $action
 * @property string $options
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ref_dataid', 'from_user_id', 'status'], 'integer'],
            [['message', 'to_user_id', 'read', 'link_to', 'options'], 'string'],
            [['create_at'], 'safe'],
            [['module', 'controller', 'action'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ref_dataid' => 'Ref Dataid',
            'message' => 'ข้อความ',
            'from_user_id' => 'ส่งจาก',
            'to_user_id' => 'ส่งถึง',
            'status' => 'สถานะ',
            'create_at' => 'วันที่ส่ง',
            'read' => 'อ่านแล้ว',
            'link_to' => 'Link',
            'module' => 'Module',
            'controller' => 'Controller',
            'action' => 'Action',
            'options' => 'Options',
        ];
    }
}
