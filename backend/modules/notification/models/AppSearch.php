<?php

namespace backend\modules\notification\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\notification\models\Notification;

/**
 * AppSearch represents the model behind the search form about `backend\modules\notification\models\Notification`.
 */
class AppSearch extends Notification
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from_user_id', 'ref_dataid', 'status'], 'integer'],
            [['message', 'to_user_id', 'create_at', 'read', 'link_to', 'options', 'module', 'controller', 'action'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notification::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'from_user_id' => $this->from_user_id,
            'to_user_id' => $this->to_user_id,
            'status' => $this->status,
            'create_at' => $this->create_at,
            'read' => $this->read,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'link_to', $this->link_to])
            ->andFilterWhere(['like', 'options', $this->options]);

        return $dataProvider;
    }
}
