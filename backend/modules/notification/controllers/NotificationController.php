<?php

namespace backend\modules\notification\controllers;

use Yii;
use backend\modules\notification\models\Notification;
use backend\modules\notification\models\AppSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\helpers\Url;

/**
 * AppController implements the CRUD actions for Notification model.
 */
class NotificationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notification model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Notification();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Notification model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Notification model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionListNotify(){

        $user_id=Yii::$app->user->identity->id;
        $searchModel = new AppSearch();

/*
        $sql="SELECT *  FROM `notification` WHERE `to_user_id` LIKE '%$user_id%'   ORDER BY `id` DESC LIMIT 0,20 ";
     $model=Yii::$app->db->createCommand($sql)->queryAll();
*/
     $query = Notification::find()->where("to_user_id =:id", [':id'=>$user_id])->orderBy(['id'=>SORT_DESC]);


     // get the total number of articles (but do not fetch the article data yet)
     //$count = $query->count();

     // create a pagination object with the total count
     $pagination = new Pagination(['totalCount' => $count]);
     //$pagination->setPageSize(2, $validatePageSize =flase);
     // limit the query using the pagination and retrieve the articles
     $articles = $query->offset($pagination->offset)
         ->limit($pagination->limit)
         ->all();

        // $query->pagination->pageSize = 3;

        return $this->renderAjax('list_notify', [
          'model' => $articles,
          'pages' => $pagination,
        ]);
    }
    public function CheckNotify($user_id) {
    $sql="SELECT COUNT(*) as total  FROM `notification` WHERE `to_user_id` LIKE '%$user_id%' and  `read` = '0'";
     $notify_count=Yii::$app->db->createCommand($sql)->queryAll();
     //\yii\helpers\VarDumper::dump($notify_count,10,true);
     //exit();
       return $notify_count[0][total];
    }
    public function SendNotify($ref_dataid,$message,$user,$status,$read,$link,$link_to,$options) {
        $from_user_id=$user[from];
        $to_user_id=$user[to];
        $link_module=$link[module];
        $link_controller=$link[controller];
        $link_action=$link[action];
        $create_at=date("Y-m-d H:i:s");

        $user_to_expolde=explode(",", $user[to]);
        $total_user=count($user_to_expolde);
        for ($i=0; $i < $total_user; $i++) {
          $sql="INSERT INTO `notification` "
                  . "(`id`, `ref_dataid`, `message`, `from_user_id`, `to_user_id`, `status`, `create_at`, `read`, `link_to`, `module`, `controller`, `action`, `options`)"
                  . " VALUES "
                  . "(NULL, '$ref_dataid', '$message', '$from_user_id', '$user_to_expolde[$i]', '$status', '$create_at', '$read', '$link_to', '$link_module', '$link_controller', '$link_action', '$options')";
       Yii::$app->db->createCommand($sql)->execute();
        }
    }

    public function actionReadNotify($ref_dataid,$module,$controller,$action) {


     $sql="UPDATE `notification` SET `read` = '1' WHERE  `ref_dataid` ='$ref_dataid' and  `module` ='$module' and `controller` ='$controller' and `action` ='$action';";
     Yii::$app->db->createCommand($sql)->execute();
     exit();
    }
    public function actionTest() {
             $ref_dataid=1;
             $message="nut";
             $user[from]="1435745159010041100";
             $user[to]="1435745159010041100";
             $status=0;
             $read=0;
             $link[module]="test";
             $link[controller]="test";
             $link[action]="test";
             $link_to="www.google.co.th";
             $options="yes";
             $testcount=  $this->CheckNotify($user[to]);
             echo "count=".$testcount."<br>";
             //\yii\helpers\VarDumper::dump($testcount, 10,true);
             //$this->SendNotify($ref_dataid,$message,$user,$status,$read,$link,$link_to,$options);


    }

    public function CheckAdminNotification()
    {
      $sql="SELECT user_id  FROM `rbac_auth_assignment` WHERE `item_name` LIKE '%administrator%' ";
   $model=Yii::$app->db->createCommand($sql)->queryAll();
   $num=0;
   foreach ($model as $value) {

       $user_to.=$model[$num][user_id].",";

     $num++;
   }
   $user[from] = Yii::$app->user->identity->userProfile->user_id;
   $user[to]=substr($user_to,0,-1);
   return $user;
    }


}
