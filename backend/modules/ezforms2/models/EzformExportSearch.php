<?php

namespace backend\modules\ezforms2\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ezforms2\models\EzformExport;

/**
 * EzformExportSearch represents the model behind the search form about `backend\modules\ezforms2\models\EzformExport`.
 */
class EzformExportSearch extends EzformExport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tbname', 'export_sql', 'export_select', 'export_join', 'export_where', 'lable_extra'], 'safe'],
            [['status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EzformExport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'tbname', $this->tbname])
            ->andFilterWhere(['like', 'export_sql', $this->export_sql])
            ->andFilterWhere(['like', 'export_select', $this->export_select])
            ->andFilterWhere(['like', 'export_join', $this->export_join])
            ->andFilterWhere(['like', 'export_where', $this->export_where])
            ->andFilterWhere(['like', 'lable_extra', $this->lable_extra]);

        return $dataProvider;
    }
}
