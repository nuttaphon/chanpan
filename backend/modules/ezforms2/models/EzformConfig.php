<?php

namespace backend\modules\ezforms2\models;

use Yii;

/**
 * This is the model class for table "ezform_config".
 *
 * @property integer $config_id
 * @property string $config_type
 * @property integer $ezf_id
 * @property string $config_name
 * @property string $config_value
 * @property string $config_options
 */
class EzformConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['config_id', 'config_type', 'ezf_id', 'config_name'], 'required'],
            [['config_id', 'ezf_id'], 'integer'],
            [['config_value', 'config_options'], 'string'],
            [['config_type'], 'string', 'max' => 100],
            [['config_name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'config_id' => Yii::t('app', 'ID'),
            'config_type' => Yii::t('app', 'Type'),
            'ezf_id' => Yii::t('app', 'Ezf ID'),
            'config_name' => Yii::t('app', 'การตั้งค่า'),
            'config_value' => Yii::t('app', 'ค่าเริ่มต้น'),
            'config_options' => Yii::t('app', 'Options'),
        ];
    }
}
