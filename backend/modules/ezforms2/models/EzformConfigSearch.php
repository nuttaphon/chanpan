<?php

namespace backend\modules\ezforms2\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ezforms2\models\EzformConfig;

/**
 * EzformConfigSearch represents the model behind the search form about `backend\modules\ezforms2\models\EzformConfig`.
 */
class EzformConfigSearch extends EzformConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['config_id', 'ezf_id'], 'integer'],
            [['config_type', 'config_name', 'config_value', 'config_options'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ezf_id, $type)
    {
        $query = EzformConfig::find()->where('ezf_id=:ezf_id AND config_type=:type', [':ezf_id'=>$ezf_id, ':type'=>$type]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'config_id' => $this->config_id,
            'ezf_id' => $this->ezf_id,
        ]);

        $query->andFilterWhere(['like', 'config_type', $this->config_type])
            ->andFilterWhere(['like', 'config_name', $this->config_name])
            ->andFilterWhere(['like', 'config_value', $this->config_value])
            ->andFilterWhere(['like', 'config_options', $this->config_options]);

        return $dataProvider;
    }
}
