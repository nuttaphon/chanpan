<?php

namespace backend\modules\ezforms2\models;

use Yii;

/**
 * This is the model class for table "ezform_input".
 *
 * @property integer $input_id
 * @property string $input_name
 * @property string $input_class
 * @property string $input_function
 * @property string $input_class_validate
 * @property string $input_function_validate
 * @property string $system_class
 * @property string $input_data
 * @property string $input_validate
 * @property string $input_specific
 * @property string $input_option
 * @property string $table_field_type
 * @property integer $table_field_length
 * @property string $input_version
 * @property double $input_order
 */
class EzformInput extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_input';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['input_name', 'system_class','input_function', 'table_field_type'], 'required'],
            [['input_data', 'input_validate', 'input_specific', 'input_option'], 'string'],
            [['table_field_length'], 'integer'],
            [['input_order'], 'number'],
            [['input_name', 'table_field_type', 'input_version'], 'string', 'max' => 50],
            [['input_class', 'input_class_validate'], 'string', 'max' => 80],
	    [['system_class'], 'string', 'max' => 100],
            [['input_function', 'input_function_validate'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'input_id' => Yii::t('app', 'ID'),
            'input_name' => Yii::t('app', 'Name'),
            'input_class' => Yii::t('app', 'Class'),
            'input_function' => Yii::t('app', 'Function'),
            'input_class_validate' => Yii::t('app', 'Class validate'),
            'input_function_validate' => Yii::t('app', 'Function validate'),
	    'system_class' => Yii::t('app', 'System Class'),
	    'input_data' => Yii::t('app', 'Data Items'),
            'input_validate' => Yii::t('app', 'Validate'),
            'input_specific' => Yii::t('app', 'Specific'),
            'input_option' => Yii::t('app', 'Option'),
            'table_field_type' => Yii::t('app', 'Field Type'),
            'table_field_length' => Yii::t('app', 'Field Length'),
            'input_version' => Yii::t('app', 'Version'),
            'input_order' => Yii::t('app', 'Order'),
        ];
    }
}
