<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformFields */

$this->title = Yii::t('app', 'Create Ezform Fields');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ezform Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-fields-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
