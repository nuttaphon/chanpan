<?php
use appxq\sdii\widgets\ModalForm;

/**
 * _report_show_widget file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 5 ต.ค. 2559 16:36:20
 * @link http://www.appxq.com/
 */

?>
<div class="row">
    <div class="col-md-12 text-center">
	<button class="btn btn-info" id="show-report"><i class="fa fa-line-chart"></i> แสดงรายงาน</button>
    </div>
</div>

<br>
<div id="report-show-box" style="display: none;">
    <?php
    echo $this->render('_show', [
			    'ezform' => $ezform,
			    'target' => $target,
			]);
    ?>
</div>

<?=  ModalForm::widget([
    'id' => 'modal-ezform-config',
    //'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("


$('#show-report').click(function(){
    if($('#report-show-box').css('display')=='none'){
	$('#report-show-box').show('slow');
	
	
    } else {
	$('#report-show-box').hide('slow');
    }
    
});

$('#ezform-config-grid-pjax').on('click', '#modal-addbtn-ezform-config', function() {
    modalEzformConfig($(this).attr('data-url'));
});

function modalEzformConfig(url) {
    $('#modal-ezform-config .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ezform-config').modal('show')
    .find('.modal-content')
    .load(url);
}

");

/*
 * $.ajax({
	    method: 'POST',
	    url:'".\yii\helpers\Url::to(['/ezforms2/ezform-config/show'])."',
	    data:{ezform:".yii\helpers\Json::encode($ezform).", target:'$target'},
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    $('#report-show-box').html(result.html);
		    $('#report-show-box').show('slow');
		}
	    }
	});
 */
?>