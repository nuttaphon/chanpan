<?php

/**
 * _show file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 5 ต.ค. 2559 16:55:21
 * @link http://www.appxq.com/
 */
$user_id = Yii::$app->user->id;

?>

<?php foreach ($ezform as $key => $ezf_id):?>
    <?php
    $ezfConfig = \backend\modules\ezforms2\classes\EzfQuery::getEzformReportById($ezf_id);
    
    ?>
    <?php if($ezfConfig):?>
	
	    <div class="alert alert-warning">
		<span class="h3">รายงานฟอร์ม <?=$ezfConfig['ezf_name'];?></span>
		<a class="pull-right edit-report" style="cursor: pointer;" data-id="<?=$ezfConfig['config_id'];?>" data-ezfid="<?=$ezfConfig['ezf_id'];?>" data-type="report" ><span class="fa fa-cog fa-2x"></span></a>
	    </div>
	    <?php
	    $ezfReport = \backend\modules\ezforms2\classes\EzfQuery::getEzformReportByUser($user_id, $ezf_id);
	    if(empty($ezfReport)){
		$ezfReport = \backend\modules\ezforms2\classes\EzfQuery::getEzformReportByIdAll($ezf_id);
	    }
	    //miloschuman\highcharts\Highcharts::widget([]);
	    ?>
	    
	    <div class="container">
	    <div class="row">
	    <?php foreach ($ezfReport as $report_key => $report_value):?>
		<?php
		    if($report_value['config_name']=='line_graph'){
			echo '<div class="col-md-6">';
			echo $this->render('_line_graph', [
			    'config' => $report_value,
			    'ezf_id' => $ezf_id,
			    'ezf_name' => $ezfConfig['ezf_name'],
			    'ezf_table' => $ezfConfig['ezf_table'],
			    'target' => $target,
			]);
		    echo '</div>';
		    } elseif ($report_value['config_name']=='bar_chart') {
			echo '<div class="col-md-6">';
			echo $this->render('_bar_chart', [
			    'config' => $report_value,
			    'ezf_id' => $ezf_id,
			    'ezf_name' => $ezfConfig['ezf_name'],
			    'ezf_table' => $ezfConfig['ezf_table'],
			    'target' => $target,
			]);
		    echo '</div>';
		    } elseif ($report_value['config_name']=='pie') {
			echo '<div class="col-md-6">';
			echo $this->render('_pie', [
			    'config' => $report_value,
			    'ezf_id' => $ezf_id,
			    'ezf_name' => $ezfConfig['ezf_name'],
			    'ezf_table' => $ezfConfig['ezf_table'],
			    'target' => $target,
			]);
			echo '</div>';
		    }
		?>
	    <?php endforeach; ?>
	    </div>
	    </div>
    <?php endif; ?>
<?php endforeach; ?>

<?php  $this->registerJs("


$('.edit-report').click(function(){
    
});

");?>
