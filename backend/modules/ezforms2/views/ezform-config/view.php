<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformConfig */

$this->title = 'Ezform Config#'.$model->config_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ezform Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-config-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'config_id',
		'config_type',
		'ezf_id',
		'config_name',
		'config_value:ntext',
		'config_options:ntext',
	    ],
	]) ?>
    </div>
</div>
