<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ezforms2\models\EzformExportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ezform Exports');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ezform-export-index">

    <?php  Pjax::begin(['id'=>'ezform-export-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'ezform-export-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['ezform-export/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ezform-export']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['ezform-export/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-ezform-export', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionEzformExportIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'tbname',
            //'export_sql:ntext',
            'export_select:ntext',
            //'export_join:ntext',
            //'export_where:ntext',
             'lable_extra:ntext',
             'status',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-ezform-export',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#ezform-export-grid-pjax').on('click', '#modal-addbtn-ezform-export', function() {
    modalEzformExport($(this).attr('data-url'));
});

$('#ezform-export-grid-pjax').on('click', '#modal-delbtn-ezform-export', function() {
    selectionEzformExportGrid($(this).attr('data-url'));
});

$('#ezform-export-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#ezform-export-grid').yiiGridView('getSelectedRows');
	disabledEzformExportBtn(key.length);
    },100);
});

$('#ezform-export-grid-pjax').on('click', '.selectionEzformExportIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledEzformExportBtn(key.length);
});

$('#ezform-export-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalEzformExport('".Url::to(['ezform-export/update', 'id'=>''])."'+id);
});	

$('#ezform-export-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalEzformExport(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ezform-export-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledEzformExportBtn(num) {
    if(num>0) {
	$('#modal-delbtn-ezform-export').attr('disabled', false);
    } else {
	$('#modal-delbtn-ezform-export').attr('disabled', true);
    }
}

function selectionEzformExportGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionEzformExportIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ezform-export-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalEzformExport(url) {
    $('#modal-ezform-export .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ezform-export').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>