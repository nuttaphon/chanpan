<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformExport */

$this->title = Yii::t('app', 'Create Ezform Export');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ezform Exports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-export-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
