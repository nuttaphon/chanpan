<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezforms2\models\EzformInput */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="ezform-input-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Ezform Input</h4>
    </div>

    <div class="modal-body">
	
	<div class="row">
		<div class="col-md-6 ">
		    <?= $form->field($model, 'input_name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-6 sdbox-col">
		    <?= $form->field($model, 'system_class')->textInput(['maxlength' => true]) ?>
		    
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 ">
		    <?= $form->field($model, 'input_class')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-6 sdbox-col">
		    <?= $form->field($model, 'input_function')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6 ">
		    <?= $form->field($model, 'table_field_type')->dropDownList([
			'none'=>'(NOT SET)',
			'VARCHAR'=>'VARCHAR',
			'INT'=>'INT',
			'TEXT'=>'TEXT',
			'DATE'=>'DATE',
			'DATETIME'=>'DATETIME',
			'DOUBLE'=>'DOUBLE',
			'TINYINT'=>'TINYINT',
			'BIGINT'=>'BIGINT',
			'LONGTEXT'=>'LONGTEXT',
		    ]) ?>
		</div>
		<div class="col-md-6 sdbox-col">
		    <?= $form->field($model, 'table_field_length')->textInput(['type'=>'number']) ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6 ">
		    <?= $form->field($model, 'input_data')->textarea(['rows' => 3]) ?>
		</div>
		<div class="col-md-6 sdbox-col">
		    <?= $form->field($model, 'input_validate')->textarea(['rows' => 3]) ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6 ">
		    <?= $form->field($model, 'input_specific')->textarea(['rows' => 3]) ?>
		</div>
		<div class="col-md-6 sdbox-col">
		    <?= $form->field($model, 'input_option')->textarea(['rows' => 3]) ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-3 ">
		    <?= $form->field($model, 'input_order')->textInput(['type'=>'number']) ?>
		</div>
		
	</div>
	
	<?= $form->field($model, 'input_version')->hiddenInput()->label(false) ?>

	
	
	<?= $form->field($model, 'input_class_validate')->hiddenInput()->label(false) ?>

	<?= $form->field($model, 'input_function_validate')->hiddenInput()->label(false) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(document).find('#modal-ezform-input').modal('hide');
		$.pjax.reload({container:'#ezform-input-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-ezform-input').modal('hide');
		$.pjax.reload({container:'#ezform-input-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>