<?php
namespace backend\modules\ezforms2\classes;

use Yii;

/**
 * OvccaQuery class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class EzfQuery {
    public static function getIntUserAll() {
	$sql = "SELECT user_id as id, 
		    CONCAT(firstname, ' ', lastname) AS text
		FROM user_profile
		";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getInputv2All() {
	$sql = "SELECT *
		FROM ezform_input
		WHERE input_version='v2'
		ORDER BY input_order
		";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getFieldsCountById($id) {
	$sql = "SELECT count(*) AS num
		FROM ezform_fields
		WHERE ezf_id=:id AND ezf_field_ref_field IS NULL
		";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryScalar();
    }
    
    public static function getEzformById($id) {
	
	$sql = "SELECT ezf_id, ezf_name, ezf_table, comp_id_target, field_detail, unique_record FROM ezform WHERE ezf_id = :id";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryOne();
    }
    
    public static function getConditionFieldsName($field, $cond) {
	$sql = "SELECT $field
		FROM ezform_fields 
		WHERE ezform_fields.ezf_field_id in($cond) ";
	
	if($cond!=''){
	    return Yii::$app->db->createCommand($sql)->queryAll();
	}
	return '';
    }
    
    public static function deleteChoice($ezf_field_id) {
	$sql = "DELETE FROM `ezform_choice` WHERE `ezf_field_id` = :id ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$ezf_field_id])->execute();
    }
    
    public static function deleteCondition($ezf_id, $ezf_field_name) {
	$sql = "DELETE FROM `ezform_condition` WHERE `ezf_id` = :id AND `ezf_field_name` = :name ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$ezf_id, ':name'=>$ezf_field_name])->execute();
    }
    
    public static function getCondition($ezf_id, $ezf_field_name) {
	$sql = "SELECT *
		FROM ezform_condition
		WHERE ezform_condition.ezf_id = :ezf_id AND ezform_condition.ezf_field_name = :ezf_field_name
		ORDER BY ezform_condition.cond_id;";

	return Yii::$app->db->createCommand($sql, [':ezf_id'=>$ezf_id, ':ezf_field_name'=>$ezf_field_name])->queryAll();
    }
    
    public static function getEzformReportById($id) {
	
	$sql = "SELECT ezform.ezf_id, ezform.ezf_name, ezf_table, comp_id_target, field_detail, unique_record , ezform_config.*
		FROM ezform INNER JOIN ezform_config ON ezform_config.ezf_id = ezform.ezf_id
		WHERE ezform.ezf_id = :id AND ezform_config.config_type = 'report' ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryOne();
    }
    
    public static function getEzformReportByIdAll($id) {
	
	$sql = "SELECT ezform_config.*
		FROM ezform_config
		WHERE ezform_config.ezf_id = :id AND ezform_config.config_type = 'report' ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryAll();
    }
    
    public static function getEzformReportByUser($user_id, $ezf_id) {
	
	$sql = "SELECT ezform_config.*
		FROM ezform_report INNER JOIN ezform_config ON ezform_config.config_id = ezform_report.config_id
		WHERE ezform_report.ezf_id = :id AND ezform_report.user_id = :user_id ";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$ezf_id, ':user_id'=>$user_id])->queryAll();
    }
}
