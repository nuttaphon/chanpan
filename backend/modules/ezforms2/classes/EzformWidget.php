<?php

namespace backend\modules\ezforms2\classes;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

class EzformWidget extends Html {
    
    /*
     * echo $form->field($model, 'ezf_name')->inline()->radioList([
						'data'=>[1,2,3,4,5,6], 
						'other'=>[
						    1=>[
							'attribute'=>'ezf_id', 
							'suffix'=>'หน่วย2'
						    ],
						    5=>[
							'attribute'=>'ezf_id', 
							'suffix'=>'หน่วย'
						    ]
						]
					]);
     */
    public static function radioList($name, $selection = null, $items = [], $options = []) {
	$encode = !isset($options['encode']) || $options['encode'];
	$formatter = isset($options['item']) ? $options['item'] : null;
	$itemOptions = isset($options['itemOptions']) ? $options['itemOptions'] : [];
	$lines = [];
	$index = 0;
	if(isset($items['data']) && !empty($items['data'])){
	    $items_other = isset($items['other'])?$items['other']:[];
	    
	    foreach ($items['data'] as $value => $label) {
		$checked = $selection !== null &&
			(!is_array($selection) && !strcmp($value, $selection) || is_array($selection) && in_array($value, $selection));
		
		$other = $items_other[$value];
		
		if ($formatter !== null) {
		    $lines[] = call_user_func($formatter, $index, $label, $name, $checked, $value, $other);
		} else {
		    $lines[] = static::radio($name, $checked, array_merge($itemOptions, [
				'value' => $value,
				'label' => '<span>'.($encode ? static::encode($label) : $label).'</span> ',
		    ]));
		}
		$index++;
	    }
	}

	$separator = isset($options['separator']) ? $options['separator'] : "\n";
	if (isset($options['unselect'])) {
	    // add a hidden field so that if the list box has no option being selected, it still submits a value
	    $hidden = static::hiddenInput($name, $options['unselect']);
	} else {
	    $hidden = '';
	}

	$tag = isset($options['tag']) ? $options['tag'] : 'div';
	unset($options['tag'], $options['unselect'], $options['encode'], $options['separator'], $options['item'], $options['itemOptions']);

	return $hidden . static::tag($tag, implode($separator, $lines), $options);
    }

}

?>
