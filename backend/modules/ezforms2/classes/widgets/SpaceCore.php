<?php

namespace backend\modules\ezforms2\classes\widgets;

use Yii;
use yii\base\Object;
use yii\base\InvalidConfigException;
use appxq\sdii\utils\SDUtility;
use yii\web\View;

/**
 * TextInput class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 19 ส.ค. 2559 17:27:52
 * @link http://www.appxq.com/
 * @example backend\modules\ezforms2\classes\widgets\TextInput
 */
class SpaceCore extends Object {

    const BEHAVIOR_CLASS_NAME = '';
    
    /**
     * Initializes this TextInput.
     */
    public function init() {
	
    }

    public function generateViewEditor($input, $model) {
	$view = new View();
	
	return $view->renderAjax('/widgets/_space_view_editor', [
	    'model'=>$model,
	]);
    }
    
    public function generateViewInput($field) {
	$view = new View();
	
	return $view->renderAjax('/widgets/_space_view_item', [
	    'field'=>$field,
	]);
    }

    public function generateOptions($input, $model) {
	return '';
    }
    
    public function generateValidations($input, $model) {
	return '';
    }
    
}
