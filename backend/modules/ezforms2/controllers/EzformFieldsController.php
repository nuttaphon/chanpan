<?php

namespace backend\modules\ezforms2\controllers;

use Yii;
use backend\modules\ezforms2\models\EzformFields;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use backend\modules\ezforms2\classes\EzfFunc;
use backend\modules\ezforms2\classes\EzfQuery;
use common\lib\codeerror\helpers\GenMillisecTime;
use appxq\sdii\utils\SDUtility;
use yii\helpers\ArrayHelper;

/**
 * EzformFieldsController implements the CRUD actions for EzformFields model.
 */
class EzformFieldsController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => [], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view-input', 'create', 'update', 'delete', 'clone'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * Creates a new EzformFields model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($ezf_id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new EzformFields();
	    $model->ezf_id = $ezf_id;
	    $model->ezf_field_id = GenMillisecTime::getMillisecTime();
	    $model->ezf_field_lenght = 12;
	    $model->ezf_field_label = 'คำถามไม่ระบุหัวข้อ';
	    $model->ezf_field_name = EzfFunc::generateFieldName($ezf_id);
	    $model->ezf_field_order = EzfQuery::getFieldsCountById($ezf_id);

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		//fix
		$model->ezf_id = $ezf_id;
		$model->ezf_field_id = GenMillisecTime::getMillisecTime();
		
		$dataInput;
	    
		if(isset(Yii::$app->session['ezf_input'])){
		    $dataInput = EzfFunc::getInputByArray($model->ezf_field_type, Yii::$app->session['ezf_input']);
		}
		
		if($dataInput){
		    $model->table_field_type = $dataInput['table_field_type'];
		    $model->table_field_length = $dataInput['table_field_length'];
		   
		    //set data 
		    //EX. data['items'=>[], 'func'=>'', 'builder'=>[], 'delete'=>[]]
		    $dataType = isset($_POST['options']['data-type'])?$_POST['options']['data-type']:null;
		    $model->ezf_field_data = EzfFunc::setEzfData($_POST['data'], $model->ezf_field_id, $id, $model->ezf_field_name, $model->ezf_field_name, $dataType);
		    
		    //set options
		    $model->ezf_field_options = EzfFunc::setEzfFields($dataInput['input_option'], $_POST['options'], FALSE);
		    
		    //fix specific
		    $model->ezf_field_specific = EzfFunc::setSpecific($dataInput, $_POST['options']);
		    
		    //set validation
		    $model->ezf_field_validate = EzfFunc::setEzfFields($dataInput['input_validate'], $_POST['validate']);
		
		    
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'ไม่พบประเภทคำถามที่เลือก'),
			'data' => $model,
		    ];
		    return $result;
		}
		
		//Create table fields
		$alterTable = true;

		if($dataInput['table_field_type']!='none'){
		    $dataEzf = Yii::$app->session['ezform'];
		    $alterTable = EzfFunc::alterTableAdd($dataEzf['ezf_table'], $model->ezf_field_name, $model->table_field_type, $model->table_field_length);
		}

		if(!$alterTable){
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'สร้างคอลัมน์ไม่สำเสร็จ'),
			'data' => $model,
		    ];
		    return $result;
		}
		
		if ($model->save()) {
		    $inputWidget = Yii::createObject($dataInput['system_class']);
		    $htmlInput = $inputWidget->generateViewInput($model->attributes);
		    $html = EzfFunc::createChildrenItem($model->attributes, $htmlInput);
		    
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
			'html'=>$html,
			'alterTable'=>$alterTable,
		    ];
		    
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionSpace($ezf_id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    $model = new EzformFields();
	    $model->ezf_id = $ezf_id;
	    $model->ezf_field_id = GenMillisecTime::getMillisecTime();
	    $model->ezf_field_lenght = 6;
	    $model->ezf_field_label = '';
	    $model->ezf_field_name = EzfFunc::generateFieldName($ezf_id);
	    $model->ezf_field_order = EzfQuery::getFieldsCountById($ezf_id);
	    $model->ezf_field_type = 57;
	    $model->table_field_type = 'none';
	    
	    if ($model->save()) {
		if(isset(Yii::$app->session['ezf_input'])){
		    $dataInput = EzfFunc::getInputByArray($model->ezf_field_type, Yii::$app->session['ezf_input']);
		}
		if($dataInput){
		    $inputWidget = Yii::createObject($dataInput['system_class']);
		    $htmlInput = $inputWidget->generateViewInput($model->attributes);
		    $html = EzfFunc::createChildrenItem($model->attributes, $htmlInput);
		    //Create table fields
		    $alterTable = true;
		    
		    
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
			'html'=>$html,
			'alterTable'=>$alterTable,
		    ];
		    return $result;
		}
		
	    } 
	    $result = [
		'status' => 'error',
		'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		'data' => $model,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionClone($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $modelClone = $this->findModel($id);
	    
	    $model = new EzformFields();
	    $model->attributes = $modelClone->attributes;
	    $model->ezf_field_id = GenMillisecTime::getMillisecTime();
	    $model->ezf_field_name = EzfFunc::generateFieldName($modelClone->ezf_id);
	    $model->ezf_field_order = EzfQuery::getFieldsCountById($modelClone->ezf_id);
	    
	    $data = [];
	    $field_data = SDUtility::string2Array($modelClone->ezf_field_data);
	    if(isset($field_data['builder']) && !empty($field_data['builder'])){
		foreach ($field_data['builder'] as $key => $value) {
		    $id = GenMillisecTime::getMillisecTime();
		    $value['action'] = 'create';
		    
		    $data['builder'][$id] = $value;
		}
	    }
	    
	    $model->ezf_field_data = $data;
	    $model->ezf_field_options = SDUtility::string2Array($modelClone->ezf_field_options);
	    $model->ezf_field_specific = SDUtility::string2Array($modelClone->ezf_field_specific);
	    $model->ezf_field_validate = SDUtility::string2Array($modelClone->ezf_field_validate);
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$model->ezf_field_id = GenMillisecTime::getMillisecTime();
		
		$dataInput;
	    
		if(isset(Yii::$app->session['ezf_input'])){
		    $dataInput = EzfFunc::getInputByArray($model->ezf_field_type, Yii::$app->session['ezf_input']);
		}
		if($dataInput){
		    
		    $model->table_field_type = $dataInput['table_field_type'];
		    $model->table_field_length = $dataInput['table_field_length'];
		   
		    //set data 
		    //EX. data['items'=>[], 'func'=>'', 'builder'=>[], 'delete'=>[]]
		    $dataType = isset($_POST['options']['data-type'])?$_POST['options']['data-type']:null;
		    $model->ezf_field_data = EzfFunc::setEzfData($_POST['data'], $model->ezf_field_id, $id, $model->ezf_field_name, $model->ezf_field_name, $dataType);
		    
		    //set options
		    $model->ezf_field_options = EzfFunc::setEzfFields($dataInput['input_option'], $_POST['options'], FALSE);
		    
		    //fix specific
		    $model->ezf_field_specific = EzfFunc::setSpecific($dataInput, $_POST['options']);
		    
		    //set validation
		    $model->ezf_field_validate = EzfFunc::setEzfFields($dataInput['input_validate'], $_POST['validate']);
		
		    
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'ไม่พบประเภทคำถามที่เลือก'),
			'data' => $model,
		    ];
		    return $result;
		}
		
		//Create table fields
		$alterTable = true;

		if($dataInput['table_field_type']!='none'){
		    $dataEzf = Yii::$app->session['ezform'];
		    $alterTable = EzfFunc::alterTableAdd($dataEzf['ezf_table'], $model->ezf_field_name, $model->table_field_type, $model->table_field_length);
		}

		if(!$alterTable){
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'สร้างคอลัมน์ไม่สำเสร็จ'),
			'data' => $model,
		    ];
		    return $result;
		}
		
		if ($model->save()) {
		    $inputWidget = Yii::createObject($dataInput['system_class']);
		    $htmlInput = $inputWidget->generateViewInput($model->attributes);
		    $html = EzfFunc::createChildrenItem($model->attributes, $htmlInput);
		    
		    
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
			'html'=>$html,
			'alterTable'=>$alterTable,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    /**
     * Updates an existing EzformFields model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $ezf_field_id
     * @param integer $ezf_field_icon
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);
	    $model->ezf_field_data = SDUtility::string2Array($model->ezf_field_data);
	    $model->ezf_field_options = SDUtility::string2Array($model->ezf_field_options);
	    $model->ezf_field_specific = SDUtility::string2Array($model->ezf_field_specific);
	    $model->ezf_field_validate = SDUtility::string2Array($model->ezf_field_validate);
	    
	    $oldName = $model->ezf_field_name;
	    $oldType = $model->table_field_type;
	    $oldLength = $model->table_field_length;
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$dataInput;
	    
		if(isset(Yii::$app->session['ezf_input'])){
		    $dataInput = EzfFunc::getInputByArray($model->ezf_field_type, Yii::$app->session['ezf_input']);
		}
		if($dataInput){
		    $model->table_field_type = $dataInput['table_field_type'];
		    $model->table_field_length = $dataInput['table_field_length'];
		   
		    //set data 
		    //EX. data['items'=>[], 'func'=>'', 'builder'=>[], 'delete'=>[]]
		    $dataType = isset($_POST['options']['data-type'])?$_POST['options']['data-type']:null;
		    $model->ezf_field_data = EzfFunc::setEzfData($_POST['data'], $model->ezf_field_id, $model->ezf_id, $oldName, $model->ezf_field_name, $dataType);
		    
		    //set options
		    $model->ezf_field_options = EzfFunc::setEzfFields($dataInput['input_option'], $_POST['options'], FALSE);
		    
		    //fix specific
		    $model->ezf_field_specific = EzfFunc::setSpecific($dataInput, $_POST['options']);
		    
		    //set validation
		    $model->ezf_field_validate = EzfFunc::setEzfFields($dataInput['input_validate'], $_POST['validate']);
		
		    
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'ไม่พบประเภทคำถามที่เลือก'),
			'data' => $model,
		    ];
		    return $result;
		}
		
		//Create table fields
		$alterTable = true;

		if($model->ezf_field_name != $oldName || $model->table_field_type != $oldType || $model->table_field_length != $oldLength){
		    $dataEzf = Yii::$app->session['ezform'];

		    if($oldType=='none' && $model->table_field_type!='none'){
			$alterTable = EzfFunc::alterTableAdd($dataEzf['ezf_table'], $model->ezf_field_name, $model->table_field_type, $model->table_field_length);
		    } elseif($oldType!='none' && $model->table_field_type=='none') {
			$alterTable = EzfFunc::alterDropColumn($dataEzf['ezf_table'], $oldName);
		    } elseif($oldType!='none' && $model->table_field_type!='none') {
			$alterTable = EzfFunc::alterTableChange($dataEzf['ezf_table'], $oldName, $model->ezf_field_name, $model->table_field_type, $model->table_field_length);
		    }
		} 

		if(!$alterTable){
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'สร้างคอลัมน์ไม่สำเสร็จ'),
			'data' => $model,
		    ];
		    return $result;
		}
		
		if ($model->save()) {
		    
		    $inputWidget = Yii::createObject($dataInput['system_class']);
		    $htmlInput = $inputWidget->generateViewInput($model->attributes);
		    $html = EzfFunc::createChildrenItem($model->attributes, $htmlInput);
		    
		    
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
			'html'=>$html,
			'alterTable'=>$alterTable,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing EzformFields model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $ezf_field_id
     * @param integer $ezf_field_icon
     * @return mixed
     */
    public function actionDelete()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $id = isset($_POST['id'])?$_POST['id']:0;
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $model = $this->findModel($id);
	    $column = $model->ezf_field_name;
	    
	    if ($model->delete()) {
		
		$dataEzf = Yii::$app->session['ezform'];
		$alterTable = EzfFunc::alterDropColumn($dataEzf['ezf_table'], $column);
		EzfQuery::deleteChoice($model->ezf_field_id);
		
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionResize()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $id = isset($_POST['id'])?$_POST['id']:0;
	    $method = isset($_POST['method'])?$_POST['method']:1;
	    
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $model = $this->findModel($id);
	    $oldSize = $model->ezf_field_lenght;
	    
	    if($method==1){
		$size = $oldSize+1;
		$size = $size>12?1:$size;
	    } else {
		$size = $oldSize-1;
		$size = $size==0?12:$size;
	    }
	    
	    $model->ezf_field_lenght = $size;
	    
	    if ($model->save()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		    'oldSize' => $oldSize,
		    'newSize' => $size,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionViewInput()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $id = isset($_POST['id'])?$_POST['id']:0;
	    $attrEzf = isset($_POST['model'])?$_POST['model']:[];
	    
	    
	    $dataInput;
	    
	    if(isset(Yii::$app->session['ezf_input'])){
		$dataInput = EzfFunc::getInputByArray($id, Yii::$app->session['ezf_input']);
	    }
	    
	    if ($dataInput && !empty($dataInput['system_class'])) {
		try {
		    $inputWidget = Yii::createObject($dataInput['system_class']);
		    
		    $model = new EzformFields();
		    $model->attributes = $attrEzf;
		    
		    $html = $inputWidget->generateViewEditor($dataInput, $model);
		    $options = $inputWidget->generateOptions($dataInput, $model);
		    $validations = $inputWidget->generateValidations($dataInput, $model);
		    
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Generate completed.'),
			'data' => $id,
			'html' => $html,
			'options' => $options,
			'validations' => $validations,
		    ];
		    return $result;
		} catch (\ReflectionException $e) {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Class '.$dataInput['system_class'].' does not exist.'),
			'data' => $id,
		    ];
		    return $result;
		}
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not input.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionViewValidations()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $view = isset($_POST['view'])?$_POST['view']:0;
	    $row = isset($_POST['row'])?$_POST['row']:0;
	    
	    if ($view) {
		$html = $this->renderAjax($view, [
		    'row'=>$row,
		]);
		
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Generate completed.'),
		    'data' => $id,
		    'html' => $html,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not input.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    /**
     * Finds the EzformFields model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $ezf_field_id
     * @param integer $ezf_field_icon
     * @return EzformFields the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ezf_field_id)
    {
        if (($model = EzformFields::findOne(['ezf_field_id' => $ezf_field_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
