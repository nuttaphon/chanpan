<?php

namespace backend\modules\ezforms2\controllers;

use Yii;
use backend\modules\ezforms2\models\Ezform;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use backend\modules\ezforms2\models\EzformFields;
use backend\modules\component\models\EzformComponent;
use yii\helpers\ArrayHelper;
use backend\modules\ezforms2\classes\EzfQuery;
use backend\models\EzformCoDev;
use backend\modules\ezforms2\classes\EzfFunc;

/**
 * EzformController implements the CRUD actions for Ezform model.
 */
class EzformController extends Controller {

    public function behaviors() {
	return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'],
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'],
			'roles' => ['@'],
		    ],
		],
	    ],
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    'delete' => ['post'],
		],
	    ],
	];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }

    /**
     * Updates an existing Ezform model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {

	$model = $this->findModel($id);
	
	Yii::$app->session['ezf_input'] = EzfQuery::getInputv2All();
	Yii::$app->session['ezform'] = $model->attributes;
	
        $modelFields = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $model->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();

        $modelComponents = EzformComponent::find()
            ->where('(user_create=:user_create OR shared =1) and status<>3', [':user_create'=>Yii::$app->user->id])
            ->all();
	
        $dataComponent = ArrayHelper::map($modelComponents, 'comp_id', 'comp_name');
	
	$userlist = ArrayHelper::map(EzfQuery::getIntUserAll(), 'id', 'text'); //explode(",", $model1->assign);

	$model->assign = explode(',', $model->assign);
	$model->field_detail = explode(",", $model->field_detail);
	
        $modelCoDevs = EzformCoDev::find()
            ->where(['ezf_id' => $id])
            ->all();
	
	$userprofile = \backend\models\UserProfile::findOne(['user_id' => $model->user_create]);
	$siteconfig = \common\models\SiteConfig::find()->One();
	
        $modelDynamic = EzfFunc::setDynamicModel($modelFields);

        //get table from tcc bot
        $tccTables = [];
        try {
            $sql = "select id, CONCAT(tbname, ' (',IF(LENGTH(`title`),`title`,'no detail'),')') as text from `buffe_webservice`.`buffe_data_table`;";
            $tccTables = Yii::$app->dbbot->createCommand($sql)->queryAll();
            $tccTables = ArrayHelper::map($tccTables, 'id', 'text');
        }catch (\yii\db\Exception $e){
	    $tccTables = [];
        }
	
	return $this->render('update', [
	    'model' => $model,
	    'ezf_id' => $id,
	    'modelFields' => $modelFields,
	    'modelComponents' => $modelComponents,
	    'dataComponent' => $dataComponent,
	    'userlist' => $userlist,
	    'modelCoDevs' => $modelCoDevs,
	    'modelDynamic' => $modelDynamic,
	    'tccTables' => $tccTables,
	    'userprofile'=>$userprofile,
	    'siteconfig'=>$siteconfig,
	]);
    }

    public function actionOrderUpdate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $position = isset($_POST['position'])?$_POST['position']:[];

	    $sql = '';
	    foreach ($position as $key => $field_id) {
		$order = $key+1;
		$sql .= "UPDATE `ezform_fields` SET `ezf_field_order`='$order' WHERE `ezf_field_id`='$field_id'; ";
	    }
	    try {
		Yii::$app->db->createCommand($sql)->execute();
	    }
	    catch (\yii\db\Exception $e)
	    {
		
	    }
	}
    }
    
    public function actionViewform($id)
    {
	$model = $this->findModel($id);
	
	Yii::$app->session['ezf_input'] = EzfQuery::getInputv2All();
	Yii::$app->session['ezform'] = $model->attributes;
	
        $modelFields = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $model->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
	
	$modelTable = EzfFunc::setDynamicModel($modelFields);
	
	return $this->render('viewform', [
	    'model' => $model,
	    'ezf_id' => $id,
	    'modelFields' => $modelFields,
	    'modelTable'=>$modelTable,
	]);
    }
    
    /**
     * Finds the Ezform model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ezform the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
	if (($model = Ezform::findOne($id)) !== null) {
	    return $model;
	} else {
	    throw new NotFoundHttpException('The requested page does not exist.');
	}
    }

}
