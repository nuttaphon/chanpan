<?php

namespace backend\modules\inv\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inv\models\Tbdata;

/**
 * TbdataSearch represents the model behind the search form about `backend\models\Tbdata`.
 */
class TbdataSearch extends Tbdata
{
    protected static $table;
    protected static $ezf_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
	$return = [];
	if(isset(Yii::$app->session['sql_main_fields']['enable_field'])){
	    $fields = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_field']);
	    $safe = isset($fields['field'])?$fields['field']:[];
	    $return = [
		[$safe, 'safe']
	    ];
	}
	
        return $return;
    }

    public static function setTableName($table)
    {
        self::$table = $table;
    }    
    public static function setEZFid($ezf_id)
    {
        self::$ezf_id = $ezf_id;
    }  
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ovfilter_sub)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $modelFields = Yii::$app->session['sql_main_fields'];
         
        $dataItems = $this->searchGetItem($params, $ovfilter_sub);
        
        $items = implode(',', $dataItems['items']);

        $tbdata = new Tbdata();
        $tbdata->setTableName($modelFields['ezf_table']);
        $query = $tbdata->find();
	
	//$ezform_main = \backend\modules\inv\classes\InvQuery::getEzformById($modelFields['ezf_id']);
	
        $selectField = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
        
        $sql_col[] = $modelFields['ezf_table'].'.id';
        $sql_col[] = $modelFields['ezf_table'].'.ptid';
        $sql_col[] = $modelFields['ezf_table'].'.target';
        $sql_col[] = $modelFields['ezf_table'].'.rstat';
        $sql_col[] = $modelFields['ezf_table'].'.create_date';
        if($modelFields['special']==1){
            $sql_col[] = $modelFields['ezf_table'].'.cid';
        }
        
        $groupByArr = [];
        
        if(is_array($selectField) && !empty($selectField)){
            foreach ($selectField['field'] as $keyFd => $valueFd) {
                $groupByArr[] = $modelFields['ezf_table'].'.'.$valueFd;
                if(!in_array($modelFields['ezf_table'].'.'.$valueFd, $sql_col)){
                    $sql_col[] = $modelFields['ezf_table'].'.'.$valueFd;
                }
            }
        } else {
            $sql_col[] = "{$modelFields['ezf_table']}.*";
        }
        
        $groupBy = ' GROUP BY '. implode(',', $groupByArr);
        
        
//	if(isset($ezform_main['field_detail']) && $ezform_main['field_detail']!=''){
//	    $sql_col[] = "CONCAT({$ezform_main['field_detail']}) AS detail_main";
//	}
	
//	$modelEzform = \backend\modules\ezforms\models\Ezform::find()
//		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$ezform_main['ezf_id']])
//		->one();
//	
//	if(isset($modelEzform->comp_id_target) && !empty($modelEzform->comp_id_target)){
//	    $dataCompMain = \backend\modules\component\models\EzformComponent::find()->select('ezform_component.*, ezform.ezf_table, ezform_fields.ezf_field_name AS pk_field')
//		    ->innerJoin('ezform', 'ezform_component.ezf_id=ezform.ezf_id')
//		    ->innerJoin('ezform_fields', 'ezform_component.field_id_key=ezform_fields.ezf_field_id')
//		    ->where('comp_id=:comp_id',[':comp_id'=>$modelEzform->comp_id_target])->one();
//	    if($dataCompMain && $dataCompMain->ezf_id!=$modelEzform->ezf_id){
//	       $dataFieldsMain = \backend\modules\inv\classes\InvFunc::getFields($dataCompMain->ezf_id, '');
//	       
//	       foreach ($dataFieldsMain as $keyMain => $valueMain) {
//		   $table = $dataCompMain->ezf_table;
//		   $newField = "fxmain_$keyMain";
//		   $sql_col[] = "$table.$keyMain AS $newField";
//		   
//		   $query->andFilterWhere(['like', "$table.$keyMain", $this->$newField]);
//	       }
//	       
//	       $pk = $dataCompMain['pk_field'];
//		$pkJoin = 'target';
//		if($dataCompMain['special']==1){
//		    $pk = 'ptid';
//		    $pkJoin = 'ptid';
//		}
//		
//	       $query->innerJoin($table, "{$ezform_main['ezf_table']}.$pkJoin = $table.$pk");
//	       
//	    }
//	    
//	}
	
	$fields = Yii::$app->session['sql_fields'];
	
	if(isset($fields) && !empty($fields)){
	    foreach ($fields as $key => $value) {
		$arry = \backend\modules\inv\classes\InvFunc::createSqlWeb($value, $modelFields);
		$sql_col = array_merge($sql_col, $arry);
	    }
	} else {
	    $main_forms = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_form']);
	    $form = isset($main_forms['form'])?$main_forms['form']:[];
	    $label = isset($main_forms['label'])?$main_forms['label']:[];
	    $width = isset($main_forms['width'])?$main_forms['width']:[];
	    $condition = isset($main_forms['condition'])?$main_forms['condition']:[];
	    $display = isset($main_forms['display'])?$main_forms['display']:[];
	    $show = isset($main_forms['show'])?$main_forms['show']:[];
	    $result = isset($main_forms['result'])?$main_forms['result']:[];
	    $date = isset($main_forms['date'])?$main_forms['date']:[];
	    
	    $fd = isset($main_forms['field_detail'])?$main_forms['field_detail']:[];
	    $en = isset($main_forms['ezf_name'])?$main_forms['ezf_name']:[];
	    $et = isset($main_forms['ezf_table'])?$main_forms['ezf_table']:[];
	    $ct = isset($main_forms['comp_id_target'])?$main_forms['comp_id_target']:[];
	    $ur = isset($main_forms['unique_record'])?$main_forms['unique_record']:[];
//	    \yii\helpers\VarDumper::dump($main_forms,10,true);
//	    exit();
	    
	    foreach ($form as $key => $value) {
		//$ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value);
		$form_fix=[];
		
		$form_fix = [
		    'ezf_id' => $value,
		    'ezf_name' => $en[$key],
		    'ezf_table' => $et[$key],
		    'comp_id_target' => $ct[$key],
		    'sql_id_all' => 'idall_'.$value,
		    'sql_id_name' => 'id_'.$value,
		    'sql_idsubmit_name' => 'idsubmit_'.$value,
		    'sql_result_name' => 'result_'.$value,
		    'field_detail' => $fd[$key],
		    'value_options' => \appxq\sdii\utils\SDUtility::array2String($condition[$value]),
		    'header' => $label[$key],
		    'width' => $width[$key],
		    'display_options' => \appxq\sdii\utils\SDUtility::array2String($display[$value]),
		    'show' => $show[$key],
		    'result' => $result[$key],
		    'date' => $date[$key],
		];
		
		
		$arry = \backend\modules\inv\classes\InvFunc::createSqlWeb($form_fix, $modelFields);
		$sql_col = array_merge($sql_col, $arry);
		
	    }
	   
	}
	
	$query->select($sql_col)
		//->where("{$modelFields['ezf_table']}.rstat NOT IN(3)");
                ->where("{$modelFields['ezf_table']}.id IN($items)");
                
//        $innerJoin = '';   
//        $whereCount = '';
//	if($modelFields['ezf_id']!=$modelFields['main_ezf_id']){
//	    $pk = $modelFields['pk_field'];
//	    $pkJoin = 'target';
//	    if($modelFields['special']==1){
//		$pk = 'ptid';
//		$pkJoin = 'ptid';
//	    }
//            $innerJoin = "INNER JOIN {$modelFields['main_ezf_table']} ON `{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`";
//            $whereCount .= " AND `{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)";
//            if (isset($params['sort'])) {
//                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
//            }else{
//                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
//                //$query->innerJoin($modelFields['main_ezf_table']." FORCE INDEX (create_date)", "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
//            }
//	    $query->andWhere("`{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)");
//	    
//	}	
        
        
//	if($ovfilter_sub>0){
//	    $modelFilter = InvFilterSub::find()->where('sub_id = :sub_id', [':sub_id'=>$ovfilter_sub])->one();
//	    $condition = \appxq\sdii\utils\SDUtility::string2Array($modelFilter->options);
////	    \yii\helpers\VarDumper::dump($condition,10,true);
////	    exit();
//	    if(isset($condition) && is_array($condition) && !empty($condition)){
//		$formCond = isset($condition['form'])?$condition['form']:[];
//		$value1Cond = isset($condition['value1'])?$condition['value1']:[];
//		$value2Cond = isset($condition['value2'])?$condition['value2']:[];
//		$fieldCond = isset($condition['field'])?$condition['field']:[];
//		$condCond = isset($condition['cond'])?$condition['cond']:[];
//		$moreCond = isset($condition['more'])?$condition['more']:[];
//
//		$formCondUnique = array_unique($formCond);
//		
//		$pk = $modelFields['pk_field'];
//		$pkJoin = 'target';
//		if($modelFields['special']==1){
//		    $pk = 'ptid';
//		    $pkJoin = 'ptid';
//		}
//		$ezformTmp = [];
//		$ezformTmpUnipue = [];
//		//innerJoin
//		foreach ($formCondUnique as $key_cond => $value_cond) {
//		    $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value_cond);
//		    $ezformTmp[$ezform['ezf_id']] = $ezform['ezf_table'];
//		    
//		    if($ezform['ezf_table']!=$modelFields['ezf_table'] && $ezform['ezf_table']!=$modelFields['main_ezf_table'] ){//
//			if(!in_array($ezform['ezf_table'], $ezformTmpUnipue)){
//			    $query->innerJoin($ezform['ezf_table'], "{$ezform['ezf_table']}.$pkJoin = {$modelFields['ezf_table']}.$pk");
//			    $ezformTmpUnipue[] = $ezform['ezf_table'];
//			}
//		    }
//		}
//		//Where
//		$condType = 'AND';
//		foreach ($formCond as $key_cond => $value_cond) {
//		    if($condType=='AND'){
//			if($condCond[$key_cond]!='between'){
//			    $pos = strpos($value1Cond[$key_cond], '_FIELD_');
//			    $checkPos = $pos===false;
//			    if($value1Cond[$key_cond]=='_SITECODE_'){
//				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$sitecode]);
//			    } elseif ($checkPos!==true) {
//				$fieldStr = str_replace('_FIELD_', '', $value1Cond[$key_cond]);
//				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} $fieldStr");
//			    }else {
//				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
//			    }
//			} else {
//			    $query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond1_$key_cond AND :cond2_$key_cond", [":cond1_$key_cond"=>$value1Cond[$key_cond], ":cond2_$key_cond"=>$value2Cond[$key_cond]]);
//			}
//		    } else {
//			if($condCond[$key_cond]!='between'){
//			    $pos = strpos($value1Cond[$key_cond], '_FIELD_');
//			    $checkPos = $pos===false;
//			    if($value1Cond[$key_cond]=='_SITECODE_'){
//				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$sitecode]);
//			    } elseif ($checkPos!==true) {
//				$fieldStr = str_replace('_FIELD_', '', $value1Cond[$key_cond]);
//				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} $fieldStr");
//			    }else {
//				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
//			    }
//			    //$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
//			} else {
//			    $query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond1_$key_cond AND :cond2_$key_cond", [":cond1_$key_cond"=>$value1Cond[$key_cond], ":cond2_$key_cond"=>$value2Cond[$key_cond]]);
//			}
//		    }
//		    $condType = $moreCond[$key_cond];
//		}
//	    } else {
//		$query->innerJoin('inv_sub_list', "inv_sub_list.person_id = {$modelFields['ezf_table']}.id");
//		$query->andWhere("inv_sub_list.sub_id = :sub_id", [':sub_id'=>$ovfilter_sub]);
//	    }
//	}
	
//	if($modelFields['special']==1){
//            $whereCount .= " AND {$modelFields['ezf_table']}.hsitecode = $sitecode";
//	    $query->andWhere("{$modelFields['ezf_table']}.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
//	} else {
//            $whereCount .= " AND {$modelFields['ezf_table']}.xsourcex = $sitecode";
//	    $query->andWhere("{$modelFields['ezf_table']}.xsourcex = :sitecode", [':sitecode'=>$sitecode]);
//	}
        
        if (0) {
	//if (!isset($params['sort'])) {
            $query->orderBy($modelFields['main_ezf_table'].'.create_date desc');
        }
	
//	if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
//            $orderBy = [];
//            $orderBy[$modelFields['main_ezf_table'].'.create_date'] = 'DESC';
//            $order_field = explode(',', $modelFields['order_field']);
//            if(is_array($order_field)){
//                foreach ($order_field as $keyOd => $valueOd) {
//                    $orderBy[$valueOd] = $modelFields['label_field'];
//                }
//            }
//	    $query->orderBy($orderBy);
//	} else {
//            $query->orderBy($modelFields['main_ezf_table'].'.create_date desc');
//        }
	
//	if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
//	    $query->orderBy($modelFields['order_field'].' '.$modelFields['label_field']);
//	}
	
	//\appxq\sdii\utils\VarDumper::dump($query->createCommand()->rawSql);
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'pagination' => [
//            'pageSize' => 20,
//        ],
//        ]);
        //$query->groupBy("{$modelFields['ezf_table']}.id");
        $this->load($params);

        if (!$this->validate()) {
//            $count = Yii::$app->db->createCommand("
//                SELECT COUNT(*) FROM {$modelFields['ezf_table']} $innerJoin 
//                WHERE {$modelFields['ezf_table']}.rstat NOT IN(3) $whereCount 
//            ")->queryScalar();
//
//
            $dataProvider = new \yii\data\SqlDataProvider([
                'sql' => $query->createCommand()->rawSql,
                //'params' => [':status' => 1],
                'totalCount' => $dataItems['count'],
                'pagination' => [
                    'pageSize' => 20,
                ],
        //        'sort' => [
        //            'attributes' => [
        //                'title',
        //                'view_count',
        //                'created_at',
        //            ],
        //        ],
            ]);
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'id' => $this->id,
//            'rstat' => $this->rstat,
//        ]);
//        
//        $query->andFilterWhere(['like', 'xsourcex', $this->xsourcex]);
//	
	
//	if(isset($modelFields['enable_field'])){
//	    $fields = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
//	    
//	    $safe = isset($fields['field'])?$fields['field']:[];
//	    
//	    foreach ($safe as $key => $value) {
//		$pos = strpos($value, 'fxmain_');
//		if ($pos === false) {
//                    $paramsValue = $this->$value;
//                    if($paramsValue!=''){
//                        $query->andFilterWhere(['like', $modelFields['ezf_table'].'.'.$value, $this->$value]);
//                        $whereCount .= " AND {$modelFields['ezf_table']}.$value like '%$paramsValue%'";
//                    }
//		}
//	    }
//	    
//             
//	}
        
//    $count = Yii::$app->db->createCommand("
//        SELECT COUNT(*) FROM {$modelFields['ezf_table']} $innerJoin 
//        WHERE {$modelFields['ezf_table']}.rstat NOT IN(3) $whereCount 
//    ")->queryScalar();
//
//    
        //\appxq\sdii\utils\VarDumper::dump($query->createCommand()->rawSql);
    $dataProvider = new \yii\data\SqlDataProvider([
        'sql' => $query->createCommand()->rawSql,
        //'params' => [':status' => 1],
        'totalCount' => $dataItems['count'],
        'pagination' => [
            'pageSize' => 20,
        ],
//        'sort' => [
//            'attributes' => [
//                'title',
//                'view_count',
//                'created_at',
//            ],
//        ],
    ]);
        
        
	//\appxq\sdii\utils\VarDumper::dump($provider->sql);
        return $dataProvider;
    }
    
    public function searchItem($params, $ovfilter_sub)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $modelFields = Yii::$app->session['sql_main_fields'];
	
        $tbdata = new Tbdata();
        $tbdata->setTableName($modelFields['ezf_table']);
        $query = $tbdata->find();
	
	$ezform_main = \backend\modules\inv\classes\InvQuery::getEzformById($modelFields['ezf_id']);
	
        $selectField = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
        
        $sql_col[] = $modelFields['ezf_table'].'.id';
        $sql_col[] = $modelFields['ezf_table'].'.ptid';
        $sql_col[] = $modelFields['ezf_table'].'.target';
        $sql_col[] = $modelFields['ezf_table'].'.rstat';
        $sql_col[] = $modelFields['ezf_table'].'.create_date';
        if($modelFields['special']==1){
            $sql_col[] = $modelFields['ezf_table'].'.cid';
        }
        $groupByArr = [];
        
        if(is_array($selectField) && !empty($selectField)){
            foreach ($selectField['field'] as $keyFd => $valueFd) {
                $groupByArr[] = $modelFields['ezf_table'].'.'.$valueFd;
                if(!in_array($modelFields['ezf_table'].'.'.$valueFd, $sql_col)){
                    $sql_col[] = $modelFields['ezf_table'].'.'.$valueFd;
                }
            }
        } else {
            $sql_col[] = "{$modelFields['ezf_table']}.*";
        }
        
        $groupBy = ' GROUP BY '. implode(',', $groupByArr);
        
//	if(isset($ezform_main['field_detail']) && $ezform_main['field_detail']!=''){
//	    $sql_col[] = "CONCAT({$ezform_main['field_detail']}) AS detail_main";
//	}
	
//	$modelEzform = \backend\modules\ezforms\models\Ezform::find()
//		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$ezform_main['ezf_id']])
//		->one();
//	
//	if(isset($modelEzform->comp_id_target) && !empty($modelEzform->comp_id_target)){
//	    $dataCompMain = \backend\modules\component\models\EzformComponent::find()->select('ezform_component.*, ezform.ezf_table, ezform_fields.ezf_field_name AS pk_field')
//		    ->innerJoin('ezform', 'ezform_component.ezf_id=ezform.ezf_id')
//		    ->innerJoin('ezform_fields', 'ezform_component.field_id_key=ezform_fields.ezf_field_id')
//		    ->where('comp_id=:comp_id',[':comp_id'=>$modelEzform->comp_id_target])->one();
//	    if($dataCompMain && $dataCompMain->ezf_id!=$modelEzform->ezf_id){
//	       $dataFieldsMain = \backend\modules\inv\classes\InvFunc::getFields($dataCompMain->ezf_id, '');
//	       
//	       foreach ($dataFieldsMain as $keyMain => $valueMain) {
//		   $table = $dataCompMain->ezf_table;
//		   $newField = "fxmain_$keyMain";
//		   $sql_col[] = "$table.$keyMain AS $newField";
//		   
//		   $query->andFilterWhere(['like', "$table.$keyMain", $this->$newField]);
//	       }
//	       
//	       $pk = $dataCompMain['pk_field'];
//		$pkJoin = 'target';
//		if($dataCompMain['special']==1){
//		    $pk = 'ptid';
//		    $pkJoin = 'ptid';
//		}
//		
//	       $query->innerJoin($table, "{$ezform_main['ezf_table']}.$pkJoin = $table.$pk");
//	       
//	    }
//	    
//	}
	
	$fields = Yii::$app->session['sql_fields'];
	
	if(isset($fields) && !empty($fields)){
	    foreach ($fields as $key => $value) {
		$arry = \backend\modules\inv\classes\InvFunc::createSqlWeb($value, $modelFields);
		$sql_col = array_merge($sql_col, $arry);
	    }
	} else {
	    $main_forms = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_form']);
	    $form = isset($main_forms['form'])?$main_forms['form']:[];
	    $label = isset($main_forms['label'])?$main_forms['label']:[];
	    $width = isset($main_forms['width'])?$main_forms['width']:[];
	    $condition = isset($main_forms['condition'])?$main_forms['condition']:[];
	    $display = isset($main_forms['display'])?$main_forms['display']:[];
	    $show = isset($main_forms['show'])?$main_forms['show']:[];
	    $result = isset($main_forms['result'])?$main_forms['result']:[];
	    $date = isset($main_forms['date'])?$main_forms['date']:[];
	    
	    $fd = isset($main_forms['field_detail'])?$main_forms['field_detail']:[];
	    $en = isset($main_forms['ezf_name'])?$main_forms['ezf_name']:[];
	    $et = isset($main_forms['ezf_table'])?$main_forms['ezf_table']:[];
	    $ct = isset($main_forms['comp_id_target'])?$main_forms['comp_id_target']:[];
	    $ur = isset($main_forms['unique_record'])?$main_forms['unique_record']:[];
//	    \yii\helpers\VarDumper::dump($main_forms,10,true);
//	    exit();
	    
	    foreach ($form as $key => $value) {
		//$ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value);
		$form_fix=[];
		
		$form_fix = [
		    'ezf_id' => $value,
		    'ezf_name' => $en[$key],
		    'ezf_table' => $et[$key],
		    'comp_id_target' => $ct[$key],
		    'sql_id_all' => 'idall_'.$value,
		    'sql_id_name' => 'id_'.$value,
		    'sql_idsubmit_name' => 'idsubmit_'.$value,
		    'sql_result_name' => 'result_'.$value,
		    'field_detail' => $fd[$key],
		    'value_options' => \appxq\sdii\utils\SDUtility::array2String($condition[$value]),
		    'header' => $label[$key],
		    'width' => $width[$key],
		    'display_options' => \appxq\sdii\utils\SDUtility::array2String($display[$value]),
		    'show' => $show[$key],
		    'result' => $result[$key],
		    'date' => $date[$key],
		];
		
		
		$arry = \backend\modules\inv\classes\InvFunc::createSqlWeb($form_fix, $modelFields);
		$sql_col = array_merge($sql_col, $arry);
		
	    }
	   
	}
	
	$query->select($sql_col)
		->where("{$modelFields['ezf_table']}.rstat NOT IN(3)");
	
        $innerJoin = '';   
        $whereCount = '';
	if($modelFields['ezf_id']!=$modelFields['main_ezf_id']){
	    $pk = $modelFields['pk_field'];
	    $pkJoin = 'target';
	    if($modelFields['special']==1){
		$pk = 'ptid';
		$pkJoin = 'ptid';
	    }
            $innerJoin = "INNER JOIN {$modelFields['main_ezf_table']} ON `{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`";
            $whereCount .= " AND `{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)";
            if (isset($params['sort'])) {
                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
            }else{
                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
                //$query->innerJoin($modelFields['main_ezf_table']." FORCE INDEX (create_date)", "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
            }
	    $query->andWhere("`{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)");
	    
	}	
        
        
	if($ovfilter_sub>0){
	    $modelFilter = InvFilterSub::find()->where('sub_id = :sub_id', [':sub_id'=>$ovfilter_sub])->one();
	    $condition = \appxq\sdii\utils\SDUtility::string2Array($modelFilter->options);
//	    \yii\helpers\VarDumper::dump($condition,10,true);
//	    exit();
	    if(isset($condition) && is_array($condition) && !empty($condition)){
		$formCond = isset($condition['form'])?$condition['form']:[];
		$value1Cond = isset($condition['value1'])?$condition['value1']:[];
		$value2Cond = isset($condition['value2'])?$condition['value2']:[];
		$fieldCond = isset($condition['field'])?$condition['field']:[];
		$condCond = isset($condition['cond'])?$condition['cond']:[];
		$moreCond = isset($condition['more'])?$condition['more']:[];

		$formCondUnique = array_unique($formCond);
		
		$pk = $modelFields['pk_field'];
		$pkJoin = 'target';
		if($modelFields['special']==1){
		    $pk = 'ptid';
		    $pkJoin = 'ptid';
		}
		$ezformTmp = [];
		$ezformTmpUnipue = [];
		//innerJoin
		foreach ($formCondUnique as $key_cond => $value_cond) {
		    $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value_cond);
		    $ezformTmp[$ezform['ezf_id']] = $ezform['ezf_table'];
		    
		    if($ezform['ezf_table']!=$modelFields['ezf_table'] && $ezform['ezf_table']!=$modelFields['main_ezf_table'] ){//
			if(!in_array($ezform['ezf_table'], $ezformTmpUnipue)){
			    $query->innerJoin($ezform['ezf_table'], "{$ezform['ezf_table']}.$pkJoin = {$modelFields['ezf_table']}.$pk");
			    $ezformTmpUnipue[] = $ezform['ezf_table'];
			}
		    }
		}
		//Where
		$condType = 'AND';
		foreach ($formCond as $key_cond => $value_cond) {
		    if($condType=='AND'){
			if($condCond[$key_cond]!='between'){
			    $pos = strpos($value1Cond[$key_cond], '_FIELD_');
			    $checkPos = $pos===false;
			    if($value1Cond[$key_cond]=='_SITECODE_'){
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$sitecode]);
			    } elseif ($checkPos!==true) {
				$fieldStr = str_replace('_FIELD_', '', $value1Cond[$key_cond]);
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} $fieldStr");
			    }else {
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			    }
			} else {
			    $query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond1_$key_cond AND :cond2_$key_cond", [":cond1_$key_cond"=>$value1Cond[$key_cond], ":cond2_$key_cond"=>$value2Cond[$key_cond]]);
			}
		    } else {
			if($condCond[$key_cond]!='between'){
			    $pos = strpos($value1Cond[$key_cond], '_FIELD_');
			    $checkPos = $pos===false;
			    if($value1Cond[$key_cond]=='_SITECODE_'){
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$sitecode]);
			    } elseif ($checkPos!==true) {
				$fieldStr = str_replace('_FIELD_', '', $value1Cond[$key_cond]);
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} $fieldStr");
			    }else {
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			    }
			    //$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			} else {
			    $query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond1_$key_cond AND :cond2_$key_cond", [":cond1_$key_cond"=>$value1Cond[$key_cond], ":cond2_$key_cond"=>$value2Cond[$key_cond]]);
			}
		    }
		    $condType = $moreCond[$key_cond];
		}
	    } else {
		$query->innerJoin('inv_sub_list', "inv_sub_list.person_id = {$modelFields['ezf_table']}.id");
		$query->andWhere("inv_sub_list.sub_id = :sub_id", [':sub_id'=>$ovfilter_sub]);
	    }
	}
	
	if($modelFields['special']==1){
            $whereCount .= " AND {$modelFields['ezf_table']}.hsitecode = $sitecode";
	    $query->andWhere("{$modelFields['ezf_table']}.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
	} else {
            $whereCount .= " AND {$modelFields['ezf_table']}.xsourcex = $sitecode";
	    $query->andWhere("{$modelFields['ezf_table']}.xsourcex = :sitecode", [':sitecode'=>$sitecode]);
	}
        
        if (0) {
	//if (!isset($params['sort'])) {
            $query->orderBy($modelFields['main_ezf_table'].'.create_date desc');
        }
//	
//	if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
//            $orderBy = [];
//            $orderBy[$modelFields['main_ezf_table'].'.create_date'] = 'DESC';
//            $order_field = explode(',', $modelFields['order_field']);
//            if(is_array($order_field)){
//                foreach ($order_field as $keyOd => $valueOd) {
//                    $orderBy[$valueOd] = $modelFields['label_field'];
//                }
//            }
//	    $query->orderBy($orderBy);
//	} else {
//            $query->orderBy($modelFields['main_ezf_table'].'.create_date desc');
//        }
	
//	if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
//	    $query->orderBy($modelFields['order_field'].' '.$modelFields['label_field']);
//	}
	
	//\appxq\sdii\utils\VarDumper::dump($query->createCommand()->rawSql);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 20,
        ],
        ]);
        //$query->groupBy($groupByArr);
        
        $this->load($params);

        if (!$this->validate()) {
//            $count = Yii::$app->db->createCommand("
//                SELECT COUNT(*) FROM {$modelFields['ezf_table']} $innerJoin 
//                WHERE {$modelFields['ezf_table']}.rstat NOT IN(3) $whereCount 
//            ")->queryScalar();
//
//
//            $dataProvider = new \yii\data\SqlDataProvider([
//                'sql' => $query->createCommand()->rawSql,
//                //'params' => [':status' => 1],
//                'totalCount' => $count,
//                'pagination' => [
//                    'pageSize' => 20,
//                ],
//        //        'sort' => [
//        //            'attributes' => [
//        //                'title',
//        //                'view_count',
//        //                'created_at',
//        //            ],
//        //        ],
//            ]);
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'id' => $this->id,
//            'rstat' => $this->rstat,
//        ]);
//        
//        $query->andFilterWhere(['like', 'xsourcex', $this->xsourcex]);
//	
	
	if(isset($modelFields['enable_field'])){
	    $fields = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
	    
	    $safe = isset($fields['field'])?$fields['field']:[];
	    
	    foreach ($safe as $key => $value) {
		$pos = strpos($value, 'fxmain_');
		if ($pos === false) {
                    $paramsValue = $this->$value;
                    if($paramsValue!=''){
                        $query->andFilterWhere(['like', $modelFields['ezf_table'].'.'.$value, $this->$value]);
                        $whereCount .= " AND {$modelFields['ezf_table']}.$value like '%$paramsValue%'";
                    }
		}
	    }
	    
             
	}
        
//    $count = Yii::$app->db->createCommand("
//        SELECT COUNT(*) FROM {$modelFields['ezf_table']} $innerJoin 
//        WHERE {$modelFields['ezf_table']}.rstat NOT IN(3) $whereCount 
//    ")->queryScalar();
//
//    
//    $dataProvider = new \yii\data\SqlDataProvider([
//        'sql' => $query->createCommand()->rawSql,
//        //'params' => [':status' => 1],
//        'totalCount' => $count,
//        'pagination' => [
//            'pageSize' => 20,
//        ],
////        'sort' => [
////            'attributes' => [
////                'title',
////                'view_count',
////                'created_at',
////            ],
////        ],
//    ]);
        
        
	//\appxq\sdii\utils\VarDumper::dump($provider->sql);
        return $dataProvider;
    }
    
    public function searchDpMain($params, $ovfilter_sub)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $modelFields = Yii::$app->session['sql_main_fields'];
	
        $tbdata = new Tbdata();
        $tbdata->setTableName($modelFields['ezf_table']);
        $query = $tbdata->find();
	
        $selectField = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
        
        $sql_col[] = $modelFields['ezf_table'].'.id';
        $sql_col[] = $modelFields['ezf_table'].'.ptid';
        $sql_col[] = $modelFields['ezf_table'].'.target';
        $sql_col[] = $modelFields['ezf_table'].'.rstat';
        $sql_col[] = $modelFields['ezf_table'].'.create_date';
        if($modelFields['special']==1){
            $sql_col[] = $modelFields['ezf_table'].'.cid';
        }
        
        $groupByArr = [];
        
        if(is_array($selectField) && !empty($selectField)){
            foreach ($selectField['field'] as $keyFd => $valueFd) {
                $groupByArr[] = $modelFields['ezf_table'].'.'.$valueFd;
                if(!in_array($modelFields['ezf_table'].'.'.$valueFd, $sql_col)){
                    $pos = strpos($valueFd, 'fxmain_');
                    if ($pos === false) {
                        $sql_col[] = $modelFields['ezf_table'].'.'.$valueFd;
                    } else {
                        $sql_col[] = $modelFields['main_ezf_table'].'.'. str_replace('fxmain_', '', $valueFd).' AS '.$valueFd;
                    }
                    
                }
            }
        } else {
            $sql_col[] = "{$modelFields['ezf_table']}.*";
        }
        
        $groupBy = ' GROUP BY '. implode(',', $groupByArr);
        
        $fields = Yii::$app->session['sql_fields'];
	
//	if(isset($fields) && !empty($fields)){
//	    foreach ($fields as $key => $value) {
//		$arry = \backend\modules\inv\classes\InvFunc::createSqlWeb($value, $modelFields);
//		$sql_col = array_merge($sql_col, $arry);
//	    }
//	} else {
//	    $main_forms = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_form']);
//	    $form = isset($main_forms['form'])?$main_forms['form']:[];
//	    $label = isset($main_forms['label'])?$main_forms['label']:[];
//	    $width = isset($main_forms['width'])?$main_forms['width']:[];
//	    $condition = isset($main_forms['condition'])?$main_forms['condition']:[];
//	    $display = isset($main_forms['display'])?$main_forms['display']:[];
//	    $show = isset($main_forms['show'])?$main_forms['show']:[];
//	    $result = isset($main_forms['result'])?$main_forms['result']:[];
//	    $date = isset($main_forms['date'])?$main_forms['date']:[];
//	    
//	    $fd = isset($main_forms['field_detail'])?$main_forms['field_detail']:[];
//	    $en = isset($main_forms['ezf_name'])?$main_forms['ezf_name']:[];
//	    $et = isset($main_forms['ezf_table'])?$main_forms['ezf_table']:[];
//	    $ct = isset($main_forms['comp_id_target'])?$main_forms['comp_id_target']:[];
//	    $ur = isset($main_forms['unique_record'])?$main_forms['unique_record']:[];
////	    \yii\helpers\VarDumper::dump($main_forms,10,true);
////	    exit();
//	    
//	    foreach ($form as $key => $value) {
//		//$ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value);
//		$form_fix=[];
//		
//		$form_fix = [
//		    'ezf_id' => $value,
//		    'ezf_name' => $en[$key],
//		    'ezf_table' => $et[$key],
//		    'comp_id_target' => $ct[$key],
//		    'sql_id_all' => 'idall_'.$value,
//		    'sql_id_name' => 'id_'.$value,
//		    'sql_idsubmit_name' => 'idsubmit_'.$value,
//		    'sql_result_name' => 'result_'.$value,
//		    'field_detail' => $fd[$key],
//		    'value_options' => \appxq\sdii\utils\SDUtility::array2String($condition[$value]),
//		    'header' => $label[$key],
//		    'width' => $width[$key],
//		    'display_options' => \appxq\sdii\utils\SDUtility::array2String($display[$value]),
//		    'show' => $show[$key],
//		    'result' => $result[$key],
//		    'date' => $date[$key],
//		];
//		
//		
//		$arry = \backend\modules\inv\classes\InvFunc::createSqlWeb($form_fix, $modelFields);
//		$sql_col = array_merge($sql_col, $arry);
//		
//	    }
//	   
//	}
	
	$query->select($sql_col)
		->where("{$modelFields['ezf_table']}.rstat NOT IN(3)");
        
                
        $innerJoin = '';   
        $whereCount = '';
	if($modelFields['ezf_id']!=$modelFields['main_ezf_id']){
	    $pk = $modelFields['pk_field'];
	    $pkJoin = 'target';
	    if($modelFields['special']==1){
		$pk = 'ptid';
		$pkJoin = 'ptid';
	    }
            $innerJoin = "INNER JOIN {$modelFields['main_ezf_table']} ON `{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`";
            $whereCount .= " AND `{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)";
            if (isset($params['sort'])) {
                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
            }else{
                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
                //$query->innerJoin($modelFields['main_ezf_table']." FORCE INDEX (create_date)", "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
            }
	    $query->andWhere("`{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)");
	    $query->groupBy("{$modelFields['ezf_table']}.id");
	}
        
	if($ovfilter_sub>0){
	    $modelFilter = InvFilterSub::find()->where('sub_id = :sub_id', [':sub_id'=>$ovfilter_sub])->one();
	    $condition = \appxq\sdii\utils\SDUtility::string2Array($modelFilter->options);
            
	    if(isset($condition) && is_array($condition) && !empty($condition)){
		$formCond = isset($condition['form'])?$condition['form']:[];
		$value1Cond = isset($condition['value1'])?$condition['value1']:[];
		$value2Cond = isset($condition['value2'])?$condition['value2']:[];
		$fieldCond = isset($condition['field'])?$condition['field']:[];
		$condCond = isset($condition['cond'])?$condition['cond']:[];
		$moreCond = isset($condition['more'])?$condition['more']:[];

		$formCondUnique = array_unique($formCond);
		
		$pk = $modelFields['pk_field'];
		$pkJoin = 'target';
		if($modelFields['special']==1){
		    $pk = 'ptid';
		    $pkJoin = 'ptid';
		}
		$ezformTmp = [];
		$ezformTmpUnipue = [];
		//innerJoin
		foreach ($formCondUnique as $key_cond => $value_cond) {
		    $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value_cond);
		    $ezformTmp[$ezform['ezf_id']] = $ezform['ezf_table'];
		    
		    if($ezform['ezf_table']!=$modelFields['ezf_table'] && $ezform['ezf_table']!=$modelFields['main_ezf_table'] ){//
			if(!in_array($ezform['ezf_table'], $ezformTmpUnipue)){
			    $query->innerJoin($ezform['ezf_table'], "{$ezform['ezf_table']}.$pkJoin = {$modelFields['ezf_table']}.$pk");
			    $ezformTmpUnipue[] = $ezform['ezf_table'];
			}
		    }
		}
		//Where
		$condType = 'AND';
		foreach ($formCond as $key_cond => $value_cond) {
		    if($condType=='AND'){
			if($condCond[$key_cond]!='between'){
			    $pos = strpos($value1Cond[$key_cond], '_FIELD_');
			    $checkPos = $pos===false;
			    if($value1Cond[$key_cond]=='_SITECODE_'){
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$sitecode]);
			    } elseif ($checkPos!==true) {
				$fieldStr = str_replace('_FIELD_', '', $value1Cond[$key_cond]);
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} $fieldStr");
			    }else {
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			    }
			} else {
			    $query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond1_$key_cond AND :cond2_$key_cond", [":cond1_$key_cond"=>$value1Cond[$key_cond], ":cond2_$key_cond"=>$value2Cond[$key_cond]]);
			}
		    } else {
			if($condCond[$key_cond]!='between'){
			    $pos = strpos($value1Cond[$key_cond], '_FIELD_');
			    $checkPos = $pos===false;
			    if($value1Cond[$key_cond]=='_SITECODE_'){
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$sitecode]);
			    } elseif ($checkPos!==true) {
				$fieldStr = str_replace('_FIELD_', '', $value1Cond[$key_cond]);
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} $fieldStr");
			    }else {
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			    }
			    //$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			} else {
			    $query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond1_$key_cond AND :cond2_$key_cond", [":cond1_$key_cond"=>$value1Cond[$key_cond], ":cond2_$key_cond"=>$value2Cond[$key_cond]]);
			}
		    }
		    $condType = $moreCond[$key_cond];
		}
	    } else {
		$query->innerJoin('inv_sub_list', "inv_sub_list.person_id = {$modelFields['ezf_table']}.id");
		$query->andWhere("inv_sub_list.sub_id = :sub_id", [':sub_id'=>$ovfilter_sub]);
	    }
	}
	
	if($modelFields['special']==1){
            $whereCount .= " AND {$modelFields['ezf_table']}.hsitecode = $sitecode";
	    $query->andWhere("{$modelFields['ezf_table']}.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
	} else {
            $whereCount .= " AND {$modelFields['ezf_table']}.xsourcex = $sitecode";
	    $query->andWhere("{$modelFields['ezf_table']}.xsourcex = :sitecode", [':sitecode'=>$sitecode]);
	}
        
        if (!isset($params['sort'])) {
            if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
                $orderBy = [];
                //$orderBy[$modelFields['main_ezf_table'].'.create_date'] = 'DESC';
                $order_field = explode(',', $modelFields['order_field']);
                if(is_array($order_field)){
                    foreach ($order_field as $keyOd => $valueOd) {
                        $orderBy[$valueOd] = $modelFields['label_field'];
                    }
                }
                $query->orderBy($orderBy);
            } else {
                $query->orderBy($modelFields['main_ezf_table'].'.create_date DESC');
            }
        }
	
	
	
//	if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
//	    $query->orderBy($modelFields['order_field'].' '.$modelFields['label_field']);
//	}
        
	
        
        $this->load($params);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 20,
        ],
        ]);
            
        if (!$this->validate()) {
//           
            return ['items'=>$query->column(), 'count'=>$count];
        }

	
	if(isset($modelFields['enable_field'])){
	    $fields = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
	    $fixType = isset($fields['type'])?$fields['type']:[];
	    $safe = isset($fields['field'])?$fields['field']:[];
	    
	    foreach ($safe as $key => $value) {
                $fType = $fixType[$key];
                
		$pos = strpos($value, 'fxmain_');
		if ($pos === false) {
                    $paramsValue = $this->$value;
                    if($paramsValue!=''){
                        if ($value=='create_date' || $value=='fxmain_create_date' || $fType==7 || $fType==9) {
                            $daterang = explode(' to ', $paramsValue);
                            if(isset($daterang[1])){
                                $sdate = \common\lib\sdii\components\utils\SDdate::phpThDate2mysqlDate($daterang[0], '-');
                                $edate = \common\lib\sdii\components\utils\SDdate::phpThDate2mysqlDate($daterang[1], '-');
                                $query->andFilterWhere(['between', 'date('.$modelFields['ezf_table'].'.'.$value.')', $sdate, $edate]);
                            }
                        } else {
                            $query->andFilterWhere(['like', $modelFields['ezf_table'].'.'.$value, $this->$value]);
                        }
                    }
		} else {
                    $paramsValue = $this->$value;
                    if($paramsValue!=''){
                        $origin = str_replace('fxmain_', '', $value);
                        
                        if ($value=='create_date' || $value=='fxmain_create_date' || $fType==7 || $fType==9) {
                            $daterang = explode(' to ', $paramsValue);
                            
                            if(isset($daterang[1])){
                                $sdate = \common\lib\sdii\components\utils\SDdate::phpThDate2mysqlDate($daterang[0], '-');
                                $edate = \common\lib\sdii\components\utils\SDdate::phpThDate2mysqlDate($daterang[1], '-');
                                $query->andFilterWhere(['between', 'date('.$modelFields['main_ezf_table'].'.'.$origin.')', $sdate, $edate]);
                            }
                        } else {
                            $query->andFilterWhere(['like', $modelFields['main_ezf_table'].'.'.$origin, $paramsValue]);
                        }
                        
                    }
                }
	    }
	    
             
	}
        
        return $dataProvider;
    }
    
    public function searchGetItem($params, $ovfilter_sub)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $modelFields = Yii::$app->session['sql_main_fields'];
	
        $tbdata = new Tbdata();
        $tbdata->setTableName($modelFields['ezf_table']);
        $query = $tbdata->find();
	
        $sql_col[] = $modelFields['ezf_table'].'.id';
        
	$fields = Yii::$app->session['sql_fields'];
	
        $selectField = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
        
        $groupByArr = [];
        
        if(is_array($selectField) && !empty($selectField)){
            foreach ($selectField['field'] as $keyFd => $valueFd) {
                $groupByArr[] = $modelFields['ezf_table'].'.'.$valueFd;
            }
        } 
        
        $groupBy = ' GROUP BY '. implode(',', $groupByArr);
        
	$query->select($sql_col)
		->where("{$modelFields['ezf_table']}.rstat NOT IN(3)");
        
                
        $innerJoin = '';   
        $whereCount = '';
	if($modelFields['ezf_id']!=$modelFields['main_ezf_id']){
	    $pk = $modelFields['pk_field'];
	    $pkJoin = 'target';
	    if($modelFields['special']==1){
		$pk = 'ptid';
		$pkJoin = 'ptid';
	    }
            $innerJoin = "INNER JOIN {$modelFields['main_ezf_table']} ON `{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`";
            $whereCount .= " AND `{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)";
            if (isset($params['sort'])) {
                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
            }else{
                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
                //$query->innerJoin($modelFields['main_ezf_table']." FORCE INDEX (create_date)", "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
            }
	    $query->andWhere("`{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)");
	    
	}
        
	if($ovfilter_sub>0){
	    $modelFilter = InvFilterSub::find()->where('sub_id = :sub_id', [':sub_id'=>$ovfilter_sub])->one();
	    $condition = \appxq\sdii\utils\SDUtility::string2Array($modelFilter->options);
            
	    if(isset($condition) && is_array($condition) && !empty($condition)){
		$formCond = isset($condition['form'])?$condition['form']:[];
		$value1Cond = isset($condition['value1'])?$condition['value1']:[];
		$value2Cond = isset($condition['value2'])?$condition['value2']:[];
		$fieldCond = isset($condition['field'])?$condition['field']:[];
		$condCond = isset($condition['cond'])?$condition['cond']:[];
		$moreCond = isset($condition['more'])?$condition['more']:[];

		$formCondUnique = array_unique($formCond);
		
		$pk = $modelFields['pk_field'];
		$pkJoin = 'target';
		if($modelFields['special']==1){
		    $pk = 'ptid';
		    $pkJoin = 'ptid';
		}
		$ezformTmp = [];
		$ezformTmpUnipue = [];
		//innerJoin
		foreach ($formCondUnique as $key_cond => $value_cond) {
		    $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value_cond);
		    $ezformTmp[$ezform['ezf_id']] = $ezform['ezf_table'];
		    
		    if($ezform['ezf_table']!=$modelFields['ezf_table'] && $ezform['ezf_table']!=$modelFields['main_ezf_table'] ){//
			if(!in_array($ezform['ezf_table'], $ezformTmpUnipue)){
			    $query->innerJoin($ezform['ezf_table'], "{$ezform['ezf_table']}.$pkJoin = {$modelFields['ezf_table']}.$pk");
			    $ezformTmpUnipue[] = $ezform['ezf_table'];
			}
		    }
		}
		//Where
		$condType = 'AND';
		foreach ($formCond as $key_cond => $value_cond) {
		    if($condType=='AND'){
			if($condCond[$key_cond]!='between'){
			    $pos = strpos($value1Cond[$key_cond], '_FIELD_');
			    $checkPos = $pos===false;
			    if($value1Cond[$key_cond]=='_SITECODE_'){
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$sitecode]);
			    } elseif ($checkPos!==true) {
				$fieldStr = str_replace('_FIELD_', '', $value1Cond[$key_cond]);
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} $fieldStr");
			    }else {
				$query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			    }
			} else {
			    $query->andWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond1_$key_cond AND :cond2_$key_cond", [":cond1_$key_cond"=>$value1Cond[$key_cond], ":cond2_$key_cond"=>$value2Cond[$key_cond]]);
			}
		    } else {
			if($condCond[$key_cond]!='between'){
			    $pos = strpos($value1Cond[$key_cond], '_FIELD_');
			    $checkPos = $pos===false;
			    if($value1Cond[$key_cond]=='_SITECODE_'){
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$sitecode]);
			    } elseif ($checkPos!==true) {
				$fieldStr = str_replace('_FIELD_', '', $value1Cond[$key_cond]);
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} $fieldStr");
			    }else {
				$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			    }
			    //$query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond_$key_cond", [":cond_$key_cond"=>$value1Cond[$key_cond]]);
			} else {
			    $query->orWhere("{$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} {$condCond[$key_cond]} :cond1_$key_cond AND :cond2_$key_cond", [":cond1_$key_cond"=>$value1Cond[$key_cond], ":cond2_$key_cond"=>$value2Cond[$key_cond]]);
			}
		    }
		    $condType = $moreCond[$key_cond];
		}
	    } else {
		$query->innerJoin('inv_sub_list', "inv_sub_list.person_id = {$modelFields['ezf_table']}.id");
		$query->andWhere("inv_sub_list.sub_id = :sub_id", [':sub_id'=>$ovfilter_sub]);
	    }
	}
	
	if($modelFields['special']==1){
            $whereCount .= " AND {$modelFields['ezf_table']}.hsitecode = $sitecode";
	    $query->andWhere("{$modelFields['ezf_table']}.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
	} else {
            $whereCount .= " AND {$modelFields['ezf_table']}.xsourcex = $sitecode";
	    $query->andWhere("{$modelFields['ezf_table']}.xsourcex = :sitecode", [':sitecode'=>$sitecode]);
	}
        
        $query->orderBy($modelFields['main_ezf_table'].'.create_date desc');
	
//	if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
//            $orderBy = [];
//            $orderBy[$modelFields['main_ezf_table'].'.create_date'] = 'DESC';
//            $order_field = explode(',', $modelFields['order_field']);
//            if(is_array($order_field)){
//                foreach ($order_field as $keyOd => $valueOd) {
//                    $orderBy[$valueOd] = $modelFields['label_field'];
//                }
//            }
//	    $query->orderBy($orderBy);
//	} else {
//            $query->orderBy($modelFields['main_ezf_table'].'.create_date desc');
//        }
	
//	if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
//	    $query->orderBy($modelFields['order_field'].' '.$modelFields['label_field']);
//	}
        
	$query->groupBy("{$modelFields['ezf_table']}.id");
        
        $this->load($params);
        
        if (!$this->validate()) {
            return implode(',', $query->column());
        }

	if(isset($modelFields['enable_field'])){
	    $fields = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
	    
	    $safe = isset($fields['field'])?$fields['field']:[];
	    
	    foreach ($safe as $key => $value) {
		$pos = strpos($value, 'fxmain_');
		if ($pos === false) {
                    $paramsValue = $this->$value;
                    if($paramsValue!=''){
                        $query->andFilterWhere(['like', $modelFields['ezf_table'].'.'.$value, $this->$value]);
                        $whereCount .= " AND {$modelFields['ezf_table']}.$value like '%$paramsValue%'";
                    }
		}
	    }
	    
             
	}
        
        return implode(',', $query->column());
    }
    
    public function searchView($params, $jtable, $jselect, $jvalue)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $modelFields = Yii::$app->session['sql_main_fields'];
	
        $tbdata = new Tbdata();
        $tbdata->setTableName($modelFields['ezf_table']);
        $query = $tbdata->find();
	
	$ezform_main = \backend\modules\inv\classes\InvQuery::getEzformById($modelFields['ezf_id']);
	
	$sql_col[] = "{$modelFields['ezf_table']}.*";
	
//	if(isset($ezform_main['field_detail']) && $ezform_main['field_detail']!=''){
//	    $sql_col[] = "CONCAT({$ezform_main['field_detail']}) AS detail_main";
//	}

	$pk = $modelFields['pk_field'];
	$pkJoin = 'target';
	if($modelFields['special']==1){
	    $pk = 'ptid';
	    $pkJoin = 'ptid';
	}
	$query->innerJoin($jtable, "$jtable.$pkJoin = {$modelFields['ezf_table']}.$pk");
	$sql_col[] = "$jtable.id AS id2";
	
	
	$query->select($sql_col)
		->where("{$modelFields['ezf_table']}.rstat NOT IN(3,0)");
	
	
	if($modelFields['ezf_id']!=$modelFields['main_ezf_id']){
	    $pk = $modelFields['pk_field'];
	    $pkJoin = 'target';
	    if($modelFields['special']==1){
		$pk = 'ptid';
		$pkJoin = 'ptid';
	    }
	    
	    if($jtable != $modelFields['main_ezf_table']){
		$query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
	    }
	    $query->andWhere("`{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)");
	    
	}
	
	if($jvalue=='null'){
	    $query->andWhere("$jtable.$jselect IS NULL");
	} else {
	    
	    $query->andWhere("$jtable.$jselect = :jselect", [':jselect'=>$jvalue]);
	}
	
	
	if($modelFields['special']==1){
	    $query->andWhere("{$modelFields['ezf_table']}.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
	} 
	
	if(isset($modelFields['order_field']) && !empty($modelFields['order_field'])){
	    $query->orderBy($modelFields['order_field'].' '.$modelFields['label_field']);
	}
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }
 
        $query->andFilterWhere([
            'id' => $this->id,
            'rstat' => $this->rstat,
        ]);
        
        $query->andFilterWhere(['like', 'xsourcex', $this->xsourcex]);
	
	if(isset($modelFields['enable_field'])){
	    $fields = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_field']);
	    $safe = isset($fields['field'])?$fields['field']:[];
	    foreach ($safe as $key => $value) {
		$query->andFilterWhere(['like', $value, $this->$value]);
	    }
	}
	
        return $dataProvider;
    }
}
