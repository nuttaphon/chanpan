<?php

namespace backend\modules\inv\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inv\models\InvFields;

/**
 * InvFieldsSearch represents the model behind the search form about `backend\modules\inv\models\InvFields`.
 */
class InvFieldsSearch extends InvFields
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['if_id', 'sub_id', 'ezf_id', 'comp_id_target', 'width'], 'integer'],
            [['ezf_name', 'ezf_table', 'result_field', 'sql_id_name', 'sql_result_name', 'sql_idsubmit_name','value_options', 'header'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $sub_id)
    {
        $query = InvFields::find()->where('sub_id=:sub_id', [':sub_id'=>$sub_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'if_id' => $this->if_id,
            'sub_id' => $this->sub_id,
            'ezf_id' => $this->ezf_id,
            'comp_id_target' => $this->comp_id_target,
            'width' => $this->width,
        ]);

        $query->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table])
            ->andFilterWhere(['like', 'result_field', $this->result_field])
            ->andFilterWhere(['like', 'sql_id_name', $this->sql_id_name])
            ->andFilterWhere(['like', 'sql_result_name', $this->sql_result_name])
            ->andFilterWhere(['like', 'value_options', $this->value_options])
            ->andFilterWhere(['like', 'header', $this->header]);

        return $dataProvider;
    }
}
