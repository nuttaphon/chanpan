<?php

namespace backend\modules\inv\models;

use Yii;

/**
 * This is the model class for table "inv_user".
 *
 * @property integer $id
 * @property integer $gid
 * @property integer $user_id
 * @property string $username
 * @property string $password
 * @property string $sitecode
 */
class InvUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gid', 'user_id', 'username', 'password', 'sitecode'], 'required'],
            [['gid', 'user_id'], 'integer'],
            [['username', 'sitecode'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'gid' => Yii::t('app', 'Gid'),
            'user_id' => Yii::t('app', 'User ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'sitecode' => Yii::t('app', 'Sitecode'),
        ];
    }
}
