<?php

namespace backend\modules\Comments\models;

use Yii;
use \backend\models\UserProfile;
/**
 * This is the model class for table "cmd".
 *
 * @property integer $id
 * @property string $gid
 * @property integer $user_id
 * @property string $message
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_by
 * @property string $updated_at
 */
class Cmd extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return "inv_comment";
    }



    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'gid', 'user_id','message', 'created_by', 'updated_by','vote'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['message'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gid' => 'Gid',
            'user_id' => 'User ID',
            'message' => 'Message',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    public function checkValidations($keys_word)
    {
         //คำสั่งที่เราไม่ต้องการให้พิมพ์
        $keys = "><=!/|?@#$^&*(_+-\)"; 
        $count_str = strlen($keys);
        $keys_arr = str_split($keys);


        //ค่าที่ input จาก user
        $x = $keys_word;
        $x_arr = str_split($x);
        $x_count = strlen($x);
        $status = 0;
       
        foreach($keys_arr as $k=>$v){//ค่าที่เราไม่ต้องการให้พิมพ์นะครบ 
            for($i=0; $i<$x_count; $i++)
            {
                if($x_arr[$i] == $v)
                {
                    $status =1;
                }
            }
        }
        if($status == 1)
        {
            return true;
        }else{
            return false;
        }
    }//checkValidations

    public function getAvatar()
    {
        $user_id = $this->userprofile->user_id;
        foreach($user_id as $id)
        {

        }
        return $this->avatar_path
            ? Yii::getAlias($this->avatar_base_url . '/' . $this->avatar_path)
            : false;
    }//getAvatar
    public function getUserprofile()
    {
        return $this->hasOne(UserProfile::className(),['user_id'=>'user_id']);
    }
    
}
