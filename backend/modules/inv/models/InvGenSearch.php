<?php

namespace backend\modules\inv\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inv\models\InvGen;

/**
 * InvGenSearch represents the model behind the search form about `backend\modules\inv\models\InvGen`.
 */
class InvGenSearch extends InvGen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'gid', 'public', 'created_by', 'updated_by', 'ezf_id', 'comp_id_target', 'special'], 'integer'],
            [['gname', 'gdetail', 'gicon', 'share', 'created_at', 'updated_at', 'sitecode', 'ezf_name', 'ezf_table', 'pk_field', 'field_name', 'label_field', 'order_field', 'enable_field', 'enable_form', 'enable_report'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
	$userId = Yii::$app->user->id;
	
        $query = InvGen::find()->where('active=1 AND created_by=:created_by', [':created_by'=>$userId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'gid' => $this->gid,
	    'active' => $this->active,
            'public' => $this->public,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'ezf_id' => $this->ezf_id,
            'comp_id_target' => $this->comp_id_target,
            'special' => $this->special,
        ]);

        $query->andFilterWhere(['like', 'gname', $this->gname])
            ->andFilterWhere(['like', 'gdetail', $this->gdetail])
            ->andFilterWhere(['like', 'gicon', $this->gicon])
            ->andFilterWhere(['like', 'share', $this->share])
            ->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table])
            ->andFilterWhere(['like', 'pk_field', $this->pk_field])
            ->andFilterWhere(['like', 'field_name', $this->field_name])
            ->andFilterWhere(['like', 'label_field', $this->label_field])
            ->andFilterWhere(['like', 'order_field', $this->order_field])
            ->andFilterWhere(['like', 'enable_field', $this->enable_field])
            ->andFilterWhere(['like', 'enable_form', $this->enable_form])
            ->andFilterWhere(['like', 'enable_report', $this->enable_report]);

        return $dataProvider;
    }
    
    public function searchAuth($params)
    {
	$userId = Yii::$app->user->id;
	
        $query = InvGen::find()->where('active=1 AND public=1');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'gid' => $this->gid,
	    'active' => $this->active,
            'public' => $this->public,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'ezf_id' => $this->ezf_id,
            'comp_id_target' => $this->comp_id_target,
            'special' => $this->special,
        ]);

        $query->andFilterWhere(['like', 'gname', $this->gname])
            ->andFilterWhere(['like', 'gdetail', $this->gdetail])
            ->andFilterWhere(['like', 'gicon', $this->gicon])
            ->andFilterWhere(['like', 'share', $this->share])
            ->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'ezf_name', $this->ezf_name])
            ->andFilterWhere(['like', 'ezf_table', $this->ezf_table])
            ->andFilterWhere(['like', 'pk_field', $this->pk_field])
            ->andFilterWhere(['like', 'field_name', $this->field_name])
            ->andFilterWhere(['like', 'label_field', $this->label_field])
            ->andFilterWhere(['like', 'order_field', $this->order_field])
            ->andFilterWhere(['like', 'enable_field', $this->enable_field])
            ->andFilterWhere(['like', 'enable_form', $this->enable_form])
            ->andFilterWhere(['like', 'enable_report', $this->enable_report]);

        return $dataProvider;
    }
}
