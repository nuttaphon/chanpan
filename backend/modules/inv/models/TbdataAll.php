<?php

namespace backend\modules\inv\models;

use Yii;
/**
 * This is the model class for table "tbdata_1435745159010048800".
 *
 * @property integer $id
 */
class TbdataAll extends \yii\db\ActiveRecord
{
    protected static $table;
    public $field_desc;

//    public function _construct($table, $scenario)
//    {
//        parent::_construct($scenario);
//        self::$table = $table;
//    }

    public function attributes()
    {
	$attrDB = array_keys(static::getTableSchema()->columns);
	$colFieldsID=[];
	
        return array_merge($attrDB, $colFieldsID);
    }
    
    public static function tableName()
    {
        return self::$table;
    }

    /* UPDATE */
    public static function setTableName($table)
    {
        self::$table = $table;
    }

}
