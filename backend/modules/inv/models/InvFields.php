<?php

namespace backend\modules\inv\models;

use Yii;

/**
 * This is the model class for table "inv_fields".
 *
 * @property integer $if_id
 * @property integer $comp
 * @property integer $sub_id
 * @property integer $ezf_id
 * @property string $ezf_name
 * @property string $ezf_table
 * @property integer $comp_id_target
 * @property string $result_field
 * @property string $sql_id_name
 * @property string $sql_idsubmit_name
 * @property string $sql_result_name
 * @property string $value_options
 * @property string $header
 * @property integer $width
 * @property integer $report_width
 * @property integer $report_show
 */
class InvFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inv_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp', 'sub_id', 'ezf_id', 'header', 'width', 'report_width'], 'required'],
            [['comp', 'sub_id', 'ezf_id', 'comp_id_target', 'width', 'report_width'], 'integer'],
            [['value_options', 'report_show'], 'string'],
            [['ezf_name', 'ezf_table', 'result_field', 'sql_id_name', 'sql_idsubmit_name','sql_result_name', 'header'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'if_id' => Yii::t('app', 'If ID'),
	    'comp' => Yii::t('app', 'Comp'),
            'sub_id' => Yii::t('app', 'Sub ID'),
            'ezf_id' => Yii::t('app', 'Field'),
            'ezf_name' => Yii::t('app', 'Field'),
            'ezf_table' => Yii::t('app', 'Ezf Table'),
            'comp_id_target' => Yii::t('app', 'Comp Id Target'),
            'result_field' => Yii::t('app', 'Visible Condition'),
            'sql_id_name' => Yii::t('app', 'Sql Id Name'),
            'sql_result_name' => Yii::t('app', 'Sql Result Name'),
            'value_options' => Yii::t('app', 'Value Options'),
            'header' => Yii::t('app', 'Label'),
            'width' => Yii::t('app', 'Width'),
	    'report_width' => Yii::t('app', 'PDF Width'),
	    'report_show' => Yii::t('app', 'Show in PDF?'),
        ];
    }
}
