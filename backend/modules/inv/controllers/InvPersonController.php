<?php

namespace backend\modules\inv\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use common\lib\tcpdf\SDPDF;
use common\lib\sdii\components\utils\SDdate;
use yii\helpers\ArrayHelper;
use backend\modules\inv\models\TbData1Search;
use backend\modules\inv\models\TbData1;
use backend\modules\inv\classes\InvQuery;
use backend\modules\inv\models\InvFilterSub;
use backend\modules\inv\models\InvSubList;
use backend\modules\inv\models\InvMain;
use backend\modules\ezforms\models\Ezform;
use yii\helpers\Json;
use backend\modules\inv\models\Tbdata;
use backend\modules\inv\models\TbdataSearch;
use backend\modules\inv\classes\InvFunc;
use backend\modules\inv\models\InvGen;
use yii\web\UploadedFile;
use backend\modules\inv\models\InvFavorite;
use yii\db\Expression;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\component\models\EzformComponent;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\models\EzformTarget;
use yii\db\Query;
use backend\modules\ezforms\components\EzformFunc;

//nut
use backend\modules\comments\models\Cmd;
use backend\modules\comments\models\CmdSearch;

/**
 * OvPersonController implements the CRUD actions for OvPerson model.
 */
class InvPersonController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes', 'import-all', 'import-views', 'ovfilter'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    //nut
    public $table= "inv_comment";
    public $table_user_profile = "user_profile";


    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    public function actionOvfilterSub()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $comp = isset($_GET['comp'])?$_GET['comp']:0;
	    $module = isset($_GET['module'])?$_GET['module']:0;
	    
	    $model = new InvFilterSub();
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    $model->sitecode = $sitecode;
	    $model->urine_status = 1;
	    $model->filter_id = $comp;
	    $model->gid = $module;
	    $model->filter_order = 0;
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model->sitecode = $sitecode;
		$model->created_by = Yii::$app->user->id;
		$model->filter_id = $comp;
		$model->gid = $module;
		
		if(isset($_POST['options'])){
		    $model->options = \appxq\sdii\utils\SDUtility::array2String($_POST['options']);
		} else {
		    $model->options = '';
		}
		
		if(isset($model->share) && !empty($model->share)){
		    $model->share = implode(',', $model->share);
		}
		
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('_form_fileter_sub', [
		    'model' => $model,
		    'comp' => $comp,
		    'module' => $module,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    /**
     * Lists all OvPerson models.
     * @return mixed
     */
    public function actionProxy()
    {
	$module = isset($_GET['module'])?$_GET['module']:0;
	$userId = Yii::$app->user->id;
	
	$ezform = InvQuery::getComp();
	
	if(!$ezform){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'ท่านไม่ได้เลือกฟอร์มที่มีคำถามพิเศษซึ่งเก็บข้อมูลเป้าหมายการบันทึกข้อมูลกรุณาเลือกฟอร์มดังกล่าวก่อน '.\yii\helpers\Html::a('ที่นี่', ['/inputdata/index'], ['class'=>'btn btn-warning']),
		'options'=>['class'=>'alert-info']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	$comp = $ezform[0]['ezf_id'];
	$inv_main = InvMain::find()->where('created_by=:userId', [':userId'=>$userId])->limit(1)->one();
	
	if($inv_main && isset($inv_main['main_ezf_id']) && !empty($inv_main['main_ezf_id'])){
	    $comp = $inv_main['main_ezf_id'];
	} 
	
	return $this->redirect(['main', 'main_ezf_id'=>$comp]);
    }
    
    
    
    public function actionIndex()
    {
        //return $this->render('offline');
        
	$module = isset($_GET['module'])?$_GET['module']:0;
	
	$ezform = InvQuery::getCompAll();
	
	if(!$ezform){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'ท่านไม่ได้เลือกฟอร์มที่มีคำถามพิเศษซึ่งเก็บข้อมูลเป้าหมายการบันทึกข้อมูลกรุณาเลือกฟอร์มดังกล่าวก่อน '.\yii\helpers\Html::a('ที่นี่', ['/inputdata/index'], ['class'=>'btn btn-warning']),
		'options'=>['class'=>'alert-danger']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$userId = Yii::$app->user->id;
	
	$comp;
	$inv_main;
	if($module>0){
	    $inv_main = InvQuery::getModule($module, $userId);
	    if($inv_main){
		$comp = $inv_main['ezf_id'];
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ไม่พบโมดูล',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/tccbots/my-module']);
	    }
	} else {
	    if(isset($_GET['comp'])){
		$comp = $_GET['comp'];
		$inv_main = InvMain::find()->where('created_by=:userId AND main_ezf_id=:comp', [':userId'=>$userId, ':comp'=>$comp])->one();
	    } else {
		$inv_main = InvMain::find()->where('created_by=:userId', [':userId'=>$userId])->limit(1)->one();
		if($inv_main){
		    $comp = $inv_main['main_ezf_id'];
		} else {
		    $comp = $ezform[0]['ezf_id'];
		}
	    } 
	    
	    if(!$inv_main){
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ท่านยังไม่ได้ตั้งค่าเริ่มต้น กรุณาตั้งค่าฟอร์มเริ่มต้น',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['main', 'main_ezf_id'=>$comp]);
	    }
	}
	
	$ovfilter_sub = isset($_GET['ovfilter_sub'])?$_GET['ovfilter_sub']:0;
	
	$modelFilter = InvFilterSub::find()->where('sub_id = :sub_id', [':sub_id'=>$ovfilter_sub])->one();
	
	Yii::$app->session['sql_fields'] = false;//InvQuery::getInvFields($ovfilter_sub);
	Yii::$app->session['sql_main_fields'] = $inv_main;
	if($inv_main['comp_id_target']=='100000' || $inv_main['comp_id_target']=='100001'|| empty($inv_main['comp_id_target'])){
	    Yii::$app->session['sql_main_fields']['pk_join'] = 'id';
	} else {
	    Yii::$app->session['sql_main_fields']['pk_join'] = InvQuery::getPkJoin($inv_main['comp_id_target']);
	}
	
	 
	$dataOvFilterSub = InvQuery::getFilterSub($sitecode, $comp, $module, $modelFilter);
	
	try {
	    $searchModel = new TbdataSearch();
            
	    $dataProvider = $searchModel->searchDpMain(Yii::$app->request->queryParams, $ovfilter_sub);
	    //$dataProvider->pagination->pageSize = 20;
            
	    $modelFields = Yii::$app->session['sql_main_fields'];
	    
	    $pk = $modelFields['pk_field'];
	    $pkJoin = 'target';
	    if($modelFields['special']==1){
		$pk = 'ptid';
		$pkJoin = 'ptid';
	    }
	    $where = '';
	    $join = '';
	    if($modelFields['ezf_table']!=$modelFields['main_ezf_table'] ){
		$where = "`{$modelFields['ezf_table']}`.rstat NOT IN(0,3) AND ";
		$join = "inner join `{$modelFields['ezf_table']}` ON `{$modelFields['ezf_table']}`.$pk = `{$modelFields['main_ezf_table']}`.$pkJoin";
	    }
	    
	    $sql = "SELECT COUNT(*) FROM `{$modelFields['main_ezf_table']}`
		    $join
		 WHERE $where `{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3) AND `{$modelFields['main_ezf_table']}`.xsourcex = :sitecode";
		 
	    if($modelFields['special']==1){
		$sql = "SELECT COUNT(*) FROM `{$modelFields['main_ezf_table']}`
		    $join
		 WHERE $where `{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3) AND `{$modelFields['main_ezf_table']}`.hsitecode = :sitecode";
	    } 
	    
	    
	    $totalAll = Yii::$app->db->createCommand($sql, [':sitecode'=>$sitecode])->queryScalar();

//	    $tbdata = new Tbdata();
//	    $tbdata->setTableName($modelFields['main_ezf_table']);
//	    $query = $tbdata->find();
//	    $query->where('rstat NOT IN(0,3)');
//
//	    if($modelFields['special']==1){
//		$query->andWhere("hsitecode = :sitecode", [':sitecode'=>$sitecode]);
//	    } 
//
//	    $totalAll = $query->count();
	    
	    //$totalAll = $dataProvider->totalCount;

	    return $this->render('index', [
		'searchModel' => $searchModel,
		'dataProvider' => $dataProvider,
		'dataOvFilterSub' => $dataOvFilterSub,
		'ovfilter_sub'=>$ovfilter_sub,
		'ezform'=>$ezform,
		'comp'=>$comp,
		'module' => $module,
		'sitecode' =>$sitecode,
		'totalAll'=>$totalAll,
		'inv_main'=>$inv_main,
		'modelFilter' => $modelFilter,
	    ]);
	} catch (\yii\db\Exception $e) {
	    return $this->render('error', [
		'inv_main'=>$inv_main,
		'ovfilter_sub'=>$ovfilter_sub,
		'module' => $module,
		'sitecode' =>$sitecode,
		'comp'=>$comp,
		'msg'=>$e->getMessage(),
	    ]);
	}
	
    }
    public function actionBackdoor()
    {
        /////return $this->render('offline');
        
	$module = isset($_GET['module'])?$_GET['module']:0;
	
	$ezform = InvQuery::getComp();
	
	if(!$ezform){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'ท่านไม่ได้เลือกฟอร์มที่มีคำถามพิเศษซึ่งเก็บข้อมูลเป้าหมายการบันทึกข้อมูลกรุณาเลือกฟอร์มดังกล่าวก่อน '.\yii\helpers\Html::a('ที่นี่', ['/inputdata/index'], ['class'=>'btn btn-warning']),
		'options'=>['class'=>'alert-danger']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$userId = Yii::$app->user->id;
	
	$comp;
	$inv_main;
	if($module>0){
	    $inv_main = InvQuery::getModule($module, $userId);
	    if($inv_main){
		$comp = $inv_main['ezf_id'];
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ไม่พบโมดูล',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/tccbots/my-module']);
	    }
	} else {
	    if(isset($_GET['comp'])){
		$comp = $_GET['comp'];
		$inv_main = InvMain::find()->where('created_by=:userId AND ezf_id=:comp', [':userId'=>$userId, ':comp'=>$comp])->one();
	    } else {
		$inv_main = InvMain::find()->where('created_by=:userId', [':userId'=>$userId])->limit(1)->one();
		if($inv_main){
		    $comp = $inv_main['ezf_id'];
		} else {
		    $comp = $ezform[0]['ezf_id'];
		}
	    } 
	   
	    if(!$inv_main){
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> 'ท่านยังไม่ได้ตั้งค่าเริ่มต้น กรุณาตั้งค่าฟอร์มเริ่มต้น',
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['main', 'main_ezf_id'=>$comp]);
	    }
	}
	
	$ovfilter_sub = isset($_GET['ovfilter_sub'])?$_GET['ovfilter_sub']:0;
	
	$modelFilter = InvFilterSub::find()->where('sub_id = :sub_id', [':sub_id'=>$ovfilter_sub])->one();
	
	Yii::$app->session['sql_fields'] = false;//InvQuery::getInvFields($ovfilter_sub);
	Yii::$app->session['sql_main_fields'] = $inv_main;
	if($inv_main['comp_id_target']=='100000' || $inv_main['comp_id_target']=='100001'|| empty($inv_main['comp_id_target'])){
	    Yii::$app->session['sql_main_fields']['pk_join'] = 'id';
	} else {
	    Yii::$app->session['sql_main_fields']['pk_join'] = InvQuery::getPkJoin($inv_main['comp_id_target']);
	}
	
	 
	$dataOvFilterSub = InvQuery::getFilterSub($sitecode, $comp, $module, $modelFilter);
	
	 
	try {
	    $searchModel = new TbdataSearch();

	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ovfilter_sub);
	    $dataProvider->pagination->pageSize = 100;

	    $modelFields = Yii::$app->session['sql_main_fields'];

	    $tbdata = new Tbdata();
	    $tbdata->setTableName($modelFields['ezf_table']);
	    $query = $tbdata->find();
	    $query->where('rstat<>3');

	    if($modelFields['special']==1){
		$query->andWhere("hsitecode = :sitecode", [':sitecode'=>$sitecode]);
	    } 

	    $totalAll = $query->count();

	    return $this->render('index', [
		'searchModel' => $searchModel,
		'dataProvider' => $dataProvider,
		'dataOvFilterSub' => $dataOvFilterSub,
		'ovfilter_sub'=>$ovfilter_sub,
		'ezform'=>$ezform,
		'comp'=>$comp,
		'module' => $module,
		'sitecode' =>$sitecode,
		'totalAll'=>$totalAll,
		'inv_main'=>$inv_main,
		'modelFilter' => $modelFilter,
	    ]);
	} catch (\yii\db\Exception $e) {
	    return $this->render('error', [
		'inv_main'=>$inv_main,
		'ovfilter_sub'=>$ovfilter_sub,
		'module' => $module,
		'sitecode' =>$sitecode,
		'comp'=>$comp,
		'msg'=>$e->getMessage(),
	    ]);
	}
	
    }    
    public function actionMain($main_ezf_id)
    {
	$module = isset($_GET['module'])?$_GET['module']:0;
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$userId = Yii::$app->user->id;
	
	$ezform = InvQuery::getComp();
	
	
	$dataComp = ArrayHelper::map($ezform, 'ezf_id', 'special');
	$dataComp2 = ArrayHelper::map($ezform, 'ezf_id', 'ezf_field_name');
	
	$modelEzformMain = Ezform::find()
		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$main_ezf_id])
		->one();
	$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$modelEzformMain['comp_id_target']])->queryOne();
	$ezf_id = $ezform_comp['ezf_id'];
	
	$model = InvMain::find()->where('created_by=:userId AND main_ezf_id=:ezf_id', [':userId'=>$userId, ':ezf_id'=>$main_ezf_id])->one();
	
	if(!$model){
	    
	    $model = new InvMain();
	    $model->sitecode = $sitecode;
	    $model->created_by = $userId;
	    $model->main_ezf_id = $main_ezf_id;
	    $model->special = isset($dataComp[$ezf_id])?$dataComp[$ezf_id]:0;
	    $model->pk_field = isset($dataComp2[$ezf_id])?$dataComp2[$ezf_id]:'id';
	} else {
	    Yii::$app->session['create_module'] = $model->attributes;
	    $model->main_ezf_id = $main_ezf_id;
	    $model->special = isset($dataComp[$ezf_id])?$dataComp[$ezf_id]:0;
	    $model->pk_field = isset($dataComp2[$ezf_id])?$dataComp2[$ezf_id]:'id';
	    $model->field_name = explode(',', $model->field_name);
	    $model->order_field = explode(',', $model->order_field);
	    $model->enable_field = \appxq\sdii\utils\SDUtility::string2Array($model->enable_field);
	    $model->enable_form = \appxq\sdii\utils\SDUtility::string2Array($model->enable_form);
	} 
	
	
	
	$model->ezf_id = $ezf_id;
	
	$modelEzform = Ezform::find()
		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$model->ezf_id])
		->one();
	
	$dataFields = InvFunc::getFields($model->ezf_id);
        //appxq\sdii\utils\VarDumper::dump($modelEzform);
	if(isset($modelEzform->comp_id_target) && !empty($modelEzform->comp_id_target)){
	    $dataCompMain = \backend\modules\component\models\EzformComponent::find()->select('ezform_component.*, ezform.ezf_table')
		    ->innerJoin('ezform', 'ezform_component.ezf_id=ezform.ezf_id')
		    ->where('comp_id=:comp_id',[':comp_id'=>$modelEzform->comp_id_target])->one();
            //\appxq\sdii\utils\VarDumper::dump($dataCompMain);
	    if($dataCompMain && $model->main_ezf_id!=$model->ezf_id){
		$dataFieldsMain = InvFunc::getFields($model->main_ezf_id, "fxmain_");
		$dataFields = array_merge(['ฟิลด์จากฟอร์มเป้าหมาย ('.$dataCompMain['comp_name'].')'=>$dataFields], ['ฟิลด์จากฟอร์มตั้งต้น ('.$model['main_ezf_name'].')'=>$dataFieldsMain]);
	    }
	}
	
	//\appxq\sdii\utils\VarDumper::dump($dataFields);
	$fromOwn = ArrayHelper::map(InvQuery::getEzformByOwn($modelEzform->comp_id_target),'id','name');
	$fromFav = ArrayHelper::map(InvQuery::getEzformByFav($modelEzform->comp_id_target),'id','name');
	$fromAssign = ArrayHelper::map(InvQuery::getEzformByAssign($modelEzform->comp_id_target, $sitecode),'id','name');
	
	//$fromAll = array_merge(['ฟอร์มที่ฉันสร้าง'=>$fromOwn], ['ฟอร์มที่ฉันเลือก Favorite'=>$fromFav]);
	$dataSubForm = array_merge(['ฟอร์มที่ฉันสร้าง'=>$fromOwn], ['ฟอร์มที่ฉันเลือก Favorite'=>$fromFav], ['ฟอร์มที่ถูกแชร์มาให้ฉันหรือสาธารณะ'=>$fromAssign]);
	$dataSelect = array_merge($dataFields, $dataSubForm);
	
	
	
	
	if ($model->load(Yii::$app->request->post())) {
	    $model->special = isset($dataComp[$ezf_id])?$dataComp[$ezf_id]:0;
	    $model->pk_field = isset($dataComp2[$ezf_id])?$dataComp2[$ezf_id]:'id';
	    
	    if ($model->validate()) {

		$model->ezf_id = $ezf_id;
		
		$model->main_ezf_name = $modelEzformMain->ezf_name;
		$model->main_ezf_table = $modelEzformMain->ezf_table;
		$model->main_comp_id_target = $modelEzformMain->comp_id_target;
		
		$model->ezf_name = $modelEzform->ezf_name;
		$model->ezf_table = $modelEzform->ezf_table;
		$model->comp_id_target = $modelEzform->comp_id_target;
		
		if(isset($model->field_name) && !empty($model->field_name)){
		    $model->field_name = implode(',', $model->field_name);
		}
		if(isset($model->order_field) && !empty($model->order_field)){
		    $model->order_field = implode(',', $model->order_field);
		}
		//----------------------------------
		$enable_field = '';
		if(isset($_POST['fields'])){
		    $enable_field = \appxq\sdii\utils\SDUtility::array2String($_POST['fields']);
		}
		$model->enable_field = $enable_field;
		//----------------------------------
		$enable_form = '';
		if(isset($_POST['forms'])){
		    $enable_form = \appxq\sdii\utils\SDUtility::array2String($_POST['forms']);
		}
		$model->enable_form = $enable_form;
		//----------------------------------
		
	    
		Yii::$app->session['create_module'] = $model->attributes;
		if ($model->save()) {
		    Yii::$app->getSession()->setFlash('alert', [
			'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'options'=>['class'=>'alert-success']
		    ]);
		} else {
		    Yii::$app->getSession()->setFlash('alert', [
			'body'=> SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'options'=>['class'=>'alert-danger']
		    ]);
		}
		
		$model->field_name = explode(',', $model->field_name);
		$model->order_field = explode(',', $model->order_field);
		$model->enable_field = \appxq\sdii\utils\SDUtility::string2Array($model->enable_field);
		$model->enable_form = \appxq\sdii\utils\SDUtility::string2Array($model->enable_form);
		
		
	    }
	}
	
	return $this->render('main', [
            'sitecode' =>$sitecode,
	    'userId'=>$userId,
	    'model' => $model,
	    'module' => $module,
	    'ezform'=>$ezform,
	    'ezf_id'=>$ezf_id,
	    'modelEzformMain'=>$modelEzformMain,
	    'main_ezf_id'=>$main_ezf_id,
	    'dataFields' => $dataFields,
	    'modelEzform' => $modelEzform,
	    'dataSubForm' => $dataSubForm,
	    'dataSelect' => $dataSelect,
        ]);
    }

    
    public function actionCreateModules()
    {
	$module = isset($_GET['module'])?$_GET['module']:0;
	$main_ezf_id = isset($_GET['main_ezf_id'])?$_GET['main_ezf_id']:0;
	$userId = Yii::$app->user->id;
	
	
	
	$model = InvQuery::getModule($module, $userId);
	$ezform = InvQuery::getCompAll();
	
	$dataComp = ArrayHelper::map($ezform, 'ezf_id', 'special');
	$dataComp2 = ArrayHelper::map($ezform, 'ezf_id', 'ezf_field_name');
	
	if($model){
	    $ezf_id = $model->ezf_id;
	    $model->special = isset($dataComp[$ezf_id])?$dataComp[$ezf_id]:0;
	    $model->pk_field = isset($dataComp2[$ezf_id])?$dataComp2[$ezf_id]:'id';
	    $model->field_name = explode(',', $model->field_name);
	    $model->order_field = explode(',', $model->order_field);
	    $model->enable_field = \appxq\sdii\utils\SDUtility::string2Array($model->enable_field);
	    $model->enable_form = \appxq\sdii\utils\SDUtility::string2Array($model->enable_form);
	    
	    if(($model->public==1 && Yii::$app->user->can('administrator')) || $model['created_by']==Yii::$app->user->id){

	    } else {
		$userProfile = Yii::$app->user->identity->userProfile;
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgError() . Yii::t('app', 'คุณไม่มีสิทธิ์แก้ไขโมดูลนี้กรุณาติดต่อผู้สร้าง  '.'คุณ '.$userProfile->firstname.' '.$userProfile->lastname. ' เบอร์โทร '.$userProfile->telephone),
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/inv/inv-person/index', 'module'=>$module]);
	    }	
	} else {
	    if(!isset(Yii::$app->session['create_module'])){
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'ไม่พบข้อมูลตั้งค่า'),
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/inv/inv-person/index', 'module'=>$module]);
	    }
	    $model = new InvGen();
	    $model->attributes = Yii::$app->session['create_module'];
	    $model->created_by = Yii::$app->user->id;
	    $model->active = 1;
	    $model->gsystem = 0;
            $model->enable_target = 0;
	}
	
	$modelEzform = Ezform::find()
		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$model->ezf_id])
		->one();
	
	$dataFields = InvFunc::getFields($model->ezf_id);
	
	if(isset($modelEzform->comp_id_target) && !empty($modelEzform->comp_id_target)){
	    $dataCompMain = \backend\modules\component\models\EzformComponent::find()->select('ezform_component.*, ezform.ezf_table')
		    ->innerJoin('ezform', 'ezform_component.ezf_id=ezform.ezf_id')
		    ->where('comp_id=:comp_id',[':comp_id'=>$modelEzform->comp_id_target])->one();
            //\appxq\sdii\utils\VarDumper::dump($dataCompMain);
	    if($dataCompMain && $model->main_ezf_id!=$model->ezf_id){
		$dataFieldsMain = InvFunc::getFields($model->main_ezf_id, "fxmain_");
		$dataFields = array_merge(['ฟิลด์จากฟอร์มเป้าหมาย ('.$dataCompMain['comp_name'].')'=>$dataFields], ['ฟิลด์จากฟอร์มตั้งต้น ('.$model['main_ezf_name'].')'=>$dataFieldsMain]);
	    }
	}
	
	//\appxq\sdii\utils\VarDumper::dump($dataFields);
	$fromOwn = ArrayHelper::map(InvQuery::getEzformByOwn($modelEzform->comp_id_target),'id','name');
	$fromFav = ArrayHelper::map(InvQuery::getEzformByFav($modelEzform->comp_id_target),'id','name');
	$fromAssign = ArrayHelper::map(InvQuery::getEzformByAssign($modelEzform->comp_id_target, $sitecode),'id','name');
	
	//$fromAll = array_merge(['ฟอร์มที่ฉันสร้าง'=>$fromOwn], ['ฟอร์มที่ฉันเลือก Favorite'=>$fromFav]);
	$dataSubForm = array_merge(['ฟอร์มที่ฉันสร้าง'=>$fromOwn], ['ฟอร์มที่ฉันเลือก Favorite'=>$fromFav], ['ฟอร์มที่ถูกแชร์มาให้ฉันหรือสาธารณะ'=>$fromAssign]);
	$dataSelect = array_merge($dataFields, $dataSubForm);
	//$model->share = explode(',', $model->share);

	$oldFileName = $model->gicon;
	
	if ($model->load(Yii::$app->request->post())) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if(isset($model->share) && !empty($model->share)){
		$model->share = implode(',', $model->share);
	    }
	    
	    if($_POST['action_submit']=='submit'){
		$model->active = 1;
	    } else {
		$model->active = 0;
	    }
	    
	    if($model->public==0){
		$model->approved = 0;
	    } 
	    
	    if (isset($_FILES['InvGen']['name']['gicon']) && $_FILES['InvGen']['name']['gicon']!='') {
		$icon_file = UploadedFile::getInstance($model, 'gicon');
		
		$typeArry = explode('/', $icon_file->type);
		
		$typeName = 'jpg';
		if(isset($typeArry[1])){
		    $typeName = $typeArry[1];
		}
		
		$newFileName = 'icon_'.date("Ymd_His").'_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'.'.$typeName;
		$fullPath = Yii::$app->basePath . '/web/module_icon/' . $newFileName;
		
		$icon_file->saveAs($fullPath);
		
		@unlink(Yii::$app->basePath . '/web/module_icon/' .$oldFileName);
		
		$model->gicon = $newFileName;
	    } else {
		$model->gicon = isset($oldFileName)?$oldFileName:'';
	    }
	    //$model->gid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
	    $action = $model->isNewRecord;
	    if(!$model->isNewRecord){
		
		if(isset($model->field_name) && !empty($model->field_name)){
		    $model->field_name = implode(',', $model->field_name);
		}
		if(isset($model->order_field) && !empty($model->order_field)){
		    $model->order_field = implode(',', $model->order_field);
		}
		//----------------------------------
		$enable_field = '';
		if(isset($_POST['fields'])){
		    $enable_field = \appxq\sdii\utils\SDUtility::array2String($_POST['fields']);
		}
		$model->enable_field = $enable_field;
		//----------------------------------
		$enable_form = '';
		if(isset($_POST['forms'])){
		    $enable_form = \appxq\sdii\utils\SDUtility::array2String($_POST['forms']);
		}
		$model->enable_form = $enable_form;
		
		//----------------------------------
	    }else {
			$model->gid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
	     }
	    
	    if ($model->save()) {
		
		unset(Yii::$app->session['create_module']);
		
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		    'options'=>['class'=>'alert-success']
		]);
		
		if($action){
		    return $this->redirect(['/inv/inv-gen/index']);
		} else {
		    return $this->redirect(['/inv/inv-person/create-modules', 'module'=>$model->gid]);
		}
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		    'options'=>['class'=>'alert-danger']
		]);
	    }
	}
	
	return $this->render('create-modules', [
	    'model' => $model,
	    'module' => $module,
	    'ezf_id' => $ezf_id,
	    'dataFields' => $dataFields,
	    'modelEzform' => $modelEzform,
	    'dataSubForm' => $dataSubForm,
	    'dataSelect' => $dataSelect,
	]);
    }
    
    public function actionGetfields(){
	if (Yii::$app->getRequest()->isAjax) {
	    $id = isset($_POST['id'])?$_POST['id']:0;
	    
	    $dataFields = InvQuery::getFields($id);
	    
	    $html = '<option value="">-----เลือกฟิลด์-----</option>';
	    foreach ($dataFields as $key => $value) {
		$name = $value['name'];
		
		if($value['ezf_field_type']==21){
		    if($value['ezf_field_label']==1){
			$name = 'จังหวัด';
		    } elseif ($value['ezf_field_label']==2) {
			$name = 'อำเภอ';
		    } elseif ($value['ezf_field_label']==3) {
			$name = 'ตำบล';
		    }
		} 
		
		$html .= '<option value="'.$value['id'].'">'.$name.'</option>';
	    }
	    
	    $html .= '<option value="rstat">rstat</option>';
	    
	    return $html;
	}
    }
    
    public function actionCheckfields(){
	if (Yii::$app->getRequest()->isAjax) {
	    $fname = isset($_POST['fname'])?$_POST['fname']:'';
	    $formid = isset($_POST['formid'])?$_POST['formid']:0;
	    //\appxq\sdii\utils\VarDumper::dump($fname.' - '.$formid);
	    
	    $dataFields = InvQuery::getFieldsEzf($formid,$fname);
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    //\appxq\sdii\utils\VarDumper::dump($dataFields);
	    $type = (in_array($dataFields['ezf_field_type'], [7, 9]))?1:0;
	    
	    $result = [
		'status' => 'success',
		'message' => Yii::t('app', 'Load completed.'),
		'type' => $type,
	    ];
	    return $result;
	}
    }
    
    public function actionGetDetail(){
	if (Yii::$app->getRequest()->isAjax) {
	    $id = isset($_POST['id'])?$_POST['id']:0;
	    
	    $dataFields = InvQuery::getEzformById($id);
	    $str = 'ไม่ระบุ';
	    if($dataFields){
		$str = $dataFields['field_detail'];
	    }
	    return "<code>$str</code>";
	}
    }
    
    public function actionGetType() {
	if (Yii::$app->getRequest()->isAjax) {
	    $id = isset($_POST['id'])?$_POST['id']:'';
	    $ezf_id = isset($_POST['ezf_id'])?$_POST['ezf_id']:0;
	   
            $pos = strpos($id, 'fxmain_');
		if ($pos === false) {
                    
                } else {
                    $id = str_replace('fxmain_', '', $id);
                    $ezf_id = Yii::$app->session['sql_main_fields']['main_ezf_id'];
                }
            
            //\appxq\sdii\utils\VarDumper::dump($ezf_id);
            
	    Yii::$app->response->format = Response::FORMAT_JSON;
	   
	    $modelField = \backend\modules\ezforms\models\EzformFields::find()->where('ezf_id=:ezf_id AND ezf_field_name=:id',[':ezf_id'=>$ezf_id, ':id'=>$id])->one();
	    
	    $type = 0;
	    $id = 0;
	    $orglabel = 0;
	    if($modelField){
		$type = $modelField['ezf_field_type'];
		$id = $modelField['ezf_field_id'];
		$orglabel = $modelField['ezf_field_label'];
	    }
	    
	    $result = [
		'status' => 'success',
		'message' => Yii::t('app', 'Load completed.'),
		'type' => $type,
		'id' => $id,
		'orglabel'=>$orglabel,
	    ];
	    return $result;
	}
    }
    
    public function actionGetFormType() {
	if (Yii::$app->getRequest()->isAjax) {
	    
	    $ezf_id = isset($_POST['ezf_id'])?$_POST['ezf_id']:0;
	    
	    Yii::$app->response->format = Response::FORMAT_JSON;
	   
	    $ezform = InvQuery::getEzformById($ezf_id);
	    
	    $fd = 0;
	    $en = 0;
	    $et = 0;
	    $ct = 0;
	    $ur = 0;
	    if($ezform){
		$fd = isset($ezform['field_detail'])?$ezform['field_detail']:'';
		$en = $ezform['ezf_name'];
		$et = $ezform['ezf_table'];
		$ct = $ezform['comp_id_target'];
		$ur = isset($ezform['unique_record'])?$ezform['unique_record']:1;
	    }
	    
	    $result = [
		'status' => 'success',
		'message' => Yii::t('app', 'Load completed.'),
		'fd' => $fd,
		'en' => $en,
		'et'=>$et,
		'ct'=>$ct,
		'ur'=>$ur,
	    ];
	   
	    return $result;
	}
    }
    
    public function actionGetWidget($view) {
	if (Yii::$app->getRequest()->isAjax) {
	    $dataFields = $_POST['data'];
	    $dataSubForm = $_POST['data_sub'];
	    $dataSelect = $_POST['data_select'];
	    
	    $html = $this->renderAjax($view, ['id'=>$_POST['id'], 'dataFields'=>$dataFields, 'dataSubForm' => $dataSubForm, 'dataSelect' => $dataSelect,]);
	    
	    return $html;
	}
    }
    
    public function actionSavelist() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;

	    if(isset($_POST['selection'])){
		foreach ($_POST['selection'] as $key => $value) {
		    
		    $ceck = InvSubList::find()->where('sub_id=:sub_id AND person_id=:id', [':sub_id'=>$_POST['sub'], ':id'=>$value])->count();
		    if($ceck==0){
			$model = new InvSubList();
			$model->sub_id = $_POST['sub'];
			$model->filter_id = $_POST['id'];
			$model->person_id = $value;
			$model->save();
		    }
		}
	    }
	    
	    $result = [
		'status' => 'success',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Add list completed.'),
	    ];
	    return $result;
	}
    }
    
    public function actionAddSelectlist() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $ovfilter_sub = isset($_GET['ovfilter_sub'])?$_GET['ovfilter_sub']:0;
	    $comp = isset($_GET['comp'])?$_GET['comp']:0;
	    $module = isset($_GET['module'])?$_GET['module']:0;
	    
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    if (isset($_POST['selection'])) {
		$dataOvFilterSub = InvQuery::getFilterSubNotSafe($ovfilter_sub, $sitecode, $comp, $module);
		
		$return = $this->renderAjax('_sub_selectlist', [
		    'model' => $dataOvFilterSub,
		    'selection'=>$_POST['selection'],
		]);
		
		$result = [
		    'status' => 'success',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Load completed.'),
		    'html' => $return,
		];
		return $result;
		
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not add the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionAddlist() {
	if (Yii::$app->getRequest()->isAjax) {
	    ini_set('max_execution_time', 0);
	    set_time_limit(0);
	    ini_set('memory_limit','256M');
	    
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $ovfilter_sub = isset($_GET['ovfilter_sub'])?$_GET['ovfilter_sub']:0;
	    $comp = isset($_GET['comp'])?$_GET['comp']:0;
	    $module = isset($_GET['module'])?$_GET['module']:0;
	    $select = isset($_POST['select'])?$_POST['select']:0;
	    
	    $dataOvFilterSub = InvFunc::getList();
	    
	    
	    $modelFields = Yii::$app->session['sql_main_fields'];
	    
	    $field_name = Yii::$app->session['sql_main_fields']['field_name'];
	    $field_nameArry = explode(',', $field_name);
	    $concat = '';
	    $comma = '';
	    foreach ($field_nameArry as $valueF) {
                $pos = strpos($valueF, 'fxmain_');
		if ($pos === false) {
                   $concat .= "$comma `{$modelFields['ezf_table']}`.`$valueF`";
		} else {
                    $origin = str_replace('fxmain_', '', $valueF);
                    $concat .= "$comma `{$modelFields['main_ezf_table']}`.`$origin`";
                }
		
		$comma = ", ' ',";
	    }
	    
	    $tbdata = new Tbdata();
	    $tbdata->setTableName($modelFields['ezf_table']);
	    $query = $tbdata->find();
	    
	    $query->where("{$modelFields['ezf_table']}.rstat<>3");

	    if($modelFields['special']==1){
		$query->andWhere("{$modelFields['ezf_table']}.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
	    } else {
                $query->andWhere("{$modelFields['ezf_table']}.xsourcex = :sitecode", [':sitecode'=>$sitecode]);
            }
            
            if($modelFields['ezf_id']!=$modelFields['main_ezf_id']){
                $pk = $modelFields['pk_field'];
                $pkJoin = 'target';
                if($modelFields['special']==1){
                    $pk = 'ptid';
                    $pkJoin = 'ptid';
                }
                $query->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
                $query->andWhere("`{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)");
                $query->groupBy("{$modelFields['ezf_table']}.id");
            }
	    
	    if($select!=''){
		$ezform = \backend\modules\inv\classes\InvQuery::getEzformById($select);
		
		if($modelFields['special']==1){
		    $query->innerJoin($ezform['ezf_table'], "{$ezform['ezf_table']}.ptid = {$modelFields['ezf_table']}.ptid");
		} else {
		    $query->innerJoin($ezform['ezf_table'], "{$ezform['ezf_table']}.target = {$modelFields['ezf_table']}.{$modelFields['pk_field']}");
		}
		
		$query->andWhere("{$ezform['ezf_table']}.rstat <> 3");
		$query->groupBy("{$modelFields['ezf_table']}.id");
		
//		if(isset($ezform['field_detail']) && !empty($ezform['field_detail'])){
//		    $pk = $modelFields['pk_field'];
//		    $pkJoin = 'target';
//		    if($modelFields['special']==1){
//			$pk = 'ptid';
//			$pkJoin = 'ptid';
//		    }
//		    $sqlx = "(SELECT concat({$ezform['field_detail']}) FROM {$ezform['ezf_table']} WHERE $pkJoin={$modelFields['ezf_table']}.$pk LIMIT 0,1)";
//		    $concat .= ", ' ', $sqlx";
//		}
	    }
	    
	    $query->select(["{$modelFields['ezf_table']}.id", "CONCAT($concat) AS `name`"]);
	    
	    $query2 = (new \yii\db\Query())
	    ->select(["{$modelFields['ezf_table']}.id", "CONCAT($concat) AS `name`"])
	    ->from($modelFields['ezf_table'])
	    ->innerJoin('inv_sub_list', "inv_sub_list.person_id = {$modelFields['ezf_table']}.id")
	    ->andWhere("inv_sub_list.sub_id = :sub_id", [':sub_id'=>$ovfilter_sub]);
	    
            if($modelFields['ezf_id']!=$modelFields['main_ezf_id']){
                $pk = $modelFields['pk_field'];
                $pkJoin = 'target';
                if($modelFields['special']==1){
                    $pk = 'ptid';
                    $pkJoin = 'ptid';
                }
                $query2->innerJoin($modelFields['main_ezf_table'], "`{$modelFields['main_ezf_table']}`.`$pkJoin` = `{$modelFields['ezf_table']}`.`$pk`");
                $query2->andWhere("`{$modelFields['main_ezf_table']}`.rstat NOT IN(0,3)");
                $query2->groupBy("{$modelFields['ezf_table']}.id");
            }
            
	    //$query->union($query2);
	    
	    $data = $query;
	    
	    $model = new \backend\modules\ovcca\models\ListForm();
	    $selecList = InvQuery::getSeclectList($ovfilter_sub);
	    
	    $model->list_person = $selecList?\yii\helpers\Json::encode($selecList):'';
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->validate()) {
		    $saveList = \yii\helpers\Json::decode($model->list_person);
		    Yii::$app->db->createCommand()
			    ->delete('inv_sub_list', 'inv_sub_list.sub_id=:sub', [':sub'=>$ovfilter_sub])
			    ->execute();
		    if(is_array($saveList)){
			foreach ($saveList as $value) {
			    $ov_sub_list = new \backend\modules\inv\models\InvSubList();
			    $ov_sub_list->sub_id = $ovfilter_sub;
			    $ov_sub_list->person_id = $value;
			    $ov_sub_list->filter_id = $comp;
			    $ov_sub_list->save();
			    
			}
		    }
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'html' => $return,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		    ];
		    return $result;
		}
	    } else {
		$return = $this->renderAjax('_sublist', [
		    'model' => $model,
		    'data' => $data,
		    'ovfilter_sub' => $ovfilter_sub,
		    'comp' => $comp,
		    'select' => $select,
		    'dataOvFilterSub' =>$dataOvFilterSub,
		]);
		$result = [
		    'status' => 'success',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Load completed.'),
		    'html' => $return,
		];
		return $result;
	    }
	    
	    

	    
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionReportOvcca() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $comp = isset($_GET['comp'])?$_GET['comp']:0;
	    $sid = isset($_GET['sid'])?$_GET['sid']:0;
	    
	    
	    $selection = null;
	    if (isset($_POST['selection'])) {
		$selection = implode(',', $_POST['selection']);
		Yii::$app->session['selection'] = $selection;
	    } else {
		unset(Yii::$app->session['selection']);
	    }
	    
	    $url = \yii\helpers\Url::to(['report', 'comp'=>$comp, 'ovfilter_sub'=>$sid]);
	    
	    $html = \backend\modules\ovcca\classes\OvccaFunc::getIframe($url);
	    $result = [
		'status' => 'success',
		'action' => 'report',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionReportExel() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $comp = isset($_GET['comp'])?$_GET['comp']:0;
	    $sid = isset($_GET['sid'])?$_GET['sid']:0;
	    
	    $selection = null;
	    $post = null;
	    if (isset($_POST['selection'])) {
		$selection = implode(',', $_POST['selection']);
		Yii::$app->session['selection'] = $selection;
	    } else {
		unset(Yii::$app->session['selection']);
	    }
	    

	    $url = \yii\helpers\Url::to(['report-export', 'comp'=>$comp, 'ovfilter_sub'=>$sid]);
	    
	    $html = \backend\modules\ovcca\classes\OvccaFunc::getIframe($url);
	    
	    $result = [
		'status' => 'success',
		'action' => 'report',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionReport($comp, $ovfilter_sub, $selection=null)
    {
	if(isset(Yii::$app->session['selection'])){
	    $selection = Yii::$app->session['selection'];
	}
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M');        
	// create new PDF document
	//PDF_UNIT = pt , mm , cm , in 
	//PDF_PAGE_ORIENTATION = P , LANDSCAPE = L
	//PDF_PAGE_FORMAT 4A0,2A0,A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,C0,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,RA0,RA1,RA2,RA3,RA4,SRA0,SRA1,SRA2,SRA3,SRA4,LETTER,LEGAL,EXECUTIVE,FOLIO
	$orient = 'L';

	$pdf = new SDPDF($orient, PDF_UNIT, 'A4', true, 'UTF-8', false);
	//spl_autoload_register(array('YiiBase', 'autoload'));

	// set document information
	$pdf->SetCreator('AppXQ');
	$pdf->SetAuthor('iencoded@gmail.com');
	$pdf->SetTitle('Report');
	$pdf->SetSubject('Original');
	$pdf->SetKeywords('AppXQ, SDII, PDF, report, medical, clinic');

	// remove default header/footer
	$pdf->setPrintHeader(TRUE);
	$pdf->setPrintFooter(TRUE);

	// set margins
	$pdf->SetMargins(10, 17, 10);
	$pdf->SetHeaderMargin(10);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 15);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set font
	$pdf->SetFont($pdf->fontName, '', 10);

	/* ------------------------------------------------------------------------------------------------------------------ */
	$header = [
	    [
		'name'=>'orderID',
		'txt'=>'ลำดับ',
		'w'=>13,
		'align'=>'C'
	    ]
	];
	
	if(isset(Yii::$app->session['sql_main_fields']['enable_field'])){
	    $main_fields = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_field']);
	    $fixFields = isset($main_fields['field'])?$main_fields['field']:[];
	    $fixLabel = isset($main_fields['label'])?$main_fields['label']:[];
	    $fixAlign = isset($main_fields['align'])?$main_fields['align']:[];
	    $fixWidth = isset($main_fields['rwidth'])?$main_fields['rwidth']:[];
	    $fixReport = isset($main_fields['report'])?$main_fields['report']:[];
	    
	    foreach ($fixFields as $keyFix => $valueFix) {
		if($fixReport[$keyFix]=='Y'){
		    $header[] = [
			'name'=>$valueFix,
			'txt'=>$fixLabel[$keyFix],
			'w'=>$fixWidth[$keyFix],
			'align'=> InvFunc::getAlign($fixAlign[$keyFix]),
		    ];
		}
	    }
	}
	
	$fields_col = Yii::$app->session['sql_fields'];
	if($fields_col){
	    foreach ($fields_col as $keyField => $valueField) {
		if($valueField['report_show']=='Y'){
		    $header[] = [
			'name'=>'notset',//$valueField['sql_id_name'],
			'txt'=>$valueField['header'],
			'w'=>$valueField['report_width'],
			'align'=>'L',
		    ];
		}
	    }
	} else {
	    
	    if(isset(Yii::$app->session['sql_main_fields']['enable_form'])){
		$main_forms = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_form']);
		$ffixFields = isset($main_forms['form'])?$main_forms['form']:[];
		$ffixLabel = isset($main_forms['label'])?$main_forms['label']:[];
		$ffixResult = isset($main_forms['result'])?$main_forms['result']:[];
		$ffixWidth = isset($main_forms['rwidth'])?$main_forms['rwidth']:[];
		$ffixReport = isset($main_forms['report'])?$main_forms['report']:[];
		
		foreach ($ffixFields as $keyForm => $valueForm) {
		    if($ffixReport[$keyForm]=='Y'){
			$header[] = [
			    'name'=>'notset',//'id_'.$valueForm,
			    'txt'=>$ffixLabel[$keyForm],
			    'w'=>$ffixWidth[$keyForm],
			    'align'=> 'L',
			];
		    }
		}
	    }
	}
	
	$header[] = [
	    'name'=>'notset',
	    'txt'=>'หมายเหตุ',
	    'w'=>0,
	    'align'=>'C'
	];
	    
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	
	$pdf->contentHeader=[
	    'title'=>'ใบกำกับงาน',
	    
	];
	$pdf->tableHeader=$header;
	$pdf->lineFooter=true;

	// add a page
	$pdf->AddPage();
	
	$data = InvQuery::getTbData($sitecode, $comp, $ovfilter_sub, $selection);
	
	//getOVDataSetect
	foreach ($data as $key => $value) {
	   
	    if ($value['id']!=null) {
		$fields_col = Yii::$app->session['sql_fields'];
		if($fields_col){
		    foreach ($fields_col as $keyField => $valueField) {
			$result = trim($valueField['result_field']);

			if($result!=''){
			    $field = $valueField;
			    if ($value[$field['sql_id_name']]!=null) {
				$arrStr = explode(',', $value[$field['sql_id_name']]);
				$arrResult = explode(',', $value[$field['sql_result_name']]);
				$rowReturn = '';
				$comma = '';
				foreach ($arrStr as $i=>$valueID) {
				    
				    $options = appxq\sdii\utils\SDUtility::string2Array($field['value_options']);
				    $convert = '';
				    foreach ($options['value'] as $keyOp => $valueOp) {
					if($arrResult[$i]==$valueOp){
					    $convert = $options['convert'][$keyOp];
					}
				    }
				    $rowReturn .= $comma.$convert;
				    $comma = ', ';
				}
				$data[$key][$field['sql_id_name']] = $rowReturn;
			    }
			} else {
			    $field = $valueField;

			    if ($value[$field['sql_id_name']]!=null) {
				$arrStr = explode(',', $value[$field['sql_id_name']]);
				$rowReturn = '';
				$comma = '';
				foreach ($arrStr as $valueID) {
				    $rowReturn .= $comma.'/';
				    $comma = ', ';
				}
				$data[$key][$field['sql_id_name']] = $rowReturn;
			    }
			}
		    }
		} else {

		    if(isset(Yii::$app->session['sql_main_fields']['enable_form'])){
			$main_forms = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_form']);
			$ffixFields = isset($main_forms['form'])?$main_forms['form']:[];
			$ffixLabel = isset($main_forms['label'])?$main_forms['label']:[];
			$ffixResult = isset($main_forms['result'])?$main_forms['result']:[];
			$ffixWidth = isset($main_forms['rwidth'])?$main_forms['rwidth']:[];
			$ffixCondition = isset($main_forms['condition'])?$main_forms['condition']:[];

			foreach ($ffixFields as $keyForm => $valueForm) {
			    $result = trim($ffixResult[$keyForm]);
			    
			    if($result!=''){
				$sql_id_name = 'id_'.$valueForm;
				$sql_result_name = 'result_'.$valueForm;
				
				if ($value[$sql_id_name]!=null) {
				    $arrStr = explode(',', $value[$sql_id_name]);
				    $arrResult = explode(',', $value[$sql_result_name]);
				    $rowReturn = '';
				    $comma = '';
				    foreach ($arrStr as $i=>$valueID) {
					
					$options = $ffixCondition[$valueForm];
					$convert = '';
					foreach ($options['value'] as $keyOp => $valueOp) {
					    if($arrResult[$i]==$valueOp){
						$convert = $options['convert'][$keyOp];
					    }
					}
					$rowReturn .= $comma.$convert;
					$comma = ', ';
				    }
				    $data[$key][$sql_id_name] = $rowReturn;
				}
			    } else {
				$sql_id_name = 'id_'.$valueForm;
				$sql_result_name = 'result_'.$valueForm;

				if ($value[$sql_id_name]!=null) {
				    $arrStr = explode(',', $value[$sql_id_name]);
				    $rowReturn = '';
				    $comma = '';
				    foreach ($arrStr as $valueID) {
					$rowReturn .= $comma.'/';
					$comma = ', ';
				    }
				    $data[$key][$sql_id_name] = $rowReturn;
				}
			    }
			}
		    }
		}
	    }
	}
	
	$pdf->createTableData($header, $data, false);
	
	$pdf->Output('report.pdf', 'I');
	Yii::$app->end();
    }
    
    public function actionReportExport($comp, $ovfilter_sub, $selection=null)//
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M'); 
	
	if(isset(Yii::$app->session['selection'])){
	    $selection = Yii::$app->session['selection'];
	}
	
	$objPHPExcel = new \common\lib\phpexcel\SDExcelView();
	
	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Damasac")
				    ->setLastModifiedBy("Damasac")
				    ->setTitle("Export Document")
				    ->setSubject("Export Document")
				    ->setDescription("Export ovcca")
				    ->setKeywords("ovcca")
				    ->setCategory("ovcca");
	// Add some data
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$data = InvQuery::getTbData($sitecode, $comp, $ovfilter_sub, $selection);
	
	$dataColumn = [];
	
	if(isset(Yii::$app->session['sql_main_fields']['enable_field'])){
	    $main_fields = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_field']);
	    $fixFields = isset($main_fields['field'])?$main_fields['field']:[];
	    $fixLabel = isset($main_fields['label'])?$main_fields['label']:[];
	    $fixAlign = isset($main_fields['align'])?$main_fields['align']:[];
	    $fixWidth = isset($main_fields['rwidth'])?$main_fields['rwidth']:[];
	    $fixReport = isset($main_fields['report'])?$main_fields['report']:[];
	    
	    foreach ($fixFields as $keyFix => $valueFix) {
		if($fixReport[$keyFix]=='Y'){
		    $dataColumn[$valueFix] = $fixLabel[$keyFix];
		}
		
	    }
	}
	
	$fields_col = Yii::$app->session['sql_fields'];
	if($fields_col){
	    foreach ($fields_col as $keyField => $valueField) {
		if($valueField['report_show']=='Y'){
		    $dataColumn[$valueField['sql_id_name']] = $valueField['header'];
		}
	    }
	} else {
	    
	    if(isset(Yii::$app->session['sql_main_fields']['enable_form'])){
		$main_forms = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_form']);
		$ffixFields = isset($main_forms['form'])?$main_forms['form']:[];
		$ffixLabel = isset($main_forms['label'])?$main_forms['label']:[];
		$ffixResult = isset($main_forms['result'])?$main_forms['result']:[];
		$ffixWidth = isset($main_forms['rwidth'])?$main_forms['rwidth']:[];
		$ffixReport = isset($main_forms['report'])?$main_forms['report']:[];
		
		foreach ($ffixFields as $keyForm => $valueForm) {
		    if($ffixReport[$keyForm]=='Y'){
			$dataColumn['id_'.$valueForm] = $ffixLabel[$keyForm];
		    }
		}
	    }
	}
	
	$dataColumn['commtnotset']='หมายเหตุ';
	
	$column = 'A';
	$row=1;
	foreach ($dataColumn as $key => $value) {
	    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($column . $row, $value);
	    $column++;
	}
 
	$row=2;
	//getOVDataSetect
	foreach ($data as $key => $value) {
	    
	    if ($value['id']!=null) {
		$fields_col = Yii::$app->session['sql_fields'];
		if($fields_col){
		    foreach ($fields_col as $keyField => $valueField) {
			$result = trim($valueField['result_field']);

			if($result!=''){
			    $field = $valueField;
			    if ($value[$field['sql_id_name']]!=null) {
				$arrStr = explode(',', $value[$field['sql_id_name']]);
				$arrResult = explode(',', $value[$field['sql_result_name']]);
				$rowReturn = '';
				$comma = '';
				foreach ($arrStr as $i=>$valueID) {
				    
				    $options = appxq\sdii\utils\SDUtility::string2Array($field['value_options']);
				    $convert = '';
				    foreach ($options['value'] as $keyOp => $valueOp) {
					if($arrResult[$i]==$valueOp){
					    $convert = $options['convert'][$keyOp];
					}
				    }
				    $rowReturn .= $comma.$convert;
				    $comma = ', ';
				}
				$value[$field['sql_id_name']] = $rowReturn;
			    }
			} else {
			    $field = $valueField;

			    if ($value[$field['sql_id_name']]!=null) {
				$arrStr = explode(',', $value[$field['sql_id_name']]);
				$rowReturn = '';
				$comma = '';
				foreach ($arrStr as $valueID) {
				    $rowReturn .= $comma.'/';
				    $comma = ', ';
				}
				$value[$field['sql_id_name']] = $rowReturn;
			    }
			}
		    }
		} else {

		    if(isset(Yii::$app->session['sql_main_fields']['enable_form'])){
			$main_forms = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_form']);
			$ffixFields = isset($main_forms['form'])?$main_forms['form']:[];
			$ffixLabel = isset($main_forms['label'])?$main_forms['label']:[];
			$ffixResult = isset($main_forms['result'])?$main_forms['result']:[];
			$ffixWidth = isset($main_forms['rwidth'])?$main_forms['rwidth']:[];
			$ffixCondition = isset($main_forms['condition'])?$main_forms['condition']:[];

			foreach ($ffixFields as $keyForm => $valueForm) {
			    $result = trim($ffixResult[$keyForm]);
			    
			    if($result!=''){
				$sql_id_name = 'id_'.$valueForm;
				$sql_result_name = 'result_'.$valueForm;
				
				if ($value[$sql_id_name]!=null) {
				    $arrStr = explode(',', $value[$sql_id_name]);
				    $arrResult = explode(',', $value[$sql_result_name]);
				    $rowReturn = '';
				    $comma = '';
				    foreach ($arrStr as $i=>$valueID) {
					
					$options = $ffixCondition[$valueForm];
					$convert = '';
					foreach ($options['value'] as $keyOp => $valueOp) {
					    if($arrResult[$i]==$valueOp){
						$convert = $options['convert'][$keyOp];
					    }
					}
					$rowReturn .= $comma.$convert;
					$comma = ', ';
				    }
				    $value[$sql_id_name] = $rowReturn;
				}
			    } else {
				$sql_id_name = 'id_'.$valueForm;
				$sql_result_name = 'result_'.$valueForm;

				if ($value[$sql_id_name]!=null) {
				    $arrStr = explode(',', $value[$sql_id_name]);
				    $rowReturn = '';
				    $comma = '';
				    foreach ($arrStr as $valueID) {
					$rowReturn .= $comma.'/';
					$comma = ', ';
				    }
				    $value[$sql_id_name] = $rowReturn;
				}
			    }
			}
		    }
		}
	    }
	    
	    $column = 'A';
	    foreach ($dataColumn as $keyCol => $valueCol) {
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($column . $row, $value[$keyCol]);
		$column++;
	    }

	    $row++;
	}
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('export');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="export.xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
    }
    
    public function actionInfoApp($gid)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $userId = Yii::$app->user->id;
	    $model = InvQuery::getModule($gid, $userId);
            
	    $sql="select count(*) AS user,count(distinct p.sitecode)  AS org from user_profile p inner join inv_favorite i on p.user_id=i.user_id where gid=:gid";
            $ncount = Yii::$app->db->createCommand($sql, [':gid'=>$gid])->queryOne();
            $maintb = InvGen::find()->where('gid=:gid', [':gid'=>$gid])->one();
            
            $pcoc = NULL;
            if(isset($maintb['main_ezf_table']) && $maintb['main_ezf_table']!=''){
                $sql="select count(*) AS ntime,count(distinct ifnull(ptid,id)) AS npatient from {$maintb['main_ezf_table']} where rstat<>3;";
                $pcoc = Yii::$app->db->createCommand($sql)->queryOne();
            }


        //nut
         $sqli="SELECT 
	        (select count(vote) from inv_comment as i5 where vote =5) as v5,
	        (select count(vote) from inv_comment as i4 where vote =4) as v4,
	        (select count(vote) from inv_comment as i3 where vote =3) as v3,
	        (select count(vote) from inv_comment as i2 where vote =2) as v2,
	        (select count(vote) from inv_comment as i1 where vote =1) as v1
	        FROM inv_comment limit 1";

        $vote = Yii::$app->db->createCommand($sqli)->queryAll();
        $count = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
               
            
	    return $this->renderAjax('_info-app', [
		'model' => $model,
                'ncount' => $ncount,
                'pcoc' => $pcoc,  
                'vote'=>$vote,
                'count'=>$count
                
	    ]);
	}
    }
    
    public function actionFavorite($gid)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $userId = Yii::$app->user->id;
	    $model = InvFavorite::find()->where('gid=:gid AND user_id=:user_id', [':user_id'=>$userId, ':gid'=>$gid])->one();
	    
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    $active = 0;
	    if($model){
		$model->delete();
		$active = 0;
	    } else {
		$model = new InvFavorite();
		$model->gid = $gid;
		$model->user_id = $userId;
		$model->save();
		
		$active = 1;
	    }
	    
	    $result = [
		'status' => 'success',
		'action' => 'update',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		'active' => $active,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionEditUser()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $userId = Yii::$app->user->id;
            $id = isset($_GET['id'])?$_GET['id']:0;
	    
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($_POST['user']=='' || $_POST['pw']==''){
                $result = [
                        'status' => 'error',
                        'message' => SDHtml::getMsgError() . Yii::t('app', 'Username/Password จะต้องไม่เป็นค่าว่าง'),
                    ];
                    return $result;
            }
            
            $invuser = \backend\modules\inv\models\InvUser::find()->where('id=:id', [':id'=>$id])->one();
            if($invuser){
                $invuser->username = $_POST['user'];
                $invuser->password = Yii::$app->getSecurity()->generatePasswordHash($_POST['pw']);
                if($invuser->save()){
                    $result = [
                            'status' => 'success',
                            'action' => 'create',
                            'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'แก้ไขข้อมูลผู้ใช้สำเร็จ'),
                        ];
                        return $result;
                } else {
                    $result = [
                        'status' => 'error',
                        'message' => SDHtml::getMsgError() . Yii::t('app', 'แก้ไขข้อมูลไม่ได้'),
                    ];
                    return $result;
                }   
            } else {
                $result = [
                    'status' => 'error',
                    'message' => SDHtml::getMsgError() . Yii::t('app', 'โหลดข้อมูลไม่ได้'),
                ];
                return $result;
            }    
	}
    }
    
    public function actionDelUser()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $userId = Yii::$app->user->id;
            $id = isset($_GET['id'])?$_GET['id']:0;
	    
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            $invuser = \backend\modules\inv\models\InvUser::find()->where('id=:id', [':id'=>$id])->one();
            if($invuser){
                if($invuser->delete()){
                    $result = [
                            'status' => 'success',
                            'action' => 'create',
                            'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'ลบข้อมูลผู้ใช้สำเร็จ'),
                        ];
                        return $result;
                } else {
                    $result = [
                        'status' => 'error',
                        'message' => SDHtml::getMsgError() . Yii::t('app', 'ลบข้อมูลไม่ได้.'),
                    ];
                    return $result;
                }   
            } else {
                $result = [
                    'status' => 'error',
                    'message' => SDHtml::getMsgError() . Yii::t('app', 'โหลดข้อมูลไม่ได้.'),
                ];
                return $result;
            }    
	}
    }
    
    public function actionGenUser()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $userId = Yii::$app->user->id;
            $gid = isset($_GET['gid'])?$_GET['gid']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid'])?$_GET['dataid']:NULL;
	    $target = isset($_GET['target'])?$_GET['target']:NULL;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:NULL;
            $table = isset($_GET['table'])?$_GET['table']:NULL;
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            $sql = "SELECT * FROM $table WHERE id=:id";
            $data = Yii::$app->db->createCommand($sql, [':id'=>$dataid])->queryOne();
            
            //check
            if($data){
                $sqlUser = "SELECT user_profile.*, user.* FROM user_profile INNER JOIN user ON user_profile.user_id = user.id WHERE cid=:cid";
                $dataUser = Yii::$app->db->createCommand($sqlUser, [':cid'=>$data['cid']])->queryOne();
                if($dataUser){
                    $invuser = \backend\modules\inv\models\InvUser::find()->where('gid=:gid AND user_id=:user_id', [':user_id'=>$dataUser['user_id'], ':gid'=>$gid])->one();
                    
                    //\appxq\sdii\utils\VarDumper::dump($invuser,1,0);
                    if($invuser){
                        $result = [
                            'status' => 'success',
                            'action' => 'create',
                            'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'มีผู้ใช้นี้อยู่ในระบบแล้ว'),
                        ];
                        return $result;
                    } else {
                        $model = new \backend\modules\inv\models\InvUser();
                        $model->id = $data['cid'];
                        $model->sitecode = $dataUser['sitecode'];
                        $model->gid = $gid;
                        $model->user_id = $dataUser['user_id'];
                        $model->username = $dataUser['username'];
                        $model->password = $dataUser['password_hash'];
                        $model->save();
                        
                        $result = [
                            'status' => 'success',
                            'action' => 'create',
                            'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'เพิ่มผู้ใช้สำเร็จๅ'),
                        ];
                        return $result;
                    }
                } else {
                    // registor 
                    $sqlCheck = "SELECT
                        `user`.`id`
                        FROM
                        `user`
                        WHERE
                        `user`.`username` =:cid";
                    $dataUserCheck = Yii::$app->db->createCommand($sqlCheck, [':cid'=>$data['cid']])->queryOne();
                    if($dataUserCheck){
                        Yii::$app->db->createCommand()->delete('user', 'id=:id', [':id'=>$dataUserCheck['id']])->execute();
                    }
                    
                    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
                    
                    $signup = new \frontend\modules\user\models\SignupForm();
                    //$signup->id = GenMillisecTime::getMillisecTime();
                    $signup->username = $data['cid'];
                    $signup->password = $data['cid'];
                    $signup->email = $data['cid'].'@gmail.com';
                    
                    $mprofile = new \common\models\UserProfile();
                    $mprofile->locale = Yii::$app->language;
                    $mprofile->firstname = $data['name'];
                    $mprofile->lastname = $data['surname'];
                    $mprofile->email = $signup->email;
                    $mprofile->gender = $data['v3'];
                    $mprofile->nation = 0;
                    $mprofile->cid = $data['cid'];
                    $mprofile->telephone = $data['mobile'];
                    
                    $mprofile->status = 0;
//                    $mprofile->status_personal = $data["status_personal"];
//                    $mprofile->status_manager = $data["status_manager"];
//                    $mprofile->status_other = $data["status_other"];

                    $mprofile->department = $sitecode;
//                    $mprofile->department_nation_text = $data['department_nation_text'];
//                    $mprofile->department_area = $data['department_area'];
//                    $mprofile->department_area_text = $data["department_area_text"];
//                    $mprofile->department_province = $data["department_province"];
//                    $mprofile->department_province_text = $data["department_province_text"];
//                    $mprofile->department_amphur = $data["department_amphur"];
//                    $mprofile->department_amphur_text = $data["department_amphur_text"];

                    $mprofile->sitecode = $sitecode;

                    //$mprofile->citizenid_file = $data["citizenid_file"];
                    //$mprofile->secret_file = $data["secret_file"];
                    $title = null;
                    if($data['title']=='นาย'){
                        $title = 0;
                    } elseif($data['title']=='นาง'){
                        $title = 1;
                    } elseif($data['title']=='นางสาว' || $data['title']=='น.ส.'){
                        $title = 2;
                    }
                    $mprofile->inout = 0;
                    $mprofile->title = $title;
                    $mprofile->enroll_key = 'GenModule';
                    $mprofile->volunteer = 1;
                    $mprofile->volunteer_status = 2;
                    $mprofile->citizenid_file = '';
                    $mprofile->secret_file = '';
                    
                    $userNew = $signup->signup($mprofile);
                    if($userNew){
                        $model = new \backend\modules\inv\models\InvUser();
                        $model->id = $data['cid'];
                        $model->sitecode = $mprofile->sitecode;
                        $model->gid = $gid;
                        $model->user_id = $userNew->id;
                        $model->username = $signup->username;
                        $model->password = Yii::$app->getSecurity()->generatePasswordHash($signup->password);
                        $model->save();
                        
                        $result = [
                            'status' => 'success',
                            'action' => 'create',
                            'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'เพิ่มผู้ใช้สำเร็จ'),
                        ];
                        return $result;
                    } else {
                        $result = [
                            'status' => 'error',
                            'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
                        ];
                        return $result;
                    }
                }
            } else {
                $result = [
                    'status' => 'error',
                    'message' => SDHtml::getMsgError() . Yii::t('app', 'โหลดข้อมูลไม่ได้.'),
                ];
                return $result;
            }
            
            
            
	}
    }
    
    public function actionEzformPrint() {
	    $readonly = isset($_GET['readonly'])?$_GET['readonly']:0;
	    $end = isset($_GET['end'])?$_GET['end']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $main_ezf_id = isset($_GET['main_ezf_id'])?$_GET['main_ezf_id']:0;
	    $dataid = isset($_GET['dataid'])?$_GET['dataid']:NULL;
	    $target = isset($_GET['target'])?$_GET['target']:NULL;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:NULL;
	    
	    try {
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:$modelform->comp_id_target;
		
		if(isset($ezf_id) && isset($comp_id_target) && isset($target) && (!isset($dataid) || $dataid=='' || $dataid=='null')) {
		    $dataid = InvFunc::insertRecord([
			'ezf_id'=>$ezf_id,
			'target'=>$target,
			'comp_id_target'=>$comp_id_target,
		    ]);
		    
		}
		
		Yii::$app->session['ezform_main'] = $ezf_id;
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();
		
		
		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
		
		
		
		if(isset($dataid)){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();
		    
		    if(!isset($target)){
			$target = base64_encode($data->target);
		    }
		    
		    $model_gen->attributes = $data->attributes;
		} 
		
		return $this->renderAjax('_ezform_print', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'main_ezf_id'=>$main_ezf_id,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'ezform_comp'=>$ezform_comp,
		    'comp_id_target'=>$comp_id_target,
		    'dataComponent'=>$dataComponent,
		    'end'=>$end,
		    'readonly'=>$readonly,
		]);

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }

    public function actionEzformPrintAuto() {
	    $readonly = isset($_GET['readonly'])?$_GET['readonly']:0;
	    $end = isset($_GET['end'])?$_GET['end']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $main_ezf_id = isset($_GET['main_ezf_id'])?$_GET['main_ezf_id']:0;
	    $dataid = isset($_GET['dataid'])?$_GET['dataid']:NULL;
	    $target = isset($_GET['target'])?$_GET['target']:NULL;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:NULL;

            $params['comp_id'] = Yii::$app->request->get('comp_id_target');
            $params['target'] = self::actionGenForeigner($params['comp_id_target']); //cid
            $params['mode'] = 'gen-foreigner';

            self::actionInsertSpecial($params);

	    try {
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:$modelform->comp_id_target;
		
		if(isset($ezf_id) && isset($comp_id_target) && isset($target) && (!isset($dataid) || $dataid=='' || $dataid=='null')) {
		    $dataid = InvFunc::insertRecord([
			'ezf_id'=>$ezf_id,
			'target'=>$target,
			'comp_id_target'=>$comp_id_target,
		    ]);
		    
		}
		
		Yii::$app->session['ezform_main'] = $ezf_id;
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();
		
		
		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
		
		
		
		if(isset($dataid)){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();
		    
		    if(!isset($target)){
			$target = base64_encode($data->target);
		    }
		    
		    $model_gen->attributes = $data->attributes;
		} 
		
		return $this->renderAjax('_ezform_print', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'main_ezf_id'=>$main_ezf_id,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'ezform_comp'=>$ezform_comp,
		    'comp_id_target'=>$comp_id_target,
		    'dataComponent'=>$dataComponent,
		    'end'=>$end,
		    'readonly'=>$readonly,
		]);

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    
    public function actionStep2Confirm($queryParams=null){

        $queryParams = $queryParams ? $queryParams : Yii::$app->request->queryParams;

        $queryParams['target'] = preg_replace("/[^0-9]/", "", $queryParams['target']);
        $ezform_comp = Yii::$app->db->createCommand("SELECT ezf_id, field_id_desc FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$queryParams['comp_id']])->queryOne();

        $dataOtherSite=[];
        if($queryParams['task']=='add-from-site'){
            $arr_desc = explode(',', $ezform_comp['field_id_desc']);
            $dataComponent=[];
            foreach ($arr_desc AS $key => $val) {
                //คือไม่เอาตัวแรก
                if($key>1) {
                    $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id=:id;", [':id'=>$val])->queryOne();
                    $dataComponent[] = $ezf_fields['ezf_field_name'];
                }
            }
            $dataComponent[] = 'xsourcex';
            $dataComponent[] = 'id';
            $ezfrom = EzformQuery::getFormTableName($ezform_comp['ezf_id']);
            $query = new Query();
            $query->select($dataComponent)->from($ezfrom->ezf_table)->where('rstat <> 3 AND `cid` LIKE :cid AND id=ptid', [':cid'=>$queryParams['target']]);
            $query = $query->createCommand()->queryOne();
            $hospital = EzformQuery::getHospital($query['xsourcex']);
            $query['hospital'] = $hospital['name']. ' ต.'. $hospital['tambon']. ' อ.'. $hospital['amphur']. ' จ.'. $hospital['province'];
            $dataOtherSite=$query;
            //primary ezf_id
            $queryParams['comp_ezf_id'] = $ezform_comp['ezf_id'];
            $queryParams['comp_ezf_name'] = $ezfrom->ezf_name;
        }else if($queryParams['task'] == 'gen-foreigner'){
            $target = self::actionGenForeigner($queryParams['comp_id']);
            $queryParams['target'] = $target;
            $queryParams['task'] = 'add-new';
            $queryParams['mode'] = 'gen-foreigner';
        }
        //check tcc mapping fields
        $chkMappingTccBot = Yii::$app->db->createCommand("select count(*) as total from `ezform_maping_tcc` WHERE ezf_id=:ezf_id;", [':ezf_id'=>$ezform_comp['ezf_id']])->queryOne();

        //กรณีมีการส่งค่าผ่านทาง GET
        if($_GET['comp_id']){
            return $this->renderAjax('_step2confirm', [
                'queryParams' => $queryParams,
                'dataOtherSite' => $dataOtherSite,
                'chkMappingTccBot' => $chkMappingTccBot['total'],
            ]);
        }else {
            $arr['queryParams'] = $queryParams;
            $arr['dataOtherSite'] = $dataOtherSite;
            $arr['chkMappingTccBot'] = $chkMappingTccBot;
            return $arr;
        }
    }
    public function actionGenForeigner($comp_id=null){
        if($comp_id){
            $ezf_table = EzformQuery::getTablenameFromComponent($comp_id);
            //gen foreigner
            do {
                $cid = implode('', self::actionGenFid());
                //tbdata_1
                $res = Yii::$app->db->createCommand("SELECT cid AS total FROM `".($ezf_table->ezf_table)."` WHERE cid = :cid", [':cid'=>$cid])->queryOne();
            }while($res['total']);
            return $cid;
        }
    }
    
    public function actionGenFid(){
        $id = array();
        for($i = 0; $i < 13; $i++){
            if($i==0){
                $id[$i] = 0;
            }else{
                $id[$i] = rand(1,9);
            }
        }
        return $id;
    }
    
    public static function actionGenTarget()
    {
        //ถ้าตัวที่คลิกกับตัวในเซสชั่นมี component เท่ากัน ให้แสดงเป้าหมายเดิม
        $session = Yii::$app->session;
        $session->set('ezf_id', ($_POST['ezf_id']));
        $session->set('inputTarget', base64_encode($_POST['target']));//as base64
        echo base64_encode($_POST['target']);
    }
    public function actionRegFromBot(){
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        if (isset($_POST['findbot'])) {
            $cid = $_POST['findbot']['cid'];
            $key = $_POST['findbot']['key'];
            $convert = isset($_POST['findbot']['convert'])?$_POST['findbot']['convert']:0;
            $comp_id = $_POST['comp_id'];

            $checkCid = \backend\modules\ovcca\classes\OvccaFunc::check_citizen($cid);
            if($checkCid){

                $tccbot = InvFunc::getPersonOneByCid($sitecode, $cid, $key, $convert);
                //ดึงข้อมูลจาก tcc bot
                //เช็คความถูกต้องของข้อมูล
                //นำเข้าข้อมูล

                if(($tccbot)){

                    //+ getPersonOldServer
                    if(!\backend\modules\ovcca\classes\OvccaFunc::check_citizen($tccbot['cid'])){
                        $tccbot = InvFunc::getPersonOldServer($sitecode, $cid, $key, $convert);
                    }

                    if(\backend\modules\ovcca\classes\OvccaFunc::check_citizen($tccbot['cid'])){
                        $checkthaiword = trim(\backend\modules\ovcca\classes\OvccaFunc::checkthai($tccbot['fname']));

                        if ($checkthaiword  != '') {

                            if($_POST['import_botconfirm']) {
                                $r = InvFunc::importDataFromBot($sitecode, $cid, $tccbot, $comp_id);
                                if (count($r)) {
                                    $arr = [];
                                    $message = "นำเข้าข้อมูลแล้ว <code>เลขบัตรประชาชน: {$tccbot['cid']} ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}</code>";
                                    $arr['message'] = $message;
                                    $arr['status'] = 'success';
                                    $arr['import'] = 'success';
                                    return json_encode(array_merge($arr, $r));
                                } else {
                                    $arr = [];
                                    $message = "ไม่สามารถนำเข้าข้อมูลเข้าสู่ระบบ <code>เลขบัตรประชาชน: {$tccbot['cid']} ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}</code>";
                                    $arr['message'] = $message;

                                    return json_encode($arr);
                                }
                            }
                            $arr = [];
                            $message = "พบข้อมูล <code>เลขบัตรประชาชน: {$tccbot['cid']}</code> ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}";
                            $arr['message'] = $message;
                            $arr['status'] = 'success';
                            return json_encode($arr);

                        } else {
                            $arr = [];
                            $message = "กรณีชื่อ-สกุล อ่านไม่ออกให้เข้ารหัสแบบ tis620";
                            $arr['message'] = $message;
                            return json_encode($arr);
                        }
                    } else {
                        $arr = [];
                        $message = "Convert ข้อมูลไม่ได้ หรือ เลขบัตรประชาชน ไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง";
                        $arr['message'] = $message;
                        return json_encode($arr);
                    }

                } else {
                    $arr = [];
                    $arr['message'] = 'ไม่พบข้อมูลใน TDC';
                    return json_encode($arr);
                }

            } else {
                $arr = [];
                $arr['message'] = 'เลขบัตรประชาชนไม่ถูกต้อง';
                return json_encode($arr);
            }


        }

        return $this->renderAjax('_form_regbot', ['queryParams'=>$_POST]);
    }

    
    public function actionInsertSpecial($params=null)
    {
        //กำหนดค่าให้เลือกเป้าหมายเดิมหลัง insert ใหม่
        $session = Yii::$app->session;

        if($params){
            //
        }else{
            $params = [
                'comp_id' => Yii::$app->request->post('comp_id'),
                'target' => Yii::$app->request->post('target'), //cid
                'task' => Yii::$app->request->post('task'),
                'mode' => Yii::$app->request->post('mode'),
            ];
        }
	
        //final re check (task add new or add new from other site ?)
        $paramsx = self::actionRegisterFromComponent($params['comp_id'], $params['target']);
        $params['task'] = $paramsx['task'];

        $comp = EzformComponent::find()
            ->select('ezf_id, comp_id, field_search_cid')
            ->where(['comp_id' => $params['comp_id']])
            ->one();
        $ezform = Ezform::find()
            ->select('ezf_table')
            ->where(['ezf_id' => $comp->ezf_id])
            ->one();

        $result_success = [
            'status' => 'success',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'บันทึกข้อมูลสำเร็จ.'),
        ];
        $result_error = [
            'status' => 'error',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถดำเนินการได้ โปรดลองใหม่ภายหลัง.'),
        ];

        $cid = preg_replace("/[^0-9]/", "", $params['target']);
        if(!InvFunc::checkCid($cid) && $params['mode'] != 'gen-foreigner'){
            $result_error['message'] = '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ข้อมูลเลขบัตรประชาชนไม่ถูกต้อง.');
            return json_encode($result_error);
        }

        //set field cid
        $fieldCid = EzformQuery::getFieldNameByID($comp->field_search_cid);
        $fieldCid = $fieldCid->ezf_field_name;

        if ($params['task'] == 'add-new') {
            //type of special
            $id = GenMillisecTime::getMillisecTime();
            $hsitecode = Yii::$app->user->identity->userProfile->sitecode;
            $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
            $user_create = Yii::$app->user->id;
            $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `".($ezform->ezf_table)."` WHERE xsourcex = :xsourcex;", [':xsourcex'=>$hsitecode])->queryOne();
            $pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
            $ptcodefull = $hsitecode.$pid;

            Yii::$app->db->createCommand()->insert($ezform->ezf_table, [
                'id' =>$id,
                'ptid' =>$id,
                'xsourcex' => $hsitecode,
                'xdepartmentx' => $xdepartmentx,
                'target' => $id,
                'user_create' => $user_create,
                'create_date' => new Expression('NOW()'),
                'sitecode' => $hsitecode,
                'ptcode' =>$pid,
                'ptcodefull' =>$ptcodefull,
                'hsitecode' =>$hsitecode,
                'hptcode' =>$pid,
                $fieldCid => $cid,
                'rstat' => '0',
            ])->execute();

            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $comp->ezf_id;
            $model->data_id = $id;
            $model->target_id = $id;
            $model->comp_id = $comp->comp_id;
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $hsitecode;
            $model->save();
            //

            //set session
            $session->set('ezf_id', $comp->ezf_id);
            $session->set('inputTarget', base64_encode($id));//as base64

            //$url_redirect = Url::to(['/inputdata/step4', 'ezf_id' =>$comp->ezf_id, 'comp_id_target'=>$comp->comp_id, 'target' => base64_encode($id), 'dataid' => $id]);
            //self::redirect($url_redirect, 302);

        } else if ($params['task'] == 'add-from-site') {
            //type of special

            $id = GenMillisecTime::getMillisecTime();
            $hsitecode = Yii::$app->user->identity->userProfile->sitecode;
            $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
            $user_create = Yii::$app->user->id;
            $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `".($ezform->ezf_table)."` WHERE xsourcex = :xsourcex;", [':xsourcex'=>$hsitecode])->queryOne();
            $pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
            //หาข้อมูลที่สมบูรณ์ก่อน
            $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND (error IS NULL OR error = '') AND rstat <> 3 AND rstat IS NOT NULL ORDER BY update_date DESC;")->queryOne();
            //หาไม่เจอเอาข้อมูลที่แรกให้
            if(!$model['id'])  $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND id=ptid AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
            //หาไม่เจอเอาข้อมูลที่พบมาให้
            if(!$model['id'])  $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();

            //set values
            $model['id'] = $id;
            $model['xsourcex'] = $hsitecode;
            $model['xdepartmentx'] = $xdepartmentx;
            $model['user_create'] = $user_create;
            $model['create_date'] = new Expression('NOW()');
            $model['update_date'] = new Expression('NOW()');
            if($model['hncode']) $model['hncode'] = null;
            $model['tccbot'] = 0;
            $model['hsitecode'] = $hsitecode;
            $model['hptcode'] = $pid;
            $model['rstat'] = '0';
            $ptid = $model['ptid'];
            $model['id'] = $id;

            Yii::$app->db->createCommand()->insert($ezform->ezf_table, $model)->execute();

            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $comp->ezf_id;
            $model->data_id = $id;
            $model->target_id = $ptid;
            $model->comp_id = $comp->comp_id;
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $hsitecode;
            $model->save();
            //

            //set session
            $session->set('ezf_id', $comp->ezf_id);
            $session->set('inputTarget', base64_encode($ptid));//as base64
        }

        if(Yii::$app->request->get('redirect')=='url')
            return self::redirect(Url::to(['/inputdata/redirect-page', 'ezf_id' => $comp->ezf_id, 'dataid' => $id]), 302);

        $result_success['dataid'] = $id;
        $result_success['ezf_id'] = $comp->ezf_id;
	$result_success['target'] = base64_encode($id);
	$result_success['comp_id_target'] = Yii::$app->request->post('comp_id');
	$result_success['cid'] = $cid;
        $result_success['hsitecode'] = $hsitecode;
        $result_success['hptcode'] = $pid;
	
	$result_success['ezf_id_main'] = Yii::$app->session['ezform_main'];
	
		return json_encode($result_success);
    }
    public static function actionRegisterFromComponent($comp_id, $cid){

        $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id])->queryOne();
        $out = ['results' => ['id' => '', 'text' => '']];
        $modelComp = self::actionSpecialTarget1($ezform_comp);

        $res = InvFunc::taskSpecial1($out, $cid, $modelComp['sqlSearch'], $ezform_comp['ezf_id'], $ezform_comp);
        //VarDumper::dump($res,10,true);

        if($res['results'][0]['id']=='-990'){
            $queryParams['task'] = 'add-new';
            $queryParams['comp_id'] = $comp_id;
            $queryParams['target'] = $cid;
            //VarDumper::dump($res,10,true);
        }else if($res['results'][0]['id']=='-991'){
            $queryParams['task'] = 'add-from-site';
            $queryParams['comp_id'] = $comp_id;
            $queryParams['target'] = $cid;
        }

        //if foreigner
        $cid_k = substr($cid, 0, 1) =='0' ? true : false;
        if($cid_k){
            $queryParams['mode'] = 'gen-foreigner';
        }

        return $queryParams;

    }
    public static function actionSpecialTarget1($ezform_comp)
    {
        $ezf_table = EzformQuery::getTablenameFromComponent($ezform_comp['comp_id']);

        // array("hsitecode", 'hptcode', "name", "surname");
        //$arr = explode(',', $ezform_comp['field_id_desc']);
        $concatSearch ='';
        $modelComp = [];
        $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id in(".$ezform_comp['field_id_desc'].");")->queryAll();
        foreach ($ezf_fields AS $val) {
            $modelComp['arr_comp_desc_field_name'][] = $val['ezf_field_name'];
            $concatSearch .= "IFNULL(`".$val['ezf_field_name']."`, ''), ' ', ";
        }
        $concatSearch = substr($concatSearch, 0, -2);

        $sqlSearch = "select ptid as id, concat(".$concatSearch.") as text FROM `".($ezf_table->ezf_table)."` WHERE 1";

        //VarDumper::dump($sqlSearch); 

        $modelComp['comp_id'] = $ezform_comp['comp_id'];
        $modelComp['concatSearch'] = $concatSearch;
        $modelComp['tagMultiple'] = false;
        $modelComp['sqlSearch'] = $sqlSearch;
        $modelComp['special'] = true;
        //ใช้แสดงชื่อเป้าหมาย ขั้นตอนที่ 3
        $modelComp['ezf_table_comp'] = $ezf_table->ezf_table;

        $modelComp['comp_key_name'] = 'ptid';
        return $modelComp;
    }
    //เป้าหมาย (พิเศษ)
    
    
    public function actionRedirectPage(){
       
            $comp_id_target = Yii::$app->request->get('comp_id_target');
            $ezf_id = Yii::$app->request->get('ezf_id');
	    
            $comp = EzformComponent::find()
                ->where(['comp_id' => $comp_id_target])
                ->andWhere('comp_id <> 100000 AND comp_id <> 100001')
                ->one();

            $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id' => $comp->ezf_id])->queryOne();
            $comp_id = $ezform['comp_id_target'];

//	    $insertRecord = self::insertRecord([
//		'ezf_id'=>$comp->ezf_id,
//		'comp_id_target'=>$comp_id_target,
//	    ]);
		$table_name = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezf_id);
	    
                $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
	    
	    
//		$xsourcex = Yii::$app->user->identity->userProfile->sitecode;
//		$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
//		$target = $genid;
//		$ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
//		$targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target);
//		$pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED)) AS pidcode FROM $ezform->ezf_table WHERE xsourcex = '$xsourcex' ORDER BY id DESC LIMIT 1;")->queryOne();
//		
//		$pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
//		
//		$id = $genid;
//		
//                $model->id = $genid;
//                $model->rstat = 0;
//                $model->user_create = Yii::$app->user->id;
//                $model->create_date = new Expression('NOW()');
//                $model->user_update = Yii::$app->user->id;
//                $model->update_date = new Expression('NOW()');
//                $model->target = $genid;
//                $model->xsourcex = $xsourcex;//hospitalcode
//		$model->hsitecode = $xsourcex;
//		$model->hptcode = $pid;
//		$model->sitecode = $xsourcex;
//		$model->ptid = $genid;
//		$model->ptcode = $pid;
//		$model->ptcodefull = $xsourcex.$pid;
//		
//		if ($model->save()) {
//		    //save EMR
//		    $modelTarget = new \backend\models\EzformTarget();
//		    $modelTarget->ezf_id = $ezf_id;
//		    $modelTarget->data_id = $genid;
//		    $modelTarget->target_id = $genid;
//		    $modelTarget->comp_id = $comp_id_target;
//		    $modelTarget->user_create = Yii::$app->user->id;
//		    $modelTarget->create_date = new Expression('NOW()');
//		    $modelTarget->user_update = Yii::$app->user->id;
//		    $modelTarget->update_date = new Expression('NOW()');
//		    $modelTarget->rstat = 0;
//		    $modelTarget->xsourcex = $xsourcex;
//		    $modelTarget->save();
//		    
//		    //
//		}
	    
	    header('Access-Control-Allow-Origin: *');
            header("content-type:text/javascript;charset=utf-8");
            echo json_encode([
		'ezf_id'=>$ezf_id,
		'comp_id_target'=>$comp_id_target,
		//'dataid'=>$genid,
		//'target'=>  base64_encode($genid),
		'ezf_id_main' => Yii::$app->session['ezform_main'],
		    ]);
	    

    }    
     
    public function actionRegFromTdc(){
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        if (isset($_POST['findbot'])) {
            $cid = $_POST['findbot']['cid'];
            $key = $_POST['findbot']['key'];
            $convert = isset($_POST['findbot']['convert'])?$_POST['findbot']['convert']:0;
            $comp_id = $_POST['comp_id'];

            $checkCid = \backend\modules\ovcca\classes\OvccaFunc::check_citizen($cid);
            if($checkCid){

                $tdc = InvFunc::getPersonTdcOneByCid($sitecode, $cid, $key, $convert);
                //ดึงข้อมูลจาก tdc
                //เช็คความถูกต้องของข้อมูล
                //นำเข้าข้อมูล

                if(($tdc)){
                    if(\backend\modules\ovcca\classes\OvccaFunc::check_citizen($tdc['CID'])){
                        $checkthaiword = trim(\backend\modules\ovcca\classes\OvccaFunc::checkthai($tdc['Name']));

                        if ($checkthaiword  != '') {

                            if($_POST['import_botconfirm']) {
                                $r = InvFunc::importDataFromTdc($sitecode, $cid, $tdc, $comp_id);
                                if (count($r)) {
                                    $arr = [];
                                    $message = "นำเข้าข้อมูลแล้ว <code>เลขบัตรประชาชน: {$tdc['CID']} ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}</code>";
                                    $arr['message'] = $message;
                                    $arr['status'] = 'success';
                                    $arr['import'] = 'success';
                                    return json_encode(array_merge($arr, $r));
                                } else {
                                    $arr = [];
                                    $message = "ไม่สามารถนำเข้าข้อมูลเข้าสู่ระบบ <code>เลขบัตรประชาชน: {$tdc['CID']} ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}</code>";
                                    $arr['message'] = $message;

                                    return json_encode($arr);
                                }
                            }
                            $arr = [];
                            $message = "พบข้อมูล <code>เลขบัตรประชาชน: {$tdc['CID']}</code> ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}";
                            $arr['message'] = $message;
                            $arr['status'] = 'success';
                            return json_encode($arr);

                        } else {
                            $arr = [];
                            $message = "กรณีชื่อ-สกุล อ่านไม่ออกให้เข้ารหัสแบบ tis620";
                            $arr['message'] = $message;
                            return json_encode($arr);
                        }
                    } else {
                        $arr = [];
                        $message = "Convert ข้อมูลไม่ได้ หรือ เลขบัตรประชาชน ไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง";
                        $arr['message'] = $message;
                        return json_encode($arr);
                    }

                } else {
                    $arr = [];
                    $arr['message'] = 'ไม่พบข้อมูลใน TDC';
                    return json_encode($arr);
                }

            } else {
                $arr = [];
                $arr['message'] = 'เลขบัตรประชาชนไม่ถูกต้อง';
                return json_encode($arr);
            }


        }

        return $this->renderAjax('_form_regtdc', ['queryParams'=>$_POST]);
    }

    

    

     
    public function actionEzformEmr() {
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid'])?$_GET['dataid']:NULL;
	    $target = isset($_GET['target'])?$_GET['target']:NULL;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:NULL;
	    
	    try {
		$dataProvidertarget = \backend\models\InputDataSearch::searchTarget($ezf_id, $target, 'draft', $comp_id_target);
		
		$modelEzform = Ezform::find()
		->where('ezform.ezf_id=:ezf_id', [':ezf_id'=>$ezf_id])
		->one();
		
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();

		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
		
		
		return $this->renderAjax('_ezform_emr', [
		    'dataProvidertarget'=>$dataProvidertarget,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'dataComponent'=>$dataComponent,
		    'modelEzform' =>$modelEzform,
		]);

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    
    public static function actionLookupTarget()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $string_q = Yii::$app->request->post('q');
        $table = Yii::$app->request->post('table');
        $string_decode = base64_decode(Yii::$app->request->post('search')); //sql
        $ezf_id = Yii::$app->request->post('ezf_id');
        $ezf_comp = \backend\modules\component\models\EzformComponent::find()
            ->where(['comp_id' => Yii::$app->request->post('ezf_comp_id')])
            ->one();
    
        //หากมีการกำหนด special component
        if ($ezf_comp['special'] == 1) {
            $out = InvFunc::taskSpecial1($out, $string_q, $string_decode, $ezf_id, $ezf_comp);
        } else {
            //ถ้าไม่มีการกำหนด ให้ค้นหาแบบธรรมดา ซ้ำๆตั้ง
            //\appxq\sdii\utils\VarDumper::dump($table,true,false);
            $ortQuery = Yii::$app->db->createCommand(str_replace('$q', $string_q, $string_decode). " AND $table.rstat not in(0, 3) LIMIT 0,10");
            $data = $ortQuery->queryAll();
            $out['results'] = array_values($data);

            if (!$ortQuery->query()->count())
                $out['results'] = [['id' => '-992', 'text' => 'ไม่พบคำที่ค้นหา! ลองค้นคำอื่นๆ (คลิกที่นี่ถ้าต้องการเพิ่มเป้าหมายใหม่)']];
        }

        echo json_encode($out);
        return;
    }
    
    public function actionEzformSave() {
	if (Yii::$app->getRequest()->isAjax) {
	    
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = isset($_GET['target']) && $_GET['target']!=''?$_GET['target']:'';
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:'';
	    
	    $target_decode = $target!=''?base64_decode($target):0;
	    
	    $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
	    $rstat = $_POST['submit'];
	    
	    $model_fields = \backend\modules\ezforms\components\EzformQuery::getFieldsByEzf_id($ezf_id);
            $model_form = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($model_fields);
            $table_name = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezf_id);
            
	    
	    $model_form->attributes = $_POST['SDDynamicModel'];

	    $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);
	    
	    
	    
	    
	    if (isset($_POST['SDDynamicModel'])) {
                if($id==0 && $target_decode>0){
		//insert new and set $id
		
                $genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
		
		$ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
		$targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target_decode);
		$pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED)) AS pidcode FROM $ezform->ezf_table WHERE xsourcex = '$xsourcex' ORDER BY id DESC LIMIT 1;")->queryOne();
		
		$pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
		
		$id = $genid;
		
                $model->id = $genid;
                $model->rstat = 0;
                $model->user_create = Yii::$app->user->id;
                $model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                $model->target = $target_decode;
                $model->xsourcex = $xsourcex;//hospitalcode
		$model->hsitecode = $xsourcex;
		$model->hptcode = $pid;
		$model->sitecode = $targetx['sitecode'];
		$model->ptid = $targetx['ptid'];
		$model->ptcode = $targetx['ptcode'];
		$model->ptcodefull = $targetx['sitecode'].$targetx['ptcode'];
		
		if ($model->save()) {
		    //save EMR
		    $modelTarget = new \backend\models\EzformTarget();
		    $modelTarget->ezf_id = $ezf_id;
		    $modelTarget->data_id = $genid;
		    $modelTarget->target_id = $target_decode;
		    $modelTarget->comp_id = $comp_id_target;
		    $modelTarget->user_create = Yii::$app->user->id;
		    $modelTarget->create_date = new Expression('NOW()');
		    $modelTarget->user_update = Yii::$app->user->id;
		    $modelTarget->update_date = new Expression('NOW()');
		    $modelTarget->rstat = 0;
		    $modelTarget->xsourcex = $xsourcex;
		    $modelTarget->save();
		    
		    //
		}
	    } elseif($id==0 && $target_decode==0){
		$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
		$target = $genid;
		$ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
		//$targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target);
		$pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED)) AS pidcode FROM $ezform->ezf_table WHERE xsourcex = '$xsourcex' ORDER BY id DESC LIMIT 1;")->queryOne();
		
		$pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
		
		
		
                $model->id = $genid;
                $model->rstat = 0;
                $model->user_create = Yii::$app->user->id;
                $model->create_date = new Expression('NOW()');
                $model->user_update = Yii::$app->user->id;
                $model->update_date = new Expression('NOW()');
                $model->target = $genid;
                $model->xsourcex = $xsourcex;//hospitalcode
		$model->hsitecode = $xsourcex;
		$model->hptcode = $pid;
		$model->sitecode = $xsourcex;
		$model->ptid = $genid;
		$model->ptcode = $pid;
		$model->ptcodefull = $xsourcex.$pid;
		
		if ($model->save()) {
                    $id = $genid;
                    $target = base64_encode($genid);
                    $target_decode = $genid;

		    //save EMR
		    $modelTarget = new \backend\models\EzformTarget();
		    $modelTarget->ezf_id = $ezf_id;
		    $modelTarget->data_id = $genid;
		    $modelTarget->target_id = $genid;
		    $modelTarget->comp_id = $comp_id_target;
		    $modelTarget->user_create = Yii::$app->user->id;
		    $modelTarget->create_date = new Expression('NOW()');
		    $modelTarget->user_update = Yii::$app->user->id;
		    $modelTarget->update_date = new Expression('NOW()');
		    $modelTarget->rstat = 0;
		    $modelTarget->xsourcex = $xsourcex;
		    $modelTarget->save();
		    
		    //
		}
            } 
            
            Yii::$app->response->format = Response::FORMAT_JSON;
		
            $modelOld = $model->find()->where('id = :id', ['id' => $id])->One();

	    //$model_form->attributes = $modelOld->attributes;
	    $model_form->attributes = $_POST['SDDynamicModel'];

            //if click final save (rstat not change)
	    
	    //VarDumper::dump($modelOld->attributes,10,true);
	    //VarDumper::dump($model_form->attributes,10,true);
	    $model->attributes = $modelOld->attributes;
	    
	    if($rstat != 3){
                $model->attributes = $model_form->attributes;
            }
	    
	    $model->hsitecode = $modelOld->hsitecode;
	    $model->hptcode = $modelOld->hptcode;
	    $model->sitecode = $modelOld->sitecode;
	    $model->ptid = $modelOld->ptid;
	    $model->ptcode = $modelOld->ptcode;
	    $model->ptcodefull = $modelOld->ptcodefull;
	    $model->user_create = $modelOld->user_create;
	    $model->create_date = $modelOld->create_date;
	    $model->error = $modelOld->error;
	    $model->xdepartmentx = $modelOld->xdepartmentx;
	    
	    
	    $model->id = $id; //ห้ามเอาออก ไม่งั้นจะ save ไม่ได้
	    $model->rstat = $rstat;
	    $model->xsourcex = $xsourcex;
	    //$model->create_date = new Expression('NOW()');
	    $model->user_update = Yii::$app->user->id;
	    $model->update_date = new Expression('NOW()');
	    //echo $rstat;
            
	    $model->target = $target_decode;
            
	    foreach ($model_fields as $key => $value) {
                
                    if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {
                        // set Data Sql Format
                        $data = $model[$value['ezf_field_name']];
                        if($data+0) {
                            $explodeDate = explode('/', $data);
                            $formateDate = ($explodeDate[2] - 543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                            $model->{$value['ezf_field_name']} = $formateDate;
                        }else{
                            $model->{$value['ezf_field_name']} = new Expression('NULL');
                        }
                    }
                    else if ($value['ezf_field_type'] == 10) {
                        if (count($model->{$value['ezf_field_name']}) > 1)
                            $model->{$value['ezf_field_name']} = implode(',', $model->{$value['ezf_field_name']});
                    }
                    else if ($value['ezf_field_type'] == 14) {
                       
                        if (isset($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']]) && $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']] !='' && is_array($_FILES['SDDynamicModel']['name'][$value['ezf_field_name']])) {
                            $fileItems = $_FILES['SDDynamicModel']['name'][$value['ezf_field_name']];
                            $fileType = $_FILES['SDDynamicModel']['type'][$value['ezf_field_name']];
                            $newFileItems = [];
                            $action = false;
                            foreach ($fileItems as $i => $fileItem) {
                                if($fileItem!=''){
                                    $action = true;
                                    $fileArr = explode('/', $fileType[$i]);
                                    if (isset($fileArr[1])) {
                                        $fileBg = $fileArr[1];

                                        //$newFileName = $fileName;
                                        $newFileName = $value['ezf_field_name'].'_'.($i+1).'_'.$model->id.'_'.date("Ymd_His").(microtime(true)*10000) . '.' . $fileBg;
                                        //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                                        $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
                                        $fieldname = 'SDDynamicModel[' . $value['ezf_field_name'] . '][' . $i . ']';

                                        $file = UploadedFile::getInstanceByName($fieldname);
                                        $file->saveAs($fullPath);
                                        
					if($file){
					    $newFileItems[] = $newFileName;

					    //add file to db
					    $file_db = new \backend\modules\ezforms\models\FileUpload();
					    $file_db->tbid = $model->id;
					    $file_db->ezf_id = $value['ezf_id'];
					    $file_db->ezf_field_id = $value['ezf_field_id'];
					    $file_db->file_active = 0;
					    $file_db->file_name = $newFileName;
					    $file_db->file_name_old = $fileItem;
					    $file_db->target = ($model->ptid ? $model->ptid :  $model->target).'';
					    $file_db->save();
					}
                                    }
                                }
                            }
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            if($action){
                                $model->{$value['ezf_field_name']} = implode(',', $newFileItems);

//				if($res){
//				    $res_items = explode(',', $res['filename']);
//
//				    foreach ($res_items as $dataTmp) {
//					@unlink(Yii::$app->basePath . '/../backend/web/fileinput/' . $dataTmp);
//				    }
//				}
                            } else {
                                if($res){
                                    $model->{$value['ezf_field_name']} = $res['filename'];
                                }
                            }

                        } else {
                            $res = Yii::$app->db->createCommand("SELECT `" . $value['ezf_field_name'] . "` as filename FROM `" . $table_name->ezf_table . "` WHERE id = :dataid", [':dataid' => $_POST['id']])->queryOne();
                            $model->{$value['ezf_field_name']} = $res['filename'];
                        }
                    }
                    else if ($value['ezf_field_type'] == 24) {

                        if (stristr($model[$value['ezf_field_name']], 'tmp.png') == TRUE) {
                            //set data Drawing
                            $fileArr = explode(',', $model[$value['ezf_field_name']]);
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            $newFileName = $fileName;
                            $newFileBg = $fileBg;
                            $nameEdit = false;
                            $bgEdit = false;
                            if (stristr($fileName, 'tmp.png') == TRUE) {
                                $nameEdit = true;
                                $newFileName = date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                                @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                            }
                            if (stristr($fileBg, 'tmp.png') == TRUE) {
                                $bgEdit = true;
                                $newFileBg = 'bg_' . date("Ymd_His").(microtime(true)*10000) . '.png';
                                @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                            }

                            $model[$value['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                            $modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

                            if (isset($modelTmp['id'])) {
                                $fileArr = explode(',', $modelTmp[$value['ezf_field_name']]);
                                if (count($fileArr) > 1) {
                                    $fileName = $fileArr[0];
                                    $fileBg = $fileArr[1];
                                    if ($nameEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/data/' . $fileName);
                                    }
                                    if ($bgEdit) {
                                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileBg);
                                    }
                                }
                            }
                        }
                    } else if ($value['ezf_field_type'] == 30) {
			$fileName = $model[$value['ezf_field_name']];
			$newFileName = $fileName;
                        $nameEdit = false;
			
			$foder = 'sddynamicmodel-'.$value['ezf_field_name'].'_'.Yii::$app->user->id;
			
			if (stristr($fileName, 'fileNameAuto') == TRUE) {
			    $nameEdit = true;
			    $newFileName = date("Ymd_His").(microtime(true)*10000) . '.mp3';
			    @copy(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName, Yii::$app->basePath . '/../storage/web/audio/' . $newFileName);
			    @unlink(Yii::$app->basePath . "/../storage/web/audio/$foder/" . $fileName);
			}
			
			$model[$value['ezf_field_name']] = $newFileName;
			
			$modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $_POST['id']);

			if (isset($modelTmp['id'])) {
			    
				$fileName = $modelTmp[$value['ezf_field_name']];
				//sddynamicmodel-
				if ($nameEdit) {
				    @unlink(Yii::$app->basePath . "/../storage/web/audio/" . $fileName);
				}
			}
		    }
                }
		
		if ($model->update()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
                        'id'=>$id,
                        'target'=>$target,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } 
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionEzformMian() {
	    $readonly = isset($_GET['readonly'])?$_GET['readonly']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = isset($_GET['target']) && $_GET['target']!=''?$_GET['target']:'';
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:0;
	    $target_decode = base64_decode($target);
	    $targetOld = $target_decode;
            //\appxq\sdii\utils\VarDumper::dump($id);
	    try {
		
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();
		//\appxq\sdii\utils\VarDumper::dump(!(isset($target) && $target!=''));
		if($target==''){
		    $modelezform_target = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezform_comp['ezf_id']])->one();

		    $model = new \backend\modules\ezforms\models\EzformDynamic($modelezform_target->ezf_table);

		    $modelOld = $model->find()->where('id = :id', ['id' => $id])->One();
		    
		    $ftarget = 'id';
		    if ($ezform_comp['special'] == 1) {
			$ftarget = 'ptid';
		    } 
		    $target_decode = $modelOld[$ftarget];
		    
		    $target = base64_encode($target_decode);
		}
		
		if(isset($ezf_id) && isset($comp_id_target) && isset($target) && $dataid==0) {
		    $dataid = InvFunc::insertRecord([
			'ezf_id'=>$ezf_id,
			'target'=>$target,
			'comp_id_target'=>$comp_id_target,
		    ]);
		    
		}
		
		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
		
		if(isset($dataid) && $dataid>0){
		    $model = new \backend\models\Tbdata();
		    $model->setTableName($table);

		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);

		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		}

                //ฟอร์มบันทึก '.$modelform['ezf_name'].'
		$prefix = '<div class="panel">
		    <div class="panel-heading clearfix" id="panel-ezform-box">
			<h3 class="panel-title"><a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezf_id.'&amp;print=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>'.'</h3>
		</div>
		    <div class="panel-body" id="panel-ezform-body">';
		                

		return $prefix.$this->renderAjax('_ezform_widget', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezf_id,
		    'dataid'=>$dataid,
		    'target'=>$target,
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-main',
		    'end'=>1,
                    'readonly'=>$readonly,
		]).'</div></div>';

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    
    public function actionEzformTarget() {
	    $readonly = isset($_GET['readonly'])?$_GET['readonly']:0;
	    $end = isset($_GET['end'])?$_GET['end']:0;
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    $dataid = isset($_GET['dataid']) && $_GET['dataid']>0?$_GET['dataid']:0;
	    $target = isset($_GET['target']) && $_GET['target']!=''?$_GET['target']:'';
	    $comp_id_target = isset($_GET['comp_id_target'])?$_GET['comp_id_target']:0;
	    $target_decode = base64_decode($target);
	    $targetOld = $target_decode;
	    try {
		
		$ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id_target])->queryOne();
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezform_comp['ezf_id']])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezform_comp['ezf_id']])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		$ftarget='id';
                $get_target = null;
		if($dataid>0){
//		    $model = new \backend\models\Tbdata();
//		    $model->setTableName($table);
		    
		    $model = new \backend\modules\ezforms\models\EzformDynamic($table);
		    $data = $model->find()->where('id = :id', ['id' => $dataid])->One();
	    
//		    $query = $model->find()->where('id=:id', [':id'=>$dataid]);
//
//		    $data = $query->one();

		    $model_gen->attributes = $data->attributes;
		    
		    if ($ezform_comp['special'] == 1) {
			$target_decode = $data->ptid;
			$ftarget='ptid';
		    } else {
			$target_decode = $data->id;
		    }
		 
                    
		}
		$get_target = isset($target) && $target!=''?$targetOld:Yii::$app->db->createCommand("select $ftarget from $table where id = '$target_decode'")->queryScalar();
		//$get_target = Yii::$app->db->createCommand("select $ftarget from $table where id = '$target'")->queryScalar();
		
		$dataComponent = [];
		if(isset($comp_id_target)){
		    if ($ezform_comp['special'] == 1) {
			//หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
			
			$dataComponent = InvFunc::specialTarget1($ezform_comp);
			
			//$dataProvidertarget = \backend\models\InputDataSearch::searchEMR($target);
		    } else {
			//หาแบบปกติ
			$dataComponent = InvFunc::findTargetComponent($ezform_comp);
			
		    }
		} else {
		    //target skip
		    $dataComponent['sqlSearch'] = '';
		    $dataComponent['tagMultiple'] = FALSE;
		    $dataComponent['ezf_table_comp'] = '';
		    $dataComponent['arr_comp_desc_field_name'] = '';
		}
                //ฟอร์มเป้าหมาย '.$modelform['ezf_name'].'
		$prefix = '<div class="panel ">
		    <div class="panel-heading clearfix" >
			<h3 class="panel-title"><a class="pull-right" href="/inputdata/printform?dataid='.$dataid.'&amp;id='.$ezform_comp['ezf_id'].'&amp;print=1" target="_blank"><span class="fa fa-print fa-2x"></span></a>'.'</h3>
		</div>
		    <div class="panel-body" id="panel-target-body">';
		

		return $prefix.$this->renderAjax('_ezform_widget', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		    'ezf_id'=>$ezform_comp['ezf_id'],
		    'dataid'=>$dataid,
		    'target'=> $get_target?base64_encode($get_target):'',
		    'comp_id_target'=>$comp_id_target,
		    'formname'=>'ezform-target',
		    'end'=>$end,
		    'readonly'=>$readonly,
		]).'</div></div>';

	    } catch (\yii\db\Exception $e) {
		//\appxq\sdii\utils\VarDumper::dump($e);
	    }
	    
    }
    ///nut
    	public function actionCheckuser()
    {
         $user_id = Yii::$app->user->identity->id;
         $user_cmd = Cmd::find()->where(["user_id"=>$user_id])->one();
         if(!empty($user_cmd))
         {
            echo $user_id." ได้แสดงความคิดเห็นแล้วครับ";
         }
         //echo $user_id;
    }
    public function actionCounts()
    {
        $сount = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
        $sum = Yii::$app->db->createCommand("select sum(vote) as sums from {$this->table}")->queryAll();
        $sums = (int)$sum[0]['sums'];
        $counts =  round($sums /= $сount,1);
        $strCounts = explode(".","".$counts);

        $html = "";

        $html .= "<span class=\"rating-num\">".number_format($counts,1)."</span></span>";

        $html .= "<div class=\"rating-stars\">";
             // $arr = [1,2,3,4,5];  
        if($strCounts[1]==0 || empty($strCounts[1])){
            $strCounts[1]=1;
        }

        for($i=1; $i<=$counts; $i++)
        {
            if($i == $strCounts[0])
            {
                for($j=0; $j<$strCounts[0]; $j++)
                {
                    $html .= "<apan><i class='fa fa-star' style=\"color: #737373;\"></i></span>";
                            }//end for 

                            $number = 5-$strCounts[0];

                            for($j=0; $j<$number; $j++)
                            {
                                if($strCounts[1]  >= 5)
                                {
                                    $html .= "<i class=\"fa fa-star-half-o\" style=\"color: #737373;\"></i>";
                                    $strCounts[1] =1;
                                    
                                }else{

                                    $html .= "<apan><i class='fa fa-star-o' style=\"color: #737373;\"></i></span>";
                                    
                                } //end if
                                
                            }//end for
                        }//end for 
                        
                    }
                    $html .= "</div>";
                    $html .= "<div class=\"rating-users\">";
                    $html .= "<i class=\"icon-user\"></i> ".$сount ." คน";
                    $html .= "</div>";
                    echo $html;  
    }//Counts 
    public function actionCountInv()
    {
       $сount = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
       echo $сount;
   }
   public function actionShowInv()
   {
    $html = "";
    $pageSize = 3;
    if(!empty($_POST['pageSize']))
    {
        $pageSize = $_POST['pageSize'];
    }
           // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //$sql="SELECT * FROM {$this->table} ORDER BY id DESC LIMIT 0,$pageSize";
     $uid = "";
     if(!empty($_POST['uid']))
     {

        $uid = Yii::$app->user->identity->id;
        //echo $uid;exit();
     }
  
       if(empty($uid)){
        $sql="
            SELECT us.username,concat(u.firstname,'  ',u.lastname) as name, concat(u.avatar_base_url,'/',u.avatar_path) as images, i.*
             FROM inv_comment as i
            INNER JOIN  user_profile as u
            on u.user_id = i.user_id
            INNER JOIN user as us 
            ON i.user_id = us.id
            ORDER BY id DESC LIMIT 0,$pageSize
        "; 
       }else{
           $sql="
            SELECT us.id,us.username,concat(u.firstname,'  ',u.lastname) as name, concat(u.avatar_base_url,'/',u.avatar_path) as images, i.*
             FROM inv_comment as i
            INNER JOIN  user_profile as u
            on u.user_id = i.user_id
            INNER JOIN user as us 
            ON i.user_id = us.id
            WHERE us.id = $uid LIMIT 1"; 
       }
    
    $cmd= Yii::$app->db->createCommand($sql)->queryAll();
    $arr = array();
    foreach($cmd as $c)
    {
        $arr[] = [
        'id'=>$c['id'],
        'vote'=>$c['vote'],
        'message'=>$c['message'],
        'updated_at'=>$c['updated_at'],
        'user_id'=>$c['user_id'],
        'name'=>$c['name'],
        'images'=>$c['images']
        ];
    }
    $cmds = new Cmd();
    foreach($arr as $a){
        if(!empty($uid))
        {
            $html .= "<div class=\"media\" style='background: #ffffff; padding: 5px; border-radius: 5px;'>";
        }else{
            $html .= "<div class=\"media\" style='background: #ffffff; padding: 5px; border-radius: 5px;border: 1px solid rgba(9, 9, 35, 0.08);'>";
        }
        
        $html .= "<div class=\"media-left\">";
        
        if(!empty($a['images'])){
            $html .= "<img src='".$a['images']."' class=\"media-object img img-circle\" style=\"width:60px\">";
        }else{
            $html .= "<img src=\"/img/anonymous.jpg\" class=\"media-object img img-circle\" style=\"width:60px\">";
        }

       // $html .= "<img src=".Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ." alt=\"\" class=\"center-block img-thumbnail img-responsive\">";


        $html .= "</div>";
        $html .= "<div class=\"media-body\">";
        $html .= "<h5 class=\"media-heading\">".$a['name']."</h5>";
        if(!empty($uid))
        {
            $html .= "<span class='pull-right'><button class='btn btn-default btn-sm'><i class='glyphicon glyphicon-pencil'></i>แก้ไข</button>
            <button class='btn btn-default btn-sm'><i class='glyphicon glyphicon-trash'></i> ลบ</button></span>";
        }
        $html .= "<p>";
        $vote = $a['vote'];
        if(empty($vote)){
            $vote = 0;
        }
        $vote_arr = [1,2,3,4,5]; 
        $voteEnd = 5;
        foreach($vote_arr as $v){
            if($vote == $v)
            {
                for($i=0; $i<$vote; $i++)
                {
                    $html .= '<i class="glyphicon glyphicon-star" style="color:#ffb234;"></i>';
                }

                $x = 5-$vote;
                for($j=0; $j<$x; $j++){
                    $html .= '<i class="glyphicon glyphicon-star-empty"></i>';
                }
            } 
        }
        $html .= '</p>'; 
        $html .= "<p>".$a['message']."</p>"; 

        $html .= "</div>";

        $html .= "</div>";
    }
    echo $html;       
}
public function actionRealtime()
{
    $sql="SELECT * FROM {$this->table} WHERE `real` = 1";
    $model = Yii::$app->db->createCommand($sql)->queryAll();
       // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $count = Count($model);
    if($count == 0 )
    {
        $count ='0';
    }
    echo $count;  
    }//Realtime 
    public function actionSaverealtime()
    {
        $sql="UPDATE {$this->table} SET `real` = 0";
        $model = Yii::$app->db->createCommand($sql)->execute();

    }//Saverealtime
    public function actionShowcmd(){
        //$cmd = Cmd::find()->all();
        try{

            $endPage = 5;
            if(!empty($_POST))
            {
                $endPage = $_POST["endPage"];
            }
            $sql="SELECT * FROM {$this->table} ORDER BY id DESC LIMIT 0,$endPage";
            $cmd= Yii::$app->db->createCommand($sql)->queryAll();

        }catch(Exception $e){
            echo $e->getMessage();
        }
        
        return $this->render('cmdshow',[
            'cmd'=>$cmd
            ]);
        
    }//end Showcomment
    public function actionShowform()
    {
    	//echo 'hello';exit();
         $sql="
            SELECT  concat(u.avatar_base_url,'/',u.avatar_path) as images 
             FROM  user_profile as u
            WHERE user_id = ".Yii::$app->user->identity->id."
             LIMIT 1        
        "; 
        $img = Yii::$app->db->createCommand($sql)->queryOne();
        echo $img['images']; 
        return $this->renderAjax("/cmd/create",[
            'img'=>$img
        ]);
    }//end Showform
    public function actionCreate()
    {

        $model = new Cmd();
        
        if(!empty($_POST))
        {

            $date=Date('Y-m-d');
            $vote = trim($_POST["vote"]);
            $message = trim($_POST["message"]);
            $message_alert=[];

            if($model->checkValidations($message) == true){
                $message_alert=[
                'status'=>404,
                'message'=>'คุณไม่สามารถกรอก script ได้นะครับ'
                ];
                echo json_encode($message_alert);
                exit();
            }


            $columns = array(
                'gid' => 1,
                'user_id' => 1, 
                'vote'=>$vote,
                'message'=>$message,
                'created_by'=>$date,
                'created_at'=>$date,
                'updated_by'=>$date,
                'updated_at'=>$date,
                );          
            $model->save();
            $cmd = Yii::$app->db->createCommand();
            $table= $this->table;
            $cmd->insert($table,$columns);

            if($cmd->execute())
            {
               $message_alert=[
               'status'=>200,
               'message'=>'บันทึกข้อมูลแล้วครับ'
               ];
               echo json_encode($message_alert);
               exit();
           } 

        }//end Create
        




    }//actionCreate ---> Create
    public function actionUpdate($id)
    {

    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    //end nut
    
    
}
