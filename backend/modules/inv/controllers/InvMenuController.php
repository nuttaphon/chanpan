<?php

namespace backend\modules\inv\controllers;

use Yii;
use backend\modules\inv\models\InvMenu;
use backend\modules\inv\models\InvMenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use yii\db\Expression;

/**
 * InvMenuController implements the CRUD actions for InvMenu model.
 */
class InvMenuController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    

    /**
     * Displays a single InvMenu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($module, $id)
    {
	$userId = Yii::$app->user->id;
	$modelGen = \backend\modules\inv\classes\InvQuery::getModule($module, $userId);
	
	$model = InvMenu::find()->where('menu_id=:id AND gid=:gid', [':id'=>$id, ':gid'=>$module])->one();
	
	if(!$modelGen){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'คุณไม่มีสิทธิ์ใช้โมดูลนี้',
		'options'=>['class'=>'alert-danger']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	if(!$model){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> SDHtml::getMsgError() . Yii::t('app', 'คุณไม่มีสิทร์ใช้เมนูนี้'),
		'options'=>['class'=>'alert-danger']
	    ]);
	    return $this->redirect(['/inv/inv-person/index', 'module'=>$module]);
	}
	
	return $this->render('view', [
	    'model' => $model,
	    'module'=>$module,
	    'modelGen'=>$modelGen,
	]);
    }

    /**
     * Creates a new InvMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($module)
    {
	$userId = Yii::$app->user->id;
	$modelGen = \backend\modules\inv\classes\InvQuery::getModule($module, $userId);
	
	if(!$modelGen){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'คุณไม่มีสิทธิ์ใช้โมดูลนี้',
		'options'=>['class'=>'alert-danger']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	    $model = new InvMenu();
	    $model->gid = $module;
	    $model->created_by = Yii::$app->user->id;
	    $model->created_at = new Expression('NOW()');
	    $model->menu_active = 1;
	    
	    $modelmainmenu = InvMenu::find()->where(['gid' => $module,'menu_parent'=>0])->addOrderBy('menu_order')->all();
	    $modelmenu = InvMenu::find()->where(['gid' => $module])->addOrderBy('menu_order')->all();
	    
	    if ($model->load(Yii::$app->request->post())) {
		if ($model->save()) {
		    $this->reorder($module);
		    
		    Yii::$app->getSession()->setFlash('alert', [
			'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'options'=>['class'=>'alert-success']
		    ]);
		    
		    return $this->redirect(['/inv/inv-menu/view', 'module'=>$module , 'id'=>$model->menu_id]);
		} else {
		    Yii::$app->getSession()->setFlash('alert', [
			'body'=> SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'options'=>['class'=>'alert-danger']
		    ]);
		    
		    return $this->redirect(['/inv/inv-menu/create', 'module'=>$module]);
		}
	    } 
	    
	    return $this->render('create', [
		'model' => $model,
		'module'=>$module,
		'modelmainmenu' => $modelmainmenu,
                'modelmenu' => $modelmenu,   
		'modelGen'=>$modelGen,
	    ]);
    }

    public function reorder($module) {
        Yii::$app->db->createCommand("set @i:=0;UPDATE inv_menu set `menu_order`=(@i:=@i+1)*10 WHERE gid=:gid order by `menu_order`", [':gid'=>$module])->query();
    }
    /**
     * Updates an existing InvMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $module)
    {
	$userId = Yii::$app->user->id;
	$modelGen = \backend\modules\inv\classes\InvQuery::getModule($module, $userId);
	
	if(!$modelGen){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> 'คุณไม่มีสิทธิ์ใช้โมดูลนี้',
		'options'=>['class'=>'alert-danger']
	    ]);

	    return $this->redirect(['/tccbots/my-module']);
	}
	
	$model = InvMenu::find()->where('menu_id=:id AND gid=:gid AND created_by=:created_by', [':id'=>$id, ':gid'=>$module, ':created_by'=>$userId])->one();
	
	if(!$model){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> SDHtml::getMsgError() . Yii::t('app', 'คุณไม่มีสิทร์แก้ไขข้อมูลนี้'),
		'options'=>['class'=>'alert-danger']
	    ]);
	    return $this->redirect(['/inv/inv-person/index', 'module'=>$module]);
	}
	
	$modelmainmenu = InvMenu::find()->where(['gid' => $module,'menu_parent'=>0])->addOrderBy('menu_order')->all();
	$modelmenu = InvMenu::find()->where(['gid' => $module])->addOrderBy('menu_order')->all();
	
	if ($model->load(Yii::$app->request->post())) {
	    if ($model->save()) {
		$this->reorder($module);

		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		    'options'=>['class'=>'alert-success']
		]);

		return $this->redirect(['/inv/inv-menu/view', 'module'=>$module , 'id'=>$model->menu_id]);
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/inv/inv-menu/update', 'module'=>$module, 'id'=>$id]);
	    }
	} 

	return $this->render('update', [
	    'model' => $model,
	    'module'=>$module,
	    'modelmainmenu' => $modelmainmenu,
	    'modelmenu' => $modelmenu,    
	    'modelGen'=>$modelGen,
	]);
	
    }

    /**
     * Deletes an existing InvMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $module)
    {
	$userId = Yii::$app->user->id;
	$model = InvMenu::find()->where('menu_id=:id AND gid=:gid AND created_by=:created_by', [':id'=>$id, ':gid'=>$module, ':created_by'=>$userId])->one();
	
	if(!$model){
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> SDHtml::getMsgError() . Yii::t('app', 'คุณไม่มีสิทร์ลบข้อมูลนี้'),
		'options'=>['class'=>'alert-danger']
	    ]);
	    return $this->redirect(['/inv/inv-person/index', 'module'=>$module]);
	}
	
	if ($model->delete()) {
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		'options'=>['class'=>'alert-success']
	    ]);
	    
	    return $this->redirect(['/inv/inv-person/index', 'module'=>$module]);
	    
	} else {
	    Yii::$app->getSession()->setFlash('alert', [
		'body'=> SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		'options'=>['class'=>'alert-danger']
	    ]);
	    
	    return $this->redirect(['/inv/inv-menu/view', 'module'=>$module, 'id'=>$id]);
	}
	
	return $this->redirect(['/inv/inv-menu/view', 'module'=>$module, 'id'=>$id]);
    }

    /**
     * Finds the InvMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InvMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
	
        if (($model = InvMenu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
