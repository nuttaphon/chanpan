<?php
namespace backend\modules\inv\classes;

use Yii;
use yii\data\SqlDataProvider;
use backend\modules\ezforms\models\Ezform;
use backend\modules\inv\models\InvGen;

/**
 * OvccaQuery class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 9 ก.พ. 2559 12:38:14
 * @link http://www.appxq.com/
 * @example 
 */
class InvQuery {
    public static function getFilterSub($sitecode, $comp, $module, $modelFilter=[]) {
	$userId = Yii::$app->user->id;
	$sql_main_fields = Yii::$app->session['sql_main_fields'];
	$sqlx = "(SELECT CONCAT(' (', COUNT(*), ')') AS num FROM inv_sub_list INNER JOIN {$sql_main_fields['ezf_table']} ON {$sql_main_fields['ezf_table']}.id = inv_sub_list.person_id WHERE inv_sub_list.sub_id = inv_filter_sub.sub_id)";
	$sqlx2 = '0';
//	if(isset($modelFilter) && !empty($modelFilter)){
//	    if($modelFilter['filter_order']==1){
//		$condition = \appxq\sdii\utils\SDUtility::string2Array($modelFilter['options']);
//		$sqlAddon = '';
//		
//		if(isset($condition) && is_array($condition) && !empty($condition)){
//		    $formCond = isset($condition['form'])?$condition['form']:[];
//		    $valueCond = isset($condition['value'])?$condition['value']:[];
//		    $fieldCond = isset($condition['field'])?$condition['field']:[];
//		    $condCond = isset($condition['cond'])?$condition['cond']:[];
//
//		    $innerJoin = '';
//		    $addWhere = '';
//		    $formCondUnique = array_unique($formCond);
//
//		    $pk = $sql_main_fields['pk_field'];
//		    $pkJoin = 'target';
//		    if($sql_main_fields['special']==1){
//			$pk = 'ptid';
//			$pkJoin = 'ptid';
//		    }
//		    $ezformTmp = [];
//		    //innerJoin
//		    foreach ($formCondUnique as $key_cond => $value_cond) {
//
//			$ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value_cond);
//			$ezformTmp[$ezform['ezf_id']] = $ezform['ezf_table'];
//			if($ezform['ezf_table']<>$sql_main_fields['ezf_table']){
//			    $innerJoin .= "INNER JOIN {$ezform['ezf_table']} ON {$ezform['ezf_table']}.$pkJoin = {$sql_main_fields['ezf_table']}.$pk";
//			}
//		    }
//		    //Where
//		    $condType = 'AND';
//		    $paramWhere = [];
//		    foreach ($formCond as $key_cond => $value_cond) {
//			$addWhere .= $condType . " {$ezformTmp[$value_cond]}.{$fieldCond[$key_cond]} = :cond_$key_cond ";
//			$paramWhere[":cond_$key_cond"] = $valueCond[$key_cond];
//			$condType = $condCond[$key_cond];
//		    }
//
//		    $sitecodeWhere = '';
//		    if($sql_main_fields['special']==1){
//			$sitecodeWhere = "AND {$sql_main_fields['ezf_table']}.hsitecode = '$sitecode' ";
//		    } 
//
//		    $sqlAddon = "SELECT COUNT(*) AS num 
//			    FROM {$sql_main_fields['ezf_table']} $innerJoin
//			    WHERE {$sql_main_fields['ezf_table']}.rstat<>3 $sitecodeWhere $addWhere
//			    ";
//
//		    $sqlx2 = '('.Yii::$app->db->createCommand($sqlAddon, $paramWhere)->rawSql.')';
//		    
//		}
//	    } 
//	} 
	
	$sql = "SELECT inv_filter_sub.sub_id, 
		    inv_filter_sub.urine_status, 
		    inv_filter_sub.filter_id, 
		    inv_filter_sub.sitecode, 
		    inv_filter_sub.created_by,
		    CONCAT(inv_filter_sub.sub_name, IF(filter_order=0, $sqlx, '')) AS sub_name
	    FROM inv_filter_sub
	    WHERE ((inv_filter_sub.sitecode = :sitecode AND inv_filter_sub.filter_order = 0) OR (inv_filter_sub.sitecode > 0 AND inv_filter_sub.filter_order = 1))   AND inv_filter_sub.gid = :module AND inv_filter_sub.filter_id = :comp AND (created_by=:created_by || public=1 || $userId IN (inv_filter_sub.`share`) )";

	return Yii::$app->db->createCommand($sql, [':module'=>$module, ':comp'=>$comp, ':created_by'=>$userId, ':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getFilterSubNotSafe($sub_id, $sitecode, $comp, $module) {
	$userId = Yii::$app->user->id;
	$sql_main_fields = Yii::$app->session['sql_main_fields'];
	
	$sql = "SELECT inv_filter_sub.sub_id, 
			inv_filter_sub.urine_status, 
			inv_filter_sub.filter_id, 
			inv_filter_sub.sitecode, 
			inv_filter_sub.created_by,
			CONCAT(inv_filter_sub.sub_name, ' (', (SELECT COUNT(*) AS num FROM inv_sub_list INNER JOIN {$sql_main_fields['ezf_table']} ON {$sql_main_fields['ezf_table']}.id = inv_sub_list.person_id WHERE inv_sub_list.sub_id = inv_filter_sub.sub_id),')') AS sub_name
		FROM inv_filter_sub
		WHERE filter_order=0 AND sub_id<>:sub_id AND inv_filter_sub.gid = :module AND inv_filter_sub.filter_id = :comp AND (created_by=:created_by || public=1 || $userId IN (inv_filter_sub.`share`) )";
	return Yii::$app->db->createCommand($sql, [':module'=>$module, ':sub_id'=>$sub_id, ':comp'=>$comp, ':created_by'=>$userId])->queryAll();
    }
    
    public static function getSeclectList($sub) {
	$sql = "SELECT inv_sub_list.person_id
		FROM inv_filter_sub INNER JOIN inv_sub_list ON inv_filter_sub.sub_id = inv_sub_list.sub_id
		where inv_sub_list.sub_id=:sub";
	return Yii::$app->db->createCommand($sql, [':sub'=>$sub])->queryColumn();
    }
    
    public static function getTbData($sitecode, $comp, $ovfilter_sub, $select=null) {
	$modelFields = Yii::$app->session['sql_main_fields'];
	
	$sql_col = ["{$modelFields['ezf_table']}.*"];
	
	$fields = Yii::$app->session['sql_fields'];
	if(isset($fields) && !empty($fields)){
	    foreach ($fields as $key => $value) {
		$arry = \backend\modules\inv\classes\InvFunc::createSqlReport($value, $modelFields);
		$sql_col = array_merge($sql_col, $arry);
	    }
	} else {
	    $main_forms = \appxq\sdii\utils\SDUtility::string2Array($modelFields['enable_form']);
	    $form = isset($main_forms['form'])?$main_forms['form']:[];
	    $result = isset($main_forms['result'])?$main_forms['result']:[];
	    $label = isset($main_forms['label'])?$main_forms['label']:[];
	    $width = isset($main_forms['width'])?$main_forms['width']:[];
	    $condition = isset($main_forms['condition'])?$main_forms['condition']:[];
	    
	    foreach ($form as $key => $value) {
		$ezform = \backend\modules\inv\classes\InvQuery::getEzformById($value);

		$form_fix = [
		    'ezf_id' => $value,
		    'ezf_name' => $ezform['ezf_name'],
		    'ezf_table' => $ezform['ezf_table'],
		    'comp_id_target' => $ezform['comp_id_target'],
		    'result_field' => $result[$key],
		    'sql_id_name' => 'id_'.$value,
		    'sql_idsubmit_name' => 'idsubmit_'.$value,
		    'sql_result_name' => 'result_'.$value,
		    'value_options' => \appxq\sdii\utils\SDUtility::array2String($condition[$value]),
		    'header' => $label[$key],
		    'width' => $width[$key],

		];
		
		$arry = \backend\modules\inv\classes\InvFunc::createSqlReport($form_fix, $modelFields);
		$sql_col = array_merge($sql_col, $arry);
	    }
	}
	
	$query = new \yii\db\Query();
	$query->select($sql_col)
	    ->from($modelFields['ezf_table'])
	    ->where("{$modelFields['ezf_table']}.rstat<>3");
	
	if($modelFields['special']==1){
	    $query->andWhere("{$modelFields['ezf_table']}.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
	} 
	    
	if(isset($modelFields['order_field']) && $modelFields['order_field']!=''){
	    $query->orderBy("{$modelFields['order_field']} {$modelFields['label_field']}");
	}
		
	if(isset($select)){
	    $query->andWhere("{$modelFields['ezf_table']}.id IN ($select)");
	}
	
	if($ovfilter_sub>0){
	    $query->innerJoin('inv_sub_list', "inv_sub_list.person_id = {$modelFields['ezf_table']}.id");
	    $query->andWhere("inv_sub_list.sub_id = :sub_id", [':sub_id'=>$ovfilter_sub]);
	}
	
	return $query->all();
    }
    
    public static function getEzform() {
	
	$model = Ezform::find()
                ->select('ezform.*')
                //->innerJoin('ezform_favorite', '`ezform`.`ezf_id` = `ezform_favorite`.`ezf_id`')
                ->where('ezform.`status` <> :status ', [':status' => 3])
                ->orderBy('ezform.ezf_name')->all();
	//,`ezform_favorite`.userid,`ezform_favorite`.status AS favorite_status, `ezform_favorite`.forder AS favorite_forder
//	$model = Ezform::find()
//                //->select('ezform.*,`ezform_favorite`.userid,`ezform_favorite`.status AS favorite_status, `ezform_favorite`.forder AS favorite_forder')
//                //->innerJoin('ezform_favorite', '`ezform`.`ezf_id` = `ezform_favorite`.`ezf_id`')
//                ->where('ezform.`status` <> :status ', [':status' => 3])//AND ezform_favorite.userid = :userid / , ':userid' => Yii::$app->user->id
//                //->orderBy('`ezform_favorite`.forder DESC')
//		->all();
	
	return $model;
    }
    
    public static function getEzformByCom($comid) {
	$sql = "SELECT ezform.ezf_id as id,
		ezform.ezf_name as name
		FROM ezform 
		INNER JOIN ezform_component ON ezform_component.comp_id = ezform.comp_id_target and ezform_component.ezf_id <> ezform.ezf_id
		WHERE ezform.`status` <> 3 AND ezform.comp_id_target = :comid
		
		";
	return Yii::$app->db->createCommand($sql, [':comid'=>$comid])->queryAll();
    }
    
    public static function getEzformByOwn($comid) {
	$useId = Yii::$app->user->id;
	$sql = "SELECT ezform.ezf_id as id,
		ezform.ezf_name as name
		FROM ezform 
		INNER JOIN ezform_component ON ezform_component.comp_id = ezform.comp_id_target and ezform_component.ezf_id <> ezform.ezf_id
		WHERE ezform.`status` <> 3 AND ezform.user_create = :userid AND ezform.comp_id_target = :comid
		
		";
	return Yii::$app->db->createCommand($sql, [':comid'=>$comid, ':userid'=>$useId])->queryAll();
    }
    
    public static function getEzformByFav($comid) {
	$useId = Yii::$app->user->id;
	$sql = "SELECT ezform.ezf_id as id,
		ezform.ezf_name as name
		FROM ezform 
		INNER JOIN ezform_component ON ezform_component.comp_id = ezform.comp_id_target and ezform_component.ezf_id <> ezform.ezf_id
		INNER JOIN ezform_favorite ON ezform_favorite.ezf_id = ezform.ezf_id
		WHERE ezform.`status` <> 3 AND ezform_favorite.userid = :userid AND ezform.user_create <> :userid AND ezform.comp_id_target = :comid
		
		";
	return Yii::$app->db->createCommand($sql, [':comid'=>$comid, ':userid'=>$useId])->queryAll();
    }
    
    public static function getEzformByAssign($comid, $sitecode) {
	$useId = Yii::$app->user->id;
	
	$sql = "SELECT  ezform.ezf_id as id,
		ezform.ezf_name as name
		FROM ezform 
		INNER JOIN ezform_component ON ezform_component.comp_id = ezform.comp_id_target and ezform_component.ezf_id <> ezform.ezf_id
		left JOIN user_profile ON user_profile.user_id = ezform.user_create
		WHERE ezform.`status` <> 3  AND (ezform.ezf_id not in (select distinct ezf_id from ezform_favorite where ezf_id=ezform.ezf_id and userid=:userid)) AND ezform.user_create<>:userid AND ezform.comp_id_target = :comid AND (ezform.shared=1 || (ezform.shared=3 AND user_profile.sitecode=:sitecode) || (ezform.shared=2 AND FIND_IN_SET(:userid, ezform.assign)>0 ))
		
		";
	//\appxq\sdii\utils\VarDumper::dump(Yii::$app->db->createCommand($sql, [':comid'=>$comid, ':userid'=>$useId, ':sitecode'=>$sitecode])->rawSql);
	return Yii::$app->db->createCommand($sql, [':comid'=>$comid, ':userid'=>$useId, ':sitecode'=>$sitecode])->queryAll();
    }
    
    public static function getInvFields($sub_id) {
	
	$model = \backend\modules\inv\models\InvFields::find()->where('sub_id=:sub_id',[':sub_id'=>$sub_id])->all();
	
	return $model;
    }
    public static function getCompAll() {
//	SELECT
//		ezform.ezf_id, 
//		ezform.ezf_name,
//		ezform.comp_id_target,
//		IFNULL(ezform_component.special,0) AS special,
//		IFNULL(ezform_fields.ezf_field_name, 'id') AS ezf_field_name
//		FROM ezform LEFT JOIN ezform_component ON ezform.comp_id_target = ezform_component.comp_id
//			 LEFT JOIN ezform_fields ON ezform_fields.ezf_id = ezform_component.ezf_id AND ezform_fields.ezf_field_id = ezform_component.field_id_key
//		WHERE ezform.`status` <> 3 AND ezform.user_create = :userid
//		GROUP BY ezform.ezf_id
//UNION ALL	
	$sql = "	
SELECT 
		ezform.ezf_id, 
		ezform.ezf_name,
		ezform.comp_id_target,
		IFNULL(ezform_component.special,0) AS special,
		IFNULL(ezform_fields.ezf_field_name, 'id') AS ezf_field_name
		FROM ezform INNER JOIN ezform_component ON ezform.comp_id_target = ezform_component.comp_id
			 LEFT JOIN ezform_fields ON ezform_fields.ezf_id = ezform_component.ezf_id AND ezform_fields.ezf_field_id = ezform_component.field_id_key
		WHERE ezform.`status` <> 3 
		GROUP BY ezform.ezf_id
		
		";
	//INNER JOIN ezform_favorite ON ezform_favorite.ezf_id = ezform.ezf_id
	//AND ezform_favorite.userid = :userid
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    public static function getComp() {
//	SELECT
//		ezform.ezf_id, 
//		ezform.ezf_name,
//		ezform.comp_id_target,
//		IFNULL(ezform_component.special,0) AS special,
//		IFNULL(ezform_fields.ezf_field_name, 'id') AS ezf_field_name
//		FROM ezform LEFT JOIN ezform_component ON ezform.comp_id_target = ezform_component.comp_id
//			 LEFT JOIN ezform_fields ON ezform_fields.ezf_id = ezform_component.ezf_id AND ezform_fields.ezf_field_id = ezform_component.field_id_key
//		WHERE ezform.`status` <> 3 AND ezform.user_create = :userid
//		GROUP BY ezform.ezf_id
//UNION ALL	
	$sql = "	
SELECT 
		ezform.ezf_id, 
		ezform.ezf_name,
		ezform.comp_id_target,
		IFNULL(ezform_component.special,0) AS special,
		IFNULL(ezform_fields.ezf_field_name, 'id') AS ezf_field_name
		FROM ezform INNER JOIN ezform_component ON ezform.comp_id_target = ezform_component.comp_id
			 INNER JOIN ezform_favorite ON ezform_favorite.ezf_id = ezform.ezf_id
			 LEFT JOIN ezform_fields ON ezform_fields.ezf_id = ezform_component.ezf_id AND ezform_fields.ezf_field_id = ezform_component.field_id_key
		WHERE ezform.`status` <> 3 AND ezform_favorite.userid = :userid
		GROUP BY ezform.ezf_id
		
		";
	//INNER JOIN ezform_favorite ON ezform_favorite.ezf_id = ezform.ezf_id
	//AND ezform_favorite.userid = :userid
	return Yii::$app->db->createCommand($sql, [':userid'=>  Yii::$app->user->id])->queryAll();
    }
    
    public static function getCompById($id) {
	
	$sql = "SELECT `ezform_component`.*, ezform.ezf_id, ezform.ezf_name FROM `ezform_component` INNER JOIN `ezform` ON ezform_component.ezf_id = ezform.ezf_id WHERE ezform_component.special=1 AND ezform.ezf_id = :id
		";
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryAll();
    }
    
    public static function getFields($id) {
	
	$sql = "SELECT `ezf_field_name` AS id, IF(ezf_field_label='',ezf_field_name, IFNULL(ezf_field_label,ezf_field_name)) AS name, ezf_field_label, ezf_field_type, ezf_field_sub_id  FROM `ezform_fields` WHERE (ezf_field_type NOT IN (0,2,3,13,15,23,24,25,26,30) or (ezf_field_type=0 and ezf_field_sub_id is not null)) AND `ezf_id` = :id ORDER BY ezf_field_order";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryAll();
    }
    
    public static function getEzformById($id) {
	
	$sql = "SELECT ezf_id, ezf_name, ezf_table, comp_id_target, field_detail, unique_record FROM ezform WHERE ezf_id = :id";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryOne();
    }
    
    public static function getModule($module, $userId) {
	
	$model = InvGen::find()->where("active=1 AND gid=:module AND (created_by=:created_by || public=1 || $userId IN (`share`) )", [':module'=>$module, ':created_by'=>$userId])->one();
	
	return $model;
    }
    
    public static function getModuleList($userId) {
	$query1 = (new \yii\db\Query())
	    ->select("inv_gen.*")
	    ->from('inv_gen')
	    ->innerJoin('inv_favorite', 'inv_favorite.gid=inv_gen.gid')
	    ->where("gsystem=0 AND active=1 AND inv_favorite.user_id=:user_id", [':user_id'=>$userId]);
	
	$model = InvGen::find()->union($query1)
		->where("gsystem=0 AND active=1 AND (created_by=:created_by  )", [':created_by'=>$userId])//|| $userId IN (`share`)
		->groupBy('gname')
		->all();
	
	return $model;
    }
    
    public static function getModuleListSys() {
	
	$model = InvGen::find()
		->where("gtype=1 AND gsystem=1 AND active=1 AND approved=1")
		->groupBy('gname')
		->all();
	
	return $model;
    }
    
    
    public static function getModuleListMy($userId, $gid) {
	
	$model = InvGen::find()
		->where("active=1 AND public=1 AND approved=1 AND created_by=:created_by AND gid<>:gid", [':created_by'=>$userId, ':gid'=>$gid])
		->groupBy('gname')
		->all();
	
	return $model;
    }
    
    
    public static function getModuleListNotme($userId) {
	
	$model = InvGen::find()
		->innerJoin('inv_favorite', 'inv_favorite.gid=inv_gen.gid AND inv_favorite.user_id=:user_id', [':user_id'=>$userId])
		->where("gsystem=0 AND active=1 AND public=1 AND approved=1")
		->all();
	
	return $model;
    }
    
    public static function getModulePublic($userId) {
	
	$model = InvGen::find()
		->where("gsystem=0 AND active=1 AND public=1 AND approved=1 AND (created_by<>:created_by AND $userId NOT IN (`share`) )", [':created_by'=>$userId])
		->all();
	
	return $model;
    }
    
    public static function getModuleAssign($userId) {
	
	$model = InvGen::find()
		->where("gsystem=0 AND active=1 AND (created_by<>:created_by AND $userId IN (`share`) )", [':created_by'=>$userId])
		->all();
	
	return $model;
    }
    
    public static function getModuleListFavorite($userId) {
	$query = (new \yii\db\Query())
	    ->select("inv_favorite.*")
	    ->from('inv_favorite')
	    ->where('inv_favorite.user_id=:user_id', [':user_id'=>$userId])
	    ->all();
	
	return $query;
    }
    
    public static function getProvince() {
	
	$sql = "SELECT PROVINCE_CODE as id, PROVINCE_NAME as name FROM const_province";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getAmphur() {
	
	$sql = "SELECT AMPHUR_CODE as id, AMPHUR_NAME as name FROM const_amphur";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getDistrict() {
	
	$sql = "SELECT DISTRICT_CODE as id, DISTRICT_NAME as name FROM const_district";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getProvinceSelect($select) {
	if($select==''){
	    return false;
	}
	$sql = "SELECT PROVINCE_CODE as id, PROVINCE_NAME as name FROM const_province WHERE PROVINCE_CODE IN ($select)";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getAmphurSelect($select) {
	if($select==''){
	    return false;
	}
	$sql = "SELECT AMPHUR_CODE as id, AMPHUR_NAME as name FROM const_amphur WHERE AMPHUR_CODE IN ($select)";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getDistrictSelect($select) {
	if($select==''){
	    return false;
	}
	$sql = "SELECT DISTRICT_CODE as id, DISTRICT_NAME as name FROM const_district WHERE DISTRICT_CODE IN ($select)";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getFormsSelect($select) {
	if($select==''){
	    return false;
	}
	$sql = "SELECT ezf_id, ezf_name, ezf_table, comp_id_target, field_detail, unique_record FROM ezform WHERE ezf_id IN ($select)";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getHospitalSelect($select) {
	if($select==''){
	    return false;
	}
	$sql = "SELECT hcode as id, name as name FROM all_hospital_thai WHERE hcode IN ($select)";
	
	return Yii::$app->db->createCommand($sql)->queryAll();
    }
    
    public static function getFieldsEzf($ezf_id, $ezf_field_name) {
	
	$sql = "SELECT ezf_field_id, ezf_field_name, ezf_field_label, ezf_field_sub_id, ezf_field_sub_textvalue, ezf_field_default, ezf_field_group, ezf_field_type, ezf_default_choice FROM ezform_fields WHERE ezf_field_type NOT IN (0,2,3,13,15,23,24,25,26,30) AND ezf_id=:ezf_id AND ezf_field_name = :ezf_field_name";
	
	return Yii::$app->db->createCommand($sql, [':ezf_id'=>$ezf_id, ':ezf_field_name'=>$ezf_field_name])->queryOne();
    }
    
    public static function getFieldsSelect($ezf_id, $select) {
	if($select==''){
	    return false;
	}
	$sql = "SELECT ezf_field_id, ezf_field_name, ezf_field_label, ezf_field_sub_id, ezf_field_sub_textvalue, ezf_field_default, ezf_field_group, ezf_field_type, ezf_default_choice FROM ezform_fields WHERE ezf_field_type NOT IN (0,2,3,13,15,23,24,25,26,30) AND ezf_id=:ezf_id AND ezf_field_id IN ($select)";
	
	return Yii::$app->db->createCommand($sql, [':ezf_id'=>$ezf_id])->queryAll();
    }
    
    public static function getFieldsId($id) {
	
	$sql = "SELECT `ezf_field_id` AS id, ezf_field_name, IF(ezf_field_label='',ezf_field_name, IFNULL(ezf_field_label,ezf_field_name)) AS name, ezf_field_label, ezf_field_type, ezf_field_sub_id  FROM `ezform_fields` WHERE ezf_field_type NOT IN (0,13, 2, 3, 7, 8, 9, 10, 14, 15, 18, 23, 24, 25, 26) AND `ezf_id` = :id";
	
	return Yii::$app->db->createCommand($sql, [':id'=>$id])->queryAll();
    }
    
    public static function getFieldsItems($ezf_table, $fName, $inv_main, $sitecode) {
	$paramStr = '';
	$paramArry = [];
	$main_table = $inv_main['ezf_table'];
	$pk = $inv_main['pk_field'];
	$pkJoin = 'target';
	if($inv_main['special']==1){
	    $pk = 'ptid';
	    $pkJoin = 'ptid';
	}
	
	if($inv_main['special']==1){
	    $paramStr .= " AND $ezf_table.hsitecode = :hsitecode ";
	    $paramArry[':hsitecode'] = $sitecode;
	} 
	
	
	//$sql = "SELECT $ezf_table.$fName, count(*) AS row_num FROM $main_table INNER JOIN $ezf_table ON $ezf_table.$pkJoin = $main_table.$pk  WHERE $ezf_table.rstat NOT IN(3,0) $paramStr Group BY $ezf_table.$fName ORDER BY $ezf_table.$fName ";//DESC
	$sql = "SELECT $ezf_table.$fName, count(*) AS row_num FROM $ezf_table WHERE $ezf_table.rstat NOT IN(3,0) $paramStr Group BY $ezf_table.$fName ORDER BY $ezf_table.$fName ";
	
	return Yii::$app->db->createCommand($sql, $paramArry)->queryAll();
    }
    
    public static function getFieldsItemsCount($ezf_table, $fName, $inv_main, $sitecode) {
	$paramStr = '';
	$paramArry = [];
	$main_table = $inv_main['ezf_table'];
	$pk = $inv_main['pk_field'];
	$pkJoin = 'target';
	if($inv_main['special']==1){
	    $pk = 'ptid';
	    $pkJoin = 'ptid';
	}
	
	if($inv_main['special']==1){
	    $paramStr .= " AND $ezf_table.hsitecode = :hsitecode ";
	    $paramArry[':hsitecode'] = $sitecode;
	} 
	
	
	//$sql = "SELECT count(*) AS row_num FROM $main_table INNER JOIN $ezf_table ON $ezf_table.$pkJoin = $main_table.$pk WHERE $ezf_table.rstat NOT IN(3,0) $paramStr";
	$sql = "SELECT count(*) AS row_num FROM $ezf_table WHERE $ezf_table.rstat NOT IN(3,0) $paramStr";
	
	return Yii::$app->db->createCommand($sql, $paramArry)->queryScalar();
    }
    
    public static function getPkJoin($comid) {
	$sql = "SELECT ezform_fields.ezf_field_name
	    FROM ezform_fields INNER JOIN ezform_component ON ezform_fields.ezf_id = ezform_component.ezf_id AND ezform_fields.ezf_field_id = ezform_component.field_id_key
	    WHERE ezform_component.comp_id = :comid
		";
	return Yii::$app->db->createCommand($sql, [':comid'=>$comid])->queryScalar();
    }
    
    public static function createSqlWeb($value, $modelFields) {
	$sql_col = [];
	
	    $result = isset($value['result_field'])?$value['result_field']:[];

	    $sql_id_all = $value['sql_id_all'];
	    $sql_id_name = $value['sql_id_name'];
	    $sql_idsubmit_name = $value['sql_idsubmit_name'];
	    $sql_result_name = $value['sql_result_name'];
	    
	    $pk = $modelFields['pk_field'];
	    $pkJoin = 'target';
	    if($modelFields['special']==1){
		$pk = 'ptid';
		$pkJoin = 'ptid';
	    }
	    
//	    if($value['field_detail']!=''){
//		$sql_id_detail = 'detail_'.$value['ezf_id'];
//		$sql_col[] = "SELECT CONCAT({$value['field_detail']}) AS $sql_id_detail FROM {$value['ezf_table']} et";
//	    }
	     
	    $sql_col[] = "(SELECT group_concat(et.id) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_id_all";
	    $sql_col[] = "(SELECT group_concat(et.id) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat=1 GROUP BY et.$pkJoin) AS $sql_id_name";
	    $sql_col[] = "(SELECT group_concat(et.id) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat=2 GROUP BY et.$pkJoin) AS $sql_idsubmit_name";
	    
	    $display = SDUtility::string2Array($value['display_options']);
	    
	    if($value['show']=='detail'){
		
		if(isset($value['field_detail']) && $value['field_detail']!=''){
		    $sql_col[] = "(SELECT group_concat(CONCAT({$value['field_detail']})) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
		}
			
	    } elseif ($value['show']=='condition') {
		if(isset($display) && !empty($display)){
		    $dcondition = isset($display['condition'])?$display['condition']:[];
		    $dvalue1 = isset($display['value1'])?$display['value1']:[];
		    $dvalue2 = isset($display['value2'])?$display['value2']:[];
		    $dicon = isset($display['icon'])?$display['icon']:[];
		    $dlabel = isset($display['label'])?$display['label']:[];
		    
		    $sql_col[] = "(SELECT group_concat(et.{$value['result']}) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
                    
		}
	    } elseif ($value['show']=='custom') {
		if(isset($display) && !empty($display)){
		    if(isset($display['custom']) && $display['custom']!=''){
			$sql_col[] = "(SELECT group_concat(CONCAT({$display['custom']})) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
		    }
		}
	    }
	    
	    
	    $condition = SDUtility::string2Array($value['value_options']);
	    
	    if(isset($condition) && !empty($condition)){
		$cform = $condition['form'];
		$cfield = $condition['field'];
		
		foreach ($cform as $ckey => $cvalue) {
		    $sql_result ='result_'.$cvalue.'_'.$cfield[$ckey].'_'.$ckey;
		    $ezform = \backend\modules\inv\classes\InvQuery::getEzformById($cvalue);
		    
		    $sql_col[] = "(SELECT group_concat(et.{$cfield[$ckey]}) FROM {$ezform['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>0 AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result";
		}
		
	    }
//	    $result = trim($result);
//	    if($result!=''){
//		$sql_col[] = "(SELECT group_concat(et.$result) FROM {$value['ezf_table']} et WHERE et.$pkJoin = {$modelFields['ezf_table']}.$pk AND et.rstat<>3 GROUP BY et.$pkJoin) AS $sql_result_name";
//	    }
	
	
	return $sql_col;
    }
    
}
