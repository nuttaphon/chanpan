 <?php
namespace backend\modules\comments\assets;

use yii\web\AssetBundle;
 
class BootstrapAsset extends AssetBundle {

    public $sourcePath='@backend/modules/comments/assets/bootstrap';
    
    public $css = [
	 
		'css/bootstrap.css' 
       
    ];
    public $js = [
		'js/bootstrap.js' 
         
	 
    ];
    public $depends = [
		'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
