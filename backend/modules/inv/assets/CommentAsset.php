<?php
 

namespace backend\modules\comments\assets;

use yii\web\AssetBundle;
 
class CommentAsset extends AssetBundle {

    public $sourcePath='@backend/modules/comments/assets';
    
    public $css = [
	 
		'css/rating.css',
        'css/style.css',
        'css/normalize.min.css'
    ];
    public $js = [
		'js/rating.js',
        'js/index.js',
		'js/highchart.js'
	 
    ];
    public $depends = [
		'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
