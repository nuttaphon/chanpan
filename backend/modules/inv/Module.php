<?php

namespace backend\modules\inv;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\inv\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
