<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMenu */
$labelHome = isset($modelGen['gname'])?$modelGen['gname']:'Module';
$this->title = Yii::t('app', 'สร้างเมนูใหม่');
$this->params['breadcrumbs'][] = ['label' => $labelHome, 'url' => ['/inv/inv-person/index', 'module' => $module]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="inv-menu-create">

    <?= $this->render('_form', [
        'model' => $model,
	'module'=>$module,
	'modelmainmenu' => $modelmainmenu,
        'modelmenu' => $modelmenu,  
    ]) ?>

</div>
