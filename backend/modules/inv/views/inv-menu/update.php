<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMenu */
$labelHome = isset($modelGen['gname'])?$modelGen['gname']:'Module';
$this->title = Yii::t('app', 'แก้ไขเมนู') . '#' . $model->menu_id;
$this->params['breadcrumbs'][] = ['label' => $labelHome, 'url' => ['/inv/inv-person/index', 'module' => $module]];
$this->params['breadcrumbs'][] = Yii::t('app', 'แก้ไขเมนู');
?>

<div class="inv-menu-update">

    <?= $this->render('_form', [
        'model' => $model,
	'module'=>$module,
	'modelmainmenu' => $modelmainmenu,
        'modelmenu' => $modelmenu,  
    ]) ?>

</div>
