<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMenu */
/* @var $form yii\bootstrap\ActiveForm */
?>
<?php
if($module>0){
    echo $this->render('/inv-person/_menu', ['module'=>$module, 'id'=>$model->menu_id]);
}
?>
<div class="inv-menu-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>
    <?php
    if ($model->menu_order == "") $model->menu_order=10000;

    $orderitems = ArrayHelper::merge(ArrayHelper::map($modelmenu, 'menu_order', 'menu_name'), [10000=>'ท้ายสุด']);
    
    foreach ($modelmenu as $key => $menu) {
        if ($menu->menu_parent != 0) $orderitems[$menu->menu_order]="+++++".$menu->menu_name;
    }

    $submenuitems = ArrayHelper::merge([0=>"เป็นเมนูหลัก"],ArrayHelper::map($modelmainmenu, 'menu_id', 'menu_name'));
    ?>
    <div class="modal-body">
	<div class="row">
		<div class="col-md-4 "><?= $form->field($model, 'menu_name')->textInput(['maxlength' => true]) ?></div>
		<div class="col-md-4 sdbox-col"><?= $form->field($model, 'menu_parent')->dropDownList($submenuitems) ?></div>
		<div class="col-md-4 sdbox-col"><?= $form->field($model, 'menu_order')->dropDownList($orderitems) ?></div>
	</div>

	<?= $form->field($model, 'menu_content')->widget(dosamigos\tinymce\TinyMce::className(),[
		'options' => ['rows' => 20],
		'language' => 'th_TH',
		'clientOptions' => [
			'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
			'plugins' => [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor colorpicker textpattern ",
			],
			'setup' => new yii\web\JsExpression("function (editor) {
			    editor.addButton('iframe', {
			      text: 'iFrame',
			      icon: false,
			      onclick: function () {
				editor.insertContent('<iframe src=\"https://www.tinymce.com\" width=\"1024\" height=\"600\" align=\"center\"> </iframe>');
			      }
			    });
			  }"),
			'extended_valid_elements'=>"iframe[src|frameborder|style|scrolling|class|width|height|name|align]",
			'toolbar' => "iframe | undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
			'content_css' => Yii::getAlias('@backendUrl').'/css/bootstrap.min.css',
			'image_advtab' => true,
			'filemanager_crossdomain' => true,
			'external_filemanager_path' => Yii::getAlias('@storageUrl').'/filemanager/',
			'filemanager_title' => 'Responsive Filemanager',
			'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl').'/filemanager/plugin.min.js')
		]
	]) ?>

	<?= $form->field($model, 'menu_active')->hiddenInput()->label(FALSE) ?>
	
	<?= $form->field($model, 'created_by')->hiddenInput()->label(FALSE) ?>

	<?= $form->field($model, 'created_at')->hiddenInput()->label(FALSE) ?>
	
	<?= $form->field($model, 'gid')->hiddenInput()->label(FALSE) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#inv-menu-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-inv-menu').modal('hide');
		$.pjax.reload({container:'#inv-menu-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>