<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\inv\models\InvGenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'จัดการโมดูล');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="inv-gen-index">

    <?php  Pjax::begin(['id'=>'inv-gen-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'inv-gen-grid',
	'panelBtn' => Html::a(SDHtml::getBtnAdd().' Module Maker', Url::to(['/inv/inv-person/proxy']), ['class' => 'btn btn-success btn-sm']),
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
        'columns' => [
	    
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            'gname',
            //'gdetail:ntext',
	    
            [
		'attribute' => 'ezf_name',
		
		'contentOptions' => ['style'=>'width:180px;'],
	    ],
	    [
		'attribute' => 'special',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:100px;text-align: center;'],
	    ],
	    [
		'attribute' => 'public',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:100px;text-align: center;'],
	    ],
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:150px;text-align: center;'],
		'template' => '{update} {delete}',
		'buttons' => [
		    
                    'update' => function ($url, $model) {
			return Html::a('<span class="fa fa-edit"></span> จัดการโมดูล', Url::to(['/inv/inv-person/create-modules', 'module'=>$model->gid]), [
				'title' => Yii::t('app', 'จัดการโมดูล'),
				'class'=>'btn btn-warning btn-xs',
				'data-action'=>'update',
			]);
                    },
                    'delete' => function ($url, $model) {
			return Html::a('<span class="fa fa-trash"></span> ลบ', $url, [
				 'title' => Yii::t('app', 'ลบ'),
				 'class'=>'btn btn-danger btn-xs',
				 'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
				 'data-method' => 'post',        
				 'data-action'=>'delete',
			 ]); 
                    },
                ],
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-inv-gen',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("

$('#inv-gen-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update') {
	location.href = url;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-gen-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledInvGenBtn(num) {
    if(num>0) {
	$('#modal-delbtn-inv-gen').attr('disabled', false);
    } else {
	$('#modal-delbtn-inv-gen').attr('disabled', true);
    }
}

function selectionInvGenGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionInvGenIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-gen-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalInvGen(url) {
    $('#modal-inv-gen .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-gen').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>