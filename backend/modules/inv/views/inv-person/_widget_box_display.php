<?php
use yii\helpers\Html;
use yii\helpers\Url;

$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
$indexArry = array_keys($dataSubForm);

?>
<div id="<?=$genid?>" class="display-condition-view" >
    <div class="row">
	<div class="col-md-2"><label>Condition</label></div>
	<div class="col-md-2 sdbox-col"><label>Value1</label></div>
	<div class="col-md-2 sdbox-col"><label>Value2</label></div>
	<div class="col-md-2 sdbox-col"><label>Icon</label></div>
	<div class="col-md-2 sdbox-col"><label>Label</label></div>
    </div>

    <div class="forms-display-condition-items">
	
    </div>

    <div class="row" style="margin-bottom: 15px;">
	<div class="col-md-2"><input type="text" class="form-control" disabled="disabled" value="Condition"></div>
	<div class="col-md-2 sdbox-col"><input type="text" class="form-control" disabled="disabled" value="Value1"></div>
	<div class="col-md-2 sdbox-col"><input type="text" class="form-control" disabled="disabled" value="Value2"></div>
	<div class="col-md-2 sdbox-col"><input type="text" class="form-control" disabled="disabled" value="Icon"></div>
	<div class="col-md-2 sdbox-col"><input type="text" class="form-control" disabled="disabled" value="Label"></div>
	<div class="col-md-2 sdbox-col"><button type="button" data-url="<?=  Url::to(['/inv/inv-person/get-widget', 'view'=>'_widget_form_display'])?>" class="forms-display-add btn btn-success"><i class="glyphicon glyphicon-plus"></i></button></div>
    </div>
</div>

<?php  


$valF = isset($indexArry[0])?$indexArry[0]:0;

$this->registerJs("

");?>