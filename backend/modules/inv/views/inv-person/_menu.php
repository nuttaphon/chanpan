<?php
use Yii;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use backend\modules\inv\models\InvMenu;

$moduleID = '';
$controllerID = '';
$actionID = '';

if (isset(Yii::$app->controller->module->id)) {
	    $moduleID = Yii::$app->controller->module->id;
}
if (isset(Yii::$app->controller->id)) {
	    $controllerID = Yii::$app->controller->id;
}
if (isset(Yii::$app->controller->action->id)) {
	    $actionID = Yii::$app->controller->action->id;
}

?>

<?php
$modelGen = \backend\modules\inv\models\InvGen::find()->where('gid=:gid', [':gid'=>$module])->one();//, ':created_by'=> Yii::$app->user->id
if($modelGen){
    if(($modelGen->public==1 && Yii::$app->user->can('administrator')) || $modelGen['created_by']==Yii::$app->user->id){
	echo Html::a('', ['/inv/inv-person/create-modules', 'module'=>$module], ['class'=>'fa fa-cog fa-2x pull-right',
	    'data-toggle'=>'tooltip',
	    'title'=>'ตั้งค่าโมดูล'
	]);
    }
    
    if($modelGen['created_by']==Yii::$app->user->id){
	if(isset($id) && $id>0){

	    echo Html::a('', ['/inv/inv-menu/delete', 'module'=>$module, 'id'=>$id], ['class'=>'fa fa-trash-o fa-2x pull-right',
		'data-toggle'=>'tooltip',
		'title'=>'ลบเมนู',
		'data' => [
		    'confirm' => "คุณแน่ใจที่จะต้องการลบข้อมูลนี้หรือไม่",
		    'method' => 'post',
		],
	    ]);//'data-confirm'=>'คุณแน่ใจที่จะต้องการลบข้อมูลนี้หรือไม่?'
	    echo Html::a('', ['/inv/inv-menu/update', 'module'=>$module, 'id'=>$id], ['class'=>'fa fa-pencil-square-o fa-2x pull-right',
		'data-toggle'=>'tooltip',
		'title'=>'แก้ไขเมนู']);
	}

	echo Html::a('', ['/inv/inv-menu/create', 'module'=>$module], ['class'=>'fa fa-plus fa-2x pull-right',
		'data-toggle'=>'tooltip',
		'title'=>'สร้างเมนูใหม่'
	    ]);
    }
}
?>

<?php
$labelHome = isset($modelGen['gname'])?$modelGen['gname']:'Module';

$items = [
	[
	    'label' => 'Dashboard',
	    'url' => Url::to(['/inv/inv-fix/index', 'module'=>$module]),
	    'active'=>$controllerID=='inv-fix' && $actionID=='index',
	],
	[
	    'label' => 'Workbench',
	    'url' => Url::to(['/inv/inv-person/index', 'module'=>$module]),
	    'active'=>$controllerID=='inv-person',
	],
	[
	    'label' => 'Report',
	    'url' => Url::to(['/inv/inv-fix/report', 'module'=>$module]),
	    'active'=>$controllerID=='inv-fix' && $actionID=='report',
	],
    ];

$modelmainmenu = InvMenu::find()->where(['gid' => $module, 'menu_parent'=>0])->orderBy('menu_order')->all();
foreach ($modelmainmenu as $key => $value) {
    
    $submenu = InvMenu::find()->where(['gid' => $module, 'menu_parent'=>$value['menu_id']])->orderBy('menu_order')->all();
    if($submenu){
	$subItems = [];
	$subId = [];
	foreach ($submenu as $subKey => $subValue) {
	    $subItems[] = [
		'label' => $subValue['menu_name'],
		'url' => Url::to(['/inv/inv-menu/view', 'module'=>$module, 'id'=>$subValue['menu_id']]),
		'active'=>$controllerID=='inv-menu' && $_GET['id']==$subValue['menu_id'],
	    ];
	    $subId[] = $subValue['menu_id'];
	}
	
	$items[] = [
	    'label' => $value['menu_name'],
	    'url' => '#',
	    'items' => $subItems,
	    'dropDownOptions'=>['id'=>  common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime()],
	    'active'=>$controllerID=='inv-menu' && in_array($_GET['id'], $subId),
	];
    } else {
	$items[] = [
	    'label' => $value['menu_name'],
	    'url' => Url::to(['/inv/inv-menu/view', 'module'=>$module, 'id'=>$value['menu_id']]),
	    'active'=>$controllerID=='inv-menu' && $_GET['id']==$value['menu_id'],
	];
    }
}

?>

<?= \yii\bootstrap\Nav::widget([
    'items' => $items,
    'options' => ['class'=>'nav nav-tabs'],
]);
?>
