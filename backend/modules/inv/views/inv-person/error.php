<?php
use Yii;
use yii\helpers\Html;
/**
 * error file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 ส.ค. 2559 16:02:17
 * @link http://www.appxq.com/
 */
$gname = isset($inv_main['gname'])?$inv_main['gname']:'แสดงตัวอย่างใบกำกับงาน';
$this->title = Yii::t('app', "$gname");
$this->params['breadcrumbs'][] = $this->title;
$ovfilter_sub = isset($ovfilter_sub) && !empty($ovfilter_sub)?$ovfilter_sub:0;
?>
<div class="alert alert-warning" role="alert">
    <i class="glyphicon glyphicon-warning-sign"></i> ไม่สามารถเปิดโมดูล เนื่องจากการตั้งค่าโมดูลไม่ถูกต้องกรุณาตั้งค่าโมดูลใหม่ <?=Html::a('ที่นี่คลิก', ['/inv/inv-person/create-modules', 'module'=>$module])?>
</div>
<?php

if(Yii::$app->user->can('administrator')){
    echo "<code>$msg</code>";
}
?>