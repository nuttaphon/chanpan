<?php

use Yii;
use yii\helpers\Html;
use appxq\sdii\helpers\SDNoty;
use yii\helpers\Url;
\backend\modules\comments\assets\CommentAsset::register($this); 
?>

<div class="inv-form">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">App Info</h4>
    </div>

    <div class="modal-body">
	<?php
	    $linkModule = '';
	    if($model['gtype']==1){
		$linkModule = \yii\helpers\Url::to($model['glink']);
	    } else {
		$linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$model['gid']]);
	    }
	    
	    $linkFav = '<button class="btn btn-success btn-sm  fav-btn" data-gid="'.$model['gid'].'"><i class="glyphicon glyphicon-cloud-download"></i> ยกเลิก</button>';
	    $linkNotFav = '<button class="btn btn-warning btn-sm  fav-btn" data-gid="'.$model['gid'].'"><i class="glyphicon glyphicon-cloud-download"></i> รับ</button>';
	    
	    $gname = Html::encode($model['gname']);
	    
	    $userId = Yii::$app->user->id;
	    $dataFav = backend\modules\inv\classes\InvQuery::getModuleListNotme($userId);
	    $arrayMap = \yii\helpers\ArrayHelper::map($dataFav, 'gid', 'gname');
	    $arrayFav = array_keys($arrayMap);
	    
	    //appxq\sdii\utils\VarDumper::dump($arrayFav,FALSE);   
	?>
	<div class="media"> 
	    <div class="media-left"> 
		<a href="<?=$linkModule?>"> 
		    <img alt="72x72" width="72" height="72" class="media-object img-rounded" src="<?= (isset($model['gicon']) && $model['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$model['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" > 
		</a> 
	    </div> 
	    <div class="media-body"> 
		<h4 class="media-heading"><?=$gname?></h4> 
		<div id="btn-box" class="pull-right">
		    <?php if($model['gsystem']!=1):?>
		    <?php if(in_array($model['gid'], $arrayFav)):?>
		   
		    <?=$linkFav?>
		    <?php else:?>
			<?=$linkNotFav?>
		    <?php endif;?>
		    <?php endif;?>
		</div>
		<span>
		     
		    อัพเดทเมื่อ <?= \common\lib\sdii\components\utils\SDdate::mysql2phpThDateSmall($model['updated_at'])?> 
                    (จำนวนผู้ใช้ <?=number_format($ncount['user']);?> คน จาก <?=number_format($ncount['org']);?> หน่วยงาน) <?php if(isset($pcoc)){ ?> ดูแลผู้ป่วยแล้วทั้งสิ้น <?=number_format($pcoc['npatient']);?> ราย รวม <?=number_format($pcoc['ntime']);?> ครั้ง <?php } ?>
		    
		</span> 
		
		<div class="rating-view" style="color: #ec971f;">
		    <a href="<?=$linkModule?>" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-link"></i> เปิด </a> 
			<i class="glyphicon glyphicon-star"></i>
			<i class="glyphicon glyphicon-star"></i>
			<i class="glyphicon glyphicon-star"></i>
			<i class="glyphicon glyphicon-star-empty"></i>
			<i class="glyphicon glyphicon-star-empty"></i>
		    </div>
		
	    </div> 
	</div>
	<hr>
	<ul id="myTabs" class="nav nav-pills nav-justified">
	    <li role="presentation" class="active"><a href="#page1" id="page1-tab" data-toggle="tab">รายละเอียด</a></li>
	    <li role="presentation"><a href="#page2" id="page2-tab" data-toggle="tab">บทวิจารณ์</a></li>
	</ul>
	<div class="tab-content" id="myTabContent"> 
	    <div class="tab-pane fade in active" role="tabpanel" id="page1" aria-labelledby="page1-tab"> 
		<div class="modal-header">
		    <h4 class="modal-title" id="itemModalLabel">คำอธิบาย</h4>
		</div>
		<div class="modal-body">
		    <?=$model['gdetail']?>
		</div>
		
		<div class="modal-header">
		    <h4 class="modal-title" id="itemModalLabel">ผู้พัฒนา</h4>
		</div>
		<div class="modal-body">
		    <?=$model['gdevby']?>
		</div>
		
		<div class="modal-header">
		    <h4 class="modal-title" id="itemModalLabel">แอพพลิเคชั่นอื่นๆ</h4>
		</div>
		<div class="modal-body">
		    <div class="row">

			<?php

			$dataModules = backend\modules\inv\classes\InvQuery::getModuleListMy($model['created_by'], $model['gid']);

			if($dataModules){
			?>
			<?php foreach ($dataModules as $key => $value):?>
			<div class="col-xs-4 col-md-3" style="margin-bottom: 20px;">
				<div class="media-left">
				    <?php
					$linkModule = '';
					if($value['gtype']==1){
					    $linkModule = \yii\helpers\Url::to($value['glink']);
					} else {
					    $linkModule = \yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$value['gid']]);
					}

					$gname = yii\helpers\Html::encode($value['gname']);
					$checkthai = \backend\modules\ovcca\classes\OvccaFunc::checkthai($gname);
					$len = 12;
					if($checkthai!=''){
					    $len = $len*3;
					}
					if(strlen($gname)>$len){
					    $gname = substr($gname, 0, $len).'...';
					}
				    ?>
				    <a href="<?=$linkModule?>">
					<div style="margin: 4px;"><img src="<?= (isset($value['gicon']) && $value['gicon'] != '')? Yii::getAlias('@backendUrl').'/module_icon/'.$value['gicon']:Yii::getAlias('@backendUrl').'/module_icon/no_icon.png'?>" class="img-rounded" width="72" height="72"></div>
				    </a>
				    <h4 class="media-heading text-center" style="font-size: 13px;"><strong><?= $gname?></strong></h4>
				</div>

			    </div>
			<?php endforeach;?>
			<?php } else {
			    if(count($arrayFav)==0){
				echo '<div class="col-md-4"><cite>ไม่พบโมดูล</cite></div>';
			    }
			}
			?>

		    </div>
		</div>
		
	    </div> 
<div class="tab-pane fade" role="tabpanel" id="page2" aria-labelledby="page2-tab"> 
	 <div class="row">
	    	<?php $arr=[1,2,3,4,5];?> 
	    	<div class="col-md-12">

	    		<div class="col-md-12">
	    			<div style='font-weight: bold;'>ความคิดเห็นของฉัน</div>
	    			<div id="showComment_User"></div>
	    		</div>

	    		<div class="clearfix"></div>
	    		<hr>

	    		<div class="col-md-6" style="font-weight: bold;"><b>ความเห็น</b></div>
	    		<div class="col-md-6">
					<button id="btnComment" class="btn btn-default pull-right"><i class="glyphicon glyphicon-pencil"></i> เขียนความคิดเห็น</button>
	    			 
	    			</div>
	    			<div class="clearfix"></div>
	    			<div class=""><!-- กล่องใส่กราฟ -->
	    				<div id="container">
	    					<div id="inner">
	    						<div class="rating">
	    							<span id="Count_rate"></span>
	    						</div>

	    						<div class="histo">


	    							<?php for($i=0; $i<5;$i++): ?>
	    								<?php 

                $v=array('bar-five','bar-four','bar-three','bar-two','bar-one');//สี bar
                $num=array($vote[0][v5],$vote[0][v4],$vote[0][v3],$vote[0][v2],$vote[0][v1]); //จำนวน ความคิดเห็น 
                
                
                $n=5;//จำนวนตัวเลข bar
                
                ?>
                <div class="five histo-rate">
                	<span class="histo-star">
                		<i class="active icon-star"></i><?= $n-$i;?></span>
                		<span class="bar-block"> 
                			<span id="<?= $v[$i]; ?>" class="bar">
                				<span><?= number_format($num[$i])?></span>&nbsp;
                			</span> 
                		</span>
                	</div>    
                	<?php 
                	$num[0] = ($num[0] / $count)*100;
                	$num[1] = ($num[1] / $count)*100;
                	$num[2] = ($num[2] / $count)*100;
                	$num[3] = ($num[3] / $count)*100;
                	$num[4] = ($num[4] / $count)*100;

                	?>  

                <?php endfor; ?>
            </div>
        </div>
    </div>
</div><!-- กล่องใส่กราฟ-->

<div id="showComment"></div>
<div id="showForm"></div>
</div>

</div>

<div class="row">
	<div class="clearfix"></div>   
	<div class="col-md-12">  
		<button id='btnLoadmore' class="btn btn-default btn-block">โหลดเพิ่ม    
			<span id="loadings">
				<span style="margin-left:10%;"  class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...
			</span>
		</button>
	</div>

</div>


</div> <!-- end -->
</div>
    </div>

</div>

<?php  $this->registerJs("
$('.inv-form').on('click', '.fav-btn', function() {
    var gid = $(this).attr('data-gid');
    var icon = $('#btn-box');
    $.ajax({
	method: 'POST',
	url: '".Url::to(['/inv/inv-person/favorite', 'gid'=>''])."'+gid,
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		if(result.active==1){
		    icon.html('$linkFav');
		} else {
		    icon.html('$linkNotFav');
		}
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
});

");?>


<?php 
    $this->registerJs("
      var n=1;
      var message='';
      var media = '';
      var pageSizes = 5;
      var count = 0;
      var Count_c = 0;
        setInterval(function(){
          
            realtime();

        },100); 
        var timeOut = setTimeout(function(){
          showComment(pageSizes);
        },100);
        CountInv(); 
        countRate();




        $(document).ready(function() {
          $('.bar span').hide();
          $('#bar-five').animate({
             width:'$num[0]%'    }, 1000);
          $('#bar-four').animate({
              width:'$num[1]%'}, 1000);
          $('#bar-three').animate({
              width:'$num[2]%'}, 1000);
          $('#bar-two').animate({
              width:'$num[3]%'}, 1000);
          $('#bar-one').animate({
              width:'$num[4]%'}, 1000);
          
          setTimeout(function() {
            $('.bar span').fadeIn('slow');
          }, 1000);
          
        });

  
        function countRate()
        {
          $.ajax({
            url:'".Url::to(['/inv/inv-person/counts'])."',
            success:function(data)
            {
               $('#Count_rate').html(data);
            }
          })
        }   

        function realtime()
        {
            
             if(Count_c != 0){
                $.ajax({
                  url:'".Url::to(['/inv/inv-person/realtime'])."',
                  success:function(count){
                      Count_c = count;
                      console.log(count); 
                      updateData();
                      showComment();
                  }
              });

             } 
            
            
        }//realtime <<<------------------------------------------------------------------------>>>
        function updateData()
        {
            $.ajax({
                url:'".Url::to(['/inv/inv-person/saverealtime'])."',
                success:function(count){
                   $('#showComment').load(); 
                   
                   
                }
            });
        }//updateData  <<<------------------------------------------------------------------------>>>
       
        $('#btnComment').click(function(){
            $.ajax({
                 url:'".Url::to(['/inv/inv-person/showform'])."',
                 success:function(data){
                    $('#showForm').html(data);
                    $('#showForm').show();
                    $('#showComment').hide();
                    $('#btnLoadmore').hide();
                    $('#container').hide();
                 }
            });
        });//btnComment แสดงกล่องพิมพื <<<------------------------------------------------------------------------>>>

        function showComment(pageSize=5)
        {
         

         $.ajax({
              url:'".Url::to(['/inv/inv-person/show-inv'])."',
              type:'POST',
              data:{pageSize:pageSize},
              //dataType:'json',
              success:function(data){
                
                $('#showComment').html(data);
                $('#loadings').hide();
              }

          });
          
        }//showComment <<<------------------------------------------------------------------------>>>


        $('#btnLoadmore').click(function(){
           
           $('#loadings').show();
           showComment(pageSizes+=5);
           CountInv();

        });//LoadMore โหลด 
        function CountInv()
        {
          $.ajax({
            url:'".Url::to(['/inv/inv-person/count-inv'])."',
            success:function(data)
            {
              count = data;
              if(pageSizes > count){
                  $('#btnLoadmore').hide();
              }else{
                 $('#btnLoadmore').show();
              }
            }
          })
        }//count

        function showCommentUser()
        {
         

         $.ajax({
              url:'".Url::to(['/inv/inv-person/show-inv'])."',
              type:'POST',
              data:{uid:".Yii::$app->user->identity->id."},
              success:function(data){
                
                $('#showComment_User').html(data);
                //console.log(data);
                 
              }

          });
          
        }//showComment <<<------------------------------------------------------------------------>>>
    setTimeout(function(){
        showCommentUser()
    },500);

    ");

?>



 <style>
 #loadings{
  display: none;
 }
 #btnLoadmore{
  display: none;
 }
   .glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
}

@-webkit-keyframes spin2 {
    from { -webkit-transform: rotate(0deg);}
    to { -webkit-transform: rotate(360deg);}
}

@keyframes spin {
    from { transform: scale(1) rotate(0deg);}
    to { transform: scale(1) rotate(360deg);}
}
 </style>