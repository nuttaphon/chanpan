<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\field\FieldRange;
use backend\modules\inv\classes\InvFunc;
use common\lib\sdii\widgets\SDModalForm;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ovcca\models\OvPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$gname = isset($inv_main['gname'])?$inv_main['gname']:'แสดงตัวอย่างโมดูลที่ได้จากการตั้งค่านี้';
$this->title = Yii::t('app', "$gname");
$this->params['breadcrumbs'][] = $this->title;
$ovfilter_sub = isset($ovfilter_sub) && !empty($ovfilter_sub)?$ovfilter_sub:0;

$op['language'] = 'th';
	
$q = array_filter($op);

$this->registerJsFile('https://maps.google.com/maps/api/js?'.http_build_query($q), [
    'position'=>\yii\web\View::POS_HEAD,
    'depends'=>'yii\web\YiiAsset',
]);


?>
<div class="inv-person-index" >
   
    <?php  Pjax::begin(['id'=>'inv-person-grid-pjax', 'timeout' => 1000*60]);?>
    
    <?php
    if($module>0){
	echo $this->render('/inv-person/_menu', ['module'=>$module, 'comp'=>$comp]);
    }
    ?>
    
    <div class="row" style="margin-top: 15px;">
    <?php $form = ActiveForm::begin([
		'id' => 'jump_menu',
		'action' => ['index', 'module'=>$module],
		'method' => 'get',
		'layout' => 'inline',
		'options' => ['style'=>'display: inline-block;', 'class'=>'col-md-12']	    
	    ]); 
    ?>
    
	
    <?php
    $numK = 0;
    if($module==0){
	$numK++;
	?>
	<div class="row" >
	    <div class="col-md-12 " style="margin-bottom: 15px;">
		<div style="border: 1px solid #ddd; padding: 5px 15px;border-radius: 4px;">
		<div class="box-title"> 
		    <div class="box-arrow" style="border-top-color: #dd4b39"></div> 
		    <div class="box-inner" style="background-color: #dd4b39">1</div> 
		</div>
		    <div style="display: inline-block;">
			<span style="font-size: 16px;">เลือกฟอร์มเริ่มต้น <small>(ฟอร์มที่มีคำถามพิเศษ ซึ่งมีตัวแปรสำหรับแสดงด้านซ้ายของตารางกำกับงานข้างล่าง)</small></span>
			    <br>
			    <?= Html::dropDownList('comp', $comp, ArrayHelper::map($ezform, 'ezf_id', 'ezf_name'), ['class'=>'form-control input-sm', 'onChange'=>'$("#jump_menu").submit()'])?>
			    
			    
			    <?=Html::a('<i class="glyphicon glyphicon-cog"></i> EzModule<sup>®</sup>', Url::to(['/inv/inv-person/main',
					'main_ezf_id'=>$inv_main['main_ezf_id'],
					'module'=>$module,
					]), ['class' => 'btn btn-warning btn-sm']);?>
		    </div>

		</div>
	    </div>
	</div>
<?php    }
    ?>
    
    <?php 
    
    $compArry = ArrayHelper::map($ezform, 'ezf_id', 'comp_id_target');
    $ezformArry = ArrayHelper::map($ezform, 'ezf_id', 'ezf_name');
    
    ?>
    <div class="row" >
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div style="border: 1px solid #ddd; padding: 5px 15px;border-radius: 4px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #1b95e0"></div> 
		<div class="box-inner" style="background-color: #1b95e0"> <?=1+$numK?> </div> 
	    </div>
		<div style="display: inline-block;">
		    <span style="font-size: 16px;margin-bottom: 5px; ">ลงทะเบียนใน <?=$ezformArry[$inv_main['main_ezf_id']]?> แล้ว <code><?=number_format($totalAll)?></code> รายการ </span>
			<br>
			ลงทะเบียนเพิ่ม
		    <?php /*Html::a('<i class="glyphicon glyphicon-plus"></i> จาก '.$ezformArry[$comp], Url::to(['/inputdata/step2',
				    'ezf_id'=>$comp,
				    'target'=>'',
				    'comp_id_target'=>$compArry[$comp],
				    'rurl'=>base64_encode(Yii::$app->request->url),
				    ]), ['class' => 'btn btn-success btn-sm', 'target'=>'_blank', 'style'=>'margin-bottom: 6px;']);
		     */ ?>
		     
		    <?php  echo Html::a('<i class="glyphicon glyphicon-plus"></i> จาก '.$ezformArry[$inv_main['main_ezf_id']], NULL, ['data-url'=>Url::to(['/inv/inv-person/ezform-print',
				    'ezf_id'=>$inv_main['main_ezf_id'],
				    'comp_id_target'=>$inv_main['main_comp_id_target'],
				    'end'=>$inv_main['ezf_id']==$inv_main['main_ezf_id']?1:0,
				    ]), 'class' => 'btn btn-success btn-sm open-ezfrom', 'style'=>'margin-bottom: 6px; cursor: pointer;']);
		    
		    ?>
		</div>
	    
	    <div class="pull-right" id="video-training">
		<!--<a class="btn btn-danger" href="https://cloudstorage.cascap.in.th/VDO/ControlChart01/ControlChart01.html" target="_blank">VDO สอนการใช้งาน !!</a>-->
	    </div>
	    </div>
	</div>
    </div>
	
    <div class="row">
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div style="border: 1px solid #ddd; padding: 5px 15px;border-radius: 4px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #F1C40F"></div> 
		<div class="box-inner" style="background-color: #F1C40F"> <?=2+$numK?> </div> 
	    </div>
		
			
			<div style="display: inline-block;">
			    <span style="font-size: 16px;">กลุ่มที่ลงทะเบียนใน <?=$ezformArry[$inv_main['main_ezf_id']]?> ซึ่งมีจำนวน <code><?=number_format($totalAll)?></code> รายการ </span>
			    <br>
		<?php
		$ovfilter = 0;
		$datasub = ArrayHelper::map($dataOvFilterSub, 'sub_id', 'sub_name');
		$datasubstatus = ArrayHelper::map($dataOvFilterSub, 'sub_id', 'urine_status');
		$ovshow = ($ovfilter_sub!=0)?ArrayHelper::getValue($datasubstatus, $ovfilter_sub):1;
		?>	
			    จัดการข้อมูล 
		
		<?=Html::button('<span class="glyphicon glyphicon-plus"></span> สร้างใบกำกับงาน', ['data-url'=>Url::to(['/inv/inv-person/ovfilter-sub', 'module'=>$module, 'comp'=>$comp]), 'class' => 'btn btn-success btn-sm', 'id'=>'addbtn-filter-sub'])?>
		<?= Html::dropDownList('ovfilter_sub', $ovfilter_sub, $datasub, ['class'=>'form-control input-sm', 'prompt'=>'แสดงทั้งหมด', 'onChange'=>'$("#jump_menu").submit()'])?>
		<?= Html::a('<span class="glyphicon glyphicon-pencil"></span> จัดการใบกำกับงาน', ['/inv/inv-filter-sub/index', 'comp'=>$comp, 'module'=>$module], ['class' => 'btn btn-primary btn-sm']) ?>
		    <?php
		if($ovfilter_sub>0){
		    if($modelFilter['filter_order']==0){
			echo Html::button('<span class="glyphicon glyphicon-transfer"></span> จัดการกลุ่มเป้าหมายในใบกำกับงาน', ['data-url'=>Url::to(['inv-person/addlist', 'comp'=>$comp, 'module'=>$module, 'ovfilter_sub'=>$ovfilter_sub]), 'class' => 'btn btn-sm btn-warning addlistbtn']).' '; 
		    }
		}
		echo Html::button('<span class="glyphicon glyphicon-print"></span> พิมพ์ใบกำกับงาน', ['id'=>'reportOvcca', 'data-url'=>Url::to(['inv-person/report-ovcca', 'comp'=>$comp, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default btn-sm']).' ';
		//echo Html::button('<span class="glyphicon glyphicon-tags"></span> พิมพ์สติ๊กเกอร์แบบ 2+6', ['data-url'=>Url::to(['inv-person/report-sticker', 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default reportSticker btn-sm']).' ';
		//echo Html::button('<span class="glyphicon glyphicon-tags"></span> พิมพ์สติ๊กเกอร์แบบ 1+2', ['data-url'=>Url::to(['inv-person/report-sticker', 'sizemini'=>1, 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default reportSticker btn-sm']).' ';
		echo Html::button('<span class="glyphicon glyphicon-export"></span> export', ['id'=>'reportExel', 'data-url'=>Url::to(['inv-person/report-exel', 'comp'=>$comp, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default btn-sm']).' ';
		
		?>
		
		    </div>
		</div>    
	</div>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
<!--    <div class="row">
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #31b20e"></div> 
		<div class="box-inner" style="background-color: #31b20e"> 3 </div> 
	    </div>
	    
	    
	</div>
    </div>
    <hr>-->
 
    <?php
    $panelBtn = '<span><strong>จำนวนที่เลือก</strong></span>  <font color="#ff0000"><span class="cart-num">0</span></font>  เพื่อ ' .
		      //Html::button('<span class="glyphicon glyphicon-tags"></span> พิมพ์สติ๊กเกอร์เฉพาะรายชื่อที่เลือก', ['data-url'=>Url::to(['inv-person/report-sticker', 'comp'=>$comp]), 'class' => 'btn btn-default btn-sm reportSticker', 'id'=>'modal-stickerbtn-ov-person', 'disabled'=>true]).' '.
		      Html::button('<span class="glyphicon glyphicon-plus"></span> เพิ่มเป้าหมายที่เลือกลงในใบกำกับงาน', ['data-url'=>Url::to(['inv-person/add-selectlist', 'module'=>$module, 'comp'=>$comp, 'ovfilter_sub'=>$ovfilter_sub]), 'class' => 'btn btn-success btn-sm addlistSmallbtn', 'id'=>'modal-addlistbtn-ov-person', 'disabled'=>true]);
    ?>
    <div class="row">
	<div class="col-md-12">
	    <?= $panelBtn;?>
	    
	</div>
	
    </div>
<div id="divToScroll" class="table-responsive">
	<?php
	    
	$columns = [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionOvPersonIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'min-width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
	    ],
        ];
	
	if(isset(Yii::$app->session['sql_main_fields']['enable_field'])){
	    $main_fields = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_field']);
	    $fixFields = isset($main_fields['field'])?$main_fields['field']:[];
	    $fixLabel = isset($main_fields['label'])?$main_fields['label']:[];
	    $fixAlign = isset($main_fields['align'])?$main_fields['align']:[];
	    $fixWidth = isset($main_fields['width'])?$main_fields['width']:[];
	    $fixType = isset($main_fields['type'])?$main_fields['type']:[];
	    $fixId = isset($main_fields['id'])?$main_fields['id']:[];
	    $fixOrglabel = isset($main_fields['orglabel'])?$main_fields['orglabel']:[];
	    //\yii\helpers\VarDumper::dump($main_fields,10,true);
//		exit();
	    foreach ($fixFields as $keyFix => $valueFix) {
		$fType = $fixType[$keyFix];
		
		$obj = [];
		$obj['attribute'] = $valueFix;
		
		if(in_array($fType, [4,6])){
		    $modelChoice = backend\modules\ezforms\models\EzformChoice::find()
			    ->where('ezf_field_id=:id',[':id'=>$fixId[$keyFix]])
			    ->all();
		    $choice = ArrayHelper::map($modelChoice, 'ezf_choicevalue', 'ezf_choicelabel');
		    if($modelChoice){
			$obj['choice'] = $choice;
		    }
		    $obj['filter'] = Html::activeDropDownList($searchModel, $valueFix, $choice, ['class'=>'form-control', 'prompt'=>'All']);
		} elseif ($fType==21) {
		    if($fixOrglabel[$keyFix]==1){
			$data = \backend\modules\inv\classes\InvQuery::getProvince();
			if($data){
			    $obj['choice'] = ArrayHelper::map($data, 'id', 'name');
			    $obj['filter'] = Html::activeDropDownList($searchModel, $valueFix, $obj['choice'], ['class'=>'form-control', 'prompt'=>'All']);
			}
			
		    } elseif ($fixOrglabel[$keyFix]==2) {
			$data = \backend\modules\inv\classes\InvQuery::getAmphur();
			if($data){
			    $obj['choice'] = ArrayHelper::map($data, 'id', 'name');
			}
		    } elseif ($fixOrglabel[$keyFix]==3) {
			$data = \backend\modules\inv\classes\InvQuery::getDistrict();
			if($data){
			    $obj['choice'] = ArrayHelper::map($data, 'id', 'name');
			}
		    }
		} elseif ($fType==7) {
		    $obj['type'] = $fType;
                    
                    $obj['filter'] = \kartik\daterange\DateRangePicker::widget([
                        'model'=>$searchModel,
                        'attribute'=>$valueFix,
                        'convertFormat'=>true,
                        //'useWithAddon'=>true,
                        'pluginOptions'=>[
                            'locale'=>[
                                'format'=>'d-m-Y',
                                'separator'=>' to ',
                                //'language'=>'TH',
                            ],
                            //'opens'=>'left'
                        ]
                    ]);
		} elseif ($fType==9) {
		    $obj['type'] = $fType;
                    
                    $obj['filter'] = \kartik\daterange\DateRangePicker::widget([
                        'model'=>$searchModel,
                        'attribute'=>$valueFix,
                        'convertFormat'=>true,
                        //'useWithAddon'=>true,
                        'pluginOptions'=>[
                            'locale'=>[
                                'format'=>'d-m-Y',
                                'separator'=>' to ',
                                //'language'=>'TH',
                            ],
                            //'opens'=>'left'
                        ]
                    ]);
		} elseif ($valueFix=='create_date' || $valueFix=='fxmain_create_date') {
                    
                    $obj['filter'] = \kartik\daterange\DateRangePicker::widget([
                        'model'=>$searchModel,
                        'attribute'=>$valueFix,
                        'convertFormat'=>true,
                        //'useWithAddon'=>true,
                        'pluginOptions'=>[
                            'locale'=>[
                                'format'=>'d-m-Y',
                                'separator'=>' to ',
                                //'language'=>'TH',
                            ],
                            //'opens'=>'left'
                        ]
                    ]);
                    
                } elseif ($fType==10) {
		    $sql = "SELECT ezform_fields.ezf_id, 
				ezform_fields.ezf_field_id, 
				ezform_fields.ezf_field_name,  
				ef2.ezf_field_name as keyid,
				(SELECT group_concat(ef3.ezf_field_name) FROM ezform_fields ef3 WHERE ef3.ezf_id = ezform_component.ezf_id AND FIND_IN_SET(ef3.ezf_field_id, ezform_component.field_id_desc)>0) AS keyname,
				ezform.ezf_table,
				ezform_component.field_id_desc,
				ezform_component.ezf_id as com_ezf_id
			FROM ezform_fields INNER JOIN ezform_component ON ezform_fields.ezf_component = ezform_component.comp_id
			INNER JOIN ezform_fields ef2 ON ef2.ezf_field_id = ezform_component.field_id_key
			INNER JOIN ezform ON ezform.ezf_id = ezform_component.ezf_id
			WHERE ezform_fields.ezf_field_id = :ezf_field_id ";
			
		    $data_com = Yii::$app->db->createCommand($sql, [':ezf_field_id'=>$fixId[$keyFix]])->queryOne();
		    if($data_com){
			$arrKey = explode(',', $data_com['keyname']);
			$name = '';
			if(count($arrKey)>1){
			    $concat ='';
			    $comma = '';
			    foreach ($arrKey as $valueName) {
				$concat .= $comma."$valueName";
				$comma = ", ' ', ";
			    }
			    $name = ", CONCAT($concat) AS name";
			    
			} else {
			    $name = ", {$data_com['keyname']} AS name";
			}
			
			$sql = "SELECT {$data_com['keyid']} $name
				FROM {$data_com['ezf_table']}
				";
				
			$data = Yii::$app->db->createCommand($sql)->queryAll();
			if($data){
			    $obj['choice'] = ArrayHelper::map($data, $data_com['keyid'], 'name');
			    $obj['filter'] = Html::activeDropDownList($searchModel, $valueFix, $obj['choice'], ['class'=>'form-control', 'prompt'=>'All']);
			}
		    }
		    
		}
		
		$columns[] = [
			'attribute'=>$valueFix,
			'label'=>$fixLabel[$keyFix],
			'value'=>function ($data) use ($obj) { 
			    
			    if (isset($obj['choice'])) {
				return $obj['choice'][$data[$obj['attribute']]];
			    } elseif (isset ($obj['type'])) {
				if($obj['type']==7){
				    if(isset($data[$obj['attribute']]) && $data[$obj['attribute']]!=''){
					return \common\lib\sdii\components\utils\SDdate::mysql2phpThDateSmall($data[$obj['attribute']]);
				    }
				} elseif($obj['type']==9){
				    if(isset($data[$obj['attribute']]) && $data[$obj['attribute']]!=''){
					return \common\lib\sdii\components\utils\SDdate::mysql2phpThDateTime($data[$obj['attribute']]);
				    }
				}
			    } elseif ($obj['attribute']=='create_date' || $obj['attribute']=='fxmain_create_date') {
                                if(isset($data[$obj['attribute']]) && $data[$obj['attribute']]!=''){
                                    return \common\lib\sdii\components\utils\SDdate::mysql2phpThDateTime($data[$obj['attribute']]);
                                }
                            } else {
				return $data[$obj['attribute']];
			    }
			},
			'filter'=>$obj['filter'],
			'headerOptions'=>['style'=>"text-align: {$fixAlign[$keyFix]};"],
			'contentOptions'=>['style'=>"min-width:{$fixWidth[$keyFix]}px; text-align: {$fixAlign[$keyFix]};"],
		    ];
	    }
	    
	    $columns[] = [
		'header'=>Yii::$app->session['sql_main_fields']['ezf_name'],
		'format'=>'raw',
		'value'=>function ($data) use ($inv_main, $compArry){ 
		    if ($data['id']!=null) {
			
			$icon = InvFunc::getStatusIcon($data['rstat']);
			
			$rurl = base64_encode(Yii::$app->request->url);
			
//			$rowReturnAdd = Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i> ', null, [
//			'data-url'=>Url::to(['/inv/inv-person/ezform-print',
//			    'target'=>$data['ptid'],
//			    'ezf_id'=>$comp,
//			    'comp_id_target'=>$compArry[$comp],
//			]),
//			'class'=>'open-ezfrom',
//			'style'=>'cursor: pointer;',
//			'data-toggle'=>'tooltip',
//			'title'=>'เพิ่มข้อมูล',
//		    ]);
			$pkJoin = Yii::$app->session['sql_main_fields']['pk_join'];
			if(Yii::$app->session['sql_main_fields']['special']==1){
			    $pkJoin = 'ptid';
			}
			return Html::a('<i '.$icon.'></i>', NULL, [
			    'class' => 'btn-lg open-ezfrom',
			    'data-url'=>Url::to(['/inv/inv-person/ezform-print', 'ezf_id'=>Yii::$app->session['sql_main_fields']['ezf_id'], 'dataid'=>$data['id'], 
				'target'=>  base64_encode($data[$pkJoin]),
				'comp_id_target'=>Yii::$app->session['sql_main_fields']['comp_id_target'] ]),
			    'data-toggle'=>'tooltip',
			    //'data-placement'=>'right',
			    'style'=>'cursor: pointer;',
			    'title'=>  isset($data['detail_main'])?$data['detail_main']:'แสดงข้อมูล',
			]);
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:100px; text-align: center;'],
	    ];
            //appxq\sdii\utils\VarDumper::dump(Yii::$app->session['sql_main_fields']['created_by']);
            if(in_array(Yii::$app->session['sql_main_fields']['ezf_table'], ['tb_data_1', 'tb_data_coc']) && Yii::$app->session['sql_main_fields']['enable_target']==1){
                //if(Yii::$app->user->can('administrator') || Yii::$app->user->can('adminsite') || Yii::$app->user->can('adminnamphur') || Yii::$app->user->can('adminnchangwat')) {//adminsite
                    $columns[] = [
                        'header'=>'เปลี่ยนรหัสผ่าน',
                        'format'=>'raw',
                        'value'=>function ($data){ 
                            $invuser = \backend\modules\inv\models\InvUser::find()->where('gid=:gid AND id=:cid', [':cid'=>$data['cid'], ':gid'=>$_GET['module']])->one();
                            $html='';
                            if($invuser){
                                $html .='<form class="form-inline">';
                                $html .= '<div class="form-group">';
                                $html .= Html::textInput('user', $invuser['username'], ['class'=>'form-control', 'placeholder'=>'Username']);
                                $html .= '</div>';
                                $html .= '<div class="form-group">';
                                $html .= Html::passwordInput('pw', null, ['class'=>'form-control', 'placeholder'=>'Password']);
                                $html .= '</div>';
                                $html .= ' '.Html::a('<i class="glyphicon glyphicon-pencil"></i>', NULL, [
                                    
                                    'class' => 'btn-success btn edit-auto-user',
                                    'data-url'=>Url::to(['/inv/inv-person/edit-user', 'id'=>$invuser['id']
                                        ]),
                                    'data-toggle'=>'tooltip',

                                    'style'=>'cursor: pointer;',
                                    'title'=> 'เปลี่ยนรหัสผ่าน',
                                ]);
                                $html .= ' '.Html::a('<i class="glyphicon glyphicon-remove"></i>', NULL, [

                                    'class' => 'btn-danger btn del-auto-user',
                                    'data-url'=>Url::to(['/inv/inv-person/del-user', 'id'=>$invuser['id']
                                        ]),
                                    'data-toggle'=>'tooltip',

                                    'style'=>'cursor: pointer;',
                                    'title'=> 'ยกเลิกผู้ใช้รายนี้',
                                ]);
                                $html .= '</form>';
                            }
                            return $html;
                        },
                        'filter'=>'',
                        //'headerOptions'=>['style'=>"text-align: center;"],
                        'contentOptions'=>['style'=>"min-width:480px; width:480px; "],
                    ];
                        
                    $columns[] = [
                        'header'=>'',
                        'format'=>'raw',
                        'value'=>function ($data){ 
                            $pkJoin = Yii::$app->session['sql_main_fields']['pk_join'];
                                if(Yii::$app->session['sql_main_fields']['special']==1){
                                    $pkJoin = 'ptid';
                            }
                            
                            return Html::a('<i class="glyphicon glyphicon-user"></i>', NULL, [

                                'class' => 'btn-primary btn open-auto-user',
                                'data-url'=>Url::to(['/inv/inv-person/gen-user', 'ezf_id'=>Yii::$app->session['sql_main_fields']['ezf_id'], 'dataid'=>$data['id'], 'gid'=>$_GET['module'], 
                                    'target'=>  base64_encode($data[$pkJoin]),
                                    'comp_id_target'=>Yii::$app->session['sql_main_fields']['comp_id_target'],
                                    'table'=>  Yii::$app->session['sql_main_fields']['ezf_table'],
                                    ]),
                                'data-toggle'=>'tooltip',

                                'style'=>'cursor: pointer;',
                                'title'=> 'สร้าง Username/Password ด้วยเป้าหมายนี้',
                            ]);
                        },
                        'filter'=>'',
                        'headerOptions'=>['style'=>"text-align: center;"],
                        'contentOptions'=>['style'=>"min-width:80px; width:80px; text-align: center;"],
                    ];
                //}
            } 
            
	}
        
        
		
	$fields_col = Yii::$app->session['sql_fields'];
	if($fields_col){
//	    foreach ($fields_col as $keyField => $valueField) {
//		$result = trim($valueField['result_field']);
//		
//		$columns[] = InvFunc::createColumnsCount($valueField, Yii::$app->session['sql_main_fields']['special']);
////		if($result!=''){
////		    $columns[] = InvFunc::createColumnsResult($valueField);
////			
////		} else {
////		    $columns[] = InvFunc::createColumns($valueField);
////		}
//	    }
	} else {
	    
	    if(isset(Yii::$app->session['sql_main_fields']['enable_form'])){
		$main_forms = \appxq\sdii\utils\SDUtility::string2Array(Yii::$app->session['sql_main_fields']['enable_form']);
		$ffixFields = isset($main_forms['form'])?$main_forms['form']:[];
		$ffixLabel = isset($main_forms['label'])?$main_forms['label']:[];
		$ffixVisible = isset($main_forms['visible'])?$main_forms['visible']:[];
		$ffixWidth = isset($main_forms['width'])?$main_forms['width']:[];
		$ffixCondition = isset($main_forms['condition'])?$main_forms['condition']:[];
		$ffixDisplay = isset($main_forms['display'])?$main_forms['display']:[];
		$ffixShow = isset($main_forms['show'])?$main_forms['show']:[];
		$ffixResult = isset($main_forms['result'])?$main_forms['result']:[];
		$ffixDate = isset($main_forms['date'])?$main_forms['date']:[];

		$ffixfd = isset($main_forms['field_detail'])?$main_forms['field_detail']:[];
		$ffixen = isset($main_forms['ezf_name'])?$main_forms['ezf_name']:[];
		$ffixet = isset($main_forms['ezf_table'])?$main_forms['ezf_table']:[];
		$ffixct = isset($main_forms['comp_id_target'])?$main_forms['comp_id_target']:[];
		$ffixur = isset($main_forms['unique_record'])?$main_forms['unique_record']:[];
		
		foreach ($ffixFields as $keyForm => $valueForm) {
		    
		    $result = trim($ffixVisible[$keyForm]);
		    //$ezform = \backend\modules\inv\classes\InvQuery::getEzformById($valueForm);
		    $form_fix = [
			'ezf_id' => $valueForm,
			'ezf_name' => $ffixen[$keyForm],
			'ezf_table' => $ffixet[$keyForm],
			'comp_id_target' => $ffixct[$keyForm],
			'visible_field' => $ffixVisible[$keyForm],
			'unique_record' => $ffixur[$keyForm],//
			'sql_id_all' => 'idall_'.$valueForm,
			'sql_id_name' => 'id_'.$valueForm,
			'sql_idsubmit_name' => 'idsubmit_'.$valueForm,
			'sql_result_name' => 'result_'.$valueForm,
                        'field_detail' => $ffixfd[$keyForm],
			'value_options' => \appxq\sdii\utils\SDUtility::array2String($ffixCondition[$valueForm]),
			'header' => $ffixLabel[$keyForm],
			'width' => $ffixWidth[$keyForm],
			'display_options' => \appxq\sdii\utils\SDUtility::array2String($ffixDisplay[$valueForm]),
			'show' => $ffixShow[$keyForm],
			'result' => $ffixResult[$keyForm],
			'date' => $ffixDate[$keyForm],
		    ];
		    $columns[] = InvFunc::createColumnsCount($form_fix, Yii::$app->session['sql_main_fields']['special']);
		   
//		    if($result!=''){
//			$columns[] = InvFunc::createColumnsResult($form_fix);
//
//		    } else {
//			$columns[] = InvFunc::createColumns($form_fix);
//		    }
		}
	    }
	}
	
	?>
    <br>
    <?php
    $this->registerJsFile("/js/jquery.fixedheadertable.js");
    $this->registerJs("
//        $(document).ready(function() {
//            $('#invtable').fixedHeaderTable({ 
//                    footer: true,
//                    cloneHeadToFoot: true,
//                    altClass: 'odd',
//                    autoShow: false
//            });            
//        });            
    ");
    echo  \common\lib\sdii\widgets\SDGridView::widget([
	'id' => 'inv-person-grid',
	'panel' => false,
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'layout' => '{summary}{pager}{items}{pager}',
        'columns' => $columns,
        'tableOptions' => ['class' => 'table table-striped table-bordered table-hover','id' => 'invtable'],
    ]); 
//    bluezed\floatThead\FloatThead::widget(
//        [
//            'tableId' => 'invtable', 
//            'options' => [
//                'top'=>'50'
//            ]
//        ]
//    );
//    echo \kartik\grid\GridView::widget([
//	'id' => 'inv-person-grid',
//	'panel' => false,
//	'dataProvider' => $dataProvider,
//	'filterModel' => $searchModel,
//	'layout' => '{summary}{pager}{items}{pager}',
//        'columns' => $columns,
//        'floatHeader'=>true,
//        'floatHeaderOptions' => ['scrollingTop'=>'50'],
//    ]);     
    ?>
	</div>
    
    <?php  
    $js = '';
    if($module>0){
	$js = isset($inv_main['inv_js'])?$inv_main['inv_js']:'';
    }
    $this->registerJs($js);
    
    Pjax::end();?>

</div>



<?=  ModalForm::widget([
    'id' => 'modal-inv-emr',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
     'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-inv-person',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
    'options'=>['style'=>'overflow-y:scroll;'],
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-print',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
    'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-ovlist',
    //'size'=>'modal-lg',
    'tabindexEnable'=>false,
    'clientOptions'=>['backdrop'=>'static'],
     'options'=>['style'=>'overflow-y:scroll;']
]);
?>

<?php  
    echo SDModalForm::widget([
        'id' => 'modal-query-request',
        'size' => 'modal-lg',
        'tabindexEnable' => false,
        'clientOptions'=>['backdrop'=>'static'],
	 'options'=>['style'=>'overflow-y:scroll;']
    ]);  
    

$fix = '';
$invFixcol = isset($inv_main['fixcol'])?$inv_main['fixcol']:5;
if($dataProvider->count>0){
    $fix = "var table = $('.table').DataTable( {
        scrollY:        '60vh',
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
	searching:      false,
        fixedColumns:   {
            leftColumns: $invFixcol
        }
    } );";
}

$this->registerJs("
	    
$('.modal-lg').width('90%');	

$('#inv-person-grid-pjax').on('click', '.open-ezfrom', function() {
    modalEzfrom($(this).attr('data-url'));
    
});

$('#inv-person-grid-pjax').on('click', '.open-ezfrom-emr', function() {
    modalEzfromEmr($(this).attr('data-url'));
    
});

$('#inv-person-grid-pjax').on('click', '.open-auto-user', function() {
    modalGenuser($(this).attr('data-url'));
    
});

$('#inv-person-grid-pjax').on('click', '.del-auto-user', function() {
    var url = $(this).attr('data-url');
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
        modalGenuser(url);
    });
});

$('#inv-person-grid-pjax').on('click', '.edit-auto-user', function() {
    var url = $(this).attr('data-url');
    var data = $(this).parent().serialize();
    $.ajax({
	method: 'POST',
        data:data,
	url: url,
	dataType: 'Json',
	success: function(result, textStatus) {
            ". SDNoty::show('result.message', 'result.status') ."
                $.pjax.reload({container:'#inv-person-grid-pjax'});
	    return false;
	}
    });
});

function modalGenuser(url) {
    
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'Json',
	success: function(result, textStatus) {
            ". SDNoty::show('result.message', 'result.status') ."
                $.pjax.reload({container:'#inv-person-grid-pjax'});
	    return false;
	}
    });
}

$('#modal-print').on('hidden.bs.modal', function () {
    //$.pjax.reload({container:'#inv-person-grid-pjax'});
});

function modalEzfrom(url) {
    $('#modal-print .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-print').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-print .modal-content').html(result);
	    return false;
	}
    });
}

$('#modal-print').on('hidden.bs.modal', function () {
    //$.pjax.reload({container:'#inv-person-grid-pjax'});
});

function modalEzfromEmr(url) {
    $('#modal-inv-emr .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-emr').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $('#modal-inv-emr .modal-content').html(result);
	    return false;
	}
    });
}

$('#divToScroll').attachDragger();

$('#inv-person-grid-pjax').on('click', '#modal-addbtn-ov-person', function() {
    modalOvPerson($(this).attr('data-url'));
});

$('#inv-person-grid-pjax').on('click', '#modal-delbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#inv-person-grid-pjax').on('click', '#modal-delallbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#inv-person-grid-pjax').on('click', '#modal-importbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#inv-person-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#inv-person-grid').yiiGridView('getSelectedRows');
	disabledOvPersonBtn(key.length);
    },100);
});

$('#inv-person-grid-pjax').on('click', '.selectionOvPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledOvPersonBtn(key.length);
});

$('#inv-person-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    //modalOvPerson('".Url::to(['inv-person/update', 'id'=>''])."'+id);
});	

$('#inv-person-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalOvPerson(url);
	return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
	return false;
    } else if(action === 'ov01') {
	return false;
    } else if(action === 'ov01-status') {
	$.post(
	    url
	).done(function(result) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$.pjax.reload({container:'#inv-person-grid-pjax'});
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}).fail(function() {
	    ". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	    console.log('server error');
	});
	return false;
    }
   
});

//$('#inv-person-grid-pjax').on('click', '#select_all', function() {
//    if($(this).prop('checked')){
//	$('#modal-importallbtn-ov-person').attr('disabled', false);
//	$('#select_show').html($(this).attr('data-count'));
//	$('.cart-num').html($(this).attr('data-count'));
//    } else {
//	$('#modal-importallbtn-ov-person').attr('disabled', true);
//	$('#select_show').html(0);
//	$('.cart-num').html(0);
//    }
//});

$('#inv-person-grid-pjax').on('click', '#modal-importallbtn-ov-person', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

function disabledOvPersonBtn(num) {
    if(num>0) {
	$('#modal-stickerbtn-ov-person').attr('disabled', false);
	$('#modal-addlistbtn-ov-person').attr('disabled', false);
    } else {
	$('#modal-stickerbtn-ov-person').attr('disabled', true);
	$('#modal-addlistbtn-ov-person').attr('disabled', true);
    }
    $('.cart-num').html(num);
}

$('#reportOvcca, #reportOvcca1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('.reportSticker').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportDetail, #reportDetail1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportExel, #reportExel1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonExel(url);
});

function selectionOvPersonExel(url) {
    $('#modal-inv-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-inv-person .modal-content').html(result.html);
		
		$('#modal-inv-person').modal('hide');
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonReport(url) {
    $('#modal-inv-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		    $('#modal-inv-person .modal-content').html(result.html);
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function statusOvPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to update status all items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#inv-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

$('#inv-person-grid-pjax').on('click', '#modal-statust-ov-person', function() {
    statusOvPersonGrid($(this).attr('data-url'));
});

$('#inv-person-grid-pjax').on('click', '#modal-statusf-ov-person', function() {
    statusOvPersonGrid($(this).attr('data-url'));
});

$('#inv-person-grid-pjax').on('click', '#addbtn-filter', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#inv-person-grid-pjax').on('click', '#addbtn-filter-sub', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#inv-person-grid-pjax').on('click', '.addlistSmallbtn', function() {
    modalOvListSmall($(this).attr('data-url'));
});

$('#inv-person-grid-pjax').on('click', '.addlistbtn', function() {
    modalOvList($(this).attr('data-url'));
});

function modalOvList(url) {
    $('#modal-inv-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $('#modal-inv-person .modal-content').html(result.html);
	}
    });
}

function modalOvListSmall(url) {
    $('#modal-ovlist .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ovlist').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $('#modal-ovlist .modal-content').html(result.html);
	}
    });
}

function modalOvPerson(url) {
    $('#modal-inv-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-inv-person').modal('show')
    .find('.modal-content')
    .load(url);
}

function modalOvPersonSmall(url) {
    $('#modal-ovlist .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ovlist').modal('show')
    .find('.modal-content')
    .load(url);
}

$('#inv-person-grid-pjax').on('click', '.btn-action-ezf', function() {
    var target = $(this).attr('data-target');
    var url = $(this).attr('href');
    var ezf_id = $(this).attr('data-ezf_id');
    var comp_id = $(this).attr('data-comp_id');
    
    $.post( url, {ezf_id: ezf_id, target: $.trim(target), comp_id_target: comp_id, rurl: '". base64_encode(Yii::$app->request->url)."'}, function(result){
	$(location).attr('href',result);
    });
    
});

$('[data-toggle=\"tooltip\"]').tooltip({html:true});
    
");?>
