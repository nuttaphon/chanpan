<?php
use yii\helpers\Html;
use yii\helpers\Url;

$genid = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
$indexArry = array_keys($dataSubForm);

?>
<div id="<?=$genid?>" class="row" style="margin-bottom: 15px;">
    <div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[condition]['.$id.'][form][]', null, $dataSubForm, ['class'=>'form-control cform-input'])?></div>
    <div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[condition]['.$id.'][field][]', null, [], ['class'=>'form-control cfield-input'])?> </div>
    <div class="col-md-2 sdbox-col"><?= Html::dropDownList('forms[condition]['.$id.'][cond][]', null, ['=='=>'=', '<'=>'<', '>'=>'>', '<='=>'<=', '>='=>'>=', '!='=>'!=', 'between'=>'Between' ,'func'=>'PHP Function'], ['class'=>'form-control ccond-input'])?></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control cvalue1-input" name="forms[condition][<?=$id?>][value1][]" ></div>
    <div class="col-md-2 sdbox-col"><input type="text" class="form-control cvalue2-input" readonly="readonly" name="forms[condition][<?=$id?>][value2][]" ></div>
    <div class="col-md-1 sdbox-col ccondmore"><?= Html::dropDownList('forms[condition]['.$id.'][more][]', 'or', ['||'=>'OR', '&&'=>'AND'], ['class'=>'form-control cmore-input'])?></div>
    <div class="col-md-1 sdbox-col"><button type="button" class="forms-condition-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
</div>
<?php  


$valF = isset($indexArry[0])?$indexArry[0]:0;

$this->registerJs("

getFieldResult('$valF');
    
function getFieldResult(valField){
    if(valField === undefined || valField === null){
    
    } else {
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/inv/inv-person/getfields'])."',
	    data: {id:valField},
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#$genid .cfield-input').html(result);
	    }
	});
    }
}

");?>