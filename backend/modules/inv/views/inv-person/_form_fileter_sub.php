<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\inv\classes\InvQuery;
/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\OvFilterSub */
/* @var $form yii\bootstrap\ActiveForm */

\yii\jui\DatePicker::className();
?>

<div class="ov-filter-sub-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">ใบกำกับงาน</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'sub_name')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'public')->checkbox() ?>
		<?php
		$userlist = \backend\modules\ezforms\components\EzformQuery::getIntUserAll(); //explode(",", $model1->assign);
		?>
		<?= $form->field($model, 'share')->widget(\kartik\select2\Select2::className(), [
		    'options' => ['placeholder' => 'แชร์', 'multiple' => true],
		    'data' => \yii\helpers\ArrayHelper::map($userlist, 'id', 'text'),
		    'pluginOptions' => [
			    'tags' => true,
			    'tokenSeparators' => [',', ' '],
		    ],
		]) ?>
	<div class="modal-header" style="margin-bottom: 15px;">
	    <?= $form->field($model, 'filter_order')->radioList(['เลือกเร็คคอร์ดเข้าใบกำกับงานด้วยตนเอง', 'เลือกเร็คคอร์ดเข้าใบกำกับงานตามเงื่อนไขต่อไปนี้'])->label(false) ?> 
	</div>
	
	<div class="condition-view">
	    <div class="row">
		<div class="col-md-2 sdbox-col"><label>Form</label></div>
		<div class="col-md-2 sdbox-col"><label>Field</label></div>
		<div class="col-md-2 sdbox-col"><label>Condition</label></div>
		<div class="col-md-2 sdbox-col"><label>Value1</label></div>
		<div class="col-md-2 sdbox-col"><label>Value2</label></div>
		<div class="col-md-1 sdbox-col"><label>More</label></div>
	    </div>

	    <div class="forms-condition-items">
		<?php
		$condition = \appxq\sdii\utils\SDUtility::string2Array($model->options);
		
		$modelEzform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id=:ezf_id', [':ezf_id'=>$comp])->one();
		$dataSub = ArrayHelper::map(InvQuery::getEzformByCom($modelEzform['comp_id_target']),'id','name');
		$dataMain[$modelEzform->ezf_id] = $modelEzform->ezf_name;
		$dataSubForm = ArrayHelper::merge($dataMain, $dataSub);
				
		if(isset($condition) && is_array($condition) && !empty($condition)){
		    
		    $formCond = isset($condItems['form'])?$condItems['form']:[];
		    $value1Cond = isset($condItems['value1'])?$condItems['value1']:[];
		    $value2Cond = isset($condItems['value2'])?$condItems['value2']:[];
		    $fieldCond = isset($condItems['field'])?$condItems['field']:[];
		    $condCond = isset($condItems['cond'])?$condItems['cond']:[];
		    $moreCond = isset($condItems['more'])?$condItems['more']:[];

		    foreach ($formCond as $key_cond => $value_cond) {
			    $dataFieldsResult = ArrayHelper::map(InvQuery::getFields($value_cond), 'id', 'name');
		?>
		    <div class="row" style="margin-bottom: 15px;">
			<div class="col-md-2 sdbox-col"><?= Html::dropDownList('options[form][]', $value_cond, $dataSubForm, ['class'=>'form-control cform-input'])?></div>
			<div class="col-md-2 sdbox-col"><?= Html::dropDownList('options[field][]', $fieldCond[$key_cond], $dataFieldsResult, ['class'=>'form-control cfield-input'])?></div>
			<div class="col-md-2 sdbox-col"><?= Html::dropDownList('options[cond][]', $condCond[$key_cond], ['='=>'=', '<'=>'<', '>'=>'>', '<='=>'<=', '>='=>'>=', '<>'=>'!=', 'between'=>'Between'], ['class'=>'form-control ccond-input'])?></div>
			<div class="col-md-2 sdbox-col"><input type="text" class="form-control cvalue1-input" name="options[value1][]" value="<?=$value1Cond[$key_cond]?>"></div>
			<div class="col-md-2 sdbox-col"><input type="text" <?=$condCond[$key_cond]!='between'?'readonly="readonly"':'';?> class="form-control cvalue2-input" name="options[value2][]" value="<?=$value2Cond[$key_cond]?>"></div>
			<div class="col-md-1 sdbox-col ccondmore"><?= yii\helpers\Html::dropDownList('options[more][]', $moreCond[$key_cond], ['OR'=>'OR', 'AND'=>'AND'], ['class'=>'form-control cmore-input'])?></div>
			<div class="col-md-1 sdbox-col"><button type="button" class="forms-condition-del btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></div>
		    </div>
		<?php
		    }
		}
		?>
	    </div>
	    <div class="row" style="margin-bottom: 15px;">
		<div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][form][]" value="Form"></div>
		<div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][field][]" value="Field"></div>
		<div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][cond][]" value="Condition"></div>
		<div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][value][]" value="Value1"></div>
		<div class="col-md-2 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][value][]" value="Value2"></div>
		<div class="col-md-1 sdbox-col"><input type="text" class="form-control " disabled="disabled" name="forms[result][condition][value][]" value="More"></div>
		<div class="col-md-1 sdbox-col"><button type="button" data-url="<?=  Url::to(['/inv/inv-filter-sub/get-widget', 'view'=>'_widget_form_condition'])?>" class="forms-condition-add btn btn-success"><i class="glyphicon glyphicon-plus"></i></button></div>
	    </div>
	    
	</div>
	
	<?= $form->field($model, 'options')->hiddenInput()->label(FALSE) ?>
	
	<?= $form->field($model, 'urine_status')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'sitecode')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'filter_id')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'gid')->hiddenInput()->label(false) ?>
    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    $(document).find('#modal-inv-person').modal('hide');
	    window.location.href = '".yii\helpers\Url::to(['/inv/inv-person/index', 'module'=>$module, 'comp'=>$comp,'ovfilter_sub'=>''])."'+result.data.sub_id
	    //$.pjax.reload({container:'#inv-person-grid-pjax'});
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});


$('.condition-view').on('click', '.forms-condition-del', function(){
    $(this).parent().parent().remove();
});

$('.condition-view').on('click', '.forms-condition-add', function(){
    getWidget($(this).attr('data-url') , $(this).parent().parent().parent().find('.forms-condition-items'));
});

$('.condition-view').on('change', '.cform-input', function(){
    getSubField($(this).find('option:selected').val(), $(this).parent().parent().find('.cfield-input'));
});

$('.condition-view').on('change', '.cfield-input', function(){
    checkField($(this).find('option:selected').val(), $(this).parent().parent().find('.cform-input').val(), $(this).parent().parent().find('.cvalue1-input'), $(this).parent().parent().find('.cvalue2-input'));
});

$('.condition-view').on('change', '.ccond-input', function(){
    if($(this).val()=='between'){
	$(this).parent().parent().find('.cvalue2-input').removeAttr('readonly');
    } else {
	$(this).parent().parent().find('.cvalue2-input').attr('readonly','readonly');
	$(this).parent().parent().find('.cvalue2-input').val('');
    }
});

var cond = $('input:radio[name=\"InvFilterSub[filter_order]\"]:checked').val();
if(cond==1){
    $('.condition-view').show();
} else {
    $('.condition-view').hide();
    $('.forms-condition-items').html('');
}

$('input:radio[name=\"InvFilterSub[filter_order]\"]').change(function(){
    if($(this).val()==1){
	$('.condition-view').show();
    } else {
	$('.condition-view').hide();
	$('.forms-condition-items').html('');
    }
});

function getSubField(valField, appendId){
    if(valField === undefined || valField === null){
    
    } else {
	$.ajax({
	    method: 'POST',
	    url: '".Url::to(['/inv/inv-person/getfields'])."',
	    data: {id:valField},
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$(appendId).html(result);
	    }
	});
    }
}

function checkField(valField, valForm, val1ID, val2ID){
    $.ajax({
	method: 'POST',
	url: '".Url::to(['/inv/inv-person/checkfields'])."',
	data: {fname:valField, formid:valForm},
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.type==1){
		$(val1ID).datepicker({
		    dateFormat: 'yy-mm-dd'
		});

		$(val2ID).datepicker({
		    dateFormat: 'yy-mm-dd'
		});
		
	    } else {
		
		$(val1ID).datepicker('destroy');
		$(val2ID).datepicker('destroy');
	    }
	}
    });
}

function getWidget(url, appendId, id=0) {
    $.ajax({
	method: 'POST',
	url: url,
	data: {id:id, data_sub:".\yii\helpers\Json::encode($dataSubForm)."},
	dataType: 'HTML',
	success: function(result, textStatus) {
	    $(appendId).append(result);
	}
    });
}

");?>