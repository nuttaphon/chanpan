<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvMain */

$this->title = Yii::t('app', 'หน้าแรก');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-home">
    <?php
    if($module>0){
	echo $this->render('/inv-person/_menu', ['module'=>$module, 'comp'=>$comp]);
    }
    ?>
    <div class="container">
    <?php
           
            echo '<div class="row">';
                echo '<div class="col-md-12 " style="padding-top: 15px;">';
                    echo '<div class="alert" id="report-overview-info">';
                       
                        echo '<label id="report-overview-lastcal"></label> ';
                        echo '<button class="btn btn-primary" id="report-overview-refresh" sitecode="'.$mysitecode.'">Refresh <i class="fa fa-refresh"></i></button>';
                        echo '<br/><br/>';
                        echo '<label id="report-overview-daterange"></label> ';

                    echo '</div>';
                    echo '<div class="alert table-responsive" id="report-overview-container">';
                        echo $table;
                    echo '</div>';
                echo '</div>';
            echo '</div>';
	?>
	
	</div>
    
	<?php
        $this->registerJs('
            $("#report-overview-refresh").off().click(function(){

                $(this).children("i").addClass("fa-spin");
                $("#overview-report-table").addClass("fadeout");

                $.ajax({
                    type    : "GET",
                    cache   : false,
                    url     : "'.Url::to('/inv/inv-fix/report-overview-refresh/').'",
                    data    : {
                        sitecode: $(this).attr("sitecode"),
			module: "'.$module.'",
                    },
                    success  : function(response) {
                        $("#report-overview-container").html(response);
                        $("#overview-report-table").removeClass("fadeout");
                        $("#report-overview-refresh i").removeClass("fa-spin");
                    },
                    error : function(){
                        $("#report-overview-refresh i").removeClass("fa-spin");
                    }
                });
            });
        ');
    ?>
</div>
