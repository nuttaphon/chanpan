<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\inv\models\InvFilterSub */

$this->title = Yii::t('app', 'สร้างใบกำกับงาน');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inv Filter Subs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inv-filter-sub-create">

    <?= $this->render('_form', [
        'model' => $model,
	'comp' => $comp,
    ]) ?>

</div>
