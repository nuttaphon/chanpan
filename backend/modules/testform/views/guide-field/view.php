<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\testform\models\GuideField */

$this->title = 'Guide Field#'.$model->auto_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guide Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-field-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'auto_id',
		'uid',
		'text_input',
		'textarea:ntext',
		'email:email',
		'number',
		'int_input',
		'checkbox',
		'checkbox_list:ntext',
		'radio',
		'multiple:ntext',
		'dropdown',
		'readonly',
		'disabled',
		'textmask_input',
		'optional_icons',
		'select2',
		'dropdown_db',
		'file:ntext',
		'date',
		'time',
		'datetime',
		'create_by',
		'create_time',
		'update_by',
		'update_time',
	    ],
	]) ?>
    </div>
</div>
