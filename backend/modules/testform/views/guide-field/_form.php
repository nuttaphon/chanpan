<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\testform\models\GuideField */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="guide-field-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Guide Field</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'uid')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'text_input')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'textarea')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'number')->textInput() ?>

	<?= $form->field($model, 'int_input')->textInput() ?>

	<?= $form->field($model, 'checkbox')->textInput() ?>

	<?= $form->field($model, 'checkbox_list')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'radio')->textInput() ?>

	<?= $form->field($model, 'multiple')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'dropdown')->textInput() ?>

	<?= $form->field($model, 'readonly')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'disabled')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'textmask_input')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'optional_icons')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'select2')->textInput() ?>

	<?= $form->field($model, 'dropdown_db')->textInput() ?>

	<?= $form->field($model, 'file')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'date')->textInput() ?>

	<?= $form->field($model, 'time')->textInput() ?>

	<?= $form->field($model, 'datetime')->textInput() ?>

	<?= $form->field($model, 'create_by')->textInput() ?>

	<?= $form->field($model, 'create_time')->textInput() ?>

	<?= $form->field($model, 'update_by')->textInput() ?>

	<?= $form->field($model, 'update_time')->textInput() ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#guide-field-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-guide-field').modal('hide');
		$.pjax.reload({container:'#guide-field-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>