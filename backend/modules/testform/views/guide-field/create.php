<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\testform\models\GuideField */

$this->title = Yii::t('app', 'Create Guide Field');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guide Fields'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-field-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
