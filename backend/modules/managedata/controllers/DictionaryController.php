<?php

namespace backend\modules\managedata\controllers;

use backend\modules\ezforms\models\EzformFields;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class DictionaryController extends Controller
{
	public function actionIndex($ezf_id)
	{
		$dataProvider = new ActiveDataProvider([
			'query' => EzformFields::find()->where('ezf_id = :ezf_id AND ezf_field_type <> 2 AND ezf_field_type <> 15 AND ezf_field_type <> 13', [':ezf_id' => $ezf_id])->orderBy(['ezf_field_id' => SORT_ASC]),
			'pagination' => [
				'pageSize' => 100,
			],
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}
}