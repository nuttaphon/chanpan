<?php

namespace backend\modules\managedata\controllers;

use Yii;
use backend\modules\managedata\models\Managedata;
use backend\modules\managedata\models\ManagedataSearch;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use backend\modules\managedata\classes\ManagerQuery;
use backend\modules\managedata\classes\ManagerFunc;
use common\lib\tcpdf\SDPDF;
/**
 * ManagedataController implements the CRUD actions for Managedata model.
 */
class ManagedataController extends Controller
{
	public function actionComingsoon(){

		$this->layout = '@backend/views/layouts/common';

		return $this->render('_comingsoon.php');
	}
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['create', 'update', 'delete'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all Managedata models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ManagedataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionFromTdc()
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	Yii::$app->session['db_zone'] = ManagerQuery::getZone($sitecode);

        //$query = Yii::$app->dbsvr1->createCommand("");
        //->andWhere(['ezform.status' => 1])
//        \yii\helpers\VarDumper::dump($query->createCommand()->sql, 10, TRUE);
//        echo Yii::$app->user->id;
//        Yii::$app->end();
        $dataProvider = new SqlDataProvider([
            'sql' => "select ezform.ezf_id, ezform.ezf_name, ezform.ezf_table , (SELECT ezform_map_f43.f43 FROM ezform_map_f43 WHERE ezform_map_f43.ezf_id = ezform.ezf_id) AS f43, (SELECT tdc_count.total FROM tdc_count WHERE tdc_count.ezf_id = ezform.ezf_id) AS total FROM ezform WHERE ezform.shared = '99' and ezform.ezf_name like '(%' ORDER BY ezf_name",
            'pagination' => array(
                'pageSize' => 20,
            ),
	    'db'=>'dbwebs1'
        ]);

        return $this->render('index2', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
//    public function actionTdcCount()
//    {
//	 $data = \backend\modules\ezforms\models\Ezform::find()
//            ->select('ezform.*')
//            ->where("shared = '99' and ezf_name like '(%' ORDER BY ezf_name")->all();
//	    //\appxq\sdii\utils\VarDumper::dump($data);
//	echo 'Start';
//	foreach ($data as $key => $value) {
//	    Yii::$app->db->createCommand()->insert('tdc_count', [
//		'ezf_id'=>$value['ezf_id'],
//		'ezf_name'=>$value['ezf_name'],
//		'total'=>0,
//	    ])->execute();
//	}
//	echo ' -> End';
//    }

    public function actionViewTdc()
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M');
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	//Yii::$app->session['db_zone'] = ManagerQuery::getZone($sitecode);
	
	
	if (isset($_COOKIE['save_keyckd']) && $_COOKIE['save_keyckd']==1) {
	    if (isset($_COOKIE['key_ckd'])) {
		Yii::$app->session['key_ckd'] = $_COOKIE['key_ckd'];
	    }
	    if (isset($_COOKIE['convert_ckd'])) {
		Yii::$app->session['convert_ckd'] = $_COOKIE['convert_ckd'];
	    }
	    if (isset($_COOKIE['save_keyckd'])) {
		Yii::$app->session['save_keyckd'] = $_COOKIE['save_keyckd'];
	    }
	} else {
	    Yii::$app->session['key_ckd'] = '';
	    Yii::$app->session['convert_ckd'] = 0;
	    Yii::$app->session['save_keyckd'] = 0;
	    
	    setcookie('save_keyckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    setcookie('key_ckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    setcookie('convert_ckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	}
	
	
	    if (isset($_GET['save_keyckd']) && $_GET['save_keyckd']==1) {
		setcookie("save_keyckd", $_GET['save_keyckd'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		setcookie("key_ckd", $_GET['key'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		setcookie("convert_ckd", $_GET['convert'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    
		
	    }
	    
	$params=Yii::$app->request->queryParams;
        $ezf_id=$params['id'];
	$table=$params['table'];  
	
//        $getOwn = \common\lib\codeerror\helpers\CheckOwn::checkOwnForm($ezf_id);
//        if($getOwn == 0){
//            Yii::$app->getSession()->setFlash('alert', [
//		'body'=> 'ท่านไม่มีสิทธิ์ใช้งานฟอร์มนี้ ',
//		'options'=>['class'=>'alert-danger']
//	    ]);
//
//	    return $this->redirect(['/managedata/managedata/from-tdc']);
//        }
        
	if(isset($_GET['key'])){    
	    Yii::$app->session['key_ckd'] = $_GET['key'];
	    Yii::$app->session['convert_ckd'] = $_GET['convert'];
	    Yii::$app->session['save_keyckd'] = $_GET['save_keyckd'];
	    
	    return $this->redirect(['view-tdc',
		'id'=>$ezf_id,
		'table'=>$table,
	    ]);
	} 
	
	$convert = isset(Yii::$app->session['convert_ckd']) ? Yii::$app->session['convert_ckd'] : 0;
	$key = isset(Yii::$app->session['key_ckd']) ?Yii::$app->session['key_ckd'] : '';
	$save_keyckd = isset(Yii::$app->session['save_keyckd']) ?Yii::$app->session['save_keyckd'] : 0;
	
	
	$show = isset($_GET['show'])?$_GET['show']:'one';
	
        
	
        $sql = "SELECT * FROM ezform WHERE ezf_id='".$ezf_id."' ";
        $dataForm = \Yii::$app->dbwebs1->createCommand($sql)->queryOne();
        
        $dataColumn = ManagerQuery::getColumn($table);
	
	//ตรวจสอบ owner
	Yii::$app->session['tdc_owner'] = 0;
//	$sqlOwner = "SELECT * FROM 
//(SELECT ezform.ezf_id, 
//	query_manager.user_id AS manager, 
//	ezform_co_dev.user_co AS co_dev, 
//	ezform.user_create AS ez_owner
//FROM ezform LEFT OUTER JOIN ezform_co_dev ON ezform.ezf_id = ezform_co_dev.ezf_id, 
//	query_manager) tmp_owner
//WHERE tmp_owner.ezf_id = :ezf_id AND (tmp_owner.co_dev = :user OR tmp_owner.manager = :user OR tmp_owner.ez_owner = :user)";
//        $dataOwner = Yii::$app->dbwebs1->createCommand($sqlOwner, [':ezf_id'=>$ezf_id, ':user'=>Yii::$app->user->id])->queryAll();
//	
//	if($dataOwner){
//	    Yii::$app->session['tdc_owner'] = 1;
//	}
	
	$show_list = ['one'=>'เฉพาะไซต์ตัวเองกรอก'];
	$sql = "SELECT site_url.`code`, 
		site_url.`name`, 
		site_url.url
	FROM site_url";
	$data = Yii::$app->db->createCommand($sql)->queryAll();
	foreach ($data as $value) {
	    $show_list[$value['code']] = $value['name'];
	}
	
        $searchModel = new \backend\modules\managedata\models\TbDataTdcSearch();
        $searchModel->setTableName($table);
        $searchModel->setEZFid($ezf_id);
        $dataProvider = $searchModel->export(Yii::$app->request->queryParams, Yii::$app->session['tdc_owner'], $show, $key, $convert);
	//\appxq\sdii\utils\VarDumper::dump($dataProvider->getModels());
        return $this->render('view-tdc', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataColumn'=>$dataColumn,
	    'show_list' => $show_list,
	    'show' => $show,
	    'dataForm'=>$dataForm,
	    
        ]);
    }
    public function actionEzformView() {
	//eyJzaXRlY29kZSI6IjExMDUyIiwiSE9TUENPREUiOiIxMTA1MiIsIlBJRCI6IiJ9
	//MTEwNTIxMTA1Mg==
	    $table = isset($_GET['table'])?$_GET['table']:'';
	    
	    $data_set = isset($_GET['data_set'])?\yii\helpers\Json::decode(base64_decode($_GET['data_set'])):[];
	    $key_ckd = isset($_GET['key_ckd'])?base64_decode($_GET['key_ckd']):'';
	    $convert_ckd = isset($_GET['convert_ckd'])?$_GET['convert_ckd']:0;
	    
	    $ezf_id = ManagerQuery::getMapEzf($table);
	    
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    Yii::$app->session['db_zone'] = ManagerQuery::getZone($sitecode);
	    
	    Yii::$app->session['key_ckd'] = $key_ckd;
	    Yii::$app->session['convert_ckd'] = $convert_ckd;
	    try {
		$model = new \backend\modules\managedata\models\TbDataTdc();
		$model->setTableName($table);

		if(empty($data_set)){
		    $query = $model->find();
		} else {
		    $query = $model->find()->where($data_set);
		    
		}
		
		$select = ManagerFunc::decodeTable($table, Yii::$app->session['key_ckd'], Yii::$app->session['convert_ckd']);
		
		if(isset($select) && !empty($select)){
		    $query->select($select);
		    
		}
		//\appxq\sdii\utils\VarDumper::dump($query->createCommand()->rawSql);
		
		$data = $query->one();
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		$attributes = [];
		foreach ($data->attributes as $key => $value) {
		    $attributes[strtolower($key)] = $value;
		}
		
		$model_gen->attributes = $attributes;
		
		return $this->render('ezform-view', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		]);

	    } catch (\yii\db\Exception $e) {
		throw new NotFoundHttpException('Query error.');
	    }
	    
    }
    
    public function actionEzform() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    $table = isset($_GET['table'])?$_GET['table']:'';
	    $ezf_id = isset($_GET['ezf_id'])?$_GET['ezf_id']:0;
	    
	    try {
		$model = new \backend\modules\managedata\models\TbDataTdc();
		$model->setTableName($table);

		$query = $model->find()->where($_POST);
		
		$select = ManagerFunc::decodeTable($table, Yii::$app->session['key_ckd'], Yii::$app->session['convert_ckd']);
		
		if(!empty($select)){
		    $query->select($select);
		}
		
		$data = $query->one();
		
		$modelform = \backend\modules\managedata\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		$modelfield = \backend\modules\managedata\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		$attributes = [];
		foreach ($data->attributes as $key => $value) {
		    $attributes[strtolower($key)] = $value;
		}
		
		$model_gen->attributes = $attributes;
		
		$html = $this->renderAjax('_ezform_popup', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		]);
		
		$result = [
		    'status' => 'success',
		    'action' => 'ezform',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		    'html' => $html,
		];
		return $result;

	    } catch (\yii\db\Exception $e) {
		$result = [
		    'status' => 'error',
		    'action' => 'ezform',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Query error.'),
		    'html' => '',
		];
		return $result;
	    }
	    
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionEzformPdf($ezf_id, $dataid) {
	$url = Yii::getAlias('@frontendUrl').\yii\helpers\Url::to(['/site/ezform-print', 'ezf_id'=>$ezf_id, 'dataid'=>$dataid]);
	
	$link = \yii\helpers\Url::to(['/http://FreeHTMLtoPDF.com', 'convert'=>$url]);
	//\appxq\sdii\utils\VarDumper::dump(substr($link, 1));
	
	$this->redirect(substr($link, 1));
    }
    
    public function actionEzformPrint($ezf_id, $dataid) {
	
	    try {
		
		$modelform = \backend\modules\ezforms\models\Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();
		
		$modelfield = \backend\modules\ezforms\models\EzformFields::find()
			->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
			->orderBy(['ezf_field_order' => SORT_ASC])
			->all();
		
		$table = $modelform->ezf_table;
		
		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
		
		$model = new \backend\models\Tbdata();
		$model->setTableName($table);
		
		$query = $model->find()->where('id=:id', [':id'=>$dataid]);
		
		$data = $query->one();
	    
		$model_gen->attributes = $data->attributes;
		
		return $this->renderAjax('_ezform_print', [
		    'modelform'=>$modelform,
		    'modelfield'=>$modelfield,
		    'model_gen'=>$model_gen,
		]);

	    } catch (\yii\db\Exception $e) {
		
	    }
	    
    }
    
    public function actionViewData()
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','512M');
	
	$show = isset($_GET['show'])?$_GET['show']:'one';
	
        $params=Yii::$app->request->queryParams;
        $ezfid=$params['id'];
        $sql = "SELECT * FROM ezform WHERE ezf_id='".$ezfid."' ";
        $dataForm = \Yii::$app->db->createCommand($sql)->queryOne();
        $sqlColumn = "SELECT COLUMN_NAME AS `column` FROM INFORMATION_SCHEMA.COLUMNS "
                                    . "WHERE TABLE_NAME = :tbname AND table_schema = :tbschema";
        $dataColumn = \Yii::$app->db->createCommand($sqlColumn, [':tbname' => $dataForm["ezf_table"], ':tbschema' => explode('=', getenv('DB_DSN'))['3']])->queryAll();
		
	$dataField = \backend\modules\ezforms\components\EzformQuery::getFieldsByEzf_id($dataForm['ezf_id']);
		
	
	//ตรวจสอบ เป้าหมาย
	$objective = 0;
	$sqlObj = "SELECT ezform.ezf_id, 
	ezform_component.special
FROM ezform INNER JOIN ezform_component ON ezform.comp_id_target = ezform_component.comp_id
WHERE ezform.ezf_id = :ezf_id AND IFNULL(ezform_component.special,0) <> 0 ";
	$dataObj = Yii::$app->db->createCommand($sqlObj, [':ezf_id'=>$dataForm['ezf_id']])->queryAll();
	
	if($dataObj){
	    $objective = 1;
	}
	
	//ตรวจสอบ owner
	$owner = 0;
	$sqlOwner = "SELECT * FROM 
(SELECT ezform.ezf_id, 
	query_manager.user_id AS manager, 
	ezform_co_dev.user_co AS co_dev, 
	ezform.user_create AS ez_owner
FROM ezform LEFT OUTER JOIN ezform_co_dev ON ezform.ezf_id = ezform_co_dev.ezf_id, 
	query_manager) tmp_owner
WHERE tmp_owner.ezf_id = :ezf_id AND (tmp_owner.co_dev = :user OR tmp_owner.manager = :user OR tmp_owner.ez_owner = :user)";
        $dataOwner = Yii::$app->db->createCommand($sqlOwner, [':ezf_id'=>$dataForm['ezf_id'], ':user'=>Yii::$app->user->id])->queryAll();
	
	if($dataOwner){
	    $owner = 1;
	}
	
	if($objective){
	    $show_list = ['all'=>'ทั้งหมด', 'one'=>'เฉพาะไซต์ตัวเองกรอก'];
	    $sql = "SELECT site_url.`code`, 
	site_url.`name`, 
	site_url.url
FROM site_url";
	    $data = Yii::$app->db->createCommand($sql)->queryAll();
	    foreach ($data as $value) {
		$show_list[$value['code']] = $value['name'];
	    }
	} else {
	    $show_list = ['all'=>'ทั้งหมด', 'one'=>'เฉพาะตัวเองกรอก'];
	    $sql = "SELECT user_profile.user_id, 
	user_profile.firstname, 
	user_profile.middlename, 
	user_profile.lastname, 
	CONCAT(user_profile.firstname, ' ', user_profile.lastname) AS fullname,
	user_profile.sitecode,
	`user`.username
FROM `user` INNER JOIN user_profile ON `user`.id = user_profile.user_id";
	    $data = Yii::$app->db->createCommand($sql)->queryAll();
	    foreach ($data as $value) {
		$show_list[$value['user_id']] = trim($value['fullname'])!=''?$value['fullname']:$value['username'];
	    }
	}
	
	//sql custom 
	
	$dataExport = Yii::$app->db->createCommand("SELECT * FROM ezform_export WHERE status = 1 AND tbname = :tbname ", [':tbname'=>$dataForm["ezf_table"]])->queryOne();
	Yii::$app->session['data_export_tmp'] = $dataExport;
	
        $searchModel = new \backend\models\TbdataSearch();
        $searchModel->setTableName($dataForm["ezf_table"]);
        $searchModel->setEZFid($ezfid);
        $dataProvider = $searchModel->export(Yii::$app->request->queryParams, $owner, $objective, $show, $dataExport);
	$dataProvider->pagination->pageSize = 100;
	//$dpExport = $searchModel->export(Yii::$app->request->queryParams, $owner, $objective, $show);
        return $this->render('view-data', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataColumn'=>$dataColumn,
            'dataForm'=>$dataForm,
	    'dataField'=>$dataField,
	    'owner' => $owner,
	    'objective' => $objective,
	    'show_list' => $show_list,
	    'show' => $show,
	    'dataExport'=>$dataExport,
        ]);
    }
    
    public function actionReportExel() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    $id = isset($_GET['id'])?$_GET['id']:0;
	    $table = isset($_GET['table'])?$_GET['table']:'';
	    $show = isset($_GET['show'])?$_GET['show']:'one';

	    $url = \yii\helpers\Url::to(['report-export', 'id'=>$id, 'table'=>$table, 'show'=>$show]);

	    $html = \backend\modules\ovcca\classes\OvccaFunc::getIframe($url);
	    $result = [
		'status' => 'success',
		'action' => 'report',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		'html' => $html,
	    ];
	    return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
     public function actionReportExport($id, $table, $show)//
    {
	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit','1012M');
	
	$colFields = \backend\modules\inv\classes\InvQuery::getFields($id);
	$colFields_label = \yii\helpers\ArrayHelper::map($colFields, 'id', 'name');
	
	
	$objPHPExcel = new \common\lib\phpexcel\SDExcelView();
	
	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Damasac")
				    ->setLastModifiedBy("Damasac")
				    ->setTitle("Export Document")
				    ->setSubject("Export Document")
				    ->setDescription("Export data")
				    ->setKeywords("data")
				    ->setCategory("data");
	// Add some data
	$data;
	//ตรวจสอบ เป้าหมาย
	$objective = 0;
	$sqlObj = "SELECT ezform.ezf_id, 
		ezform_component.special
	FROM ezform INNER JOIN ezform_component ON ezform.comp_id_target = ezform_component.comp_id
	WHERE ezform.ezf_id = :ezf_id AND IFNULL(ezform_component.special,0) <> 0 ";
		$dataObj = Yii::$app->db->createCommand($sqlObj, [':ezf_id'=>$id])->queryAll();

		if($dataObj){
		    $objective = 1;

		}

		//ตรวจสอบ owner
		$owner = 0;
		$sqlOwner = "SELECT * FROM 
	(SELECT ezform.ezf_id, 
		query_manager.user_id AS manager, 
		ezform_co_dev.user_co AS co_dev, 
		ezform.user_create AS ez_owner
	FROM ezform LEFT OUTER JOIN ezform_co_dev ON ezform.ezf_id = ezform_co_dev.ezf_id, 
		query_manager) tmp_owner
	WHERE tmp_owner.ezf_id = :ezf_id AND (tmp_owner.co_dev = :user OR tmp_owner.manager = :user OR tmp_owner.ez_owner = :user)";
		$dataOwner = Yii::$app->db->createCommand($sqlOwner, [':ezf_id'=>$id, ':user'=>Yii::$app->user->id])->queryAll();

		if($dataOwner){
		    $owner = 1;
		}
		
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
		$queryx = new \backend\models\Tbdata();
		$queryx->setTableName($table);
		$query;
		if($objective){
		    if($owner){
			if($show=='all'){
			    $query=$queryx->find()->where("$table.rstat<>3");
			}elseif ($show=='one') {
			    $query=$queryx->find()
			    //->innerJoin('user_profile', $table.'.user_create = user_profile.user_id')
			    ->where("$table.rstat<>3 AND $table.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
			} else {
			    $query=$queryx->find()
			    //->innerJoin('user_profile', $table.'.user_create = user_profile.user_id')
			    ->where("$table.rstat<>3 AND $table.hsitecode = :sitecode", [':sitecode'=>$show]);
			}

		    } else {
			$query=$queryx->find()
			    //->innerJoin('user_profile', $table.'.user_create = user_profile.user_id')
			    ->where("$table.rstat<>3 AND $table.hsitecode = :sitecode", [':sitecode'=>$sitecode]);
		    }
		} else {
		    if($owner){
			if($show=='all'){
			    $query=$queryx->find()->where("$table.rstat<>3");
			}elseif ($show=='one') {
			    $query=$queryx->find()
			    ->innerJoin('user_profile', $table.'.user_create = user_profile.user_id')
			    ->where("$table.rstat<>3 AND user_profile.user_id = :user_id", [':user_id'=>Yii::$app->user->id]);
			} else {
			    $query=$queryx->find()
			    ->innerJoin('user_profile', $table.'.user_create = user_profile.user_id')
			    ->where("$table.rstat<>3 AND user_profile.user_id = :user_id", [':user_id'=>$show]);
			}
		    } else {
			$query=$queryx->find()
			    ->innerJoin('user_profile', $table.'.user_create = user_profile.user_id')
			    ->where("$table.rstat<>3 AND user_profile.user_id = :user_id", [':user_id'=>Yii::$app->user->id]);
		    }
		}
		
		$sqlColumn = "SELECT COLUMN_NAME AS `column` FROM INFORMATION_SCHEMA.COLUMNS "
                                    . "WHERE TABLE_NAME = :tbname AND table_schema = :tbschema";
		$dataColumn = \Yii::$app->db->createCommand($sqlColumn, [':tbname' => $table, ':tbschema' => explode('=', getenv('DB_DSN'))['3']])->queryAll();
	
		
		//sql custom 
		$moreColumn = [];
		$dataExport = Yii::$app->session['data_export_tmp'];
		if($dataExport){
		    if(trim($dataExport['export_select'])!=''){
			$sql_col = explode('#', $dataExport['export_select']);
			if(is_array($sql_col)){
			    $query->select($sql_col);
			}
		    }

		    if(trim($dataExport['export_join'])!=''){
			$sql_join = explode('@', $dataExport['export_join']);
			if(is_array($sql_join)){
			    foreach ($sql_join as $keyJoin => $valueJoin) {
				$sql_join_items = explode('#', $valueJoin);
				if(is_array($sql_join_items)){
				    $typeJoin = trim($sql_join_items[0]);
				    $tableJoin = trim($sql_join_items[1]);
				    $onJoin = trim($sql_join_items[2]);

				    $query->join($typeJoin, $tableJoin, $onJoin);
				}
			    }
			}
		    }

		    if(trim($dataExport['export_where'])!=''){
			$query->andWhere($dataExport['export_where']);
		    }
		    
		    if(isset($dataExport['lable_extra']) && $dataExport['lable_extra']!=''){
			$lable_extra = \yii\helpers\Json::decode($dataExport['lable_extra']);
			if(is_array($lable_extra)){
			    $moreColumn = $lable_extra;
			    $colKey = \yii\helpers\ArrayHelper::getColumn($dataColumn, 'column');

			    foreach ($moreColumn as $keyMore => $valueMore) {
				if(!in_array($keyMore, $colKey)){
				    $dataColumn[] = ['column'=>$keyMore];
				}
			    }

			}
		    }
		}
		
		
		
		$dataColumn = array_merge($dataColumn, $moreColumn);
		$colFields_label = array_merge($colFields_label, $moreColumn);
		$colFields_label = array_merge($colFields_label, ['user_create'=>'สร้างโดย','create_date'=>'สร้างวันที่','ptid'=>'PTID','hptcode'=>'hptcode','hsitecode'=>'hsitecode']);
		
		$colFields = array_keys($colFields_label);
	
		$data = $query->all();
		
		//-----
	
		
		
	if($dataColumn){
	    $columnTable = \yii\helpers\ArrayHelper::getColumn($dataColumn, 'column');
	    
	    $column = 'A';
	    $row=1;
	    foreach($colFields as $key => $field_name) {
				
                if(!in_array($field_name, $columnTable)){
			continue;
		}
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($column . $row, $field_name);
		$column++;
	    }
	    
	    $column = 'A';
	    $row=2;
            foreach($colFields as $key => $field_name) {
				
                if(!in_array($field_name, $columnTable)){
			continue;
		}
                $columnHint = isset($colFields_label[$field_name])?$colFields_label[$field_name]:$colFields_label[$field_name];
		
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($column . $row, $columnHint);
		$column++;
	    }
	    
	    if($data){
		
		$row=3;
		foreach ($data as $key => $value) {
		    $column = 'A';
		    foreach($colFields as $key => $field_name) {
				
			if(!in_array($field_name, $columnTable)){
				continue;
			}
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($column . $row, $value[$field_name]);
			
			$objPHPExcel->getActiveSheet()->getCell($column . $row)->setValueExplicit($value[$field_name], \PHPExcel_Cell_DataType::TYPE_STRING);
			
			$column++;
		    }
		    $row++;
		}

	    }
	}
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Export data');
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="export_data.xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
    }
    
    /**
     * Displays a single Managedata model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Creates a new Managedata model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new Managedata();

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Updates an existing Managedata model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {

		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing Managedata model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if ($this->findModel($id)->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Finds the Managedata model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Managedata the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Managedata::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
