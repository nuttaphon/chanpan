<?php

namespace backend\modules\managedata\controllers;

use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Controller;

class AnnotatedController extends Controller
{
	public function actionIndex($ezf_id)
	{
		$modelfield = EzformFields::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->orderBy(['ezf_field_order' => SORT_ASC])->all();
		$modelezform = Ezform::find()->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->one();

		$model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);


        if($_GET['print']==1){
            return $this->renderAjax('index', [
                'modelfield' => $modelfield,
                'model_gen' => $model_gen,
                'modelezform' => $modelezform,
            ]);
        } else {
            return $this->render('index', [
                'modelfield' => $modelfield,
                'model_gen' => $model_gen,
                'modelezform' => $modelezform,
            ]);
        }
	}
}