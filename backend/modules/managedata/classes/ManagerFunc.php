<?php
namespace backend\modules\managedata\classes;

use Yii;
use backend\modules\managedata\classes\ManagerQuery;

/**
 * ManagerFunc class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 16 พ.ย. 2559 14:05:38
 * @link http://www.appxq.com/
 * @example 
 */
class ManagerFunc {
    public static function getDbZone($zone) {
	if(in_array($zone, ['01','02','07'])){
	    return Yii::$app->dbsvr1;
	} elseif(in_array($zone, ['03','04','08'])){
	    return Yii::$app->dbsvr2;
	} elseif(in_array($zone, ['05','06','09'])){
	    return Yii::$app->dbsvr3;
	} elseif(in_array($zone, ['10', '11', '12', '13'])){
	    return Yii::$app->dbsvr4;
	}
	
	return FALSE;
    }
    
    public static function queryAll($zone, $sql, $param=[]) {
	$dbcon = self::getDbZone($zone);
	if($dbcon){
	    return $dbcon->createCommand($sql, $param)->queryAll();
	}
	return FALSE;
    }
    
    public static function queryOne($zone, $sql, $param=[]) {
	$dbcon = self::getDbZone($zone);
	if($dbcon){
	    return $dbcon->createCommand($sql, $param)->queryOne();
	}
	return FALSE;
    }
    
    public static function queryScalar($zone, $sql, $param=[]) {
	$dbcon = self::getDbZone($zone);
	if($dbcon){
	    return $dbcon->createCommand($sql, $param)->queryScalar();
	}
	return FALSE;
    }
    
    public static function decodeSql($field, $key, $convert) {
	$str = "decode(unhex($field),sha2('$key',256)) AS $field";
	if($convert==1){
	    $str = "convert(decode(unhex($field),sha2('$key',256)) using tis620) AS $field";
	}
	
	return $str;
    }
    
    public static function decodeTable($table, $key, $convert) {
	if($table=='f_person'){
	    $select = self::decodeFields($table, ['Father', 'Mother','Couple' , 'CID', 'HN', 'Pname', 'Name', 'Lname', 'HOUSENO', 'VILLCODE','VILLNAME','VILLAGE'], $key, $convert);
	} elseif($table=='f_address') {
	    $select = self::decodeFields($table, ['House_id', 'HouseNo', 'ROOMNO', 'TelePhone', 'Mobile'], $key, $convert);
	}
	return $select;
    }
    
    public static function decodeFields($table, $change, $key, $convert) {
	$select = [];
	$dataColumn = ManagerQuery::getColumn($table);
	$columnTable = \yii\helpers\ArrayHelper::getColumn($dataColumn, 'column');
	foreach ($columnTable as $field) {
	    if(in_array($field, $change)){
		$select[] = \backend\modules\managedata\classes\ManagerFunc::decodeSql($field, $key, $convert);
	    } else {
		$select[] = $field;
	    }
	}

	return $select;
    }
    
}
