<?php

namespace backend\modules\managedata\models;

use Yii;

class TbDataTdc extends \yii\db\ActiveRecord
{
    protected static $table;
    
    public function _construct($table, $scenario)
    {
        parent::_construct($scenario);
        self::$table = $table;
    }
   
    public static function getDb()
    {
        return \backend\modules\ckdnet\classes\CkdnetFunc::getDb();
    }
    
    public function attributes()
    {
	$attrDB = array_keys(static::getTableSchema()->columns);
	//$attrDB = \backend\modules\ckdnet\classes\CkdnetFunc::convertColumn($attrDB);
	
	return $attrDB;
    }
    
    public static function tableName()
    {
        return self::$table;
    }

    /* UPDATE */
    public static function setTableName($table)
    {
        self::$table = $table;
    }

}
