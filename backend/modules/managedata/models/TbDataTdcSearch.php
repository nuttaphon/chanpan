<?php

namespace backend\modules\managedata\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\managedata\models\TbDataTdc;

/**
 * TbData1Search represents the model behind the search form about `backend\modules\managedata\models\TbData1`.
 */
class TbDataTdcSearch extends TbDataTdc
{
    protected static $table;
    protected static $ezf_id;
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    public static function setTableName($table)
    {
        self::$table = $table;
    }    
    public static function setEZFid($ezf_id)
    {
        self::$ezf_id = $ezf_id;
    }  
    
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
//    public function getDb() {
//	return Yii::$app->dbwebs1;
//    }
    
    public function export($params, $owner, $show, $key, $convert)
    {
        //$query = Tbdata::find();
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
        $queryx = new TbDataTdc();
        $queryx->setTableName(self::$table);
	$query;
	
	if($owner){
	    if($show=='all'){
		$query=$queryx->find();
	    }elseif ($show=='one') {
		$query=$queryx->find()
		->where('sitecode = :sitecode', [':sitecode'=>$sitecode]);
	    } else {
		$query=$queryx->find()
		->where('sitecode = :sitecode', [':sitecode'=>$show]);
	    }

	} else {
	    $query=$queryx->find()
		->where('sitecode = :sitecode', [':sitecode'=>$sitecode]);
	}
	$table = self::$table;
	$select = \backend\modules\managedata\classes\ManagerFunc::decodeTable($table, $key, $convert);
	if(!empty($select)){
	    $query->select($select);
	}
	
	$this->load($params);
	
	
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ]);

        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        return $dataProvider;
    }
    
}
