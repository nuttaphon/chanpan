<?php

namespace backend\modules\managedata\models;

use Yii;

/**
 * This is the model class for table "tb_data_1".
 *
 * @property integer $id
 * @property integer $rstat
 * @property integer $user_create
 * @property string $create_date
 * @property integer $user_update
 * @property string $update_date
 * @property string $value1
 * @property string $value2
 * @property string $value3
 * @property integer $edattype
 */
class TbData1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_data_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'rstat', 'user_create', 'user_update', 'edattype'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['value1', 'value2', 'value3'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rstat' => Yii::t('app', 'Rstat'),
            'user_create' => Yii::t('app', 'User Create'),
            'create_date' => Yii::t('app', 'Create Date'),
            'user_update' => Yii::t('app', 'User Update'),
            'update_date' => Yii::t('app', 'Update Date'),
            'value1' => Yii::t('app', 'Value1'),
            'value2' => Yii::t('app', 'Value2'),
            'value3' => Yii::t('app', 'Value3'),
            'edattype' => Yii::t('app', 'Select EDAT type'),
        ];
    }

    /**
     * @inheritdoc
     * @return TbData1Query the active query used by this AR class.
     */
    public static function find()
    {
        return new TbData1Query(get_called_class());
    }
}
