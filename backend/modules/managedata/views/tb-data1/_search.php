<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\ezforms\models\Ezform;


/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\TbData1Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tb-data1-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="form-group field-guidefield-checkbox_list">
    <?= Html::activeRadioList($model, 'edattype', ['1. Frequency ','2. Summary','3. List Records','Cross-Tabulation with'], ['itemOptions'=>['labelOptions'=>['class'=>'radio-inline']]]) ?>
    <?= Html::error($model, 'edattype') ?>
  </div>
  
    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'value1') ?>

    <?php // echo $form->field($model, 'value2') ?>

    <?php // echo $form->field($model, 'value3') ?>

    <?php // echo $form->field($model, 'edattype') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Display Result'), ['class' => 'btn btn-primary']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
