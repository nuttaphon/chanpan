<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\TbData1 */

$this->title = 'Tb Data1#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Tb Data1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-data1-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'id',
		'rstat',
		'user_create',
		'create_date',
		'user_update',
		'update_date',
		'value1',
		'value2',
		'value3',
		'edattype',
	    ],
	]) ?>
    </div>
</div>
