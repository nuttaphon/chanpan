<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\managedata\models\TbData1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Tb Data1s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-data1-index">

    <div class="sdbox-header">
	<h3><?=  Html::encode($this->title) ?></h3>
    </div>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="padding-top: 10px;">
	<span class="label label-primary">Notice</span>
	<?= Yii::t('app', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.') ?>
    </p>

    <?php  Pjax::begin(['id'=>'tb-data1-grid-pjax']);?>
    <?= SDGridView::widget([
	'id' => 'tb-data1-grid',
	//'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['tb-data1/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-tb-data1']),
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'rstat',
            'user_create',
            'create_date',
            'user_update',
             'update_date',
             'value1',
             'value2',
             'value3',
             'edattype',

            //['class' => 'common\lib\sdii\widgets\SDActionColumn'],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  SDModalForm::widget([
    'id' => 'modal-tb-data1',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#tb-data1-grid-pjax').on('click', '#modal-addbtn-tb-data1', function(){
modalTbData1($(this).attr('data-url'));
});

$('#tb-data1-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalTbData1('".Url::to(['tb-data1/update', 'id'=>''])."'+id);
});

$('#tb-data1-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'view'){
	modalTbData1(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
	    $.post(
		url
	    ).done(function(result){
		if(result.status == 'success'){
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#tb-data1-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function(){
		console.log('server error');
	    });
	})
    }
    return false;
});

function modalTbData1(url) {
    $('#modal-tb-data1 .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-tb-data1').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>
