<?php
/**
 * Created by PhpStorm.
 * User: hackablex
 * Date: 5/25/2016 AD
 * Time: 15:24
 */
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use backend\modules\component\models\EzformComponent;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;


$this->title = 'Data dictionary';
?>
<div class=""><?php echo Html::a('<li class="fa fa-chevron-circle-left"></li> ย้อนกลับก่อนหน้านี้', Yii::$app->request->referrer, ['class' => 'btn btn-success'])?></div><hr>
<?php
echo GridView::widget([
    'dataProvider'=>$dataProvider,
    //'filterModel'=>$searchModel,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'resizableColumns'=>false,
    'columns'=>[
        [
            'class' => 'kartik\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'format' => 'text',
            'label' => 'Variable name',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                if($model->ezf_field_type <> 19 && $model->ezf_field_type <> 23 && $model->ezf_field_type <> 250){
                    //check box
                    return $model->ezf_field_name;
                }
            }
        ],
        [
            'format' => 'text',
            'label' => 'Description',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                if($model->ezf_field_type == 21){
                    if($model->ezf_field_label == 1){
                        return 'จังหวัด';
                    }else if($model->ezf_field_label == 2){
                        return 'อำเภอ';
                    }else if($model->ezf_field_label == 3){
                        return 'ตำบล';
                    }
                }
                return $model->ezf_field_label;
            }
        ],
        [
            'format' => 'html',
            'label' => 'Code',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                if($model->ezf_field_type == 4 || $model->ezf_field_type == 6){
                    $ezformChoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $model->ezf_field_id,])->all();
                    $html ='';
                    foreach ($ezformChoice as $key => $val){
                        $html .= $val->ezf_choicevalue. ' = '. $val->ezf_choicelabel.'<br>';
                    }
                    $html  = substr($html, 0, -4);
                    return $html;
                }else if($model->ezf_field_type == 10){
                    //conponent
                }else if($model->ezf_field_type == 16){
                    //check box
                    return '0 = ไม่ถูกเลือก (ไม่กา)<br>1 = ถูกเลือก (กา)';
                }else if($model->ezf_field_type == 17){
                    return '';
                }else if($model->ezf_field_type == 231){
                    $ezformChoice = \backend\modules\ezforms\models\EzformChoice::find()->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $model->ezf_field_sub_id,])->all();
                    $html ='';
                    foreach ($ezformChoice as $key => $val){
                        $html .= $val->ezf_choicevalue. ' = '. $val->ezf_choicelabel.'<br>';
                    }
                    $html  = substr($html, 0, -4);
                    return $html;
                }else if($model->ezf_field_type == 0){
                    //check box list
                    if($model->ezf_field_name=='id'){
                        return 'Primary key';
                    }
                    $fieldx = EzformFields::find()->select('ezf_field_name, ezf_field_label, ezf_field_type')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$model->ezf_field_sub_id])->one();
                    if($fieldx->ezf_field_type == 19){
                        return '0 = ไม่ถูกเลือก (ไม่กา)<br>1 = ถูกเลือก (กา)';
                    }
                }

            }
        ],
    ],

    'headerRowOptions'=>['class'=>'kartik-sheet-style'],

    'pjax'=>true, // pjax is set to always true for this demo
    'pjaxSettings' => ['options' => ['id' => 'pjax-page-consult',], 'enablePushState' => false],
    // set your toolbar
    'toolbar'=> [

        '{export}',
        '{toggleData}',
    ],
    // set export properties
    'export'=>[
        'fontAwesome'=>true
    ],
    // parameters from the demo form
    'bootstrap'=>true,
    'bordered'=>true,
    'striped'=>true,
    'condensed'=>true,
    'responsiveWrap' => false,
    'responsive' => true,
    'showPageSummary'=>false,
    'pageSummaryRowOptions'=>['class' => 'text-center bg-warning text-bold'],
    'hover'=>false,
    'panel'=>[
        'type'=>GridView::TYPE_PRIMARY,
        'heading'=>'รายการทั้งหมด',
    ],
    'persistResize'=>false,
]);

$this->registerJs("
function changeType(val){
    var url = \"?type=\"+$.trim(val); $(location).attr(\"href\",url);
}
",\yii\web\View::POS_END);
