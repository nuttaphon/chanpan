<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use appxq\sdii\widgets\ModalForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TbdataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$column='A';
$column++;
$this->title = 'จัดการข้อมูล';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'จัดการข้อมูล'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'ดูข้อมูล';




?>
<style>
a.asc:after {
  content: "\e151";
}
a.asc:after, a.desc:after {
  position: relative;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: normal;
  padding-left: 5px;
}
:after, :before {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
a.desc:after {
  content: "\e152";
}
a.asc:after, a.desc:after {
  position: relative;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: normal;
  padding-left: 5px;
}
</style>
<div class="tbdata-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>
        <?= Html::a('Create Tbdata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->
    <br>
    
    <div style='float:right'> 
        <?= Html::a('<i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;กลับไปหน้าเลือกฟอร์ม', ['/managedata/managedata/from-tdc', 'id' => $dataForm["ezf_id"]], ['class' => 'btn btn-warning btn-flat']) ?>
    </div>
    <br>
    <br>
    
    <div class="box">
    <div class="box-body">
<?php
use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
//$columns = [
//    ['class'=>'kartik\grid\SerialColumn', 'order'=>DynaGrid::ORDER_FIX_LEFT],
//    'var2',
//    'var3',
//    'var4',
//    'var6',
//    'var7',
//    'var9',
//    'var10',
//    'var11',
//    'var12',
//    'var13',
//    'var14',
//    'var15',
//    'var16',
//    'var17',
//    'var18',
//    'var19',
//    'var20',
//    ['class'=>'kartik\grid\CheckboxColumn',  'order'=>DynaGrid::ORDER_FIX_RIGHT]
//];

        $columns['0']['class']='kartik\grid\SerialColumn';
	$columns['0']['contentOptions']=['style'=>'min-width:80px;text-align: center;'];
        $columns['0']['order']=DynaGrid::ORDER_FIX_LEFT;
	
	$columns[1]=[
			    'header'=>'Action',
			    'label'=>'Action',
			    'format'=>'raw',
			    'value' => function ($data)
			    {
				return Html::button('<i class="glyphicon glyphicon-eye-open"></i> view', 
				[
				    'class'=>'btn btn-info btn-sm btn-view',
				    'data-url'=>'#',
				]);
			    }
			    //'headerOptions'=>['data-toggle'=>'tooltip', 'title'=>'fsdff23432'],
			    //'contentOptions'=>['style'=>'width:100px; text-align: center;'],
			];
        $aindex=2;
        $params=Yii::$app->request->queryParams;
        $ezfid=$params['id'];
	
	$columnTable = ArrayHelper::getColumn($dataColumn, 'column');
	//$columnTable = backend\modules\ckdnet\classes\CkdnetFunc::convertColumn($columnTable);
	//\appxq\sdii\utils\VarDumper::dump($columnTable);
	if (count($dataColumn)>0) {
            foreach($columnTable as $key => $field_name) {
			
				$title = $field_name;
				$columns[$aindex]=[
				    'attribute'=>$field_name,
				    'label'=>$field_name,
				    'noWrap' => TRUE,
				    'sortLinkOptions' =>['data-toggle'=>'tooltip', 'title'=>$title, 'data-placement'=>'bottom'],
				    //'headerOptions'=>['data-toggle'=>'tooltip', 'title'=>'fsdff23432'],
				    //'contentOptions'=>['style'=>'width:100px; text-align: center;'],
				];
                
                $aindex++;
            }
			
        }
        $columns[$aindex]['class']='kartik\grid\CheckboxColumn';
        $columns[$aindex]['order']=DynaGrid::ORDER_FIX_RIGHT;   


        

     ?>
  
	<div class="row" style="margin-bottom: 10px;">
	    <div class="col-sm-6">
		<?php $form = ActiveForm::begin([
	'id' => 'jump_menu',
        'action' => ['view-tdc', 'id'=>$_GET['id'], 'table'=>$_GET['table']],
        'method' => 'get',
	'layout' => 'horizontal',
    ]); ?>
	<div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-6">
	    <?=  Html::label('กุญแจถอดรหัสข้อมูล')?>
	    <?=  Html::passwordInput('key', isset(Yii::$app->session['key_ckd'])?Yii::$app->session['key_ckd']:'', ['class'=>'form-control', 'placeholder'=> 'กรุณากรอกกุญแจถอดรหัส', 'required'=>true])?>
	</div>
    </div>
	
	<?= Html::checkbox('save_keyckd', isset(Yii::$app->session['save_keyckd'])?Yii::$app->session['save_keyckd']:false, ['label'=>'จดจำคีย์นี้'])?>
	<?= Html::checkbox('convert', isset(Yii::$app->session['convert_ckd'])?Yii::$app->session['convert_ckd']:false, ['label'=>'เข้ารหัสแบบ tis620 (กรณีชื่อ-สกุล อ่านไม่ออกให้กาช่องนี้ด้วย)'])?>
    
    <div class="row" style="margin-bottom: 15px;">
	<div class="col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
	</div>
    </div>
	<?php
	
	    echo Html::dropDownList('show', $show, $show_list, ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()', 'disabled'=>!$owner]);
	
	
	?>
	
    <?php ActiveForm::end(); ?>
	    </div>
	
	</div>
	<?php
//	if (isset($_GET['show']) && $_GET['show']=='all'){
//	    $dataProvider->pagination = ['pageSize'=>$dataProvider->getTotalCount()];
//	    echo 'dsfsdfs';
//	}
//	
$dynagrid = DynaGrid::begin([
    'columns'=>$columns,
    'theme'=>'panel-info',
    'showPersonalize'=>true,
    'storage'=>'cookie',
    'gridOptions'=>[
        'dataProvider'=>$dataProvider,
        //'filterModel'=>$searchModel,
        'showPageSummary'=>true,
        'floatHeader'=>true,
		'responsiveWrap'=>FALSE,
       //'export'=>true,   //Got error at server
        'pjax'=>true,
        'panel'=>[
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i>  ชื่อฟอร์ม '.$dataForm['ezf_name'].'</h3>',
            'before' =>  '<div style="padding-top: 7px;"><em>* ท่านสามารถเรียกดูข้อมูลตามเงื่อนไขได้ทั้งสิ้น '.$dataProvider->getTotalCount().' record. </em>  </div>',
            'after' => false
        ],        
        'toolbar' =>  [
        ],
    ],
    'options'=>['id'=>'dynagrid-1','pageSize'=>50] // a unique identifier is important ,'pageSize'=>Yii::$app->request->get('show')=='all' ? 0 : 50,
]);

if (substr($dynagrid->theme, 0, 6) == 'simple') {
    $dynagrid->gridOptions['panel'] = false;
}  
DynaGrid::end();  
    ?>

</div>
    </div></div>

<?=  ModalForm::widget([
    'id' => 'modal-ov-person',
    'size'=>'modal-lg',
    'tabindexEnable'=>false,
]);
?>

<?php  $this->registerJs("
$('#reportExel, #reportExel1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonExel(url);
});

$('.tbdata-index').on('click', '.btn-view', function() {
    var param = jQuery.parseJSON($(this).parent().parent().attr('data-key'));
    modalView(param, '".Url::to(['/managedata/managedata/ezform', 'table'=>$_GET['table'], 'ezf_id'=>$_GET['id']])."');
});

$('.tbdata-index').on('dblclick', 'tbody tr', function() {
    var param = jQuery.parseJSON($(this).attr('data-key'));
    modalView(param, '".Url::to(['/managedata/managedata/ezform', 'table'=>$_GET['table'], 'ezf_id'=>$_GET['id']])."');
});

function modalView(param, url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: param,
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-ov-person .modal-content').html(result.html);
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonExel(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-ov-person .modal-content').html(result.html);
		
		$('#modal-ov-person').modal('hide');
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

");?>