<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;

/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\Managedata */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="managedata-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>
	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="itemModalLabel">Managedata</h4>
	</div>

	<div class="modal-body">
		<?= $form->field($model, 'ezf_id')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'ezf_name')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'ezf_detail')->textarea(['rows' => 6]) ?>

		<?= $form->field($model, 'ezf_table')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'user_create')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'create_date')->textInput() ?>

		<?= $form->field($model, 'user_update')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'update_date')->textInput() ?>

		<?= $form->field($model, 'status')->textInput() ?>

		<?= $form->field($model, 'shared')->textInput() ?>

		<?= $form->field($model, 'public_listview')->textInput() ?>

		<?= $form->field($model, 'public_edit')->textInput() ?>

		<?= $form->field($model, 'public_delete')->textInput() ?>

	</div>
	<div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e){
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result){
	if(result.status == 'success'){
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create'){
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#managedata-grid-pjax'});
	    } else if(result.action == 'update'){
		$(document).find('#modal-managedata').modal('hide');
		$.pjax.reload({container:'#managedata-grid-pjax'});
	    }
	} else{
	    ". SDNoty::show('result.message', 'result.status') ."
	}
    }).fail(function(){
	console.log('server error');
    });
    return false;
});

");?>
