<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\ManagedataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="managedata-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ezf_id') ?>

    <?= $form->field($model, 'ezf_name') ?>

    <?= $form->field($model, 'ezf_detail') ?>

    <?= $form->field($model, 'ezf_table') ?>

    <?= $form->field($model, 'user_create') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'user_update') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'shared') ?>

    <?php // echo $form->field($model, 'public_listview') ?>

    <?php // echo $form->field($model, 'public_edit') ?>

    <?php // echo $form->field($model, 'public_delete') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
