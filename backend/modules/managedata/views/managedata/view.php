<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\managedata\models\Managedata */

$this->title = 'Managedata#'.$model->ezf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Managedata'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="managedata-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
	//	'ezf_id',
		'ezf_name',
		'ezf_detail:ntext',
		'ezf_table',
		'user_create',
		//'create_date',
	//	'user_update',
	//	'update_date',
	//	'status',
	//	'shared',
	//	'public_listview',
	//	'public_edit',
	//	'public_delete',
	    ],
	]) ?>
    </div>
</div>
