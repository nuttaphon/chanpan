<?php

namespace backend\modules\tccadmin;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\tccadmin\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
