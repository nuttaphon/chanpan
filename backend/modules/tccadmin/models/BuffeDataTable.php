<?php

namespace backend\modules\tccadmin\models;

use Yii;

/**
 * This is the model class for table "buffe_data_table".
 *
 * @property integer $id
 * @property integer $tborder
 * @property string $tbname
 * @property string $description
 * @property integer $enable
 */
class BuffeDataTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buffe_data_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tborder', 'tbname', 'title', 'enable'], 'required'],
            [['tborder', 'enable'], 'integer'],
            [['title','description'], 'string'],
            [['tbname'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tborder' => 'ลำดับ',
            'tbname' => 'Table Name',
            'title' => 'ชื่อตาราง',
            'description' => 'รายละเอียด',
            'enable' => 'Enable',
        ];
    }

    /**
     * @inheritdoc
     * @return BuffeDataTableQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BuffeDataTableQuery(get_called_class());
    }
}
