<?php

namespace backend\modules\tccadmin\models;

use Yii;

/**
 * This is the model class for table "buffe_data_table_view".
 *
 * @property string $tbname
 * @property integer $his
 * @property string $viewsql
 */
class BuffeDataTableView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buffe_data_table_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode','tbname', 'his'], 'required'],
            [['his','tinsert','tupdate','tdelete'], 'integer'],
            [['viewsql','tinsert','tupdate','tdelete','tbtrigger','lfield1','rfield1','lfield2','rfield2','lfield3','rfield3'], 'safe'],
            [['viewsql'], 'string'],
            [['sitecode','tbname','tbtrigger','lfield1','rfield1','lfield2','rfield2','lfield3','rfield3'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sitecode'=>'เลขหน่วยบริการ',
            'tbname' => 'ชื่อตาราง',
            'his' => 'ชนิด HIS',
            'viewsql' => 'SQL View',
            'tbtrigger' => 'ตาราง Trigger',
            'lfield1' => 'ฟิลด์คีย์หลัก (Local)',
            'rfield1' => 'ฟิลด์คีย์หลัก (Remote)',
            'lfield2' => 'ฟิลด์คีย์หลัก (Local)',
            'rfield2' => 'ฟิลด์คีย์หลัก (Remote)',
            'lfield3' => 'ฟิลด์คีย์หลัก (Local)',
            'rfield3' => 'ฟิลด์คีย์หลัก (Remote)',
            'tinsert' => 'Insert',
            'tupdate' => 'Update',
            'tdelete' => 'Delete',
        ];
    }
}
