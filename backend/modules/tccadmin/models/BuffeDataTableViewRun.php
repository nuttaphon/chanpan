<?php

namespace backend\modules\tccadmin\models;

use Yii;

/**
 * This is the model class for table "buffe_data_table_view_run".
 *
 * @property string $tbname
 * @property integer $his
 * @property string $sitecode
 * @property string $viewsql
 * @property string $result
 * @property string $create_at
 * @property string $update_at
 */
class BuffeDataTableViewRun extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buffe_data_table_view_run';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tbname', 'his', 'sitecode', 'tositecode'], 'required'],
            [['id','his'], 'integer'],
            [['viewsql', 'result','client_err'], 'string'],
            [['create_at', 'update_at', 'viewsql', 'result', 'create_at', 'update_at','client_err'], 'safe'],
            [['tbname'], 'string', 'max' => 100],
            [['sitecode','tositecode'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tbname' => 'ชื่อตาราง',
            'his' => 'ชนิด HIS',
            'sitecode' => 'รหัสหน่วยบริการ',
            'tositecode' => 'รหัสหน่วยบริการ',
            'viewsql' => 'SQL View',
            'result' => 'ผลลัพท์',
            'create_at' => 'วันที่ส่งคำสั่ง',
            'update_at' => 'วันที่ได้รับผล',
        ];
    }
}
