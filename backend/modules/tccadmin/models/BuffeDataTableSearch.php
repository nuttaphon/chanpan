<?php

namespace backend\modules\tccadmin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\tccadmin\models\BuffeDataTable;

/**
 * BuffeDataTableSearch represents the model behind the search form about `backend\modules\tccadmin\models\BuffeDataTable`.
 */
class BuffeDataTableSearch extends BuffeDataTable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tborder'], 'integer'],
            [['tbname','title', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BuffeDataTable::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tborder' => $this->tborder,
            'enable' => $this->enable,
        ]);

        $query->andFilterWhere(['like', 'tbname', $this->tbname])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andWhere(['enable'=>1])
            ->addOrderBy(['tborder'=>SORT_ASC]);

        return $dataProvider;
    }
}
