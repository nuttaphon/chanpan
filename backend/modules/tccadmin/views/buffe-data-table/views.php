<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use kartik\popover\PopoverX;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tccadmin\models\BuffeDataTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการ HIS View สำหรับตาราง ' . $table;
$this->params['breadcrumbs'][] = $this->title;

global $tbname;

$tbname=$table;

?>
<div class="buffe-data-table-view-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  Pjax::begin(['id'=>'buffe-data-table-view-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'buffe-data-table-view-grid',
	'dataProvider' => $dataprovider,
        'panelBtn' => Html::a('<span class="fa fa-arrow-circle-o-left"></span> กลับไปหน้าจัดการตาราง', Url::to(['buffe-data-table/']), [
                                    'title' => Yii::t('app', 'Go back'),
                                    'class'=>'btn btn-success btn-sm',
			    ]),
        'columns' => [
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            [
                'attribute'=>'sitecode',
                'label' => 'เลขหน่วยบริการ',
            ],   
            [
                'attribute'=>'his',
                'label' => 'ชื่อ HIS',
                'value'=> function ($model) {
                    $his=  Yii::$app->db->createCommand("select * from buffe_his where id=".$model['his'])->queryOne();
                    return $his['his_name'];
                }
            ],   
            [
                'label' => 'SQL View',
                'format'=>'html',
                'value'=> function ($model) {
                    global $tbname;
                    $his=$model['his'];
                    $vdata = \backend\modules\tccadmin\models\BuffeDataTableView::findOne(['sitecode'=>$model['sitecode'],'tbname'=>$tbname,'his'=>$his]);
                    
                    //return trim($vdata->viewsql)!="" ? '<i class="fa fa-file-text-o"></i>':"";
                    return nl2br($vdata->viewsql);
                    
                },
                'contentOptions'=>[ 'style'=>'width: 600px','align'=>'left'], 
            ],
            [
                'attribute'=>'last_sent',
                'label' => 'ส่งล่าสุด',
                'value'=>function ($model) {
                    $viewrun = backend\modules\tccadmin\models\BuffeDataTableViewRun::findOne(['sitecode'=>$model['sitecode'],'tbname'=>$model['tbname'],'his'=>$model['his']]);
                    return $viewrun['create_at'];
                }
            ],            
            [
                'attribute'=>'last_result',
                'label' => 'ได้ผลล่าสุด',
                'value'=>function ($model) {
                    $viewrun = backend\modules\tccadmin\models\BuffeDataTableViewRun::findOne(['sitecode'=>$model['sitecode'],'tbname'=>$model['tbname'],'his'=>$model['his']]);
                    return $viewrun['update_at'];
                }                
            ],
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{update} {test} {sync}',
                'buttons' => [
                    'update' => function ($url, $model,$index) {
                            global $tbname;
                            $his=$model['his'];
                            $sitecode=$model['sitecode'];
                            $fdata = \backend\modules\tccadmin\models\BuffeDataTableView::findOne(['sitecode'=>$model['sitecode'],'tbname'=>$tbname,'his'=>$his]);
                            if ($fdata) {
                                $url="updateview?sitecode={$sitecode}&table={$tbname}&his={$his}";
                                $action="updateview";
                            }else{
                                $url="createview?table={$tbname}&his={$his}";
                                $action="createview";
                            }
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-success btn-xs',
                                    'data-action'=>$action,
                                    'data-pjax'=>'0',
			    ]);
			
                    },
                    'test' => function ($url, $model,$index) {
                            global $tbname;
                            $his=$model['his'];
                            $fdata = \backend\modules\tccadmin\models\BuffeDataTableView::findOne(['sitecode'=>$model['sitecode'],'tbname'=>$tbname,'his'=>$his]);
                            if ($fdata) {
                                $url="viewrun?sitecode={$model['sitecode']}&table={$tbname}&his={$his}";
                                return Html::a('<span class="fa fa-caret-square-o-right"></span> Test View', $url, [
                                        'title' => Yii::t('app', 'Test View'),
                                        'class'=>'btn btn-primary btn-xs',
                                        'data-action'=>$action,
                                        'data-pjax'=>'0',
                                ]);
                            }else{
                                return ;
                            }
                    },
                    'sync' => function ($url, $model,$index) {
                            global $tbname;
                            $his=$model['his'];
                            $viewrun = backend\modules\tccadmin\models\BuffeDataTableViewRun::findOne(['sitecode'=>$model['sitecode'],'tbname'=>$model['tbname'],'his'=>$model['his']]);
                            if ($viewrun['result']!="") {
                                $url="sync?sitecode={$model['sitecode']}&table={$tbname}&his={$his}";
                                return Html::a('<span class="fa fa-refresh fa-spin fa-fw"></span> Sync', $url, [
                                        'title' => Yii::t('app', 'Sync'),
                                        'class'=>'btn btn-primary btn-xs',
                                        'data-action'=>$action,
                                        'data-pjax'=>'0',
                                ]);
                            }else{
                                return ;
                            }
                    },
                ],
                'contentOptions' => ['style' => 'width:220px;']                
	    ],
        ],
    ]); 
                      
?>
    
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-buffe-data-table-view',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#buffe-data-table-view-grid-pjax').on('click', '#modal-addbtn-buffe-data-table-view', function() {
    modalBuffeDataTable($(this).attr('data-url'));
});

$('#buffe-data-table-view-grid-pjax').on('click', '#modal-delbtn-buffe-data-table-view', function() {
    selectionBuffeDataTableGrid($(this).attr('data-url'));
});

$('#buffe-data-table-view-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#buffe-data-table-view-grid').yiiGridView('getSelectedRows');
	disabledBuffeDataTableBtn(key.length);
    },100);
});

$('#buffe-data-table-view-grid-pjax').on('click', '.selectionBuffeDataTableIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledBuffeDataTableBtn(key.length);
});

$('#buffe-data-table-view-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'updateview' || action === 'createview') {
	modalBuffeDataTable(url);
        return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-view-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
        return false;
    }
    //return false;
});

function disabledBuffeDataTableBtn(num) {
    if(num>0) {
	$('#modal-delbtn-buffe-data-table-view').attr('disabled', false);
    } else {
	$('#modal-delbtn-buffe-data-table-view').attr('disabled', true);
    }
}

function selectionBuffeDataTableGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionBuffeDataTableIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-view-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalBuffeDataTable(url) {
    $('#modal-buffe-data-table-view .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-buffe-data-table-view').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>