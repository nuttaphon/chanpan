<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tccadmin\models\BuffeDataTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการฟิลด์ สำหรับตาราง ' . $table;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buffe-data-table-field-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  Pjax::begin(['id'=>'buffe-data-table-field-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'buffe-data-table-field-grid',
	'dataProvider' => $dataprovider,
        'panelBtn' => Html::a('<span class="fa fa-arrow-circle-o-left"></span> กลับไปหน้าจัดการตาราง', Url::to(['buffe-data-table/']), [
                                    'title' => Yii::t('app', 'Go back'),
                                    'class'=>'btn btn-success btn-sm',
			    ]),
        'columns' => [
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            [
                'attribute'=>'fname',
                'label' => 'ชื่อฟิลด์',
            ],   
            [
                'attribute'=>'ftype',
                'label' => 'ชนิด',
            ],   
            [
                'attribute'=>'pk',
                'label'=>'PK',
                'format'=>'html',
                'value'=> function($model) {
                    if ($model['pk']=="PRI") return '<i class="fa fa-check" aria-hidden="true"></i>';
                    return "";
                }
            ],
            [
                'attribute'=>'isnull',
                'label' => 'Null',
                'format'=>'html',
                'value'=> function($model) {
                    if ($model['isnull']=="YES") return '<i class="fa fa-check" aria-hidden="true"></i>';
                    return "";
                }                
            ],
            [
                'attribute'=>'caption',
                'label' => 'ชื่อฟิลด์',
                'value'=> function ($model) {
                    $tbname=$model['tbname'];
                    $fname=$model['fname'];
                    $fdata = \backend\modules\tccadmin\models\BuffeDataTableField::findOne(['tbname'=>$tbname,'fname'=>$fname]);
                    if ($fdata->caption) return $fdata->caption;
                    return '';
                }
            ],
            [
                'attribute'=>'description',
                'label' => 'รายละเอียด',
                'format'=>'html',
                'value'=> function ($model) {
                    $tbname=$model['tbname'];
                    $fname=$model['fname'];
                    $fdata = \backend\modules\tccadmin\models\BuffeDataTableField::findOne(['tbname'=>$tbname,'fname'=>$fname]);
                    if ($fdata->description) return nl2br($fdata->description);
                    return '';
                }                
            ],            
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model,$index) {
                            $tbname=$model['tbname'];
                            $fname=$model['fname'];
                            $fdata = \backend\modules\tccadmin\models\BuffeDataTableField::findOne(['tbname'=>$tbname,'fname'=>$fname]);
                            if ($fdata) {
                                $url="updatefield?table={$tbname}&field={$fname}";
                                $action="updatefield";
                            }else{
                                $url="createfield?table={$tbname}&field={$fname}";
                                $action="createfield";
                            }
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-success btn-xs',
                                    'data-action'=>$action,
                                    'data-pjax'=>'0',
			    ]);
			
                    },
                ],
                'contentOptions' => ['style' => 'width:100px;']                
	    ],
        ],
    ]); 
                      
?>
    
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-buffe-data-table-field',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#buffe-data-table-field-grid-pjax').on('click', '#modal-addbtn-buffe-data-table-field', function() {
    modalBuffeDataTable($(this).attr('data-url'));
});

$('#buffe-data-table-field-grid-pjax').on('click', '#modal-delbtn-buffe-data-table-field', function() {
    selectionBuffeDataTableGrid($(this).attr('data-url'));
});

$('#buffe-data-table-field-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#buffe-data-table-field-grid').yiiGridView('getSelectedRows');
	disabledBuffeDataTableBtn(key.length);
    },100);
});

$('#buffe-data-table-field-grid-pjax').on('click', '.selectionBuffeDataTableIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledBuffeDataTableBtn(key.length);
});

$('#buffe-data-table-field-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'updatefield' || action === 'createfield') {
	modalBuffeDataTable(url);
        return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-field-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
        return false;
    }
    //return false;
});

function disabledBuffeDataTableBtn(num) {
    if(num>0) {
	$('#modal-delbtn-buffe-data-table-field').attr('disabled', false);
    } else {
	$('#modal-delbtn-buffe-data-table-field').attr('disabled', true);
    }
}

function selectionBuffeDataTableGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionBuffeDataTableIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-field-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalBuffeDataTable(url) {
    $('#modal-buffe-data-table-field .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-buffe-data-table-field').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>