<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\tccadmin\models\BuffeDataTable */

$this->title = 'Create Buffe Data Table';
$this->params['breadcrumbs'][] = ['label' => 'Buffe Data Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="buffe-data-table-field-create">
    
    <?= $this->render('_formfield', [
        'model' => $model,
        'tbname'=>$tbname,
        'fname'=>$fname,        
    ]) ?>

</div>
