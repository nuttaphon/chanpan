<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\tccadmin\models\BuffeDataTable */
/* @var $form yii\bootstrap\ActiveForm */
if ($model->tbname=="" & $tbname != "") {
    $model->tbname=$tbname;
    $model->fname=$fname;
}
?>

<div class="buffe-data-table-field-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">จัดการฟิลด์</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'tbname')->textInput(['maxlength' => true,'readonly'=> true]) ?>
        
        <?= $form->field($model, 'fname')->textInput(['maxlength' => true,'readonly'=> true]) ?>
        <?= $form->field($model, 'caption')->textInput(['maxlength' => true]) ?>        
<?= $form->field($model, 'description')->textArea(['rows' => '6']) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Update') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'createfield') {
                $(document).find('#modal-buffe-data-table-field').modal('hide');
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#buffe-data-table-field-grid-pjax'});
	    } else if(result.action == 'updatefield') {
		$(document).find('#modal-buffe-data-table-field').modal('hide');
		$.pjax.reload({container:'#buffe-data-table-field-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>