<?php
use kartik\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


echo Html::jumbotron([
    'heading' => 'TCC Bot!', 
    'body' => 'ท่านยังไม่ได้ทำการติดตั้ง TCC Bot กรุณาติดตั้ง TCC Bot ก่อน.',
    'buttons' => [
        [
            'label' => 'Install TCC Bot',
            'icon' => 'book',
            'url' => '/tccbots/setup-tccbot',
            'type' => Html::TYPE_PRIMARY,
            'size' => Html::SIZE_LARGE
        ],
    ],
]);
?>

