<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
//use appxq\sdii\widgets\GridView;
use kartik\grid\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use yii\data\ArrayDataProvider;
use kartik\helpers\Enum;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tccadmin\models\BuffeDataTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$table = Yii::$app->request->get('table');
$hisid = Yii::$app->request->get('his');
$sitecode = $hisid = Yii::$app->request->get('sitecode');
$this->title = 'ทดสอบ View สำหรับตาราง ' . $table;
$this->params['breadcrumbs'][] = $this->title;

$his = \Yii::$app->db->createCommand("select his_name from buffe_his where id=".$model->his)->queryOne();

$delfield=trim($model->rfield1).",".trim($model->rfield2).",".trim($model->rfield3).",,";

$delfield=  str_replace(",,,,", "", $delfield);
$delfield=  str_replace(",,,", "", $delfield);
$delfield=  str_replace(",,", "", $delfield);

$onfield = "[".trim($model->rfield1)."=".trim($model->lfield1)."]";
if (trim($model->rfield2)!= "") $onfield .= "[".trim($model->rfield2)."=".trim($model->lfield2)."]";
if (trim($model->rfield3)!= "") $onfield .= "[".trim($model->rfield3)."=".trim($model->lfield3)."]";

$trigger[insert][name]=$model['tbname']."_insert";
$trigger[insert][type]="AFTER INSERT";
$trigger[insert][on]=$onfield;
$trigger[insert][command]="SELECT * FROM tcc_".$model['tbname']."_view WHERE " . $model->rfield1."='xxxx'";
$trigger[insert][controller]="replace";

$trigger[update][name]=$model['tbname']."_update";
$trigger[update][type]="AFTER UPDATE";
$trigger[update][on]=$onfield;
$trigger[update][command]="SELECT * FROM tcc_".$model['tbname']."_view WHERE " . $model->rfield1."='xxxx'";
$trigger[update][controller]="replace";

$trigger[delete][name]=$model['tbname']."_delete";
$trigger[delete][type]="AFTER DELETE";
$trigger[delete][on]=$onfield;
$trigger[delete][command]="SELECT {$delfield} FROM tcc_".$model['tbname']."_view WHERE " . $model->rfield1."='xxxx'";
$trigger[delete][controller]="delete";

///$triggerdata=json_decode($dataprovider[result],true);
                    
?>
<div class="buffe-data-table-trigger-run-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  Pjax::begin(['id'=>'buffe-data-table-trigger-run-grid-pjax']);?>
<div class="buffe-data-table-trigger">

    <div class="modal-body">
        <?=Html::a('<span class="fa fa-arrow-circle-o-left"></span> กลับไปหน้าจัดการ View', Url::to(['buffe-data-table/views'])."?table={$table}", [
                            'title' => Yii::t('app', 'Go back'),
                            'class'=>'btn btn-success btn-sm pull-right',
                    ]);?>
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
                'sitecode',
		'tbname',
                [
                    'label' => 'HIS',
                    'value' => $his['his_name'],
                ],
                [
                    'label' => 'Triggers',
                    'format'=> 'html',
                    'value' =>  yii\helpers\VarDumper::dumpAsString($trigger,10,true),
                ],
		'tbtrigger',
	    ],
            'template' => '<tr><th style="min-width:200px">{label}</th><td>{value}</td></tr>',
	]);
    
                    
    ?>
    </div>
</div>

    <?php 
    $run = new backend\modules\tccadmin\models\BuffeDataTableViewRun();
    $form = ActiveForm::begin([
	'id'=>$run->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    </div>

    <div class="modal-body">
        <?php
        $data = \Yii::$app->db->createCommand("select c.id,concat(c.id,' ',h.name,IF(abs(timediff(last_ping,NOW()))<300,' (Online)',' (Offline)')) as name from buffe_config c inner join all_hospital_thai_unicode h on c.id+0=h.hcode+0 where h.hcode+0>0 and c.his_type='{$model->his}' order by (abs(timediff(last_ping,NOW()))<300) desc,c.id;")->queryAll();
        $run->tositecode=Yii::$app->user->identity->userProfile->sitecode;
        $run->sitecode=$model->sitecode;
        if (Yii::$app->user->can('administrator')) {
            echo $form->field($run, 'tositecode')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'name'),
                'options' => ['placeholder' => 'เลือกหน่วยบริการ'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        }else{
            
            echo $form->field($run, 'tositecode')->textInput(['readonly'=>true]);
        }
        echo $form->field($run, 'sitecode')->hiddenInput();
        ?>
        
        <?= Html::submitButton(Yii::t('app', 'ส่งคำสั่งไปทดสอบ') , ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>    
    
    <?php  Pjax::end();?>
    <a name="result">
<?php
    //$sitecode = Yii::$app->user->identity->userProfile->sitecode;
    if (Yii::$app->user->can('administrator')) {
        $run = Yii::$app->db->createCommand("SELECT * from buffe_data_table_view_run where sitecode='{$sitecode}' and tbname='{$model->tbname}' and his='{$model->his}'")->queryAll();
    }else{
        $run = Yii::$app->db->createCommand("SELECT * from buffe_data_table_view_run where sitecode='{$sitecode}' and tbname='{$model->tbname}' and his='{$model->his}'")->queryAll();
    }
    
    $dataprovider = new ArrayDataProvider([
            'allModels'=>$run,
            'pagination' => [
                'pageSize' => 100,
                ],
            ]);
    
        $gridColumns = [
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            [
                'attribute'=>'tositecode',
                'label' => 'รหัสหน่วยบริการ',
                'contentOptions' => ['style'=>'width:140px;text-align: center;'],
            ],       
            [
                'attribute' => 'update_at',
                'format' => 'text',
                'label' => 'เวลาที่ทำ',
                'value' => function ($model) {
                        if ($model['update_at']=="0000-00-00 00:00:00") {
                            return '';
                        }else{
                            return $model['update_at'];
                        }
                },
                'contentOptions' => ['style'=>'width:100px;text-align: center;'],
            ],                   
            [
                'attribute' => 'result',
                'label' => 'ผลลัพท์',
                'format' => 'html',
                'value' => function ($dataprovider) {
                    $data=json_decode($dataprovider[result],true);
                    
                    //$test=\yii\helpers\VarDumper::dump($data,10,true);
                    if (trim($dataprovider[result]) != "") {
                        //return '<i class="fa fa-file-text-o"></i>';
                        return yii\helpers\VarDumper::dumpAsString($data,10,true);
                    }else{
                        return 'ยังไม่มีข้อมูล';
                    }
                    //return '<div id="divToScroll" class="table-responsive" >' . Enum::array2table($data,true,false,false,array('style'=>'overflow-y:scroll;width:100%;')). '</div>';
                    
                },
                'contentOptions' => ['style'=>'width:550px;text-align: left;'],
            ],
            [
                'attribute' => 'client_err',
                'label' => 'ข้อผิดพลาด',
                'contentOptions' => ['style'=>'width:200px;text-align: left;'],
            ],       
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{delete}',
                'buttons' => [
                    'update' => function ($url, $model,$index) {
                            global $tbname;
                            $his=$model['id'];
                            $fdata = \backend\modules\tccadmin\models\BuffeDataTableView::findOne(['tbname'=>$tbname,'his'=>$his]);
                            if ($fdata) {
                                $url="updateview?table={$tbname}&his={$his}";
                                $action="updateview";
                            }else{
                                $url="createview?table={$tbname}&his={$his}";
                                $action="createview";
                            }
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-success btn-xs',
                                    'data-action'=>$action,
                                    'data-pjax'=>'0',
			    ]);
			
                    },
                    'delete' => function ($url, $model,$index) {
                            $url="deleteviewrun?id={$model[id]}";
                            $action="deleteviewrun";
			    return Html::a('<span class="fa fa-trash-o"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-success btn-xs',
                                    'data-action'=>$action,
                                    'data-pjax'=>'0',
			    ]);
			
                    },
                ],
                'contentOptions' => ['style' => 'width:50px;'],
	    ],
        ];    
    echo GridView::widget([
    'dataProvider'=> $dataprovider,
    //'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'responsive'=>true,
    'hover'=>true
]);
/*
    echo GridView::widget([
	'id' => 'buffe-data-table-trigger-grid',
	'dataProvider' => $dataprovider,
        'panelBtn' => Html::a('<span class="fa fa-arrow-circle-o-left"></span> กลับไปหน้าจัดการ View', Url::to(['buffe-data-table/views'])."?table=$table", [
                                    'title' => Yii::t('app', 'Go back'),
                                    'class'=>'btn btn-success btn-sm',
			    ])." ".Html::a('<span class="fa fa-refresh fa-spin fa-fw"></span> Refresh', Url::to(['buffe-data-table/viewrun'])."?table=$table&his=$hisid#result", [
                                    'title' => Yii::t('app', 'Refresh'),
                                    'class'=>'btn btn-info btn-sm',
			    ]),
        'columns' => [
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
            [
                'attribute'=>'sitecode',
                'label' => 'รหัสหน่วยบริการ',
                'contentOptions' => ['style'=>'width:140px;text-align: center;'],
            ],       
            [
                'attribute' => 'update_at',
                'format' => 'text',
                'label' => 'เวลาประมวลผล',
                'value' => function ($model) {
                        if ($model['update_at']=="0000-00-00 00:00:00") {
                            return '';
                        }else{
                            return $model['update_at'];
                        }
                },
                'contentOptions' => ['style'=>'width:120px;text-align: center;'],
            ],                   
            [
                'attribute' => 'result',
                'label' => 'ผลลัพท์',
                'format' => 'html',
                'value' => function ($dataprovider) {
                    $data=json_decode($dataprovider[result],true);
                    //$test=\yii\helpers\VarDumper::dump($data,10,true);
                    
                    return Enum::array2table($data,false,false,false,array('style'=>'overflow-y:scroll;width:100%;'));
                    
                }
            ],
            [
                'attribute' => 'client_err',
                'label' => 'ผิดพลาด',
                'contentOptions' => ['style'=>'min-width:100px;text-align: center;'],
            ],       
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{delete}',
                'buttons' => [
                    'update' => function ($url, $model,$index) {
                            global $tbname;
                            $his=$model['id'];
                            $fdata = \backend\modules\tccadmin\models\BuffeDataTableView::findOne(['tbname'=>$tbname,'his'=>$his]);
                            if ($fdata) {
                                $url="updateview?table={$tbname}&his={$his}";
                                $action="updateview";
                            }else{
                                $url="createview?table={$tbname}&his={$his}";
                                $action="createview";
                            }
			    return Html::a('<span class="fa fa-edit"></span> Edit', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class'=>'btn btn-success btn-xs',
                                    'data-action'=>$action,
                                    'data-pjax'=>'0',
			    ]);
			
                    },
                    'delete' => function ($url, $model,$index) {
                            $url="deleteviewrun?id={$model[id]}";
                            $action="deleteviewrun";
			    return Html::a('<span class="fa fa-trash-o"></span> Delete', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class'=>'btn btn-success btn-xs',
                                    'data-action'=>$action,
                                    'data-pjax'=>'0',
			    ]);
			
                    }
                ],
                'contentOptions' => ['style' => 'width:50px;'],
	    ],
        ],
    ]); 
 * 
 */
?>
</div>

<?=  ModalForm::widget([
    'id' => 'modal-buffe-data-table-trigger-run',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#buffe-data-table-trigger-run-grid-pjax').on('click', '#modal-addbtn-buffe-data-table-trigger-run', function() {
    modalBuffeDataTable($(this).attr('data-url'));
});

$('#buffe-data-table-trigger-run-grid-pjax').on('click', '#modal-delbtn-buffe-data-table-trigger-run', function() {
    selectionBuffeDataTableGrid($(this).attr('data-url'));
});

$('#buffe-data-table-trigger-run-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#buffe-data-table-trigger-run-grid').yiiGridView('getSelectedRows');
	disabledBuffeDataTableBtn(key.length);
    },100);
});

$('#buffe-data-table-trigger-run-grid-pjax').on('click', '.selectionBuffeDataTableIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledBuffeDataTableBtn(key.length);
});

$('#buffe-data-table-trigger-run-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'updateview' || action === 'createview') {
	modalBuffeDataTable(url);
        return false;
    } else if(action === 'deleteviewrun') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-trigger-run-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
        return false;
    }
    //return false;
});

function disabledBuffeDataTableBtn(num) {
    if(num>0) {
	$('#modal-delbtn-buffe-data-table-trigger-run').attr('disabled', false);
    } else {
	$('#modal-delbtn-buffe-data-table-trigger-run').attr('disabled', true);
    }
}

function selectionBuffeDataTableGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionBuffeDataTableIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#buffe-data-table-trigger-run-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalBuffeDataTable(url) {
    $('#modal-buffe-data-table-trigger-run .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-buffe-data-table-trigger-run').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>