<?php

namespace backend\modules\tccadmin\controllers;

use Yii;
use backend\modules\tccadmin\models\BuffeDataTable;
use backend\modules\tccadmin\models\BuffeDataTableSearch;
use backend\modules\tccadmin\models\BuffeDataTableField;
use backend\modules\tccadmin\models\BuffeDataTableView;
use backend\modules\tccadmin\models\BuffeDataTableViewRun;
use backend\modules\tccadmin\models\BuffeCommand;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use yii\data\SqlDataProvider;
use yii\data\ArrayDataProvider;
use appxq\sdii\helpers\SDNoty;
/**
 * BuffeDataTableController implements the CRUD actions for BuffeDataTable model.
 */
class BuffeDataTableController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all BuffeDataTable models.
     * @return mixed
     */
    public function actionIndex()
    {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $config = Yii::$app->db->createCommand("select * from buffe_config where id='{$sitecode}'")->queryOne();
        if (!$config) {
            return $this->render('indexnobot');
        }
        if ($config['his_type'] == "") {
            return $this->render('indexnobot');
        }
        $initview = Yii::$app->db->createCommand("insert ignore into buffe_data_table_view (select '{$config[id]}' as sitecode,tbname,his,viewsql,tbtrigger,tinsert,tupdate,tdelete,lfield1,rfield1,lfield2,rfield2,lfield3,rfield3 from buffe_data_table_view where sitecode='0'  and his='{$config[his_type]}')")->query();
        
        $searchModel = new BuffeDataTableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 50];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tborderitems' => $tborderitems,
            'siteconfig'=>$config,
        ]);
    }

    public function actionFields($table) {
        $dbname=  \Yii::$app->dbbot->createCommand("SELECT DATABASE();")->queryScalar();
        $sql="SELECT '{$table}' as tbname,COLUMN_NAME as fname, COLUMN_TYPE as ftype, IS_NULLABLE as isnull,'' as caption,'' as description,COLUMN_KEY as pk FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{$table}' AND table_schema = '{$dbname}'";
        $model = \Yii::$app->dbbot->createCommand($sql)->queryAll();
        $dataprovider = new ArrayDataProvider([
            'allModels'=>$model,
            'pagination' => [
                'pageSize' => 100,
                ],
            ]);
            
                    
        return $this->render('fields', [
            'table'=>$table,
            'dataprovider' => $dataprovider,
        ]);        
    }

    public function actionCreatefield()
    {        
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new BuffeDataTableField();
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'createfield',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('createfield', [
		    'model' => $model,
                    'tbname'=>Yii::$app->getRequest()->get('table'),
                    'fname'=>Yii::$app->getRequest()->get('field'),
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }    
    
    public function actionUpdatefield($table,$field)
    {        
	if (Yii::$app->getRequest()->isAjax) {
	    $model = BuffeDataTableField::findOne(['tbname'=>$table,'fname'=>$field]);
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'updatefield',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('createfield', [
		    'model' => $model,
                    'tbname'=>Yii::$app->getRequest()->get('table'),
                    'fname'=>Yii::$app->getRequest()->get('field'),
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }    
    
    public function actionViews($table) {
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $config = Yii::$app->db->createCommand("select * from buffe_config where id='{$sitecode}'")->queryOne();
        
        $initview = Yii::$app->db->createCommand("insert ignore into buffe_data_table_view (select '{$config[id]}' as sitecode,tbname,his,viewsql,tbtrigger,tinsert,tupdate,tdelete,lfield1,rfield1,lfield2,rfield2,lfield3,rfield3 from buffe_data_table_view where sitecode='0'  and tbname='{$table}' and his='{$config[his_type]}')")->query();
        
        if (Yii::$app->user->can('administrator')) {
            $sql="SELECT * FROM buffe_data_table_view where (sitecode=0 or sitecode='{$config[id]}')  and tbname='{$table}' order by sitecode desc";
        }else{
            $sql="SELECT * FROM buffe_data_table_view where (sitecode='{$config[id]}' and tbname='{$table}') order by sitecode desc";
        }
        
                
        if (count($model)==0) {
            $initview = Yii::$app->db->createCommand("insert ignore into buffe_data_table_view (sitecode,`tbname`,his) values('{$sitecode}','{$table}','{$config[his_type]}')")->query();
        }
        
        $model = \Yii::$app->db->createCommand($sql)->queryAll();
        $dataprovider = new ArrayDataProvider([
            'allModels'=>$model,
            'pagination' => [
                'pageSize' => 100,
                ],
            ]);
            
                    
        return $this->render('views', [
            'table'=>$table,
            'dataprovider' => $dataprovider,
        ]);        
    }

    public function actionCreateview()
    {        
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new BuffeDataTableView();
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'createview',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('createview', [
		    'model' => $model,
                    'tbname'=>Yii::$app->getRequest()->get('table'),
                    'his'=>Yii::$app->getRequest()->get('his'),
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }    
    
    public function actionUpdateview($table,$his,$sitecode)
    {        
	if (Yii::$app->getRequest()->isAjax) {
	    $model = BuffeDataTableView::findOne(['sitecode'=>$sitecode,'tbname'=>$table,'his'=>$his]);
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'updateview',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('createview', [
		    'model' => $model,
                    'tbname'=>Yii::$app->getRequest()->get('table'),
                    'his'=>Yii::$app->getRequest()->get('his'),
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }        

    public function actionReport($table) {

        $sql="select hospcode as sitecode,count(*) as nrecs from buffe_data.{$table} group by hospcode";
        $model = \Yii::$app->db->createCommand($sql)->queryAll();
        $dataProvider = new ArrayDataProvider([
            'allModels'=>$model,
            'pagination' => [
                'pageSize' => $model->count,
                ],
            ]);        

        return $this->render('report', [
            'dataprovider' => $dataProvider,
            'table'=>$table,
        ]);        
    }
    
    public function actionSync($sitecode,$table, $his) {

        $model = BuffeDataTableView::findOne(['sitecode'=>$sitecode,'tbname'=>$table, 'his'=>$his]);
        if (Yii::$app->request->post()) {
            $run = new BuffeDataTableViewRun();
            if ($run->load(Yii::$app->request->post())) {
                $run->sitecode=$sitecode;
                $run->tbname=$table;
                $run->his=$his;
                $run->viewsql = $model->viewsql;
                $run->create_at = date("Y-m-d H:i:s");
                
                if ($run->save()) {  
                    $command = new BuffeCommand();
                    $command->sitecode = $run->tositecode;
                    $command->template_id = '0';
                    $command->dadd = date("Y-m-d H:i:s");
                    $command->ctype = "View";
                    $command->cname = "Test view";
                    $command->presql = "DROP VIEW IF EXISTS tcc_{$model->tbname}_view; CREATE VIEW tcc_{$model->tbname}_view AS {$model->viewsql};";
                    $command->sql="SELECT * FROM tcc_{$model->tbname}_view limit 10";
                    $command->controller="testview";
                    $command->table = $run->id;
                    if (!$command->save()) {
                        SDNoty::show('Error. View has not been sent.');
                    }                    
                    SDNoty::show('Send command completed.');
                    $this->redirect("viewrun?sitecode={$sitecode}&table={$table}&his={$his}#result");
                }
            }
        }
        
        return $this->render('triggers', [
            'model' => $model,
        ]);        
    }
    
    public function actionViewrun($sitecode,$table, $his) {

        $model = BuffeDataTableView::findOne(['sitecode'=>$sitecode,'tbname'=>$table, 'his'=>$his]);
        if (Yii::$app->request->post()) {
            $run = new BuffeDataTableViewRun();
            if ($run->load(Yii::$app->request->post())) {
                $run->sitecode=$sitecode;
                $run->tbname=$table;
                $run->his=$his;
                $run->viewsql = $model->viewsql;
                $run->create_at = date("Y-m-d H:i:s");
                
                if ($run->save()) {  
                    $command = new BuffeCommand();
                    $command->sitecode = $run->tositecode;
                    $command->template_id = '0';
                    $command->dadd = date("Y-m-d H:i:s");
                    $command->ctype = "View";
                    $command->cname = "Test view";
                    $command->presql = "DROP VIEW IF EXISTS tcc_{$model->tbname}_view; CREATE VIEW tcc_{$model->tbname}_view AS {$model->viewsql};";
                    $command->sql="SELECT * FROM tcc_{$model->tbname}_view limit 10";
                    $command->controller="testview";
                    $command->table = $run->id;
                    if (!$command->save()) {
                        SDNoty::show('Error. View has not been sent.');
                    }                    
                    SDNoty::show('Send command completed.');
                    $this->redirect("viewrun?sitecode={$sitecode}&table={$table}&his={$his}#result");
                }
            }
        }
        
        return $this->render('viewrun', [
            'model' => $model,
        ]);        
    }

    public function actionCreateviewrun()
    {        
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new BuffeDataTableViewRun();
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'createviewrun',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('createviewrun', [
		    'model' => $model,
                    'tbname'=>Yii::$app->getRequest()->get('table'),
                    'his'=>Yii::$app->getRequest()->get('his'),
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }    
    public function actionDeleteviewrun($id)
    {
            $run = BuffeDataTableViewRun::findOne(['id'=>$id]);
	    $table = $run->tbname;
            $sitecode = $run->sitecode;
            $his = $run->his;
            $run->delete();
            $this->redirect("viewrun?sitecode={$sitecode}&table={$table}&his={$his}#result");
    }
    
    public function actionUpdateviewrun($table,$his)
    {        
	if (Yii::$app->getRequest()->isAjax) {
	    $model = BuffeDataTableView::findOne(['sitecode'=>$sitecode,'tbname'=>$table,'his'=>$his]);
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'updateviewrun',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('createviewrun', [
		    'model' => $model,
                    'tbname'=>Yii::$app->getRequest()->get('table'),
                    'his'=>Yii::$app->getRequest()->get('his'),
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }    
    /**
     * Displays a single BuffeDataTable model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
		'model' => $this->findModel($id),
	    ]);
	}
    }

    public function actionEnable($id,$setto) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $tables = $this->findModel($id);
        $tables->enable=$setto;
        
        
    }

    /**
     * Creates a new BuffeDataTable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new BuffeDataTable();

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Updates an existing BuffeDataTable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing BuffeDataTable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if ($this->findModel($id)->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionDeletes() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if (isset($_POST['selection'])) {
		foreach ($_POST['selection'] as $id) {
		    $this->findModel($id)->delete();
		}
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $_POST['selection'],
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    /**
     * Finds the BuffeDataTable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BuffeDataTable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BuffeDataTable::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
