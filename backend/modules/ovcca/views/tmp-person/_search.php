<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\TmpPersonSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="tmp-person-search" style="margin-bottom: 15px;">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'layout' => 'inline',
	
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'khet') ?>

    <?= $form->field($model, 'province') ?>

    <?= $form->field($model, 'amphur') ?>

    <?= $form->field($model, 'tambon') ?>

    <?php // echo $form->field($model, 'hospcode') ?>

    <?php // echo $form->field($model, 'hospname') ?>

    <?php // echo $form->field($model, 'person_id') ?>

    <?php // echo $form->field($model, 'house_id') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'cid') ?>

    <?php // echo $form->field($model, 'hn') ?>

    <?php // echo $form->field($model, 'pname') ?>

    <?php // echo $form->field($model, 'fname') ?>

    <?php // echo $form->field($model, 'lname') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'nationality') ?>

    <?php // echo $form->field($model, 'education') ?>

    <?php // echo $form->field($model, 'type_area') ?>

    <?php // echo $form->field($model, 'religion') ?>

    <?php // echo $form->field($model, 'birthdate') ?>

    <?php // echo $form->field($model, 'village_id') ?>

    <?php // echo $form->field($model, 'village_code') ?>

    <?php // echo $form->field($model, 'village_name') ?>

    <?php // echo $form->field($model, 'pttype') ?>

    <?php // echo $form->field($model, 'pttype_begin_date') ?>

    <?php // echo $form->field($model, 'pttype_expire_date') ?>

    <?php // echo $form->field($model, 'pttype_hospmain') ?>

    <?php // echo $form->field($model, 'pttype_hospsub') ?>

    <?php // echo $form->field($model, 'marrystatus') ?>

    <?php // echo $form->field($model, 'death') ?>

    <?php // echo $form->field($model, 'death_date') ?>

    <div class="form-group">
	<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
