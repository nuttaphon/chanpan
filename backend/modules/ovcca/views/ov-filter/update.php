<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\OvFilter */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ov Filter',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ov Filters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ov-filter-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
