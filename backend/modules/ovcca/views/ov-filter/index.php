<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ovcca\models\OvFilterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'รอบการทำงาน');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ov-filter-index">

    <div class="ov-person-back" style="margin-bottom: 15px; text-align: right;">
	<?php echo Html::a('<i class="fa fa-reply"></i> กลับไปหน้าแสดงใบกำกับงาน', ['/ovcca/ov-person/index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php  Pjax::begin(['id'=>'ov-filter-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'ov-filter-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['ov-filter/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ov-filter']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['ov-filter/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-ov-filter', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionOvFilterIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'name',
            'start',
            'end',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-ov-filter',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#ov-filter-grid-pjax').on('click', '#modal-addbtn-ov-filter', function() {
    modalOvFilter($(this).attr('data-url'));
});

$('#ov-filter-grid-pjax').on('click', '#modal-delbtn-ov-filter', function() {
    selectionOvFilterGrid($(this).attr('data-url'));
});

$('#ov-filter-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#ov-filter-grid').yiiGridView('getSelectedRows');
	disabledOvFilterBtn(key.length);
    },100);
});

$('#ov-filter-grid-pjax').on('click', '.selectionOvFilterIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledOvFilterBtn(key.length);
});

$('#ov-filter-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalOvFilter('".Url::to(['ov-filter/update', 'id'=>''])."'+id);
});	

$('#ov-filter-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalOvFilter(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-filter-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledOvFilterBtn(num) {
    if(num>0) {
	$('#modal-delbtn-ov-filter').attr('disabled', false);
    } else {
	$('#modal-delbtn-ov-filter').attr('disabled', true);
    }
}

function selectionOvFilterGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionOvFilterIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-filter-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalOvFilter(url) {
    $('#modal-ov-filter .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-filter').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>