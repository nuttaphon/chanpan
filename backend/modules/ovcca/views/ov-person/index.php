<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use kartik\field\FieldRange;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ovcca\models\OvPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'จัดการใบกำกับงาน (Ver.2.5)');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ov-person-index">
   
    <?php  Pjax::begin(['id'=>'ov-person-grid-pjax', 'timeout' => 10000]);?>
   
    <div class="row" >
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div style="border: 1px solid #ddd; padding: 5px 15px;border-radius: 4px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #1b95e0"></div> 
		<div class="box-inner" style="background-color: #1b95e0"> 1 </div> 
	    </div>
		<div style="display: inline-block;">
		    <span style="font-size: 16px;margin-bottom: 5px;">จากประชากรทั้งหมด <code><?=number_format($total)?></code> ราย ลงทะเบียนใน Isan Cohort (มี PID) แล้ว <code><?=number_format($totalAll)?></code> ราย </span>
			<br>
			ลงทะเบียนเพิ่ม 
		    <?= Html::a('<i class="glyphicon glyphicon-plus"></i> จาก TDC (เดิมมี <code>'.number_format($totalBot).'</code> ราย)', ['/ovcca/ov-person/indexreg'], ['class' => 'btn btn-primary btn-sm', 'style'=>'margin-bottom: 6px;']) ?> 
		    <?=Html::a('<i class="glyphicon glyphicon-plus"></i> จากใบทำบัตร (เดิมมี <code>'.number_format($totalTb).'</code> ราย)', Url::to(['/inputdata/step2',
				    'ezf_id'=>'1437377239070461301',
				    'target'=>'',
				    'comp_id_target'=>'1437725343023632100',
				    ]), ['class' => 'btn btn-success btn-sm', 'style'=>'margin-bottom: 6px;']);?>
		</div>
	    
	    <div class="pull-right">
		<a class="btn btn-danger" href="https://cloudstorage.cascap.in.th/VDO/ControlChart01/ControlChart01.html" target="_blank"><i class="fa fa-youtube-play"></i> VDO สอนการใช้งาน !!</a>
	    </div>
	    </div>
	</div>
    </div>
    <div class="row">
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div style="border: 1px solid #ddd; padding: 5px 15px;border-radius: 4px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #F1C40F"></div> 
		<div class="box-inner" style="background-color: #F1C40F"> 2 </div> 
	    </div>
		
			<?php $form = ActiveForm::begin([
		'id' => 'jump_menu',
		'action' => ['index'],
		'method' => 'get',
		'layout' => 'inline',
		'options' => ['style'=>'display: inline-block;']	    
	    ]); 
    
	    ?>
			<div style="margin-bottom: -10px;">
			    <span style="font-size: 16px;">กลุ่มที่ลงทะเบียนใน Isan Cohort ซึ่งมีจำนวน <code><?=number_format($totalAll)?></code> ราย </span>
			    <br>
		<?php
		$datasub = ArrayHelper::map($dataOvFilterSub, 'sub_id', 'sub_name');
		$datasubstatus = ArrayHelper::map($dataOvFilterSub, 'sub_id', 'urine_status');
		$ovshow = ($ovfilter_sub!=0)?ArrayHelper::getValue($datasubstatus, $ovfilter_sub):1;
		?>	
			    จัดการข้อมูล 
		
		<?=Html::button('<span class="glyphicon glyphicon-plus"></span> สร้างใบกำกับงาน', ['data-url'=>Url::to(['/ovcca/ov-person/ovfilter-sub']), 'class' => 'btn btn-success btn-sm', 'id'=>'addbtn-filter-sub'])?>
		<?= Html::dropDownList('ovfilter_sub', $ovfilter_sub, $datasub, ['class'=>'form-control input-sm', 'prompt'=>'แสดงทั้งหมด ('.  number_format($totalAll).')', 'onChange'=>'$("#jump_menu").submit()'])?>
		<?= Html::a('<span class="glyphicon glyphicon-pencil"></span> จัดการใบกำกับงาน', ['/ovcca/ov-filter-sub/index'], ['class' => 'btn btn-primary btn-sm']) ?>
		    <?php
		if($ovfilter_sub>0){
		    echo Html::button('<span class="glyphicon glyphicon-transfer"></span> จัดการกลุ่มเป้าหมายในใบกำกับงาน', ['data-url'=>Url::to(['ov-person/addlist', 'ovfilter_sub'=>$ovfilter_sub]), 'class' => 'btn btn-sm btn-warning addlistbtn']).' '; 
		    
		    
		}
		echo Html::button('<span class="glyphicon glyphicon-print"></span> พิมพ์รายงาน', ['id'=>'reportOvcca', 'data-url'=>Url::to(['ov-person/report-ovcca', 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default btn-sm']).' ';
		echo Html::button('<span class="glyphicon glyphicon-tags"></span> พิมพ์สติ๊กเกอร์แบบ 2+6', ['data-url'=>Url::to(['ov-person/report-sticker', 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default reportSticker btn-sm']).' ';
		echo Html::button('<span class="glyphicon glyphicon-tags"></span> พิมพ์สติ๊กเกอร์แบบ 1+2', ['data-url'=>Url::to(['ov-person/report-sticker', 'sizemini'=>1, 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default reportSticker btn-sm']).' ';
		echo Html::button('<span class="glyphicon glyphicon-export"></span> export', ['id'=>'reportExel', 'data-url'=>Url::to(['ov-person/report-exel', 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub]), 'class' => 'btn  btn-default btn-sm']).' ';
		?>
		
	    <?php ActiveForm::end(); ?>
		    </div>
		</div>    
	</div>
    </div>
    
<!--    <div class="row">
	<div class="col-md-12 " style="margin-bottom: 15px;">
	    <div class="box-title"> 
		<div class="box-arrow" style="border-top-color: #31b20e"></div> 
		<div class="box-inner" style="background-color: #31b20e"> 3 </div> 
	    </div>
	    
	    
	</div>
    </div>
    <hr>-->
<!--    <ul class="nav nav-tabs">
	<li role="presentation" class="active"><a href="<?=  Url::to(['/ovcca/ov-person/index'])?>">หน้าหลัก</a></li>
	<li role="presentation" ><a href="<?=  Url::to(['/ovcca/ov-person/index-mini'])?>">บันทึกผลตรวจ</a></li>
    </ul>-->
    
    <?php
    $panelBtn = '<span><strong>จำนวนที่เลือก</strong></span>  <font color="#ff0000"><span class="cart-num">0</span></font>  เพื่อ ' .
		      Html::button('<span class="glyphicon glyphicon-tags"></span> พิมพ์สติ๊กเกอร์เฉพาะรายชื่อที่เลือก', ['data-url'=>Url::to(['ov-person/report-sticker']), 'class' => 'btn btn-default btn-sm reportSticker', 'id'=>'modal-stickerbtn-ov-person', 'disabled'=>true]).' '.
		      Html::button('<span class="glyphicon glyphicon-plus"></span> เพิ่มรายชื่อที่เลือกลงในใบกำกับงาน', ['data-url'=>Url::to(['ov-person/add-selectlist']), 'class' => 'btn btn-success btn-sm addlistbtn', 'id'=>'modal-addlistbtn-ov-person', 'disabled'=>true]). ' '.
		Html::a('<i class="glyphicon glyphicon-edit"></i> แก้ไขการเข้ารหัส', ['/ovcca/tmp-person/edit'], ['class' => 'btn btn-warning btn-sm']);
    ?>
    <div class="row" >
	<div class="col-md-12">
	    <?= $panelBtn;?>
	    <code>***ถ้าพิมพ์สติ๊กเกอร์ไม่ได้ให้รีเฟรช google chrome</code>
	</div>
	
    </div>

    <div id="divToScroll" class="table-responsive" >
	<?php
	$mooList = backend\modules\ovcca\classes\OvccaQuery::getMoo($sitecode);
	$mooMap = ArrayHelper::map($mooList, 'id', 'text');
	?>
	<p style="margin-top: 10px;">[<i class="glyphicon glyphicon-plus" style="color:#00a65a;"></i> เพิ่มข้อมูลใหม่] [<i class="glyphicon glyphicon-ok" style="color:#3c8dbc;"></i> บันทึกข้อมูลแล้ว] [<i class="glyphicon glyphicon-plus-sign" style="color:#d73925;"></i> ผลตรวจเป็นบวก] [<i class="glyphicon glyphicon-minus-sign" style="color:#008d4c;"></i> ผลตรวจเป็นลบ] [<i class="glyphicon glyphicon-exclamation-sign" style="color:#999;"></i> ยังไม่ลงผลตรวจ]</p>
    <?= \common\lib\sdii\widgets\SDGridView::widget([
	'id' => 'ov-person-grid',
	'panel' => false,
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionOvPersonIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'min-width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
	    ],
	    
	    
	    [
		'attribute'=>'title',
		'label'=>'คำนำหน้า',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:85px; text-align: center;'],
	    ],
            [
		'attribute'=>'name',
		'label'=>'ชื่อ',
		'contentOptions'=>['style'=>'min-width:160px;'],
	    ],
	    [
		'attribute'=>'surname',
		'label'=>'นามสกุล',
		'contentOptions'=>['style'=>'min-width:160px;'],
	    ],
	    [
		'attribute'=>'cid',
		'label'=>'CID',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:120px; text-align: center;'],
	    ],
	    [
		'attribute'=>'hsitecode',
		'label'=>'SiteCode',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:100px; text-align: center;'],
	    ],
	    [
		'attribute'=>'hptcode',
		'label'=>'PID',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:80px; text-align: center;'],
	    ],
            [
		'attribute'=>'age',
		'label'=>'อายุ',
		//'value'=>function ($data){ return \backend\modules\ovcca\classes\OvccaFunc::getAge($data['v2']); },
		//'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:60px; text-align: center;'],
	    ],
	    [
		'attribute'=>'add1n1',
		'label'=>'บ้านเลขที่',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:85px; text-align: center;'],
	    ],	
	[
		'attribute'=>'add1n5',
		'label'=>'หมู่',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:60px; text-align: center;'],
		'filter'=>  Html::activeDropDownList($searchModel, 'add1n5', $mooMap, ['class'=>'form-control', 'prompt'=>'All']),
	    ],		
	
	    [
		'header'=>'Register',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			$icon = 'class="glyphicon glyphicon-ok"';
			$rurl = base64_encode(Yii::$app->request->url);

			return Html::a('<i '.$icon.'></i>', Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1437377239070461301', 'dataid'=>$data['id'], 'rurl'=>$rurl]), [
			    'class' => 'btn-lg',
			]);
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:80px; text-align: center;'],
	    ],	
	    [
		'header'=>'CCA-01',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			$rowReturn = '';
			
			if ($data['cca01_id']!=null) {
			    $arrStr = explode(',', $data['cca01_id']);
			    
			    foreach ($arrStr as $value) {
				$icon = 'class="glyphicon glyphicon-ok" style="font-size: 18px;"';
				$rurl = base64_encode(Yii::$app->request->url);

				$rowReturn .= Html::a('<i '.$icon.'></i>', Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1437377239070461302', 'dataid'=>$value, 'rurl'=>$rurl]), [
				   
				]).' ';
			    }
			} else {
			    $rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
				'ezf_id'=>'1437377239070461302',
				'target'=>base64_encode($data->ptid),
				'comp_id_target'=>'1437725343023632100',
				'rurl'=>base64_encode(Yii::$app->request->url),
				]), [

			    ]);
			}
			
			
			return $rowReturn;
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:70px;text-align: center; '],
	    ],
	[
		'header'=>'OV-01K',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			
			if ($data['ov01k_id']!=null) {
			    $arrStr = explode(',', $data['ov01k_id']);
			    $arrResult = explode(',', $data['ov01k_result']);
			    $rowReturn = '';
			    
			    $rurl = base64_encode(Yii::$app->request->url);
				
			    foreach ($arrStr as $key=>$value) {
				
				if($arrResult[$key]=='1'){
				    $icon = '<i class="glyphicon glyphicon-plus-sign" style="font-size: 18px;"></i>';
				    $color = '#d73925;';
				} elseif($arrResult[$key]=='0'){
				    $icon = '<i class="glyphicon glyphicon-minus-sign" style="font-size: 18px;"></i>';
				    $color = '#008d4c;';
				} else {
				    $icon = '<i class="glyphicon glyphicon-exclamation-sign" style="font-size: 18px;"></i>';
				    $color = '#999;';
				}
				
				$rowReturn .= Html::a($icon, Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1455214361078703000', 'dataid'=>$value, 'rurl'=>$rurl]), [
				    'style'=>'color:'.$color
				]).' ';
			    }

			    
			} 
			
			$rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			    'ezf_id'=>'1455214361078703000',
			    'target'=>base64_encode($data->ptid),
			    'comp_id_target'=>'1437725343023632100',
			    'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
			    
			]);
			
			return $rowReturn;
			
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:110px;'],
		'visible'=>$ovshow
	    ],
	[
		'header'=>'OV-01P',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			
			if ($data['ov01p_id']!=null) {
			    $arrStr = explode(',', $data['ov01p_id']);
			    $arrResult = explode(',', $data['ov01p_result']);
			    $rowReturn = '';
			    
			    $rurl = base64_encode(Yii::$app->request->url);
				
			    foreach ($arrStr as $key=>$value) {
				
				if($arrResult[$key]=='1'){
				    $icon = '<i class="glyphicon glyphicon-plus-sign" style="font-size: 18px;"></i>';
				    $color = '#d73925;';
				} elseif($arrResult[$key]=='0'){
				    $icon = '<i class="glyphicon glyphicon-minus-sign" style="font-size: 18px;"></i>';
				    $color = '#008d4c;';
				}else {
				    $icon = '<i class="glyphicon glyphicon-exclamation-sign" style="font-size: 18px;"></i>';
				    $color = '#999;';
				}
				
				$rowReturn .= Html::a($icon, Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1455214361078703001', 'dataid'=>$value, 'rurl'=>$rurl]), [
				    'style'=>'color:'.$color
				]).' ';
			    }

			    
			} 
			
			$rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			    'ezf_id'=>'1455214361078703001',
			    'target'=>base64_encode($data->ptid),
			    'comp_id_target'=>'1437725343023632100',
			    'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
			    
			]);
			
			return $rowReturn;
			
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:110px;'],
			'visible'=>$ovshow
	    ],
	[
		'header'=>'OV-01F',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			
			if ($data['ov01f_id']!=null) {
			    $arrStr = explode(',', $data['ov01f_id']);
			    $arrResult = explode(',', $data['ov01f_result']);
			    $rowReturn = '';
			    
			    $rurl = base64_encode(Yii::$app->request->url);
				
			    foreach ($arrStr as $key=>$value) {
				
				if($arrResult[$key]=='1'){
				    $icon = '<i class="glyphicon glyphicon-plus-sign" style="font-size: 18px;"></i>';
				    $color = '#d73925;';
				} elseif($arrResult[$key]=='0'){
				    $icon = '<i class="glyphicon glyphicon-minus-sign" style="font-size: 18px;"></i>';
				    $color = '#008d4c;';
				}else {
				    $icon = '<i class="glyphicon glyphicon-exclamation-sign" style="font-size: 18px;"></i>';
				    $color = '#999;';
				}
				
				$rowReturn .= Html::a($icon, Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1455214361078703002', 'dataid'=>$value, 'rurl'=>$rurl]), [
				    'style'=>'color:'.$color
				]).' ';
			    }

			    
			} 
			
			$rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			    'ezf_id'=>'1455214361078703002',
			    'target'=>base64_encode($data->ptid),
			    'comp_id_target'=>'1437725343023632100',
			    'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
			    
			]);
			
			return $rowReturn;
			
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:110px;'],
			'visible'=>$ovshow
	    ],	
	[
		'header'=>'OV-01U',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			
			if ($data['ov01u_id']!=null) {
			    $arrStr = explode(',', $data['ov01u_id']);
			    $arrResult = explode(',', $data['ov01u_result']);
			    $rowReturn = '';
			    
			    $rurl = base64_encode(Yii::$app->request->url);
				
			    foreach ($arrStr as $key=>$value) {
				
				if($arrResult[$key]=='1'){
				    $icon = '<i class="glyphicon glyphicon-plus-sign" style="font-size: 18px;"></i>';
				    $color = '#d73925;';
				} elseif($arrResult[$key]=='0'){
				    $icon = '<i class="glyphicon glyphicon-minus-sign" style="font-size: 18px;"></i>';
				    $color = '#008d4c;';
				}else {
				    $icon = '<i class="glyphicon glyphicon-exclamation-sign" style="font-size: 18px;"></i>';
				    $color = '#999;';
				}
				
				$rowReturn .= Html::a($icon, Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1455214361078703003', 'dataid'=>$value, 'rurl'=>$rurl]), [
				    'style'=>'color:'.$color
				]).' ';
			    }

			} 
			
			$rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			    'ezf_id'=>'1455214361078703003',
			    'target'=>base64_encode($data->ptid),
			    'comp_id_target'=>'1437725343023632100',
			    'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
			    
			]);
			
			return $rowReturn;
			
			
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:110px; '],
			'visible'=>$ovshow
	    ],	
	[
		'header'=>'Ov-02',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			
			if ($data['ov02_id']!=null) {
			    $arrStr = explode(',', $data['ov02_id']);
			    $rowReturn = '';
			    foreach ($arrStr as $value) {
				$icon = 'class="glyphicon glyphicon-ok" style="font-size: 18px;"';
				$rurl = base64_encode(Yii::$app->request->url);

				$rowReturn .= Html::a('<i '.$icon.'></i>', Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1455257503082760700', 'dataid'=>$value, 'rurl'=>$rurl]), [
				   
				]).' ';
			    }

			}
			
			$rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			    'ezf_id'=>'1455257503082760700',
			    'target'=>base64_encode($data->ptid),
			    'comp_id_target'=>'1437725343023632100',
			    'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
			    
			]);
			
			return $rowReturn;
			
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:110px; '],
	    ],		
	[
		'header'=>'OV-03',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			
			if ($data['ov03_id']!=null) {
			    $arrStr = explode(',', $data['ov03_id']);
			    $rowReturn = '';
			    foreach ($arrStr as $value) {
				$icon = 'class="glyphicon glyphicon-ok" style="font-size: 18px;"';
				$rurl = base64_encode(Yii::$app->request->url);

				$rowReturn .= Html::a('<i '.$icon.'></i>', Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1455222779086150700', 'dataid'=>$value, 'rurl'=>$rurl]), [
				    
				]).' ';
			    }

			}
			
			$rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			    'ezf_id'=>'1455222779086150700',
			    'target'=>base64_encode($data->ptid),
			    'comp_id_target'=>'1437725343023632100',
			    'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
			    
			]);
			
			return $rowReturn;
			
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:110px;'],
	    ],	
	[
		'header'=>'CCA-02',
		'format'=>'raw',
		'value'=>function ($data){ 
		    if ($data['id']!=null) {
			
			if ($data['cca02_id']!=null) {
			    $arrStr = explode(',', $data['cca02_id']);
			    $rowReturn = '';
			    foreach ($arrStr as $value) {
				$icon = 'class="glyphicon glyphicon-ok" style="font-size: 18px;"';
				$rurl = base64_encode(Yii::$app->request->url);

				$rowReturn .= Html::a('<i '.$icon.'></i>', Url::to(['/inputdata/redirect-page', 'ezf_id'=>'1437619524091524800', 'dataid'=>$value, 'rurl'=>$rurl]), [
				   
				]).' ';
			    }

			}
			$rowReturn .= Html::a('<i class="glyphicon glyphicon-plus" style="color:#00a65a;font-size: 18px;"></i>', Url::to(['/inputdata/step4',
			    'ezf_id'=>'1437619524091524800',
			    'target'=>base64_encode($data->ptid),
			    'comp_id_target'=>'1437725343023632100',
			    'rurl'=>base64_encode(Yii::$app->request->url),
			    ]), [
			    
			]);
			
			return $rowReturn;
		    } else {
			return '';
		    }
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:110px;'],
	    ],			
//	    [
//		'class' => 'appxq\sdii\widgets\ActionColumn',
//		'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
//		'template' => '{delete}',
//		'buttons'=>[
//		    'delete'=>function ($url, $model, $key) {
//			return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
//			    'data-action' => 'delete',
//			    'title' => Yii::t('yii', 'ย้ายออกจากใบกำกับงาน (ข้อมูลยังคงอยู่)'),
//			    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
//			    'data-method' => 'post',
//			    'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
//			]);
//		    }
//		]
//	    ],
        ],
    ]); ?>
	</div>
    
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-ov-person',
    'size'=>'modal-lg',
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-ovlist',
    'size'=>'modal-lg',
]);

$fix = '';
if($dataProvider->count>0){
    $fix = "var table = $('.table').DataTable( {
        scrollY:        '60vh',
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
	searching:      false,
        fixedColumns:   {
            leftColumns: 5
        }
    } );";
}

?>


<?php  $this->registerJs("

	

$('#divToScroll').attachDragger();

$('#ov-person-grid-pjax').on('click', '#modal-addbtn-ov-person', function() {
    modalOvPerson($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#modal-delbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#modal-delallbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#modal-importbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#ov-person-grid').yiiGridView('getSelectedRows');
	disabledOvPersonBtn(key.length);
    },100);
});

$('#ov-person-grid-pjax').on('click', '.selectionOvPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledOvPersonBtn(key.length);
});

$('#ov-person-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    //modalOvPerson('".Url::to(['ov-person/update', 'id'=>''])."'+id);
});	

$('#ov-person-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalOvPerson(url);
	return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
	return false;
    } else if(action === 'ov01') {
	return false;
    } else if(action === 'ov01-status') {
	$.post(
	    url
	).done(function(result) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$.pjax.reload({container:'#ov-person-grid-pjax'});
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}).fail(function() {
	    ". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	    console.log('server error');
	});
	return false;
    }
   
});

//$('#ov-person-grid-pjax').on('click', '#select_all', function() {
//    if($(this).prop('checked')){
//	$('#modal-importallbtn-ov-person').attr('disabled', false);
//	$('#select_show').html($(this).attr('data-count'));
//	$('.cart-num').html($(this).attr('data-count'));
//    } else {
//	$('#modal-importallbtn-ov-person').attr('disabled', true);
//	$('#select_show').html(0);
//	$('.cart-num').html(0);
//    }
//});

$('#ov-person-grid-pjax').on('click', '#modal-importallbtn-ov-person', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

function disabledOvPersonBtn(num) {
    if(num>0) {
	$('#modal-stickerbtn-ov-person').attr('disabled', false);
	$('#modal-addlistbtn-ov-person').attr('disabled', false);
    } else {
	$('#modal-stickerbtn-ov-person').attr('disabled', true);
	$('#modal-addlistbtn-ov-person').attr('disabled', true);
    }
    $('.cart-num').html(num);
}

$('#reportOvcca, #reportOvcca1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('.reportSticker').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportDetail, #reportDetail1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportExel, #reportExel1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonExel(url);
});

function selectionOvPersonExel(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-ov-person .modal-content').html(result.html);
		
		$('#modal-ov-person').modal('hide');
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonReport(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		    $('#modal-ov-person .modal-content').html(result.html);
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function statusOvPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to update status all items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

$('#ov-person-grid-pjax').on('click', '#modal-statust-ov-person', function() {
    statusOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#modal-statusf-ov-person', function() {
    statusOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax').on('click', '#addbtn-filter', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#ov-person-grid-pjax').on('click', '#addbtn-filter-sub', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#ov-person-grid-pjax').on('click', '.addlistbtn', function() {
    modalOvList($(this).attr('data-url'));
});

function modalOvList(url) {
    $('#modal-ovlist .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ovlist').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $('#modal-ovlist .modal-content').html(result.html);
	}
    });
}

function modalOvPerson(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show')
    .find('.modal-content')
    .load(url);
}

$('#ov-person-grid-pjax').on('click', '.btn-action-ezf', function() {
    var target = $(this).attr('data-target');
    var url = $(this).attr('href');
    var ezf_id = $(this).attr('data-ezf_id');
    var comp_id = $(this).attr('data-comp_id');
    
    $.post( url, {ezf_id: ezf_id, target: $.trim(target), comp_id_target: comp_id, rurl: '". base64_encode(Yii::$app->request->url)."'}, function(result){
	$(location).attr('href',result);
    });
    
});


");?>