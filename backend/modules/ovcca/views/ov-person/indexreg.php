<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use kartik\field\FieldRange;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ovcca\models\OvPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการลงทะเบียนใน Isan Cohort';
$this->params['breadcrumbs'][] = 'จัดการลงทะเบียนใน Isan Cohort';

?>
<div class="ov-person-index-34">
    <?php  Pjax::begin(['id'=>'ov-person-grid-pjax1']);?>
    
    <h4 style="color: #15c">ก) สรุป</h4>
    <div style="border: 1px solid #ddd; padding: 10px;border-radius: 4px;margin-bottom: 15px;">
	<span style="font-size: 16px;margin-bottom: 5px;">กลุ่มเป้าหมายที่ได้รับการลงทะเบียนใน Isan Cohort (มี PID) แล้ว <code><?=number_format($totalAll)?></code> ราย</span>
	<?php echo Html::a('<i class="fa fa-reply"></i> กลับไปจัดการใบกำกับงาน', ['/ovcca/ov-person/index'], ['class' => 'btn btn-sm btn-default']) ?>

	<div style="font-size: 16px;margin-bottom: 5px;">จากประชากรทั้งหมด <code><?=number_format($total)?></code> ราย ถูกนำเข้าเพื่อลงทะเบียน <code><?=number_format($ovCount)?></code> ราย ( ลงทะเบียนแล้ว <code><?=number_format($ovJoinCount)?></code> ราย ยังไม่ลงทะเบียน <code><?=number_format($totalTb)?></code> ราย )</div>
	
    </div>
    
    <h4 style="color: #15c">ข) นำเข้าจาก TDC โดยช่องทางใดทางหนึ่งต่อไปนี้</h4>
    <h4 style="color: #15c">ข.1) ค้นจากรหัสบัตรประชาชน</h4>
    <div style="border: 1px solid #ddd; padding: 10px;border-radius: 4px;margin-bottom: 15px;">
	<?php $form = ActiveForm::begin([
	    'id'=>'findbot-form',
	    'layout'=>'inline',
	]); ?>
	
	<div class="form-group">
	    <input type="number" name="findbot[cid]" value="<?=$cid?>" class="form-control" placeholder="เลขบัตรประชาชน 13 หลัก" style="width: 250px;">
	</div>
	<div class="form-group">
	    <label>กุญแจถอดรหัสข้อมูล</label>
	    <input type="password" name="findbot[key]" value="<?=$key?>" class="form-control" placeholder="โปรดกรอกรหัสที่กำหนดใน TDC" style="width: 250px;">
	</div>
	<div class="checkbox">
	    <label>
		<input type="checkbox" name="findbot[convert]" <?=$convert==1?'checked':''?> value="1"> เข้ารหัสแบบ tis620
	    </label>
	</div>
	
	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-import"></i> นำเข้าข้อมูล</button>
	<?php ActiveForm::end(); ?>
    </div>
    
    <h4 style="color: #15c">ข.2) นำเข้าเป็นรายหมู่บ้าน</h4>
	<?= Html::a('<i class="glyphicon glyphicon-import"></i> นำเข้าจาก TDC', ['/ovcca/tmp-person/index'], ['class' => 'btn btn-primary ']) ?>
    <h4 style="color: #15c">ค) ดำเนินการลงทะเบียน (ให้ PID)</h4>
    <?php
    $panelBtn = '<span><strong>จำนวนที่เลือก</strong></span>  <font color="#ff0000"><span class="cart-num">0</span></font>  เพื่อ ';
    ?>
    <div class="row">
	<div class="col-md-12">
	    <?= $panelBtn;?>
	    <?=Html::button('<span class="glyphicon glyphicon-check"></span> ลงทะเบียนที่เลือกออก', ['data-url'=>Url::to(['ov-person/import-views', 'select'=>1]), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-importbtn-ov-person', 'disabled'=>true, 'data-toggle'=>'tooltip', 'title'=>'']);?>
	    <?=Html::button('<i class="glyphicon glyphicon-plus"></i> ลงทะเบียนทั้งหมด', ['data-url'=>Url::to(['ov-person/import-views']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-importallbtn-ov-person'])?>
	    
	    <?=Html::button('<span class="glyphicon glyphicon-check"></span> เคลียร์ที่เลือกออก', ['data-url'=>Url::to(['ov-person/deletes']), 'class' => 'btn btn-warning btn-sm', 'id'=>'modal-delbtn-ov-person', 'disabled'=>true, 'data-toggle'=>'tooltip', 'title'=>'การลบรายการที่แสดงในตารางนี้ไม่มีผลต่อข้อมูลแต่อย่างใด']);?>
	    <?=Html::button('<span class="glyphicon glyphicon-trash"></span> เคลียร์', ['data-url'=>Url::to(['ov-person/deletes-all']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delallbtn-ov-person', 'data-toggle'=>'tooltip', 'title'=>'การลบรายการที่แสดงในตารางนี้ไม่มีผลต่อข้อมูลแต่อย่างใด']);?>
	    
	</div>
    </div>
    <div id="divToScroll" class="table-responsive" style="margin-top: 10px;">
	
    
    <?= \common\lib\sdii\widgets\SDGridView::widget([
	'id' => 'ov-person-grid',
	'panel' => false,
	'panelBtn' => //Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['ov-person/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ov-person']). ' ' .
		      '<span><strong>จำนวนที่เลือก</strong></span>  <font color="#ff0000"><span class="cart-num">0</span></font> ' .
		      Html::button('<span class="glyphicon glyphicon-trash"></span> ', ['data-url'=>Url::to(['ov-person/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-ov-person', 'disabled'=>true]).' '.
		      Html::button('ตรวจอุจจาระทั้งหมด ', ['data-url'=>Url::to(['ov-person/statust']), 'class' => 'btn btn-info btn-sm', 'id'=>'modal-statust-ov-person']).' '.
		      Html::button('ไม่ตรวจอุจจาระทั้งหมด ', ['data-url'=>Url::to(['ov-person/statusf']), 'class' => 'btn btn-default btn-sm', 'id'=>'modal-statusf-ov-person']),
		      
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionOvPersonIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'min-width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
	    ],
	    
	    [
		'attribute'=>'cid',
		'label'=>'CID',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:120px; text-align: center;'],
	    ],
	    [
		'attribute'=>'pname',
		'label'=>'คำนำหน้า',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:85px; text-align: center;'],
	    ],
            [
		'attribute'=>'fname',
		'label'=>'ชื่อ',
		'contentOptions'=>['style'=>'min-width:160px;'],
	    ],
	    [
		'attribute'=>'lname',
		'label'=>'นามสกุล',
		'contentOptions'=>['style'=>'min-width:160px;'],
	    ],
	    [
		'attribute'=>'status_import',
		'label'=>'ลงทะเบีนยนแล้ว?',
		'value'=>function ($data){ return $data['status_import']==1?'<font color="#31b20e">Yes</font>':'<font color="#ff0000">No</font>'; },
		'format'=>'raw',	
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:60px; text-align: center;'],
	    ],
            [
		'attribute'=>'birthdate',
		'label'=>'อายุ',
		'value'=>function ($data){ return \backend\modules\ovcca\classes\OvccaFunc::getAge($data['birthdate']); },
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:60px; text-align: center;'],
	    ],
	    [
		'attribute'=>'address',
		'label'=>'บ้านเลขที่',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:85px; text-align: center;'],
	    ],	
	[
		'attribute'=>'moo',
		'label'=>'หมู่',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:60px; text-align: center;'],
	    ],		
			
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
		'template' => '{delete}',
		'buttons'=>[
		    'delete'=>function ($url, $model, $key) {
			return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
			    'data-action' => 'delete',
			    'title' => Yii::t('yii', 'การลบรายการที่แสดงในตารางนี้ไม่มีผลต่อข้อมูลแต่อย่างใด'),
			    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
			    'data-method' => 'post',
			    'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
			]);
		    }
		]
	    ],
        ],
    ]); ?>
	</div>
    
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-ov-person',
    'size'=>'modal-lg',
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-ovlist',
    'size'=>'modal-sm',
]);
?>

<?php  $this->registerJs("
    
$('#divToScroll').attachDragger();

$('#ov-person-grid-pjax1').on('click', '#modal-addbtn-ov-person', function() {
    modalOvPerson($(this).attr('data-url'));
});

$('#ov-person-grid-pjax1').on('click', '#modal-delbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax1').on('click', '#modal-delallbtn-ov-person', function() {
    selectionOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax1').on('click', '#modal-importbtn-ov-person', function() {
    selectionOvPersonGridon($(this).attr('data-url'));
});

$('#ov-person-grid-pjax1').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#ov-person-grid').yiiGridView('getSelectedRows');
	disabledOvPersonBtn(key.length);
    },100);
});

$('#ov-person-grid-pjax1').on('click', '.selectionOvPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledOvPersonBtn(key.length);
});

$('#ov-person-grid-pjax1').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    //modalOvPerson('".Url::to(['ov-person/update', 'id'=>''])."'+id);
});	

$('#ov-person-grid-pjax1').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalOvPerson(url);
	return false;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax1'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
	return false;
    } else if(action === 'ov01') {
	return false;
    } else if(action === 'ov01-status') {
	$.post(
	    url
	).done(function(result) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$.pjax.reload({container:'#ov-person-grid-pjax1'});
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}).fail(function() {
	    ". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	    console.log('server error');
	});
	return false;
    }
   
});

//$('#ov-person-grid-pjax1').on('click', '#select_all', function() {
//    if($(this).prop('checked')){
//	$('#modal-importallbtn-ov-person').attr('disabled', false);
//	$('#select_show').html($(this).attr('data-count'));
//	$('.cart-num').html($(this).attr('data-count'));
//    } else {
//	$('#modal-importallbtn-ov-person').attr('disabled', true);
//	$('#select_show').html(0);
//	$('.cart-num').html(0);
//    }
//});

$('#ov-person-grid-pjax1').on('click', '#modal-importallbtn-ov-person', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

function disabledOvPersonBtn(num) {
    if(num>0) {
	$('#modal-delbtn-ov-person').attr('disabled', false);
	$('#modal-addlistbtn-ov-person').attr('disabled', false);
	$('#modal-importbtn-ov-person').attr('disabled', false);
	
    } else {
	$('#modal-delbtn-ov-person').attr('disabled', true);
	$('#modal-addlistbtn-ov-person').attr('disabled', true);
	$('#modal-importbtn-ov-person').attr('disabled', true);
    }
    $('.cart-num').html(num);
}

$('#reportOvcca, #reportOvcca1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportSticker, #reportSticker1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportStickerMini, #reportStickerMini1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportDetail, #reportDetail1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});

$('#reportExel, #reportExel1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonExel(url);
});

function selectionOvPersonExel(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		$('#modal-ov-person .modal-content').html(result.html);
		
		$('#modal-ov-person').modal('hide');
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonReport(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		    $('#modal-ov-person .modal-content').html(result.html);
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}

function selectionOvPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax1'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function selectionOvPersonGridon(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#modal-ov-person .modal-content').html(result);
		". SDNoty::show('result.message', 'result.status') ."
	    }
	});
}

function statusOvPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to update status all items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-person-grid-pjax1'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

$('#ov-person-grid-pjax1').on('click', '#modal-statust-ov-person', function() {
    statusOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax1').on('click', '#modal-statusf-ov-person', function() {
    statusOvPersonGrid($(this).attr('data-url'));
});

$('#ov-person-grid-pjax1').on('click', '#addbtn-filter', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#ov-person-grid-pjax1').on('click', '#addbtn-filter-sub', function() {
    var url = $(this).attr('data-url');
    modalOvPerson(url);
});

$('#ov-person-grid-pjax1').on('click', '#modal-addlistbtn-ov-person', function() {
    modalOvList($(this).attr('data-url'));
});

function modalOvList(url) {
    $('#modal-ovlist .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ovlist').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	data: $('.selectionOvPersonIds:checked[name=\"selection[]\"]').serialize(),
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $('#modal-ovlist .modal-content').html(result.html);
	}
    });
}

function modalOvPerson(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show')
    .find('.modal-content')
    .load(url);
}

$('#ov-person-grid-pjax1').on('click', '.btn-action-ezf', function() {
    var target = $(this).attr('data-target');
    var url = $(this).attr('href');
    var ezf_id = $(this).attr('data-ezf_id');
    var comp_id = $(this).attr('data-comp_id');
    
    $.post( url, {ezf_id: ezf_id, target: $.trim(target), comp_id_target: comp_id, rurl: '". base64_encode(Yii::$app->request->url)."'}, function(result){
	$(location).attr('href',result);
    });
    
});


");?>