<?php 
use yii\helpers\Html;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">ลงทะเบียนใน Isan Cohort</h4>
</div>

<div class="modal-body">
    <table class="table">
	<tr>
	    <td><h2>เลือกเพื่อลงทะเบียนในครั้งนี้</h2></td>
	    <td><h2><?=$sum-$sumF?> ราย</h2></td>
	</tr>
	<tr>
	    <td><h2>ลงทะเบียนได้ในครั้งนี้</h2></td>
	    <td><h2><?=$sumT+$sumN?> ราย</h2></td>
	</tr>
	<tr>
	    <td><h2>ลงทะเบียนไม่ได้ในครั้งนี้</h2></td>
	    <td><h2><?=$sumF?> ราย</h2></td>
	</tr>
	
    </table>
    <?php if($sumF>0): ?>
    <code>*หมายเหตุ : ลงทะเบียนไม่ได้ อาจเนื่องมาจาก เลขบัตรไม่ถูกต้อง, ไม่ได้ถอดรหัสข้อมูล, ข้อมูลไม่ครบถ้วน</code>
    <?php endif;?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
</div>

<?php  $this->registerJs("
    
$.pjax.reload({container:'#ov-person-grid-pjax1'});
");?>