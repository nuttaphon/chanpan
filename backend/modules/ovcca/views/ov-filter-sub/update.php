<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\OvFilterSub */

$this->title = Yii::t('app', 'แก้ไข{modelClass}: ', [
    'modelClass' => 'ใบกำกับงาน',
]) . ' ' . $model->sub_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ov Filter Subs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sub_id, 'url' => ['view', 'id' => $model->sub_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ov-filter-sub-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
