<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ovcca\models\OvFilterSubSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ใบกำกับงาน');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ov-filter-sub-index">

    <div class="ov-person-back" style="margin-bottom: 15px; text-align: right;">
	<?php echo Html::a('<i class="fa fa-reply"></i> กลับไปหน้าแสดงใบกำกับงาน', ['/ovcca/ov-person/index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php  Pjax::begin(['id'=>'ov-filter-sub-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'ov-filter-sub-grid',
	'panelBtn' => //Html::a('<span class="glyphicon glyphicon-transfer"></span> เลือกประชากรเข้าใบกำกับงาน', Url::to(['ov-filter-sub/assign']), ['class' => 'btn btn-primary btn-sm']). ' ' .
		    Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['ov-filter-sub/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-ov-filter-sub']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['ov-filter-sub/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-ov-filter-sub', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	//'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionOvFilterSubIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'sub_name',
	    'urine_status',
	    [
		'header'=>'n=?',
		'format'=>'raw',
		'value'=>function ($data){ 
		    $count = \backend\modules\ovcca\models\OvSubList::find()->where('sub_id=:sub_id', ['sub_id'=>$data['sub_id']])->count();
		    
		    return $count;
		},
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'min-width:70px; text-align: center;'],
	    ],	
	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{update} {delete}',
		'buttons' => [
		    'assign' => function ($url, $model) {
			
			    return Html::a('<span class="glyphicon glyphicon-transfer"></span> ', Url::to(['/ovcca/ov-filter-sub/assign', 'id'=>$model['sub_id']]), [
				    'data-action' => 'assign',
                                    'title' => Yii::t('app', 'Assign'),
			    ]);
			
                    },
		    
		]
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-ov-filter-sub',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#ov-filter-sub-grid-pjax').on('click', '#modal-addbtn-ov-filter-sub', function() {
    modalOvFilterSub($(this).attr('data-url'));
});

$('#ov-filter-sub-grid-pjax').on('click', '#modal-delbtn-ov-filter-sub', function() {
    selectionOvFilterSubGrid($(this).attr('data-url'));
});

$('#ov-filter-sub-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#ov-filter-sub-grid').yiiGridView('getSelectedRows');
	disabledOvFilterSubBtn(key.length);
    },100);
});

$('#ov-filter-sub-grid-pjax').on('click', '.selectionOvFilterSubIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledOvFilterSubBtn(key.length);
});

$('#ov-filter-sub-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalOvFilterSub('".Url::to(['ov-filter-sub/update', 'id'=>''])."'+id);
});	

$('#ov-filter-sub-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalOvFilterSub(url);
    } else if(action === 'assign') {
	location.href = url;
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-filter-sub-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledOvFilterSubBtn(num) {
    if(num>0) {
	$('#modal-delbtn-ov-filter-sub').attr('disabled', false);
    } else {
	$('#modal-delbtn-ov-filter-sub').attr('disabled', true);
    }
}

function selectionOvFilterSubGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionOvFilterSubIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#ov-filter-sub-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalOvFilterSub(url) {
    $('#modal-ov-filter-sub .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-filter-sub').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>