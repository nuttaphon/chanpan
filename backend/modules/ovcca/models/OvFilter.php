<?php

namespace backend\modules\ovcca\models;

use Yii;

/**
 * This is the model class for table "ov_filter".
 *
 * @property integer $id
 * @property string $name
 * @property string $start
 * @property string $end
 * @property string $sitecode
 */
class OvFilter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ov_filter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start', 'end'], 'required'],
            [['start', 'end'], 'safe'],
	    [['sitecode'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'รอบการทำงาน'),
            'start' => Yii::t('app', 'เริ่ม'),
            'end' => Yii::t('app', 'สิ้นสุด'),
	    'sitecode' => Yii::t('app', 'รหัสโรงพยาบาล'),
        ];
    }
}
