<?php

namespace backend\modules\ovcca\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ovcca\models\TmpPerson;

/**
 * TmpPersonSearch represents the model behind the search form about `backend\modules\ovcca\models\TmpPerson`.
 */
class TmpPersonSearch extends TmpPerson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'person_id'], 'integer'],
            [['khet', 'province', 'amphur', 'tambon', 'hospcode', 'hospname', 'house_id', 'address', 'cid', 'hn', 'pname', 'fname', 'lname', 'sex', 'nationality', 'education', 'type_area', 'religion', 'birthdate', 'village_id', 'village_code', 'village_name', 'pttype', 'pttype_begin_date', 'pttype_expire_date', 'pttype_hospmain', 'pttype_hospsub', 'marrystatus', 'death', 'death_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $village)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $query = TmpPerson::find()->where('hospcode = :sitecode AND house_id = :village_code', [':sitecode'=>$sitecode, ':village_code'=>"$village"]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
	
        $query->andFilterWhere([
            'id' => $this->id,
            'person_id' => $this->person_id,
            'birthdate' => $this->birthdate,
            'pttype_begin_date' => $this->pttype_begin_date,
            'pttype_expire_date' => $this->pttype_expire_date,
            'death_date' => $this->death_date,
	    'village_id' => $this->village_id,
        ]);

        $query->andFilterWhere(['like', 'khet', $this->khet])
            ->andFilterWhere(['like', 'province', $this->province])
            ->andFilterWhere(['like', 'amphur', $this->amphur])
            ->andFilterWhere(['like', 'tambon', $this->tambon])
            ->andFilterWhere(['like', 'hospcode', $this->hospcode])
            ->andFilterWhere(['like', 'hospname', $this->hospname])
            ->andFilterWhere(['like', 'house_id', $this->house_id])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'cid', $this->cid])
            ->andFilterWhere(['like', 'hn', $this->hn])
            ->andFilterWhere(['like', 'pname', $this->pname])
            ->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'nationality', $this->nationality])
            ->andFilterWhere(['like', 'education', $this->education])
            ->andFilterWhere(['like', 'type_area', $this->type_area])
            ->andFilterWhere(['like', 'religion', $this->religion])
            ->andFilterWhere(['like', 'village_code', $this->village_code])
            ->andFilterWhere(['like', 'village_name', $this->village_name])
            ->andFilterWhere(['like', 'pttype', $this->pttype])
            ->andFilterWhere(['like', 'pttype_hospmain', $this->pttype_hospmain])
            ->andFilterWhere(['like', 'pttype_hospsub', $this->pttype_hospsub])
            ->andFilterWhere(['like', 'marrystatus', $this->marrystatus])
            ->andFilterWhere(['like', 'death', $this->death]);

        return $dataProvider;
    }
}
