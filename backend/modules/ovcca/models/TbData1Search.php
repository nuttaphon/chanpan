<?php

namespace backend\modules\ovcca\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ovcca\models\TbData1;

/**
 * TbData1Search represents the model behind the search form about `backend\modules\ovcca\models\TbData1`.
 */
class TbData1Search extends TbData1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'usmobile', 'ptid_link', 'ptid', 'cid_check', 'uidadd', 'uidedit', 'sys_assigncheck', 'confirm', 'pay', 'recieve', 'pidcheck', 'agecheck', 'user_create', 'user_update'], 'integer'],
            [['sitecode', 'ptcode', 'ptcodefull', 'reccheck', 'cid', 'title', 'name', 'surname', 'dadd', 'dedit', 'v2', 'age', 'v3', 'mobile', 'homephone', 'telcontact1', 'telcontact2', 'telcontact3', 'add1n1', 'add1n2', 'add1n3', 'add1n4', 'add1n5', 'add1n6code', 'add1n7code', 'add1n8code', 'add1n9', 'hospitalcurrent', 'hn', 'selfenroll', 'selfenrolldate', 'vconsent', 'vconsentdate', 'vconsentdatedb', 'dlastcheck', 'confirmdate', 'venroll', 'venrolldate', 'venrolldatedb', 'paytime', 'recievedate', 'bankdate', 'sys_dateoficf', 'sys_dateoficfdb', 'sys_ecoficf', 'sys_ecoficfdb', 'rstat', 'addr', 'lat', 'lng', 'geocode', 'sitezone', 'siteprov', 'siteamp', 'sitetmb', 'target', 'xsourcex', 'create_date', 'update_date', 'value1', 'value2', 'value3', 'edattype', 'icf_upload1', 'icf_upload2', 'icf_upload3', 'error', 'hncode', 'hsitecode', 'hptcode', 'cohorttype_chartreview'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ovfilter_sub)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $query = TbData1::find()
		->select("tb_data_1.*,
		    (SELECT group_concat(ov01k.id) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_id,
		    (SELECT group_concat(ov01k.results) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_result,
		    (SELECT group_concat(ov01p.id) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_id,
		    (SELECT group_concat(ov01p.results) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_result,
		    (SELECT group_concat(ov01f.id) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_id,
		    (SELECT group_concat(ov01f.results) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_result,
		    (SELECT group_concat(ov01u.id) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_id,
		    (SELECT group_concat(ov01u.results) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_result,
		    (SELECT group_concat(ov02.id) FROM tbdata_25 ov02 WHERE ov02.ptid = tb_data_1.ptid AND ov02.rstat<>3 GROUP BY ov02.ptid) AS ov02_id,
		    (SELECT group_concat(ov03.id) FROM tbdata_26 ov03 WHERE ov03.ptid = tb_data_1.ptid AND ov03.rstat<>3 GROUP BY ov03.ptid) AS ov03_id,
		    (SELECT group_concat(cca02.id) FROM tb_data_3 cca02 WHERE cca02.ptid = tb_data_1.ptid AND cca02.rstat<>3 GROUP BY cca02.ptid) AS cca02_id,
		    (SELECT group_concat(cca01.id) FROM tb_data_2 cca01 WHERE cca01.ptid = tb_data_1.ptid AND cca01.rstat<>3 GROUP BY cca01.ptid) AS cca01_id
		");
	
	if($ovfilter_sub>0){
	    $query->innerJoin('ov_sub_list', 'ov_sub_list.person_id = tb_data_1.id');
	    $query->where('tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode AND ov_sub_list.sub_id = :sub_id', [':sitecode'=>$sitecode, ':sub_id'=>$ovfilter_sub]);
	}else{
	    $query->where('tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode', [':sitecode'=>$sitecode]);
	}
	
	$query->orderBy('add1n8code, add1n7code, add1n6code, add1n5, add1n1, name, surname');
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

	
	
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

	
	
        $query->andFilterWhere([
            'tb_data_1.id' => $this->id,
            'tb_data_1.usmobile' => $this->usmobile,
            'tb_data_1.ptid_link' => $this->ptid_link,
            'tb_data_1.ptid' => $this->ptid,
            'tb_data_1.cid_check' => $this->cid_check,
            'tb_data_1.uidadd' => $this->uidadd,
            'tb_data_1.dadd' => $this->dadd,
            'tb_data_1.uidedit' => $this->uidedit,
            'tb_data_1.dedit' => $this->dedit,
            'tb_data_1.v2' => $this->v2,
            'tb_data_1.selfenrolldate' => $this->selfenrolldate,
            'tb_data_1.vconsentdatedb' => $this->vconsentdatedb,
            'tb_data_1.sys_assigncheck' => $this->sys_assigncheck,
            'tb_data_1.dlastcheck' => $this->dlastcheck,
            'tb_data_1.confirmdate' => $this->confirmdate,
            'tb_data_1.confirm' => $this->confirm,
            'tb_data_1.venrolldatedb' => $this->venrolldatedb,
            'tb_data_1.pay' => $this->pay,
            'tb_data_1.paytime' => $this->paytime,
            'tb_data_1.recieve' => $this->recieve,
            'tb_data_1.recievedate' => $this->recievedate,
            'tb_data_1.bankdate' => $this->bankdate,
            'tb_data_1.sys_dateoficfdb' => $this->sys_dateoficfdb,
            'tb_data_1.sys_ecoficfdb' => $this->sys_ecoficfdb,
            'tb_data_1.pidcheck' => $this->pidcheck,
            'tb_data_1.agecheck' => $this->agecheck,
            'tb_data_1.user_create' => $this->user_create,
            'tb_data_1.create_date' => $this->create_date,
            'tb_data_1.user_update' => $this->user_update,
            'tb_data_1.update_date' => $this->update_date,
	    'tb_data_1.add1n5' => $this->add1n5,
	    //'tb_data_1.age' => $this->age,
        ]);

	if (!empty($this->age)) {
	    $operator = $this->getOperator($this->age);
            $operand = str_replace($operator,'',$this->age);
            $query->andFilterWhere([$operator, 'age', $operand]);
//	    
//            preg_match('/(<=|>=|<|>|=)/', $this->age, $operator);
//            preg_match('/\d+/', $this->age, $age);
//            $operator = isset($operator[0]) ? $operator[0] : 'like';
//            $age = isset($views[0]) ? $views[0] : '';
//
//            $query->andFilterWhere([$operator,'age', $age]);
        }
	
        $query->andFilterWhere(['like', 'tb_data_1.sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'tb_data_1.ptcode', $this->ptcode])
            ->andFilterWhere(['like', 'tb_data_1.ptcodefull', $this->ptcodefull])
            ->andFilterWhere(['like', 'tb_data_1.reccheck', $this->reccheck])
            ->andFilterWhere(['like', 'tb_data_1.cid', $this->cid])
            ->andFilterWhere(['like', 'tb_data_1.title', $this->title])
            ->andFilterWhere(['like', 'tb_data_1.name', $this->name])
            ->andFilterWhere(['like', 'tb_data_1.surname', $this->surname])
            //->andFilterWhere(['like', 'tb_data_1.age', $this->age])
            ->andFilterWhere(['like', 'tb_data_1.v3', $this->v3])
            ->andFilterWhere(['like', 'tb_data_1.mobile', $this->mobile])
            ->andFilterWhere(['like', 'tb_data_1.homephone', $this->homephone])
            ->andFilterWhere(['like', 'tb_data_1.telcontact1', $this->telcontact1])
            ->andFilterWhere(['like', 'tb_data_1.telcontact2', $this->telcontact2])
            ->andFilterWhere(['like', 'tb_data_1.telcontact3', $this->telcontact3])
            ->andFilterWhere(['like', 'tb_data_1.add1n1', $this->add1n1])
            ->andFilterWhere(['like', 'tb_data_1.add1n2', $this->add1n2])
            ->andFilterWhere(['like', 'tb_data_1.add1n3', $this->add1n3])
            ->andFilterWhere(['like', 'tb_data_1.add1n4', $this->add1n4])
            ->andFilterWhere(['like', 'tb_data_1.add1n6code', $this->add1n6code])
            ->andFilterWhere(['like', 'tb_data_1.add1n7code', $this->add1n7code])
            ->andFilterWhere(['like', 'tb_data_1.add1n8code', $this->add1n8code])
            ->andFilterWhere(['like', 'tb_data_1.add1n9', $this->add1n9])
            ->andFilterWhere(['like', 'tb_data_1.hospitalcurrent', $this->hospitalcurrent])
            ->andFilterWhere(['like', 'tb_data_1.hn', $this->hn])
            ->andFilterWhere(['like', 'tb_data_1.selfenroll', $this->selfenroll])
            ->andFilterWhere(['like', 'tb_data_1.vconsent', $this->vconsent])
            ->andFilterWhere(['like', 'tb_data_1.vconsentdate', $this->vconsentdate])
            ->andFilterWhere(['like', 'tb_data_1.venroll', $this->venroll])
            ->andFilterWhere(['like', 'tb_data_1.venrolldate', $this->venrolldate])
            ->andFilterWhere(['like', 'tb_data_1.sys_dateoficf', $this->sys_dateoficf])
            ->andFilterWhere(['like', 'tb_data_1.sys_ecoficf', $this->sys_ecoficf])
            ->andFilterWhere(['like', 'tb_data_1.rstat', $this->rstat])
            ->andFilterWhere(['like', 'tb_data_1.addr', $this->addr])
            ->andFilterWhere(['like', 'tb_data_1.lat', $this->lat])
            ->andFilterWhere(['like', 'tb_data_1.lng', $this->lng])
            ->andFilterWhere(['like', 'tb_data_1.geocode', $this->geocode])
            ->andFilterWhere(['like', 'tb_data_1.sitezone', $this->sitezone])
            ->andFilterWhere(['like', 'tb_data_1.siteprov', $this->siteprov])
            ->andFilterWhere(['like', 'tb_data_1.siteamp', $this->siteamp])
            ->andFilterWhere(['like', 'tb_data_1.sitetmb', $this->sitetmb])
            ->andFilterWhere(['like', 'tb_data_1.target', $this->target])
            ->andFilterWhere(['like', 'tb_data_1.xsourcex', $this->xsourcex])
            ->andFilterWhere(['like', 'tb_data_1.value1', $this->value1])
            ->andFilterWhere(['like', 'tb_data_1.value2', $this->value2])
            ->andFilterWhere(['like', 'tb_data_1.value3', $this->value3])
            ->andFilterWhere(['like', 'tb_data_1.edattype', $this->edattype])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload1', $this->icf_upload1])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload2', $this->icf_upload2])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload3', $this->icf_upload3])
            ->andFilterWhere(['like', 'tb_data_1.error', $this->error])
            ->andFilterWhere(['like', 'tb_data_1.hncode', $this->hncode])
            ->andFilterWhere(['like', 'tb_data_1.hsitecode', $this->hsitecode])
            ->andFilterWhere(['like', 'tb_data_1.hptcode', $this->hptcode])
            ->andFilterWhere(['like', 'tb_data_1.cohorttype_chartreview', $this->cohorttype_chartreview]);

        return $dataProvider;
    }
    
    public function searchMini($params, $ovfilter_sub)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $query = TbData1::find()
		->select("tb_data_1.*,
		    (SELECT group_concat(ov01k.id) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_id,
		    (SELECT group_concat(ov01k.results) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_result,
		    (SELECT group_concat(ov01k.exdate) FROM tbdata_21 ov01k WHERE ov01k.ptid = tb_data_1.ptid AND ov01k.rstat<>3 GROUP BY ov01k.ptid) AS ov01k_date,
		    (SELECT group_concat(ov01p.id) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_id,
		    (SELECT group_concat(ov01p.results) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_result,
		    (SELECT group_concat(ov01p.exdate) FROM tbdata_22 ov01p WHERE ov01p.ptid = tb_data_1.ptid AND ov01p.rstat<>3 GROUP BY ov01p.ptid) AS ov01p_date,
		    (SELECT group_concat(ov01f.id) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_id,
		    (SELECT group_concat(ov01f.results) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_result,
		    (SELECT group_concat(ov01f.exdate) FROM tbdata_23 ov01f WHERE ov01f.ptid = tb_data_1.ptid AND ov01f.rstat<>3 GROUP BY ov01f.ptid) AS ov01f_date,
		    (SELECT group_concat(ov01u.id) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_id,
		    (SELECT group_concat(ov01u.results) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_result,
		    (SELECT group_concat(ov01u.exdate) FROM tbdata_24 ov01u WHERE ov01u.ptid = tb_data_1.ptid AND ov01u.rstat<>3 GROUP BY ov01u.ptid) AS ov01u_date
		");
	
	if($ovfilter_sub>0){
	    $query->innerJoin('ov_sub_list', 'ov_sub_list.person_id = tb_data_1.id');
	    $query->where('tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode AND ov_sub_list.sub_id = :sub_id', [':sitecode'=>$sitecode, ':sub_id'=>$ovfilter_sub]);
	}else{
	    $query->where('tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode', [':sitecode'=>$sitecode]);
	}
	
	$query->orderBy('add1n8code, add1n7code, add1n6code, add1n5, add1n1, name, surname');
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

	
	
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

	
	
        $query->andFilterWhere([
            'tb_data_1.id' => $this->id,
            'tb_data_1.usmobile' => $this->usmobile,
            'tb_data_1.ptid_link' => $this->ptid_link,
            'tb_data_1.ptid' => $this->ptid,
            'tb_data_1.cid_check' => $this->cid_check,
            'tb_data_1.uidadd' => $this->uidadd,
            'tb_data_1.dadd' => $this->dadd,
            'tb_data_1.uidedit' => $this->uidedit,
            'tb_data_1.dedit' => $this->dedit,
            'tb_data_1.v2' => $this->v2,
            'tb_data_1.selfenrolldate' => $this->selfenrolldate,
            'tb_data_1.vconsentdatedb' => $this->vconsentdatedb,
            'tb_data_1.sys_assigncheck' => $this->sys_assigncheck,
            'tb_data_1.dlastcheck' => $this->dlastcheck,
            'tb_data_1.confirmdate' => $this->confirmdate,
            'tb_data_1.confirm' => $this->confirm,
            'tb_data_1.venrolldatedb' => $this->venrolldatedb,
            'tb_data_1.pay' => $this->pay,
            'tb_data_1.paytime' => $this->paytime,
            'tb_data_1.recieve' => $this->recieve,
            'tb_data_1.recievedate' => $this->recievedate,
            'tb_data_1.bankdate' => $this->bankdate,
            'tb_data_1.sys_dateoficfdb' => $this->sys_dateoficfdb,
            'tb_data_1.sys_ecoficfdb' => $this->sys_ecoficfdb,
            'tb_data_1.pidcheck' => $this->pidcheck,
            'tb_data_1.agecheck' => $this->agecheck,
            'tb_data_1.user_create' => $this->user_create,
            'tb_data_1.create_date' => $this->create_date,
            'tb_data_1.user_update' => $this->user_update,
            'tb_data_1.update_date' => $this->update_date,
	    //'tb_data_1.age' => $this->age,
        ]);

	if (!empty($this->age)) {
	    $operator = $this->getOperator($this->age);
            $operand = str_replace($operator,'',$this->age);
            $query->andFilterWhere([$operator, 'age', $operand]);
//	    
//            preg_match('/(<=|>=|<|>|=)/', $this->age, $operator);
//            preg_match('/\d+/', $this->age, $age);
//            $operator = isset($operator[0]) ? $operator[0] : 'like';
//            $age = isset($views[0]) ? $views[0] : '';
//
//            $query->andFilterWhere([$operator,'age', $age]);
        }
	
        $query->andFilterWhere(['like', 'tb_data_1.sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'tb_data_1.ptcode', $this->ptcode])
            ->andFilterWhere(['like', 'tb_data_1.ptcodefull', $this->ptcodefull])
            ->andFilterWhere(['like', 'tb_data_1.reccheck', $this->reccheck])
            ->andFilterWhere(['like', 'tb_data_1.cid', $this->cid])
            ->andFilterWhere(['like', 'tb_data_1.title', $this->title])
            ->andFilterWhere(['like', 'tb_data_1.name', $this->name])
            ->andFilterWhere(['like', 'tb_data_1.surname', $this->surname])
            //->andFilterWhere(['like', 'tb_data_1.age', $this->age])
            ->andFilterWhere(['like', 'tb_data_1.v3', $this->v3])
            ->andFilterWhere(['like', 'tb_data_1.mobile', $this->mobile])
            ->andFilterWhere(['like', 'tb_data_1.homephone', $this->homephone])
            ->andFilterWhere(['like', 'tb_data_1.telcontact1', $this->telcontact1])
            ->andFilterWhere(['like', 'tb_data_1.telcontact2', $this->telcontact2])
            ->andFilterWhere(['like', 'tb_data_1.telcontact3', $this->telcontact3])
            ->andFilterWhere(['like', 'tb_data_1.add1n1', $this->add1n1])
            ->andFilterWhere(['like', 'tb_data_1.add1n2', $this->add1n2])
            ->andFilterWhere(['like', 'tb_data_1.add1n3', $this->add1n3])
            ->andFilterWhere(['like', 'tb_data_1.add1n4', $this->add1n4])
            ->andFilterWhere(['like', 'tb_data_1.add1n5', $this->add1n5])
            ->andFilterWhere(['like', 'tb_data_1.add1n6code', $this->add1n6code])
            ->andFilterWhere(['like', 'tb_data_1.add1n7code', $this->add1n7code])
            ->andFilterWhere(['like', 'tb_data_1.add1n8code', $this->add1n8code])
            ->andFilterWhere(['like', 'tb_data_1.add1n9', $this->add1n9])
            ->andFilterWhere(['like', 'tb_data_1.hospitalcurrent', $this->hospitalcurrent])
            ->andFilterWhere(['like', 'tb_data_1.hn', $this->hn])
            ->andFilterWhere(['like', 'tb_data_1.selfenroll', $this->selfenroll])
            ->andFilterWhere(['like', 'tb_data_1.vconsent', $this->vconsent])
            ->andFilterWhere(['like', 'tb_data_1.vconsentdate', $this->vconsentdate])
            ->andFilterWhere(['like', 'tb_data_1.venroll', $this->venroll])
            ->andFilterWhere(['like', 'tb_data_1.venrolldate', $this->venrolldate])
            ->andFilterWhere(['like', 'tb_data_1.sys_dateoficf', $this->sys_dateoficf])
            ->andFilterWhere(['like', 'tb_data_1.sys_ecoficf', $this->sys_ecoficf])
            ->andFilterWhere(['like', 'tb_data_1.rstat', $this->rstat])
            ->andFilterWhere(['like', 'tb_data_1.addr', $this->addr])
            ->andFilterWhere(['like', 'tb_data_1.lat', $this->lat])
            ->andFilterWhere(['like', 'tb_data_1.lng', $this->lng])
            ->andFilterWhere(['like', 'tb_data_1.geocode', $this->geocode])
            ->andFilterWhere(['like', 'tb_data_1.sitezone', $this->sitezone])
            ->andFilterWhere(['like', 'tb_data_1.siteprov', $this->siteprov])
            ->andFilterWhere(['like', 'tb_data_1.siteamp', $this->siteamp])
            ->andFilterWhere(['like', 'tb_data_1.sitetmb', $this->sitetmb])
            ->andFilterWhere(['like', 'tb_data_1.target', $this->target])
            ->andFilterWhere(['like', 'tb_data_1.xsourcex', $this->xsourcex])
            ->andFilterWhere(['like', 'tb_data_1.value1', $this->value1])
            ->andFilterWhere(['like', 'tb_data_1.value2', $this->value2])
            ->andFilterWhere(['like', 'tb_data_1.value3', $this->value3])
            ->andFilterWhere(['like', 'tb_data_1.edattype', $this->edattype])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload1', $this->icf_upload1])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload2', $this->icf_upload2])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload3', $this->icf_upload3])
            ->andFilterWhere(['like', 'tb_data_1.error', $this->error])
            ->andFilterWhere(['like', 'tb_data_1.hncode', $this->hncode])
            ->andFilterWhere(['like', 'tb_data_1.hsitecode', $this->hsitecode])
            ->andFilterWhere(['like', 'tb_data_1.hptcode', $this->hptcode])
            ->andFilterWhere(['like', 'tb_data_1.cohorttype_chartreview', $this->cohorttype_chartreview]);

        return $dataProvider;
    }
    
    public function searchEdit($params)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $query = TbData1::find()
		->select([
		    'md5(tb_data_1.cid) as register_id',
		    'tb_data_1.cid',
		    'tb_data_1.name',
		    'tb_data_1.surname',
		    '(SELECT concat(tmp_person.cid, " ", tmp_person.fname, " ", tmp_person.lname) FROM tmp_person WHERE tmp_person.cidlink = md5(tb_data_1.cid) ) AS tmp_id'
		    ]);
	
	//$query->leftJoin('tmp_person', 'tmp_person.cidlink = md5(tb_data_1.cid)');
	$query->where('tb_data_1.rstat<>3 AND tb_data_1.hsitecode = :sitecode', [':sitecode'=>$sitecode]);
	
	$query->orderBy('tb_data_1.add1n8code, tb_data_1.add1n7code, tb_data_1.add1n6code, tb_data_1.add1n5, tb_data_1.add1n1, tb_data_1.name, tb_data_1.surname');
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

	
	
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

	
	
        $query->andFilterWhere([
            'tb_data_1.id' => $this->id,
            'tb_data_1.usmobile' => $this->usmobile,
            'tb_data_1.ptid_link' => $this->ptid_link,
            'tb_data_1.ptid' => $this->ptid,
            'tb_data_1.cid_check' => $this->cid_check,
            'tb_data_1.uidadd' => $this->uidadd,
            'tb_data_1.dadd' => $this->dadd,
            'tb_data_1.uidedit' => $this->uidedit,
            'tb_data_1.dedit' => $this->dedit,
            'tb_data_1.v2' => $this->v2,
            'tb_data_1.selfenrolldate' => $this->selfenrolldate,
            'tb_data_1.vconsentdatedb' => $this->vconsentdatedb,
            'tb_data_1.sys_assigncheck' => $this->sys_assigncheck,
            'tb_data_1.dlastcheck' => $this->dlastcheck,
            'tb_data_1.confirmdate' => $this->confirmdate,
            'tb_data_1.confirm' => $this->confirm,
            'tb_data_1.venrolldatedb' => $this->venrolldatedb,
            'tb_data_1.pay' => $this->pay,
            'tb_data_1.paytime' => $this->paytime,
            'tb_data_1.recieve' => $this->recieve,
            'tb_data_1.recievedate' => $this->recievedate,
            'tb_data_1.bankdate' => $this->bankdate,
            'tb_data_1.sys_dateoficfdb' => $this->sys_dateoficfdb,
            'tb_data_1.sys_ecoficfdb' => $this->sys_ecoficfdb,
            'tb_data_1.pidcheck' => $this->pidcheck,
            'tb_data_1.agecheck' => $this->agecheck,
            'tb_data_1.user_create' => $this->user_create,
            'tb_data_1.create_date' => $this->create_date,
            'tb_data_1.user_update' => $this->user_update,
            'tb_data_1.update_date' => $this->update_date,
	    //'tb_data_1.age' => $this->age,
        ]);

	if (!empty($this->age)) {
	    $operator = $this->getOperator($this->age);
            $operand = str_replace($operator,'',$this->age);
            $query->andFilterWhere([$operator, 'age', $operand]);
//	    
//            preg_match('/(<=|>=|<|>|=)/', $this->age, $operator);
//            preg_match('/\d+/', $this->age, $age);
//            $operator = isset($operator[0]) ? $operator[0] : 'like';
//            $age = isset($views[0]) ? $views[0] : '';
//
//            $query->andFilterWhere([$operator,'age', $age]);
        }
	
        $query->andFilterWhere(['like', 'tb_data_1.sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'tb_data_1.ptcode', $this->ptcode])
            ->andFilterWhere(['like', 'tb_data_1.ptcodefull', $this->ptcodefull])
            ->andFilterWhere(['like', 'tb_data_1.reccheck', $this->reccheck])
            ->andFilterWhere(['like', 'tb_data_1.cid', $this->cid])
            ->andFilterWhere(['like', 'tb_data_1.title', $this->title])
            ->andFilterWhere(['like', 'tb_data_1.name', $this->name])
            ->andFilterWhere(['like', 'tb_data_1.surname', $this->surname])
            //->andFilterWhere(['like', 'tb_data_1.age', $this->age])
            ->andFilterWhere(['like', 'tb_data_1.v3', $this->v3])
            ->andFilterWhere(['like', 'tb_data_1.mobile', $this->mobile])
            ->andFilterWhere(['like', 'tb_data_1.homephone', $this->homephone])
            ->andFilterWhere(['like', 'tb_data_1.telcontact1', $this->telcontact1])
            ->andFilterWhere(['like', 'tb_data_1.telcontact2', $this->telcontact2])
            ->andFilterWhere(['like', 'tb_data_1.telcontact3', $this->telcontact3])
            ->andFilterWhere(['like', 'tb_data_1.add1n1', $this->add1n1])
            ->andFilterWhere(['like', 'tb_data_1.add1n2', $this->add1n2])
            ->andFilterWhere(['like', 'tb_data_1.add1n3', $this->add1n3])
            ->andFilterWhere(['like', 'tb_data_1.add1n4', $this->add1n4])
            ->andFilterWhere(['like', 'tb_data_1.add1n5', $this->add1n5])
            ->andFilterWhere(['like', 'tb_data_1.add1n6code', $this->add1n6code])
            ->andFilterWhere(['like', 'tb_data_1.add1n7code', $this->add1n7code])
            ->andFilterWhere(['like', 'tb_data_1.add1n8code', $this->add1n8code])
            ->andFilterWhere(['like', 'tb_data_1.add1n9', $this->add1n9])
            ->andFilterWhere(['like', 'tb_data_1.hospitalcurrent', $this->hospitalcurrent])
            ->andFilterWhere(['like', 'tb_data_1.hn', $this->hn])
            ->andFilterWhere(['like', 'tb_data_1.selfenroll', $this->selfenroll])
            ->andFilterWhere(['like', 'tb_data_1.vconsent', $this->vconsent])
            ->andFilterWhere(['like', 'tb_data_1.vconsentdate', $this->vconsentdate])
            ->andFilterWhere(['like', 'tb_data_1.venroll', $this->venroll])
            ->andFilterWhere(['like', 'tb_data_1.venrolldate', $this->venrolldate])
            ->andFilterWhere(['like', 'tb_data_1.sys_dateoficf', $this->sys_dateoficf])
            ->andFilterWhere(['like', 'tb_data_1.sys_ecoficf', $this->sys_ecoficf])
            ->andFilterWhere(['like', 'tb_data_1.rstat', $this->rstat])
            ->andFilterWhere(['like', 'tb_data_1.addr', $this->addr])
            ->andFilterWhere(['like', 'tb_data_1.lat', $this->lat])
            ->andFilterWhere(['like', 'tb_data_1.lng', $this->lng])
            ->andFilterWhere(['like', 'tb_data_1.geocode', $this->geocode])
            ->andFilterWhere(['like', 'tb_data_1.sitezone', $this->sitezone])
            ->andFilterWhere(['like', 'tb_data_1.siteprov', $this->siteprov])
            ->andFilterWhere(['like', 'tb_data_1.siteamp', $this->siteamp])
            ->andFilterWhere(['like', 'tb_data_1.sitetmb', $this->sitetmb])
            ->andFilterWhere(['like', 'tb_data_1.target', $this->target])
            ->andFilterWhere(['like', 'tb_data_1.xsourcex', $this->xsourcex])
            ->andFilterWhere(['like', 'tb_data_1.value1', $this->value1])
            ->andFilterWhere(['like', 'tb_data_1.value2', $this->value2])
            ->andFilterWhere(['like', 'tb_data_1.value3', $this->value3])
            ->andFilterWhere(['like', 'tb_data_1.edattype', $this->edattype])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload1', $this->icf_upload1])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload2', $this->icf_upload2])
            ->andFilterWhere(['like', 'tb_data_1.icf_upload3', $this->icf_upload3])
            ->andFilterWhere(['like', 'tb_data_1.error', $this->error])
            ->andFilterWhere(['like', 'tb_data_1.hncode', $this->hncode])
            ->andFilterWhere(['like', 'tb_data_1.hsitecode', $this->hsitecode])
            ->andFilterWhere(['like', 'tb_data_1.hptcode', $this->hptcode])
            ->andFilterWhere(['like', 'tb_data_1.cohorttype_chartreview', $this->cohorttype_chartreview]);

        return $dataProvider;
    }
    
    private function getOperator($qryString){
        switch ($qryString){
            case strpos($qryString,'>=') === 0:
                $operator = '>='; 
            break;
            case strpos($qryString,'>') === 0:
                $operator = '>';
                break;
            case strpos($qryString,'<=') === 0:
                $operator = '<=';
                break;
            case strpos($qryString,'<') === 0:
                $operator = '<';
                break;
	    case strpos($qryString,'=') === 0:
                $operator = '=';
                break;
            default:
                $operator =  'like';
                break;
        }
        return $operator;
    }
}
