<?php

namespace backend\modules\ovcca\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ovcca\models\OvPerson;

/**
 * OvPersonSearch represents the model behind the search form about `backend\modules\ovcca\models\OvPerson`.
 */
class OvPersonSearch extends OvPerson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'person_id', 'ov01_status', 'status_import'], 'integer'],
            [['moo', 'ptcode', 'ptid_key', 'ptid', 'khet', 'province', 'amphur', 'tambon', 'hospcode', 'hospname', 'house_id', 'address', 'cid', 'hn', 'pname', 'fname', 'lname', 'sex', 'nationality', 'education', 'type_area', 'religion', 'birthdate', 'village_id', 'village_code', 'village_name', 'pttype', 'pttype_begin_date', 'pttype_expire_date', 'pttype_hospmain', 'pttype_hospsub', 'marrystatus', 'death', 'death_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchOrg($params)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
        $query = OvPerson::find()->where('ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode]);
	//->leftJoin('tb_data_1', 'ov_person.cid = tb_data_1.cid AND ov_person.hospcode = tb_data_1.hsitecode')->groupBy('ov_person.id')
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ov_person.id' => $this->id,
            'ov_person.person_id' => $this->person_id,
            'ov_person.birthdate' => $this->birthdate,
            'ov_person.pttype_begin_date' => $this->pttype_begin_date,
            'ov_person.pttype_expire_date' => $this->pttype_expire_date,
            'ov_person.death_date' => $this->death_date,
	    'ov_person.status_import' => $this->status_import,
	    'ov_person.ov01_status' => $this->ov01_status,
        ]);
	
	$query->orderBy('status_import, village_code, address, fname, lname');
	
        $query->andFilterWhere(['like', 'ov_person.khet', $this->khet])
            ->andFilterWhere(['like', 'ov_person.province', $this->province])
            ->andFilterWhere(['like', 'ov_person.amphur', $this->amphur])
            ->andFilterWhere(['like', 'ov_person.tambon', $this->tambon])
            ->andFilterWhere(['like', 'ov_person.hospcode', $this->hospcode])
            ->andFilterWhere(['like', 'ov_person.hospname', $this->hospname])
            ->andFilterWhere(['like', 'ov_person.house_id', $this->house_id])
            ->andFilterWhere(['like', 'ov_person.address', $this->address])
            ->andFilterWhere(['like', 'ov_person.cid', $this->cid])
            ->andFilterWhere(['like', 'ov_person.hn', $this->hn])
            ->andFilterWhere(['like', 'ov_person.pname', $this->pname])
            ->andFilterWhere(['like', 'ov_person.fname', $this->fname])
            ->andFilterWhere(['like', 'ov_person.lname', $this->lname])
            ->andFilterWhere(['like', 'ov_person.sex', $this->sex])
            ->andFilterWhere(['like', 'ov_person.nationality', $this->nationality])
            ->andFilterWhere(['like', 'ov_person.education', $this->education])
            ->andFilterWhere(['like', 'ov_person.type_area', $this->type_area])
            ->andFilterWhere(['like', 'ov_person.religion', $this->religion])
            ->andFilterWhere(['like', 'ov_person.village_id', $this->village_id])
            ->andFilterWhere(['like', 'ov_person.village_code', $this->village_code])
            ->andFilterWhere(['like', 'ov_person.village_name', $this->village_name])
            ->andFilterWhere(['like', 'ov_person.pttype', $this->pttype])
            ->andFilterWhere(['like', 'ov_person.pttype_hospmain', $this->pttype_hospmain])
            ->andFilterWhere(['like', 'ov_person.pttype_hospsub', $this->pttype_hospsub])
            ->andFilterWhere(['like', 'ov_person.marrystatus', $this->marrystatus])
            ->andFilterWhere(['like', 'ov_person.death', $this->death])

	    ->andFilterWhere(['like', 'ov_person.moo', $this->moo])
	    ->andFilterWhere(['like', 'ov_person.ptcode', $this->ptcode])
	    ->andFilterWhere(['like', 'ov_person.ptid_key', $this->ptid_key])
	    ->andFilterWhere(['like', 'ov_person.ptid', $this->ptid]);
        return $dataProvider;
    }
    
    public function search($params, $page=null, $perPage=100, $sort='village_code, address, fname, lname')
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	\backend\modules\ovcca\classes\OvccaQuery::dpOvTemporary($sitecode, $page, $perPage, $sort);
	
        $query = OvPerson::find()->select('ov_person.*, tmpov.*')
		->where('ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])
		->leftJoin('tmpov', 'tmpov.tmp_id=ov_person.id');
	//->leftJoin('tb_data_1', 'ov_person.cid = tb_data_1.cid AND ov_person.hospcode = tb_data_1.hsitecode')->groupBy('ov_person.id')
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ov_person.id' => $this->id,
            'ov_person.person_id' => $this->person_id,
            'ov_person.birthdate' => $this->birthdate,
            'ov_person.pttype_begin_date' => $this->pttype_begin_date,
            'ov_person.pttype_expire_date' => $this->pttype_expire_date,
            'ov_person.death_date' => $this->death_date,
	    'ov_person.status_import' => $this->status_import,
	    'ov_person.ov01_status' => $this->ov01_status,
        ]);
	
	//$query->orderBy('village_code, address, fname, lname');
	
        $query->andFilterWhere(['like', 'ov_person.khet', $this->khet])
            ->andFilterWhere(['like', 'ov_person.province', $this->province])
            ->andFilterWhere(['like', 'ov_person.amphur', $this->amphur])
            ->andFilterWhere(['like', 'ov_person.tambon', $this->tambon])
            ->andFilterWhere(['like', 'ov_person.hospcode', $this->hospcode])
            ->andFilterWhere(['like', 'ov_person.hospname', $this->hospname])
            ->andFilterWhere(['like', 'ov_person.house_id', $this->house_id])
            ->andFilterWhere(['like', 'ov_person.address', $this->address])
            ->andFilterWhere(['like', 'ov_person.cid', $this->cid])
            ->andFilterWhere(['like', 'ov_person.hn', $this->hn])
            ->andFilterWhere(['like', 'ov_person.pname', $this->pname])
            ->andFilterWhere(['like', 'ov_person.fname', $this->fname])
            ->andFilterWhere(['like', 'ov_person.lname', $this->lname])
            ->andFilterWhere(['like', 'ov_person.sex', $this->sex])
            ->andFilterWhere(['like', 'ov_person.nationality', $this->nationality])
            ->andFilterWhere(['like', 'ov_person.education', $this->education])
            ->andFilterWhere(['like', 'ov_person.type_area', $this->type_area])
            ->andFilterWhere(['like', 'ov_person.religion', $this->religion])
            ->andFilterWhere(['like', 'ov_person.village_id', $this->village_id])
            ->andFilterWhere(['like', 'ov_person.village_code', $this->village_code])
            ->andFilterWhere(['like', 'ov_person.village_name', $this->village_name])
            ->andFilterWhere(['like', 'ov_person.pttype', $this->pttype])
            ->andFilterWhere(['like', 'ov_person.pttype_hospmain', $this->pttype_hospmain])
            ->andFilterWhere(['like', 'ov_person.pttype_hospsub', $this->pttype_hospsub])
            ->andFilterWhere(['like', 'ov_person.marrystatus', $this->marrystatus])
            ->andFilterWhere(['like', 'ov_person.death', $this->death])

	    ->andFilterWhere(['like', 'ov_person.moo', $this->moo])
	    ->andFilterWhere(['like', 'ov_person.ptcode', $this->ptcode])
	    ->andFilterWhere(['like', 'ov_person.ptid_key', $this->ptid_key])
	    ->andFilterWhere(['like', 'ov_person.ptid', $this->ptid]);
        return $dataProvider;
    }
    
    public function searchSub($params, $ovfilter_sub, $page=null, $perPage=100, $sort='village_code, address, fname, lname')
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	\backend\modules\ovcca\classes\OvccaQuery::dpOvTemporary($sitecode, $page, $perPage, $sort, $ovfilter_sub);
	
        $query = OvPerson::find()->select('ov_person.*, tmpov.*')
		->where('ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])
		->leftJoin('tmpov', 'tmpov.tmp_id=ov_person.id')
		->innerJoin('ov_sub_list', 'ov_sub_list.person_id = ov_person.id AND ov_sub_list.sub_id = :sub_id', [':sub_id'=>$ovfilter_sub])->groupBy('ov_person.id');
	//->leftJoin('tb_data_1', 'ov_person.cid = tb_data_1.cid AND ov_person.hospcode = tb_data_1.hsitecode')->groupBy('ov_person.id')
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ov_person.id' => $this->id,
            'ov_person.person_id' => $this->person_id,
            'ov_person.birthdate' => $this->birthdate,
            'ov_person.pttype_begin_date' => $this->pttype_begin_date,
            'ov_person.pttype_expire_date' => $this->pttype_expire_date,
            'ov_person.death_date' => $this->death_date,
	    'ov_person.status_import' => $this->status_import,
	    'ov_person.ov01_status' => $this->ov01_status,
        ]);
	
	
        $query->andFilterWhere(['like', 'ov_person.khet', $this->khet])
            ->andFilterWhere(['like', 'ov_person.province', $this->province])
            ->andFilterWhere(['like', 'ov_person.amphur', $this->amphur])
            ->andFilterWhere(['like', 'ov_person.tambon', $this->tambon])
            ->andFilterWhere(['like', 'ov_person.hospcode', $this->hospcode])
            ->andFilterWhere(['like', 'ov_person.hospname', $this->hospname])
            ->andFilterWhere(['like', 'ov_person.house_id', $this->house_id])
            ->andFilterWhere(['like', 'ov_person.address', $this->address])
            ->andFilterWhere(['like', 'ov_person.cid', $this->cid])
            ->andFilterWhere(['like', 'ov_person.hn', $this->hn])
            ->andFilterWhere(['like', 'ov_person.pname', $this->pname])
            ->andFilterWhere(['like', 'ov_person.fname', $this->fname])
            ->andFilterWhere(['like', 'ov_person.lname', $this->lname])
            ->andFilterWhere(['like', 'ov_person.sex', $this->sex])
            ->andFilterWhere(['like', 'ov_person.nationality', $this->nationality])
            ->andFilterWhere(['like', 'ov_person.education', $this->education])
            ->andFilterWhere(['like', 'ov_person.type_area', $this->type_area])
            ->andFilterWhere(['like', 'ov_person.religion', $this->religion])
            ->andFilterWhere(['like', 'ov_person.village_id', $this->village_id])
            ->andFilterWhere(['like', 'ov_person.village_code', $this->village_code])
            ->andFilterWhere(['like', 'ov_person.village_name', $this->village_name])
            ->andFilterWhere(['like', 'ov_person.pttype', $this->pttype])
            ->andFilterWhere(['like', 'ov_person.pttype_hospmain', $this->pttype_hospmain])
            ->andFilterWhere(['like', 'ov_person.pttype_hospsub', $this->pttype_hospsub])
            ->andFilterWhere(['like', 'ov_person.marrystatus', $this->marrystatus])
            ->andFilterWhere(['like', 'ov_person.death', $this->death])

	    ->andFilterWhere(['like', 'ov_person.moo', $this->moo])
	    ->andFilterWhere(['like', 'ov_person.ptcode', $this->ptcode])
	    ->andFilterWhere(['like', 'ov_person.ptid_key', $this->ptid_key])
	    ->andFilterWhere(['like', 'ov_person.ptid', $this->ptid]);
        return $dataProvider;
    }
}
