<?php

namespace backend\modules\ovcca\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ovcca\models\OvFilterSub;

/**
 * OvFilterSubSearch represents the model behind the search form about `backend\modules\ovcca\models\OvFilterSub`.
 */
class OvFilterSubSearch extends OvFilterSub
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by','sub_id', 'filter_id', 'urine_status'], 'integer'],
            [['sub_name', 'sitecode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
        $query = OvFilterSub::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sub_id' => $this->sub_id,
            'filter_id' => $this->filter_id,
	    'urine_status' => $this->urine_status,
        ]);

        $query->andFilterWhere(['like', 'sub_name', $this->sub_name]);
	$query->andFilterWhere(['like', 'sitecode', $this->sitecode]);

        return $dataProvider;
    }
}
