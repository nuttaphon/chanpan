<?php

namespace backend\modules\ovcca;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ovcca\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
