<?php

namespace backend\modules\ovcca\controllers;

use Yii;
use backend\modules\ovcca\models\TmpPerson;
use backend\modules\ovcca\models\TmpPersonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;
use backend\modules\ovcca\models\OvPerson;
use backend\modules\ovcca\classes\OvccaFunc;
use backend\modules\ovcca\classes\OvccaQuery;

/**
 * TmpPersonController implements the CRUD actions for TmpPerson model.
 */
class TmpPersonController extends Controller {

    public function behaviors() {
	return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'index2', 'view'],
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes', 'drump', 'drump2', 'village', 'village2', 'import'],
			'roles' => ['@'],
		    ],
		],
	    ],
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    'delete' => ['post'],
		],
	    ],
	];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }

    public function actionVillage() {
	$min = isset($_GET['minage']) ? $_GET['minage'] : 15;
	$max = isset($_GET['maxage']) ? $_GET['maxage'] : 100;
	$type = isset($_GET['type']) ? $_GET['type'] : [1,3];
	$type_in = $type;
	if ($type > 0) {
	    $type_in = implode(',', $type);
	}
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	
	
	unset( Yii::$app->session['tmp_Villagelist']);
	$data = \backend\modules\ovcca\classes\OvccaQuery::getVillagelist($min, $max, $type_in, $sitecode);
	$sum = OvccaQuery::getSumPerson($sitecode);
	$modelSum = \backend\modules\ovcca\models\OvSum::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode])->one();
	    if($modelSum){
		$modelSum->total = $sum;
		$modelSum->type = 1;
	    } else {
		$modelSum = new \backend\modules\ovcca\models\OvSum();
		$modelSum->sitecode = $sitecode;
		$modelSum->total = $sum;
		$modelSum->type = 1;
	    }
	    
	    $modelSum->save();
	if ($data) {
	    Yii::$app->session['tmp_Villagelist'] = $data;
	}
	
	return $this->redirect(['index', 'minage' => $min, 'maxage' => $max, 'type' => $type]);
    }

    public function actionVillage2() {
	$min = isset($_GET['minage']) ? $_GET['minage'] : 0;
	$max = isset($_GET['maxage']) ? $_GET['maxage'] : 0;
	$type = isset($_GET['type'])?$_GET['type']:0;
	unset( Yii::$app->session['tmp_Villagelist']);
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	if($type){
	    $data = \backend\modules\ovcca\classes\OvccaQuery::getVillagelist3($min, $max, $sitecode);
	} else {
	    $data = \backend\modules\ovcca\classes\OvccaQuery::getVillagelist2($min, $max, $sitecode);
	}
	
	if ($data) {
	    Yii::$app->session['tmp_Villagelist'] = $data;
	    foreach ($data as $value) {
		$sum += $value['num'];
	    }
	    $modelSum = \backend\modules\ovcca\models\OvSum::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode])->one();
	    if($modelSum){
		$modelSum->total = $sum;
		$modelSum->type = 2;
	    } else {
		$modelSum = new \backend\modules\ovcca\models\OvSum();
		$modelSum->sitecode = $sitecode;
		$modelSum->total = $sum;
		$modelSum->type = 2;
	    }
	    $modelSum->save();
	}
	
	return $this->redirect(['index2', 'minage' => $min, 'maxage' => $max, 'type' => $type]);
    }
    
    public function actionDrump() {
	$min = isset($_GET['minage']) ? $_GET['minage'] : 0;
	$max = isset($_GET['maxage']) ? $_GET['maxage'] : 0;
	$type = isset($_GET['type']) ? $_GET['type'] : 0;
	$village = isset($_GET['village']) ? $_GET['village'] : 0;

	$type_in = $type;
	if ($type > 0) {
	    $type_in = implode(',', $type);
	}
	
	if (isset($_COOKIE['save_key']) && $_COOKIE['save_key']==1) {
	    if (isset($_COOKIE['key_db'])) {
		Yii::$app->session['key_db'] = $_COOKIE['key_db'];
	    }
	    if (isset($_COOKIE['convert'])) {
		Yii::$app->session['convert'] = $_COOKIE['convert'];
	    }
	}
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	if(isset(Yii::$app->session['key_db']) && Yii::$app->session['key_db']!=''){
	    $data = \backend\modules\ovcca\classes\OvccaQuery::getPersonAllDecode($min, $max, $type_in, $village, Yii::$app->session['key_db'], $sitecode);
	} else {
	    $data = \backend\modules\ovcca\classes\OvccaQuery::getPersonAll($min, $max, $type_in, $village, $sitecode);
	}
	
	if ($data) {
	    //Yii::$app->db->createCommand('DELETE FROM `tmp_person` WHERE hospcode=:sitecode', [':sitecode'=>$sitecode])->execute();

	    foreach ($data as $key => $value) {
		
		$model = TmpPerson::find()->where('hospcode=:hospcode AND person_id=:person_id', [':hospcode'=>$value['hospcode'], ':person_id'=>$value['person_id']])->one();
		if($model){
		    $model->attributes = $value;
		    if(!isset($model->khet)){
			$model->khet = 0;
		    }
		    $model->moo = substr($value['village_code'], -2);
		} else {
		    $model = new TmpPerson();
		    $model->attributes = $value;
		    if(!isset($model->khet)){
			$model->khet = 0;
		    }
		    $model->moo = substr($value['village_code'], -2);
		}
//		$model->hospname = iconv('tis-620', 'utf-8', $model->hospname);
//		$model->village_name = iconv('tis-620', 'utf-8', $model->village_name);
//		$model->fname = iconv('tis-620', 'utf-8', $model->fname);
//		$model->lname = iconv('tis-620', 'utf-8', $model->lname);
//		$model->pname = iconv('tis-620', 'utf-8', $model->pname);
		//\appxq\sdii\utils\VarDumper::dump($model->validate());
		$model->save();
	    }
	}
	
	return $this->redirect(['index', 'minage' => $min, 'maxage' => $max, 'type' => $type, 'village' => $village]);
    }
    
    public function actionDrump2() {
	$min = isset($_GET['minage']) ? $_GET['minage'] : 0;
	$max = isset($_GET['maxage']) ? $_GET['maxage'] : 0;
	$village = isset($_GET['village']) ? $_GET['village'] : 0;
	$type = isset($_GET['type'])?$_GET['type']:0;
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	if($type){
	    $data = \backend\modules\ovcca\classes\OvccaQuery::getPersonAll3($min, $max, $village, $sitecode);
	} else {
	    $data = \backend\modules\ovcca\classes\OvccaQuery::getPersonAll2($min, $max, $village, $sitecode);
	}
	
	
	if ($data) {
	    
	    //Yii::$app->db->createCommand('DELETE FROM `tmp_person` WHERE hospcode=:sitecode', [':sitecode'=>$sitecode])->execute();
	    
	    foreach ($data as $key => $value) {
		
		    
		$hdata = Yii::$app->db->createCommand('SELECT * FROM all_hospital_thai WHERE hcode = :hcode', [':hcode'=>$value['hsitecode']])->queryOne();
		
		$model = TmpPerson::find()->where('hospcode=:hospcode AND cidlink=:cidlink', [':hospcode'=>$value['hsitecode'], ':cidlink'=>$value['cidlink']])->one();
		if($model){
		    
		} else {
		    $model = new TmpPerson();
		}
		
		if($hdata){
		    $model->khet = $hdata['zone_code'];
		    $model->province = $hdata['provincecode'];
		    $model->amphur = $hdata['provincecode'].$hdata['amphurcode'];
		    $model->tambon = $hdata['provincecode'].$hdata['amphurcode'].$hdata['tamboncode'];
		    $model->hospname = $hdata['name'];
		}
		
		$moo = strlen($value['add1n5'])>=2?$value['add1n5']:'0'.$value['add1n5'];
		
		$model->hospcode = $value['hsitecode'];
		$model->person_id = $value['ptcode'];
		$model->house_id = '0';
		$model->address = $value['add1n1'];
		$model->cid = $value['cid'];
		$model->hn = $value['hn'];
		$model->pname = $value['title'];
		$model->fname = $value['name'];
		$model->lname = $value['surname'];
		$model->sex = $value['v3'];
		$model->type_area = '1';
		$model->birthdate = $value['v2'];
		$model->village_code = $value['add1n6code'].$moo;
		$model->village_name = $value['add1n2'];
		$model->death = 'N';
		$model->ptcode = $value['ptcode'];
		$model->ptid_key = $value['ptid'];
		$model->ptid = $value['id'];
		$model->moo = $value['add1n5'];
		
		$model->save();
	    }
	}
	return $this->redirect(['index2', 'minage' => $min, 'type' => $type, 'maxage' => $max, 'village' => $village]);
    }

    /**
     * Lists all TmpPerson models.
     * @return mixed
     */
    public function actionIndex() {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$min = isset($_GET['minage']) ? $_GET['minage'] : 15;
	$max = isset($_GET['maxage']) ? $_GET['maxage'] : 100;
	$type = isset($_GET['type']) ? $_GET['type'] : [1,3];
	$village = isset($_GET['village']) ? $_GET['village'] : 0;
	
	if(isset(Yii::$app->session['key_db'])){
	    $sum = OvccaQuery::getSumPerson($sitecode);
	    $keysum = OvccaQuery::checkKey($sitecode, Yii::$app->session['key_db']);
	    $sum = $sum/2;
	    //\appxq\sdii\utils\VarDumper::dump($keysum>=$sum);
	    if($keysum>=$sum){
		Yii::$app->session->setFlash('alert', [
		    'options' => ['class' => 'alert-danger'],
		    'body' => '<i class="glyphicon glyphicon-remove"></i> กุญแจถอดรหัสที่ท่านใส่อาจจะไม่ถูกต้อง'	
		]);
	    } else {
		if(Yii::$app->session['convert']==1){
		    $tis620sum = OvccaQuery::checkTis620($sitecode, Yii::$app->session['key_db']);
		    if($tis620sum==0){
			$tis620sum = OvccaQuery::checkTis620Not($sitecode, Yii::$app->session['key_db']);
			if($tis620sum>0){
			    Yii::$app->session['convert'] = 0;
			    setcookie("convert", 0, time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
			    return $this->redirect(['village', 'minage' => $min, 'maxage' => $max, 'type' => $type]);
			} else {
			    Yii::$app->session->setFlash('alert', [
				'options' => ['class' => 'alert-danger'],
				'body' => '<i class="glyphicon glyphicon-sign"></i> กรุณาตรวจสอบการเข้ารหัสแบบ tis620'	
			    ]);
			}
		    } 
		} else {
		    $tis620sum = OvccaQuery::checkTis620Not($sitecode, Yii::$app->session['key_db']);
		    if($tis620sum==0){
			$tis620sum = OvccaQuery::checkTis620($sitecode, Yii::$app->session['key_db']);
			if($tis620sum>0){
			    Yii::$app->session['convert'] = 1;
			    setcookie("convert", 1, time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
			    return $this->redirect(['village', 'minage' => $min, 'maxage' => $max, 'type' => $type]);
			} else {
			    Yii::$app->session->setFlash('alert', [
				'options' => ['class' => 'alert-danger'],
				'body' => '<i class="glyphicon glyphicon-sign"></i> กรุณาตรวจสอบการเข้ารหัสแบบ tis620'	
			    ]);
			}
		    } 
		}
		
		
		$tis620sum = OvccaQuery::checkTis620Not($sitecode, Yii::$app->session['key_db']);
		if($tis620sum>0){
		    Yii::$app->session['convert'] = 0;
		    
		} else {
		    $tis620sum = OvccaQuery::checkTis620($sitecode, Yii::$app->session['key_db']);
		    if($tis620sum>0){
			Yii::$app->session['convert'] = 1;
			
		    } else {
			Yii::$app->session->setFlash('alert', [
			    'options' => ['class' => 'alert-danger'],
			    'body' => '<i class="glyphicon glyphicon-sign"></i> กรุณาตรวจสอบการเข้ารหัสแบบ tis620'	
			]);
		    }
		}
	    }
	} else {
	    Yii::$app->session->setFlash('alert', [
		'options' => ['class' => 'alert-danger'],
		'body' => '<i class="glyphicon glyphicon-lock"></i> ท่านยังไม่ได้ใส่กุญแจถอดรหัส'	
	    ]);
	}
	
	$searchModel = new TmpPersonSearch();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $village);
	$dataProvider->pagination->pageSize = 9999999;
	$totalTb = OvPerson::find()->where('status_import=0 AND hospcode = :sitecode', [':sitecode'=>$sitecode])->count();
	$ovCount = OvPerson::find()->where('ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])->count();
	$ovJoinCount = OvPerson::find()->where('status_import=1 AND  ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])->count();
	
	if (isset($_COOKIE['save_key']) && $_COOKIE['save_key']==1) {
	    if (isset($_COOKIE['key_db'])) {
		Yii::$app->session['key_db'] = $_COOKIE['key_db'];
	    }
	    if (isset($_COOKIE['convert'])) {
		Yii::$app->session['convert'] = $_COOKIE['convert'];
	    }
	}
	$total = 0;
	$total_label = 'จำนวนทั้งหมดในคลังจาก TDC';
	$modelSum = \backend\modules\ovcca\models\OvSum::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode])->one();
	if($modelSum){
	    if($modelSum->type==1){
		$total = $modelSum->total;
	    }
	}
	return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'ovCount' => $ovCount,
		    'total' => $total,
	    'total_label' => $total_label,  
	    'totalTb'=>$totalTb,
	    'ovJoinCount'=>$ovJoinCount,
	]);
    }

    public function actionEdit() {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$key = $_POST['findbot']['key'];
	$convert = isset($_POST['findbot']['convert'])?$_POST['findbot']['convert']:0;
	
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	
	if(isset($_POST['findbot']['key'])){
	    Yii::$app->session['tmp_key'] = $key;
	    Yii::$app->session['tmp_convert'] = $convert;
	    Yii::$app->session['tmp_show'] = true;
	    $find = \backend\modules\ovcca\classes\OvccaQuery::getRegisterId($sitecode);
	    if($find){
		$colArray = \yii\helpers\ArrayHelper::getColumn($find, 'cid');
		$colString = implode(',', $colArray);
		
		$data = \backend\modules\ovcca\classes\OvccaQuery::getPersonAllDecodeIn($key, $convert, $colString, $sitecode);
		if($data){
		    //Yii::$app->db->createCommand('DELETE FROM `tmp_person` WHERE hospcode=:sitecode', [':sitecode'=>$sitecode])->execute();
		    
		    foreach ($data as $value) {
			$model = TmpPerson::find()->where('hospcode=:hospcode AND cidlink=:cidlink', [':hospcode'=>$value['hospcode'], ':cidlink'=>$value['cidlink']])->one();
			
			if($model){
			    $model->attributes = $value;
			    $model->moo = substr($value['village_code'], -2);
			} else {
			    $model = new TmpPerson();
			    $model->attributes = $value;
			    $model->moo = substr($value['village_code'], -2);
			}
			
			$model->save();
		    }
		}
		
		//\appxq\sdii\utils\VarDumper::dump($data_bot);
	    }
	    
	}
	
	$searchModel = new \backend\modules\ovcca\models\TbData1Search();
        $dataProvider = $searchModel->searchEdit(Yii::$app->request->queryParams);
	$dataProvider->pagination->pageSize = 100;
	
	return $this->render('edit', [
	    'key' => $key,
	    'convert' => $convert,
	    'searchModel' => $searchModel,
	    'dataProvider' => $dataProvider,
	]);
    }
    
    public function actionIndex2() {
	$sitecode = Yii::$app->user->identity->userProfile->sitecode;
	$min = isset($_GET['minage']) ? $_GET['minage'] : 0;
	$max = isset($_GET['maxage']) ? $_GET['maxage'] : 0;
	$type = isset($_GET['type']) ? $_GET['type'] : 0;
	$village = isset($_GET['village']) ? $_GET['village'] : 0;
	
	$searchModel = new TmpPersonSearch();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $village);
	$dataProvider->pagination->pageSize = 9999999;
	$ovCount = \backend\modules\ovcca\models\OvPerson::find()->where('ov_person.hospcode = :sitecode', [':sitecode'=>$sitecode])->count();

	$total = 0;
	$total_label = 'จำนวนทั้งหมดในคลังจากใบทำบัตร';
	$modelSum = \backend\modules\ovcca\models\OvSum::find()->where('sitecode=:sitecode', [':sitecode'=>$sitecode])->one();
	if($modelSum){
	    if($modelSum->type==2){
		$total = $modelSum->total;
	    }
	}
	
	return $this->render('index2', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'ovCount' => $ovCount,
		    'total' => $total,
		    'total_label' => $total_label,
	]);
    }
    /**
     * Displays a single TmpPerson model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
			'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
			'model' => $this->findModel($id),
	    ]);
	}
    }

    /**
     * Creates a new TmpPerson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new TmpPerson();

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
			    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Updates an existing TmpPerson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);

	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
			    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Deletes an existing TmpPerson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if ($this->findModel($id)->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionDeletes() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if (isset($_POST['selection'])) {
		foreach ($_POST['selection'] as $id) {
		    $this->findModel($id)->delete();
		}
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $_POST['selection'],
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionImportUpdateAll() {
	if (Yii::$app->getRequest()->isAjax) {
	    ini_set('max_execution_time', 0);
	    set_time_limit(0);
	    ini_set('memory_limit','512M');
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    try {
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		
		$find = \backend\modules\ovcca\classes\OvccaQuery::getRegisterCid($sitecode);
		if ($find) {
		    $colArray = \yii\helpers\ArrayHelper::getColumn($find, 'cid');
		    
		    $key = Yii::$app->session['tmp_key'];
		    $convert = Yii::$app->session['tmp_convert'];
		    $selectItem = 0;
		    $allItem = 0;
		    foreach ($colArray as $cid) {
			
			    $allItem ++;
			    
			    $tccbot = OvccaQuery::getPersonOneByCid($sitecode, $cid, $key, $convert);
			    //ดึงข้อมูลจาก TDC
			    //เช็คความถูกต้องของข้อมูล
			    //นำเข้าข้อมูล
			    
			    if($tccbot){
				if(OvccaFunc::check_citizen($tccbot['cid'])){
				    $checkthaiword = trim(OvccaFunc::checkthai($tccbot['fname']));
				    if ($checkthaiword  != '') {
					
					$r = OvccaFunc::findImportPersonOneGetId($sitecode, $cid, $tccbot);
					
					if($r){
					    $selectItem++;
					}
				    }
				} 
			    } 
			    
		    }
		    
		    $result = [
			'status' => 'success',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'ปรับปรุงข้อมูลแล้ว '."$selectItem / $allItem".' ราย'),
			'html' => $selectItem,
		    ];
		    
		    return $result;
		    
		} else {

		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not import the data.'),
		    ];
		    return $result;
		}
	    } catch (\yii\db\Exception $e) {

		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . '<strong>Error: ' . $e->getCode() . ' </strong> ' . $e->getMessage(),
		    'data' => $id,
		];
		return $result;
	    }
	    
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionImportUpdate() {
	if (Yii::$app->getRequest()->isAjax) {
	    ini_set('max_execution_time', 0);
	    set_time_limit(0);
	    ini_set('memory_limit','512M');
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    try {
		if (isset($_POST['selection'])) {
		    
		    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
		    $key = Yii::$app->session['tmp_key'];
		    $convert = Yii::$app->session['tmp_convert'];
		    $selectItem = 0;
		    $allItem = 0;
		    foreach ($_POST['selection'] as $cid) {
			
			    $allItem ++;
			    
			    $tccbot = OvccaQuery::getPersonOneByCid($sitecode, $cid, $key, $convert);
			    //ดึงข้อมูลจาก TDC
			    //เช็คความถูกต้องของข้อมูล
			    //นำเข้าข้อมูล
			    
			    if($tccbot){
				if(OvccaFunc::check_citizen($tccbot['cid'])){
				    $checkthaiword = trim(OvccaFunc::checkthai($tccbot['fname']));
				    if ($checkthaiword  != '') {
					
					$r = OvccaFunc::findImportPersonOneGetId($sitecode, $cid, $tccbot);
					
					if($r){
					    $selectItem++;
					}
				    }
				} 
			    } 
			    
		    }
		    
		    $result = [
			'status' => 'success',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'ปรับปรุงข้อมูลแล้ว '."$selectItem / $allItem".' ราย'),
			'html' => $selectItem,
		    ];
		    
		    return $result;
		    
		} else {

		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not import the data.'),
		    ];
		    return $result;
		}
	    } catch (\yii\db\Exception $e) {

		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . '<strong>Error: ' . $e->getCode() . ' </strong> ' . $e->getMessage(),
		    'data' => $id,
		];
		return $result;
	    }
	    
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionImport() {
	if (Yii::$app->getRequest()->isAjax) {
	    ini_set('max_execution_time', 0);
	    set_time_limit(0);
	    ini_set('memory_limit','256M');
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    try {
		if (isset($_POST['selection'])) {
		    $importNum = 0;
		    $ovNum = 0;
		    $allNum = 0;
		    //check convert
		    $cidNum = 0;
		    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
//		    $row = \backend\modules\ovcca\classes\OvccaQuery::checkConvert($sitecode);
//		    if($row==0){
//			$html = $this->renderAjax('_error', [
//			    'msg'=>'Convert ข้อมูลไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง'
//			]);
//			
//			$result = [
//			    'status' => 'error',
//			    'message' => SDHtml::getMsgError() . Yii::t('app', 'Convert ข้อมูลไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง'),
//			];
//			return $result;
//		    }
		    $cek = 1;
		    foreach ($_POST['selection'] as $id) {
			$model = TmpPerson::find()->select(["tmp_person.*", "(SELECT p.ptid FROM tb_data_1 p WHERE p.rstat<>3 AND p.cid = tmp_person.cid AND p.hsitecode = tmp_person.hospcode GROUP BY p.cid, p.hsitecode) AS register_id"])->where('id=:id', [':id'=>$id])->one();
			$modelOv = new \backend\modules\ovcca\models\OvPerson();
			$modelOv->attributes = $model->attributes;
			$allNum++;
			
			if($cek){
			    if(\backend\modules\ovcca\classes\OvccaFunc::check_citizen($modelOv['cid'])){
				$checkthaiword = trim(\backend\modules\ovcca\classes\OvccaFunc::checkthai($modelOv['fname']));
				if ($checkthaiword  == '') {
				    $result = [
					'status' => 'error',
					'message' => SDHtml::getMsgError() . Yii::t('app', "กรณีชื่อ-สกุล อ่านไม่ออกให้เข้ารหัสแบบ tis620 "),
				    ];
				    return $result;
				}
			    } else {
				$result = [
				    'status' => 'error',
				    'message' => SDHtml::getMsgError() . Yii::t('app', "Convert ข้อมูลไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง "),
				];
				return $result;
			    }
			    
			    $cek=0;
			}
			
			//check cid
			$checkCid = \backend\modules\ovcca\classes\OvccaFunc::check_citizen($modelOv->cid);
			if(!$checkCid){
			    $cidNum++;
			    continue;
			}
			
			$checkthaiword = trim(\backend\modules\ovcca\classes\OvccaFunc::checkthai($modelOv->fname));
			if ($checkthaiword  == '') {
			    continue;
			}
			
			if(isset($model->register_id) && $model->register_id!=''){
			    $modelOv->status_import=1;
			}else{
			    $modelOv->status_import=0;
			}
			
			if($modelOv->save()){
			    $importNum++;
			}
		    }
		    
		    $ovNum = \backend\modules\ovcca\models\OvPerson::find()->where('hospcode = :hospcode', [':hospcode' => $sitecode])->count();
		    
		    
		    $html = $this->renderAjax('_import', [
			'importNum' => $importNum,
			'ovNum' => $ovNum,
			'cidNum' => $cidNum,
			'allNum'=>$allNum
		    ]);
		    
		    $result = [
			'status' => 'success',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'นำเข้าแล้ว '.$importNum.' ราย'),
			'html'=>$html,
		    ];
		    return $result;
		    
		} else {

		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not import the data.'),
		    ];
		    return $result;
		}
	    } catch (\yii\db\Exception $e) {

		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . '<strong>Error: ' . $e->getCode() . ' </strong> ' . $e->getMessage(),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionAddkey() {
	if (Yii::$app->getRequest()->isAjax) {
	    if (isset($_COOKIE['save_key']) && $_COOKIE['save_key']==1) {
		if (isset($_COOKIE['key_db'])) {
		    Yii::$app->session['key_db'] = $_COOKIE['key_db'];
		}
		if (isset($_COOKIE['convert'])) {
		    Yii::$app->session['convert'] = $_COOKIE['convert'];
		}
	    }
	    
	    if (isset($_POST['add_key'])) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		if (isset($_POST['save_key']) && $_POST['save_key']==1) {
		    
		    setcookie("save_key", $_POST['save_key'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		    setcookie("key_db", $_POST['add_key'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		    setcookie("convert", $_POST['convert'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		} else {
//		    
//		    unset($_COOKIE['save_key']);
//		    unset($_COOKIE['key_db']);
//		    unset($_COOKIE['convert']);
		    setcookie('save_key', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		    setcookie('key_db', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		    setcookie('convert', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		}
		
		Yii::$app->session['key_db'] = $_POST['add_key'];
		//Yii::$app->session['save_key'] = $_POST['save_key'];
		Yii::$app->session['convert'] = $_POST['convert'];

		$result = [
		    'status' => 'success',
		    'action' => 'create',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		];
		return $result;
	    } else {
		return $this->renderAjax('_form_key', [
			    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionRemovekey() {
	if (Yii::$app->getRequest()->isAjax) {

	    setcookie('save_key', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    setcookie('key_db', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    setcookie('convert', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);

	    Yii::$app->session['key_db'] = $_POST['add_key'];
	    //Yii::$app->session['save_key'] = $_POST['save_key'];
	    Yii::$app->session['convert'] = $_POST['convert'];

	    return $this->redirect(['/ovcca/tmp-person/village', 'minage'=>$_GET['minage'], 'maxage'=>$_GET['maxage'], 'type'=>$_GET['type'], 'village'=>$_GET['village']]);
	    
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionDelall() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;

	    try {
		Yii::$app->db->createCommand('TRUNCATE TABLE `tmp_person`')->execute();

		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		];
		return $result;
	    } catch (\yii\db\Exception $e) {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . '<strong>Error: ' . $e->getCode() . ' </strong> ' . $e->getMessage(),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Finds the TmpPerson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TmpPerson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
	if (($model = TmpPerson::findOne($id)) !== null) {
	    return $model;
	} else {
	    throw new NotFoundHttpException('The requested page does not exist.');
	}
    }

}
