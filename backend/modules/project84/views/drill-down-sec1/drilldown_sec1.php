<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\project84\controllers\DrillDownSec1Controller;
use backend\modules\project84\controllers\ReportController;
?>
<div class="table-responsive">
<ul class="nav nav-tabs" id="myTab">
<li class="active"><a href="#report1" data-toggle="tab">รายงาน</a></li>
<li><a id="drilldownperson" report_id="<?= $report_id  ?>"  tamboncode="<?= $tamboncode ?>"  div="#report2" sec="1" href="#report2" data-toggle="tab">รายงานตามพื้นที่อยู่อาศัย</a></li>
<li><a id="drilldownperson" report_id="<?= $report_id  ?>"   div="#report3" sec="1" href="#report3" tabmenu="yes" data-toggle="tab">รายงานรายบุคคลของสถานบริการ</a></li>
</ul>
<div class="tab-content">
  <div id="report1" class="tab-pane fade in active">

<h4>ตำบล <?= $dataArrayDrillSec1[0][tambon] ?> อำเภอ <?= $dataArrayDrillSec1[0][amphur] ?>  จังหวัด <?= $dataArrayDrillSec1[0][province] ?> </h4>
<div style="overflow-x:auto;">
<table>
<thead>
<tr>
<th width="10%">ประเภทการตรวจ</th>
<th width="10%">ผู้รับบริการ (ราย)</th>
<th >OV (ราย)</th>
<th >ร้อยละ</th>
<th >mif (ราย)</th>
<th >ร้อยละ</th>
<th >ss (ราย)</th>
<th >ร้อยละ</th>
<th >ech (ราย)</th>
<th >ร้อยละ</th>
<th >taenia (ราย)</th>
<th >ร้อยละ</th>
<th >tt (ราย)</th>
<th >ร้อยละ</th>
<th >other (ราย)</th>
<th >ร้อยละ</th>
</tr>
</thead>
<tbody>
<tr>
<td>Kato-Katz</td>
<td style="text-align:right;"><?= $dataArrayDrillSec1[0][kpos] ?></td>
<td style="text-align:right;"><?= $datakpos[0][ov] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][ov],$dataArrayDrillSec1[0][kpos],"yes") ?></td>
<td style="text-align:right;"><?= $datakpos[0][mif] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][mif],$dataArrayDrillSec1[0][kpos],"yes") ?></td>
<td style="text-align:right;"><?= $datakpos[0][ss] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][ss],$dataArrayDrillSec1[0][kpos],"yes") ?></td>
<td style="text-align:right;"><?= $datakpos[0][ech] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][ech],$dataArrayDrillSec1[0][kpos],"yes") ?></td>
<td style="text-align:right;"><?= $datakpos[0][taenia] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][taenia],$dataArrayDrillSec1[0][kpos],"yes") ?></td>
<td style="text-align:right;"><?= $datakpos[0][tt] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][tt],$dataArrayDrillSec1[0][kpos],"yes") ?></td>
<td style="text-align:right;"><?= $datakpos[0][other] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datakpos[0][other],$dataArrayDrillSec1[0][kpos],"yes") ?></td>
</tr>
<tr>
<td>Parasep</td>
<td style="text-align:right;"><?= $dataArrayDrillSec1[0][ppos] ?></td>
<td style="text-align:right;"><?= $datappos[0][ov] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datappos[0][ov],$dataArrayDrillSec1[0][ppos],"yes") ?></td>
<td style="text-align:right;"><?= $datappos[0][mif] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datappos[0][mif],$dataArrayDrillSec1[0][ppos],"yes") ?></td>
<td style="text-align:right;"><?= $datappos[0][ss] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datappos[0][ss],$dataArrayDrillSec1[0][ppos],"yes") ?></td>
<td style="text-align:right;"><?= $datappos[0][ech] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datappos[0][ech],$dataArrayDrillSec1[0][ppos],"yes") ?></td>
<td style="text-align:right;"><?= $datappos[0][taenia] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datappos[0][taenia],$dataArrayDrillSec1[0][ppos],"yes") ?></td>
<td style="text-align:right;"><?= $datappos[0][tt] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datappos[0][tt],$dataArrayDrillSec1[0][ppos],"yes") ?></td>
<td style="text-align:right;"><?= $datappos[0][other] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datappos[0][other],$dataArrayDrillSec1[0][ppos],"yes") ?></td>
</tr>
<tr>
<td>FECT</td>
<td style="text-align:right;"><?= $dataArrayDrillSec1[0][fpos] ?></td>
<td style="text-align:right;"><?= $datafpos[0][ov] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][ov],$dataArrayDrillSec1[0][fpos],"yes") ?></td>
<td style="text-align:right;"><?= $datafpos[0][mif] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][mif],$dataArrayDrillSec1[0][fpos],"yes") ?></td>
<td style="text-align:right;"><?= $datafpos[0][ss] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][ss],$dataArrayDrillSec1[0][fpos],"yes") ?></td>
<td style="text-align:right;"><?= $datafpos[0][ech] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][ech],$dataArrayDrillSec1[0][fpos],"yes") ?></td>
<td style="text-align:right;"><?= $datafpos[0][taenia] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][taenia],$dataArrayDrillSec1[0][fpos],"yes") ?></td>
<td style="text-align:right;"><?= $datafpos[0][tt] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][tt],$dataArrayDrillSec1[0][fpos],"yes") ?></td>
<td style="text-align:right;"><?= $datafpos[0][other] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($datafpos[0][other],$dataArrayDrillSec1[0][fpos],"yes") ?></td>
</tr>
<tr>
<td>Urine</td>
<td style="text-align:right;"><?= $dataArrayDrillSec1[0][upos] ?></td>
<td style="text-align:right;"><?= $dataupos[0][ov] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][ov],$dataArrayDrillSec1[0][upos],"yes") ?></td>
<td style="text-align:right;"><?= $dataupos[0][mif] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][mif],$dataArrayDrillSec1[0][upos],"yes") ?></td>
<td style="text-align:right;"><?= $dataupos[0][ss] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][ss],$dataArrayDrillSec1[0][upos],"yes") ?></td>
<td style="text-align:right;"><?= $dataupos[0][ech] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][ech],$dataArrayDrillSec1[0][upos],"yes") ?></td>
<td style="text-align:right;"><?= $dataupos[0][taenia] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][taenia],$dataArrayDrillSec1[0][upos],"yes") ?></td>
<td style="text-align:right;"><?= $dataupos[0][tt] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][tt],$dataArrayDrillSec1[0][upos],"yes") ?></td>
<td style="text-align:right;"><?= $dataupos[0][other] ?></td>
<td style="text-align:right;"><?= ReportController::calpercent($dataupos[0][other],$dataArrayDrillSec1[0][upos],"yes") ?></td>
</tr>
</tbody>
</table>
</div>
</div>
</br>
</br>
<div id="report2" class="tab-pane fade">
</div>
<div id="report3" class="tab-pane fade">
</div>
</div>
</div>
</div>
