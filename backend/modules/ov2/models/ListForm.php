<?php
namespace backend\modules\ov2\models;
/**
 * FindForm class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 2 ธ.ค. 2558 12:02:34
 * @link http://www.appxq.com/
 * @example 
 */
use Yii;
use yii\base\Model;

class ListForm extends Model {
    
    public $list_person;
    
    public function rules()
    {
	return [
	    [['list_person'], 'required'],
	    [['list_person'], 'safe'],
        ];
    }
    
    public function attributeLabels() {
	return [
	    'list_person' => CoreFunc::t('ใบกำกับงาน'),
	];
    }
    
    public function save()
    {
	try {
	   
	} catch (\yii\db\Exception $e) {
	   return false;
	}
    }
    
}
