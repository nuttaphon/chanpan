<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\OvFilterSub */

$this->title = 'ใบกำกับงาน#'.$model->sub_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ov Filter Subs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ov-filter-sub-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		//'sub_id',
		'sub_name',
		'urine_status',
		//'filter_id',
		'sitecode',
	    ],
	]) ?>
    </div>
</div>
