<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ovcca\models\OvFilterSub */

$this->title = 'เลือกประชากรเข้าใบกำกับงาน';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ใบกำกับงาน'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ov-filter-sub-view">
    <div class="ov-person-back" style="margin-bottom: 15px; text-align: right;">
	<?php echo Html::a('<i class="fa fa-reply"></i> กลับไปหน้าแสดงใบกำกับงาน', ['/ov2/ov-person/index'], ['class' => 'btn btn-default']) ?>
    </div>
   <?php $form = ActiveForm::begin([
		'id' => 'assign-form',
		'action' => ['assign'],
		'method' => 'post',
		'layout' => 'horizontal'
	    ]);    
	    ?>
    <div class="row">
	<div class="col-md-5">
	    <?= Html::label('ค้นหา')?>
	    <?= Html::textInput('lserch', '', ['class'=>'form-control'])?>
	    <div style="margin-top: 5px;">
		<?= Html::label('รายชื่อประชากร')?>
		<?= Html::dropDownList('lserch', null, [], ['class'=>'form-control'])?>
	    </div>
	    <div style="margin-top: 5px; border: 1px solid #ddd; padding: 10px;">
		<?= Html::checkboxList('llist', null, [1,2,3,4,5], ['item'=>'\common\lib\sdii\components\utils\SDUtility::checkboxInput'])?>
	    </div>
	</div>
	<div class="col-md-2" style="text-align: center; vertical-align: middle;">df</div>
	<div class="col-md-5">
	    <?= Html::label('ค้นหา')?>
	    <?= Html::textInput('rserch', '', ['class'=>'form-control'])?>
	    <div style="margin-top: 5px;">
		<?= Html::label('ใบกำกับงาน')?>
		<?= Html::dropDownList('lserch', null, [], ['class'=>'form-control'])?>
	    </div>
	    <div style="margin-top: 5px; border: 1px solid #ddd; padding: 10px;">
		<?= Html::checkboxList('rlist', null, [1,2,3,4,5], ['item'=>'\common\lib\sdii\components\utils\SDUtility::checkboxInput'])?>
	    </div>
	    
	</div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
