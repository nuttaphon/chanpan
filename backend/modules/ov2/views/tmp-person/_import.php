<?php 
use yii\helpers\Html;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">นำเข้าจาก TCC Bot เพื่อจัดการลงทะเบียนใน Isan Cohort</h4>
</div>

<div class="modal-body">
    <table class="table">
	<tr>
	    <td><h2>เลือกเพื่อนำเข้าในครั้งนี้</h2></td>
	    <td><h2><?=$allNum?> คน</h2></td>
	</tr>
	<tr>
	    <td><h2>นำเข้าได้</h2></td>
	    <td><h2><?=$importNum?> คน</h2></td>
	</tr>
	<tr>
	    <td><h2>นำเข้าไม่ได้</h2></td>
	    <td><h2><?=$allNum-$importNum?> คน</h2></td>
	</tr>
    </table>
    <?php if($sumF>0): ?>
    <code>*หมายเหตุ : นำเข้าไม่ได้ อาจเนื่องมาจาก เลขบัตรไม่ถูกต้อง, ไม่ได้ถอดรหัสข้อมูล, ข้อมูลไม่ครบถ้วน</code>
    <?php endif;?>
</div>
<div class="modal-footer">
    <?php echo Html::a('<i class="fa fa-reply"></i> กลับไปหน้าลงทะเบียนใน Isan Cohort', ['/ov2/ov-person/indexreg'], ['class' => 'btn btn-primary']) ?>
    <button type="button" class="btn btn-default" data-dismiss="modal">เลือกหมู่บ้านอื่น</button>
</div>

<?php  $this->registerJs("
    
$.pjax.reload({container:'#tmp-person-rs-pjax'});

");?>