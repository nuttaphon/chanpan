<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ov2\models\TmpPersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'นำเข้าจากใบทำบัตร');
$this->params['breadcrumbs'][] = $this->title;

$min = isset($_GET['minage'])?$_GET['minage']:15;
$max = isset($_GET['maxage'])?$_GET['maxage']:100;
$village = isset($_GET['village'])?$_GET['village']:0;
$type = isset($_GET['type'])?$_GET['type']:1;
?>
<div class="tmp-person-index">
    <div class="pull-left">
	<?php  Pjax::begin(['id'=>'tmp-person-rs-pjax']);?>
	<h4 >รายการในใบกำกับงาน <font color="#ff0000"><strong><?=$ovCount?></strong></font> คน / <?=$total_label?> <font color="#ff0000"><strong><?=$total?></strong></font> คน (นับทุกคนจาก person)</h4>
	<?php  Pjax::end();?>
    </div>
    <div class="ov-person-back" style="margin-bottom: 15px; text-align: right;">
	<?php echo Html::a('<i class="fa fa-reply"></i> กลับไปหน้าแสดงใบกำกับงาน', ['/ov2/ov-person/index'], ['class' => 'btn btn-default']) ?>
    </div>
    
    <?= Html::a('ไปหน้าเลือกจาก TCC Bot', ['/ov2/tmp-person/index'], ['class' => 'btn btn-success']) ?>
    <div class="ov-person-list" style="margin-bottom: 15px; text-align: center; ">
	<?php $form = ActiveForm::begin([
	    'action' => ['/ov2/tmp-person/village2'],
	    'method' => 'get',
	    'layout' => 'inline',

	]); ?>

	<?= Html::label('ช่วงอายุตั้งแต่ '); ?>
	
	<?= Html::textInput('minage', $min, ['class'=>'form-control', 'type'=>'number', 'min'=>0, 'max'=>100]) ?>
	
	<?= Html::label(' ปีขึ้นไป ถึงอายุ '); ?>
	
	<?= Html::textInput('maxage', $max, ['class' => 'form-control', 'type'=>'number', 'min'=>0, 'max'=>100]) ?>
	
	<?= Html::label(' ปี '); ?>
	
	<?= Html::checkbox('type', $type, ['class'=>'form-group' , 'label'=>'แยกตามหมู่'])?>
	
	<div class="form-group">
	    <?= Html::submitButton(Yii::t('app', '<span class="glyphicon glyphicon-eye-open"></span> แสดงเป้าหมาย'), ['class' => 'btn btn-primary']) ?>
	</div>  
	
	<?php ActiveForm::end(); ?>
    </div>
    <?php
   
    
    if(isset(Yii::$app->session['tmp_Villagelist']) && isset($_GET['minage']) && isset($_GET['maxage']) ){
    ?>
    <div class="ov-person-show" style="margin-bottom: 15px; text-align: center; ">
	<?php $form = ActiveForm::begin([
	    'action' => ['/ov2/tmp-person/drump2', 'minage'=>$min, 'maxage'=>$max, 'type'=>$type],
	    'method' => 'get',
	    'layout' => 'inline',
	    'id' => 'jump_menu',
	]); ?>

	<?php
	
	    $vlist = Yii::$app->session['tmp_Villagelist'];
//	    foreach ($vlist as $key => $value) {
//		$vlist[$key]['text'] = iconv('tis-620', 'utf-8', $value['text']);
//		
//	    }
    
	    echo Html::label('จำนวนเป้าหมายในแต่ละหมู่บ้าน &nbsp; ');
	    
	    echo Html::dropDownList('village', $village, yii\helpers\ArrayHelper::map($vlist, 'add1n2', 'text'), ['class'=>'form-control' ,'prompt'=>'เลือกเป้าหมายเพื่อแสดงผู้รับบริการ', 'onChange'=>'$("#jump_menu").submit()']);
	    
	
	?>
	
	<?php ActiveForm::end(); ?>
    </div>
    
    <?php if(isset(Yii::$app->session['tmp_Villagelist']) && isset($_GET['minage']) && isset($_GET['maxage']) && isset($_GET['village'])){ ?>
    
    <?php  Pjax::begin(['id'=>'tmp-person-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'tmp-person-grid',
	'panelBtn' => //Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['tmp-person/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-tmp-person']). ' ' .
		      '<span><strong>จำนวนที่เลือก</strong></span>  <font color="#ff0000"><span id="cart-num">0</span></font> '.Html::button('<i class="fa fa-cart-plus"></i> นำเข้าข้อมูล', ['data-url'=>Url::to(['tmp-person/import']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-delbtn-tmp-person', 'disabled'=>true]),
		      
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionTmpPersonIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
	    [
		'attribute'=>'cid',
		'label'=>'CID',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:120px; text-align: center;'],
	    ],
	    [
		'attribute'=>'pname',
		'label'=>'คำนำหน้า',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:85px; text-align: center;'],
	    ],
            [
		'attribute'=>'fname',
		'label'=>'ชื่อ',
	    ],
	    [
		'attribute'=>'lname',
		'label'=>'นามสกุล',
	    ],
            [
		'attribute'=>'birthdate',
		'label'=>'อายุ',
		'value'=>function ($data){ return \backend\modules\ov2\classes\OvccaFunc::getAge($data['birthdate']); },
		'filter'=>'',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:60px; text-align: center;'],
	    ],
		[
		'attribute'=>'type_area',
		'label'=>'Area',
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:60px; text-align: center;'],
	    ],	
			[
		'attribute'=>'sex',
		'label'=>'เพศ',
		'value'=>function ($data){ 
		    if($data['sex']==1){
			return 'ชาย';
		    } elseif ($data['sex']==2) {
			return 'หญิง';
		    } else {
			return 'ไม่ทราบเพศ';
		    }
		},
		'filter'=>Html::activeDropDownList($searchModel, 'sex', [1=>'ชาย',2=>'หญิง'], ['class'=>'form-control', 'prompt'=>'All']),
		'headerOptions'=>['style'=>'text-align: center;'],
		'contentOptions'=>['style'=>'width:80px; text-align: center;'],
	    ],
	    [
		'attribute'=>'hospname',
		'label'=>'โรงพยาบาล',
		'filter'=>'',
		'contentOptions'=>['style'=>'width:250px; '],
	    ],
//	    [
//		'class' => 'appxq\sdii\widgets\ActionColumn',
//		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
//		'template' => '{view} {update} {delete}',
//	    ],
        ],
    ]); ?>
    <?php  Pjax::end(); } }?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-tmp-person',
    //'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#tmp-person-grid-pjax').on('click', '#modal-addbtn-tmp-person', function() {
    modalTmpPerson($(this).attr('data-url'));
});

$('#tmp-person-grid-pjax').on('click', '#modal-delbtn-tmp-person', function() {
    importTmpPerson($(this).attr('data-url'));
});

$('#tmp-person-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#tmp-person-grid').yiiGridView('getSelectedRows');
	disabledTmpPersonBtn(key.length);
    },100);
});

$('#tmp-person-grid-pjax').on('click', '.selectionTmpPersonIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledTmpPersonBtn(key.length);
});

$('#tmp-person-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    //modalTmpPerson('".Url::to(['tmp-person/update', 'id'=>''])."'+id);
});	

$('#tmp-person-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalTmpPerson(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#tmp-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledTmpPersonBtn(num) {
    if(num>0) {
	$('#modal-delbtn-tmp-person').attr('disabled', false);
    } else {
	$('#modal-delbtn-tmp-person').attr('disabled', true);
    }
    $('#cart-num').html(num);
}

$('#modal-delallbtn-tmp-person').on('click', function() {
    var url = $(this).attr('data-url');
    modalTmpPerson(url);
});

function selectionTmpPersonGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionTmpPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#tmp-person-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function importTmpPerson(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to import these items?')."', function() {
	$('#modal-tmp-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
	$('#modal-tmp-person').modal('show');
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionTmpPersonIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'HTML',
	    success: function(result, textStatus) {
		$('#modal-tmp-person .modal-content').html(result);
	    }
	});
    });
}

function modalTmpPerson(url) {
    $('#modal-tmp-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-tmp-person').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>