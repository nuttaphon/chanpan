<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\ov2\models\OvPerson */

$this->title = Yii::t('app', 'Create Ov Person');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ov People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ov-person-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
