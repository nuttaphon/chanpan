<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">เพิ่มลงในใบกำกับงาน</h4>
</div>

<div class="modal-body">
    <?php if($model):?>

    <div id="fieldsSelect" class="list-group">
    <?php foreach ($model as $key => $value):?>
	
	<a style="cursor: pointer;" id="list-<?=$value['sub_id']?>" data-id="<?=$value['filter_id']?>" data-sub="<?=$value['sub_id']?>" class="addbtnlist list-group-item"><?=$value['sub_name']?></a>
	    
	
    <?php endforeach;?>
	    </div>
    <?php endif; ?>
</div>

<?php  $this->registerJs("
$('.addbtnlist').on('click', function() {
    var id = $(this).attr('data-id');
    var sub = $(this).attr('data-sub');
    var selection = ".\yii\helpers\Json::encode($selection).";
    $.ajax({
	method: 'POST',
	url: '".yii\helpers\Url::to(['ov-person/savelist'])."',
	data: {id:id, sub:sub, selection:selection},
	dataType: 'JSON',
	success: function(result, textStatus) {
	    $(document).find('#modal-ovlist').modal('hide');
	    ". common\lib\sdii\components\helpers\SDNoty::show('result.message', 'result.status') ."
		$.pjax.reload({container:'#ov-person-grid-pjax'});
	}
    });
});


");?>