<?php 
use yii\helpers\Html;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="itemModalLabel">สรุปผล</h4>
</div>

<div class="modal-body">
    จำนวนที่จะตรวจอุจจาระ, ปัสสาวะ และ ตรวจอัลตร้าซาวด์ (วางแผน/ทำได้ = <font color="#ff0000"><strong><?=$sumT?></strong></font>/<font color="#ff0000"><strong><?=$sumTY?></strong></font> คน) (คิดเป็น <font color="#ff0000"><strong><?=  number_format(($sumTY/$sumT)*100, 2)?></strong></font> %)<br>
    จำนวนที่จะตรวจเฉพาะอัลตร้าซาวด์เท่านั้น (วางแผน/ทำได้ = <font color="#ff0000"><strong><?=$sumF?></strong></font>/<font color="#ff0000"><strong><?=$sumFY?></strong></font> คน) (คิดเป็น <font color="#ff0000"><strong><?=  number_format(($sumFY/$sumF)*100, 2)?></strong></font> %)
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
</div>

<?php  $this->registerJs("
    

");?>