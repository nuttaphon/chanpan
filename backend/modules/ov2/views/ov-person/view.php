<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ov2\models\OvPerson */

$this->title = 'Ov Person#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ov People'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ov-person-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'id',
		'khet',
		'province',
		'amphur',
		'tambon',
		'hospcode',
		'hospname',
		'person_id',
		'house_id',
		'address',
		'cid',
		'hn',
		'pname',
		'fname',
		'lname',
		'sex',
		'nationality',
		'education',
		'type_area',
		'religion',
		'birthdate',
		'village_id',
		'village_code',
		'village_name',
		'pttype',
		'pttype_begin_date',
		'pttype_expire_date',
		'pttype_hospmain',
		'pttype_hospsub',
		'marrystatus',
		'death',
		'death_date',
	    ],
	]) ?>
    </div>
</div>
