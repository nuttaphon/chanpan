<?php

namespace backend\modules\webboard\controllers;

use backend\modules\ezforms\components\EzformQuery;
use Yii;
use backend\modules\webboard\models\WbQuestion;
use backend\modules\webboard\models\WbQuestionSearch;
use yii\db\Expression;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WbQuestionController implements the CRUD actions for WbQuestion model.
 */
class WbQuestionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all WbQuestion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WbQuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WbQuestion model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WbQuestion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WbQuestion();

        if ($model->load(Yii::$app->request->post())) {

            $model->create_by = Yii::$app->user->identity->id;
            $model->create_at = new Expression('NOW()');
            $model->rstat = 1;
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing WbQuestion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing WbQuestion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WbQuestion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return WbQuestion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WbQuestion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionHelpDesk()
    {
        $model = new WbQuestion();
        if ($model->load(Yii::$app->request->post())) {
            //VarDumper::dump($_POST,10,true);exit;

            $model->wb_titile = '[ขอความช่วยเหลือ] '.mb_substr($model->wb_detail, 0,50,'UTF-8').'...';
            //VarDumper::dump($model->wb_titile,10,true);exit;
            $model->create_by = Yii::$app->user->identity->id;
            $model->create_at = new Expression('NOW()');
            $model->rstat = 1;
            $hospital = EzformQuery::getHospital(Yii::$app->user->identity->userProfile->sitecode);
            $model->wb_detail = $this->renderPartial('_str_help_desk', [
                'model'=>$model,
                'fullname' =>$_POST['fullname'],
                'email' =>$_POST['email'],
                'phone' =>$_POST['phone'],
                'help_url' =>$_POST['help_url'],
                'hospital' => $hospital
            ]);

            /*
            //send mail
            $mailer = Yii::$app->mailer->compose('@app/mail/help_desk',[
                'data'=>$model,
            ])
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')])
                ->setTo( Yii::$app->keyStorage->get('help_desk_email'))
                ->setCc(\backend\modules\ezforms\components\EzformFunc::getEmailSite())
                ->setSubject('Help Desk from : '.Yii::$app->keyStorage->get('sender'));
                //->attach(Yii::getAlias('@storageUrl').'/source/'.$model->file)
            $mailer->send();

            //send to telegram
            $text = '*Help Desk from : '.Yii::$app->keyStorage->get('sender').'* %0A*เรื่อง :* '.mb_substr($_POST['WbQuestion']['wb_detail'], 0,50,'UTF-8').'... %0A*โดย :* '.$_POST['fullname'].' %0A*หน่วยงาน : '.$hospital['hcode'].' '.$hospital['name'].'* %0A*E-mail : '.$_POST['email'].'* %0A*เบอร์โทรศัพท์ :* '.$_POST['phone'].' %0A*URL ที่รายงาน :* [Click to open]('.urlencode($_POST['help_url']).') %0A*รายละเอียด :* %0A '.addcslashes(strip_tags($_POST['WbQuestion']['wb_detail']), '*,/,_,[,],\\');
            $bot = new \common\lib\telegram\helpers\TelegramBot();
            $bot->token = Yii::$app->keyStorage->get('telegram.token');
            $res = $bot->sendMessage('-1001030196726', $text);

            //
            $resx = json_decode($res);
            if($resx->ok){
                $model->telegram_error = $res;
                $model->telegram_result = $res;
            }else{
                $model->telegram_error = $res;
                $model->telegram_result = $text;
            }
            */

            if(true) {
                if ($model->save()) {
                    $result = [
                        'status' => 'success',
                        'action' => 'create',
                        'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'ส่งคำขอช่วยเหลือเรียบร้อย.'),
                        'data' => $model,
                    ];
                } else {
                    $result = [
                        'status' => 'error',
                        'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถส่งคำขอช่วย โปรดลองใหม่ภายหลัง.'),
                        'data' => $model,
                    ];
                }
            }else{
                $result = [
                    'status' => 'error',
                    'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถส่งคำขอช่วย โปรดลองใหม่ภายหลัง.'),
                    'data' => $model,
                ];
            }
            return json_encode($result);

        }else {
            return $this->renderAjax('_form_help_desk', [
                'model' => $model,
                'helpurl' => Yii::$app->request->referrer,
            ]);
        }
    }
}
