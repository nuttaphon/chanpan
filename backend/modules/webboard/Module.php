<?php

namespace backend\modules\webboard;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\webboard\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
