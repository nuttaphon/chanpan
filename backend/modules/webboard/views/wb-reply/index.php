<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\webboard\models\WbReplySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wb Replies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-reply-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Wb Reply', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ques_id',
            'wb_comment:ntext',
            'create_by',
            'create_at',
            // 'update_at',
            // 'rstat',
            // 'bookmark',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
