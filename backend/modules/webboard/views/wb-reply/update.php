<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\webboard\models\WbReply */

$this->title = 'Update Wb Reply: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Wb Replies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wb-reply-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
