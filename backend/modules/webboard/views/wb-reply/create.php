<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\webboard\models\WbReply */

$this->title = 'Create Wb Reply';
$this->params['breadcrumbs'][] = ['label' => 'Wb Replies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-reply-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
