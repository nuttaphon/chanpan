<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\webboard\models\WbReply */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Wb Replies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-reply-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ques_id',
            'wb_comment:ntext',
            'create_by',
            'create_at',
            'update_at',
            'rstat',
            'bookmark',
        ],
    ]) ?>

</div>
