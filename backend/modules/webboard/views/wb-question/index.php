<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\webboard\models\WbQuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Web board';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wb-question-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ตั้งหัวข้อใหม่', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'wb_titile',
            /*
            [
                'label' =>"Action",
                'attribute' => 'wb_detail',
                'format'=>'html',
                'value'=>function($model, $key, $index, $widget){
                    return strip_tags($model->wb_detail);
                }
            ],
            */
            [
                'label' =>"ตั้งโดย",
                'attribute' => 'create_by',
                'format'=>'text',
                'value'=>function($model, $key, $index, $widget){
                    $user = common\models\UserProfile::findOne($model->create_by);
                    return $user->firstname . ' ' . $user->lastname;
                }
            ],
            'create_at',
            // 'update_at',
            // 'rstat',
            // 'hits',
            // 'bookmark',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttonOptions'=>['class'=>'btn btn-default'],
                'template'=>'<div class="btn-group btn-group-sm text-center" role="group">{view}</div>',
                'options'=> ['style'=>'width:150px;'],
                'buttons'=>[
                    'view' => function($url,$model,$key){
                        return Html::a('<i class="fa fa-file-text-o"></i>',$url,['class'=>'btn btn-default']);
                    }
                ]
            ],
        ],
    ]); ?>

</div>
