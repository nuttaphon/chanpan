<?php

namespace backend\modules\webboard\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\webboard\models\WbQuestion;

/**
 * WbQuestionSearch represents the model behind the search form about `backend\modules\webboard\models\WbQuestion`.
 */
class WbQuestionSearch extends WbQuestion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'create_by', 'rstat', 'type', 'hits_reply', 'hits_read', 'bookmark'], 'integer'],
            [['wb_titile', 'wb_detail', 'create_at', 'update_at', 'telegram_result', 'telegram_error'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WbQuestion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'create_by' => $this->create_by,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'rstat' => $this->rstat,
            'type' => $this->type,
            'hits_reply' => $this->hits_reply,
            'hits_read' => $this->hits_read,
            'bookmark' => $this->bookmark,
        ]);

        $query->andFilterWhere(['like', 'wb_titile', $this->wb_titile])
            ->andFilterWhere(['like', 'wb_detail', $this->wb_detail])
            ->andFilterWhere(['like', 'telegram_result', $this->telegram_result])
            ->andFilterWhere(['like', 'telegram_error', $this->telegram_error]);

        return $dataProvider;
    }
}
