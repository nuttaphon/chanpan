<?php

namespace backend\modules\webboard\models;

use Yii;

/**
 * This is the model class for table "wb_question".
 *
 * @property string $id
 * @property string $wb_titile
 * @property string $wb_detail
 * @property string $create_by
 * @property string $create_at
 * @property string $update_at
 * @property integer $rstat
 * @property integer $type
 * @property integer $hits_reply
 * @property integer $hits_read
 * @property integer $bookmark
 * @property string $telegram_result
 * @property string $telegram_error
 */
class WbQuestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wb_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wb_titile', 'create_by', 'create_at', 'wb_detail'], 'required'],
            [['wb_detail', 'telegram_result', 'telegram_error'], 'string'],
            [['create_by', 'rstat', 'type', 'hits_reply', 'hits_read', 'bookmark'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['wb_titile'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'primary key',
            'wb_titile' => 'หัวข้อกระทู้',
            'wb_detail' => 'รายละเอียด',
            'create_by' => 'โดย',
            'create_at' => 'เวลาที่ตั้งกระทู้',
            'update_at' => 'เวลาที่ปรับปรุงล่าสุด',
            'rstat' => 'rstat',
            'type' => 'Type',
            'hits_reply' => 'Hits',
            'hits_read' => 'Hits Read',
            'bookmark' => 'Bookmark',
            'telegram_result' => 'Telegram Result',
            'telegram_error' => 'Telegram Error',
        ];
    }
}
