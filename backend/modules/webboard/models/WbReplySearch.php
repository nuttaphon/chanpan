<?php

namespace backend\modules\webboard\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\webboard\models\WbReply;

/**
 * WbReplySearch represents the model behind the search form about `backend\modules\webboard\models\WbReply`.
 */
class WbReplySearch extends WbReply
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ques_id', 'create_by', 'rstat', 'bookmark'], 'integer'],
            [['wb_comment', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WbReply::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ques_id' => $this->ques_id,
            'create_by' => $this->create_by,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'rstat' => $this->rstat,
            'bookmark' => $this->bookmark,
        ]);

        $query->andFilterWhere(['like', 'wb_comment', $this->wb_comment]);

        return $dataProvider;
    }
}
