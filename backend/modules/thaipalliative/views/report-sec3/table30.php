<?
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;


$domain=Url::home();

?>
<div class="table-responsive">
  <h2>ตารางที่ 30</h2>
  <p>จำนวนและร้อยละผู้ป่วย จำแนกตามผลการประเมินด้านจิตวิญญาณ เป็นรายโรค ระหว่าง <ว/ด/ป> ถึง <ว/ด/ป></p>
  <table class="table table-bordered">
    <thead>
      <tr>
        <td rowspan="2"><center>โรค</center></td>
        <td colspan="2"><center>Hope</center></td>
       <td colspan="2" ><center>Fear</center></td>
        <td colspan="2" ><center>Unfinished business</center></td>
    	 <td colspan="2"><center>Connected ness </center></td>
       <td colspan="2" ><center>Control helpless</center></td>
        <td colspan="2" ><center>Religious</center></td>
      </tr>
      <tr>
        <td><center>n</center></td>
        <td><center>%</center></td>
    	 <td><center>n</center></td>
        <td><center>%</center></td>
    	 <td><center>n</center></td>
        <td><center>%</center></td>
    	 <td><center>n</center></td>
        <td><center>%</center></td>
    	 <td><center>n</center></td>
        <td><center>%</center></td>
    	 <td><center>n</center></td>
        <td><center>%</center></td>

      </tr>
      </thead>
    <tbody>
      <tr >
      <!--cancer var49=1 -->
      <td>Cancer ทุกประเภท</td>
      <td align='right'><? $sumhope+=$cancerhope[0][total];echo $cancerhope[0][total] ?></td>
      <td align='right'><? $sumhopepercent+=number_format(($cancerhope[0][total]/$cancerhope[0][sumtotal])*100,1); echo number_format(($cancerhope[0][total]/$cancerhope[0][sumtotal])*100,1) ?></td>
      <td align='right'><? $sumfear+=$cancerfear[0][total];echo $cancerfear[0][total] ?></td>
    	<td align='right'><? $sumfearpercent+=number_format(($cancerfear[0][total]/$cancerfear[0][sumtotal])*100,1); echo number_format(($cancerfear[0][total]/$cancerfear[0][sumtotal])*100,1) ?></td>
    	<td align='right'><? $sumunfinished+=$cancerunfinished[0][total]; echo $cancerunfinished[0][total] ?></td>
    	<td align='right'><? $sumunfinishedpercent+=number_format(($cancerunfinished[0][total]/$cancerunfinished[0][sumtotal])*100,1) ;echo number_format(($cancerunfinished[0][total]/$cancerunfinished[0][sumtotal])*100,1) ?></td>
      <td align='right'><? $sumconnected+=$cancerconnected[0][total];echo $cancerconnected[0][total] ?></td>
      <td align='right'><? $sumconnectedpercent+=number_format(($cancerconnected[0][total]/$cancerconnected[0][sumtotal])*100,1); echo number_format(($cancerconnected[0][total]/$cancerconnected[0][sumtotal])*100,1) ?></td>
      <td align='right'><? $sumcontrol+=$cancercontrol[0][total]; echo $cancercontrol[0][total] ?></td>
      <td align='right'><? $sumcontrolpercent+=number_format(($cancercontrol[0][total]/$cancercontrol[0][sumtotal])*100,1);echo number_format(($cancercontrol[0][total]/$cancercontrol[0][sumtotal])*100,1) ?></td>
      <td align='right'><? $sumreligious+=$cancerreligious[0][total];echo $cancerreligious[0][total] ?></td>
      <td align='right'><? $sumreligiouspercent+=number_format(($cancerreligious[0][total]/$cancerreligious[0][sumtotal])*100,1); echo number_format(($cancerreligious[0][total]/$cancerreligious[0][sumtotal])*100,1) ?></td>
      </tr>
      <!--Renal-->
      <tr>
        <td >End stage renal diseases</td>
        <td align='right'><? $sumhope+=$renalhope[0][total];echo $renalhope[0][total] ?></td>
        <td align='right'><? $sumhopepercent+=number_format(($renalhope[0][total]/$renalhope[0][sumtotal])*100,1); echo number_format(($renalhope[0][total]/$renalhope[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumfear+=$renalfear[0][total];echo $renalfear[0][total] ?></td>
      	<td align='right'><? $sumfearpercent+=number_format(($renalfear[0][total]/$renalfear[0][sumtotal])*100,1); echo number_format(($renalfear[0][total]/$renalfear[0][sumtotal])*100,1) ?></td>
      	<td align='right'><? $sumunfinished+=$renalunfinished[0][total]; echo $renalunfinished[0][total] ?></td>
      	<td align='right'><? $sumunfinishedpercent+=number_format(($renalunfinished[0][total]/$renalunfinished[0][sumtotal])*100,1) ;echo number_format(($renalunfinished[0][total]/$renalunfinished[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumconnected+=$renalconnected[0][total];echo $renalconnected[0][total] ?></td>
        <td align='right'><? $sumconnectedpercent+=number_format(($renalconnected[0][total]/$renalconnected[0][sumtotal])*100,1); echo number_format(($renalconnected[0][total]/$renalconnected[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumcontrol+=$renalcontrol[0][total]; echo $renalcontrol[0][total] ?></td>
        <td align='right'><? $sumcontrolpercent+=number_format(($renalcontrol[0][total]/$renalcontrol[0][sumtotal])*100,1);echo number_format(($renalcontrol[0][total]/$renalcontrol[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumreligious+=$renalreligious[0][total];echo $renalreligious[0][total] ?></td>
        <td align='right'><? $sumreligiouspercent+=number_format(($renalreligious[0][total]/$renalreligious[0][sumtotal])*100,1); echo number_format(($renalreligious[0][total]/$renalreligious[0][sumtotal])*100,1) ?></td>
      </tr>
      <tr>
        <td >End stage trauma</td>
        <td align='right'><? $sumhope+=$traumahope[0][total];echo $traumahope[0][total] ?></td>
        <td align='right'><? $sumhopepercent+=number_format(($traumahope[0][total]/$traumahope[0][sumtotal])*100,1); echo number_format(($traumahope[0][total]/$traumahope[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumfear+=$traumafear[0][total];echo $traumafear[0][total] ?></td>
      	<td align='right'><? $sumfearpercent+=number_format(($traumafear[0][total]/$traumafear[0][sumtotal])*100,1); echo number_format(($traumafear[0][total]/$traumafear[0][sumtotal])*100,1) ?></td>
      	<td align='right'><? $sumunfinished+=$traumaunfinished[0][total]; echo $traumaunfinished[0][total] ?></td>
      	<td align='right'><? $sumunfinishedpercent+=number_format(($traumaunfinished[0][total]/$traumaunfinished[0][sumtotal])*100,1) ;echo number_format(($traumaunfinished[0][total]/$traumaunfinished[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumconnected+=$traumaconnected[0][total];echo $traumaconnected[0][total] ?></td>
        <td align='right'><? $sumconnectedpercent+=number_format(($traumaconnected[0][total]/$traumaconnected[0][sumtotal])*100,1); echo number_format(($traumaconnected[0][total]/$traumaconnected[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumcontrol+=$traumacontrol[0][total]; echo $traumacontrol[0][total] ?></td>
        <td align='right'><? $sumcontrolpercent+=number_format(($traumacontrol[0][total]/$traumacontrol[0][sumtotal])*100,1);echo number_format(($traumacontrol[0][total]/$traumacontrol[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumreligious+=$traumareligious[0][total];echo $traumareligious[0][total] ?></td>
        <td align='right'><? $sumreligiouspercent+=number_format(($traumareligious[0][total]/$traumareligious[0][sumtotal])*100,1); echo number_format(($traumareligious[0][total]/$traumareligious[0][sumtotal])*100,1) ?></td>

      </tr>
      <tr>
        <td >End stage lung diseases</td>
        <td align='right'><? $sumhope+=$lunghope[0][total];echo $lunghope[0][total] ?></td>
        <td align='right'><? $sumhopepercent+=number_format(($lunghope[0][total]/$lunghope[0][sumtotal])*100,1); echo number_format(($lunghope[0][total]/$lunghope[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumfear+=$lungfear[0][total];echo $lungfear[0][total] ?></td>
      	<td align='right'><? $sumfearpercent+=number_format(($lungfear[0][total]/$lungfear[0][sumtotal])*100,1); echo number_format(($lungfear[0][total]/$lungfear[0][sumtotal])*100,1) ?></td>
      	<td align='right'><? $sumunfinished+=$lungunfinished[0][total]; echo $lungunfinished[0][total] ?></td>
      	<td align='right'><? $sumunfinishedpercent+=number_format(($lungunfinished[0][total]/$lungunfinished[0][sumtotal])*100,1) ;echo number_format(($lungunfinished[0][total]/$lungunfinished[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumconnected+=$lungconnected[0][total];echo $lungconnected[0][total] ?></td>
        <td align='right'><? $sumconnectedpercent+=number_format(($lungconnected[0][total]/$lungconnected[0][sumtotal])*100,1); echo number_format(($lungconnected[0][total]/$lungconnected[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumcontrol+=$lungcontrol[0][total]; echo $lungcontrol[0][total] ?></td>
        <td align='right'><? $sumcontrolpercent+=number_format(($lungcontrol[0][total]/$lungcontrol[0][sumtotal])*100,1);echo number_format(($lungcontrol[0][total]/$lungcontrol[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumreligious+=$lungreligious[0][total];echo $lungreligious[0][total] ?></td>
        <td align='right'><? $sumreligiouspercent+=number_format(($lungreligious[0][total]/$lungreligious[0][sumtotal])*100,1); echo number_format(($lungreligious[0][total]/$lungreligious[0][sumtotal])*100,1) ?></td>

      </tr>
       <tr>
        <td >Neurological diseases</td>
        <td align='right'><? $sumhope+=$neurohope[0][total];echo $neurohope[0][total] ?></td>
        <td align='right'><? $sumhopepercent+=number_format(($neurohope[0][total]/$neurohope[0][sumtotal])*100,1); echo number_format(($neurohope[0][total]/$neurohope[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumfear+=$neurofear[0][total];echo $neurofear[0][total] ?></td>
      	<td align='right'><? $sumfearpercent+=number_format(($neurofear[0][total]/$neurofear[0][sumtotal])*100,1); echo number_format(($neurofear[0][total]/$neurofear[0][sumtotal])*100,1) ?></td>
      	<td align='right'><? $sumunfinished+=$neurounfinished[0][total]; echo $neurounfinished[0][total] ?></td>
      	<td align='right'><? $sumunfinishedpercent+=number_format(($neurounfinished[0][total]/$neurounfinished[0][sumtotal])*100,1) ;echo number_format(($neurounfinished[0][total]/$neurounfinished[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumconnected+=$neuroconnected[0][total];echo $neuroconnected[0][total] ?></td>
        <td align='right'><? $sumconnectedpercent+=number_format(($neuroconnected[0][total]/$neuroconnected[0][sumtotal])*100,1); echo number_format(($neuroconnected[0][total]/$neuroconnected[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumcontrol+=$neurocontrol[0][total]; echo $neurocontrol[0][total] ?></td>
        <td align='right'><? $sumcontrolpercent+=number_format(($neurocontrol[0][total]/$neurocontrol[0][sumtotal])*100,1);echo number_format(($neurocontrol[0][total]/$neurocontrol[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumreligious+=$neuroreligious[0][total];echo $neuroreligious[0][total] ?></td>
        <td align='right'><? $sumreligiouspercent+=number_format(($neuroreligious[0][total]/$neuroreligious[0][sumtotal])*100,1); echo number_format(($neuroreligious[0][total]/$neuroreligious[0][sumtotal])*100,1) ?></td>
      </tr>
       <tr>
        <td >End stage heart diseases</td>
        <td align='right'><? $sumhope+=$hearthope[0][total];echo $hearthope[0][total] ?></td>
        <td align='right'><? $sumhopepercent+=number_format(($hearthope[0][total]/$hearthope[0][sumtotal])*100,1); echo number_format(($hearthope[0][total]/$hearthope[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumfear+=$heartfear[0][total];echo $heartfear[0][total] ?></td>
      	<td align='right'><? $sumfearpercent+=number_format(($heartfear[0][total]/$heartfear[0][sumtotal])*100,1); echo number_format(($heartfear[0][total]/$heartfear[0][sumtotal])*100,1) ?></td>
      	<td align='right'><? $sumunfinished+=$heartunfinished[0][total]; echo $heartunfinished[0][total] ?></td>
      	<td align='right'><? $sumunfinishedpercent+=number_format(($heartunfinished[0][total]/$heartunfinished[0][sumtotal])*100,1) ;echo number_format(($heartunfinished[0][total]/$heartunfinished[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumconnected+=$heartconnected[0][total];echo $heartconnected[0][total] ?></td>
        <td align='right'><? $sumconnectedpercent+=number_format(($heartconnected[0][total]/$heartconnected[0][sumtotal])*100,1); echo number_format(($heartconnected[0][total]/$heartconnected[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumcontrol+=$heartcontrol[0][total]; echo $heartcontrol[0][total] ?></td>
        <td align='right'><? $sumcontrolpercent+=number_format(($heartcontrol[0][total]/$heartcontrol[0][sumtotal])*100,1);echo number_format(($heartcontrol[0][total]/$heartcontrol[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumreligious+=$heartreligious[0][total];echo $heartreligious[0][total] ?></td>
        <td align='right'><? $sumreligiouspercent+=number_format(($heartreligious[0][total]/$heartreligious[0][sumtotal])*100,1); echo number_format(($heartreligious[0][total]/$heartreligious[0][sumtotal])*100,1) ?></td>
      </tr>
      <tr>
        <td >อื่นๆ </td>
        <td align='right'><? $sumhope+=$otherhope[0][total];echo $otherhope[0][total] ?></td>
        <td align='right'><? $sumhopepercent+=number_format(($otherhope[0][total]/$otherhope[0][sumtotal])*100,1); echo number_format(($otherhope[0][total]/$otherhope[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumfear+=$otherfear[0][total];echo $otherfear[0][total] ?></td>
        <td align='right'><? $sumfearpercent+=number_format(($otherfear[0][total]/$otherfear[0][sumtotal])*100,1); echo number_format(($otherfear[0][total]/$otherfear[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumunfinished+=$otherunfinished[0][total]; echo $otherunfinished[0][total] ?></td>
        <td align='right'><? $sumunfinishedpercent+=number_format(($otherunfinished[0][total]/$otherunfinished[0][sumtotal])*100,1) ;echo number_format(($otherunfinished[0][total]/$otherunfinished[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumconnected+=$otherconnected[0][total];echo $otherconnected[0][total] ?></td>
        <td align='right'><? $sumconnectedpercent+=number_format(($otherconnected[0][total]/$otherconnected[0][sumtotal])*100,1); echo number_format(($otherconnected[0][total]/$otherconnected[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumcontrol+=$othercontrol[0][total]; echo $othercontrol[0][total] ?></td>
        <td align='right'><? $sumcontrolpercent+=number_format(($othercontrol[0][total]/$othercontrol[0][sumtotal])*100,1);echo number_format(($othercontrol[0][total]/$othercontrol[0][sumtotal])*100,1) ?></td>
        <td align='right'><? $sumreligious+=$otherreligious[0][total];echo $otherreligious[0][total] ?></td>
        <td align='right'><? $sumreligiouspercent+=number_format(($otherreligious[0][total]/$otherreligious[0][sumtotal])*100,1); echo number_format(($otherreligious[0][total]/$otherreligious[0][sumtotal])*100,1) ?></td>

      </tr>

      </tbody>
      <tfoot style="font-weight:bold;">
      <tr bgcolor="#ecee1c">
      <td align="center">รวมทั้งหมด</td>
    	<td align="right"><? echo $sumhope ?></td>
      <td align="right"><? //echo $sumhopepercent ?></td>
      <td align="right"><? echo $sumfear ?></td>
    	<td align="right"><? //echo $sumfearpercent ?></td>
    	<td align="right"><? echo $sumunfinished ?></td>
    	<td align="right"><? //echo $sumunfinishedpercent ?></td>
    	<td align="right"><? echo $sumconnected ?></td>
      <td align="right"><? //echo $sumconnectedpercent ?></td>
      <td align="right"><? echo $sumcontrol ?></td>
    	<td align="right"><? //echo $sumcontrolpercent ?></td>
    	<td align="right"><? echo $sumreligious ?></td>
    	<td align="right"><? //echo $sumreligiouspercent ?></td>
      </tr>
      </tfoot>
    </table>
  </div>

  <?php
  $totacount=count($dataArrayTable1);
  for ($i=0; $i < $totacount; $i++)
  {
   ?>

  <?php
  }
   ?>
