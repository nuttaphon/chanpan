<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
Yii::$app->formatter->locale = 'th-TH';

$domain=Url::home();
?>
<div class="table-responsive">
    <h2>ตารางที่ 3</h2>
  <p>จากระยะเวลาการดูแลผู้ป่วย ที่ให้บริการโดย <?=  $data[0][hosname] ?> </p>
  <table class="table table-bordered">
    <thead>
  <tr  bgcolor="#EAF4FF">
    <td rowspan="2"><center>ลักษณะประชากร</center></td>
    <td colspan="2"><center>ทั้งหมด</center></td>
   <td colspan="2" ><center>เฉพาะระหว่าง
  <? echo Yii::$app->formatter->asDate($data[0][date_start], 'long')  ?>ถึง <? echo Yii::$app->formatter->asDate($data[0][date_end], 'long')  ?></center>
  </td>
  </tr>
  <tr bgcolor="#EAF4FF">
    <td><center>จำนวน</center></td>
    <td><center>ร้อยละ</center></td>
   <td><center>จำนวน</center></td>
    <td><center>ร้อยละ</center></td>
  </tr>
  </thead>
  <tbody>
  <tr bgcolor="#EAF4EF">
    <td>ระยะเวลาการดูแลใน รพ.</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>
  <tr>
    <td>เฉลี่ย (ส่วนเบี่ยงเบนสาตรฐาน), เดือน</td>
    <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>
  <tr>
    <td>มัธยฐาน (ต่ำสุด : สูงสุด) , เดือน</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>
  <tr bgcolor="#EAF4EF">
    <td >ระยะเวลาการดูแลที่บ้าน</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>
  <tr>
    <td>เฉลี่ย (ส่วนเบี่ยงเบนสาตรฐาน), เดือน</td>
    <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>
  <tr>
    <td>มัธยฐาน (ต่ำสุด : สูงสุด) , เดือน</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>
  <tr bgcolor="#EAF4EF">
    <td >ระยะเวลาที่ดูแลทั้งหมด</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>
  <tr>
    <td>เฉลี่ย (ส่วนเบี่ยงเบนสาตรฐาน), เดือน</td>
    <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>
  <tr>
    <td>มัธยฐาน (ต่ำสุด : สูงสุด) , เดือน</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
     <td align='right'>&nbsp;</td>
   <td align='right'>&nbsp;</td>
  </tr>


  </tbody>
  <tfoot>
  <tr bgcolor="#EAF4FF">
    <td align="right"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </tfoot>
  </table>
</div>
