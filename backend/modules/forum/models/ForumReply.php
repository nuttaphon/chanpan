<?php

namespace backend\modules\forum\models;

use Yii;

/**
 * This is the model class for table "forum_reply".
 *
 * @property integer $id
 * @property integer $forum_id
 * @property string $reply_image
 * @property string $reply_comment
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class ForumReply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['forum_id', 'reply_image', 'reply_comment', 'created_by', 'created_at', 'updated_at'], 'required'],
            [['forum_id', 'created_by'], 'integer'],
            [['reply_image', 'reply_comment'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'forum_id' => 'Forum ID',
            'reply_image' => 'Reply Image',
            'reply_comment' => 'Reply Comment',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
