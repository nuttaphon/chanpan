<?php

namespace backend\modules\forum\models;

use Yii;

/**
 * This is the model class for table "forum_type".
 *
 * @property integer $id
 * @property string $forum_type_name
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class ForumType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['forum_type_name', 'created_by', 'created_at', 'updated_at'], 'required'],
            [['created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['forum_type_name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'forum_type_name' => 'Forum Type Name',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
