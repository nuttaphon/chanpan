<?php

namespace backend\modules\forum\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\forum\models\ForumReply;

/**
 * ForumReplySearch represents the model behind the search form about `backend\modules\forum\models\ForumReply`.
 */
class ForumReplySearch extends ForumReply
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'forum_id', 'created_by'], 'integer'],
            [['reply_image', 'reply_comment', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ForumReply::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'forum_id' => $this->forum_id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'reply_image', $this->reply_image])
            ->andFilterWhere(['like', 'reply_comment', $this->reply_comment]);

        return $dataProvider;
    }
}
