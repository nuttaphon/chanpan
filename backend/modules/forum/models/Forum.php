<?php

namespace backend\modules\forum\models;

use Yii;

/**
 * This is the model class for table "forum".
 *
 * @property integer $id
 * @property integer $forum_type_id
 * @property string $forum_title
 * @property string $forum_detail
 * @property string $forum_image
 * @property string $url_report
 * @property integer $bookmark
 * @property integer $hits_read
 * @property integer $hits_reply
 * @property integer $status
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Forum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forum';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['forum_type_id', 'forum_title', 'forum_detail', 'forum_image', 'url_report', 'bookmark', 'hits_read', 'hits_reply', 'status', 'created_by', 'created_at', 'updated_at'], 'required'],
            [['forum_type_id', 'bookmark', 'hits_read', 'hits_reply', 'status', 'created_by'], 'integer'],
            [['forum_detail', 'forum_image', 'url_report'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['forum_title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'forum_type_id' => 'Forum Type ID',
            'forum_title' => 'Forum Title',
            'forum_detail' => 'Forum Detail',
            'forum_image' => 'Forum Image',
            'url_report' => 'Url Report',
            'bookmark' => 'Bookmark',
            'hits_read' => 'Hits Read',
            'hits_reply' => 'Hits Reply',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
