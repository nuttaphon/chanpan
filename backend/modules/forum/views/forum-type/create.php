<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\forum\models\ForumType */

$this->title = 'Create Forum Type';
$this->params['breadcrumbs'][] = ['label' => 'Forum Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forum-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
