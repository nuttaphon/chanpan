<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\forum\models\ForumReply */

$this->title = 'Update Forum Reply: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Forum Replies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="forum-reply-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
