<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\forum\models\ForumSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="forum-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'forum_type_id') ?>

    <?= $form->field($model, 'forum_title') ?>

    <?= $form->field($model, 'forum_detail') ?>

    <?= $form->field($model, 'forum_image') ?>

    <?php // echo $form->field($model, 'url_report') ?>

    <?php // echo $form->field($model, 'bookmark') ?>

    <?php // echo $form->field($model, 'hits_read') ?>

    <?php // echo $form->field($model, 'hits_reply') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
