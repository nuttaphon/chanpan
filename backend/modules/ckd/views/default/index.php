<?php
use yii\bootstrap\Html;
$this->title = Yii::t('backend', 'CHRONIC KIDNEY DISEASE PREVENTION IN THE NORTHEAST OF THAILAND');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');
$this->registerCss('
.primary{
     cursor: pointer;
}
.p1 {
    left: 18%;
    top: 1.5%;
    width: 24%;
    height: 6%;
    position: absolute;
}
.p2 {
    left: 41.5%;
    top: 13.5%;
    width: 16%;
    height: 6%;
    position: absolute;
}
.p3 {
    left: 20.5%;
    top: 26.8%;
    width: 15%;
    height: 6%;
    position: absolute;
}
.p4 {
    left: 56.5%;
    top: 26.8%;
    width: 24%;
    height: 6%;
    position: absolute;
}  
.p5 {
    left: 40%;
    top: 37.9%;
    width: 26.5%;
    height: 8%;
    position: absolute;
}
.p6 {
    left: 44%;
    top: 47.9%;
    width: 23%;
    height: 6%;
    position: absolute;
}
.p7 {
    left: 44%;
    top: 55.9%;
    width: 23%;
    height: 6.5%;
    position: absolute;
}
.p8 {
    left: 44%;
    top: 63.9%;
    width: 23%;
    height: 6%;
    position: absolute;
}
.p9 {
    left: 68.5%;
    top: 38.5%;
    width: 27.5%;
    height: 7.6%;
    position: absolute;
}
.p10 {
    left: 72.5%;
    top: 47.5%;
    width: 23.5%;
    height: 8.6%;
    position: absolute;
}
.p11 {
    left: 72.5%;
    top: 58.5%;
    width: 23.5%;
    height: 5.6%;
    position: absolute;
}
.p12 {
    left: 72.5%;
    top: 66.5%;
    width: 22.5%;
    height: 5.6%;
    position: absolute;
}
.p13 {
    left: 2.5%;
    top: 14.5%;
    width: 17.5%;
    height: 5.6%; 
    position: absolute;
}
.p14 {
    left: 2.5%;
    top: 26.8%;
    width: 16%;
    height: 6%;
    position: absolute;
}
.p15 {
    left: 2.7%;
    top: 47.7%;
    width: 15.5%;
    height: 8.6%;
    position: absolute;
}
.p16 {
    left: 20.5%;
    top: 47.5%;
    width: 15.5%;
    height: 8.6%;
    position: absolute;
}
.p17 {
    left: 4.5%;
    top: 65.5%;
    width: 31.5%;
    height: 6.6%;
    position: absolute;
}
.p18 {
    left: 40.5%;
    top: 86.5%;
    width: 26.5%;
    height: 8.6%;
    position: absolute;
}
.p19 {
    left: 3%;
    top: 81.5%;
    width: 35%;
    height: 3.5%;
    position: absolute;
}

.p20 {
    left: 3%;
    top: 85%;
    width: 35%;
    height: 2.5%;
    position: absolute;
}
.p21 {
    left: 3%;
    top: 87.5%;
    width: 35%;
    height: 2.5%;
    position: absolute;
}

.p22 {
    left: 3%;
    top: 90%;
    width: 35%;
    height: 2.5%;
    position: absolute;
}

.p23 {
    left: 3%;
    top: 92.5%;
    width: 35%;
    height: 3%;
    position: absolute;
}

.p24 {
    left: 3%;
    top: 95.5%;
    width: 35%;
    height: 3.5%;
    position: absolute;
}

');
$ckdDiagram = backend\modules\ckdnet\classes\CkdnetFunc::getCkdDiagram(Yii::$app->user->identity->userProfile->sitecode, '', '');
$mapDiagram = yii\helpers\ArrayHelper::map($ckdDiagram, 'id', 'value');
//$mapDiagramLabel = yii\helpers\ArrayHelper::map($ckdDiagram, 'id', 'label');
//echo yii\helpers\Html::dropDownList('ssd', '', $mapDiagramLabel);
//appxq\sdii\utils\VarDumper::dump(\backend\modules\ckdnet\classes\CkdnetQuery::getTotalPop(Yii::$app->user->identity->userProfile->sitecode, '', ''));
//appxq\sdii\utils\VarDumper::dump($mapDiagram);
?>
<div class="ckd-default-index">
    <!--
    <div class="row">
        <div class="col-md-12">
            <?php
            echo Html::checkbox('search_year1', false, ['id'=>'search_year1', 'style'=>' transform: scale(2);']);
            echo Html::label('&nbsp;&nbsp;&nbsp;ปีงบประมาณปัจจุบัน', 'search_year1');
            echo '<br>';
            echo Html::checkbox('search_year2', false, ['id'=>'search_year2', 'style'=>' transform: scale(2);']);
            echo Html::label('&nbsp;&nbsp;&nbsp;ปีงบประมาณอื่นๆ', 'search_year2');
            echo '<br>';
            echo Html::checkbox('search_year3', false, ['id'=>'search_year3', 'style'=>' transform: scale(2);']);
            echo Html::label('&nbsp;&nbsp;&nbsp;เลือกตามช่วงเวลา', 'search_year3');
            ?>
            <div class="row">
                <div class="col-md-6">
                    ระบุช่วงเวลา <?php echo Html::checkbox('set_between', $_GET['set_between']); ?>
                    <?php
                    echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                        //'model'=>$model,
                        'id'=>'start_date',
                        'value' => $_GET['start_date'] ? $_GET['start_date'] : '01'.(date('-m-').(date('Y')+543)),
                        'name' =>'start_date',
                    ]);
                    ?>
                </div>
                <div class="col-md-6">
                    ถึง
                    <?php
                    echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                        //'model'=>$model,
                        'id'=>'end_date',
                        'value' => $_GET['end_date'] ? $_GET['end_date'] : (date('d-m-').(date('Y')+543)),
                        'name' =>'end_date',
                    ]);
                    ?>
                </div>
            </div>
            <hr>
        </div>
    </div>
    -->
    <div class="col-md-8 col-sm-8 col-xs-8" style="max-width:1051px;">
        <img class="img-rounded img-responsive" src="<?= \yii\helpers\Url::to(['@web/images/ckd/ckd-diagram.png']) ?>" alt="">
        <a class="p1 primary text-center" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>1]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[1])?></td></tr></tbody></table></a>
        <a class="p2 primary " href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>2]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[2])?></td></tr></tbody></table></a>
        <a class="p3 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>3]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[3])?></td></tr></tbody></table></a>
        <a class="p4 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>4]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[4])?></td></tr></tbody></table></a>
        <a class="p5 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>5]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[5])?></td></tr></tbody></table></a>
        <a class="p6 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>6]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[6])?></td></tr></tbody></table></a>
        <a class="p7 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>7]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[7])?></td></tr></tbody></table></a>
        <a class="p8 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>8]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[8])?></td></tr></tbody></table></a>
        <a class="p9 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>9]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[9])?></td></tr></tbody></table></a>
        <a class="p10 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>10]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[10])?></td></tr></tbody></table></a>
        <a class="p11 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>11]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[11])?></td></tr></tbody></table></a>
        <a class="p12 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>12]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[12])?></td></tr></tbody></table></a>
        <a class="p13 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>13]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[13])?></td></tr></tbody></table></a>
        <a class="p14 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>14]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[14])?></td></tr></tbody></table></a>
        <a class="p15 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>15]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[15])?></td></tr></tbody></table></a>
        <a class="p16 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>16]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[16])?></td></tr></tbody></table></a>
        <a class="p17 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>17]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: bottom !important;color: red;"><?= number_format($mapDiagram[17])?></td></tr></tbody></table></a>
        <a class="p18 primary" href="<?= \yii\helpers\Url::to(['inv/index', 'state'=>18]) ?>"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important; color: red;"><?= number_format($mapDiagram[18])?></td></tr></tbody></table></a>
        
        <a class="p19 primary"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important;color: red;">19</td></tr></tbody></table></a>
        <a class="p20 primary"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important;color: red;">20</td></tr></tbody></table></a>
        <a class="p21 primary"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important;color: red;">21</td></tr></tbody></table></a>
        <a class="p22 primary"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important;color: red;">22</td></tr></tbody></table></a>
        <a class="p23 primary"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important;color: red;">23</td></tr></tbody></table></a>
        <a class="p24 primary"><table style="height: 100%;width: 100%;"><tbody><tr><td class="text-center"  style="vertical-align: middle !important;color: red;">24</td></tr></tbody></table></a>
    </div>
</div>
