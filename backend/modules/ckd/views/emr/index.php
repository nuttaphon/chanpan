<?php
use yii\bootstrap\Modal;
use kartik\tabs\TabsX;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
$this->title = Yii::t('backend', 'CHRONIC KIDNEY DISEASE PREVENTION IN 
THE NORTHEAST OF THAILAND');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');
$activetab2 = $tab;
if ($activetab2 != "") {
    $active[$activetab2]=true;
}else{
    $active['emr']=true;
}

//yii\helpers\VarDumper::dump($patient,10,true);exit;
//echo \Yii::$app->request->getUrl() . Url::to('/ckd/emr/ckd?ptlink='.$patient->ptlink.'&hospcode='.$hospcode);exit;
$items2 = [
    [
        'label'=>'<i class="fa fa-user"></i> CKD EMR',
        'content'=>$this->renderAjax('ckd',[
            'hospcode'      => $patient->HOSPCODE,
            'patient'       =>  $patient,
            'creatinine'    =>  $creatinine,
            ]),
        'active'=>$active['emr'],
        'linkOptions'=>['data-url'=>Url::to(['/ckd/emr/ckd?ptlink='.$patient->ptlink.'&hospcode='.$hospcode])]
    ],
    [
        'label'=>'<i class="fa fa-user-plus"></i> PHR',
        'content'=>$this->renderAjax('phr',['ptlink'=>$patient->ptlink.'&hospcode='.$hospcode]),
        'active'=>$active['phr'],
        'linkOptions'=>['data-url'=>Url::to(['/ckd/emr/phr?ptlink='.$patient->ptlink.'&hospcode='.$hospcode])]
    ],
];


?>
<div class="ckdnet-default-index">

<div class="form-inline pull-right">    
    <div class="input-group margin-bottom-sm">
      <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
      <input class="form-control" type="text" name="cid" placeholder="เลขที่บัตรประชาชน">
    </div> 
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
</div>
<?php    
echo TabsX::widget([
    'items'=>$items2,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
?>

</div>