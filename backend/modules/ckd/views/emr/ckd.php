<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use miloschuman\highcharts\Highcharts;
use yii\helpers\ArrayHelper;

//echo $hospcode;
//yii\helpers\VarDumper::dump($patient,10,true);

$ptname = $patient->Name;
$ptsurname = $patient->Lname;
$ptcid = $patient->CID;
$ptdupdate = $patient->D_Update;
$ptbirth = $patient->Birth;
if ($ptbirth != "") {
    $ptagey = DateTime::createFromFormat('Y-m-d', $patient->Birth)
         ->diff(new DateTime('now', $tz))
         ->y;
    $ptagem = DateTime::createFromFormat('Y-m-d', $patient->Birth)
         ->diff(new DateTime('now', $tz))
         ->m;
    $ptaged = DateTime::createFromFormat('Y-m-d', $patient->Birth)
         ->diff(new DateTime('now', $tz))
         ->d;
}
$ptsex = $patient-sex == 1 ? "ชาย" : "หญิง";
$ptabogroup = iconv("tis-620", "utf-8", $patient->ABOGROUP );

$pthn=$patient->HN;


?>
<div class="row">
        <div class="col-md-8 col-sm-8 col-lg-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
				<h3 class="panel-title">ข้อมูลเบื้องต้น วันที่ : <?=$ptdupdate;?>		 					
              	</h3>
            </div>
			<div class="panel-body">
            <table width="100%" class="table table-striped  table-condensed">
              <tbody><tr>
                <td width="40%"><span class="style7">ชื่อ-สกุล : <?=$ptname;?> <?=$ptsurname;?></span></td>
                <td width="32%" colspan="2"><span class="style7">CID : <?=$ptcid;?></span></td>
                <td rowspan="4" align="center" bgcolor="#FFFFFF"><img src="https://service.thaicarecloud.org/modules/emr/temp/qrcode2ff09fcfe8d265d38ee53d1a98fe9587.png"></td>
              </tr>
              <tr>
                <td><span class="style7">วันเกิด : <?=$ptbirth;?> อายุ : <?=$ptagey;?> ปี <?=$ptagem;?> เดือน <?=$ptaged;?> วัน</span></td>
                <td><span class="style7">กรุ๊ปเลือด : <?=$ptabogroup;?></span></td>
                <td><span class="style7">เพศ : <?=$ptsex;?></span></td>
                </tr>
              <tr>
                <td><span class="style7">บ้านเลขที่ :
                  9C7C                  </span></td>
                <td colspan="2"><span class="style7">HN : <?=$pthn;?> </span></td>
                </tr>
              <tr>
                <td colspan="3">สถานะ <span class="label label-default label-lg">ตรวจแล้ว</span>
                 <a class="btn btn-success" data-toggle="modal" data-target="#modal-move-site2">แจ้งเตือนรายบุคคล</a>
                </td>
                </tr>
            </tbody></table>
          </div>
        </div>
        </div>
        <div class="col-md-4 col-sm-4 col-lg-4">
            <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6">
                    <div class="panel panel-primary">
                      <div class="panel-heading text-center">
                                          <h3 class="panel-title">CVD Risk score</h3>
                      </div>
                      <div class="panel-body text-center text-danger">
                        <span class="fa-stack fa-2x">
                          <i class="fa fa-circle  fa-stack-2x"></i>
                          <i class="fa fa-inverse fa-stack-1x">5.4</i>
                        </span>      
                      </div>
                    </div>
            </div>   
            <div class="col-md-6 col-sm-6 col-lg-6">
                      <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                                            <h3 class="panel-title">CKD</h3>
                        </div>
                                    <div class="panel-body text-center text-danger">
                            <span class="fa-stack fa-2x">
                              <i class="fa fa-circle  fa-stack-2x"></i>
                              <i class="fa fa-inverse fa-stack-1x">5</i>
                            </span>
                      </div>
                    </div>
            </div>                 
            </div>
            <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6">
                      <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                                            <h3 class="panel-title">CVA</h3>
                        </div>
                                    <div class="panel-body text-center text-danger">
                            <i class="fa fa-check fa-3x" aria-hidden="true"></i> 20/03/2559
                      </div>
                    </div>
            </div>        
            <div class="col-md-6 col-sm-6 col-lg-6">
                      <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                                            <h3 class="panel-title">MI</h3>
                        </div>
                                    <div class="panel-body text-center text-danger">
                            <i class="fa fa-check fa-3x" aria-hidden="true"></i>  12/05/2559
                      </div>
                    </div>
            </div>                 
            </div>            
    

        </div>
    </div>       
    <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-6">
<?php
$crdata = ArrayHelper::map($creatinine, 'SEQ', 'LABRESULT');

\yii\helpers\VarDumper::dump($crdata);
echo Highcharts::widget([
   'options' => [
      'title' => ['text' => 'Creatinine'],
      'xAxis' => [
         'categories' => ['Apples', 'Bananas', 'Oranges']
      ],
      'yAxis' => [
         'title' => ['text' => 'Fruit eaten']
      ],
      'series' => [
         ['name' => 'Jane', 'data' => $crdata],
         //['name' => 'John', 'data' => [5, 7, 3]]
      ]
   ]
]);
?>
            <?=Html::img('../img/ckd_egfr.png',['class'=>'img-rounded img-responsive']);?>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6">
            <?=Html::img('../img/ckd_ckp.png',['class'=>'img-rounded img-responsive']);?>
        </div>
    </div>
    
    <div class="row"> 
        <div class="col-md-6 col-sm-6 col-lg-6">
            <div class="panel panel-primary">
              <!-- Default panel contents -->
              <div class="panel-heading">รายการยา</div>
              <div class="panel-body">
                <p>   
                      **11069 เสลภูมิ 8-6-2558**<br>
            INFLUENZA VACCINE INJ. 0.5 DOSE1Vial [iv.5a(ฉีด 0.5 amp) ฉีดเข้าเส้นเลือด (iv)]	<br>
            <br>
              **11069 เสลภูมิ 5-1-2554**<br>
            BICOBON TAB(NED) mg.20TAB [2 tab tid-pc รับประทานครั้งละ 2 เม็ด]	<br>
            HYOSCINE ( C ) 10 mg.10TAB [1 tab tid-pc รับประทาน ครั้งละ 1 เม็ด]	<br>
            DOMPERIDONE 10 mg.10TAB [1 tab tid-ac รับประทาน ครั้งละ 1เม็ด]	<br>
            metoCLOPRAMIDE INJ. (BavL) 10 mg/2ml1INJ. [im1amp(1 amp/vial  )]	<br>
            ORS ผู้ใหญ่ 6.975 g.5ซอง [1 sac แทนน้ำ 1 แก้ว ใช้ 1ซอง ผสมน้ำต้มสุกที่เย็นแล้ว 1 แก้ว]	<br>
            <br>
              **11069 เสลภูมิ 9-4-2553**<br>
            METHYL SALICYLATE 30 g.2CREAM [นวด bid นวดบริเวณที่ปวดเมื่อย]	<br>
            GEMFIBROZIL   (C) 300 mg.180CAP [12at (1 เม็ด * 2 AC) รับประทาน ครั้งละ  1   เม็ด]	<br>
            CLOBETASOL  ACETONIDE 0.05% CREAM -2CREAM [ทา bid ทาบางๆบริเวณที่เป็น]	<br>
            gliBenClaMide  (CL) 5 mg.90TAB [11at(1 เม็ด* 1AC เช้า)]	<br>
            METFORMIN    (Bn/a) 500 mg.180TAB [12pt (1 เม็ด * 2 PC) รับประทาน ครั้งละ     1     เม็ด]	<br>
            <br>
              **11069 เสลภูมิ 7-1-2552**<br>
            GEMFIBROZIL   (C) 300 mg.180CAP [12at (1 เม็ด * 2 AC) รับประทาน ครั้งละ  1   เม็ด]	<br>
            gliBenClaMide  (CL) 5 mg.90TAB [11at(1 เม็ด* 1AC เช้า)]	<br>
            METFORMIN    (Bn/a) 500 mg.90TAB [11pt อ รับประทานครั้งละ 1 เม็ด วันละ 1 ครั้ง]	<br>
            <br>
              **11069 เสลภูมิ 8-8-2551**<br>
            METFORMIN    (Bn/a) 500 mg.90TAB [11pt อ รับประทานครั้งละ 1 เม็ด วันละ 1 ครั้ง]	<br>
            ANTACID SUSP 240 ml.2ขวด [23pj(2 ชต*3 PC)  รับประทานครั้งละ 2 ช้อนโต๊ะ]	<br>
            gliBenClaMide  (CL) 5 mg.90TAB [11at(1 เม็ด* 1AC เช้า)]	<br>
            VIT B 1-6-12(NED) mg.180TAB [1 tab bid-pc รับประทาน ครั้งละ 1 เม็ด]	<br>
            PARACETAMOL (B) 500 mg.30tab [2 tab prn pain  รับประทานครั้งละ 2 เม็ด]	<br>
            GEMFIBROZIL   (C) 300 mg.360CAP [2 tab bid-ac รับประทานครั้งละ 2 เม็ด]	<br>
            METHYL SALICYLATE 30 g.2CREAM [นวด bid นวดบริเวณที่ปวดเมื่อย]	<br>
            M. CARMINATIVE 180 ML 180 ml.2SOL. [1 ชต tid-pc รับประทานครั้งละ 1 ช้อนโต๊ะ]	<br>
            <br>
                    </p>
              </div>
              </div>      
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6">
            <div class="panel panel-primary">
              <div class="panel-heading"> <h3 class="panel-title">eGFR</h3></div>
              <div class="panel-body">
            
            <table width="100%" class="table-bordered table">
                <tbody><tr>
                  <td>ครั้งที่</td>
                  <td>รหัส</td>
                  <td>วันที่</td>
                  <td>GFR</td>
                  <td>Stage</td>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>11069</td>
                  <td>29/03/2559</td>
                  <td>54.51100</td>
                  <td bgcolor="
							#FFFF00">3.00000</td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>11069</td>
                  <td>03/03/2558</td>
                  <td>89.87500</td>
                  <td bgcolor="
							#99FF00">2.00000</td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>11069</td>
                  <td>03/02/2558</td>
                  <td>60.47300</td>
                  <td bgcolor="
							#99FF00">2.00000</td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>11069</td>
                  <td>28/12/2555</td>
                  <td>76.45300</td>
                  <td bgcolor="
							#99FF00">2.00000</td>
                </tr>
                <tr>
                  <td>5.</td>
                  <td>11069</td>
                  <td>30/09/2554</td>
                  <td>87.45100</td>
                  <td bgcolor="
							#99FF00">2.00000</td>
                </tr>
                <tr>
                  <td>6.</td>
                  <td>11069</td>
                  <td>02/07/2553</td>
                  <td>69.09600</td>
                  <td bgcolor="
							#99FF00">2.00000</td>
                </tr>
                <tr>
                  <td>7.</td>
                  <td>11069</td>
                  <td>15/09/2552</td>
                  <td>98.33900</td>
                  <td bgcolor="
							#00CC00">1.00000</td>
                </tr>
                <tr>
                  <td>8.</td>
                  <td>11069</td>
                  <td>12/05/2552</td>
                  <td>93.08700</td>
                  <td bgcolor="
							#00CC00">1.00000</td>
                </tr>
                <tr>
                  <td>9.</td>
                  <td>11069</td>
                  <td>15/02/2551</td>
                  <td>79.18600</td>
                  <td bgcolor="
							#99FF00"></td>
                </tr>
                <tr>
                  <td>10.</td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td bgcolor="
							#FFFFFF"></td>
                </tr>
              </tbody></table>
              </div>
            </div>

            <div class="panel panel-primary">
              <div class="panel-heading"> <h3 class="panel-title">ประวัติการรักษาล่าสุด</h3></div>
              <div class="panel-body">
                <table width="100%" class="table table-striped  table-condensed">
                  <tbody><tr>
                    <td colspan="2"><span class="style6 style7">t : </span><span class="style7"><strong>36.500</strong> bp :</span> <span class="style7"><strong>128.000</strong>/<strong>76.000</strong> pulse :</span> <span class="style7"><strong>0.000</strong> rr :</span> <span class="style7"><strong>0.000</strong> bw : <strong>50.000</strong> height :</span> <span class="style7"><strong>175</strong> bmi : <strong>16.327</strong></span></td>
                  </tr>
                  <tr>
                    <td colspan="2"><span class="style6 style7">cc  :</span> <span class="style7"><strong>ได้รับการฉีดวัคซีนไข้หวัดใหญ่ 2559</strong> </span></td>
                  </tr>
                  <tr>
                    <td width="35%"><span class="style7">เป็นมา : 0:วัน </span></td>
                    <td><span class="style7">เริ่มเป็นวันที่:
                                        </span></td>
                  </tr>
                </tbody></table>
                <span class="label label-default">ลงผลการวินิจฉัย</span>
                <table width="100%" class="table-condensed">
                  <tbody><tr>
                    <td>pdx : Z251 : Need for immunization against influenza<hr>                  dx0 : E119 : Non-insulin-dependent diabetes mellitus, without complications<br>                  dx1 : Z133 : Special screening examination for mental and behavioural disorders<br>                                                                        </td>
                  </tr>
                  <tr>
                    <td>                                                                                          </td>
                  </tr>
                </tbody></table>

              </div>
            </div>
        </div>               
    </div>
<?php
        Modal::begin([
            'id' => 'modal-move-site2',
            'header' => '<a class="btn btn-danger" href="/ckd/inv">สิ่งแจ้งเตือนรายบุคคล</a>',
            'closeButton' => ['label' => 'ปิด','class' => 'btn btn-success btn-sm pull-right'],
            'options' => ['class' => 'modal-wide'],
        ]);
?>
<table class="table table-bordered table-striped">
<tbody>
<tr>
<td width="71">#</td>
<td width="868">สิ่งแจ้งเตือนรายบุคคล</td>
<td width="71">&nbsp;</td>
</tr>
<tr>
<td width="71">1</td>
<td width="868">BP &le; 140/90 mmHg</td>
<td><i class="fa fa-check"></i></td>
</tr>
<tr>
<td width="71">2</td>
<td width="868">ผู้ป่วยได้รับ ACEI /ARBs</td>
<td><i class="fa fa-check"></i></td>
</tr>
<tr>
<td width="71">3</td>
<td width="868">ผู้ป่วยมีค่า Slope ของ eGFR ต่อระยะเวลา &ge; 0</td>
<td><i class="fa fa-check"></i></td>
</tr>
<tr>
<td width="71">4</td>
<td width="868">Hb&nbsp; &ge; 10 g/dl</td>
<td><i class="fa fa-close"></i></td>
</tr>
<tr>
<td width="71">5</td>
<td width="868">HbA1C&nbsp; &lt; 7 %</td>
<td><i class="fa fa-check"></i></td>
</tr>
<tr>
<td width="71">6</td>
<td width="868">LDL cholesterol &lt; 100 mg/dl</td>
<td><i class="fa fa-close"></i></td>
</tr>
<tr>
<td width="71">7</td>
<td width="868">ผู้ป่วยมีค่า serum potassium &lt; 5.5 mEq/L</td>
<td><i class="fa fa-check"></i></td>
</tr>
<tr>
<td width="71">8</td>
<td width="868">ผู้ป่วยมีค่า serum bicarbonate &gt; 22 mEq/L</td>
<td><i class="fa fa-close"></td>
</tr>
<tr>
<td width="71">9</td>
<td width="868">ผู้ป่วยได้รับการประเมิน Urine Protien Strip&nbsp; (รพ.ระดับ F-M)</td>
<td><i class="fa fa-close"></td>
</tr>
<tr>
<td width="71">10</td>
<td width="868">ผู้ป่วยได้รับการประเมิน urine protein creatinine ratio (UPCR) หรือ urine protein 24 hrs</td>
<td><i class="fa fa-close"></td>
</tr>
<tr>
<td width="71">11</td>
<td width="868">UPCR &lt; 500 mg/g&nbsp; หรือ Urine&nbsp; protein 24 hrs &lt; 500 mg/day</td>
<td><i class="fa fa-close"></td>
</tr>
<tr>
<td width="71">12</td>
<td width="868">Serum phosphate &lt; 4.5 mg/L</td>
<td><i class="fa fa-close"></td>
</tr>
<tr>
<td width="71">13</td>
<td width="868">Serum parathyroid hormone (PTH)&nbsp; อยู่ในเกณฑ์ปกติ</td>
<td><i class="fa fa-close"></td>
</tr>
<tr>
<td width="71">14</td>
<td width="868">ผู้ป่วยได้รับการเตรียม AVF พร้อม ก่อนเริ่ม hemodialysis</td>
<td><i class="fa fa-close"></td>
</tr>
<tr>
<td width="71">15</td>
<td width="868">ผู้ป่วยได้เข้าร่วม educational class ในหัวข้อต่างๆครบ</td>
<td><i class="fa fa-close"></td>
</tr>
<tr>
<td width="71">16</td>
<td width="868">แจ้งค่า eGFR และระยะของ CKD&nbsp; ครั้งนี้ และภายใน 3 เดือนที่ผ่านมา</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">17</td>
<td width="868">แจ้งค่า eGFR &lt; 60 ควรพบอายุรแพทย์</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">18</td>
<td width="868">ผู้ป่วยที่มีอัตราการลดลงของ GFR มากกว่าปีละ&nbsp; 5 ml/min/1.73 m2&nbsp;</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">19</td>
<td width="868">ผู้ป่วยที่ eGFR &lt; 30 ได้รับการให้คำปรึกษาเรื่อง RRT</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">20</td>
<td>ผู้ป่วยที่ eGFR &lt; 30 ยังได้รับยา metformin</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">21</td>
<td width="868">ผู้ป่วยเบาหวานที่มี albuminuria &gt; 30 mg/day และไม่ได้รับยา ACEI หรือ ARB</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">22</td>
<td width="868">ผู้ป่วยที่ไม่ได้เป็นเบาหวานมี albuminuria &gt; 300 mg/day และไม่ได้รับยา ACEI หรือ ARB</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">23</td>
<td width="868">ได้รับ ACEi/ARB&nbsp; ระวังการเกิด AKI และ hyperkalemia ควรได้รับคำแนะนำการปฏิบัติตัว</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">24</td>
<td width="868">ผู้ป่วยเคยได้รับยา ACEI หรือ ARB แต่ต้องหยุดยาเพราะมี adverse event</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">25</td>
<td width="868">ผู้ป่วยที่มี ปริมาณโปรตีนในปัสสาวะ &ge; 1+</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">26</td>
<td width="868">ผู้ป่วยเบาหวานที่ไม่ได้ตรวจ urine albumin อย่างน้อย 2 ครั้งต่อปี</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">27</td>
<td width="868">ผู้ป่วยที่ไม่ได้เป็น CKD และได้รับยา NSAIDs นานกว่า 2 สัปดาห์</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">28</td>
<td width="868">ผู้ป่วย CKD ที่ได้รับยา NSAIDs&nbsp;</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">29</td>
<td width="868">เวลาสั่งจ่ายยาในโปรแกรม ถ้ามียาที่ต้องปรับตาม CrCl ถ้าสั่งผิดให้มีการเตือนทุกครั้ง หรือถ้าไม่มีการปรับตาม CrCl ให้เตือนทุกครั้ง</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">30</td>
<td width="868">&nbsp;ประวัติการเคยวินิจฉัยว่ามี acute kidney injury ล่าสุด วัน เดือน ปี เท่าไร&nbsp;</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">31</td>
<td width="868">ผู้ป่วยที่มีโรคหรือภาวะที่เสี่ยงต่อการเกิด CKD ได้รับการตรวจ serum creatinine และ urine protein หรือ albumin ปีละ 1 ครั้ง (เบาหวาน, ความดันโลหิตสูง, เก๊าท์, SLE, อายุมากกว่า 60 ปี, ได้รับยาที่ nephrotoxic, ติดเชื้อทางเดินปัสสาวะส่วนบน &ge; 3 ครั้งต่อปี, มีโรคหัวใจและหลอดเลือด, polycystic kidney disease, โรคไตตั้งแต่กำเนิด, มีประวัติโรคไตในครอบครัว)</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">32</td>
<td width="868">ตรวจ UA พบโปรตีนหรือหรือเม็ดเลือดแดง ในปัสสาวะพิจารณาส่งปรึกษาแพทย์</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">33</td>
<td width="868">Serum creatinine เพิ่มขึ้นมากกว่าหรือเท่ากับ 0.3 mg/dL: acute kidney injury ควรหาสาเหตุ</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">34</td>
<td width="868">GFR &lt; 30 ml/min/1.73m<sup>2 </sup>พิจารณาส่งปรึกษาอายุรแพทย์โรคไต</td>
<td><i class="fa fa-check"></td>
</tr>
<tr>
<td width="71">35</td>
<td>GFR &lt; 15 ml/min/1.73m2 ควรส่งประเมินเตรียมความพร้อมในการทำบำบัดทดแทนไต&nbsp;</td>
<td><i class="fa fa-check"></td>
</tr>
</tbody>
</table>
<?php
    Modal::end();
?>