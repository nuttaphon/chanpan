<?php

namespace backend\modules\ckd\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public $activetab="home";
    
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionRedirect($tab) {
        return $this->redirect('/ckd/'.$tab,302);
    }    
}
