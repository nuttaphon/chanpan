<?php

namespace backend\modules\ckd\controllers;

use Yii;
use backend\modules\ckd\models\FPersonSearch;

class InvController extends \yii\web\Controller
{
    public $activetab="inv";
    
    public function actionIndex()
    {
        $state = isset($_GET['state']) ? $_GET['state'] : 0;
	
	if (isset($_COOKIE['save_keyckd']) && $_COOKIE['save_keyckd']==1) {
	    if (isset($_COOKIE['key_ckd'])) {
		Yii::$app->session['key_ckd'] = $_COOKIE['key_ckd'];
	    }
	    if (isset($_COOKIE['convert_ckd'])) {
		Yii::$app->session['convert_ckd'] = $_COOKIE['convert_ckd'];
	    }
	    if (isset($_COOKIE['save_keyckd'])) {
		Yii::$app->session['save_keyckd'] = $_COOKIE['save_keyckd'];
	    }
	} else {
	    Yii::$app->session['key_ckd'] = '';
	    Yii::$app->session['convert_ckd'] = 0;
	    Yii::$app->session['save_keyckd'] = 0;
	    
	    setcookie('save_keyckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    setcookie('key_ckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	    setcookie('convert_ckd', null, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
	}
	
	
	    if (isset($_GET['save_keyckd']) && $_GET['save_keyckd']==1) {
		setcookie("save_keyckd", $_GET['save_keyckd'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		setcookie("key_ckd", $_GET['key'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		setcookie("convert_ckd", $_GET['convert'], time()+3600*24*30, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
		
		
	    } 
	
	if(isset($_GET['key'])){    
	    Yii::$app->session['key_ckd'] = $_GET['key'];
	    Yii::$app->session['convert_ckd'] = $_GET['convert'];
	    Yii::$app->session['save_keyckd'] = $_GET['save_keyckd'];
	} 
	
	$convert = isset(Yii::$app->session['convert_ckd']) ? Yii::$app->session['convert_ckd'] : 0;
	$key = isset(Yii::$app->session['key_ckd']) ?Yii::$app->session['key_ckd'] : '';
	$save_keyckd = isset(Yii::$app->session['save_keyckd']) ?Yii::$app->session['save_keyckd'] : 0;
	
	if(isset($_GET['save_keyckd'])){
	    
	}
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;
        
        $ckdDiagram = \backend\modules\ckdnet\classes\CkdnetFunc::getCkdDiagram($sitecode, '', '');
        $dataList = \yii\helpers\ArrayHelper::map($ckdDiagram, 'id', 'label');
         
	$searchModel = new FPersonSearch();
	$dataProvider = $searchModel->searchState(Yii::$app->request->queryParams, $sitecode, '', '', $state);
	
	return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'items' => $items,
		    'state' => $state,
		    'dataList' => $dataList,
	]);
    }

}
