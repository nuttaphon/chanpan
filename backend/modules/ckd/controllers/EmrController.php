<?php

namespace backend\modules\ckd\controllers;
use app\models\FPerson;
use \app\models\FLabfu;
use Yii;
use yii\helpers\Json;

class EmrController extends \yii\web\Controller
{
    public $activetab="emr";
    
    public function actionIndex($ptlink='7912ec3925546dada0e6b8d1ddad76d6',$cid='')
    {   
        if ($cid != "") {
            $ptlink = md5($cid);
        }
        //$hospcode='11065';
        $hospcode = Yii::$app->user->identity->userProfile->sitecode;
        
        $patient = FPerson::findOne(['ptlink'=>$ptlink, 'hospcode'=>$hospcode]);
        $creatinine = FLabfu::findAll(['ptlink'=>$ptlink,'LABTEST'=>'11']);
        //\yii\helpers\VarDumper::dump($patient);exit;
        return $this->render('index',[
            'hospcode'      => $hospcode,
            'patient'       =>  $patient,
            'creatinine'    =>  $creatinine,
            ]);
    }
    public function actionCkd($ptlink,$hospcode)
    {
        $patient = FPerson::findOne(['ptlink'=>$ptlink, 'hospcode'=>$hospcode]);
        $creatinine = FLabfu::findAll(['ptlink'=>$ptlink,'LABTEST'=>'11']);
       //\yii\helpers\VarDumper::dump($patient,10,true);exit;
        return Json::encode($this->renderAjax('ckd',[
            'hospcode'      => $hospcode,
            'patient'       =>  $patient,
            'creatinine'    =>  $creatinine,
            ]));
    }
    public function actionPhr($ptlink,$hospcode)
    {
        $patient = FPerson::findOne(['ptlink'=>$ptlink, 'hospcode'=>$hospcode]);
        
        return Json::encode($this->renderAjax('phr',[
            'hospcode'      => $hospcode,
            'patient'       =>  $patient,
            'creatinine'    =>  $creatinine,
            ]));
    }    
}
