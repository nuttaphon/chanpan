<?php

namespace backend\modules\ckd\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ckd\models\FPerson;
use backend\modules\ckdnet\classes\CkdnetQuery;
use backend\modules\ckdnet\classes\CkdnetQueryRight;

/**
 * FPersonSearch represents the model behind the search form about `backend\modules\ckdnet\models\FPerson`.
 */
class FPersonSearch extends FPerson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'HOSPCODE', 'CID', 'PID', 'HID', 'PreName', 'Pname', 'Name', 'Lname', 'HN', 'sex', 'Birth', 'Mstatus', 'Occupation_Old', 'Occupation_New', 'Race', 'Nation', 'Religion', 'Education', 'Fstatus', 'Father', 'Mother', 'Couple', 'Vstatus', 'MoveIn', 'Discharge', 'Ddischarge', 'ABOGROUP', 'RHGROUP', 'Labor', 'PassPort', 'TypeArea', 'D_Update', 'ptlink'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $sitecode, $key, $convert, $state)
    {
        $query = FPerson::find()
		->where('HOSPCODE=:sitecode', [':sitecode'=>$sitecode]);

	if($key!=''){
	    $selectList = [
			'f_person.sitecode',
			'f_person.HOSPCODE',
			'f_person.PID',
			'f_person.HID',
			'f_person.PreName',
			'f_person.sex',
			'f_person.Birth',
			'f_person.Mstatus',
			'f_person.Occupation_Old',
			'f_person.Occupation_New',
			'f_person.Race',
			'f_person.Nation',
			'f_person.Religion',
			'f_person.Education',
			'f_person.Fstatus',
			'f_person.Couple',
			'f_person.Vstatus',
			'f_person.MoveIn',
			'f_person.Discharge',
			'f_person.Ddischarge',
			'f_person.ABOGROUP',
			'f_person.RHGROUP',
			'f_person.Labor',
			'f_person.PassPort',
			'f_person.TypeArea',
			'f_person.D_Update',
//			'(SELECT ckd.lastegfr FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS lastegfr',
//			'(SELECT ckd.cvdrisk FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS cvdrisk',
//			'(SELECT ckd.cva FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS cva',
//			'(SELECT ckd.urineprotine FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS urineprotine',
			'f_person.ptlink'
		    ];
	    
	    $selectKey = [
		"decode(unhex(f_person.Father),sha2('$key',256)) AS Father",
		"decode(unhex(f_person.Mother),sha2('$key',256)) AS Mother",
		"decode(unhex(f_person.CID),sha2('$key',256)) AS CID",
		"decode(unhex(f_person.HN),sha2('$key',256)) AS HN",
		"decode(unhex(f_person.Pname),sha2('$key',256)) AS Pname",
		"decode(unhex(f_person.`Name`),sha2('$key',256)) AS `Name`",
		"decode(unhex(f_person.Lname),sha2('$key',256)) AS Lname"
	    ];
	    
	    if ($convert==1) {
		$selectKey = [
		    "convert(decode(unhex(f_person.Father),sha2('$key',256)) using tis620) AS Father",
		    "convert(decode(unhex(f_person.Mother),sha2('$key',256)) using tis620) AS Mother",
		    "convert(decode(unhex(f_person.CID),sha2('$key',256)) using tis620) AS CID",
		    "convert(decode(unhex(f_person.HN),sha2('$key',256)) using tis620) AS HN",
		    "convert(decode(unhex(f_person.Pname),sha2('$key',256)) using tis620) AS Pname",
		    "convert(decode(unhex(f_person.`Name`),sha2('$key',256)) using tis620) AS `Name`",
		    "convert(decode(unhex(f_person.Lname),sha2('$key',256)) using tis620) AS Lname"
		];
	    }
	    
	    $selectList = array_merge($selectList, $selectKey);
	    
	    $query->select($selectList);
	} else {
	    $selectList = [
//		'(SELECT ckd.lastegfr FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS lastegfr',
//		'(SELECT ckd.cvdrisk FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS cvdrisk',
//		'(SELECT ckd.cva FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS cva',
//		'(SELECT ckd.urineprotine FROM ckd_patient_central ckd WHERE ckd.ptlink = f_person.ptlink AND ckd.hospcode = f_person.HOSPCODE) AS urineprotine',
		'f_person.*'
	    ];
	    $query->select($selectList);
	}

	$dataProvider = new ActiveDataProvider([
            'query' => $query,
	    'pagination' => [
		'pageSize' => 100,
	    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Birth' => $this->Birth,
            'MoveIn' => $this->MoveIn,
            'Ddischarge' => $this->Ddischarge,
            'D_Update' => $this->D_Update,
        ]);

        $query->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'HOSPCODE', $this->HOSPCODE])
            ->andFilterWhere(['like', 'PID', $this->PID])
            ->andFilterWhere(['like', 'HID', $this->HID])
            ->andFilterWhere(['like', 'PreName', $this->PreName])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'Mstatus', $this->Mstatus])
            ->andFilterWhere(['like', 'Occupation_Old', $this->Occupation_Old])
            ->andFilterWhere(['like', 'Occupation_New', $this->Occupation_New])
            ->andFilterWhere(['like', 'Race', $this->Race])
            ->andFilterWhere(['like', 'Nation', $this->Nation])
            ->andFilterWhere(['like', 'Religion', $this->Religion])
            ->andFilterWhere(['like', 'Education', $this->Education])
            ->andFilterWhere(['like', 'Fstatus', $this->Fstatus])
            ->andFilterWhere(['like', 'Couple', $this->Couple])
            ->andFilterWhere(['like', 'Vstatus', $this->Vstatus])
            ->andFilterWhere(['like', 'Discharge', $this->Discharge])
            ->andFilterWhere(['like', 'ABOGROUP', $this->ABOGROUP])
            ->andFilterWhere(['like', 'RHGROUP', $this->RHGROUP])
            ->andFilterWhere(['like', 'Labor', $this->Labor])
            ->andFilterWhere(['like', 'PassPort', $this->PassPort])
            ->andFilterWhere(['like', 'TypeArea', $this->TypeArea])
            ->andFilterWhere(['like', 'ptlink', $this->ptlink]);

	if ($convert==1) {
	    $query->andFilterWhere(['like', "convert(decode(unhex(f_person.CID),sha2('$key',256)) using tis620)", $this->CID])
		    ->andFilterWhere(['like', "convert(decode(unhex(f_person.Pname),sha2('$key',256)) using tis620)", $this->Pname])
		    ->andFilterWhere(['like', "convert(decode(unhex(f_person.Name),sha2('$key',256)) using tis620)", $this->Name])
		    ->andFilterWhere(['like', "convert(decode(unhex(f_person.Lname),sha2('$key',256)) using tis620)", $this->Lname])
		    ->andFilterWhere(['like', "convert(decode(unhex(f_person.HN),sha2('$key',256)) using tis620)", $this->HN])
		    ->andFilterWhere(['like', "convert(decode(unhex(f_person.Father),sha2('$key',256)) using tis620)", $this->Father])
		    ->andFilterWhere(['like', "convert(decode(unhex(f_person.Mother),sha2('$key',256)) using tis620)", $this->Mother]);
	} else {
	    $query->andFilterWhere(['like', "decode(unhex(f_person.CID),sha2('$key',256))", $this->CID])
		    ->andFilterWhere(['like', "decode(unhex(f_person.Pname),sha2('$key',256))", $this->Pname])
		    ->andFilterWhere(['like', "decode(unhex(f_person.Name),sha2('$key',256))", $this->Name])
		    ->andFilterWhere(['like', "decode(unhex(f_person.Lname),sha2('$key',256))", $this->Lname])
		    ->andFilterWhere(['like', "decode(unhex(f_person.HN),sha2('$key',256))", $this->HN])
		    ->andFilterWhere(['like', "decode(unhex(f_person.Father),sha2('$key',256))", $this->Father])
		    ->andFilterWhere(['like', "decode(unhex(f_person.Mother),sha2('$key',256))", $this->Mother]);
	}
	
        return $dataProvider;
    }
    
    public function searchState($params, $sitecode, $sdate, $edate, $state)
    {
        $sqlObj = [];
        
        if($state==1){
            $sqlObj = CkdnetQuery::getTotalPop($sitecode, $sdate, $edate, TRUE);
        } elseif($state==13){
            $sqlObj = CkdnetQueryRight::getCkdNonDMHT($sitecode, $sdate, $edate, TRUE);
        } elseif($state==14){
            $sqlObj = CkdnetQueryRight::getCkdNonDMHTReportedCKD($sitecode, $sdate, $edate, TRUE);
        } elseif($state==15){
            $sqlObj = CkdnetQueryRight::getNonDMHTCkdConfirm($sitecode, $sdate, $edate, TRUE);
        } elseif($state==2){
            $sqlObj = CkdnetQuery::getDmHt($sitecode, $sdate, $edate, TRUE);
        } elseif($state==3){
            $sqlObj = CkdnetQueryRight::getCkdReport($sitecode, $sdate, $edate, TRUE);
        } elseif($state==16){
            $sqlObj = CkdnetQueryRight::getCkdReportConfirm($sitecode, $sdate, $edate, TRUE);
        } elseif($state==4){
            $sqlObj = CkdnetQueryRight::getCkdTargetScreening($sitecode, $sdate, $edate, TRUE);
        } elseif($state==5){
            $sqlObj = CkdnetQueryRight::getCkdScreened($sitecode, $sdate, $edate, TRUE);
        } elseif($state==6){
            $sqlObj = CkdnetQueryRight::getCkdScreenedNotCkd($sitecode, $sdate, $edate, TRUE);
        } elseif($state==7){
            $sqlObj = CkdnetQueryRight::getCkdScreenedWaitVer($sitecode, $sdate, $edate, TRUE);
        } elseif($state==8){
            $sqlObj = CkdnetQueryRight::getCkdScreenedConfirmCkd($sitecode, $sdate, $edate, TRUE);
        } elseif($state==9){
            $sqlObj = CkdnetQueryRight::getCkdNotScreened($sitecode, $sdate, $edate, TRUE);
        } elseif($state==10){
            $sqlObj = CkdnetQueryRight::getCkdNotScreenedNotCkd($sitecode, $sdate, $edate, TRUE);
        } elseif($state==11){
            $sqlObj = CkdnetQueryRight::getCkdNotScreenedWaitVer($sitecode, $sdate, $edate, TRUE);
        } elseif($state==12){
            $sqlObj = CkdnetQueryRight::getCkdNotScreenedConfirmCkd($sitecode, $sdate, $edate, TRUE);
        } elseif($state==17){
            $sqlObj = CkdnetQueryRight::getCkdTotalConfirmCKD($sitecode, $sdate, $edate, TRUE);
        } elseif($state==18){
            $sqlObj = CkdnetQueryRight::getCkdTotalAllConfirmCKD($sitecode, $sdate, $edate, TRUE);
        } else {
            $sqlObj = CkdnetQuery::getAll($sitecode, $sdate, $edate, TRUE);
        }

        $db = \backend\modules\ckdnet\classes\CkdnetFunc::getDb();
        //\appxq\sdii\utils\VarDumper::dump($sqlObj);
        
	$dataProvider = new \yii\data\SqlDataProvider([
            'key' => $sqlObj['keyID'],
            'sql' => $sqlObj['sql'],
            'params' => $sqlObj['param'],
            'totalCount' => $sqlObj['total'],
            'db' => $db,
	    'pagination' => [
		'pageSize' => 100,
	    ],
        ]);

        $this->load($params);

	
        return $dataProvider;
    }
}
