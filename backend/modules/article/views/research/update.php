<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Research */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Research',
]) . ' ' . $model->rid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Researches'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rid, 'url' => ['view', 'id' => $model->rid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="research-update">

    <?= $this->render('_form', [
        'model' => $model,
		'modelUi' => $modelUi,
    ]) ?>

</div>
