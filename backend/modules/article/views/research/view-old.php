<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Research */

$this->title = 'Research#'.$model->rid;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Researches'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="research-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'rid',
		'res_topic:ntext',
		'projid',
		'author',
		'author_email:email',
		'co_auther:ntext',
		'co_auther_email:ntext',
		'contact_name',
		'contact_address:ntext',
		'contact_tel',
		'contact_fax',
		'contact_email:email',
		'mabstract:ntext',
		'mmanuscript:ntext',
		'expected_completion',
		'status',
		'updated_at',
		'updated_by',
		'created_at',
		'created_by',
	    ],
	]) ?>
    </div>
</div>
