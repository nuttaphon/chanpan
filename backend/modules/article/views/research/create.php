<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Research */

$this->title = Yii::t('app', 'Create Research');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Researches'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="research-create">

    <?= $this->render('_form', [
        'model' => $model,
		'modelUi' => $modelUi,
    ]) ?>

</div>
