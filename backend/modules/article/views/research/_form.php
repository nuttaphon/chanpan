<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\lib\sdii\components\helpers\SDNoty;
use yii\helpers\Url;
use backend\modules\article\components\ArticleQuery;
use yii\widgets\Pjax;
use common\lib\sdii\widgets\SDGridView;
use common\lib\sdii\widgets\SDModalForm;
use backend\modules\article\components\ArticleFunc;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Research */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="research-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName(), 'options' => ['enctype' => 'multipart/form-data']]); ?>

	<div class="modal-body">
		<div class="row">
			<div class="col-md-6 " style="position: relative;">
			    <div class="form-inline show-in-line">
				
				<?= $form->field($model, 'author_user')->checkbox() ?>
				<?= $form->field($model, 'author_public')->checkbox() ?>
			    </div>
			    <?= $form->field($model, 'res_topic')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-6 sdbox-col">
			    <?= $form->field($model, 'projid')->widget(kartik\select2\Select2::className(),[
				'initValueText' => $research, // set the initial display text
				'options' => ['placeholder' => 'Search for a Research ...', 'multiple' => true],
				'data' => ArticleQuery::getTree(),
				'pluginOptions' => [
					'tokenSeparators' => [',', ' '],
					'maximumInputLength' => 10
				],

			    ]) ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6 ">
			    <?php
			    //$cityDesc = empty($model->author) ? '' : City::findOne($model->city)->description;
			    ?>
			    <?= $form->field($model, 'author_id')->widget(kartik\select2\Select2::className(),[
				'initValueText' => $model->author, // set the initial display text
				'options' => ['placeholder' => 'ค้นหาผู้แต่ง'],
				'pluginOptions' => [
					'allowClear' => true,
					'minimumInputLength' => 1,
					'ajax' => [
					    'url' => \yii\helpers\Url::to(['author-list']),
					    'dataType' => 'json',
					    'data' => new JsExpression('function(params) { return {q:params.term}; }')
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(author) { return author.text; }'),
					'templateSelection' => new JsExpression('function (author) { return author.text; }'),
				],
				'pluginEvents' => [
				    "select2:select" => "function(e) { $('#research-author_email').val(e.params.data.email); $('#research-author').val(e.params.data.text); }",
				    "select2:unselect" => "function() { $('#research-author_email').val(''); $('#research-author_id').val(''); }"
				]
			    ]) ?>
			</div>
			<div class="col-md-6 sdbox-col"><?= $form->field($model, 'author_email')->textInput(['maxlength' => true]) ?></div>
		</div>
	    
	    <div class="form-inline">
		<label>เกี่ยวข้องกับ CCA หรือไม่</label> &nbsp; &nbsp;
		
		    <?= $form->field($model, 'cca_status')->radioList([1=>'เกี่ยวข้อง', 0=>'ไม่เกี่ยวข้อง'], ['style'=>'padding-top: 12px;'])->label(false) ?>
		
	    </div>
		
		<h4 class="page-header">ชื่อผู้แต่งร่วม</h4>
		<?php Pjax::begin(['id'=>'auther-form-grid-pjax']);?>
	    <?= SDGridView::widget([
			'id' => 'auther-form-grid',
			'panelBtn' => Html::button(Yii::t('app', '<span class="glyphicon glyphicon-plus"></span>'), ['data-url'=>Url::to(['research/create-tmp']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-auther-form']).' '.
					Html::button(Yii::t('app', '<span class="glyphicon glyphicon-repeat"></span>'), ['data-url'=>Url::to(['research/reset-tmp']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-resetbtn-auther-form']),
			'dataProvider' => $modelUi->autherTmp(),
			'columns' => [
				[
				'class' => 'yii\grid\SerialColumn',
				'headerOptions'=>['style'=>'text-align: center;'],
				'contentOptions'=>['style'=>'width:50px;text-align: center;'],
				],
				[
				'attribute'=>'co_auther',
				'label'=>'ชื่อผู้แต่งร่วม',
				'contentOptions'=>['style'=>'width:180px;'],
				],
				[
				'attribute'=>'co_auther_email',
				'label'=>'E-mail',
				//'contentOptions'=>['style'=>'width:140px;'],
				],
				[
				'class' => 'yii\grid\ActionColumn',
				'contentOptions'=>['style'=>'width:40px;text-align: center;'],
				'template'=>'{delete}',
				'buttons'=>[
					'delete' => function ($url, $data, $key) {
					return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['research/delete-tmp', 'id'=>$data['co_auther_email']]), [
						'data-action' => 'delete',
						'title' => Yii::t('yii', 'Delete'),
						'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
						'data-method' => 'post',
						'data-pjax' => isset($this->pjax_id)?$this->pjax_id:'0',
					]);
					}
				],

				],
			],  
	    ]);	    yii\grid\ActionColumn::className(); ?>
	    <?php  Pjax::end();?>
		
		<div  style="position: relative;">
		    <h4 class="page-header">Contact</h4>
		    <div class="form-inline show-in-line">
			
			<?= $form->field($model, 'contact_user')->checkbox() ?>
			<?= $form->field($model, 'contact_public')->checkbox() ?>
		    </div>
		</div>
		
		
		<?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'contact_address')->textarea(['rows' => 4]) ?>

		<div class="row">
			<div class="col-md-4 "><?= $form->field($model, 'contact_tel')->textInput(['maxlength' => true]) ?></div>
			<div class="col-md-4 sdbox-col"><?= $form->field($model, 'contact_fax')->textInput(['maxlength' => true]) ?></div>
			<div class="col-md-4 sdbox-col"><?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?></div>
		</div>
		<hr>
		<div class="row ">
			<div class="col-md-4 "><?= $form->field($model, 'expected_completion')->dropDownList(ArticleFunc::itemYear($model->expected_completion)) ?></div>
			
			<div class="col-md-4 sdbox-col"><?= $form->field($model, 'status')->dropDownList(ArticleFunc::itemAlias('status')) ?></div>
			<div class="col-md-4 sdbox-col">
			    <?= $form->field($model, 'icon')->fileInput(); ?>
			    <?php
			    $icon = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTAyMjljNTY3YSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MDIyOWM1NjdhIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=';
			    if($model->icon!=''){
				$icon = Yii::getAlias('@storageUrl') . '/source/images/'.$model->icon;
			    }
			    ?>
			    <img class="media-object" alt="64x64" src="<?=$icon?>" data-holder-rendered="true" style="width: 64px; height: 64px;">
			</div>
		</div>
		<div class="row ">
		    <div class="col-md-4 ">
			<?= $form->field($model, 'res_size')->textInput(['maxlength' => true]) ?>
		    </div>
		</div>
		
		<div class="row" style="position: relative;">
		<div class="show-in-line"><?= $form->field($model, 'mabstract_show')->checkbox() ?></div>
		<?= $form->field($model, 'mabstract')->widget(dosamigos\tinymce\TinyMce::className(),[
			'options' => ['rows' => 20],
			'language' => 'th_TH',
			'clientOptions' => [
				'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
				'plugins' => [
					"advlist autolink lists link image charmap print preview hr anchor pagebreak",
					"searchreplace wordcount visualblocks visualchars code fullscreen",
					"insertdatetime media nonbreaking save table contextmenu directionality",
					"emoticons template paste textcolor colorpicker textpattern",
				],
				'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
				'content_css' => Yii::getAlias('@backendUrl').'/css/bootstrap.min.css',
				'image_advtab' => true,
				'filemanager_crossdomain' => true,
				'external_filemanager_path' => Yii::getAlias('@storageUrl').'/filemanager/',
				'filemanager_title' => 'Responsive Filemanager',
				'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl').'/filemanager/plugin.min.js')
			]
		]) ?>
		</div>
		
		<div class="row" style="position: relative;">
			<div class="show-in-line"><?= $form->field($model, 'mmanuscript_show')->checkbox() ?></div>
			<?= $form->field($model, 'mmanuscript')->widget(dosamigos\tinymce\TinyMce::className(),[
			'options' => ['rows' => 20],
			'language' => 'th_TH',
			'clientOptions' => [
				'fontsize_formats' => '8pt 9pt 10pt 11pt 12pt 26pt 36pt',
				'plugins' => [
					"advlist autolink lists link image charmap print preview hr anchor pagebreak",
					"searchreplace wordcount visualblocks visualchars code fullscreen",
					"insertdatetime media nonbreaking save table contextmenu directionality",
					"emoticons template paste textcolor colorpicker textpattern",
				],
				'toolbar' => "undo redo | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
				'content_css' => Yii::getAlias('@backendUrl').'/css/bootstrap.min.css',
				'image_advtab' => true,
				'filemanager_crossdomain' => true,
				'external_filemanager_path' => Yii::getAlias('@storageUrl').'/filemanager/',
				'filemanager_title' => 'Responsive Filemanager',
				'external_plugins' => array('filemanager' => Yii::getAlias('@storageUrl').'/filemanager/plugin.min.js')
			]
		]) ?>
		</div>
		
		<?= $form->field($model, 'manuscript_file')->fileInput(); ?>
		<?php
		if($model->manuscript_file!=''){
		    $url = Yii::getAlias('@storageUrl') . '/source/'.$model->manuscript_file;
		    echo Html::a('Download manuscript file', $url);
		}
		?>
		<br><br>
		<?= $form->field($model, 'abstract_file')->fileInput(); ?>
		<?php
		if($model->abstract_file!=''){
		    $url = Yii::getAlias('@storageUrl') . '/source/'.$model->abstract_file;
		    echo Html::a('Download abstract file', $url);
		}
		?>
		
		<?= Html::activeHiddenInput($model, 'show') ?>
		<?= Html::activeHiddenInput($model, 'author') ?>
		<?= Html::activeHiddenInput($model, 'co_auther_id') ?>
		<?= Html::activeHiddenInput($model, 'co_auther') ?>
		<?= Html::activeHiddenInput($model, 'co_auther_email') ?>
		<?= Html::activeHiddenInput($model, 'updated_at') ?>
		<?= Html::activeHiddenInput($model, 'updated_by') ?>
		<?= Html::activeHiddenInput($model, 'created_at') ?>
		<?= Html::activeHiddenInput($model, 'created_by') ?>
		<?= Html::activeHiddenInput($model, 'rid') ?>

	</div>
	<div class="modal-footer">
	    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>

    <?php ActiveForm::end(); ?>

</div>
<?=  SDModalForm::widget([
    'id' => 'modal-auther-form',
    'size'=>'modal-sm',
]);
?>

<?php  $this->registerJs("
$('body').tooltip('destroy');



$('#auther-form-grid-pjax').on('click', '#modal-resetbtn-auther-form', function(){
    var url = $(this).attr('data-url');
    yii.confirm('".Yii::t('app', 'Are you sure you want to reset this item?')."', function(){
	$.post(
	    url
	).done(function(result){
	    if(result.status == 'success'){
		". SDNoty::show('result.message', 'result.status') ."
		$.pjax.reload({container:'#auther-form-grid-pjax'});
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}).fail(function(){
	    ". SDNoty::show('"<strong><i class=\"glyphicon glyphicon-warning-sign\"></i> Error!</strong> "+e.responseJSON.message', '"error"') ."
	    console.log('server error');
	});
    })
});

$('#auther-form-grid-pjax').on('click', '#modal-addbtn-auther-form', function(){
    modalAutherForm($(this).attr('data-url'));
});

$('#auther-form-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalAutherForm('".Url::to(['research/update-tmp', 'id'=>''])."'+id);
});

$('#auther-form-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action == 'view'){
	modalAutherForm(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function(){
	    $.post(
		url
	    ).done(function(result){
		if(result.status == 'success'){
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#auther-form-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function(){
		". SDNoty::show('"<strong><i class=\"glyphicon glyphicon-warning-sign\"></i> Error!</strong> "+e.responseJSON.message', '"error"') ."
		console.log('server error');
	    });
	})
    }
    return false;
});

function modalAutherForm(url) {
    $('#modal-auther-form .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-auther-form').modal('show')
    .find('.modal-content')
    .load(url);
}
");?>