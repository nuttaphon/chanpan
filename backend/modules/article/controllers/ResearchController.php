<?php

namespace app\modules\article\controllers;

use Yii;
use backend\modules\article\models\Research;
use backend\modules\article\models\ResearchSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use backend\modules\article\models\AutherForm;
use common\models\UserProfile;
use yii\web\UploadedFile;

/**
 * ResearchController implements the CRUD actions for Research model.
 */
class ResearchController extends Controller {

	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
		];
	}

	public function beforeAction($action) {
		if (parent::beforeAction($action)) {
			if (in_array($action->id, array('create', 'update'))) {
				
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Lists all Research models.
	 * @return mixed
	 */
	public function actionIndex() {
		$cca = isset($_GET['cca'])?$_GET['cca']:0;
		$tree = isset($_GET['tree']) && $_GET['tree']!=''?$_GET['tree']:0;
		
		$searchModel = new ResearchSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProviderCo = $searchModel->searchCo(Yii::$app->request->queryParams);
		
		if($cca==1){
		    $searchModel->cca_status = $cca;
		}
		$dataProviderAll = $searchModel->searchAll(Yii::$app->request->queryParams, $tree);
		
		unset(Yii::$app->session['auther_tmp']);
		
		$dataTree = \backend\modules\article\components\ArticleQuery::getTreeIn();
		
		return $this->render('index', [
					'searchModel' => $searchModel,
					'dataProviderCo' => $dataProviderCo,
					'dataProvider' => $dataProvider,
					'dataProviderAll' => $dataProviderAll,
					'cca' => $cca,
					'dataTree'=>$dataTree,
					'tree' => $tree,
		]);
	}

	/**
	 * Displays a single Research model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
					'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Research model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionAuthorList($q = null, $id = null) {
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $out = ['results' => ['id' => '', 'text' => '', 'email' => '']];
	    if (!is_null($q)) {
		$sql = "SELECT user_id AS id,  concat(firstname, ' ', lastname) AS text, user.email
			FROM user_profile INNER JOIN user ON user_profile.user_id = user.id
			WHERE concat(firstname, ' ', lastname) like :name limit 20";
		$data = Yii::$app->db->createCommand($sql, [':name'=>"%$q%"])->queryAll();
		$out['results'] = array_values($data);
	    } elseif ($id > 0) {
		$model = UserProfile::find($id);
		$out['results'] = ['id' => $id, 'text' => $model->firstname . ' ' . $model->lastname, 'email' => $model->email];
	    }
	    return $out;
	}
	
	public function actionAuthorList2($q = null, $id = null) {
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	   $sql = "SELECT user_id AS id,  concat(firstname, ' ', lastname) AS text, user.email
		    FROM user_profile INNER JOIN user ON user_profile.user_id = user.id
		    WHERE concat(firstname, ' ', lastname) like :name limit 20";
	    $data = Yii::$app->db->createCommand($sql, [':name'=>"%$q%"])->queryAll();
	    $returnVal = [];
	    foreach ($data as $itemObj) {
		$returnVal[] = [
		    'label'=>$itemObj['text'],  // label for dropdown list          
		    'email'=>$itemObj['email'],  // value for input field          
		    'id'=>$itemObj['id'],            // return value from autocomplete
		]; 
	    }

	    return $returnVal;
	}

    public function actionCreate() {
		$modelUi = new AutherForm();
		$model = new Research();
		$model->projid = explode(',', $model->projid);
		$model->mabstract_show = 1;
		$model->mmanuscript_show = 1;
		$model->cca_status = 1;
		$model->show = 1;
		$model->author_auth = 1;
		$model->contact_auth = 1;
		
		$model_user = Yii::$app->user->getIdentity();
		$model_profile = $model_user->userProfile;
		
		if($model_profile){
		    $model->contact_name = $model_profile->firstname . ' ' . $model_profile->lastname;
		    $model->contact_address = '';
		    $model->contact_email = $model_user->email;
		    $model->contact_tel = $model_profile->telephone;
		    $model->contact_fax = '';
		}
		
		if ($model->load(Yii::$app->request->post())) {
			$model->icon = UploadedFile::getInstance($model, 'icon');
			$model->manuscript_file = UploadedFile::getInstance($model, 'manuscript_file');
			$model->abstract_file = UploadedFile::getInstance($model, 'abstract_file');
			
			if ($model->icon !== null) {
			    
			    $idName = '';
			    if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
				    $idName = 'mycom';
			    } else {
				    $idName = $_SERVER["REMOTE_ADDR"];
			    }
			    //date("YmdHis")
			    $nowFileName = 'research_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'.png';
			    $fullPath = Yii::$app->basePath . '/../storage/web/source/images/' . $nowFileName;

			    $model->icon->saveAs($fullPath);
			    $model->icon = $nowFileName;
			} else {
			    $model->icon = '';
			}
			
			if ($model->manuscript_file !== null) {
			    
			    $idName = '';
			    if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
				    $idName = 'mycom';
			    } else {
				    $idName = $_SERVER["REMOTE_ADDR"];
			    }
			    //date("YmdHis")
			    $nowFileName = 'manuscript_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().$model->manuscript_file->name;
			    $fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

			    $model->manuscript_file->saveAs($fullPath);
			    $model->manuscript_file = $nowFileName;
			} else {
			    $model->manuscript_file = '';
			}
			
			if ($model->abstract_file !== null) {
			    
			    $idName = '';
			    if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
				    $idName = 'mycom';
			    } else {
				    $idName = $_SERVER["REMOTE_ADDR"];
			    }
			    //date("YmdHis")
			    $nowFileName = 'abstract_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().$model->abstract_file->name;
			    $fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

			    $model->abstract_file->saveAs($fullPath);
			    $model->abstract_file = $nowFileName;
			} else {
			    $model->abstract_file = '';
			}
			
			if ($model->save()) {
				unset(Yii::$app->session['auther_tmp']);
				Yii::$app->session->setFlash('alert', [
					'body' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
					'options' => ['class' => 'alert-success']
				]);
				return $this->redirect(['index']);
			} else {
				Yii::$app->session->setFlash('alert', [
					'body' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not create the data.'),
					'options' => ['class' => 'alert-danger']
				]);
			}
		} else {
			return $this->render('create', [
						'model' => $model,
						'modelUi' => $modelUi,
			]);
		}
	}

	/**
	 * Updates an existing Research model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id) {

		$model = $this->findModel($id);
		$old_file = $model->icon;
		$manuscript_old_file = $model->manuscript_file;
		$abstract_old_file = $model->abstract_file;
		$model->projid = explode(',', $model->projid);
		
		$modelUi = new AutherForm();
		$session = Yii::$app->session;
		$arr = [];

		if (!isset($session['auther_tmp'])) {
			$idArray = explode(',', $model->co_auther_id);
			$autherArray = explode(',', $model->co_auther);
			$emailArray = explode(',', $model->co_auther_email);
			
			if (is_array($autherArray) && $autherArray[0]!='') {
				foreach ($autherArray as $key => $value) {
					$arr[] = ['co_auther_id'=>$idArray[$key],'co_auther'=>$autherArray[$key],'co_auther_email'=>$emailArray[$key]];
				}
				$session['auther_tmp'] = $arr;
			}
		}
		
		if ($model->load(Yii::$app->request->post())) {
			
			$model->icon = UploadedFile::getInstance($model, 'icon');
			$model->manuscript_file = UploadedFile::getInstance($model, 'manuscript_file');
			$model->abstract_file = UploadedFile::getInstance($model, 'abstract_file');
			
			if ($model->icon !== null) {
			    if($old_file!=''){
				unlink(Yii::$app->basePath . '/../storage/web/source/images/' . $old_file);
			    }
			    
			    $idName = '';
			    if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
				    $idName = 'mycom';
			    } else {
				    $idName = $_SERVER["REMOTE_ADDR"];
			    }
			    //date("YmdHis")
			    $nowFileName = 'research_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'.png';
			    $fullPath = Yii::$app->basePath . '/../storage/web/source/images/' . $nowFileName;

			    $model->icon->saveAs($fullPath);
			    $model->icon = $nowFileName;
			}else {
			    $model->icon = $old_file;
			}
			
			if ($model->manuscript_file !== null) {
			    if($manuscript_old_file!=''){
				unlink(Yii::$app->basePath . '/../storage/web/source/' . $manuscript_old_file);
			    }
			    
			    $idName = '';
			    if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
				    $idName = 'mycom';
			    } else {
				    $idName = $_SERVER["REMOTE_ADDR"];
			    }
			    //date("YmdHis")
			    $nowFileName = 'manuscript_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().$model->manuscript_file->name;
			    $fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

			    $model->manuscript_file->saveAs($fullPath);
			    $model->manuscript_file = $nowFileName;
			} else {
			    $model->manuscript_file = $manuscript_old_file;
			}
			
			if ($model->abstract_file !== null) {
			    if($abstract_old_file!=''){
				unlink(Yii::$app->basePath . '/../storage/web/source/' . $abstract_old_file);
			    }
			    
			    $idName = '';
			    if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
				    $idName = 'mycom';
			    } else {
				    $idName = $_SERVER["REMOTE_ADDR"];
			    }
			    //date("YmdHis")
			    $nowFileName = 'abstract_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().$model->abstract_file->name;
			    $fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

			    $model->abstract_file->saveAs($fullPath);
			    $model->abstract_file = $nowFileName;
			} else {
			    $model->abstract_file = $abstract_old_file;
			}
			
			if ($model->save()) {
				unset(Yii::$app->session['auther_tmp']);
				Yii::$app->session->setFlash('alert', [
					'body' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
					'options' => ['class' => 'alert-success']
				]);
				return $this->redirect(['index']);
			} else {
				Yii::$app->session->setFlash('alert', [
					'body' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not create the data.'),
					'options' => ['class' => 'alert-danger']
				]);
			}
		} else {
			return $this->render('update', [
						'model' => $model,
						'modelUi' => $modelUi,
			]);
		}
	}

	/**
	 * Deletes an existing Research model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id) {
		if (Yii::$app->getRequest()->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			if ($this->findModel($id)->delete()) {
				$result = [
					'status' => 'success',
					'action' => 'update',
					'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
					'data' => $id,
				];
				return $result;
			} else {
				$result = [
					'status' => 'error',
					'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not delete the data.'),
					'data' => $id,
				];
				return $result;
			}
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionCreateTmp() {
		if (Yii::$app->getRequest()->isAjax) {
			$session = Yii::$app->session;

			$model = new AutherForm();

			if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = Response::FORMAT_JSON;

				$arr = [];

				if (isset($session['auther_tmp'])) {
					$arr = $session['auther_tmp'];

					$arr[] = $model->attributes;
				} else {
					$arr[] = $model->attributes;
				}

				$session['auther_tmp'] = $arr;

				$result = [
					'status' => 'success',
					'action' => 'create',
					'message' => '<strong><i class="glyphicon glyphicon-ok"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
					'data' => $model,
				];
				return $result;
			} else {
				return $this->renderAjax('_form-ui', [
							'model' => $model,
				]);
			}
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionUpdateTmp($id) {
		if (Yii::$app->getRequest()->isAjax) {
			$session = Yii::$app->session;
			$model = new AutherForm();

			if (isset($session['auther_tmp'])) {
				$arr = $session['auther_tmp'];
				foreach ($arr as $key => $value) {
					if ($value['co_auther_email'] == $id) {
						$model->attributes = $value;
					}
				}
			}

			if ($model->load(Yii::$app->request->post())) {
				Yii::$app->response->format = Response::FORMAT_JSON;

				$arr = [];

				if (isset($session['auther_tmp'])) {
					$arr = $session['auther_tmp'];
					foreach ($arr as $key => $value) {
						if ($value['co_auther_email'] == $id) {
							$arr[$key] = $model->attributes;
							break;
						}
					}
				} else {
					$arr[] = $model->attributes;
				}

				$session['auther_tmp'] = $arr;

				$result = [
					'status' => 'success',
					'action' => 'update',
					'message' => '<strong><i class="glyphicon glyphicon-ok"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
					'data' => $model,
				];
				return $result;
			} else {
				return $this->renderAjax('_form-ui', [
							'model' => $model,
				]);
			}
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionDeleteTmp($id) {
		if (Yii::$app->getRequest()->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			$session = Yii::$app->session;
			$arr = [];

			if (isset($session['auther_tmp'])) {
				$arr = $session['auther_tmp'];
				foreach ($arr as $key => $value) {
					if ($value['co_auther_email'] == $id) {
						unset($arr[$key]);
						break;
					}
				}
			}

			$session['auther_tmp'] = $arr;

			$result = [
				'status' => 'success',
				'action' => 'update',
				'message' => '<strong><i class="glyphicon glyphicon-ok"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
				'data' => $id,
			];
			return $result;
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionResetTmp() {
		if (Yii::$app->getRequest()->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;

			unset(Yii::$app->session['auther_tmp']);

			$result = [
				'status' => 'success',
				'action' => 'update',
				'message' => '<strong><i class="glyphicon glyphicon-ok"></i> Success!</strong> ' . Yii::t('app', 'Reset data completed.'),
				'data' => '',
			];
			return $result;
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}
	/**
	 * Finds the Research model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Research the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Research::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
