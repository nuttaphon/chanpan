<?php
namespace backend\modules\article\components;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ArticleQuery {
	public static function getTree() {
		$query = new Query;
		$query->select('id, name')
				->from('tbl_tree')
				->where('icon = "institution" AND readonly = 1');
		$command = $query->createCommand();
		$data = $command->queryAll();
		
		return ArrayHelper::map($data, 'id', 'name');
    }
	
    public static function getTreeIn() {
		
		$sql = "select distinct t.id,t.name 
		    from research r 
		    inner join tbl_tree t on instr(concat(',',r.projid,','),concat(',',t.id,','))
		";
		$data = Yii::$app->db->createCommand($sql)->queryAll();
		
		return ArrayHelper::map($data, 'id', 'name');
    }
    
	public static function getTreeById($id) {
		if(isset($id) && !empty($id)){
			$query = new Query;
			$query->select('id, name')
				->from('tbl_tree')
				->where("icon = 'institution' AND readonly = 1 AND id IN($id)");
			$command = $query->createCommand();
			$data = $command->queryAll();

			return ArrayHelper::map($data, 'id', 'name');
		}
		return [];
    }
    
}