<?php
namespace backend\modules\article\components;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 1 ต.ค. 2558 14:05:20
 * @link http://www.appxq.com/
 * @example 
 */
class ArticleFunc {
	public static function itemAlias($code, $key = NULL) {
		$items = [
			'show'=> [
				'1' => 'แสดง',
				'2' => 'ไม่แสดง',
			],
			'status'=> [
				'1' => 'Research proposal preparation',
				'2' => 'Research proposal finalized',
				'3' => 'Mock abstract preparation',
				'4' => 'Mock abstract finalized',
				'5' => 'Mock manuscipt preparation',
				'6' => 'Mock manuscipt finalized',
				'7' => 'Manuscript preparation',
				'8' => 'Manuscript submitted to the journal',
				'9' => 'Published',
			],
			'signup'=> [
			    '0'=>'ผู้รับบริการ',
			    '1'=>'บุคลากร',
			    '2'=>'-- ผู้ดูแลระบบฐานข้อมูล',
                            '3'=>'-- ผู้ให้บริการด้านการแพทย์และสาธารณสุข',
                            '4'=>'-- ผู้บริหาร',
			    '5'=>'----- หน่วยงานระดับประเทศ',
			    '6'=>'----- หน่วยงานระดับเขต',
			    '7'=>'----- หน่วยงานระดับจังหวัด',
			    '8'=>'----- หน่วยงานระดับอำเภอ',
			    '9'=>'----- หน่วยงานทางการแพทย์และสาธารณสุข',
                            '10'=>'-- นักวิจัย',
                            '11'=>'-- อื่นๆ ',
			],
			'status_user'=> [
			    '0'=>'ผู้รับบริการ',
			    '1'=>'บุคลากร',
			],
			'status_personal'=> [
			    '2'=>'ผู้ดูแลระบบฐานข้อมูล',
                            '3'=>'ผู้ให้บริการด้านการแพทย์และสาธารณสุข',
                            '4'=>'ผู้บริหาร',
                            '10'=>'นักวิจัย',
                            '11'=>'อื่นๆ ',
			],
			'status_manager'=> [
			    '5'=>'หน่วยงานระดับประเทศ',
			    '6'=>'หน่วยงานระดับเขต',
			    '7'=>'หน่วยงานระดับจังหวัด',
			    '8'=>'หน่วยงานระดับอำเภอ',
			    '9'=>'หน่วยงานทางการแพทย์และสาธารณสุข',
			]
		];
		
		$return = $items[$code];
		
		if (isset($key)) {
			return isset($return[$key]) ? $return[$key] : false;
		} else {
			return isset($return) ? $return : false;
		}
	}

	public static function itemYear($value, $range = 20) {
		$r = [];
		$yyyy = date('Y');
		
		if(isset($value) && $value!=''){
			$count = $yyyy-$value;
			if($count>0){
				for($i=0;$i<=$count;$i++){
					$cYear = $value+$i;
					$r[$cYear] = $cYear;
				}
			}
		}
		
		for($i=0;$i<=$range;$i++){
			$cYear = $yyyy+$i;
			$r[$cYear] = $cYear;
		}
		
		return $r;
	}
	
	public static function owner($userProfile){
	    $str = '';
	    if(isset($userProfile->status)){
		$str = 'บทบาท: '.self::itemAlias('status_user', $userProfile->status);
	    }
	    
	    if($userProfile->status==1){
		if($userProfile->status_personal==4){
		    $str .= ' -> บทบาทเฉพาะสังกัด: '.self::itemAlias('status_personal', $userProfile->status_personal). ' - '.self::itemAlias('status_manager', $userProfile->status_manager);
		} else {
		    $str .= ' -> บทบาทเฉพาะสังกัด: '.self::itemAlias('status_personal', $userProfile->status_personal);
		}
	    }
	    return $str;
	}
}
