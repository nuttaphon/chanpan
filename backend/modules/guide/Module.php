<?php

namespace backend\modules\guide;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\guide\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
