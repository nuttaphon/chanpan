<?php
namespace backend\modules\guide\components;

class GuideFunc {

    public static function listDynamic() {
	//get db
	$model = new \yii\base\DynamicModel(['id', 'name', 'type', 'order']);
	$model->addRule(['name', 'type', 'order'], 'required')
		->addRule(['order'], 'integer')
		->addRule(['name'], 'string', ['max' => 100])
		->addRule(['type'], 'string', ['max' => 50]);
	
	return $model;
    }

}
