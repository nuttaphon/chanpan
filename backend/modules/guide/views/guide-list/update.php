<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideList */

$this->title = 'Update Guide List: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Guide Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="guide-list-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
