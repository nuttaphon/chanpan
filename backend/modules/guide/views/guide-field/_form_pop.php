<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use backend\modules\guide\models\GuideList;
/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideField */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="guide-field-form">

   <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'uid')->textInput(['readonly'=>true]) ?>

    <?php echo $form->field($model, 'text_input')->textInput(['maxlength' => true, 'placeholder'=>'Text Input (placeholder)']) ?>
    
    <?= Html::activeHiddenInput($model, 'datetime') ?>
    <?= Html::activeHiddenInput($model, 'create_by') ?>
    <?= Html::activeHiddenInput($model, 'create_time') ?>
    <?= Html::activeHiddenInput($model, 'update_by') ?>
    <?= Html::activeHiddenInput($model, 'update_time') ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  

$this->registerJs("

$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". \appxq\sdii\helpers\SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		
		$.pjax.reload({container:'#inv-fields-grid-pjax'});
	    } else if(result.action == 'update') {
		
		$.pjax.reload({container:'#inv-fields-grid-pjax'});
	    }
	    $(result.popid).popoverX('hide');
	} else {
	    ". \appxq\sdii\helpers\SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". \appxq\sdii\helpers\SDNoty::show("'" . appxq\sdii\helpers\SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>