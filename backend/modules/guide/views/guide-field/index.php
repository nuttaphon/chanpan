<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\guide\models\GuideFieldSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guide Fields';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="guide-field-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Guide Field', ['create'], ['class' => 'btn btn-success']) ?>
	<?php
	$content = '<p class="text-justify">' .
    'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.' . 
    '</p>';
	
	
	
	echo \kartik\popover\PopoverX::widget([
	    'header' => 'Hello world',
	    'placement' => \kartik\popover\PopoverX::ALIGN_RIGHT_TOP,
	    'content' => '',
	    'toggleButton' => ['label'=>'<i class="glyphicon glyphicon-plus">Right</i>', 'class'=>'btn btn-link add-btn', 'data-url'=>  yii\helpers\Url::to('/guide/guide-field/create')],
	]);
	?>
    </p>

    <?php
    $gridColumns = [
	[
            'header'=>'ADD', 
            'width'=>'250px',
	    'format'=>'raw',
            'value'=>function ($model, $key, $index, $widget) { 
                return \kartik\popover\PopoverX::widget([
		    'header' => 'Hello world',
		    'placement' => \kartik\popover\PopoverX::ALIGN_RIGHT_TOP,
		    'content' => '',
		    'toggleButton' => ['label'=>'<i class="glyphicon glyphicon-plus">Right</i>', 'class'=>'btn btn-link add-btn', 'data-url'=>  yii\helpers\Url::to('/guide/guide-field/create')],
		]);
            },
            
        ],
	[
	    'class'=>'kartik\grid\EditableColumn',
	    'attribute'=>'text_input',
	    
	    'editableOptions'=> function ($model, $key, $index) {
		return [
		    'header'=>'Form',
		    'size'=>'md',
		    'afterInput'=>function ($form, $widget) use ($model, $index) {
			return $form->field($model, 'textarea')->textarea(['rows' => 3]);
		    }
		];
	    }
	],
	[
	    'class'=>'kartik\grid\EditableColumn',
	    'attribute'=>'number',
	    'editableOptions'=>function ($model, $key, $index) {
		return [
		    'header'=>'Form',
		    'size'=>'md',
		    'afterInput'=>function ($form, $widget) use ($model, $index) {
			return $form->field($model, 'email')->textInput().$form->field($model, 'int_input')->textInput();
		    }
		];
	    }
	    
	],
    ];
    
    echo \kartik\grid\GridView::widget([
	'dataProvider'=>$dataProvider,
	'filterModel'=>$searchModel,
	'pjax' => true, 
	'pjaxSettings' =>
            [
                'neverTimeout'=>true,
                'options'=>['id'=>'inv-fields-grid-pjax'],
            ],  
	'columns'=>$gridColumns
    ]);
    ?>
    
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'auto_id',
            'uid',
            'text_input',
            'textarea:ntext',
            'email:email',
            // 'number',
            // 'int_input',
            // 'checkbox',
            // 'checkbox_list:ntext',
            // 'radio',
            // 'multiple:ntext',
            // 'dropdown',
            // 'readonly',
            // 'disabled',
            // 'textmask_input',
            // 'optional_icons',
            // 'select2',
            // 'dropdown_db',
            // 'file:ntext',
            // 'date',
            // 'time',
            // 'datetime',
            // 'create_by',
            // 'create_time',
            // 'update_by',
            // 'update_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>


<?php  

$this->registerJs("
$('body').on('click', '.add-btn', function() {
    modalInvField($(this).attr('data-target'));
});


function modalInvField(target) {
    $(target).popoverX('show');
    $(target + ' .popover-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    
    $.ajax({
		method: 'GET',
		url:'".yii\helpers\Url::to(['/guide/guide-field/create'])."',
		data: {target:target},
		dataType: 'HTML',
		success: function(result, textStatus) {
		    $(target + ' .popover-content').html(result);
		}
    });
    
}

");?>