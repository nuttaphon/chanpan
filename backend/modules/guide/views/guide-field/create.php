<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\guide\models\GuideField */

$this->title = 'Create Guide Field';
$this->params['breadcrumbs'][] = ['label' => 'Guide Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guide-field-create">

    <?php echo $this->render('_form_pop', [
        'model' => $model,
    ]) ?>

</div>
