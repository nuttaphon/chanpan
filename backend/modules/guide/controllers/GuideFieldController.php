<?php

namespace backend\modules\guide\controllers;

use Yii;
use backend\modules\guide\models\GuideField;
use backend\modules\guide\models\GuideFieldSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * GuideFieldController implements the CRUD actions for GuideField model.
 */
class GuideFieldController extends Controller {

    public function behaviors() {
	return [
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    'delete' => ['post'],
		],
	    ],
	];
    }

    /**
     * Lists all GuideField models.
     * @return mixed
     */
    public function actionIndex() {
	$searchModel = new GuideFieldSearch();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	if (Yii::$app->request->post('hasEditable')) {
	    $keyId = Yii::$app->request->post('editableKey');
	    $model = GuideField::findOne($keyId);
	    $out = Json::encode(['output' => '', 'message' => '']);

	    $posted = current($_POST['GuideField']);
	    $post = ['GuideField' => $posted];

	    if ($model->load($post)) {
		$model->save();
		$output = '';
		
		$out = Json::encode(['output' => $output, 'message' => '']);
	    }
	    
	    return $out;
	}
	
	return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'model' => $model,
		    
	]);
    }

    /**
     * Displays a single GuideField model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
	return $this->render('view', [
		    'model' => $this->findModel($id),
	]);
    }

    /**
     * Creates a new GuideField model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($target) {
	$model = new \backend\modules\guide\models\GuideField();
	$model->uid = uniqid('GuideField');
	$model->datetime = date('Y-m-d H:s:i');
	$model->create_by = Yii::$app->user-id;
	$model->create_time = date('Y-m-d H:s:i');
	$model->update_by = Yii::$app->user-id;
	$model->update_time = date('Y-m-d H:s:i');
	
	if ($model->load(Yii::$app->request->post())) {
	    
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    
	    if ($model->save()) {
		$result = [
			'status' => 'success',
			'action' => 'create',
			'message' => \appxq\sdii\helpers\SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
			'popid'=>$target,
		    ];
		    return $result;
	    } else {
		$result = [
			'status' => 'error',
			'message' => \appxq\sdii\helpers\SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
			'popid'=>$target,
		    ];
		    return $result;
	    }
	    
	} else {
	    return $this->renderAjax('create', [
			'model' => $model,
	    ]);
	}
    }

    /**
     * Updates an existing GuideField model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
	$model = $this->findModel($id);
	$model->checkbox_list = explode(',', $model->checkbox_list);
	$model->multiple = explode(',', $model->multiple);

	if ($model->load(Yii::$app->request->post())) {
	    $model->checkbox_list = implode(',', array_values($model->checkbox_list));
	    $model->multiple = implode(',', array_values($model->multiple));
	    if ($model->save()) {
		
	    }
	    return $this->redirect(['index', 'id' => $model->auto_id]);
	} else {
	    return $this->render('update', [
			'model' => $model,
	    ]);
	}
    }

    /**
     * Deletes an existing GuideField model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
	$this->findModel($id)->delete();

	return $this->redirect(['index']);
    }

    /**
     * Finds the GuideField model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GuideField the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
	if (($model = GuideField::findOne($id)) !== null) {
	    return $model;
	} else {
	    throw new NotFoundHttpException('The requested page does not exist.');
	}
    }

}
