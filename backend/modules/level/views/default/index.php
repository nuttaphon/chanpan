<?php
if(is_null($array)){
    echo '<div class="alert alert-danger">คุณไม่มีสิทธ์ใช้งาน Module นี้ กรุณาติดต่อผู้ดูแลระบบ</div>';
    exit;
}
?>

<?=\yii\helpers\Html::a('<span class="fa fa-cog"></span> ตั้งค่าสิทธิ์การเข้าใช้งาน', ['/level/level-manager-right'], ['class'=>'btn btn-success btn-sm pull-right'])?>
<br><hr>
<span class="h3">ย้ายสิทธิ์เข้าใช้งาน Module</span><hr>
<?php foreach ($array as $key => $val){ ?>
<div class="media">
    <div class="media-left">
        <span class="fa fa-list"></span>
    </div>
    <div class="media-body">
        <h4 class="media-heading"><?=$key;?></h4>

    <?php foreach ($val as $key => $val){ ?>
        <div class="media">
            <div class="media-left">
                <?php echo \yii\helpers\Html::a('<span class="fa fa-external-link"></span> ย้ายเข้าทำงาน', ['/level/default/swap', 'hcode'=>$key], [
                    'class'=>'btn btn-primary btn-xs',
                    'title' => 'ย้ายเข้าทำงาน',
                    'data-confirm' => '<div class = "h3">คุณต้องการย้ายเข้าทำงานที่ <br><br> <code>'.$key.' : '.$val.'</code></div>',
                ])?>
            </div>
            <div class="media-body">
                <h4 class="media-heading"><span class="fa fa-h-square"></span> <?=$key;?> : <?=$val;?></h4>
            </div>
        </div>
    <?php } ?>

    </div>
</div>
<?php } ?>