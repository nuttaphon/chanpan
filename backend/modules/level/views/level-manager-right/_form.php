<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\level\models\LevelManagerRight */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="level-manager-right-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'target')->widget(Select2::classname(), [
        'data' => $userList,
        'options' => ['placeholder' => 'เลือกผู้มีสิทธิ์ใช้งาน ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <?= $form->field($model, 'p')->radioList([
        '1' => 'ผู้ดูแลระดับอำเภอ',
        '2' => 'ผู้ดูแลระดับจังหวัด',
        '3' => 'ผู้ดูแลระดับเขต',
        '4' =>'ผู้ดูแลระดับกระทรวง',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
