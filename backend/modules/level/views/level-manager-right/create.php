<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\level\models\LevelManagerRight */

$this->title = 'Create Level Manager Right';
$this->params['breadcrumbs'][] = ['label' => 'Level Manager Rights', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="level-manager-right-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'userList' => $userList
    ]) ?>

</div>
