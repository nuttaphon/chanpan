<?php

namespace backend\modules\level\controllers;

use Yii;
use backend\modules\level\models\LevelManagerRight;
use backend\modules\level\models\LevelManagerRightSearch;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LevelManagerRightController implements the CRUD actions for LevelManagerRight model.
 */
class LevelManagerRightController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all LevelManagerRight models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LevelManagerRightSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        //target
        $res = Yii::$app->db->createCommand("select user_id as id, concat(firstname, '  ', lastname) as text from user_profile inner join tbdata_coc_right on user_profile.user_id=tbdata_coc_right.target group by user_profile.user_id;")->queryAll();
        $filterType['target'] = ArrayHelper::map($res, 'id', 'text');

        $res = Yii::$app->db->createCommand("select user_id as id, concat(firstname, '  ', lastname) as text from user_profile inner join tbdata_coc_right on user_profile.user_id=tbdata_coc_right.user_create group by user_profile.user_id;")->queryAll();
        $filterType['user_create'] = ArrayHelper::map($res, 'id', 'text');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'filterType' => $filterType,
        ]);
    }

    /**
     * Displays a single LevelManagerRight model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LevelManagerRight model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LevelManagerRight();

        if ($model->load(Yii::$app->request->post())) {
            $user = \common\models\UserProfile::find()->select('sitecode')->where(['user_id'=>$model->target])->one();
            $model->xsourcex = $user->sitecode;
            $model->user_create =Yii::$app->user->identity->id;
            $model->create_date = new Expression('NOW()');
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $userList = Yii::$app->db->createCommand("select user_id as id, concat(`sitecode`, ' : ', `firstname`, '  ', `lastname`) as text FROM user_profile")->queryAll();
            $userList = ArrayHelper::map($userList, 'id', 'text');
            return $this->render('create', [
                'model' => $model,
                'userList' => $userList
            ]);
        }
    }

    /**
     * Updates an existing LevelManagerRight model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $user = \common\models\UserProfile::find()->select('sitecode')->where(['user_id'=>$model->target])->one();
            $model->xsourcex = $user->sitecode;
            $model->user_update =Yii::$app->user->identity->id;
            $model->update_date = new Expression('NOW()');
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $userList = Yii::$app->db->createCommand("select user_id as id, concat(`sitecode`, ' : ', `firstname`, '  ', `lastname`) as text FROM user_profile")->queryAll();
            $userList = ArrayHelper::map($userList, 'id', 'text');
            return $this->render('create', [
                'model' => $model,
                'userList' => $userList
            ]);
        }
    }

    /**
     * Deletes an existing LevelManagerRight model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LevelManagerRight model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return LevelManagerRight the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LevelManagerRight::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
