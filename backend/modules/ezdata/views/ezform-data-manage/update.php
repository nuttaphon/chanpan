<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezdata\models\EzformDataManage */

$this->title = 'Update Ezform Data Manage: ';
$this->params['breadcrumbs'][] = ['label' => 'Ezform Data Manages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ezform-data-manage-update">


    <?= $this->render('_form', [
        'ezf_field_search' =>$ezf_field_search,
        'ezf_field_id' => $ezf_field_id,
        'model' => $model,
        'ezformItems' =>$ezformItems
    ]) ?>

</div>
