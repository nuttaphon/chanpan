<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezdata\models\EzformDataManageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ezform-data-manage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ezf_id') ?>

    <?= $form->field($model, 'params') ?>

    <?= $form->field($model, 'fsearch') ?>

    <?= $form->field($model, 'xsourcex') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
