<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\ezdata\models\EzformDataManage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ezform-data-manage-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ezf_id')->widget(Select2::classname(), [
        'id' => 'ezf_id',
        'data' => $ezformItems,
        'options' => ['placeholder' => 'เลือกฟอร์ม...',],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('เลือกฟอร์ม');
    ?>
    <?= $form->field($model, 'detail')->textarea(['rows'=>6]); ?>
    <?= $form->field($model, 'js')->textarea(['rows'=>4]); ?>

    <?php
    $this->registerJs("$('#ezformdatamanage-ezf_id').on('change',function(){
      var val = $(this).val();
      $.getJSON( '".\yii\helpers\Url::to(['/ajax/list-ezf-field', 'ezf_id' =>''])."'+val, function( data ) {
          $('#ezformdatamanage-params').html('<option value>เลือกตัวแปร...</option>');
          $.each( data, function( key, val ) {
                $('#ezformdatamanage-params').append('<option value=\"'+val.ezf_field_name+'\">'+val.ezf_field_name+ ' : ' +val.ezf_field_label+'</option>');
          });
    });

    $.getJSON( '".\yii\helpers\Url::to(['/ajax/list-ezf-field-date', 'ezf_id' =>''])."'+val, function( data ) {
          $('#ezformdatamanage-fsearch').html('<option value>เลือกตัวแปร...</option>');
          $.each( data, function( key, val ) {
                $('#ezformdatamanage-fsearch').append('<option value=\"'+val.ezf_field_name+'\">'+val.ezf_field_name+ ' : ' +val.ezf_field_label+'</option>');
          });
           $('#ezformdatamanage-fsearch').append('<option value=\"create_date\">create_date : วันที่บันทึกข้อมูล</option>');
           $('#ezformdatamanage-fsearch').append('<option value=\"update_date\">update_date : วันที่แก้ไขข้อมูลล่าสุด</option>');
    });
    });");
    ?>

    <?php $model->params = explode(',', $model->params);?>
    <?= $form->field($model, 'params')->widget(Select2::classname(), [
        'id' => 'ezf_field_id',
        'data' => \yii\helpers\ArrayHelper::map($ezf_field_id, 'ezf_field_name', 'ezf_field_label'),
        'maintainOrder'=>true,
        'options' => ['placeholder' => 'เลือกตัวแปร...', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
        ],
    ]);
    ?>

    <?= $form->field($model, 'fsearch')->widget(Select2::classname(), [
        'id' => 'ezf_field_search',
        'data' => \yii\helpers\ArrayHelper::map($ezf_field_search, 'ezf_field_name', 'ezf_field_label'),
        'value' => ['red', 'green'], // initial value
        'options' => ['placeholder' => 'เลือกตัวแปร...',],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]);
    ?>

    <?= $form->field($model, 'xsourcex')->checkbox(['label' => 'แสดงเฉพาะข้อมูลภายใน site ตัวเอง']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
