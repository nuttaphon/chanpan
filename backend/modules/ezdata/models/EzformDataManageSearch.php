<?php

namespace backend\modules\ezdata\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ezdata\models\EzformDataManage;

/**
 * EzformDataManageSearch represents the model behind the search form about `backend\modules\ezdata\models\EzformDataManage`.
 */
class EzformDataManageSearch extends EzformDataManage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'xsourcex'], 'integer'],
            [['params'], 'string'],
            [['fsearch'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EzformDataManage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ezf_id' => $this->ezf_id,
            'xsourcex' => $this->xsourcex,
        ]);

        $query->andFilterWhere(['like', 'params', $this->params])
            ->andFilterWhere(['like', 'fsearch', $this->fsearch]);

        return $dataProvider;
    }
}
