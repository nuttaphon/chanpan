<?php

namespace backend\modules\dashboard\models;

use Yii;

/**
 * This is the model class for table "dashboard_config".
 *
 * @property integer $dash_id
 * @property string $dash_name
 * @property string $dash_color
 * @property integer $dash_public
 * @property string $dash_share
 * @property integer $dash_active
 * @property integer $dash_approved
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 * @property string $sitecode
 * @property integer $ezf_id
 * @property string $ezf_name
 * @property string $ezf_table
 * @property integer $comp_id
 * @property string $pk_field
 * @property integer $special
 * @property string $ezform_config
 * @property string $dash_js
 * @property integer $date_config
 * @property string $search_config
 * @property string $event_config
 */
class DashboardConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dashboard_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dash_name', 'dash_color', 'dash_public', 'dash_share', 'dash_active', 'dash_approved', 'created_by', 'created_at', 'updated_by', 'updated_at', 'sitecode', 'ezf_id', 'ezf_name', 'ezf_table', 'comp_id', 'pk_field', 'special', 'ezform_config', 'dash_js', 'date_config', 'search_config', 'event_config'], 'required'],
            [['dash_public', 'dash_active', 'dash_approved', 'created_by', 'updated_by', 'ezf_id', 'comp_id', 'special', 'date_config'], 'integer'],
            [['dash_share', 'ezform_config', 'dash_js', 'search_config', 'event_config'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['dash_name'], 'string', 'max' => 200],
            [['dash_color'], 'string', 'max' => 50],
            [['sitecode'], 'string', 'max' => 10],
            [['ezf_name', 'ezf_table'], 'string', 'max' => 100],
            [['pk_field'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dash_id' => Yii::t('app', 'Dash ID'),
            'dash_name' => Yii::t('app', 'Dash Name'),
            'dash_color' => Yii::t('app', 'Dash Color'),
            'dash_public' => Yii::t('app', 'Dash Public'),
            'dash_share' => Yii::t('app', 'Dash Share'),
            'dash_active' => Yii::t('app', 'Dash Active'),
            'dash_approved' => Yii::t('app', 'Dash Approved'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'sitecode' => Yii::t('app', 'Sitecode'),
            'ezf_id' => Yii::t('app', 'Ezf ID'),
            'ezf_name' => Yii::t('app', 'Ezf Name'),
            'ezf_table' => Yii::t('app', 'Ezf Table'),
            'comp_id' => Yii::t('app', 'Comp ID'),
            'pk_field' => Yii::t('app', 'Pk Field'),
            'special' => Yii::t('app', 'Special'),
            'ezform_config' => Yii::t('app', 'Ezform Config'),
            'dash_js' => Yii::t('app', 'Dash Js'),
            'date_config' => Yii::t('app', 'Date Config'),
            'search_config' => Yii::t('app', 'Search Config'),
            'event_config' => Yii::t('app', 'Event Config'),
        ];
    }
}
