<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\dashboard\models\DashboardConfigSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="dashboard-config-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
	'layout' => 'horizontal',
	'fieldConfig' => [
	    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
	    'horizontalCssClasses' => [
		'label' => 'col-sm-2',
		'offset' => 'col-sm-offset-3',
		'wrapper' => 'col-sm-6',
		'error' => '',
		'hint' => '',
	    ],
	],
    ]); ?>

    <?= $form->field($model, 'dash_id') ?>

    <?= $form->field($model, 'dash_name') ?>

    <?= $form->field($model, 'dash_color') ?>

    <?= $form->field($model, 'dash_public') ?>

    <?= $form->field($model, 'dash_share') ?>

    <?php // echo $form->field($model, 'dash_active') ?>

    <?php // echo $form->field($model, 'dash_approved') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'sitecode') ?>

    <?php // echo $form->field($model, 'ezf_id') ?>

    <?php // echo $form->field($model, 'ezf_name') ?>

    <?php // echo $form->field($model, 'ezf_table') ?>

    <?php // echo $form->field($model, 'comp_id') ?>

    <?php // echo $form->field($model, 'pk_field') ?>

    <?php // echo $form->field($model, 'special') ?>

    <?php // echo $form->field($model, 'ezform_config') ?>

    <?php // echo $form->field($model, 'dash_js') ?>

    <?php // echo $form->field($model, 'date_config') ?>

    <?php // echo $form->field($model, 'search_config') ?>

    <?php // echo $form->field($model, 'event_config') ?>

    <div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
	    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
	    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
	</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
