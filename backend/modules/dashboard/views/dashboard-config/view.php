<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\dashboard\models\DashboardConfig */

$this->title = 'Dashboard Config#'.$model->dash_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dashboard Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-config-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'dash_id',
		'dash_name',
		'dash_color',
		'dash_public',
		'dash_share:ntext',
		'dash_active',
		'dash_approved',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
		'sitecode',
		'ezf_id',
		'ezf_name',
		'ezf_table',
		'comp_id',
		'pk_field',
		'special',
		'ezform_config:ntext',
		'dash_js:ntext',
		'date_config',
		'search_config:ntext',
		'event_config:ntext',
	    ],
	]) ?>
    </div>
</div>
