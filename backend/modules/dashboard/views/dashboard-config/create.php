<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\dashboard\models\DashboardConfig */

$this->title = Yii::t('app', 'Create Dashboard Config');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dashboard Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dashboard-config-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
