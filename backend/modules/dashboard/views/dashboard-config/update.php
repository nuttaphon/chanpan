<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\dashboard\models\DashboardConfig */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dashboard Config',
]) . ' ' . $model->dash_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dashboard Configs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dash_id, 'url' => ['view', 'id' => $model->dash_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dashboard-config-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
