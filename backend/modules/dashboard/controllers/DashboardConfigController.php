<?php

namespace backend\modules\dashboard\controllers;

use Yii;
use backend\modules\dashboard\models\DashboardConfig;
use backend\modules\dashboard\models\DashboardConfigSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use appxq\sdii\helpers\SDHtml;

/**
 * DashboardConfigController implements the CRUD actions for DashboardConfig model.
 */
class DashboardConfigController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['view', 'create', 'update', 'delete', 'deletes'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all DashboardConfig models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DashboardConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DashboardConfig model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    return $this->render('view', [
		'model' => $this->findModel($id),
	    ]);
	}
    }

    /**
     * Creates a new DashboardConfig model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	
	$model = new DashboardConfig();

	if ($model->load(Yii::$app->request->post())) {
	    if ($model->save()) {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		    'options'=>['class'=>'alert-success']
		]);
		
		return $this->redirect(['/dashboard/dashboard-config/index']);
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		    'options'=>['class'=>'alert-danger']
		]);
		
		return $this->redirect(['/dashboard/dashboard-config/index']);
	    }
	} 

	return $this->render('create', [
	    'model' => $model,
	]);
	
    }

    /**
     * Updates an existing DashboardConfig model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	$model = $this->findModel($id);

	if ($model->load(Yii::$app->request->post())) {
	    if ($model->save()) {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
		    'options'=>['class'=>'alert-success']
		]);

		return $this->redirect(['/dashboard/dashboard-config/index']);
	    } else {
		Yii::$app->getSession()->setFlash('alert', [
		    'body'=> SDHtml::getMsgError() . Yii::t('app', 'Can not create the data.'),
		    'options'=>['class'=>'alert-danger']
		]);

		return $this->redirect(['/dashboard/dashboard-config/index']);
	    }
	}
	return $this->render('update', [
	    'model' => $model,
	]);
	    
    }

    /**
     * Deletes an existing DashboardConfig model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if ($this->findModel($id)->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    public function actionDeletes() {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if (isset($_POST['selection'])) {
		foreach ($_POST['selection'] as $id) {
		    $this->findModel($id)->delete();
		}
		$result = [
		    'status' => 'success',
		    'action' => 'deletes',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $_POST['selection'],
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    /**
     * Finds the DashboardConfig model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DashboardConfig the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DashboardConfig::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
