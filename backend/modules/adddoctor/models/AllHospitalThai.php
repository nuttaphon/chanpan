<?php
/*
 * Module Add doctor.
 * Developed by Mark.
 * Date : 2016-05-12
*/

namespace app\modules\adddoctor\models;

use Yii;

/**
 * This is the model class for table "all_hospital_thai".
 *
 * @property string $code
 * @property string $name
 * @property string $code2
 * @property string $name3
 * @property string $code4
 * @property string $name5
 * @property string $bed
 * @property string $code6
 * @property string $provincecode
 * @property string $province
 * @property string $amphurcode
 * @property string $amphur
 * @property string $tamboncode
 * @property string $tambon
 * @property string $moo
 * @property string $code7
 * @property string $name8
 * @property string $address
 * @property string $postcode
 * @property string $tel
 * @property string $fax
 * @property string $code9
 * @property string $name10
 * @property string $code11
 * @property string $name12
 * @property string $code13
 * @property string $name14
 * @property string $note
 * @property string $init_date
 * @property string $open_date
 * @property string $close_date
 * @property string $expire_date
 * @property string $upd_date
 * @property string $code5
 * @property string $hcode
 * @property string $lat
 * @property string $lng
 * @property string $engname
 * @property string $ultrasound_client
 * @property string $zone_code
 * @property string $zone_name
 * @property string $short_thai
 * @property string $short_english
 */
class AllHospitalThai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'all_hospital_thai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hcode'], 'required'],
            [['code', 'name', 'code2', 'name3', 'code4', 'name5', 'bed', 'code6', 'provincecode', 'province', 'amphurcode', 'amphur', 'tamboncode', 'tambon', 'moo', 'code7', 'name8', 'address', 'postcode', 'tel', 'fax', 'code9', 'name10', 'code11', 'name12', 'code13', 'name14', 'note', 'init_date', 'open_date', 'close_date', 'expire_date', 'upd_date', 'code5', 'hcode', 'lat', 'lng', 'engname', 'ultrasound_client', 'zone_code', 'zone_name', 'short_thai', 'short_english'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'name' => 'Name',
            'code2' => 'Code2',
            'name3' => 'Name3',
            'code4' => 'Code4',
            'name5' => 'Name5',
            'bed' => 'Bed',
            'code6' => 'Code6',
            'provincecode' => 'Provincecode',
            'province' => 'Province',
            'amphurcode' => 'Amphurcode',
            'amphur' => 'Amphur',
            'tamboncode' => 'Tamboncode',
            'tambon' => 'Tambon',
            'moo' => 'Moo',
            'code7' => 'Code7',
            'name8' => 'Name8',
            'address' => 'Address',
            'postcode' => 'Postcode',
            'tel' => 'Tel',
            'fax' => 'Fax',
            'code9' => 'Code9',
            'name10' => 'Name10',
            'code11' => 'Code11',
            'name12' => 'Name12',
            'code13' => 'Code13',
            'name14' => 'Name14',
            'note' => 'Note',
            'init_date' => 'Init Date',
            'open_date' => 'Open Date',
            'close_date' => 'Close Date',
            'expire_date' => 'Expire Date',
            'upd_date' => 'Upd Date',
            'code5' => 'Code5',
            'hcode' => 'Hcode',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'engname' => 'Engname',
            'ultrasound_client' => 'Ultrasound Client',
            'zone_code' => 'Zone Code',
            'zone_name' => 'Zone Name',
            'short_thai' => 'Short Thai',
            'short_english' => 'Short English',
        ];
    }
}
