<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 29-Apr-16
 * Time: 15:20
 */

use app\modules\adddoctor\models\AllHospitalThai;

use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

$this->registerCss('
.field-allhospitalthai-hcode{
    margin-left: 0px !important;
    margin-right: 0px !important;
    margin-bottom: 0px !important;
}
');
$this->registerJs('   
    $(".formAddDoctor").hide();
    $(".docgroupOther").hide();
    
    $(".searchDoctor").keyup(function(){
        $(".formAddDoctor").hide();
        var pincodeSearch = $(".pincodeSearch").val();
        var doctorNameSearch = $(".doctorNameSearch").val();
        if(pincodeSearch!="" || doctorNameSearch!=""){
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/search-doctor').'",
                data    : {
                    pincode: pincodeSearch,
                    doctorName: doctorNameSearch
                },
                success  : function(response) {
                    $(".listDoctor").html(response);
                    buttonAddDoctorEventStart();
                    actionButtonEditCommand();
                },
                error : function(){
                    var strShowErr = "<div class=\"text-center\"><br><br><br><br><br><h1>Error!!!!!</h1><br><br><br><br><br></div>;"
                    $(".listDoctor").html(strShowErr);
                }
            });
        }else{
            console.log("Please. Input data in field.");
        }
    });
    
    function actionButtonEditCommand(){
        $(".btnEdit").click(function(){
            var current = $(this);
            var doctorId = current.attr("doctorId");
            console.log(doctorId);
            
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/request-data-for-edit-doctor').'",
                data    : {
                    doctorId: doctorId,
                    command: "editDoctorRequest"
                },
                success  : function(response) {
                    $(".editDoctorRequestDiv").html(response);
                    submitEditDoctor();
                },
                error : function(){
                }
            });
        });
        
        $(".btnEditDoctorApprovedInAdd").click(function(){
            var current = $(this);
            var doctorId = current.attr("doctorId");
            console.log("Test"+doctorId);
            
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/request-data-for-edit-doctor-approved').'",
                data    : {
                    doctorId: doctorId,
                    source: "add"
                },
                success  : function(response) {
                    $(".editDoctorApprovedDiv").html(response);
                    submitEditDoctor();
                },
                error : function(){
                }
            });
        });
    }
    
    $(".docgroup").change(function (){
        var docgroup = $("#docgroup").val();
        if(docgroup=="other"){
            $(".docgroupOther").show();
        }else{
            $(".docgroupOther").hide();
        }
    });
    
    $(".addDoctor").submit(function(){
        var pincode = $("#pincode").val();
        var doctorName = $("#doctorName").val();
        var hospital = $("#select2-hospital-container").attr("title");
        var docgroup = $("#docgroup").val();
        if(docgroup=="other"){
            var docgroupOther = $("#docgroupOther").val();
            docgroup = docgroupOther;
//            console.log(pincode+" "+doctorName+" "+hospital+" "+docgroup+" "+docgroupOther);
        }else{
//            console.log(pincode+" "+doctorName+" "+hospital+" "+docgroup);
        }
        
        $.ajax({
            type    : "POST",
            cache   : false,
            url     : "'.Url::to('/adddoctor/default/send-doctor-request').'",
            data    : {
                pincode: pincode,
                doctorName: doctorName,
                hospital: hospital,
                docgroup: docgroup,
                addby : "'.(Yii::$app->user->identity->id).'"
            },
            success  : function(response) {
                if(response=="1"){
                    window.location = "'.Yii::$app->request->url.'";
                }
            },
            error : function(){
            }
        });
        
        return false;
    });
    
    function buttonAddDoctorEventStart(){
        $(".btnAddDoctor").click(function(){
            $(".tableListDoctor").hide();
            $(".formAddDoctor").show();
            if($(".pincodeSearch").val()!=""){
                $(".pincode").val($(".pincodeSearch").val());
            }
            
            if($(".doctorNameSearch").val()!=""){
                $(".doctorName").val($(".doctorNameSearch").val());
            }else{
                
            }
        });
    }
');
?>

<div class="adddoctor-default-adddoctor">
    <div class="text-center">
        <h1>ค้นหาแพทย์</h1>
    </div>
    <div class="col-md-offset-3 col-md-6 form-group">
        <form id="searchDoctor" class="searchDoctor form-group">
            <dl class="dl-horizontal">
                <dt>เลข ว.</dt>
                <dd><input class="form-control pincodeSearch" id="pincodeSearch" maxlength="5" type="text"><br></dd>
                <dt>ชื่อแพทย์</dt>
                <dd><input class="form-control doctorNameSearch" id="doctorNameSearch" type="text"><br></dd>
            </dl>
        </form>
        <div class="row">
            <div class="listDoctor">

            </div>
        </div>
        <br>
        <div class="row">
            <div class="formAddDoctor">
                <form class="addDoctor form-horizontal" method="get">
                    <div class="form-group">
                        <label for="pincode" class="col-sm-3 control-label">เลข ว.</label>
                        <div class="col-sm-9">
                            <input class="form-control pincode" id="pincode" type="text" maxlength="5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pincode" class="col-sm-3 control-label">ชื่อแพทย์</label>
                        <div class="col-sm-9">
                            <input class="form-control doctorName" id="doctorName" type="text" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="hospital" class="col-sm-3 control-label">โรงพยาบาล</label>
                        <div class="col-sm-9">
                            <?php
                            // The controller action that will render the list
                            $url = \yii\helpers\Url::to(['default/hospital-list']);

                            // Get the initial city description
                            $model->hcode = $siteCode;
                            $hospitalName = empty($model->hcode) ? '' : AllHospitalThai::findOne($model->hcode)->name;

                            $formHospital = ActiveForm::begin();

                            echo $formHospital->field($model, 'hcode')->widget(Select2::className(), [
                                'initValueText' => $model->hcode." : ".$hospitalName, // set the initial display text
                                'options' => [
                                    'id' => 'hospital',
                                    'class' => 'hospital',
                                    'name' => 'hospital',
                                    'placeholder' => 'Search for a hospital ...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {keyName:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(AllHospitalThai) { return AllHospitalThai.text; }'),
                                    'templateSelection' => new JsExpression('function (AllHospitalThai) { return AllHospitalThai.text; }'),
                                ],
                            ])->label(false);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="docgroup" class="col-sm-3 control-label">ประเภทความถนัด</label>
                        <div class="col-sm-9">
                            <select class="form-control docgroup" id="docgroup" >
                                <option value="">Select doctor group</option>
                                <option value="general">General Medical</option>
                                <option value="chemo">Chemotherapy</option>
                                <option value="radio">Radiologist</option>
                                <option value="surgery">Surgery</option>
                                <option value="other">Other</option>
                            </select>
                            <input class="form-control docgroupOther" id="docgroupOther" type="text" placeholder="please specify doctor group.">
                        </div>
                    </div>
                    <div class="form-group" style="display: none">
                        <label for="goToPage" class="col-sm-3 control-label">เลข ว.</label>
                        <div class="col-sm-9">
                            <input class="form-control goToPage" id="goToPage" type="text" value="requestList" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <button type="submit" class="form-control btn btn-primary btnSubmitSendRequest" id="btnSubmitSendRequest">ส่งคำขอเพิ่มรายชื่อแพทย์</button>
                        </div>
                    </div>
                </form>
                <?php
                $formHospital = ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
</div>

