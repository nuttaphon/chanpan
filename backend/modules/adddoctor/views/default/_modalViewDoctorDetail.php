<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 25-May-16
 * Time: 12:23
 */
//\yii\helpers\VarDumper::dump($qryDoctor,10,true);
$ccaArray = array();
if($qryDoctor[0]['cca02']){
    array_push($ccaArray, "CCA-02");
}
if($qryDoctor[0]['cca02p1']){
    array_push($ccaArray, "CCA-02.1");
}
if($qryDoctor[0]['cca03']){
    array_push($ccaArray, "CCA-03");
}
if($qryDoctor[0]['cca04']){
    array_push($ccaArray, "CCA-04");
}
if($qryDoctor[0]['cca05']){
    array_push($ccaArray, "CCA-05");
}
$docGroup = ($qryDoctor[0]['docgroup']=='chemo')?'Chemotherapy':(($qryDoctor[0]['docgroup']=='general')?'General Medical':(($qryDoctor[0]['docgroup']=='radio')?'Radiologist':(($qryDoctor[0]['docgroup']=='surgery')?'Surgery':('Other : '.$qryDoctor[0]['docgroup']))));
?>
<dl class="dl-horizontal" style="font-size: large;">
    <dt>เลข ว. :</dt><dd><?= $qryDoctor[0]['pincode'] ?></dd>
    <dt>ชื่อแพทย์ :</dt><dd><?= $qryDoctor[0]['doctorfullname'] ?></dd>
    <dt>ฟอร์มทำงาน :</dt>  <dd><?= implode(", ",$ccaArray) ?></dd>
    <dt>หน่วยบริการ :</dt><dd><?= $qryDoctor[0]['hospcode']." : ".$qryDoctor[0]['hospname'] ?></dd>
    <dt>ความถนัด :</dt><dd><?= $docGroup ?></dd>
</dl>
<div class="text-center">
    <div class="col-sm-6 col-sm-offset-3">
        <?php
        if(is_null($qryDoctor[0]['dadd'])||$qryDoctor[0]['dadd']==''||is_null($qryDoctor[0]['addby'])||$qryDoctor[0]['addby']==''||(strlen($qryDoctor[0]['addby'])<19)){
            echo "<i><span title='กรุณาติดต่อผู้ดูแลระบบ'>ไม่สามารถแก้ไขได้ หากต้องการแก้ไข กรุณาติดต่อผู้ดูแลระบบ</span></i>";
        }else{
            echo "<button class='btn btn-success form-control btnEditDoctorApprovedInModal' id='btnEditDoctorApprovedInModal' data-toggle='modal' data-target='.bs-editDoctorApproved-modal-lg'>Request edit</button>";
        }
        ?>
    </div>
</div>
<br><br>

<input type="text" id="viewDoctorDetail_doctorId" class="viewDoctorDetail_doctorId" value="<?= $qryDoctor[0]['id'] ?>" disabled style="display: none;">
<input type="text" id="viewDoctorDetail_pincode" class="viewDoctorDetail_pincode" value="<?= $qryDoctor[0]['pincode'] ?>" disabled style="display: none;">
<input type="text" id="viewDoctorDetail_doctorfullname" class="viewDoctorDetail_doctorfullname" value="<?= $qryDoctor[0]['doctorfullname'] ?>" disabled style="display: none;">
<input type="text" id="viewDoctorDetail_dadd" class="viewDoctorDetail_dadd" value="<?= $qryDoctor[0]['dadd'] ?>" disabled style="display: none;">
<input type="text" id="viewDoctorDetail_addby" class="viewDoctorDetail_addby" value="<?= $qryDoctor[0]['addby'] ?>" disabled style="display: none;">
<input type="text" id="viewDoctorDetail_hospcode" class="viewDoctorDetail_hospcode" value="<?= $qryDoctor[0]['hospcode'] ?>" disabled style="display: none;">
<input type="text" id="viewDoctorDetail_hospname" class="viewDoctorDetail_hospname" value="<?= $qryDoctor[0]['hospname'] ?>" disabled style="display: none;">
<input type="text" id="viewDoctorDetail_docgroup" class="viewDoctorDetail_docgroup" value="<?= $qryDoctor[0]['docgroup'] ?>" disabled style="display: none;">
<input type="text" id="viewDoctorDetail_rstat" class="viewDoctorDetail_rstat" value="<?= $qryDoctor[0]['rstat'] ?>" disabled style="display: none;">