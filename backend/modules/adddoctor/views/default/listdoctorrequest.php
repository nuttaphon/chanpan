<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 29-Apr-16
 * Time: 16:20
 */

use app\modules\adddoctor\models\AllHospitalThai;

use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

$disabled = true;

$this->registerJs('
    actionButtonRequestCommand();
    getDoctorRequest();
    
    function getDoctorRequest(){
        $.ajax({
            type    : "POST",
            cache   : false,
            url     : "'.Url::to('/adddoctor/default/search-doctor-request').'",
            data    : {
                pincodeSearchDoctorRequest: "",
                doctorNameSearchDoctorRequest: ""
            },
            success  : function(response) {
                $(".listDoctorRequest").html(response);
                actionButtonRequestCommand();
            },
            error : function(){
                var strShowErr = "<div class=\"text-center\"><br><br><br><br><br><h1>Error!!!!!</h1><br><br><br><br><br></div>;"
                $(".listDoctorRequest").html(strShowErr);
            }
        });
    }
    
    $(".searchDoctorRequest").keyup(function(){
        var pincodeSearchDoctorRequest = $(".pincodeSearchDoctorRequest").val();
        var doctorNameSearchDoctorRequest = $(".doctorNameSearchDoctorRequest").val();
//        if(pincodeSearchDoctorRequest!="" || doctorNameSearchDoctorRequest!=""){
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/search-doctor-request').'",
                data    : {
                    pincodeSearchDoctorRequest: pincodeSearchDoctorRequest,
                    doctorNameSearchDoctorRequest: doctorNameSearchDoctorRequest
                },
                success  : function(response) {
                    $(".listDoctorRequest").html(response);
                    actionButtonRequestCommand();
                },
                error : function(){
                    var strShowErr = "<div class=\"text-center\"><br><br><br><br><br><h1>Error!!!!!</h1><br><br><br><br><br></div>;"
                    $(".listDoctorRequest").html(strShowErr);
                }
            });
//        }else{
//            console.log("Please. Input data in field.");
//        }
    });
    
    function actionButtonRequestCommand(){
        actionButtonEditCommand()
        
        $(".btnApprove").click(function(){
            var current_element = $(this);
            var doctorId = current_element.parent().parent().attr("doctorId");
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/approve-doctor-request').'",
                data    : {
                    doctorId: doctorId
                },
                success  : function(response) {
                    current_element.parent().parent().fadeOut(
                        "fast", 
                        function() { 
                            $(this).remove(); 
                        }
                    );
                },
                error : function(){
                }
            });
        });
        
        $(".btnApproveRequest").click(function(){
            var current_element = $(this);
            var doctorId = current_element.parent().parent().attr("doctorId");
            
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/approve-request-doctor-approved').'",
                data    : {
                    doctorId: doctorId
                },
                success  : function(response) {
                    console.log(response);
                    if(response=="1"){
                        current_element.parent().parent().fadeOut(
                            "fast", 
                            function() { 
                                $(this).remove(); 
                            }
                        );
                    }
                },
                error : function(){
                }
            });
        });
        
        $(".btnDecline").click(function(){
            var current_element = $(this);
            var id = current_element.parent().parent().attr("doctorId");
            var varForGetDocName = "tdDoctorName"+id;
            document.getElementById("doctorNameInModalDeclineDoctorRequest").innerHTML = document.getElementsByClassName(varForGetDocName)[0].innerHTML;
            $(".btnDeclineConfirm").attr("doctorId",id);
        });
        
        $(".btnDeclineRequest").click(function(){
            var current_element = $(this);
            var id = current_element.parent().parent().attr("doctorId");
            var varForGetDocName = "tdDoctorName"+id;
            document.getElementById("doctorNameInModaldeclineRequestDoctorApproved").innerHTML = document.getElementsByClassName(varForGetDocName)[0].innerHTML;
            
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : "'.Url::to('/adddoctor/default/get-data-compare').'",
                data    : {
                    doctorId: id
                },
                success  : function(response) {
                    var doctorData = response.split(" : ");
                                        
                    document.getElementById("tdOldPincode").innerHTML = doctorData[1];
                    document.getElementById("tdNewPincode").innerHTML = doctorData[0];
                    
                    document.getElementById("tdOldDoctorname").innerHTML = doctorData[3];
                    document.getElementById("tdNewDoctorname").innerHTML = doctorData[2];
                    
                    document.getElementById("tdOldHospital").innerHTML = doctorData[5]+" : "+doctorData[7];
                    document.getElementById("tdNewHospital").innerHTML = doctorData[4]+" : "+doctorData[6];
                    
                    document.getElementById("tdOldDocGroup").innerHTML = doctorData[9];
                    document.getElementById("tdNewDocGroup").innerHTML = doctorData[8];
                },
                error : function(){
                }
            });
            $(".btnDeclineRequestDoctorApproved").attr("doctorId",id);
        });
    }
    
    $(".btnDeclineConfirm").click(function(){
        var doctorId = $(".btnDeclineConfirm").attr("doctorId");
        $.ajax({
            type    : "POST",
            cache   : false,
            url     : "'.Url::to('/adddoctor/default/decline-doctor-request').'",
            data    : {
                doctorId: doctorId
            },
            success  : function(response) {
                if(response=="1"){
                    $(".bs-decline-modal-sm").modal("toggle");
                    $("tr.doctorId"+doctorId+" td button.btnDecline").attr("Disabled","");
                    $("tr.doctorId"+doctorId+" td button.btnApprove").attr("Disabled","");
                }
//                $(".doctorId"+doctorId).fadeOut(
//                    "fast", 
//                    function() { 
//                        $(this).remove(); 
//                    }
//                );
            },
            error : function(){
            }
        });
    });
    
    $(".btnDeclineRequestDoctorApproved").click(function(){
        var doctorId = $(".btnDeclineRequestDoctorApproved").attr("doctorId");
        $.ajax({
            type    : "POST",
            cache   : false,
            url     : "'.Url::to('/adddoctor/default/decline-doctor-approved-request').'",
            data    : {
                doctorId: doctorId
            },
            success  : function(response) {
                console.log(response);
                if(response=="1"){
                    $(".bs-declineRequestDoctorApproved-modal-lg").modal("toggle");
                    $(".doctorId"+doctorId).fadeOut(
                        "fast", 
                        function() { 
                            $(this).remove(); 
                        }
                    );
                }
            },
            error : function(){
            }
        });
    });
    
    $(".docgroupOtherPageListRequest").hide();
    $(".docgroupPageListRequest").change(function (){
        var docgroupPageListRequest = $("#docgroupPageListRequest").val();
        if(docgroupPageListRequest=="other"){
            $(".docgroupOtherPageListRequest").show();
        }else{
            $(".docgroupOtherPageListRequest").hide();
        }
    });
');
?>
<div class="adddoctor-default-listdoctorapproved">
    <div class="text-center">
        <h1>ค้นหาแพทย์</h1>
    </div>
    <div class="col-md-offset-3 col-md-6 form-group">
        <form id="searchDoctorRequest" class="searchDoctorRequest form-group">
            <dl class="dl-horizontal">
                <dt>เลข ว.</dt>
                <dd><input class="form-control pincodeSearchDoctorRequest" id="pincodeSearchDoctorRequest" maxlength="5" type="text"><br></dd>
                <dt>ชื่อแพทย์</dt>
                <dd><input class="form-control doctorNameSearchDoctorRequest" id="doctorNameSearchDoctorRequest" type="text"><br></dd>
            </dl>
        </form>
    </div>
    <div class="row">
        <div class="listDoctorRequest">
            
        </div>
    </div>
    <br>
</div>

<!-- Alert modal for decline doctor request -->
<div class="modal fade bs-decline-modal-sm" tabindex="-1" role="dialog" aria-labelledby="declineModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel" style="color: red">Warning</h4>
            </div>
            <div class="modal-body">
                <mark style="background-color: red; color: white">ท่านต้องการลบ <strong><ins>คำร้องขอเพิ่มแพทย์</ins></strong> รายนี้ ?</mark>
                <h3><p class="doctorNameInModalDeclineDoctorRequest" id="doctorNameInModalDeclineDoctorRequest"></p></h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btnDeclineConfirm">Confirm decline</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Alert modal for decline doctor request -->
<div class="modal fade bs-declineRequestDoctorApproved-modal-lg" tabindex="-1" role="dialog" aria-labelledby="declineRequestDoctorApprovedModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="declineRequestDoctorApprovedModalLabel" style="color: red">Warning</h4>
            </div>
            <div class="modal-body">
                <mark style="background-color: red; color: white">ท่านต้องการลบ <strong><ins>คำร้องขอแก้ไขข้อมูลแพทย์</ins></strong> รายนี้ ?</mark>
                <h3><p class="doctorNameInModaldeclineRequestDoctorApproved" id="doctorNameInModaldeclineRequestDoctorApproved"></p></h3>

                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <td></td>
                        <td>ข้อมูลเดิม</td>
                        <td>ข้อมูลใหม่</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>เลข ว.</td>
                        <td class="tdOldPincode" id="tdOldPincode"></td>
                        <td class="tdNewPincode" id="tdNewPincode"></td>
                    </tr>
                    <tr>
                        <td>ชื่อ</td>
                        <td class="tdOldDoctorname" id="tdOldDoctorname"></td>
                        <td class="tdNewDoctorname" id="tdNewDoctorname"></td>
                    </tr>
                    <tr>
                        <td>โรงพยาบาล</td>
                        <td class="tdOldHospital" id="tdOldHospital"></td>
                        <td class="tdNewHospital" id="tdNewHospital"></td>
                    </tr>
                    <tr>
                        <td>ความถนัด</td>
                        <td class="tdOldDocGroup" id="tdOldDocGroup"></td>
                        <td class="tdNewDocGroup" id="tdNewDocGroup"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btnDeclineRequestDoctorApproved">Confirm decline</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>