<?php
/*
 * Module Add doctor.
 * Developed by Mark.
 * Date : 2016-04-25
*/
namespace backend\modules\adddoctor\controllers;

use app\modules\adddoctor\models\AllHospitalThai;
use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;

class DefaultController extends Controller{
    public function actionIndex(){
        $model = new AllHospitalThai;
        $siteCode = Yii::$app->user->identity->userProfile->sitecode;

        $qryListdoctorrequest = $this->getListDoctorRequest();
        $qryListdoctorapproved = $this->getListDoctorApproved();

        $adddoctor = $this->renderPartial('adddoctor',[
            'model'=>$model,
            'siteCode'=>$siteCode,
        ]);
        $listdoctorrequest = $this->renderPartial('listdoctorrequest',[
            'qryDoctorRequest'=>$qryListdoctorrequest,
            'model'=>$model
        ]);
        $listdoctorapproved = $this->renderPartial('listdoctorapproved',[
            'qryDoctorApproved'=>$qryListdoctorapproved
        ]);

        $menu = [
            [
                'label'=>'<i class="fa fa-plus fa-lg"></i> เพิ่มแพทย์',
                'content'=>$adddoctor,
                'active'=>true
            ],
            [
                'label'=>'<i class="fa fa-table fa-lg"></i> รายการขอเพิ่มรายชื่อแพทย์',
                'content'=>$listdoctorrequest,
                'active'=>false
            ],
            [
                'label'=>'<i class="fa fa-table fa-lg"></i> รายชื่อแพทย์ที่ได้รับอนุมัติแล้ว',
                'content'=>$listdoctorapproved,
                'active'=>false
            ],

        ];

        return $this->render('index',[
            'menuitems'=>$menu
        ]);
    }

    public function actionHospitalList($keyName = null, $hcode = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['hcode' => '', 'text' => '']];
        if (!is_null($keyName)) {
            $strSqlAllHospital = "SELECT `hcode` as id, CONCAT(`hcode`, ' : ', `name`) AS text FROM all_hospital_thai WHERE (`name` LIKE '%$keyName%' AND `hcode` NOT LIKE '%Z%' AND `hcode` NOT LIKE '%A%') OR (`hcode` LIKE '%$keyName%') ORDER BY `province`, `name`";
            $qryAllHospital = Yii::$app->db->createCommand($strSqlAllHospital)->queryAll();
            $out['results'] = array_values($qryAllHospital);
        } elseif ($hcode > 0){
            $out['results'] = ['id' => $hcode, 'text' => AllHospitalThai::find($hcode)->name];
        }
        return $out;
    }

    public function actionAdddoctor(){
        $html = $this->renderPartial('adddoctor');
        return Json::encode($html);
    }

    public function actionListdoctorrequest(){
//        $html = $this->renderPartial('listdoctorrequest');
//        return Json::encode($html);
        $qryDoctorRequest = $this->getListDoctorRequest();
        return $this->render('listdoctorrequest',[
            'qryDoctorRequest'=>$qryDoctorRequest
        ]);
    }

    public function actionListdoctorrapproved(){
//        $html = $this->renderPartial('listdoctorrequest');
//        return Json::encode($html);
        $qryDoctorApproved = $this->getListDoctorApproved();
        return $this->render('listdoctorrequest',[
            'qryDoctorRequest'=>$qryDoctorApproved
        ]);
    }

    public function actionSearchDoctor(){
        $conditionSql = '';
        if($_POST['pincode'] != '' && $_POST['doctorName'] != ''){
            $conditionSql = "pincode LIKE '%".$_POST['pincode']."%' OR doctorfullname LIKE '%".$_POST['doctorName']."%'";
        }else if($_POST['pincode'] != '' && $_POST['doctorName'] == ''){
            $conditionSql = "pincode LIKE '%".$_POST['pincode']."%'";
        }else if($_POST['pincode'] == '' && $_POST['doctorName'] != ''){
            $conditionSql = "doctorfullname LIKE '%".$_POST['doctorName']."%'";
        }

        $strSqlDoctor = "SELECT * FROM doctor_request WHERE doctorfullname != '' AND ($conditionSql) GROUP BY id";
        $qryDoctor = Yii::$app->db->createCommand($strSqlDoctor)->queryAll();
        $this->createTableShowSearchDoctor($qryDoctor);

        /*$strSqlDoctorAll = "SELECT * FROM doctor_all WHERE doctorfullname != '' AND ($conditionSql)";
        $qryDoctorAll = Yii::$app->db->createCommand($strSqlDoctorAll)->queryAll();
//        echo $strSqlDoctorAll;
//        VarDumper::dump($qryDoctorAll,10,true);
        if(sizeof($qryDoctorAll)==0){
            $strSqlDoctorRequest = "SELECT * FROM doctor_request WHERE doctorfullname != '' AND ($conditionSql)";
            $qryDoctorRequest = Yii::$app->db->createCommand($strSqlDoctorRequest)->queryAll();

//            echo "if";
            $this->createTableShowSearchDoctor($qryDoctorRequest,"รออนุมัติ");
        }else{
//            echo "else";
            $this->createTableShowSearchDoctor($qryDoctorAll,"อนุมัติ");
        }*/
    }

    public function actionSearchDoctorApproved(){
        $pincodeSearchDoctorApproved = filter_input(INPUT_POST, 'pincodeSearchDoctorApproved', FILTER_SANITIZE_STRING);
        $doctorNameSearchDoctorApproved = filter_input(INPUT_POST, 'doctorNameSearchDoctorApproved', FILTER_SANITIZE_STRING);
        $conditionSql = '';
        if($pincodeSearchDoctorApproved != '' && $doctorNameSearchDoctorApproved != ''){
            $conditionSql = "((doctor_all.pincode LIKE '%".$pincodeSearchDoctorApproved."%' OR doctor_all.doctorfullname LIKE '%".$doctorNameSearchDoctorApproved."%') or (doctor_request.pincode LIKE '%".$pincodeSearchDoctorApproved."%' OR doctor_request.doctorfullname LIKE '%".$doctorNameSearchDoctorApproved."%'))";
        }else if($pincodeSearchDoctorApproved != '' && $doctorNameSearchDoctorApproved == ''){
            $conditionSql = "doctor_request.pincode LIKE '%".$pincodeSearchDoctorApproved."%' AND (doctor_all.pincode LIKE doctor_request.pincode)";
        }else if($pincodeSearchDoctorApproved == '' && $doctorNameSearchDoctorApproved != ''){
            $conditionSql = "(doctor_all.doctorfullname LIKE '%".$doctorNameSearchDoctorApproved."%' or doctor_request.doctorfullname LIKE '%".$doctorNameSearchDoctorApproved."%')";
        }

        $strSqlDoctorApproved = "SELECT ".
                "doctor_all.id, ".
                "doctor_all.doctorcode, ".
                "doctor_all.pincode, ".
                "doctor_all.doctorfullname, ".
                "doctor_all.cca02, ".
                "doctor_all.cca02p1, ".
                "doctor_all.cca03, ".
                "doctor_all.cca04, ".
                "doctor_all.cca05, ".
                "doctor_all.dadd, ".
                "doctor_all.addby, ".
                "doctor_all.pid, ".
                "doctor_all.hospcode, ".
                "doctor_all.hospname, ".
                "doctor_all.docgroup, ".
                "doctor_request.status, ".
                "doctor_request.comments, ".
                "doctor_request.dateaction ".
            "FROM doctor_all, doctor_request WHERE doctor_all.doctorfullname != '' ".
            "AND doctor_all.dadd = doctor_request.dadd ".
            "AND doctor_all.addby = doctor_request.addby ".
            "AND ($conditionSql) AND doctor_request.status='2' ".
            "GROUP BY id";
        $qryDoctorApproved = Yii::$app->db->createCommand($strSqlDoctorApproved)->queryAll();
//        echo Yii::$app->db->createCommand($strSqlDoctorApproved)->rawSql;
        $this->createTableShowSearchDoctorApproved($qryDoctorApproved);
    }

    public function actionSearchDoctorRequest(){
        $pincodeSearchDoctorRequest = filter_input(INPUT_POST, 'pincodeSearchDoctorRequest', FILTER_SANITIZE_STRING);
        $doctorNameSearchDoctorRequest = filter_input(INPUT_POST, 'doctorNameSearchDoctorRequest', FILTER_SANITIZE_STRING);
        $conditionSql = '';
        if($pincodeSearchDoctorRequest != '' && $doctorNameSearchDoctorRequest != ''){
            $conditionSql = "pincode LIKE '%".$pincodeSearchDoctorRequest."%' OR doctorfullname LIKE '%".$doctorNameSearchDoctorRequest."%'";
        }else if($pincodeSearchDoctorRequest != '' && $doctorNameSearchDoctorRequest == ''){
            $conditionSql = "pincode LIKE '%".$pincodeSearchDoctorRequest."%'";
        }else if($pincodeSearchDoctorRequest == '' && $doctorNameSearchDoctorRequest != ''){
            $conditionSql = "doctorfullname LIKE '%".$doctorNameSearchDoctorRequest."%'";
        }else{
            $conditionSql = "1";
        }

        $fieldSelect = "doctor_request.status, user_profile.firstname, user_profile.lastname, user_profile.telephone, doctor_request.id, doctor_request.pincode, doctor_request.doctorfullname, doctor_request.hospcode, doctor_request.hospname, doctor_request.docgroup";

        $strSqlDoctorRequest = "SELECT $fieldSelect FROM doctor_request, user_profile WHERE doctorfullname != '' AND ($conditionSql) AND (doctor_request.status LIKE '%1%' OR doctor_request.status LIKE '%4%' OR doctor_request.status LIKE '%5%') AND user_id=addby GROUP BY id";
//        echo Yii::$app->db->createCommand($strSqlDoctorRequest)->rawSql;
        $qryDoctorRequest = Yii::$app->db->createCommand($strSqlDoctorRequest)->queryAll();

        if(sizeof($qryDoctorRequest)==0){
            $strSqlDoctorRequest = "SELECT $fieldSelect FROM doctor_request, user_profile WHERE doctorfullname != '' AND ($conditionSql) AND (doctor_request.status LIKE '%1%' OR doctor_request.status LIKE '%4%' OR doctor_request.status LIKE '%5%') GROUP BY id";
//            echo Yii::$app->db->createCommand($strSqlDoctorRequest)->rawSql;
            $qryDoctorRequest = Yii::$app->db->createCommand($strSqlDoctorRequest)->queryAll();
        }

        $this->createTableShowSearchDoctorRequest($qryDoctorRequest);
    }

    public function actionSendDoctorRequest(){
        date_default_timezone_set("Asia/Bangkok");

        $pincode = filter_input(INPUT_POST, 'pincode', FILTER_SANITIZE_STRING);
        $doctorfullname = filter_input(INPUT_POST, 'doctorName', FILTER_SANITIZE_STRING);
        $dadd = date("Y-m-d H:i:s");
        $addby = filter_input(INPUT_POST, 'addby', FILTER_SANITIZE_STRING);
        $hospital = filter_input(INPUT_POST, 'hospital', FILTER_SANITIZE_STRING);
        $docgroup = filter_input(INPUT_POST, 'docgroup', FILTER_SANITIZE_STRING);
        $hosp = explode(" : ", $hospital);

        $insert = Yii::$app->db->createCommand()->insert('doctor_request', [
            'pincode' => $pincode,
            'doctorfullname' => $doctorfullname,
            'dadd' => $dadd,
            'addby' => $addby,
            'hospcode' => $hosp[0],
            'hospname' => $hosp[1],
            'docgroup' => $docgroup,
        ])->execute();

        if($insert){
            $paramsSelectDoctorAdd = [
                ':pincode' => $pincode,
                ':doctorfullname' => $doctorfullname,
                ':dadd' => $dadd,
                ':addby' => $addby,
                ':hospcode' => $hosp[0],
                ':hospname' => $hosp[1],
                ':docgroup' => $docgroup
            ];

            $select = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE pincode=:pincode AND doctorfullname=:doctorfullname AND dadd=:dadd AND addby=:addby AND hospcode=:hospcode AND hospname=:hospname AND docgroup=:docgroup')->bindValues($paramsSelectDoctorAdd);
            $doctorAdd = $select->queryOne();

            $this->storageDoctorActionLog($doctorAdd, 'add');
        }

        echo $insert;
    }

    public function actionUpdateDoctorRequest(){
        date_default_timezone_set("Asia/Bangkok");

        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);
        $pincode = filter_input(INPUT_POST, 'pincode', FILTER_SANITIZE_STRING);
        $doctorfullname = filter_input(INPUT_POST, 'doctorName', FILTER_SANITIZE_STRING);
//        $dadd = date("Y-m-d H:i:s");
        $addby = filter_input(INPUT_POST, 'addby', FILTER_SANITIZE_STRING);
        $hospital = filter_input(INPUT_POST, 'hospital', FILTER_SANITIZE_STRING);
        $docgroup = filter_input(INPUT_POST, 'docgroup', FILTER_SANITIZE_STRING);
        $hosp = explode(" : ", $hospital);

        /*
         * Get old data before edit.
         * */
//        $paramsSelectDoctorDetails = [
//            ':id' => $doctorId,
//        ];

//        $select = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE id=:id')->bindValues($paramsSelectDoctorDetails);
//        $doctorOldDetails = $select->queryOne();


        /*
         * Update data
         * */
        $updateDoctorRequest = Yii::$app->db->createCommand()->update('doctor_request', [
                    'doctorcode' => $doctorId,
                    'pincode' => $pincode,
                    'doctorfullname' => $doctorfullname,
//                    'dadd' => $doctorData['dadd'],
                    'addby' => $addby,
                    'hospcode' => $hosp[0],
                    'hospname' => $hosp[1],
                    'docgroup' => $docgroup,
                    'status' => '1',
                ],
                    'id = "'.$doctorId.'"'
                )->execute();

        /*
         * Get new data after edit.
         * */
        $paramsSelectDoctorDetails = [
            ':id' => $doctorId,
        ];

        $select = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE id=:id')->bindValues($paramsSelectDoctorDetails);
        $doctorNewDetails = $select->queryOne();

//        $doctorDetails = [ 'id'=>$doctorId, 'new'=>$doctorNewDetails, 'old'=>$doctorOldDetails ];
        $this->storageDoctorActionLog($doctorNewDetails, 'editBeforeApprove');


        echo $updateDoctorRequest;
    }

    public function actionApproveDoctorRequest(){
        // Get $_POST['doctorId'] and sanitize string.
        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);

        $selectDoctorRequest = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE id=:id')
            ->bindParam(':id', $doctorId);
        $doctorData = $selectDoctorRequest->queryOne();

        /*
         * Insert doctor to v04cascap.doctor with dbcascap.
         * Using dbcascap and iconv because v04cascap using encode tis620 but cloudcascap using utf-8
         * */
        $insertDoctor = Yii::$app->dbcascap->createCommand()->insert('v04cascap.doctor_all', [
            'pincode' => $doctorData['pincode'],
            'doctorfullname' => iconv('UTF-8', 'tis620', $doctorData['doctorfullname']),
            'cca02' => '1',
            'cca02p1' => '1',
            'cca03' => '1',
            'cca04' => '1',
            'cca05' => '1',
            'dadd' => $doctorData['dadd'],
            'addby' => $doctorData['addby'],
            'hospcode' => iconv('UTF-8', 'tis620', $doctorData['hospcode']),
            'hospname' => iconv('UTF-8', 'tis620', $doctorData['hospname']),
            'docgroup' => iconv('UTF-8', 'tis620', $doctorData['docgroup'])
        ])->execute();
//        echo "insert doctor :>".$insertDoctor."<:";
//        exit();

        // If insert success.
        if($insertDoctor){
            // Set paramiter fot select doctor_all in v04cascap after update field doctorcode in v04cascap.doctor_all and update cloudcascap.doctor_all table.
            $paramsSelectDoctorApproveV04Cascap = [':pincode' => $doctorData['pincode'], ':dadd' => $doctorData['dadd'], ':addby' => $doctorData['addby']];

            $selectDoctorApproveV04Cascap = Yii::$app->db->createCommand('SELECT * FROM v04cascap.doctor_all WHERE pincode=:pincode AND dadd=:dadd AND addby=:addby')->bindValues($paramsSelectDoctorApproveV04Cascap);
            $doctorApproveV04Cascap = $selectDoctorApproveV04Cascap->queryOne();
            echo "select doctor : ".$doctorApproveV04Cascap['id'];
            // If select success and have data.
            if(sizeof($doctorApproveV04Cascap)>0){
                // Update doctorcode field.
                $updateDoctorApproveV04Cascap = Yii::$app->db->createCommand()->update('v04cascap.doctor_all', [
                    'doctorcode' => $doctorApproveV04Cascap['id'],
                ],
                    'id = "'.$doctorApproveV04Cascap['id'].'"'
                )->execute();

                // Update cloudcascap.doctor_all table.
                $updateDoctorApproveCloudCascap = Yii::$app->db->createCommand()->update('doctor_all', [
                    'doctorcode' => $doctorApproveV04Cascap['id'],
                    'pincode' => $doctorData['pincode'],
                    'doctorfullname' => $doctorData['doctorfullname'],
                    'cca02' => '1',
                    'cca02p1' => '1',
                    'cca03' => '1',
                    'cca04' => '1',
                    'cca05' => '1',
                    'dadd' => $doctorData['dadd'],
                    'addby' => $doctorData['addby'],
                    'hospcode' => $doctorData['hospcode'],
                    'hospname' => $doctorData['hospname'],
                    'docgroup' => $doctorData['docgroup'],
                ],
                    'id = "'.$doctorApproveV04Cascap['id'].'"'
                )->execute();

                // Set status in doctor_request from 1 (request status) to 2 (approved status).
                /*
                 * Status
                 * 1 Request
                 * 2 Approved
                 * 3 Delete
                 * 4 Decline
                 * 5 Edit when admin approved (Future).
                 * */
                $updateDoctorRequestStatus = Yii::$app->db->createCommand()->update('doctor_request', [
                    'doctorcode' => $doctorId,
                    'cca02' => '1',
                    'cca02p1' => '1',
                    'cca03' => '1',
                    'cca04' => '1',
                    'cca05' => '1',
                    'status' => 2,
                ],
                    'id = "'.$doctorId.'"'
                )->execute();

                echo "Add to V04 is ".(($updateDoctorApproveV04Cascap==1)?"success":"Fail").".===";
                echo "Add to Cloud is ".(($updateDoctorApproveCloudCascap==1)?"success":"Fail").".===";
                echo "Update status doctor request is ".(($updateDoctorRequestStatus==1)?"success":"Fail").".===";

                /*
                 * Insert approve action to log.
                 * */
                $this->storageDoctorActionLog($doctorData, 'approve');

            }
        }
    }

    public function actionDeclineDoctorApprovedRequest(){
        // Get $_POST['doctorId'] and sanitize string.
        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);

        $selectDoctorRequest = Yii::$app->db->createCommand('SELECT comments FROM doctor_request WHERE id=:id')
            ->bindParam(':id', $doctorId);
        $doctorData = $selectDoctorRequest->queryOne();

        $CommentsOld = json_decode($doctorData['comments'],true);

        $updateDoctorRequestStatus = Yii::$app->db->createCommand()->update('doctor_request', [
            'pincode' => $CommentsOld['old']['pincode'],
            'doctorfullname' => $CommentsOld['old']['doctorfullname'],
            'hospcode' => $CommentsOld['old']['hospcode'],
            'hospname' => $CommentsOld['old']['hospname'],
            'docgroup' => $CommentsOld['old']['docgroup'],
            'status' => 2,
            'comments' => $CommentsOld['old']['comments']
        ],
            'id = "'.$doctorId.'"'
        )->execute();

        echo $updateDoctorRequestStatus;
    }

    public function actionApproveRequestDoctorApproved(){
        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);

        $selectDoctorRequest = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE id=:id')
            ->bindParam(':id', $doctorId);
        $doctorData = $selectDoctorRequest->queryOne();
        $doctorCommentArray = json_decode($doctorData['comments'],true);
        $doctorOldData = $doctorCommentArray['old'];
//        print_r($doctorOldData);

        $paramsSelectDoctorApproveV04Cascap = [':pincode' => $doctorOldData['pincode'], ':dadd' => $doctorOldData['dadd'], ':addby' => $doctorOldData['addby']];

        $selectDoctorApproveV04Cascap = Yii::$app->db->createCommand('SELECT * FROM v04cascap.doctor_all WHERE pincode=:pincode AND dadd=:dadd AND addby=:addby')->bindValues($paramsSelectDoctorApproveV04Cascap);
        $doctorApproveV04Cascap = $selectDoctorApproveV04Cascap->queryOne();

        if(sizeof($doctorApproveV04Cascap)>0){
            // Update v04cascap.doctor_all table.
            $updateDoctorAllV04CASCAP = Yii::$app->dbcascap->createCommand()->update('v04cascap.doctor_all', [
                'pincode' => $doctorData['pincode'],
                'doctorfullname' => iconv('UTF-8', 'tis620', $doctorData['doctorfullname']),
                'hospcode' => iconv('UTF-8', 'tis620', $doctorData['hospcode']),
                'hospname' => iconv('UTF-8', 'tis620', $doctorData['hospname']),
                'docgroup' => iconv('UTF-8', 'tis620', $doctorData['docgroup']),
            ],
                'id = "'.$doctorApproveV04Cascap['id'].'"'
            );
            $updateDoctorAllV04CASCAP->execute();

            // Update cloudcascap.doctor_all table.
            $updateDoctorApproveCloudCascap = Yii::$app->db->createCommand()->update('doctor_all', [
                'pincode' => $doctorData['pincode'],
                'doctorfullname' => $doctorData['doctorfullname'],
                'hospcode' => $doctorData['hospcode'],
                'hospname' => $doctorData['hospname'],
                'docgroup' => $doctorData['docgroup'],
            ],
                'id = "'.$doctorApproveV04Cascap['id'].'"'
            );
            $updateDoctorApproveCloudCascap->execute();

            $updateDoctorRequestStatus = Yii::$app->db->createCommand()->update('doctor_request', [
                'status' => 2,
            ],
                'id = "'.$doctorId.'"'
            );
            $updateDoctorRequestStatus->execute();

            $this->storageDoctorActionLog($doctorApproveV04Cascap,'approve');

            echo $updateDoctorAllV04CASCAP&&$updateDoctorApproveCloudCascap&&$updateDoctorRequestStatus;
        }
    }

    public function actionDeclineDoctorRequest(){
        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);
        // Set status in doctor_request from 1 (request status) to 4 (decline status).
        $update = Yii::$app->db->createCommand()->update('doctor_request', [
                'status' => 4
            ],
                'id = "'.$doctorId.'"'
        )->execute();

        /*
         * Insert approve action to log.
         * */
        $selectDoctorRequest = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE id=:id')
            ->bindParam(':id', $doctorId);
        $doctorData = $selectDoctorRequest->queryOne();

        $this->storageDoctorActionLog($doctorData, 'decline');

        echo $update;
    }
    
    public function actionDeleteDoctorApproved(){
        // Get post method
        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);
        $pincode = filter_input(INPUT_POST, 'pincode', FILTER_SANITIZE_STRING);
        $doctorfullname = filter_input(INPUT_POST, 'doctorfullname', FILTER_SANITIZE_STRING);
        $dadd = filter_input(INPUT_POST, 'dadd', FILTER_SANITIZE_STRING);
        $adddby = filter_input(INPUT_POST, 'adddby', FILTER_SANITIZE_STRING);
        $hospcode = filter_input(INPUT_POST, 'hospcode', FILTER_SANITIZE_STRING);
        $hospname = filter_input(INPUT_POST, 'hospname', FILTER_SANITIZE_STRING);
        $docgroup = filter_input(INPUT_POST, 'docgroup', FILTER_SANITIZE_STRING);
        $rstat = filter_input(INPUT_POST, 'rstat', FILTER_SANITIZE_STRING);
        
//        echo "Controller : ".$doctorId." : ".$pincode." : ".$doctorfullname." : ".$dadd." : ".$adddby." : ".$hospcode." : ".$hospname." : ".$docgroup." : ".$rstat;

        /*
         * Update rstat in V04CASCAP.
         * */
        $updateDoctorRequest = Yii::$app->db->createCommand()->update('doctor_all', [
            'rstat' => '3',
        ],
            'id = "'.$doctorId.'"'
        )->execute();

        /*
         * Update rstat in cloudcascap.
         * */
        $updateV04DoctorRequest = Yii::$app->dbcascap->createCommand()->update('doctor_all', [
            'rstat' => '3',
        ],
            'id = "'.$doctorId.'"'
        )->execute();

//        echo "Cascap : ".$updateDoctorRequest."\r\nV04: ".$updateV04DoctorRequest;

        /*
         * If update rstat v04cascap and cloudcascap is success.
         * */
        if($updateDoctorRequest && $updateV04DoctorRequest){
            $paramsSelectDoctorApprovedInRequestTable = [
                ':pincode' => $pincode,
                ':doctorfullname' => $doctorfullname,
                ':dadd' => $dadd,
                ':addby' => $adddby,
                ':hospcode' => $hospcode,
                ':hospname' => $hospname,
                ':docgroup' => $docgroup
            ];

            $selectDoctorApprovedInRequestTable = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE pincode=:pincode AND doctorfullname=:doctorfullname AND dadd=:dadd AND addby=:addby AND hospcode=:hospcode AND hospname=:hospname AND docgroup=:docgroup')->bindValues($paramsSelectDoctorApprovedInRequestTable);
            $doctorApprovedInRequestTable = $selectDoctorApprovedInRequestTable->queryOne();

            $updateDoctorApprovedInRequestTable = Yii::$app->db->createCommand()->update('doctor_request', [
                'status' => '3',
            ],
                'id = "'.$doctorApprovedInRequestTable['id'].'"'
            );

            /*
             * Insert approve action to log.
             * */
            $selectDoctorRequest = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE id=:id')
                ->bindParam(':id', $doctorApprovedInRequestTable['id']);
            $doctorData = $selectDoctorRequest->queryOne();

            $this->storageDoctorActionLog($doctorData, 'delete');

            echo $updateDoctorApprovedInRequestTable->execute();
        }
    }

    public function actionAssignForm(){
        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);
        $fieldName = filter_input(INPUT_POST, 'fieldName', FILTER_SANITIZE_STRING);
        $status = filter_input(INPUT_POST, 'status', FILTER_SANITIZE_STRING);
        $strStatus = ($status=="true")?'1':'0';
        echo $doctorId." ".$fieldName." ".$status.">>".$strStatus."\r\n";

        $updatefieldCCA_all = Yii::$app->db->createCommand()->update('doctor_all', [
            $fieldName => $strStatus,
        ],
            'id = "'.$doctorId.'"'
        )->execute();
        echo "updatefieldCCA_all : ".$updatefieldCCA_all."\r\n";

        $updatefieldCCA_v04All = Yii::$app->db->createCommand()->update('v04cascap.doctor_all', [
            $fieldName => $strStatus,
        ],
            'id = "'.$doctorId.'"'
        )->execute();
        echo "updatefieldCCA_v04All : ".$updatefieldCCA_v04All."\r\n";

        $paramsSelectDoctorAll = [':id' => $doctorId];
        $selectDoctorAll = Yii::$app->db->createCommand('SELECT * FROM doctor_all WHERE id=:id')->bindValues($paramsSelectDoctorAll);
        $doctorAll = $selectDoctorAll->queryOne();

        $paramsSelectDoctorRequest = [
            ':pincode' => $doctorAll['pincode'],
            ':doctorfullname' => $doctorAll['doctorfullname'],
            ':dadd' => $doctorAll['dadd'],
            ':addby' => $doctorAll['addby'],
            ':hospcode' =>$doctorAll['hospcode']
        ];
        $updatefieldCCA_request = Yii::$app->db->createCommand('UPDATE doctor_request SET '.$fieldName.'="'.$strStatus.'" WHERE pincode=:pincode AND doctorfullname=:doctorfullname AND dadd=:dadd AND addby=:addby AND hospcode=:hospcode')->bindValues($paramsSelectDoctorRequest)->execute();
        echo "updatefieldCCA_request : ".$updatefieldCCA_request."\r\n";
    }

    public function actionRequestDataForEditDoctor(){
        $model = new AllHospitalThai;

        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);
        $command = filter_input(INPUT_POST, 'command', FILTER_SANITIZE_STRING);

        if($command=='editDoctorRequest'){
            $distination = "_modalEditDoctor";
            $table = 'doctor_request';
        }else if($command=='editDoctorApproved'){
            $distination = "_modalRequestEditDoctor";
            $table = 'doctor_all';
        }else if($command=='showDoctorDetails'){
            $distination = "_modalViewDoctorDetail";
            $table = 'doctor_all';
        }

        $strSqlDoctor = "SELECT * FROM $table WHERE id='$doctorId'";
        $qryDoctor = Yii::$app->db->createCommand($strSqlDoctor)->queryAll();

        return $this->renderAjax($distination,[
            'qryDoctor'=>$qryDoctor,
            'model'=>$model,
        ]);
    }

    public function actionRequestDataForEditDoctorApproved(){
        $model = new AllHospitalThai;

        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);
        $source = filter_input(INPUT_POST, 'source', FILTER_SANITIZE_STRING);
        $distination = "_modalRequestEditDoctor";

        if($source=='add'){
            $table = 'doctor_request';
        }else if($source=='approved'){
            $table = 'doctor_all';
        }

        $strSqlDoctor = "SELECT * FROM $table WHERE id='$doctorId'";
        $qryDoctor = Yii::$app->db->createCommand($strSqlDoctor)->queryAll();

        return $this->renderAjax($distination,[
            'qryDoctor'=>$qryDoctor,
            'model'=>$model,
            'source'=>$source,
        ]);
    }

    public function actionReceiveRequestEditDoctorApproved(){
        $oldSource = filter_input(INPUT_POST, 'oldSource', FILTER_SANITIZE_STRING);
        $oldDoctorId = filter_input(INPUT_POST, 'oldDoctorId', FILTER_SANITIZE_STRING);
        $oldPincode = filter_input(INPUT_POST, 'oldPincode', FILTER_SANITIZE_STRING);
        $oldDoctorfullname = filter_input(INPUT_POST, 'oldDoctorfullname', FILTER_SANITIZE_STRING);
        $oldHospital = filter_input(INPUT_POST, 'oldHospital', FILTER_SANITIZE_STRING);
        $oldHospitalDetails = explode(" : ",$oldHospital);
        $oldDocgroup = filter_input(INPUT_POST, 'oldDocgroup', FILTER_SANITIZE_STRING);

        $pincode = filter_input(INPUT_POST, 'pincode', FILTER_SANITIZE_STRING);
        $doctorName = filter_input(INPUT_POST, 'doctorName', FILTER_SANITIZE_STRING);
        $hospital = filter_input(INPUT_POST, 'hospital', FILTER_SANITIZE_STRING);
        $hospitalDetails = explode(" : ",$hospital);
        $docgroup = filter_input(INPUT_POST, 'docgroup', FILTER_SANITIZE_STRING);

        $paramsSelectDoctorAll = [
            ':pincode' => $oldPincode,
            ':doctorfullname' => $oldDoctorfullname,
            ':hospcode' => $oldHospitalDetails[0],
            ':hospname' => $oldHospitalDetails[1],
            ':docgroup' => $oldDocgroup,
            ':status' => '2'
        ];
        $selectDoctorAll = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE pincode=:pincode AND doctorfullname=:doctorfullname AND hospcode=:hospcode AND hospname=:hospname AND docgroup=:docgroup AND status=:status')->bindValues($paramsSelectDoctorAll);
        $doctorAll = $selectDoctorAll->queryOne();
        $JsonText = json_encode($doctorAll);

        $dataOldAndNew['old'] = $doctorAll;

        $updateJsonTextToCommentField = Yii::$app->db->createCommand()->update('doctor_request', [
            'comments' => $JsonText,
        ],
            'id = "'.$doctorAll['id'].'"'
        )->execute();

        if($updateJsonTextToCommentField){
            $updateRequestDoctorApproved = Yii::$app->db->createCommand()->update('doctor_request', [
                'pincode' => $pincode,
                'doctorfullname' => $doctorName,
                'hospcode' => $hospitalDetails[0],
                'hospname' => $hospitalDetails[1],
                'docgroup' => $docgroup,
                'status' => '5',
            ],
                'id = "'.$doctorAll['id'].'"'
            )->execute();

            $paramsSelectDoctorRequest = [':id' => $doctorAll['id']];
            $selectDoctorRequest = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE id=:id')->bindValues($paramsSelectDoctorRequest);
            $doctorRequest = $selectDoctorRequest->queryOne();

            $dataOldAndNew['new'] = $doctorRequest;
            $JsonText = json_encode($dataOldAndNew);

            $updateJsonTextToCommentField = Yii::$app->db->createCommand()->update('doctor_request', [
                'comments' => $JsonText,
            ],
                'id = "'.$doctorAll['id'].'"'
            )->execute();

            $this->storageDoctorActionLog($doctorRequest, 'editAfterApproved');
            echo $updateJsonTextToCommentField;
        }
    }

    public function actionGetDataCompare(){
        $doctorId = filter_input(INPUT_POST, 'doctorId', FILTER_SANITIZE_STRING);

        $selectDoctorRequest = Yii::$app->db->createCommand('SELECT * FROM doctor_request WHERE id=:id')->bindParam(':id', $doctorId);
        $doctorData = $selectDoctorRequest->queryOne();
        $doctorCommentArray = json_decode($doctorData['comments'],true);
        $doctorDataNew = $doctorCommentArray['new'];
        $doctorDataOld = $doctorCommentArray['old'];

        $docgroupNew = (($doctorDataNew['docgroup']=='chemo')?'Chemotherapy':(($doctorDataNew['docgroup']=='general')?'General Medical':(($doctorDataNew['docgroup']=='radio')?'Radiologist':(($doctorDataNew['docgroup']=='surgery')?'Surgery':('Other : '.$doctorDataNew['docgroup'])))));
        $docgroupOld = (($doctorDataOld['docgroup']=='chemo')?'Chemotherapy':(($doctorDataOld['docgroup']=='general')?'General Medical':(($doctorDataOld['docgroup']=='radio')?'Radiologist':(($doctorDataOld['docgroup']=='surgery')?'Surgery':('Other: '.$doctorDataOld['docgroup'])))));

        echo $doctorDataNew['pincode']." : ".$doctorDataOld['pincode']." : ".$doctorDataNew['doctorfullname']." : ".$doctorDataOld['doctorfullname']." : ".$doctorDataNew['hospcode']." : ".$doctorDataOld['hospcode']." : ".$doctorDataNew['hospname']." : ".$doctorDataOld['hospname']." : ".$docgroupNew." : ".$docgroupOld;
    }

    private function storageDoctorActionLog($qryData, $strAction){
        date_default_timezone_set("Asia/Bangkok");
        $dadd = date("Y-m-d H:i:s");
        $userAction = Yii::$app->user->identity->id;

        $insertLog = Yii::$app->db->createCommand()->insert('doctor_action_log', [
            'doctor_id' => $qryData['id'],
            'action_by' => $userAction,
            'action_date' => $dadd,
            'action_detail' => json_encode($qryData),
            'status' => $strAction,
        ])->execute();
    }

    private function createTableShowSearchDoctor($qry){
        echo "<table class='tableListDoctor table table-striped table-hover'>";
        echo "<thead><tr><td>#</td><td>เลข ว.</td><td>ชื่อแพทย์</td><td>สถานะ</td><td></td></tr></thead>";
        echo "<tbody>";
        foreach ($qry as $no => $value){
            // Disabled Or Enabled
//            $btnEdit = "<button class='btn btn-primary form-control btnEdit' id='btnEdit' data-toggle='modal' data-target='.bs-editDoctorRequest-modal-lg' ".(($value['status']=='2'||$value['status']=='3'||$value['status']=='5')?'Disabled':'Enabled').">Edit</button>";

//            VarDumper::dump($value,10,true);
            // Have an Don't have
            $btnEdit = ($value['status']=='2'||$value['status']=='3'||$value['status']=='5')?"":"<button class='btn btn-primary form-control btnEdit' id='btnEdit' doctorId='".$value['id']."' data-toggle='modal' data-target='.bs-editDoctorRequest-modal-lg' >Edit</button>";

            $btnEdit = ($value['status']=='2')?((is_null($value['dadd'])||$value['dadd']==''||is_null($value['addby'])||$value['addby']==''||(strlen($value['addby'])<19))?"<i><span title='หากต้องการแก้ไข กรุณาติดต่อผู้ดูแลระบบ'>ไม่สามารถแก้ไขได้</span></i>":"<button class='btn btn-success form-control btnEditDoctorApprovedInAdd' id='btnEditDoctorApprovedInAdd' doctorId='".$value['id']."' data-toggle='modal' data-target='.bs-editDoctorApproved-modal-lg'>Request edit</button>"):$btnEdit;

            echo "<tr><td>".($no+1)."</td><td>".$value['pincode']."</td><td>".$value['doctorfullname']."</td><td>".(
                    ($value['status']=='1'?
                        "<i class='fa fa-refresh fa-2x' aria-hidden='true' style='color: #00FF00'></i> รออนุมัติ" :($value['status']=='2'?
                            "<i class='fa fa-check fa-2x' aria-hidden='true' style='color: #00a65a;'></i> อนุมัติ":($value['status']=='3'?
                                "<i class='fa fa-remove fa-2x' aria-hidden='true' style='color: #d9534f'></i> ลบออกจากระบบ" :($value['status']=='4'?
                                    "<i class='fa fa-exclamation fa-2x' aria-hidden='true' style='color: #808080'></i> ไม่อนุมัติ":($value['status']=='5'?
                                        "<i class='fa fa-edit fa-2x' aria-hidden='true' style='color: #0000FF'></i> ขอแก้ไข" :$value['status']." : อื่นๆ"
                                    )
                                )
                            )
                        )
                    )
                )."</td><td class='text-right'>$btnEdit</td></tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "<div class='text-center'><div class='col-md-offset-4 col-md-4'><button class='btn btn-primary form-control btnAddDoctor' id='btnAddDoctor'>เพิ่มแพทย์</button></div></div>";
    }

    private function createTableShowSearchDoctorRequest($qry){
        echo "<table class='tableListDoctorRequest table table-striped table-hover table-bordered'>";
        echo "<thead>".
            "<tr class='text-center'>".
                "<td width='4%'>#</td>".
                "<td width='10%'>เลข ว.</td>".
                "<td>ชื่อแพทย์</td>".
                "<td>โรงพยาบาล</td>".
                "<td width='15%'>ประเภทความถนัด</td>".
                "<td></td>".
                "<td width='21%' ".(Yii::$app->user->can('adminsite')=='1'?"colspan='3'":"colspan='1'")."></td>".
            "</tr>".
            "</thead>";
        echo "<tbody>";
        foreach ($qry as $no => $value){
            $btnEdit = ($value['status']=='2'||$value['status']=='3')?"":"<button class='btn btn-primary form-control btnEdit' id='btnEdit' doctorId='".$value['id']."' data-toggle='modal' data-target='.bs-editDoctorRequest-modal-lg' >Edit</button>";

            $strUserAdd = "ชื่อ : ".$value['firstname']." ".$value['lastname']." || &#013;เบอร์โทรศัพท์ : ".$value['telephone'];

            echo "<tr class='doctorId".$value['id']."' id='doctorId".$value['id']."' doctorId='".$value['id']."'>".
                    "<td class='tdNo".$value['id']." text-center'>".($no+1)."</td>".
                    "<td class='tdPincode".$value['id']." text-center' ".((strlen($value['pincode'])!=5||is_null($value['pincode']))?"style='background-color: #CD5C5C; color: #CCCC00;' title='เลข ว.ไม่ถูกต้อง'":"").">".$value['pincode']."</td>".
                    "<td class='tdDoctorName".$value['id']."' ".((strlen($value['doctorfullname'])==0||is_null($value['doctorfullname']))?"style='background-color: #CD5C5C; color: #CCCC00;' title='กรุณาระบุชื่อ'":"").">".$value['doctorfullname']."</td>".
                    "<td class='tdHospName".$value['id']."' ".((strlen($value['hospname'])==0||is_null($value['hospname']))?"style='background-color: #CD5C5C; color: #CCCC00;' title='หน่วยบริการไม่ถูกต้อง'":"").">".$value['hospname']."</td>".
                    "<td class='tdDocGroup".$value['id']."' ".((strlen($value['docgroup'])==0||is_null($value['docgroup']))?"style='background-color: #CD5C5C; color: #CCCC00;' title='กรุณาระบุประเภทความถนัด'":"").">".
                    (($value['docgroup']=='chemo')?
                        'Chemotherapy':
                        (($value['docgroup']=='general')?
                            'General Medical':
                            (($value['docgroup']=='radio')?
                                'Radiologist':
                                (($value['docgroup']=='surgery')?
                                    'Surgery':
                                    ('Other : '.$value['docgroup'])
                                )
                            )
                        )
                    ).
                    "</td>".
                    "<td class='tdAddby".$value['id']."'><i class='fa fa-user-plus fa-2x' aria-hidden='true' title='".$strUserAdd."' style='color: #6CC417'></i></td>".
                    "<td width='7%'>$btnEdit</td>".
                    (Yii::$app->user->can('adminsite')=='1'?
                        ("<td width='7%'>".(($value['status']==5)?
                                "<button class='btn btn-success form-control btnApproveRequest' id='btnApproveRequest' ".(($value['status']=='3' || $value['status']=='4')?"Disabled":"").">Approve Request</button>":
                                "<button class='btn btn-success form-control btnApprove' id='btnApprove' ".(($value['status']=='3' || $value['status']=='4')?"Disabled":"").">Approve</button>").
                            "</td>".
                            "<td width='7%'>".(($value['status']==5)?
                                "<button class='btn btn-danger form-control btnDeclineRequest' id='btnDeclineRequest' ".(($value['status']=='3' || $value['status']=='4')?"Disabled":"")." data-toggle='modal' data-target='.bs-declineRequestDoctorApproved-modal-lg'>Decline Request</button>":
                                "<button class='btn btn-danger form-control btnDecline' id='btnDecline' ".(($value['status']=='3' || $value['status']=='4')?"Disabled":"")." data-toggle='modal' data-target='.bs-decline-modal-sm'>Decline</button>").
                            "</td>")
                        :
                    "").
                "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
    }

    private function createTableShowSearchDoctorApproved($qry){
        echo "<table class='tableListDoctorApproved table table-striped table-hover table-bordered'>";
        echo "<thead>".
            "<tr class='text-center'>".
                "<td width='4%' rowspan='2' style='vertical-align: middle;'>#</td>".
                "<td width='5%' rowspan='2' style='vertical-align: middle;'>เลข ว.</td>".
                "<td rowspan='2' style='vertical-align: middle;'>ชื่อแพทย์</td>".
                "<td rowspan='2' style='vertical-align: middle;'>โรงพยาบาล</td>".
                "<td width='10%' rowspan='2' style='vertical-align: middle;'>ประเภทความถนัด</td>".
                "<td width='20%' colspan='5'>กรอกฟอร์ม</td>".
                "<td width='8%' rowspan='2' style='vertical-align: middle;'></td>".
            "</tr>".
            "<tr style='font-size: x-small; text-align: center;'>".
                "<td>CCA-02</td>".
                "<td>CCA-02.1</td>".
                "<td>CCA-03</td>".
                "<td>CCA-04</td>".
                "<td>CCA-05</td>".
            "</tr>".
            "</thead>";
        echo "<tbody>";
        foreach ($qry as $no => $value){
            echo "<tr class='doctorId".$value['id']."' id='doctorId".$value['id']."' doctorId='".$value['id']."'>".
                "<td class='text-center'>".($no+1)."</td>".
                "<td class='text-center'>".$value['pincode']."</td>".
                "<td>".$value['doctorfullname']."</td>".
                "<td>".$value['hospname']."</td>".
                "<td>".
                (($value['docgroup']=='chemo')?
                    'Chemotherapy':
                    (($value['docgroup']=='general')?
                        'General Medical':
                        (($value['docgroup']=='radio')?
                            'Radiologist':
                            (($value['docgroup']=='surgery')?
                                'Surgery':
                                ('Other : '.$value['docgroup'])
                            )
                        )
                    )
                ).
                "</td>".
                "<td width='4%' class='text-center' style='vertical-align: middle;'>".
                    (Yii::$app->user->can('adminsite')!='1'?
                        (
                            $value['cca02']=='1'?
                            '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: #00a65a;" title="สามารถใช้งานฟอร์ม CCA-02 ได้"></i>':
                            '<i class="fa fa-remove fa-2x" aria-hidden="true" style="color: #d9534f" title="ไม่สามารถใช้งานฟอร์ม CCA-02 ได้"></i>'
                        ):(
                            "<input class='ccaCheckbox cca02' id='cca02' type='checkbox' style='width: 30px; height: 30px;' ".
                            ($value['cca02']=='1'?'checked':'').
                            " ".
                            (($value['status']=='4'||$value['status']=='5')?'Disabled':'Enabled').
                            ">"
                        )
                    ).
                "</td>".
                "<td width='4%' class='text-center' style='vertical-align: middle;'>".
                    (Yii::$app->user->can('adminsite')!='1'?
                        (
                        $value['cca02p1']=='1'?
                            '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: #00a65a;" title="สามารถใช้งานฟอร์ม CCA-02.1 ได้"></i>':
                            '<i class="fa fa-remove fa-2x" aria-hidden="true" style="color: #d9534f" title="ไม่สามารถใช้งานฟอร์ม CCA-02.1 ได้"></i>'
                        ):(
                            "<input class='ccaCheckbox cca02p1' id='cca02p1' type='checkbox' style='width: 30px; height: 30px;' ".
                            ($value['cca02p1']=='1'?'checked':'').
                            " ".
                            (($value['status']=='4'||$value['status']=='5')?'Disabled':'Enabled').
                            ">"
                        )
                    ).
                "</td>".
                "<td width='4%' class='text-center' style='vertical-align: middle;'>".
                    (Yii::$app->user->can('adminsite')!='1'?
                        (
                        $value['cca03']=='1'?
                            '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: #00a65a;" title="สามารถใช้งานฟอร์ม CCA-03 ได้"></i>':
                            '<i class="fa fa-remove fa-2x" aria-hidden="true" style="color: #d9534f" title="ไม่สามารถใช้งานฟอร์ม CCA-03 ได้"></i>'
                        ):(
                            "<input class='ccaCheckbox cca03' id='cca03' type='checkbox' style='width: 30px; height: 30px;' ".
                            ($value['cca03']=='1'?'checked':'').
                            " ".
                            (($value['status']=='4'||$value['status']=='5')?'Disabled':'Enabled').
                            ">"
                        )
                    ).
                "</td>".
                "<td width='4%' class='text-center' style='vertical-align: middle;'>".
                    (Yii::$app->user->can('adminsite')!='1'?
                        (
                        $value['cca04']=='1'?
                            '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: #00a65a;" title="สามารถใช้งานฟอร์ม CCA-04 ได้"></i>':
                            '<i class="fa fa-remove fa-2x" aria-hidden="true" style="color: #d9534f" title="ไม่สามารถใช้งานฟอร์ม CCA-04 ได้"></i>'
                        ):(
                            "<input class='ccaCheckbox cca04' id='cca04' type='checkbox' style='width: 30px; height: 30px;' ".
                            ($value['cca04']=='1'?'checked':'').
                            " ".
                            (($value['status']=='4'||$value['status']=='5')?'Disabled':'Enabled').
                            ">"
                        )
                    ).
                "</td>".
                "<td width='4%' class='text-center' style='vertical-align: middle;'>".
                    (Yii::$app->user->can('adminsite')!='1'?
                        (
                        $value['cca05']=='1'?
                            '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: #00a65a;" title="สามารถใช้งานฟอร์ม CCA-05 ได้"></i>':
                            '<i class="fa fa-remove fa-2x" aria-hidden="true" style="color: #d9534f" title="ไม่สามารถใช้งานฟอร์ม CCA-05 ได้"></i>'
                        ):(
                            "<input class='ccaCheckbox cca05' id='cca05' type='checkbox' style='width: 30px; height: 30px;' ".
                            ($value['cca05']=='1'?'checked':'').
                            " ".
                            (($value['status']=='4'||$value['status']=='5')?'Disabled':'Enabled').
                            ">"
                        )
                    ).
                "</td>".
                "<td>".
                    "<button class='btn btn-info form-control btnViewDetail' id='btnViewDetail' ".
                        (($value['status']=='2'||$value['status']=='5')?'Enabled':'Disabled').
                    "data-toggle='modal' data-target='.bs-viewDoctorDetails-modal-lg'>View Detail</button>".
                "</td>".
                "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
    }

    private function getListDoctorRequest(){
        $strSqlDoctorRequest = "SELECT * FROM doctor_request WHERE doctorfullname != '' AND status!='2'";
        $qryDoctorRequest = Yii::$app->db->createCommand($strSqlDoctorRequest)->queryAll();

        return $qryDoctorRequest;
    }

    private function getListDoctorApproved(){
        $strSqlDoctorApproved = "SELECT * FROM doctor_all WHERE doctorfullname != '' LIMIT 20";
        $qryDoctorApproved = Yii::$app->db->createCommand($strSqlDoctorApproved)->queryAll();

        return $qryDoctorApproved;
    }
}
