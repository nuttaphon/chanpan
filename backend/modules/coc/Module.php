<?php

namespace backend\modules\coc;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\coc\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
