<?php

namespace backend\modules\coc\controllers;

use Yii;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\EzformReply;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\VarDumper;

class ReferController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $action = \Yii::$app->request->get('action');

        $userProfile = \Yii::$app->user->identity->userProfile;
        $sql = "SELECT id,
                    Diax,
                    addresstext,
                    Classification,
                    refer_stat,
                    id_no,
                    refer_accept_date,
                    ptid,
                    xsourcex,
                    create_date,
                    `InputUnit`,
                    user_update
                FROM `tbdata_1462172833035880400`
                where `".($action=="accept" ? 'InputUnit' : 'xsourcex')."`=:sitecode2";
        if($_GET['show']<>'all') {
            if ($_GET['start_date'] && $_GET['end_date'] && $_GET['set_between']) {
                $start_date = explode('/', $_GET['start_date']);
                $start_date = ($start_date[2] - 543) . '-' . $start_date[1] . '-' . $start_date[0];
                $end_date = explode('/', $_GET['end_date']);
                $end_date = ($end_date[2] - 543) . '-' . $end_date[1] . '-' . $end_date[0];
                $sql .= " AND create_date BETWEEN '" . $start_date . "' AND '" . $end_date."'";
            }
            if (isset($_GET['refer_stat']) && $_GET['refer_stat'] <> 9) {
                $sql .= " AND refer_stat ='" . $_GET['refer_stat'] . "'";
            }
            if (isset($_GET['classify']) && $_GET['classify'] <> 9) {
                $sql .= " AND Classification ='" . $_GET['classify'] . "'";
            }
        }
        $sql .= " AND rstat <>'3' AND rstat <>'0' ORDER  BY create_date DESC";

        $count = \Yii::$app->db->createCommand($sql, [':sitecode2' => $userProfile->sitecode])->query()->count();

        $referSqlProvider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':sitecode2' => $userProfile->sitecode],
            'pagination' => [
                'pageSize' => 50,
            ],
            'totalCount' => $count
        ]);
        //echo $sql; exit;
        return $this->renderAjax('index', [
            'referSqlProvider' => $referSqlProvider,
        ]);

    }

    public function actionAddRefer($refer_id)
    {

        Yii::$app->db->createCommand()->update('tbdata_1462172833035880400', ['refer_stat'=>1, 'refer_accept_date'=> new Expression('now()')], ['id'=>$refer_id])->execute();

    }

    public function actionConfirm(){

        $queryParams = Yii::$app->request->queryParams;
        $queryParams['ezf_id'] ='1462172833035880400';
        $ezform_comp = EzformQuery::checkIsTableComponent($queryParams['ezf_id']);

        $queryParams = \backend\controllers\InputdataController::actionRegisterFromComponent($ezform_comp['comp_id'], $queryParams['cid']);
        $queryParamsx = \backend\controllers\InputdataController::actionStep2Confirm($queryParams);
        $queryParams = $queryParamsx['queryParams'];
        $dataOtherSite = $queryParamsx['dataOtherSite'];
        //VarDumper::dump($queryParams,10,true);

        $queryParams['refer_id'] =\Yii::$app->request->get('refer_id');

        return $this->renderAjax('_referconfirm', [
            'queryParams' => $queryParams,
            'dataOtherSite' => $dataOtherSite,
        ]);

    }

    public function actionInsertRefer(){
        if($_POST['comp_id']) {
            $result = \backend\controllers\InputdataController::actionInsertSpecial();
            self::actionAddRefer($_POST['refer_id']);
            echo $result;
        }else{
            self::actionAddRefer($_POST['refer_id']);
            $arr['status'] = 'success-in-site';
            return json_encode($arr);
            // ไม่ได้ลงทะเบียน อาจจะมีข้อมูลแล้วในหน่วยงานตนเอง (รับรีเฟอร์เฉยๆ)
        }
    }

    public function actionMoveSite($refer_id=null){
        $res = Yii::$app->db->createCommand("SELECT InputUnit FROM tbdata_1462172833035880400 WHERE id = :id;", [':id'=>$refer_id])->queryOne();
        $sql = "SELECT hcode, `name` FROM all_hospital_thai WHERE `hcode`= :hcode;";
        $initValue = Yii::$app->db->createCommand($sql, [':hcode'=>$res['InputUnit']])->queryOne();
        return $this->renderAjax('_move-site', [
            'initValue'=>$initValue,
            'refer_id' => $refer_id,
        ]);
    }

    public function actionReferDetail($refer_id=null){

        $arr = [];
        foreach ($_POST['dataId'] as $key=>$value) {
            $model = \Yii::$app->db->createCommand("SELECT Diax, addresstext, Classification, refer_stat, id_no, refer_accept_date, ptid, xsourcex, create_date, `InputUnit` FROM `tbdata_1462172833035880400` WHERE id=:id", [':id' => $value])->queryOne();
            $res = \Yii::$app->db->createCommand("SELECT ezf_choicelabel as text FROM ezform_choice WHERE ezf_field_id = '1462335044039469900' AND ezf_choicevalue=:code", [':code' => $model['Classification']])->queryOne();
            $text = "ประเภทความรุนแรง : " . $res['text'];
            $res = \Yii::$app->db->createCommand("SELECT `name` FROM icd10 WHERE code=:code", [':code' => $model['Diax']])->queryOne();
            $text .= "\nผลวินิฉฉัย : " . $res['name'];
            $text .= "\nที่อยู่ : " . $model['addresstext'];
            //
            $arr[$key]['id'] = $value;
            $arr[$key]['text'] = $text;
        }
        header('Access-Control-Allow-Origin: *');
        header("content-type:text/javascript;charset=utf-8");
        echo json_encode($arr);
        return ;
    }

    public function actionMoveSiteSave(){
        $result_error = [
            'status' => 'error',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถดำเนินการได้ โปรดลองใหม่ภายหลัง.'),
        ];
        $result_success = [
            'status' => 'success',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'บันทึกข้อมูลสำเร็จ.'),
        ];
        if($_POST['InputUnit']=='' || $_POST['refer_id']=='')
            return json_encode($result_error);

        $rst = Yii::$app->db->createCommand("SELECT InputUnit FROM tbdata_1462172833035880400 WHERE id = :id;", [':id'=>$_POST['refer_id']])->queryOne();
        //
        $EzformReply = new EzformReply();
        $EzformReply->data_id = $_POST['refer_id'];
        $EzformReply->ezf_id = '1462172833035880400';
        $EzformReply->ezf_comment = 'บันทึกข้อมูล';
        $EzformReply->ezf_json_new = json_encode(['InputUnit'=>$_POST['InputUnit']]);
        $EzformReply->ezf_json_old = json_encode($rst);
        $EzformReply->type= 9 ;
        $EzformReply->create_date=new Expression('NOW()');
        $EzformReply->user_create=Yii::$app->user->id;
        $EzformReply->xsourcex = $model->xsourcex;

        if($EzformReply->save()){
            //end track change
            Yii::$app->db->createCommand()->update('tbdata_1462172833035880400', ['InputUnit'=>$_POST['InputUnit'], 'refer_stat'=>0, 'refer_accept_date'=> new Expression('null'), 'update_date'=> new Expression('now()')], ['id'=>$_POST['refer_id']])->execute();

            $result_success = [
                'status' => 'success',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'บันทึกข้อมูลสำเร็จ.'),
            ];
        }else{
            $result_error = [
                'status' => 'error',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถดำเนินการได้ โปรดลองใหม่ภายหลัง.'),
            ];
        }
        return json_encode($result);
    }


}