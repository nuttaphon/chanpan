<?php

namespace backend\modules\coc\controllers;
use Yii;
use yii\web\Controller;
use backend\modules\coc\models\TbDataCoc;
use common\lib\tcpdf\SDPDF;
use common\lib\phpexcel\SDExcelView;
use backend\modules\coc\classes\OvccaFunc;
use appxq\sdii\helpers\SDHtml;
use yii\helpers\Url;
use yii\web\Response;
class DefaultController extends Controller
{
     
    public function actionIndex()
    {   
        //$this->layout = 'main';
        $sitecode = \Yii::$app->user->identity->userProfile->sitecode;
        //$data = TbDataCoc::findAll(['sitecode'=>$sitecode]);
        //$data->sitecode =  \Yii::$app->user->identity->userProfile->sitecode;
        $html = $this->renderAjax('index');
        //\yii\helpers\VarDumper::dump($html,10,true);
        //echo $html;
        
        return $html;
    }
    public function actionReportOvcca() {
            Yii::$app->response->format = Response::FORMAT_JSON;
	    $url = Url::to(['/coc/default/report']);
	    
	    $html = OvccaFunc::getIframe($url);
            
	    $result = [
		'status' => 'success',
		'action' => 'report',
		'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Load completed.'),
		'html' => $html,
	    ];
	    return $result;
	
    }
    public function actionReport(/*ovfilter, $ovfilter_sub, $selection=null*/)
    {
	      
	// create new PDF document
	//PDF_UNIT = pt , mm , cm , in 
	//PDF_PAGE_ORIENTATION = P , LANDSCAPE = L
	//PDF_PAGE_FORMAT 4A0,2A0,A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,C0,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,RA0,RA1,RA2,RA3,RA4,SRA0,SRA1,SRA2,SRA3,SRA4,LETTER,LEGAL,EXECUTIVE,FOLIO
	$orient = 'P';

	$pdf = new SDPDF($orient, PDF_UNIT, 'A5', true, 'UTF-8', false);
	//spl_autoload_register(array('YiiBase', 'autoload'));

	// set document information
	$pdf->SetCreator('AppXQ');
	$pdf->SetAuthor('iencoded@gmail.com');
	$pdf->SetTitle('Report');
	$pdf->SetSubject('Original');
	$pdf->SetKeywords('AppXQ, SDII, PDF, report, medical, clinic');

	// remove default header/footer
	$pdf->setPrintHeader(TRUE);
	$pdf->setPrintFooter(TRUE);

	// set margins
	$pdf->SetMargins(10, 17, 10);
	$pdf->SetHeaderMargin(10);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 15);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set font
	$pdf->SetFont($pdf->fontName, '', 10);

	/* ------------------------------------------------------------------------------------------------------------------ */
	
	//$sitecode = \Yii::$app->user->identity->userProfile->sitecode;
	
	
	$pdf->contentHeader=[
	    'title'=>'รายงานติดตามกำกับงาน',
	];
	//$pdf->tableHeader=$header;
	$pdf->lineFooter=true;

	// add a page
	$pdf->AddPage();
	//$data = TbDataCoc::findAll(['sitecode' => $sitecode]);
        //$data = "";
	//$pdf->createTableData($header, $data, false);
        $pdf->Cell(10,100,'mmm1');
        $pdf->Cell(20,100,'mmm2');
        $pdf->Cell(30,50,'mmm3');
	$pdf->Output('report.pdf', 'I');
	Yii::$app->end();
    }
    public function actionImportexcel(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $objPHPExcel = \PHPExcel_IOFactory::load(Yii::$app->basePath. '/uploads/Fileexcel.xls'.urlencode($post['excelFile']));
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $header = $sheetData[1];
        \yii\helpers\VarDumper::dump($sheetData[1],10,true);
        exit();
    }
    
    public function actionGeojson(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sql = "select areacode, concat('เขต ',areacode*1) as areaname ,geojson ,areatype co from geojson where areatype = 1";
        $count = \Yii::$app->db->createCommand($sql)->queryAll();

        return $count;
    }
    public function actionGeojson1(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sqldata ="
            SELECT
	a.zonecode,
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1
WHERE 
a.zonecode <> '99'
GROUP BY
a.zonecode
ORDER BY
a.zonecode
        ";
        $sql = "select areacode, concat('เขต ',areacode*1) as areaname ,geojson ,areatype from geojson where areatype = 1";
        $data1 = \Yii::$app->db->createCommand($sqldata)->queryAll();
        $data = \Yii::$app->db->createCommand($sql)->queryAll();
        $feature = array();
        $i = 0;
        foreach ($data1 as $dataresult) {
             //$dataresult[changwatcode];
            $_data[$dataresult[zonecode]] = number_format($dataresult[Result],2);
        }
        foreach ($data as $value) {
            $feature[$i]["type"] = "Feature";
            $feature[$i]["properties"]["id"] = $value[areacode];
            $feature[$i]["properties"]["type"] = $value[areatype];      
            $feature[$i]["properties"]["name"] = $value[areaname];
            $feature[$i]["properties"]["data"] = $_data[$value[areacode]];
            $feature[$i]["geometry"] = json_decode($value[geojson]);
            $i++;
        }
        $result = [
		'type' => 'FeatureCollection',
		'features' => $feature
	    ];
        return json_decode(json_encode($result));
    }
    public function actionGeojson2(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $area = $_GET[area];
        $type = $_GET[type];
        $year = $_GET[year];
        $sqldata = "";
        $sql = "";
        if($type == "1"){
            $sqldata = "
            SELECT
	a.zonecode,
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1
WHERE 
a.zonecode <> '99'
GROUP BY
a.zonecode
ORDER BY
a.zonecode
            ";
            $sql = "select areacode, concat('เขต ',areacode*1) as areaname ,geojson ,areatype from geojson where areatype = 1";
        }elseif ($type == "2") {
            $sqldata = "
            SELECT
	a.zonecode,
	a.changwatcode,
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1
WHERE 
a.zonecode <> '99'
AND
a.zonecode = '$area'
GROUP BY
a.zonecode,
a.changwatcode
ORDER BY
a.zonecode,
a.changwatcode
            ";
            $sql = "select areacode, changwatname as areaname ,geojson ,areatype,zonecode "
                    . "from geojson INNER JOIN cchangwat ON areacode = changwatcode "
                    . "where areatype = 2 and zonecode = '$area'";
        }elseif ($type == "3") {
            $sqldata = "
            SELECT
	a.zonecode,
	a.changwatcode,
	b.ampurcodefull amphurcode,
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1 #and c.hoscode = d.sitecode and d.sitecode = '10668'
WHERE 
a.zonecode <> '99'
AND
a.changwatcode = '$area'
GROUP BY
a.zonecode,
a.changwatcode,
b.ampurcodefull
ORDER BY
a.zonecode,
a.changwatcode,
b.ampurcodefull
            ";
            $sql = "select areacode, ampurname as areaname ,geojson ,areatype ,campur.changwatcode ,changwatname "
                    . "from campur "
                    . "INNER JOIN cchangwat ON cchangwat.changwatcode=campur.changwatcode "
                    . "INNER JOIN geojson ON areacode = ampurcodefull AND length(geojson)>0 "
                    . "where areatype = 3 and cchangwat.changwatcode = '$area'";
        }elseif ($type == "4") {
            $sqldata = "
            SELECT
	a.zonecode,
	a.changwatcode,
	b.ampurcodefull amphurcode,
	c.hoscode,
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	#SUM(d.rstat),
	#SUM(d.refer_stat),
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1 #and c.hoscode = d.sitecode and d.sitecode = '10668'
WHERE 
a.zonecode <> '99'
AND
b.ampurcodefull = '$area'
GROUP BY
a.zonecode,
a.changwatcode,
b.ampurcodefull,
c.hoscode
ORDER BY
a.zonecode,
a.changwatcode,
b.ampurcodefull,
c.hoscode
            ";
            $sql = "select hoscode as areacode , hosname as areaname ,geojson.lat ,geojson.lon ,campur.changwatcode ,changwatname,areatype,chos_ltc.hcode "
                    . "from geojson "
                    . "INNER JOIN campur ON substring(areacode,1,4)=ampurcodefull "
                    . "INNER JOIN cchangwat ON cchangwat.changwatcode=campur.changwatcode "
                    . "INNER JOIN chospital ON chospital.hoscode=geojson.hcode "
                    . "LEFT JOIN chos_ltc ON chos_ltc.hcode = hoscode "
                    . "where areatype = 4 and ampurcodefull = '$area'";
           
        }
        $data1 = \Yii::$app->db->createCommand($sqldata)->queryAll();
        $data = \Yii::$app->db->createCommand($sql)->queryAll();
//        foreach ($data1 as $dataresult) {
//             //$dataresult[changwatcode];
//            $_data[$dataresult[changwatcode]] = $dataresult[Result];
//        }
        $feature = array();
        $i = 0;
        if($type == '1'){
            foreach ($data1 as $dataresult) {
                $_data[$dataresult[zonecode]] = number_format($dataresult[Result],2);
            }
            foreach ($data as $value) {
                $feature[$i]["type"] = "Feature";
                $feature[$i]["properties"]["id"] = $value[areacode];
                $feature[$i]["properties"]["type"] = $value[areatype];
                $feature[$i]["properties"]["name"] = $value[areaname];
                $feature[$i]["properties"]["data"] = $_data[$value[areacode]];
                $feature[$i]["geometry"] = json_decode($value[geojson]);
                $i++;
            }
        }elseif ($type == '2') {
            foreach ($data1 as $dataresult) {
                $_data[$dataresult[changwatcode]] = number_format($dataresult[Result],2);
            }
            foreach ($data as $value) {
                $feature[$i]["type"] = "Feature";
                $feature[$i]["properties"]["id"] = $value[areacode];
                $feature[$i]["properties"]["type"] = $value[areatype];
                $feature[$i]["properties"]["name"] = $value[areaname];
                $feature[$i]["properties"]["data"] = $_data[$value[areacode]];
                $feature[$i]["properties"]["zone"] = $value[zonecode];
                $feature[$i]["geometry"] = json_decode($value[geojson]);
                $i++;
            }
        }elseif ($type == '3') {
            foreach ($data1 as $dataresult) {
                $_data[$dataresult[amphurcode]] = number_format($dataresult[Result],2);
            }
            foreach ($data as $value) {
                $feature[$i]["type"] = "Feature";
                $feature[$i]["properties"]["id"] = $value[areacode];
                $feature[$i]["properties"]["type"] = $value[areatype];
                $feature[$i]["properties"]["name"] = "อ.".$value[areaname]." จ.".$value[changwatname];
                $feature[$i]["properties"]["data"] = $_data[$value[areacode]];
                $feature[$i]["geometry"] = json_decode($value[geojson]);
                $i++;
            }
        }elseif ($type == '4') {
            foreach ($data1 as $dataresult) {
                $_data[$dataresult[hoscode]] = number_format($dataresult[Result],2);
            }
            foreach ($data as $value) {
                $feature[$i]["type"] = "Feature";
                $feature[$i]["properties"]["id"] = $value[areacode];
                $feature[$i]["properties"]["type"] = $value[areatype];
                $feature[$i]["properties"]["name"] = $value[areaname];
                $feature[$i]["properties"]["hcode"] = $value[hcode];
                $feature[$i]["properties"]["data"] = $_data[$value[areacode]];
                $feature[$i]["geometry"]["type"] = "Point";
                
                $feature[$i]["geometry"]["coordinates"] = [$value[lon]*1,$value[lat]*1];
                $i++;
            }
        }
        
        $result = [
		'type' => 'FeatureCollection',
		'features' => $feature
	    ];
       
        //print_r($feature);
        return json_decode(json_encode($result));
        //return $geo;
        //var_dump(json_encode($feature));
        //return $data;
    }
    public function actionGetbar(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $area = $_GET[area];
        $type = $_GET[type];
        $year = $_GET[year];
        $sqldata = "";
        $sql = "";
        if($type == "1"){
            $chartTitle = "ประเทศไทย";
            $sql = "
            SELECT
	a.zonecode,
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	CONCAT('เขต ',a.zonecode) as 'name',
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1
WHERE 
a.zonecode <> '99'
GROUP BY
a.zonecode
ORDER BY
a.zonecode
            ";
            //$data1 = \Yii::$app->db->createCommand($sqldata)->queryAll();
            $data = \Yii::$app->db->createCommand($sql)->queryAll();
            $i = 0;
            foreach ($data as $value){
                $chartLabel[] = $value[name];
                if(!$value[Result]){
                    $dataresult = 0;
                }else{
                    $dataresult = number_format($value[Result],2);
                }
                $chartValue[$i][y] = $dataresult*1;
                $chartValue[$i][code] = $value[name];
                $chartValue[$i][hosp] = "";
                $i++;
            }
            $bar[chartLabel] =  $chartLabel;
            $bar[chartValue] =  $chartValue;
            $bar[chartTitle] =  $chartTitle;
            return $bar;
        }elseif($type == "2"){
            $chartTitle = "เขต ".$area;
            $sql = "SELECT
	a.zonecode,
	a.changwatcode,
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	 a.changwatname as 'name',
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1
WHERE 
a.zonecode <> '99'
AND
a.zonecode = '$area'
GROUP BY
a.zonecode,
a.changwatcode
ORDER BY
a.zonecode,
a.changwatcode";
            //$data1 = \Yii::$app->db->createCommand($sqldata)->queryAll();
            $data = \Yii::$app->db->createCommand($sql)->queryAll();
            $i = 0;
            foreach ($data as $value){
                $chartLabel[] = $value[name];
                if(!$value[Result]){
                    $dataresult = 0;
                }else{
                    $dataresult = number_format($value[Result],2);
                }
                $chartValue[$i][y] = $dataresult*1;
                $chartValue[$i][code] = $value[name];
                $chartValue[$i][hosp] = "";
                $i++;
            }
            $bar[chartLabel] =  $chartLabel;
            $bar[chartValue] =  $chartValue;
            $bar[chartTitle] =  $chartTitle;
            return $bar;
        }elseif($type == "3"){
            $chartTitle = "";
            $sql = "
            SELECT
	a.zonecode,
	a.changwatcode,
	b.ampurcodefull amphurcode,
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	 CONCAT('อ.',b.ampurname) as 'name',
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result',
	CONCAT('จ.',a.changwatname) as 'changwatname'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1 #and c.hoscode = d.sitecode and d.sitecode = '10668'
WHERE 
a.zonecode <> '99'
AND
a.changwatcode = '$area'
GROUP BY
a.zonecode,
a.changwatcode,
b.ampurcodefull
ORDER BY
a.zonecode,
a.changwatcode,
b.ampurcodefull
            ";
            //$data1 = \Yii::$app->db->createCommand($sqldata)->queryAll();
            $data = \Yii::$app->db->createCommand($sql)->queryAll();
            $i = 0;
            foreach ($data as $value){
                $chartLabel[] = $value[name];
                if(!$value[Result]){
                    $dataresult = 0;
                }else{
                    $dataresult = number_format($value[Result],2);
                }
                $chartValue[$i][y] = $dataresult*1;
                $chartValue[$i][code] = $value[name];
                $chartValue[$i][hosp] = "";
                $i++;
            }
            
            $bar[chartLabel] =  $chartLabel;
            $bar[chartValue] =  $chartValue;
            $bar[chartTitle] =  $data[0][changwatname];
            return $bar;
        }elseif($type == "4"){
            $chartTitle = "";
            $sql = "
            SELECT
	a.zonecode,
	a.changwatcode,
	b.ampurcodefull amphurcode,
	c.hoscode as 'name',
	CASE
		WHEN SUM(d.rstat) IS NULL THEN 0
		ELSE SUM(d.rstat)
	END as 'A',
	CASE
		WHEN SUM(d.refer_stat) IS NULL THEN 0
		ELSE SUM(d.refer_stat)
	END as 'B',
	c.hosname as 'hos',
	CASE
		WHEN ((SUM(d.refer_stat) / SUM(d.rstat)) * 100) IS NULL THEN 0
		ELSE ((SUM(d.refer_stat) / SUM(d.rstat)) * 100)
	END
	as 'Result',
	CONCAT('อ.',b.ampurname) as 'ampurname'
FROM
	cchangwat a
LEFT JOIN campur b on a.changwatcode = b.changwatcode
LEFT  JOIN chospital c on b.changwatcode = c.provcode AND b.ampurcode = c.distcode
LEFT  JOIN tbdata_1462172833035880400 d on c.hoscode = d.sitecode and d.rstat = 1 #and c.hoscode = d.sitecode and d.sitecode = '10668'
WHERE 
a.zonecode <> '99'
AND
b.ampurcodefull = '$area'
GROUP BY
a.zonecode,
a.changwatcode,
b.ampurcodefull,
c.hoscode
ORDER BY
a.zonecode,
a.changwatcode,
b.ampurcodefull,
c.hoscode
            ";
            //$data1 = \Yii::$app->db->createCommand($sqldata)->queryAll();
            $data = \Yii::$app->db->createCommand($sql)->queryAll();
            $i = 0;
            foreach ($data as $value){
                $chartLabel[] = $value[name];
                if(!$value[Result]){
                    $dataresult = 0;
                }else{
                    $dataresult = number_format($value[Result],2);
                }
                $chartValue[$i][y] = $dataresult*1;
                $chartValue[$i][code] = $value[name];
                $chartValue[$i][hosp] = $value[hos];
                $i++;
            }
            $bar[chartLabel] =  $chartLabel;
            $bar[chartValue] =  $chartValue;
            $bar[chartTitle] =  $data[0][ampurname];
            return $bar;
        }
    }
    public function actionMap(){
        return $this->render('map');
    }
}
