<?php
use yii\helpers\Url;
use yii\helpers\Html;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use backend\modules\coc\assets\LeafletAsset;
//use dosamigos\highcharts\HighCharts;
//Yii::$app->layout = '@backend\modules\coc\views\layouts\main';
//$view = new \yii\web\View();
\backend\assets\BackendAsset::register($this);
LeafletAsset::register($this);
$this->title = 'COC Module';

?>

<!--<div class="coc-default-index">
    <a href="/coc/refer/?ezf_id=1462172833035880400&ezf_field_id=1463903608052210300">
    <span class="fa-stack fa-lg" style="font-size: 50px;">
      <i class="fa fa-square-o fa-stack-2x"></i>
      <i class="fa fa-external-link fa-stack-1x"></i>
    </span>
    Refer Tools</a>
</div>-->

<?php /*= $this->render('_Link');*/ ?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-info flat">
            <div class="box-header with-border">
              <h3 class="box-title">แผนที่ประเทศไทย</h3>
              <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                
              </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                
              <?php
              /*echo Html::button('<span class="glyphicon glyphicon-print"></span> พิมพ์รายงาน'
                        , ['id'=>'reportOvcca'
                        , 'data-url'=>Url::to(['/coc/default/report-ovcca', 'fid'=>$ovfilter, 'sid'=>$ovfilter_sub])
                        , 'class' => 'btn flat  btn-default btn-sm']).' ';*/
              ?>
                <div id="map" style="width: 100%; height:550px; border: none; "></div>
                <div id="info"></div>
            </div><!-- /.box-body -->


        </div><!-- /.box -->
    </div> 
    <div class="col-md-6">
        <div class="box box-info flat">
            <div class="box-header with-border">
              <h3 class="box-title">ข้อมูลการลงทะเบียน</h3>
              <div class="box-tools pull-right">
                <!-- Buttons, labels, and many other things can be placed here! -->
                
              </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div id="bar-chart" style="hight:400px;margin:0 auto"></div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>  
</div>
<?=  ModalForm::widget([
    'id' => 'modal-ov-person',
    'size'=>'modal-lg',
]);
?>

<?=  ModalForm::widget([
    'id' => 'modal-ovlist',
    'size'=>'modal-lg',
]);
?>
<?php  $this->registerJs("
$('#reportOvcca, #reportOvcca1').on('click', function() {
    var url = $(this).attr('data-url');
    selectionOvPersonReport(url);
});
function selectionOvPersonReport(url) {
    $('#modal-ov-person .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-ov-person').modal('show');
    $.ajax({
	method: 'POST',
	url: url,
	
	dataType: 'JSON',
	success: function(result, textStatus) {
	    if(result.status == 'success') {
		". SDNoty::show('result.message', 'result.status') ."
		    $('#modal-ov-person .modal-content').html(result.html);
	    } else {
		". SDNoty::show('result.message', 'result.status') ."
	    }
	}
    });
}
function loadContent(type,id){
    //var divid = 'bar-chart'
    //console.log(id);
   
    $.ajax({
        dataType:'json',
        url:'coc/default/getbar',
        async:true,
        data:{type:type,area:id},
        success:function(data){
            var resp = data;
            bar('bar-chart', 80 ,data.chartLabel,data.chartValue, '' ,data.chartTitle);
            //console.log(data.chartValue);
        }
    });
}

var geojson ,pjson,ampjson ,hjson ,type ,id ,zone ,labelLoading;
var googleLayer ,osm;
var map = L.map('map').setView([13, 100], 8);
var label = new L.Label();
var info = L.control();
info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
};
var customControl =  L.Control.extend({

  options: {
    position: 'topleft'
  },

  onAdd: function (map) {
    var container = L.DomUtil.create('input','btn bg-navy ');
    container.type=\"button\";
    //container.title=\"No cat\";
    container.value = \"ย้อนกลับ\";

    //container.style.backgroundColor = 'white';     
    //container.style.backgroundSize = \"30px 30px\";
    //container.style.width = '70px';
    //container.style.height = '30px';
    
    container.onmouseover = function(){
      //container.style.backgroundColor = 'pink'; 
    }
    container.onmouseout = function(){
      //container.style.backgroundColor = 'white'; 
    }

    container.onclick = function(){
      if(typeof hjson ===\"object\"){
        map.removeLayer(hjson);
        hjson=\"\";
        if(typeof ampjson ===\"object\"){
            ampjson.setStyle(style);
            map.addLayer(ampjson);
            map.fitBounds(ampjson.getBounds());
            type = type -1;
            if(type==\"3\"){
                var p = id.substring(0,2);
                loadContent(type,p);
            }else if(type==\"2\"){
                alert(zone);
                loadContent(type,zone);
            }else if(type==\"1\"){
                loadContent(type,\"\");
            }
        }    
      }else if(typeof ampjson ===\"object\"){
        map.removeLayer(ampjson);
        if(typeof pjson ===\"object\"){
            ampjson=\"\";
            map.addLayer(pjson);
            map.fitBounds(pjson.getBounds());  
            type = type -1;
            if(type==\"3\"){
                var p = id.substring(0,2);
                loadContent(type,p);
            }else if(type==\"2\"){
                loadContent(type,zone);
            }else if(type==\"1\"){
                loadContent(type,\"\");
            }
        }else{
        map.removeLayer(ampjson);
        map.addLayer(geojson);
        map.fitBounds(geojson.getBounds());
        loadContent(\"1\",\"00\");
        }    
      }else{
        if(typeof pjson ===\"object\"){
            map.removeLayer(pjson);
        }    
        map.addLayer(geojson);
        map.fitBounds(geojson.getBounds());
        loadContent(\"1\",\"00\");
      }
    }

    return container;
  }
});
map.options.maxZoom = 20;
map.options.minZoom = 6;
var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png');
baseLayer.addTo(map);
map.addControl(new customControl());
map.on(\"zoomend\", function (e) { 
                    if (map.getZoom() === 6) {
                        if(typeof ampjson ===\"object\"){
                            map.removeLayer(ampjson);
                        }
                        if(typeof pjson ===\"object\"){
                            map.removeLayer(pjson);
                        }
                    }    
                }); 
var village_layer;
$.getJSON('coc/default/geojson1', function (data) {
    village_layer = L.geoJson(data, {
    style: style,  
    onEachFeature:onEachFeature
    }).addTo(map);
    geojson = village_layer;
    if(typeof geojson === 'object'){
        console.log('ok');
    }else{
        console.log('no ok');
    }
    map.fitBounds(village_layer.getBounds());
    //bar('bar-chart');
    loadContent(1,00);
});
    function getColor(code) {
        switch (code) {
            case '05':
                return 'lime';
            case '10':
                return 'red';
            default:
                return 'blue';
        }
    }
    function style(feature) {
        return {
            fillColor: getColor(),
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 1.0
        }
    }
    function onEachFeature(feature, layer) {
			layer.on({
				mouseover: highlightFeature,
				mouseout: resetHighlight,
				click: zoomToFeature
			});
		}
    function highlightFeature(e) {
        var layer = e.target;
        
/////////
                        label.setContent(\"<h4>\"+e.target.feature.properties.name+\"</h4>KPI = \"+e.target.feature.properties.data);
                        label.setLatLng(e.latlng);
                        map.showLabel(label);
			/*layer.setStyle({
				weight: 5,
				color: '#666',
				dashArray: '',
				fillOpacity: 0.7
			});*/
			if (!L.Browser.ie && !L.Browser.opera) {
				//layer.bringToFront();
			}

	
/////////
        layer.setStyle({
            weight: 5,
            color: '#B5E61D',
            dashArray: '',
            fillOpacity: 0.7
        });        
        update(layer.feature.properties);
    }
    function resetHighlight(e) {
        village_layer.resetStyle(e.target);
        label.close();
        $('#info').html('');
    }
    function zoomToFeature(e) {
        id =e.target.feature.properties.id ;
        type = (e.target.feature.properties.type *1)+1 ;
        if(type == '2'){
            if(typeof pjson ===\"object\"){
                map.removeLayer(pjson);
            }
            map.removeLayer(village_layer);
            $.ajax({
            dataType: 'json',
            url:'coc/default/geojson2?area='+id+'&type='+type,
            success:function(data){
               map.removeLayer(village_layer);
               pjson = L.geoJson(data, {
                    style: style,  
                    onEachFeature:onEachFeature
                }).addTo(map);
                
                map.fitBounds(pjson.getBounds());
            }
            }).error(function(){});
            loadContent(type,id);
        }else{
            map.removeLayer(pjson);
        }
        if(type == '3'){
            zone =e.target.feature.properties.zone ;
            if(typeof pjson ===\"object\"){
                map.removeLayer(pjson);
            }
            if(typeof ampjson ===\"object\"){
                map.removeLayer(ampjson);
            }
            map.removeLayer(village_layer);
            $.ajax({
            dataType: 'json',
            url:'coc/default/geojson2?area='+id+'&type='+type,
            success:function(data){
               map.removeLayer(village_layer);
               ampjson = L.geoJson(data, {
                    style: style,  
                    onEachFeature:onEachFeature
                }).addTo(map);
                
                map.fitBounds(ampjson.getBounds());
                //ampjson = village_layer;
            }
            }).error(function(){});
            loadContent(type,id);
        }else{
            map.removeLayer(ampjson);
        }
        if(type == '4'){
        var redMarker = L.ExtraMarkers.icon({
            icon: 'fa-coffee',
            markerColor: 'red',
            shape: 'square',
            prefix: 'fa'
        });
        map.removeLayer(village_layer);
            $.ajax({
            dataType: 'json',
            url:'coc/default/geojson2?area='+id+'&type='+type,
            success:function(data){
               map.removeLayer(village_layer);
               hjson = L.geoJson(data, {
                    //style: style,  
                    onEachFeature:onEachFeature,
                    //icon: redMarker
                    pointToLayer: function (feature, latlng) {                                
                                if(feature.properties.hcode == null){
                                    return L.marker(latlng,{
                                    icon: L.ExtraMarkers.icon({
                                            icon: 'fa-hospital-o',
                                            shape: 'square',
                                            markerColor: 'yellow',
                                            prefix: 'fa',
                                            iconColor: 'black'
                                        })
                                    });
                                }else{
                                    return L.marker(latlng,{
                                    icon: L.ExtraMarkers.icon({
                                            icon: 'fa-hospital-o',
                                            shape: 'square',
                                            markerColor: 'orange-dark',
                                            prefix: 'fa',
                                            iconColor: 'black'
                                        })
                                    });
                                }
                                
			}
                }).addTo(map);
                map.fitBounds(hjson.getBounds());
            }
            }).error(function(){});
            loadContent(type,id);
        }else{
            map.removeLayer(hjson);
        }
    }
    function update(props) {
        var moo = props.VILL;
        var tam = props.name;
        var content = 'หมู่ที่' + moo + ' ต.' + tam;
        $('#info').html(content);
       
    }
        ");
?>