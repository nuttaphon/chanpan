<?php
/* @var $this yii\web\View */
use kartik\grid\GridView;
use yii\helpers\Html;
use common\lib\sdii\widgets\SDModalForm;
use backend\assets\AjaxAsset;
AjaxAsset::register($this);  // $this represents the view object
$this->registerCss('
.tooltip-inner {
    max-width: 600px;
    padding: 3px 8px;
    color: #fff;
    background-color: #000;
    border-radius: 4px;
    text-align: left;
    font-size: medium;
    white-space: pre;
}
');

$this->title = Yii::t('app', 'Refer');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
        <form id="view-data-range" action="" method="get">
            <div class="col-md-6">
                ระบุช่วงเวลา <?php echo Html::checkbox('set_between', $_GET['set_between']); ?>
                <?php
                echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                    //'model'=>$model,
                    'id'=>'start_date',
                    'value' => $_GET['start_date'] ? $_GET['start_date'] : '01'.(date('-m-').(date('Y')+543)),
                    'name' =>'start_date',
                ]);
                ?>
            </div>
            <div class="col-md-6">
                ถึง
                <?php
                echo \common\lib\damasac\widgets\DMSDateWidget::widget([
                    //'model'=>$model,
                    'id'=>'end_date',
                    'value' => $_GET['end_date'] ? $_GET['end_date'] : (date('d-m-').(date('Y')+543)),
                    'name' =>'end_date',
                ]);
                ?>
            </div>
            <div class="col-md-12">
                <?php
                echo  Html::label('สถานะการรับ Refer');
                echo Html::radioList('refer_stat', $_GET['refer_stat'], ['9'=>'ทั้งหมด', '0'=>'ยังไม่รับ', '1'=>'รับแล้ว'], ['class'=>'form-control']);
                $items = \Yii::$app->db->createCommand("SELECT ezf_choicevalue, ezf_choicelabel FROM ezform_choice WHERE ezf_field_id = '1462335044039469900';")->queryAll();
                $items[] = [
                        'ezf_choicevalue' => 9,
                        'ezf_choicelabel' => 'ทั้งหมด',
                ];
                $items = \yii\helpers\ArrayHelper::map($items, 'ezf_choicevalue', 'ezf_choicelabel');
                echo  Html::label('ประเภทความรุนแรง');
                echo Html::radioList('classify', $_GET['classify'], $items, ['class'=>'form-control']);
                ?>
            </div>
            <div class="col-md-12 text-right">
                <br>
                <?php
                echo Html::hiddenInput('action', $_GET['action']);
                echo ' '.Html::a('<span class="fa fa-list"></span> แสดงข้อมูลทั้งหมด',
                        \yii\helpers\Url::to(['/coc/refer/index/', 'classify'=>9, 'refer_stat'=>9, 'action' => $_GET['action'], 'show'=>'all']),
                        [
                            'class' => 'btn btn-danger  btn-md',
                            'type' =>'submit',
                        ]);
                ?>
                <?php
                echo Html::button('<span class="fa fa-search"></span> ค้นหาตามเงื่อนไข', [
                    'class' => 'btn btn-primary btn-md',
                    'type' =>'submit',
                ]);
                ?>
            </div>
        </form>
    </div><hr>

<?php
echo GridView::widget([
    'dataProvider'=>$referSqlProvider,
    'id' => 'grid-refer-confirm',
    //'filterModel'=>$searchModel,
    // 'columns'=>$gridColumns,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'resizableColumns'=>true,
    'columns'=>[
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'format' => 'text',
            'label' => 'ชื่อ - สกุล',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-left'],
            'value' => function($model){
                $sql="SELECT hsitecode,
                            hptcode,
                            `name`,
                            surname
                      FROM tb_data_coc
                      WHERE ptid=:ptid
                      AND rstat <>'3'
                      AND rstat <>'0'
                      ORDER BY id DESC LIMIT 1";
                $register = \Yii::$app->db->createCommand($sql,[':ptid' => $model['ptid']])->queryOne();
                return $register['hsitecode'].' '.$register['hptcode'].' '.$register['name'].' '.$register['surname'];
            }
        ],
        [
            'format' => 'raw',
            'label' => 'ส่งจาก',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $sql="SELECT
                      hcode,name
                      FROM all_hospital_thai
                      WHERE hcode=:xsourcex
                      LIMIT 1";
                $hospital = \Yii::$app->db->createCommand($sql,[':xsourcex' => $model['xsourcex']])->queryOne();
                return '<h5><i class="fa fa-ambulance"></i> '.Html::label($hospital['hcode'],null,['data-toggle' =>'tooltip', 'data-original-title' =>$hospital['name'],'class'=>'label label-danger']).'</h5>';

            }
        ],
        [
            'label' => 'วันที่ส่ง',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $date = new DateTime($model['create_date']);
                return $date->format('d/m/').($date->format('Y')+543);
            }
        ],
        [
            'format' => 'raw',
            'label' => 'ส่งต่อไปที่',
            'headerOptions' => ['class' => 'text-center',  'style'=>'width:90px;'],
            'contentOptions' => ['class' => 'text-center'],
            'value' => function($model){
                $sql="SELECT
                      hcode,name
                      FROM all_hospital_thai
                      WHERE hcode=:hospitalName2
                      LIMIT 1";
                $hospital = \Yii::$app->db->createCommand($sql,[':hospitalName2' => $model['InputUnit']])->queryOne();
                if($model['InputUnit']==null)
                    return '<p class="text-muted">ไม่ระบุ</p>';
                else
                    return '<h5><i class="fa fa-heartbeat"></i> '.Html::label($hospital['hcode'],null,['data-toggle' =>'tooltip', 'data-original-title' =>$hospital['name'],'class'=>'label label-success', 'data-toggle'=>'tooltip',]).'</h5>';
            }
        ],
        [
            'label' => 'วันที่รับ Refer',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                $date = new DateTime($model['refer_accept_date']);
                return $model['refer_accept_date'] ? $date->format('d/m/').($date->format('Y')+543) : 'ยังไม่รับ';
            },

        ],
        [
            'format' => 'raw',
            'label' => 'สถานะ',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model)
            {
                if(\Yii::$app->request->get('action')) {
                    $html = Html::button('รับ Refer', [
                        'data-pjax' => '0',
                        'class' => 'btn btn-success btn-md refer-confirm',
                        'disabled' => $model['refer_stat'] ? true : false,
                        'data-url' => '/coc/refer/confirm/?refer_id=' .$model['id']. '&cid='. $model['id_no'],
                    ]);
                }
                else {
                    $html = '<h5>'.Html::label($model['refer_stat'] ? 'รับแล้ว' : 'รอรับ Refer...', null, ['class' => $model['refer_stat'] ? 'label label-primary' : 'label label-success',]).'</h5>';
                }
                return $html;
//                return Html::a('<i '.$icon.'></i>', Url::to(['ov-person/status', 'id'=>$data->id]), [
//                    'class' => 'btn btn-xs btn-'.$btn,
//                    'data-method' => 'post',
//                    'data-action' => 'ov01-status',
//                ]
            }
        ],
        [
            'format' => 'raw',
            'label' => 'ย้ายสถานบริการ',
            'headerOptions' => ['class' => 'text-center', 'style'=>'width:70px;'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model)
            {
                return Html::button("<i class='fa fa-file-text-o'></i> ย้าย",
                    [
                        'data-url' => '/coc/refer/move-site/?refer_id=' .$model['id'],
                        'class' => 'btn btn-danger btn-move-site',
                        'data-toggle' =>'tooltip', 'data-original-title' => 'ย้ายสถานบริการ',
                        'target' => '_blank',
                        'data-pjax' => '0',
                    ]);
            }
        ],
        [
            'format' => 'raw',
            'label' => 'ดูฟอร์ม',
            'headerOptions' => ['class' => 'text-center',  'style'=>'width:70px;'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model)
            {
                $res = \Yii::$app->db->createCommand("SELECT ezf_choicelabel as text FROM ezform_choice WHERE ezf_field_id = '1462335044039469900' AND ezf_choicevalue=:code",[':code' => $model['Classification']])->queryOne();
                $text = "ประเภทความรุนแรง : ".$res['text'];
                $res = \Yii::$app->db->createCommand("SELECT name FROM icd10 WHERE code=:code",[':code' => $model['Diax']])->queryOne();
                $text .= "\nผลวินิฉฉัย : ".$res['name'];
                $text .= "\nที่อยู่ : ".$model['addresstext'];
                return Html::a("<i class='fa fa-file-text-o'></i> ดูฟอร์ม",
                    [
                        '/inputdata/redirect-page',
                        'dataid' => $model['id'], 'ezf_id' => '1462172833035880400','rurl'=> base64_encode(\Yii::$app->request->url)
                    ],
                    [
                        'class' => 'btn btn-primary',
                        'data-toggle' =>'tooltip', 'data-original-title' => $text,
                        'target' => '_blank',
                        'data-pjax' => '0'
                    ]);
            }
        ],

    ],
    'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>false, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar'=> [
        ['content'=> ''
        ],
        '{export}',
        '{toggleData}',
    ],
    // set export properties
    'export'=>[
        'fontAwesome'=>true
    ],
    // parameters from the demo form
    'responsive'=>true,
    //'showPageSummary' => true,
    'panel'=>[
        'type'=>\Yii::$app->request->get('action') ? GridView::TYPE_SUCCESS : GridView::TYPE_DANGER,
    ],
    'persistResize'=>false,
]);

echo SDModalForm::widget([
    'id' => 'modal-query-request',
    'size' => 'modal-lg',
    'tabindexEnable' => false
]);

$this->registerJs("
$('#grid-refer-confirm').on('click', '.btn-move-site', function(){
        modalQueryRequest($(this).attr('data-url'));
        $('#modal-query-request').on('hidden.bs.modal', function () {
            // do something…
            location.reload(true);
        })
});
$('#grid-refer-confirm').on('click', '.refer-confirm', function(){
        modalQueryRequest($(this).attr('data-url'));
        $('#modal-query-request').on('hidden.bs.modal', function () {
            // do something…
            location.reload(true);
        })
});
function modalQueryRequest(url) {
    $('#modal-query-request .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-query-request').modal('show')
    .find('.modal-content')
    .load(url);
}
", \yii\web\View::POS_END);
?>