<?php

namespace backend\modules\coc\models;

use Yii;

/**
 * This is the model class for table "refer_to".
 *
 * @property string $id
 * @property string $dataid
 * @property string $hsitecode
 * @property string $hptcode
 * @property string $target
 * @property string $sitefrom
 * @property string $create_date
 * @property string $siteto
 * @property string $update_date
 * @property integer $status
 */
class Refer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'refer_to';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'dataid', 'target', 'status'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['hsitecode', 'hptcode'], 'string', 'max' => 10],
            [['sitefrom', 'siteto'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dataid' => 'Dataid',
            'hsitecode' => 'Hsitecode',
            'hptcode' => 'Hptcode',
            'target' => 'Target',
            'sitefrom' => 'Sitefrom',
            'create_date' => 'Create Date',
            'siteto' => 'Siteto',
            'update_date' => 'Update Date',
            'status' => 'Status',
            'ezf_id' => 'Ezf ID',
        ];
    }
}
