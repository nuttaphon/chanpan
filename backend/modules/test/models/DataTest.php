<?php

namespace backend\modules\test\models;

use Yii;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "data_test".
 *
 * @property integer $dt_id
 * @property string $dt_name
 * @property string $dt_text
 * @property integer $dt_province
 * @property integer $dt_enable
 * @property string $dt_tel
 * @property string $dt_email
 * @property double $dt_num
 * @property integer $dt_public
 * @property string $dt_like
 * @property string $dt_input
 * @property integer $dt_geography
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class DataTest extends \yii\db\ActiveRecord
{
    public function behaviors() {
	    return [
		    [
			    'class' => TimestampBehavior::className(),
			    'value' => new Expression('NOW()'),
		    ],
		    [
			    'class' => BlameableBehavior::className(),
		    ],
	    ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dt_name'], 'required'],
            [['dt_text',  'dt_input'], 'string'],
            [['dt_province', 'dt_enable', 'dt_public', 'dt_geography', 'created_by', 'updated_by'], 'integer'],
            [['dt_num'], 'number'],
	    [['dt_email'], 'email'],
            [['created_at', 'updated_at', 'dt_like'], 'safe'],
            [['dt_name', 'dt_tel', 'dt_email'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dt_id' => Yii::t('app', 'ID'),
            'dt_name' => Yii::t('app', 'ชื่อ'),
            'dt_text' => Yii::t('app', 'ที่อยู่'),
            'dt_province' => Yii::t('app', 'จังหวัด'),
            'dt_enable' => Yii::t('app', 'เปิดใช้งาน'),
            'dt_tel' => Yii::t('app', 'เบอร์โทร'),
            'dt_email' => Yii::t('app', 'E-mail'),
            'dt_num' => Yii::t('app', 'ราคา'),
            'dt_public' => Yii::t('app', 'Public'),
            'dt_like' => Yii::t('app', 'สิ่งที่ชอบ'),
            'dt_input' => Yii::t('app', 'Input'),
            'dt_geography' => Yii::t('app', 'ภาค'),
            'created_at' => Yii::t('app', 'สร้างเวลา'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'updated_at' => Yii::t('app', 'แก้ไขเวลา'),
            'updated_by' => Yii::t('app', 'แก้ไขโดย'),
        ];
    }
}
