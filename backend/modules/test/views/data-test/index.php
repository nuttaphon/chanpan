<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\test\models\DataTestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Data Tests');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="data-test-index">

    <?php  Pjax::begin(['id'=>'data-test-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'data-test-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['data-test/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-data-test']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['data-test/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-data-test', 'disabled'=>true]),
	'dataProvider' => $dp,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionDataTestIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],
	    
            'dt_name',
            'dt_text:ntext',
	    [
			    'attribute'=>'dt_province',
			    'value'=>function ($data){ 
				    $dataProv = \backend\modules\test\classes\TestQuery::getProvinceByID($data['dt_province']);
				    return isset($dataProv['PROVINCE_NAME'])?$dataProv['PROVINCE_NAME']:''; 
			    },
			    'headerOptions'=>['style'=>'text-align: center;'],
			    'contentOptions'=>['style'=>'width:100px; text-align: center;'],
	    ],
	    [
			    'attribute'=>'dt_enable',
			    'value'=>function ($data){ 
				
				    return $data['dt_enable']==1?'YES':'NO'; 
			    },
			    'headerOptions'=>['style'=>'text-align: center;'],
			    'contentOptions'=>['style'=>'width:100px; text-align: center;'],
	    ],
            // 'dt_tel',
            // 'dt_email:email',
            // 'dt_num',
            // 'dt_public',
            // 'dt_like:ntext',
            // 'dt_input:ntext',
            // 'dt_geography',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-data-test',
    'size'=>'modal-lg',
    'tabindexEnable'=>false
]);
?>

<?php  $this->registerJs("
$('#data-test-grid-pjax').on('click', '#modal-addbtn-data-test', function() {
    modalDataTest($(this).attr('data-url'));
});

$('#data-test-grid-pjax').on('click', '#modal-delbtn-data-test', function() {
    selectionDataTestGrid($(this).attr('data-url'));
});

$('#data-test-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#data-test-grid').yiiGridView('getSelectedRows');
	disabledDataTestBtn(key.length);
    },100);
});

$('#data-test-grid-pjax').on('click', '.selectionDataTestIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledDataTestBtn(key.length);
});

$('#data-test-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalDataTest('".Url::to(['data-test/update', 'id'=>''])."'+id);
});	

$('#data-test-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalDataTest(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#data-test-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledDataTestBtn(num) {
    if(num>0) {
	$('#modal-delbtn-data-test').attr('disabled', false);
    } else {
	$('#modal-delbtn-data-test').attr('disabled', true);
    }
}

function selectionDataTestGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionDataTestIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#data-test-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalDataTest(url) {
    $('#modal-data-test .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-data-test').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>
