<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\test\models\DataTest */

$this->title = Yii::t('app', 'Create Data Test');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Data Tests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-test-create">

    <?= $this->render('_form', [
        'model' => $model,
	'dpProv' => $dpProv,
	'dpInput' => $dpInput,
    ]) ?>

</div>
