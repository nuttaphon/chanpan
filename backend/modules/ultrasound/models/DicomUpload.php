<?php

namespace backend\modules\ultrasound\models;

use Yii;

/**
 * This is the model class for table "dicom_upload".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $dcm_file
 * @property string $jpg_file
 * @property string $sitecode
 * @property string $ptcode
 * @property integer $status
 */
class DicomUpload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dicom_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'dcm_file', 'jpg_file', 'sitecode', 'ptcode'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['dcm_file', 'jpg_file'], 'string', 'max' => 100],
            [['sitecode', 'ptcode'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'dcm_file' => 'Dcm File',
            'jpg_file' => 'Jpg File',
            'sitecode' => 'Sitecode',
            'ptcode' => 'Ptcode',
            'status' => 'Status',
        ];
    }
}
