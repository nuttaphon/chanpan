<?php

namespace backend\modules\ultrasound\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ultrasound\models\DicomUpload;

/**
 * DicomUploadSearch represents the model behind the search form about `backend\modules\ultrasound\models\DicomUpload`.
 */
class DicomUploadSearch extends DicomUpload
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'status'], 'integer'],
            [['dcm_file', 'jpg_file', 'sitecode', 'ptcode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DicomUpload::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'dcm_file', $this->dcm_file])
            ->andFilterWhere(['like', 'jpg_file', $this->jpg_file])
            ->andFilterWhere(['like', 'sitecode', $this->sitecode])
            ->andFilterWhere(['like', 'ptcode', $this->ptcode]);

        return $dataProvider;
    }
}
