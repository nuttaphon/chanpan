<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use appxq\sdii\widgets\GridView;
use appxq\sdii\widgets\ModalForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\ultrasound\models\DicomUploadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dicom Uploads';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dicom-upload-index">

    <div class="sdbox-header">
	<h3>อัพโหลดภาพอัลตร้าซาวน์</h3>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p style="padding-top: 10px;">
<?php
    echo FileInput::widget([
        'name' => 'dcmfile[]',
        'options' => ['accept' => 'application/dicom'],
        'options'=>[
            'multiple'=>true
        ],
        'pluginOptions' => [
            'uploadUrl' => Url::to(['dicom-upload/file-upload']),
        ]
    ]);
?>        
	<span class="label label-primary">หมายเหตุ</span>
	<?= Yii::t('app', 'หย่อนไฟล์ DCM (DICOM Files) ลงบนนี้ หรือ กด Browse หาไฟล์ DICOM') ?>
    </p>

    <?php  Pjax::begin(['id'=>'dicom-upload-grid-pjax']);?>
    <?= GridView::widget([
	'id' => 'dicom-upload-grid',
	'panelBtn' => Html::button(SDHtml::getBtnAdd(), ['data-url'=>Url::to(['dicom-upload/create']), 'class' => 'btn btn-success btn-sm', 'id'=>'modal-addbtn-dicom-upload']). ' ' .
		      Html::button(SDHtml::getBtnDelete(), ['data-url'=>Url::to(['dicom-upload/deletes']), 'class' => 'btn btn-danger btn-sm', 'id'=>'modal-delbtn-dicom-upload', 'disabled'=>true]),
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
        'columns' => [
	    [
		'class' => 'yii\grid\CheckboxColumn',
		'checkboxOptions' => [
		    'class' => 'selectionDicomUploadIds'
		],
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:40px;text-align: center;'],
	    ],
	    [
		'class' => 'yii\grid\SerialColumn',
		'headerOptions' => ['style'=>'text-align: center;'],
		'contentOptions' => ['style'=>'width:60px;text-align: center;'],
	    ],

            'id',
            'user_id',
            'dcm_file',
            'jpg_file',
            'sitecode',
            // 'ptcode',
            // 'status',

	    [
		'class' => 'appxq\sdii\widgets\ActionColumn',
		'contentOptions' => ['style'=>'width:80px;text-align: center;'],
		'template' => '{view} {update} {delete}',
	    ],
        ],
    ]); ?>
    <?php  Pjax::end();?>

</div>

<?=  ModalForm::widget([
    'id' => 'modal-dicom-upload',
    'size'=>'modal-lg',
]);
?>

<?php  $this->registerJs("
$('#dicom-upload-grid-pjax').on('click', '#modal-addbtn-dicom-upload', function() {
    modalDicomUpload($(this).attr('data-url'));
});

$('#dicom-upload-grid-pjax').on('click', '#modal-delbtn-dicom-upload', function() {
    selectionDicomUploadGrid($(this).attr('data-url'));
});

$('#dicom-upload-grid-pjax').on('click', '.select-on-check-all', function() {
    window.setTimeout(function() {
	var key = $('#dicom-upload-grid').yiiGridView('getSelectedRows');
	disabledDicomUploadBtn(key.length);
    },100);
});

$('#dicom-upload-grid-pjax').on('click', '.selectionDicomUploadIds', function() {
    var key = $('input:checked[class=\"'+$(this).attr('class')+'\"]');
    disabledDicomUploadBtn(key.length);
});

$('#dicom-upload-grid-pjax').on('dblclick', 'tbody tr', function() {
    var id = $(this).attr('data-key');
    modalDicomUpload('".Url::to(['dicom-upload/update', 'id'=>''])."'+id);
});	

$('#dicom-upload-grid-pjax').on('click', 'tbody tr td a', function() {
    var url = $(this).attr('href');
    var action = $(this).attr('data-action');

    if(action === 'update' || action === 'view') {
	modalDicomUpload(url);
    } else if(action === 'delete') {
	yii.confirm('".Yii::t('app', 'Are you sure you want to delete this item?')."', function() {
	    $.post(
		url
	    ).done(function(result) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#dicom-upload-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }).fail(function() {
		". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
		console.log('server error');
	    });
	});
    }
    return false;
});

function disabledDicomUploadBtn(num) {
    if(num>0) {
	$('#modal-delbtn-dicom-upload').attr('disabled', false);
    } else {
	$('#modal-delbtn-dicom-upload').attr('disabled', true);
    }
}

function selectionDicomUploadGrid(url) {
    yii.confirm('".Yii::t('app', 'Are you sure you want to delete these items?')."', function() {
	$.ajax({
	    method: 'POST',
	    url: url,
	    data: $('.selectionDicomUploadIds:checked[name=\"selection[]\"]').serialize(),
	    dataType: 'JSON',
	    success: function(result, textStatus) {
		if(result.status == 'success') {
		    ". SDNoty::show('result.message', 'result.status') ."
		    $.pjax.reload({container:'#dicom-upload-grid-pjax'});
		} else {
		    ". SDNoty::show('result.message', 'result.status') ."
		}
	    }
	});
    });
}

function modalDicomUpload(url) {
    $('#modal-dicom-upload .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-dicom-upload').modal('show')
    .find('.modal-content')
    .load(url);
}

");?>