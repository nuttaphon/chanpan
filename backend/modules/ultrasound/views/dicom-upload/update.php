<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ultrasound\models\DicomUpload */

$this->title = 'Update Dicom Upload: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dicom Uploads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dicom-upload-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
