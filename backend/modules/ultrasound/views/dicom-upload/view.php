<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ultrasound\models\DicomUpload */

$this->title = 'Dicom Upload#'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dicom Uploads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dicom-upload-view">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?= DetailView::widget([
	    'model' => $model,
	    'attributes' => [
		'id',
		'user_id',
		'dcm_file',
		'jpg_file',
		'sitecode',
		'ptcode',
		'status',
	    ],
	]) ?>
    </div>
</div>
