<?php

namespace backend\modules\icf\controllers;

use Yii;
use backend\modules\icf\models\Total;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\icf\models\UserProfile;
use \yii\web\Controller;
use backend\modules\icf\models\TotalSearch;
use backend\modules\icf\classes\ICFMenu;

class RegController extends \yii\web\Controller
{
    public function actionMenu()
    {
        $_webget=$_GET;
        return $this->render('menu', [
            '_webget' => $_webget,
        ]);
    }
    
    public function actionIndex()
    {
        $regCheckRecord="1-2-3";
        //$_webserver = $_SERVER;
        $userDet['userId'] = \Yii::$app->user->identity->id;
        $userDet['sitecode'] = \Yii::$app->user->identity->userProfile->sitecode;
        $dataCount['dataGroup']=  self::dataGetDataGroupBySite($userDet['sitecode']);
        
        
        return $this->render('index', [
            '_webserver' => 'reg',
            'regCheckRecord' => $regCheckRecord,
            'userDet' => $userDet,
            'dataCount' => $dataCount,
        ]);
    }
    
    public function actionIndexModal()
    {
        $regCheckRecord="1-2-3";
        //$_webserver = 'reg';
        $userDet['userId'] = \Yii::$app->user->identity->id;
        $userDet['sitecode'] = \Yii::$app->user->identity->userProfile->sitecode;
        $dataCount['dataGroup']=  self::dataGetDataGroupBySite($userDet['sitecode']);
        
        
        return $this->renderAjax('index', [
            '_webserver' => 'reg',
            'regCheckRecord' => $regCheckRecord,
            'userDet' => $userDet,
            'dataCount' => $dataCount,
        ]);
    }
    
    public function actionICFMenu(){
        return $this->render('myclass');
    }
    
    public function dataGetDataGroupBySite($sitecode){
        
        //$userid=1;
        $sql = "select count(distinct ptid) as allrecord ";
        $sql.= ",count(if(rstat='4',ptid,NULL)) as record_icf_lock ";
        //$sql.= ",count(if(confirm>0 and rstat='2',ptid,NULL)) as record_icf_completed ";
        $sql.= ",count(if(confirm>0,ptid,NULL)) as record_icf_completed ";
        $sql.= ",count(if(confirm='0' ,ptid,NULL)) as record_icfnotc ";     # ตรวจแล้วไม่ผ่าน
        $sql.= ",count(if(confirm='0' and `update_date`>`dlastcheck` and rstat='2',ptid,NULL)) as record_icfnotc_edited ";                      #  แก้ไขมาแล้ว และได้ Submit แล้ว
        $sql.= ",count(if(confirm='0' and `update_date`>`dlastcheck` and rstat='1',ptid,NULL)) as record_icfnotc_editednotsubmit ";             #  แก้ไขมาแล้ว และได้ Submit แล้ว
        $sql.= ",count(if(confirm='0' and (`update_date`<`dlastcheck` or `update_date` is null or `dlastcheck` is null) ,ptid,NULL)) as record_icfnotc_noedite ";
        $sql.= ",count(if(confirm is null and rstat='2' and `dlastcheck` is null,ptid,NULL)) as record_submited_waitingchecking ";
        $sql.= ",count(if(confirm is null and length(error)=0 and rstat='1',ptid,NULL)) as record_compedted_waitingsubmit ";
        $sql.= ",count(if(length(error)>0 and (confirm is null or confirm=0),ptid,NULL)) as record_notcompedte ";
        $sql.= ",count(if(rstat='0',ptid,NULL)) as record_new ";
        $sql.= "from tb_data_1 where hsitecode=\"".addslashes($sitecode)."\" ";
        $sql.= "and rstat<>'3' ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if($result){
            $out=$result[0];
        }
        $out['txtlink']['record_icf_lock']="/icf/reg/list?filter=lock";
        $out['txtlink']['record_icf_completed']="/icf/reg/list?filter=completed";
        $out['txtlink']['record_icfnotc']="/icf/reg/list?filter=notcomp";
        $out['txtlink']['record_icfnotc_edited']="/icf/reg/list?filter=notcompedited";
        $out['txtlink']['record_icfnotc_editednotsubmit']="/icf/reg/list?filter=notcompeditednotsubmit";
        $out['txtlink']['record_icfnotc_noedite']="/icf/reg/list?filter=notcompnoedit";
        $out['txtlink']['record_submited_waitingchecking']="/icf/reg/list?filter=wchecking";
        $out['txtlink']['record_compedted_waitingsubmit']="/icf/reg/list?filter=wsubmit";
        $out['txtlink']['record_notcompedte']="/icf/reg/list?filter=error";
        $out['txtlink']['record_new']="/icf/reg/list?filter=new";
        return $out;
        
    }
    
    private function getLimitPage(){
        $limit=100;
        return $limit;
    }
    private function getDefaultPage(){
        $out=0;
        return $out;
    }
    
    public function actionList()
    {
        $_webget=$_GET;
        $_webserver = $_SERVER;
        $_webget['limitperpage'] = self::getLimitPage();
        if(strlen($_webget['page']) == 0){
            $_webget['page'] =  self::getDefaultPage();
        }
        $userDet['userId'] = \Yii::$app->user->identity->id;
        $userDet['sitecode'] = \Yii::$app->user->identity->userProfile->sitecode;
        $_filter['condition'] = self::getListOfGroupRecord();
        $listRecord['row'] = self::getListDataGroupBySite($userDet['sitecode'],$_webget['filter']);
        $dataCount['dataGroup']=  self::dataGetDataGroupBySite($userDet['sitecode']);
        
        return $this->render('list', [
            '_webget' => $_webget,
            '_webserver' => $_webserver,
            'userDet' => $userDet,
            '_filter' => $_filter,
            'listRecord' => $listRecord,
            'dataCount' => $dataCount,
        ]);
    }
    
    public function getListOfGroupRecord(){
        $irow=0;
        $out[$irow]['msg']='จำนวนที่ตรวจแล้วผ่าน และได้รับค่าตอบแทนแล้ว';
        $out[$irow]['filter']="lock";
        $out[$irow]['url']="/icf/reg/list?filter=lock";
        $out[$irow]['sqlname']="record_icf_lock";
        $out[$irow]['sqlcond']="reg.rstat='4'";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ตรวจแล้วผ่าน';
        $out[$irow]['filter']="completed";
        $out[$irow]['url']="/icf/reg/list?filter=completed";
        $out[$irow]['sqlname']="record_icf_completed";
        //$out[$irow]['sqlcond']="confirm>0 and rstat='2'";
        $out[$irow]['sqlcond']="reg.confirm>0";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ตรวจแล้วไม่ผ่าน';
        $out[$irow]['filter']="notcomp";
        $out[$irow]['url']="/icf/reg/list?filter=notcomp";
        $out[$irow]['sqlname']="record_icfnotc";
        $out[$irow]['sqlcond']="reg.confirm='0'";
        $irow++;
        $out[$irow]['msg']='จำนวนที่แก้ไขมาแล้ว (Submited) หลังจากตรวจไม่ผ่าน';
        $out[$irow]['filter']="notcompedited";
        $out[$irow]['url']="/icf/reg/list?filter=notcompedited";
        $out[$irow]['sqlname']="record_icfnotc_edited";
        $out[$irow]['sqlcond']="reg.confirm='0' and reg.`update_date`>reg.`dlastcheck` and reg.rstat='2'";
        $irow++;
        $out[$irow]['msg']='จำนวนที่แก้ไขมาแล้ว (แต่ยังไม่ Submit) หลังจากตรวจไม่ผ่าน';
        $out[$irow]['filter']="notcompeditednotsubmit";
        $out[$irow]['url']="/icf/reg/list?filter=notcompeditednotsubmit";
        $out[$irow]['sqlname']="record_icfnotc_editednotsubmit";
        $out[$irow]['sqlcond']="reg.confirm='0' and reg.`update_date`>reg.`dlastcheck` and reg.rstat='1'";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ยังไม่ได้รับการแก้ไข หลังจากตรวจแล้ว';
        $out[$irow]['filter']="notcompnoedit";
        $out[$irow]['url']="/icf/reg/list?filter=notcompnoedit";
        $out[$irow]['sqlname']="record_icfnotc_noedite";
        $out[$irow]['sqlcond']="reg.confirm='0' and (reg.`update_date`<reg.`dlastcheck` or reg.`update_date` is null or reg.`dlastcheck` is null)";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ผ่านเงื่อนไข (ไม่มี Error) และ Submit แล้ว รอการตรวจ';
        $out[$irow]['filter']="wchecking";
        $out[$irow]['url']="/icf/reg/list?filter=wchecking";
        $out[$irow]['sqlname']="record_submited_waitingchecking";
        $out[$irow]['sqlcond']="reg.confirm is null and reg.rstat='2' and reg.`dlastcheck` is null";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ผ่านเงื่อนไข (ไม่มี Error) ยังไม่ได้ Submit';
        $out[$irow]['filter']="wsubmit";
        $out[$irow]['url']="/icf/reg/list?filter=wsubmit";
        $out[$irow]['sqlname']="record_compedted_waitingsubmit";
        $out[$irow]['sqlcond']="reg.confirm is null and length(reg.error)=0 and reg.rstat='1'";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ข้อมูลยังไม่สมบูรณ์ (กรุณาเข้าไปแก้ไขเพื่อให้ข้อมูลสมบูรณ์)';
        $out[$irow]['filter']="error";
        $out[$irow]['url']="/icf/reg/list?filter=error";
        $out[$irow]['sqlname']="record_notcompedte";
        $out[$irow]['sqlcond']="length(reg.error)>0 and (reg.confirm is null or reg.confirm=0)";
        $irow++;
        $out[$irow]['msg']='จำนวนที่ข้อมูลที่เพิ่มใหม่';
        $out[$irow]['filter']="new";
        $out[$irow]['url']="/icf/reg/list?filter=new";
        $out[$irow]['sqlname']="reg.record_new";
        $out[$irow]['sqlcond']="reg.rstat='0'";
        return $out;
    }


    public function getListDataGroupBySite($sitecode,$filter){
        //$query = new \yii\db\Query();
        $lscond=Self::getListOfGroupRecord();
        $sql = "select reg.id,reg.ptid,reg.confirm,reg.hsitecode,reg.hptcode";
        $sql.= ",reg.title,reg.name,reg.surname,reg.error ";
        $sql.= ",reg.rstat ";
        $sql.= ",reg.`dlastcheck`,reg.`update_date` ";
        $sql.= ",if(Not(cca01.rstat in ('0','3')) and (length(cca01.`error`)=0 and cca01.`error` is not null),'<span class=tdtext-text-show-blue>สมบูรณ์</span>' ";
        $sql.= ",if(cca01.`error` is null,'<span class=tdtext-text-show-black>ยังไม่ตรวจสอบ</span>' ";
        $sql.= ",'<span class=tdtext-text-show-red>ไม่สมบูรณ์</span>')) as 'cca01_error' ";
        $sql.= "from tb_data_1 as reg ";
        $sql.="left join tb_data_2 as cca01 on cca01.ptid=reg.ptid ";
        $sql.= "where reg.hsitecode=\"".addslashes($sitecode)."\" ";
        $sql.= "and reg.rstat<>'3' ";
        $checkSQL = '';
        if( count($lscond)>0 ){
            foreach($lscond as $k => $v){
                if( $lscond[$k]['filter']==$filter ){
                    $checkSQL = 'add condition';
                    $sql.="and ";
                    $sql.="(".$lscond[$k]['sqlcond'].") ";
                }
            }
        }
        if( strlen($checkSQL)>0 ){
            //
        }else{
            $sql.= "and 0 ";
        }
        /*
        switch ($filter){
            case 'lock':
            //case 'record_icf_lock':
                $sql.= "and reg.rstat='4' ";
                break;
            case 'completed':
            //case 'record_icf_completed':
                //$sql.= "and (reg.confirm>0 and reg.rstat='2') ";
                $sql.= "and (reg.confirm>0) ";
                break;
            case 'notcomp':
            //case 'record_icfnotc':
                $sql.= "and reg.confirm='0'  ";
                break;
            case 'notcompedited':
            //case 'record_icfnotc_edited':
                $sql.= "and (reg.confirm='0' and reg.`update_date`>reg.`dlastcheck`) ";
                break;
            case 'notcompnoedit':
            //case 'record_icfnotc_noedite':
                $sql.= "and (reg.confirm='0' and (reg.`update_date`<reg.`dlastcheck` or reg.`update_date` is null or reg.`dlastcheck` is null))  ";
                break;
            case 'wchecking':
            //case 'record_submited_waitingchecking':
                $sql.= "and (reg.rstat='2' and reg.`dlastcheck` is null) ";
                break;
            case 'wsubmit':
            //case 'record_compedted_waitingsubmit':
                $sql.= "and (length(reg.error)=0 and reg.rstat='1') ";
                break;
            case 'error':
            //case 'record_notcompedte':
                $sql.= "and (length(reg.error)>0 and (reg.confirm is null or reg.confirm=0)) ";
                break;
            case 'new':
            //case 'record_new':
                $sql.= "and reg.rstat='0'  ";
                break;
            default :
                $sql.= "and 0 ";        // ไม่ Return ค่าใดๆ ทั้งสิ้น
                break;
        }
         * 
         */
        $sql.= "order by reg.hsitecode,reg.hptcode ";
        $sql.= "limit 4000 ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if($result){
            $out=$result;
        }
        if( $filter=='wchecking' ){
            //$txtTime=getICFGetTimeCheckICF($ptid);
        }else{
            if(count($out)>0){
                foreach ($out as $k => $v){
                    if($out[$k]['confirm']>0){
                        //
                    }else if( $filter == 'notcompedited' ){
                        $errTxt=  self::getICFGetTimeCheckICF($out[$k]['ptid']);
                        $out[$k]['txtTime']=$errTxt['maxtime'];
                    }else{
                        $errTxt=  self::getICFCheckErrorFromTools($out[$k]['ptid']);
                        $out[$k]['txtError']=$errTxt['txtError'];
                    }
                }
            }
        }
        return $out;
    }
    
    public function getICFCheckErrorFromTools($ptid){
        // Edit at base.php
        //  'dsn' => 'mysql:host=localhost;port=3306;dbname=v04cascap',
        $sql = "select distinct `comment`  as 'txt' ";
        $sql.= "from v04cascap.query_confirmq ";
        $sql.= "where ptid=\"".$ptid."\" ";
        $sql.= "and `var`=\"icf\" ";
        $sql.= "order by dadd ";
        $result =  Yii::$app->dbcascap->createCommand($sql)->queryAll();
        if($result){
            $row=$result;
        }
        $irow=0;
        if(count($row)>0){
            foreach ( $row as $k => $v ){
                if(strlen($row[$k]['txt'])>0){
                    $txtError.=iconv('TIS-620','UTF-8', $row[$k]['txt']) ;
                    if($irow<(count($row)-1)){
                        $txtError.="<br />";
                    }
                }
                $irow++;
            }
        }
        $out['txtError']=$txtError;//."<br />".$sql;
        return $out;
    }
    
    public function getICFGetTimeCheckICF($ptid){
        // Edit at base.php
        //  'dsn' => 'mysql:host=localhost;port=3306;dbname=v04cascap',
        $sql = "select max(`times`) as maxtime ";
        $sql.= "from v04cascap.query_verify ";
        $sql.= "where ptid=\"".$ptid."\" ";
        //$sql.= "and `var`=\"icf\" ";
        $result =  Yii::$app->dbcascap->createCommand($sql)->queryAll();
        if($result){
            $row=$result[0];
        }
        
        
        return $row;
    }

}
