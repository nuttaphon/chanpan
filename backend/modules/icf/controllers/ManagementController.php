<?php

namespace backend\modules\icf\controllers;

use backend\modules\icf\models\Management;
use backend\modules\icf\models\Register;
use backend\modules\icf\models\UserProfile;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\data\SqlDataProvider;
class ManagementController extends \yii\web\Controller
{
    public function actionIndex($action)
    {
        $userId = \Yii::$app->user->identity->id;
        $userProfile = UserProfile::find()
            ->where(['user_id'=>$userId])
            ->one();
        $sql ="select id as register_id,
                ptid as register_ptid,
                concat(hsitecode,hptcode) as fullcode,
                title,
                concat(name,' ',surname) as fullname,
                target as register_target,
                    case 1
                    when (confirm>0) then '1'
                    when ((confirm is null) AND (length(icf_upload1)>0 )) then '2'
                    when ((confirm=0) AND (length(icf_upload1)>0)) then '3'
                    when((length(error)>0)OR(icf_upload1 is null)OR(length(icf_upload1)=0)) then '4'
                    end as status,
                error
                from tb_data_1
                where hsitecode=:sitecode
                AND rstat <>'3'
                ORDER BY fullcode";
        $count = \Yii::$app->db->createCommand($sql,[':sitecode' => $userProfile->sitecode])->query()->count();
        $registerProvider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':sitecode' => $userProfile->sitecode],
            'pagination' => [
                'pageSize' => 20,
            ],
            'totalCount' =>$count
        ]);
        $successRegisterCount=0;
        $waitingRegisterCount=0;
        $notSuccessConfirmRegisterCount=0;
        $errorRegisterCount=0;
        $statusAction=0;
       $model = $registerProvider->getModels();
        foreach ($model as $row) {
            if($row['status']=='1')
            {
                $successRegisterCount++;
            }elseif($row['status']=='2'){
                $waitingRegisterCount++;
            }elseif($row['status']=='3'){
                $notSuccessConfirmRegisterCount++;
            }else{
                $errorRegisterCount++;
            }
        }
        //echo $errorCount;
        //exit();
        return $this->render('index', [
                'registerProvider' =>$registerProvider,
                'successRegisterCount'=> $successRegisterCount,
                'waitingRegisterCount' => $waitingRegisterCount,
                'notSuccessConfirmRegisterCount' => $notSuccessConfirmRegisterCount,
                'errorRegisterCount' => $errorRegisterCount,
                'statusAction' => $statusAction
            ]);
    }

}
