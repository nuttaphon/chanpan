<?php

namespace backend\modules\icf\models;

use Yii;

/**
 * This is the model class for table "icf_14601825503718".
 *
 * @property string $register_ptid
 * @property string $fullcode
 * @property string $title
 * @property string $fullname
 * @property integer $register_status
 * @property string $register_error
 * @property string $cca01_id
 * @property integer $cca01_status
 * @property string $cca01_error
 * @property string $cca02_id
 * @property integer $cca02_status
 * @property integer $cca02_new
 * @property integer $cca02_draft
 * @property integer $cca02_submitted
 * @property string $cca02_error
 * @property string $cca02_1_id
 * @property integer $cca02_1_status
 * @property integer $cca02_1_new
 * @property integer $cca02_1_draft
 * @property integer $cca02_1_submitted
 * @property string $cca02_1_error
 * @property string $cca03_id
 * @property integer $cca03_status
 * @property integer $cca03_new
 * @property integer $cca03_draft
 * @property integer $cca03_submitted
 * @property string $cca03_error
 * @property string $cca04_id
 * @property integer $cca04_status
 * @property integer $cca04_new
 * @property integer $cca04_draft
 * @property integer $cca04_submitted
 * @property string $cca04_error
 * @property string $cca05_id
 * @property integer $cca05_status
 * @property integer $cca05_new
 * @property integer $cca05_draft
 * @property integer $cca05_submitted
 * @property string $cca05_error
 */
class Total extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    protected  static $table;
//    public function __construct($table)
//    {
//        //parent::__construct();
//        self::$table = $table;
//        parent::__construct();
//        //parent::__construct();
//       // pare
//        //$this->set
//    }

    public static function tableName()
    {
        //return 'icf_14601825503718';
        return self::$table;
    }
    public static  function setTableName($table)
    {
        self::$table = $table;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['register_ptid'], 'required'],
            [['register_ptid', 'register_status', 'cca01_id', 'cca01_status', 'cca02_id', 'cca02_status', 'cca02_new', 'cca02_draft', 'cca02_submitted', 'cca02_1_id', 'cca02_1_status', 'cca02_1_new', 'cca02_1_draft', 'cca02_1_submitted', 'cca03_id', 'cca03_status', 'cca03_new', 'cca03_draft', 'cca03_submitted', 'cca04_id', 'cca04_status', 'cca04_new', 'cca04_draft', 'cca04_submitted', 'cca05_id', 'cca05_status', 'cca05_new', 'cca05_draft', 'cca05_submitted'], 'integer'],
            [['register_error', 'cca01_error', 'cca02_error', 'cca02_1_error', 'cca03_error', 'cca04_error', 'cca05_error'], 'string'],
            [['fullcode'], 'string', 'max' => 20],
            [['title'], 'string', 'max' => 50],
            [['fullname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'register_ptid' => 'Register Ptid',
            'fullcode' => 'Fullcode',
            'title' => 'Title',
            'fullname' => 'Fullname',
            'register_status' => 'Register Status',
            'register_error' => 'Register Error',
            'cca01_id' => 'Cca01 ID',
            'cca01_status' => 'Cca01 Status',
            'cca01_error' => 'Cca01 Error',
            'cca02_id' => 'Cca02 ID',
            'cca02_status' => 'Cca02 Status',
            'cca02_new' => 'Cca02 New',
            'cca02_draft' => 'Cca02 Draft',
            'cca02_submitted' => 'Cca02 Submitted',
            'cca02_error' => 'Cca02 Error',
            'cca02_1_id' => 'Cca02 1 ID',
            'cca02_1_status' => 'Cca02 1 Status',
            'cca02_1_new' => 'Cca02 1 New',
            'cca02_1_draft' => 'Cca02 1 Draft',
            'cca02_1_submitted' => 'Cca02 1 Submitted',
            'cca02_1_error' => 'Cca02 1 Error',
            'cca03_id' => 'Cca03 ID',
            'cca03_status' => 'Cca03 Status',
            'cca03_new' => 'Cca03 New',
            'cca03_draft' => 'Cca03 Draft',
            'cca03_submitted' => 'Cca03 Submitted',
            'cca03_error' => 'Cca03 Error',
            'cca04_id' => 'Cca04 ID',
            'cca04_status' => 'Cca04 Status',
            'cca04_new' => 'Cca04 New',
            'cca04_draft' => 'Cca04 Draft',
            'cca04_submitted' => 'Cca04 Submitted',
            'cca04_error' => 'Cca04 Error',
            'cca05_id' => 'Cca05 ID',
            'cca05_status' => 'Cca05 Status',
            'cca05_new' => 'Cca05 New',
            'cca05_draft' => 'Cca05 Draft',
            'cca05_submitted' => 'Cca05 Submitted',
            'cca05_error' => 'Cca05 Error',
        ];
    }
}
