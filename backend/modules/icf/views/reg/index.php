<?php
/* @var $this yii\web\View */

/*
 * 
 * Result from $regCheckRecord
 *  -> fetch each row of array
 * 
 */

//echo $regCheckRecord;
if( 0 ){
    echo "<pre align='left'>";
    //echo $regCheckRecord;
    //print_r($userDet);
    //print_r($dataCount);
    print_r($_webserver);
    echo "</pre>";
}

?>
<style>
    .tdtext-number-show{
        text-align: right;
        font-size: 24px;
        font-weight: bold;
        color: blue;
    }
    .tdtext-header-show{
        text-align: left;
        font-size: 24px;
        font-weight: bold;
        color: black;
    }
    .tdtext-number-show-black{
        text-align: right;
        color: black;
    }
    .tdtext-number-show-red{
        text-align: right;
        color: red;
    }
    .tdtext-number-show-blue{
        text-align: right;
        color: blue;
    }
    .table-reg-wide {
        border-radius: 5px;
        width: 70%;
        float: none;
        font-family: "Angsana New",Arial,serif;
        font-size: 20px;
    }
    a.link{
        text-decoration: none;
    }
    a.hover{
        text-decoration: underline;
    }
</style>

<div id="step2confirm" class="modal-content" >
        <div class="modal-header">
            <h1>ตรวจสอบ ความถูกต้องของการนำเข้าข้อมูล Register</h1>
        </div>
    
        <div class="modal-body">
            <?php
                
                echo $this->render('/reg/_menu', [
                        '_webserver' => $_webserver,
                    ]);
            ?>

            <br />
            <div class="header inline">
                <label class="text-bold">สามารถเข้าดูข้อมูลราย Records โดยการคลิ๊กที่ตัวเลข ของแต่ละเงื่อนไขได้</label>
            </div>
            <table class="table table-striped" >
                <thead class="table table-inverse">
                    <tr>
                        <th>#</th>
                        <th class="tdtext-header-show">ผลการตรวจสอบ</th>
                        <th class="tdtext-number-show">จำนวนที่ตรวจ</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>จำนวนที่ตรวจแล้วผ่าน และได้รับค่าตอบแทนแล้ว</td>
                        <td class="tdtext-number-show-black">
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_icf_lock']; ?>" class="tdtext-number-show-black" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_lock'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>จำนวนที่ตรวจแล้วผ่าน</td>
                        <td class="tdtext-number-show-blue">
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_icf_completed']; ?>" class="tdtext-number-show-blue" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_icf_completed'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>จำนวนที่ตรวจแล้วไม่ผ่าน</td>
                        <td class="tdtext-number-show-red">
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_icfnotc']; ?>" class="tdtext-number-show-red" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_icfnotc'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td>จำนวนที่แก้ไขมาแล้ว (Submited) หลังจากตรวจไม่ผ่าน</td>
                        <td class="tdtext-number-show-red">
                            <span class="glyphicon glyphicon-time" style="color: orange"></span>
                            <label>(รอตรวจ) </label>
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_icfnotc_edited']; ?>" class="tdtext-number-show-red" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_icfnotc_edited'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td>จำนวนที่แก้ไขมาแล้ว (แต่ยังไม่ Submit) หลังจากตรวจไม่ผ่าน</td>
                        <td class="tdtext-number-show-red">
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_icfnotc_editednotsubmit']; ?>" class="tdtext-number-show-red" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_icfnotc_editednotsubmit'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td>จำนวนที่ยังไม่ได้รับการแก้ไข หลังจากตรวจแล้ว</td>
                        <td class="tdtext-number-show-red">
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_icfnotc_noedite']; ?>" class="tdtext-number-show-red" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_icfnotc_noedite'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">6</th>
                        <td>จำนวนที่ผ่านเงื่อนไข และ Submit แล้ว (รอการตรวจ ครั้งแรก)</td>
                        <td class="tdtext-number-show-blue">
                            <span class="glyphicon glyphicon-time" style="color: orange"></span>
                            <label style="color: red;">(รอตรวจ) </label>
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_submited_waitingchecking']; ?>" class="tdtext-number-show-blue" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_submited_waitingchecking'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">7</th>
                        <td>จำนวนที่ผ่านเงื่อนไข (ยังไม่ได้ Submit) (รอการตรวจ ครั้งแรก)</td>
                        <td class="tdtext-number-show-blue">
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_compedted_waitingsubmit']; ?>" class="tdtext-number-show-blue" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_compedted_waitingsubmit'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">8</th>
                        <td>จำนวนที่ข้อมูลยังไม่สมบูรณ์ (กรุณาเข้าไปแก้ไขเพื่อให้ข้อมูลสมบูรณ์)</td>
                        <td class="tdtext-number-show-blue">
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_notcompedte']; ?>" class="tdtext-number-show-blue" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_notcompedte'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">9</th>
                        <td>จำนวนที่ข้อมูลที่เพิ่มใหม่</td>
                        <td class="tdtext-number-show-blue">
                            <a href="<?php echo $dataCount['dataGroup']['txtlink']['record_new']; ?>" class="tdtext-number-show-blue" target="_blank">
                            <?php
                                echo number_format($dataCount['dataGroup']['record_new'],0,'.',',');
                            ?>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
<!-- p>
    You may change the content of this page by modifying
    the file <code><?= __FILE__; ?></code>.
</p -->
