<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * /icf/reg/list?fillter=lock
 * /icf/reg/list?fillter=completed
 * /icf/reg/list?fillter=notcomp
 * /icf/reg/list?fillter=notcompedited
 * /icf/reg/list?fillter=notcompnoedit
 * /icf/reg/list?fillter=wchecking
 * /icf/reg/list?fillter=wsubmit
 * /icf/reg/list?fillter=error
 * /icf/reg/list?fillter=new
 *  page
 */

if( 0 ){
    echo "<pre align='left'>";
    //print_r($_webget);
    print_r($listRecord);
    //print_r($_filter);
    //print_r($dataCount['dataGroup']);
    echo "</pre>";
}
?>
<style>
    .tdtext-text{
        text-align: left;
    }
    .tdtext-text-show-black{
        color: black;
    }
    .tdtext-text-show-red{
        color: red;
    }
    .tdtext-text-show-blue{
        color: blue;
    }
    .tdtext-number-show{
        text-align: right;
        font-size: 24px;
        font-weight: bold;
        color: blue;
    }
    .tdtext-header-show{
        text-align: left;
        font-size: 24px;
        font-weight: bold;
        color: black;
    }
    .tdtext-number-show-black{
        text-align: right;
        color: black;
    }
    .tdtext-number-show-red{
        text-align: right;
        color: red;
    }
    .tdtext-number-show-blue{
        text-align: right;
        color: blue;
    }
    .table {
        border-radius: 5px;
        width: 99%;
        float: none;
        font-family: "Angsana New",Arial,serif;
        font-size: 18px;
    }
    a.link{
        text-decoration: none;
    }
    a.hover{
        text-decoration: underline;
    }
</style>
<script type="text/javascript" >
    function openUrlFromChanged(obj)
    {
        window.open(obj.options[obj.selectedIndex].value,'_self');
    }
</script>
<!--a class="w3-left w3-btn" href="/icf/reg">❮ สรุปภาพรวม</a-->
<h1>ตรวจสอบ ความถูกต้องของการนำเข้าข้อมูล Register</h1>
<div class="container" >
    <?php
        echo $this->render('/reg/_menu', [
                '_webserver' => $_webserver,
            ]);
    ?>
    <br />
    <div class="header inline">
        <label class="text-bold">แสดงข้อมูลของชุดข้อมูล 
            <?php 
                if( 0 ){
                    if( count($_filter['condition'])>0 ){
                        foreach ( $_filter['condition'] as $k => $v ){
                            if( $_filter['condition'][$k]['filter']== $_webget['filter'] ){
                                echo $_filter['condition'][$k]['msg'];
                            }
                        }
                    }
                }
            ?>
        </label>
        <label class="">: 
        </label>
        <select id="selFilterGroup" onchange="openUrlFromChanged(this)" class="btn-info">
            <option label=" - เลือกผลของการลงข้อมูล และผลการตรวจ - " value="" selected > - เลือกผลของการลงข้อมูล และผลการตรวจ - </option>
                <?php
                    if( count($_filter['condition'])>0 ){
                        
                        foreach ( $_filter['condition'] as $k => $v ){
                            $txtSelected="";
                            if( $_filter['condition'][$k]['filter']== $_webget['filter'] ){
                                $txtSelected="selected";
                            }
                ?>
            <option label="<?php echo $_filter['condition'][$k]['msg']." (".number_format($dataCount['dataGroup'][$_filter['condition'][$k]['sqlname']],0,'.',',').") "; ?>" value="<?php echo $_filter['condition'][$k]['url']; ?>" <?php echo $txtSelected; ?> >
                <?php 
                    echo $_filter['condition'][$k]['msg']; 
                ?>
            </option>
                <?php
                        }
                    }
                ?>
        </select>
    </div>
    <table class="table table-striped">
        <thead class="table table-inverse">
            <tr>
                <th>#</th>
                <th class="tdtext-header-show">Site ID</th>
                <th class="tdtext-header-show">PID</th>
                <th class="tdtext-header-show">Title</th>
                <th class="tdtext-header-show">ชื่อ</th>
                <th class="tdtext-header-show">นามสกุล</th>
                <?php
                    if($_webget['filter']=="completed" ){
                        //ไม่ต้องแสดง Column นี้ // 
                ?>
                <th class="tdtext-header-show">วันที่ตรวจสอบ</th>
                <?php
                    }else{
                ?>
                <th class="tdtext-header-show">ข้อผิดพลาด</th>
                <?php
                    }
                ?>
                
                <?php
                    if( $_webget['filter']=="notcompedited" ){
                ?>
                <th class="tdtext-header-show">ตรวจไปแล้ว</th>
                <?php
                    }
                ?>
                <th class="tdtext-header-show">สถานะ (Register)</th>
                <th class="tdtext-header-show">CCA-01</th>
            </tr>
        </thead>
        <tbody>
<?php
    $irow=0;
    if(count($listRecord['row'])>0){
        foreach($listRecord['row'] as $k => $v){ 
            $irow++;
            $openForm="/inputdata/redirect-page?dataid=".$listRecord['row'][$k]['id']."&ezf_id=1437377239070461301";
            $siteLink="<a href=\"".$openForm."\" class=\"tdtext-text-show-black\" target=\"_blank\">".$listRecord['row'][$k]['hsitecode']."</a>";
            $pidLink="<a href=\"".$openForm."\" class=\"tdtext-text-show-black\" target=\"_blank\">".$listRecord['row'][$k]['hptcode']."</a>";
            $titleLink="<a href=\"".$openForm."\" class=\"tdtext-text-show-black\" target=\"_blank\">".$listRecord['row'][$k]['title']."</a>";
            $nameLink="<a href=\"".$openForm."\" class=\"tdtext-text-show-black\" target=\"_blank\">".$listRecord['row'][$k]['name']."</a>";
            $surnameLink="<a href=\"".$openForm."\" class=\"tdtext-text-show-black\" target=\"_blank\">".$listRecord['row'][$k]['surname']."</a>";
            if($_webget['filter']=="completed" ){
                //ไม่ต้องแสดง Column นี้ //
                if( strlen($listRecord['row'][$k]['dlastcheck'])==0 ){
                    $dlastcheck=$listRecord['row'][$k]['update_date'];
                }else{
                    $dlastcheck=$listRecord['row'][$k]['dlastcheck'];
                }
            }
            $errorLink="";
            if( $listRecord['row'][$k]['confirm']*1>0 ){
                $errorLink="";
            }else if( $listRecord['row'][$k]['confirm']=='0' ){
                //$errorLink=file_get_contents("https://tools.cascap.in.th/v4cascap/version01/console/cca00/getICFCheck.php?ptid=".$listRecord['row'][$k]['ptid']."&id=".$listRecord['row'][$k]['id']);
                if( $_webget['filter']=='notcompedited' ){
                    //
                }else{
                    $errorLink ="<a href=\"".$openForm."\" class=\"tdtext-text-show-red\" target=\"_blank\">".$listRecord['row'][$k]['txtError']."</a>";
                    $errorLink.="<br />";
                }
                $errorLink.="<span class='tdtext-text-show-black'> ".$listRecord['row'][$k]['update_date']." แก้ไขล่าสุด</span>";
                $errorLink.="<br /><span class='tdtext-text-show-black'> ".$listRecord['row'][$k]['dlastcheck']." วันที่ตรวจ</span>";
            }else{
                $errorLink="<a href=\"".$openForm."\" class=\"tdtext-text-show-red\" target=\"_blank\">".$listRecord['row'][$k]['error']."</a>";
                $errorLink.="<span class='tdtext-text-show-black'> ".$listRecord['row'][$k]['update_date']." แก้ไขล่าสุด</span>";
                $errorLink.="<br /><span class='tdtext-text-show-black'> ".$listRecord['row'][$k]['dlastcheck']." วันที่ตรวจ</span>";
            }
            //
            $checkICFTime=$listRecord['row'][$k]['txtTime']*1;
            $cca01Link="<a href=\"".$openForm."\" class=\"tdtext-text-show-black\" target=\"_blank\">".$listRecord['row'][$k]['cca01_error']."</a>";
            //
            if( $listRecord['row'][$k]['rstat']=='5' ){
                $cca01rstatLink="<a href=\"".$openForm."\" class=\"text-warning\" title=\"ไม่สามารถแก้ไขได้\" data-pjax=\"0\"><i class=\"fa fa-send\"></i> Lock</a>";
            }else if( $listRecord['row'][$k]['rstat']=='4' ){
                $cca01rstatLink="<a href=\"".$openForm."\" class=\"text-warning\" title=\"ได้รับค่าตอบแทน แล้ว ไม่สามารถแก้ไขได้\" data-pjax=\"0\"><i class=\"fa fa-send\"></i> Freze</a>";
            }else if( $listRecord['row'][$k]['rstat']=='2' ){
                $cca01rstatLink="<a href=\"".$openForm."\" class=\"text-success\" title=\"ข้อมูลถูกส่งเข้าระบบเรียบร้อยแล้ว\" data-pjax=\"0\"><i class=\"fa fa-send\"></i> Submitted</a>";
            }else if( $listRecord['row'][$k]['rstat']=='1' ){
                $cca01rstatLink="<a href=\"".$openForm."\" class=\"text-warning\" title=\"ข้อมูลยังไม่ถูกส่งเข้าระบบด้วยการคลิก Submitted\" data-pjax=\"0\"><i class=\"fa fa-pencil-square-o\"></i> Waiting</a>";
            }else if( 1 ){
                $cca01rstatLink="<a href=\"".$openForm."\" class=\"tdtext-text-show-black\" target=\"_blank\">".$listRecord['row'][$k]['rstat']."</a>";
            }
?>
            <tr>
                <th scope="row" width="50"><?php echo $irow; ?></th>
                <td class="tdtext-text" width="70">
                    <?php echo $siteLink; ?>
                </td>
                <td class="tdtext-text" width="70">
                    <?php echo $pidLink; ?>
                </td>
                <td class="tdtext-text" width="70">
                    <?php echo $titleLink; ?>
                </td>
                <td class="tdtext-text" width="120">
                    <?php echo $nameLink; ?>
                </td>
                <td class="tdtext-text" width="120">
                    <?php echo $surnameLink; ?>
                </td>
                <?php
                    if($_webget['filter']=="completed" ){
                ?>
                <td class="tdtext-text" width="400">
                    <?php echo $dlastcheck; ?> 
                </td>
                <?php
                    }else{
                ?>
                <td class="tdtext-text" width="400">
                    <?php echo $errorLink; ?> 
                </td>
                <?php
                    }
                ?>
                <?php
                    if($_webget['filter']=="notcompedited" ){
                ?>
                <td class="tdtext-text" width="120">
                    <?php echo $checkICFTime; ?> 
                </td>
                <?php
                    }
                ?>
                <td class="tdtext-text" width="150">
                    <?php echo $cca01rstatLink; ?> 
                </td>
                <td class="tdtext-text" width="100">
                    <?php echo $cca01Link; ?> 
                </td>
            </tr>
<?php
        }
    }
?>
        </tbody>    
    </table>
</div>
