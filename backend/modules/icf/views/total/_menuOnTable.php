<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$js = <<< JS
$("#btn-popupreg").on("click", function() {
    modalShowPopup($(this).attr('data-url'));
});
function modalShowPopup(url) {
    $('#modal-popupreg .modal-content').html('<div class="sdloader "><i class="sdloader-icon"></i></div>');
    $('#modal-popupreg').modal('show')
    .find('.modal-content')
    .load(url);
}
JS;
// register your javascript
$this->registerJs($js, \yii\web\View::POS_END);

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-6 col-md-4">
            <button type='button' class='btn btn-warning' id='popupsymbol' data-toggle='modal' data-target='#myModal'>อ่านคำอธิบายสัญลักษณ์</button>

            <button type='button' class='btn btn-warning' style="width: 180px" id='btn-popupreg' data-url="/icf/reg/index-modal" > Reg + ICF </button>
        </div>
        <div class="col-xs-6 col-md-4"></div>
        <div class="col-xs-6 col-md-4"></div>
    </div>
</div>
