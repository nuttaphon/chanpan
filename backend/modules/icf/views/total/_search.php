<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\icf\models\TotalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="total-search">
    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'action' => ['index'],
        'method' => 'get',
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
    ]); ?>
    <div class="form-group kv-fieldset-inline">
        <div class="col-sm-3">
            <?= $form->field($model, 'globalSearch', [
                'showLabels'=> false
            ])->textInput(['placeholder'=>'ค้นหาด้วย ชื่อ - สกุล, รหัส 10 หลัก, HN, เลขบัตรประจำตัวประชาชน']); ?>
        </div>
        <div class="col-sm-2">
            <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
