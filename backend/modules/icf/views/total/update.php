<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\icf\models\Total */

$this->title = 'Update Total: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Totals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->register_ptid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="total-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
