<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\icf\models\Total */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Totals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="total-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->register_ptid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->register_ptid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'register_ptid',
            'fullcode',
            'title',
            'fullname',
            'register_status',
            'register_error:ntext',
            'cca01_id',
            'cca01_status',
            'cca01_error:ntext',
            'cca02_id',
            'cca02_status',
            'cca02_new',
            'cca02_draft',
            'cca02_submitted',
            'cca02_error:ntext',
            'cca02_1_id',
            'cca02_1_status',
            'cca02_1_new',
            'cca02_1_draft',
            'cca02_1_submitted',
            'cca02_1_error:ntext',
            'cca03_id',
            'cca03_status',
            'cca03_new',
            'cca03_draft',
            'cca03_submitted',
            'cca03_error:ntext',
            'cca04_id',
            'cca04_status',
            'cca04_new',
            'cca04_draft',
            'cca04_submitted',
            'cca04_error:ntext',
            'cca05_id',
            'cca05_status',
            'cca05_new',
            'cca05_draft',
            'cca05_submitted',
            'cca05_error:ntext',
        ],
    ]) ?>

</div>
