<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\icf\models\Total */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="total-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'register_ptid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fullcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'register_status')->textInput() ?>

    <?= $form->field($model, 'register_error')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cca01_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cca01_status')->textInput() ?>

    <?= $form->field($model, 'cca01_error')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cca02_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cca02_status')->textInput() ?>

    <?= $form->field($model, 'cca02_new')->textInput() ?>

    <?= $form->field($model, 'cca02_draft')->textInput() ?>

    <?= $form->field($model, 'cca02_submitted')->textInput() ?>

    <?= $form->field($model, 'cca02_error')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cca02_1_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cca02_1_status')->textInput() ?>

    <?= $form->field($model, 'cca02_1_new')->textInput() ?>

    <?= $form->field($model, 'cca02_1_draft')->textInput() ?>

    <?= $form->field($model, 'cca02_1_submitted')->textInput() ?>

    <?= $form->field($model, 'cca02_1_error')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cca03_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cca03_status')->textInput() ?>

    <?= $form->field($model, 'cca03_new')->textInput() ?>

    <?= $form->field($model, 'cca03_draft')->textInput() ?>

    <?= $form->field($model, 'cca03_submitted')->textInput() ?>

    <?= $form->field($model, 'cca03_error')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cca04_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cca04_status')->textInput() ?>

    <?= $form->field($model, 'cca04_new')->textInput() ?>

    <?= $form->field($model, 'cca04_draft')->textInput() ?>

    <?= $form->field($model, 'cca04_submitted')->textInput() ?>

    <?= $form->field($model, 'cca04_error')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cca05_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cca05_status')->textInput() ?>

    <?= $form->field($model, 'cca05_new')->textInput() ?>

    <?= $form->field($model, 'cca05_draft')->textInput() ?>

    <?= $form->field($model, 'cca05_submitted')->textInput() ?>

    <?= $form->field($model, 'cca05_error')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
