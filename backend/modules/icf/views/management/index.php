<?php
/* @var $this yii\web\View */
/* @var $user  backend\modules\icf\models\UserProfile */


use kartik\grid\GridView;
use yii\helpers\Html;
$this->title = Yii::t('app', 'กำกับงาน CASCAP');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

echo Html::label('Register : ').' ';
echo Html::a(' ['."<span class='glyphicon glyphicon-remove' style='color: red'></span>".'Error '.$errorRegisterCount.' ]',[
        '/icf/management',
        'action' =>'error'
    ],['style'=>['color'=>'red']]).' ';
echo Html::a('['."<span class='glyphicon glyphicon-time' style='color: orange'></span>".'รอตรวจ '.$waitingRegisterCount.' ]',[
        '/icf/management',
        'action' =>'waiting'
    ],['style'=>['color'=>'orange']]).' ';
echo Html::a('['."<span class='glyphicon glyphicon-ok' style='color: green'></span>".'ผ่าน '.$successRegisterCount.' ]',[
        '/icf/management',
        'action' =>'complete'
    ],['style'=>['color'=>'green']]).' ';
echo Html::a('['."<span class='glyphicon glyphicon-minus' style='color: blue'></span>".'ตรวจแล้วไม่ผ่าน '.$notSuccessConfirmRegisterCount.' ] ',[
        '/icf/management',
        'action' =>'nocomplete'
    ],['style'=>['color'=>'blue']]).' ';
echo Html::a('All',[
        '/icf/management',
        'action' =>'all'
    ],['class' =>'btn btn-info btn-sm']).'<br><br>';
echo GridView::widget([
    'dataProvider'=>$registerProvider,
    //'filterModel'=>$searchModel,
   // 'columns'=>$gridColumns,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'resizableColumns'=>true,
    'columns'=>[
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['style'=>'text-align: center;'],
            'contentOptions' => ['style'=>'min-width:60px;text-align: center;'],
        ],
        [
            'attribute' => 'fullcode',
            'format' => 'text',
            'label' => 'HOSPCODE+PID',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
        ],
        [
            'attribute' => 'fullname',
            'format' => 'text',
            'label' => 'ชื่อ - สกุล',
            'headerOptions' => ['class' => 'text-left'],
            'contentOptions' => ['class' => 'text-left'],
            'value'=> function($model){
                return $model['title'].$model['fullname'];
            }
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'label' => 'Reg + ICF',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                if($model['status']=='1'){
                    return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>",
                        [
                            '/inputdata/redirect-page',
                            'dataid' => $model['register_id'], 'ezf_id' =>'1437377239070461301','rurl'=> base64_encode(Yii::$app->request->url)
                        ],
                        [
                            'title' =>'ตรวจผ่านแล้ว',
                            'data-pjax' => '0'
                        ]);
                }elseif ($model['status']=='2'){
                    return Html::a("<span class='glyphicon glyphicon-time' style='color: orange'></span>",
                        [
                            '/inputdata/redirect-page',
                            'dataid' => $model['register_id'], 'ezf_id' =>'1437377239070461301','rurl'=> base64_encode(Yii::$app->request->url)
                        ],
                        [
                            'title' =>'อัพโหลดใบยินยอมแล้ว รอตรวจใบยินยอมจากเจ้าหน้าที่',
                            'data-pjax' => '0'
                        ]);
                }elseif($model['status']=='3'){
                    return Html::a("<span class='glyphicon glyphicon-minus' style='color: blue'></span>",
                        [
                            '/inputdata/redirect-page',
                            'dataid' => $model['register_id'], 'ezf_id' =>'1437377239070461301','rurl'=> base64_encode(Yii::$app->request->url)
                        ],
                        [
                            'title' =>'ตรวจใบยินยอมแล้ว แต่ไม่ผ่าน',
                            'data-pjax' => '0'
                        ]);
                }else{
                    $strError = str_replace("<br />"," ",$model['error']);
                   return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $model['register_id'], 'ezf_id' =>'1437377239070461301','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                           [
                               'title' =>'ยังไม่อัพโหลดใบยินยอม หรือฟอร์มมี Error '.str_replace("<br>"," ",$strError),
                               'data-pjax' => '0'
                           ]);
                }
            }
        ],
        [
            //'attribute' => 'cca01_status',
            'format' => 'raw',
            'label' => 'CCA-01',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                $sql="SELECT id,error
                      FROM tb_data_2
                      WHERE target=:register_target
                      AND rstat <>'3'
                      ORDER BY id DESC LIMIT 1";
                $cca01 = \Yii::$app->db->createCommand($sql,[':register_target' => $model['register_target']])->queryOne();
                if($cca01['id']==''){
                    return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>",
                        [
                            '/inputdata/step4',
                            'comp_id_target' => '1437725343023632100',
                            'ezf_id' => '1437377239070461302',
                            'target' => base64_encode($model['register_ptid'])
                        ],
                        [
                            'title' => 'เพิ่มฟอร์ม',
                            'data-pjax' => '0'
                        ]);
                }else {
                    if ($cca01['error'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca01['id'], 'ezf_id' => '1437377239070461302','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'ผ่าน',
                                'data-pjax' => '0'
                            ]);
                    } elseif (strlen($cca01['error']) >0) {
                        $strError = str_replace("<br />"," ",$cca01['error']);
                        return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca01['id'], 'ezf_id' => '1437377239070461302','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'มี Error '.str_replace("<br>"," ",$strError),
                                'data-pjax' => '0'
                            ]);
                    }
                }
            }
        ],
        [
            //'attribute' => 'cca01_status',
            'format' => 'raw',
            'label' => 'CCA-02',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                $sql="SELECT id,error,
                      SUM(rstat = 0) AS new,
                      SUM(rstat = 1) AS draft,
                      SUM(rstat = 2) AS submitted
                      FROM tb_data_3
                      WHERE target=:register_target
                      AND rstat <>'3'
                      ORDER BY id DESC LIMIT 1";
                $cca02 = \Yii::$app->db->createCommand($sql,[':register_target' => $model['register_target']])->queryOne();
                if($cca02['id']==''){
                    return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>",
                        [
                            '/inputdata/step4',
                            'comp_id_target' => '1437725343023632100',
                            'ezf_id' => '1437619524091524800',
                            'target' => base64_encode($model['register_ptid'])
                        ],
                        [
                            'title' => 'เพิ่มฟอร์ม',
                            'data-pjax' => '0'
                        ]);
                }else {
                    if ($cca02['error'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca02['id'], 'ezf_id' => '1437619524091524800','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'ผ่าน',
                                'data-pjax' => '0'
                            ]).'('.$cca02['new'].'/'.$cca02['draft'].'/'.$cca02['submitted'].')';
                    } elseif (strlen($cca02['error'])>0) {
                        $strError = str_replace("<br />"," ",$cca02['error']);
                        return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca02['id'], 'ezf_id' => '1437619524091524800','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'มี Error '.str_replace("<br>"," ",$strError),
                                'data-pjax' => '0'
                            ]).'('.$cca02['new'].'/'.$cca02['draft'].'/'.$cca02['submitted'].')';
                    }
                }
            }
        ],
        [
            //'attribute' => 'cca01_status',
            'format' => 'raw',
            'label' => 'CCA-02.1',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                $sql="SELECT id,error,
                      SUM(rstat = 0) AS new,
                      SUM(rstat = 1) AS draft,
                      SUM(rstat = 2) AS submitted
                      FROM tb_data_4
                      WHERE target=:register_target
                      AND rstat <>'3'
                      ORDER BY id DESC LIMIT 1";
                $cca02_1 = \Yii::$app->db->createCommand($sql,[':register_target' => $model['register_target']])->queryOne();
                if($cca02_1['id']==''){
                    return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>",
                        [
                            '/inputdata/step4',
                            'comp_id_target' => '1437725343023632100',
                            'ezf_id' => '1454041742064651700',
                            'target' => base64_encode($model['register_ptid'])
                        ],
                        [
                            'title' => 'เพิ่มฟอร์ม',
                            'data-pjax' => '0'
                        ]);
                }else {
                    if ($cca02_1['error'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca02_1['id'], 'ezf_id' => '1454041742064651700','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'ผ่าน',
                                'data-pjax' => '0'
                            ]).'('.$cca02_1['new'].'/'.$cca02_1['draft'].'/'.$cca02_1['submitted'].')';
                    } elseif (strlen($cca02_1['error'])>0) {
                        $strError = str_replace("<br />"," ",$cca02_1['error']);
                        return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca02_1['id'], 'ezf_id' => '1454041742064651700','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'มี Error '.str_replace("<br>"," ",$strError),
                                'data-pjax' => '0'
                            ]).'('.$cca02_1['new'].'/'.$cca02_1['draft'].'/'.$cca02_1['submitted'].')';
                    }
                }
            }
        ],
        [
            //'attribute' => 'cca01_status',
            'format' => 'raw',
            'label' => 'CCA-03',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                $sql="SELECT id,error,
                      SUM(rstat = 0) AS new,
                      SUM(rstat = 1) AS draft,
                      SUM(rstat = 2) AS submitted
                      FROM tb_data_7
                      WHERE target=:register_target
                      AND rstat <>'3'
                      ORDER BY id DESC LIMIT 1";
                $cca03 = \Yii::$app->db->createCommand($sql,[':register_target' => $model['register_target']])->queryOne();
                if($cca03['id']==''){
                    return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>",
                        [
                            '/inputdata/step4',
                            'comp_id_target' => '1437725343023632100',
                            'ezf_id' => '1451381257025574200',
                            'target' => base64_encode($model['register_ptid'])
                        ],
                        [
                            'title' => 'เพิ่มฟอร์ม',
                            'data-pjax' => '0'
                        ]);
                }else {
                    if ($cca03['error'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca03['id'], 'ezf_id' => '1451381257025574200','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'ผ่าน',
                                'data-pjax' => '0'
                            ]).'('.$cca03['new'].'/'.$cca03['draft'].'/'.$cca03['submitted'].')';
                    } elseif (strlen($cca03['error'])>0) {
                        $strError = str_replace("<br />"," ",$cca03['error']);
                        return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca03['id'], 'ezf_id' => '1451381257025574200','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'มี Error '.str_replace("<br>"," ",$strError),
                                'data-pjax' => '0'
                            ]).'('.$cca03['new'].'/'.$cca03['draft'].'/'.$cca03['submitted'].')';
                    }
                }
            }
        ],
        [
            //'attribute' => 'cca01_status',
            'format' => 'raw',
            'label' => 'CCA-04',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                $sql="SELECT id,error,
                      SUM(rstat = 0) AS new,
                      SUM(rstat = 1) AS draft,
                      SUM(rstat = 2) AS submitted
                      FROM tb_data_8
                      WHERE target=:register_target
                      AND rstat <>'3'
                      ORDER BY id DESC LIMIT 1";
                $cca04 = \Yii::$app->db->createCommand($sql,[':register_target' => $model['register_target']])->queryOne();
                if($cca04['id']==''){
                    return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>",
                        [
                            '/inputdata/step4',
                            'comp_id_target' => '1437725343023632100',
                            'ezf_id' => '1452061550097822200',
                            'target' => base64_encode($model['register_ptid'])
                        ],
                        [
                            'title' => 'เพิ่มฟอร์ม',
                            'data-pjax' => '0'
                        ]);
                }else {
                    if ($cca04['error'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca04['id'], 'ezf_id' => '1452061550097822200','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'ผ่าน',
                                'data-pjax' => '0'
                            ]).'('.$cca04['new'].'/'.$cca04['draft'].'/'.$cca04['submitted'].')';
                    } elseif (strlen($cca04['error'])>0) {
                        $strError = str_replace("<br />"," ",$cca04['error']);
                        return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca04['id'], 'ezf_id' => '1452061550097822200','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'มี Error '.str_replace("<br>"," ",$strError),
                                'data-pjax' => '0'
                            ]).'('.$cca04['new'].'/'.$cca04['draft'].'/'.$cca04['submitted'].')';
                    }
                }
            }
        ],
        [
            //'attribute' => 'cca01_status',
            'format' => 'raw',
            'label' => 'CCA-05',
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'value' =>function($model){
                $sql="SELECT id,error,
                      SUM(rstat = 0) AS new,
                      SUM(rstat = 1) AS draft,
                      SUM(rstat = 2) AS submitted
                      FROM tb_data_11
                      WHERE target=:register_target
                      AND rstat <>'3'
                      ORDER BY id DESC LIMIT 1";
                $cca05 = \Yii::$app->db->createCommand($sql,[':register_target' => $model['register_target']])->queryOne();
                if($cca05['id']==''){
                    return Html::a("<span class='glyphicon glyphicon-plus' style='color:pink'></span>",
                        [
                            '/inputdata/step4',
                            'comp_id_target' => '1437725343023632100',
                            'ezf_id' => '1440515302053265900',
                            'target' => base64_encode($model['register_ptid'])
                        ],
                        [
                            'title' => 'เพิ่มฟอร์ม',
                            'data-pjax' => '0'
                        ]);
                }else {
                    if ($cca05['error'] == '') {
                        return Html::a("<span class='glyphicon glyphicon-ok' style='color: green'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca05['id'], 'ezf_id' => '1440515302053265900','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'ผ่าน',
                                'data-pjax' => '0'
                            ]).'('.$cca05['new'].'/'.$cca05['draft'].'/'.$cca05['submitted'].')';
                    } elseif (strlen($cca05['error'])>0) {
                        $strError = str_replace("<br />"," ",$cca05['error']);
                        return Html::a("<span class='glyphicon glyphicon-remove' style='color: red'></span>",
                            [
                                '/inputdata/redirect-page',
                                'dataid' => $cca05['id'], 'ezf_id' => '1440515302053265900','rurl'=> base64_encode(Yii::$app->request->url)
                            ],
                            [
                                'title' => 'มี Error '.str_replace("<br>"," ",$strError),
                                'data-pjax' => '0'
                            ]).'('.$cca05['new'].'/'.$cca05['draft'].'/'.$cca05['submitted'].')';
                    }
                }
            }
        ],
    ],
    'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>true, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar'=> [
//        ['content'=>
//            Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type'=>'button', 'title'=>Yii::t('kvgrid', 'Add Book'), 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . ' '.
//            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['grid-demo'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=>Yii::t('kvgrid', 'Reset Grid')])
//        ],
        '{export}',
        '{toggleData}',
    ],
    // set export properties
    'export'=>[
    'fontAwesome'=>true
],
    // parameters from the demo form
    'bordered'=>$bordered,
    'striped'=>$striped,
    'condensed'=>$condensed,
    'responsive'=>true,
    'hover'=>true,
    //'showPageSummary' => true,
    'showPageSummary'=>$pageSummary,
    'panel'=>[
    'type'=>GridView::TYPE_PRIMARY,
    'heading'=>$heading,
],
    'persistResize'=>false,
    'exportConfig'=>$exportConfig,
]);
?>
