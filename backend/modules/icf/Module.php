<?php

namespace backend\modules\icf;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\icf\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
