<?php
namespace backend\modules\icf\classes;

use Yii;
use yii\helpers\Html;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class ICFFunc {
    
    public static function menuICFAll() {
	//$txtMenu[];
       
        $txtMenu[0]=  Html::a('Register', ['/icf/reg'], ['class'=>'btn btn-info', 'role'=>'button']);
	return $txtMenu;
    }
    
    public static function getDatefromYearPeriod($year){
        if( strlen($year)== 0 ){
            $year='59';
        }
        if($year=='57'){
            $startDate='2013-10-01';
            $endDate='2014-09-30';
        }else if($year=='58'){
            $startDate='2014-10-01';
            $endDate='2015-09-30';
        }else if($year=='59'){
            // ปีงบ 59
            $startDate='2015-10-01';
            $endDate='2016-09-30';
        }else if($year=='60'){
            // ปีงบ 60
            $startDate='2016-10-01';
            $endDate='2017-09-30';
        }else{
            $startDate='2015-10-01';
            $endDate='2016-09-30';
        }
        
        $out['startDate']=$startDate;
        $out['endDate']=$endDate;
        return $out;
    }
}
