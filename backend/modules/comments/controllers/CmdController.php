<?php

namespace backend\modules\comments\controllers;

use Yii;
use backend\modules\comments\models\Cmd;
use backend\modules\comments\models\CmdSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

class CmdController extends Controller
{

    public function behaviors()
    {
        return [
        'verbs' => [
            'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $table= "inv_comment";
    public $table_user_profile = "user_profile";
    
    public function actionIndex()
    {

        $sql="SELECT 
        (select count(vote) from inv_comment as i5 where vote =5) as v5,
        (select count(vote) from inv_comment as i4 where vote =4) as v4,
        (select count(vote) from inv_comment as i3 where vote =3) as v3,
        (select count(vote) from inv_comment as i2 where vote =2) as v2,
        (select count(vote) from inv_comment as i1 where vote =1) as v1
        FROM inv_comment limit 1";

        $vote = Yii::$app->db->createCommand($sql)->queryAll();
        $count = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
        return $this->render("index",[
            'count'=>$count,
            'vote'=>$vote
            ]);    
        
    }//end Index
    public function actionCheckuser()
    {
         $user_id = Yii::$app->user->identity->id;
         $user_cmd = Cmd::find()->where(["user_id"=>$user_id])->one();
         if(!empty($user_cmd))
         {
            echo $user_id." ได้แสดงความคิดเห็นแล้วครับ";
         }
         //echo $user_id;
    }
    public function actionCounts()
    {
        $сount = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
        $sum = Yii::$app->db->createCommand("select sum(vote) as sums from {$this->table}")->queryAll();
        $sums = (int)$sum[0]['sums'];
        $counts =  round($sums /= $сount,1);
        $strCounts = explode(".","".$counts);

        $html = "";

        $html .= "<span class=\"rating-num\">".number_format($counts,1)."</span></span>";

        $html .= "<div class=\"rating-stars\">";
             // $arr = [1,2,3,4,5];  
        if($strCounts[1]==0 || empty($strCounts[1])){
            $strCounts[1]=1;
        }

        for($i=1; $i<=$counts; $i++)
        {
            if($i == $strCounts[0])
            {
                for($j=0; $j<$strCounts[0]; $j++)
                {
                    $html .= "<apan><i class='fa fa-star' style=\"color: #737373;\"></i></span>";
                            }//end for 

                            $number = 5-$strCounts[0];

                            for($j=0; $j<$number; $j++)
                            {
                                if($strCounts[1]  >= 5)
                                {
                                    $html .= "<i class=\"fa fa-star-half-o\" style=\"color: #737373;\"></i>";
                                    $strCounts[1] =1;
                                    
                                }else{

                                    $html .= "<apan><i class='fa fa-star-o' style=\"color: #737373;\"></i></span>";
                                    
                                } //end if
                                
                            }//end for
                        }//end for 
                        
                    }
                    $html .= "</div>";
                    $html .= "<div class=\"rating-users\">";
                    $html .= "<i class=\"icon-user\"></i> ".$сount ." คน";
                    $html .= "</div>";
                    echo $html;  
    }//Counts 
    public function actionCountInv()
    {
       $сount = Yii::$app->db->createCommand("select count(*) from {$this->table}")->queryScalar();
       echo $сount;
   }
   public function actionShowInv()
   {
    $html = "";
    $pageSize = 3;
    if(!empty($_POST['pageSize']))
    {
        $pageSize = $_POST['pageSize'];
    }
           // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //$sql="SELECT * FROM {$this->table} ORDER BY id DESC LIMIT 0,$pageSize";
     $uid = "";
     if(!empty($_POST['uid']))
     {

        $uid = Yii::$app->user->identity->id;
        //echo $uid;exit();
     }
  
       if(empty($uid)){
        $sql="
            SELECT us.username,concat(u.firstname,'  ',u.lastname) as name, concat(u.avatar_base_url,'/',u.avatar_path) as images, i.*
             FROM inv_comment as i
            INNER JOIN  user_profile as u
            on u.user_id = i.user_id
            INNER JOIN user as us 
            ON i.user_id = us.id
            ORDER BY id DESC LIMIT 0,$pageSize
        "; 
       }else{
           $sql="
            SELECT us.id,us.username,concat(u.firstname,'  ',u.lastname) as name, concat(u.avatar_base_url,'/',u.avatar_path) as images, i.*
             FROM inv_comment as i
            INNER JOIN  user_profile as u
            on u.user_id = i.user_id
            INNER JOIN user as us 
            ON i.user_id = us.id
            WHERE us.id = $uid LIMIT 1"; 
       }
    
    $cmd= Yii::$app->db->createCommand($sql)->queryAll();
    $arr = array();
    foreach($cmd as $c)
    {
        $arr[] = [
        'id'=>$c['id'],
        'vote'=>$c['vote'],
        'message'=>$c['message'],
        'updated_at'=>$c['updated_at'],
        'user_id'=>$c['user_id'],
        'name'=>$c['name'],
        'images'=>$c['images']
        ];
    }
    $cmds = new Cmd();
    foreach($arr as $a){
        if(!empty($uid))
        {
            $html .= "<div class=\"media\" style='background: #ffffff; padding: 5px; border-radius: 5px;'>";
        }else{
            $html .= "<div class=\"media\" style='background: #ffffff; padding: 5px; border-radius: 5px;border: 1px solid rgba(9, 9, 35, 0.08);'>";
        }
        
        $html .= "<div class=\"media-left\">";
        
        if(!empty($a['images'])){
            $html .= "<img src='".$a['images']."' class=\"media-object img img-circle\" style=\"width:60px\">";
        }else{
            $html .= "<img src=\"/img/anonymous.jpg\" class=\"media-object img img-circle\" style=\"width:60px\">";
        }

       // $html .= "<img src=".Yii::$app->user->identity->userProfile->getAvatar() ?: '/img/anonymous.jpg' ." alt=\"\" class=\"center-block img-thumbnail img-responsive\">";


        $html .= "</div>";
        $html .= "<div class=\"media-body\">";
        $html .= "<h5 class=\"media-heading\">".$a['name']."</h5>";
        if(!empty($uid))
        {
            $html .= "<span class='pull-right'><button class='btn btn-default btn-sm'><i class='glyphicon glyphicon-pencil'></i>แก้ไข</button>
            <button class='btn btn-default btn-sm'><i class='glyphicon glyphicon-trash'></i> ลบ</button></span>";
        }
        $html .= "<p>";
        $vote = $a['vote'];
        if(empty($vote)){
            $vote = 0;
        }
        $vote_arr = [1,2,3,4,5]; 
        $voteEnd = 5;
        foreach($vote_arr as $v){
            if($vote == $v)
            {
                for($i=0; $i<$vote; $i++)
                {
                    $html .= '<i class="glyphicon glyphicon-star" style="color:#ffb234;"></i>';
                }

                $x = 5-$vote;
                for($j=0; $j<$x; $j++){
                    $html .= '<i class="glyphicon glyphicon-star-empty"></i>';
                }
            } 
        }
        $html .= '</p>'; 
        $html .= "<p>".$a['message']."</p>"; 

        $html .= "</div>";

        $html .= "</div>";
    }
    echo $html;       
}
public function actionRealtime()
{
    $sql="SELECT * FROM {$this->table} WHERE `real` = 1";
    $model = Yii::$app->db->createCommand($sql)->queryAll();
       // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $count = Count($model);
    if($count == 0 )
    {
        $count ='0';
    }
    echo $count;  
    }//Realtime 
    public function actionSaverealtime()
    {
        $sql="UPDATE {$this->table} SET `real` = 0";
        $model = Yii::$app->db->createCommand($sql)->execute();

    }//Saverealtime
    public function actionShowcmd(){
        //$cmd = Cmd::find()->all();
        try{

            $endPage = 5;
            if(!empty($_POST))
            {
                $endPage = $_POST["endPage"];
            }
            $sql="SELECT * FROM {$this->table} ORDER BY id DESC LIMIT 0,$endPage";
            $cmd= Yii::$app->db->createCommand($sql)->queryAll();

        }catch(Exception $e){
            echo $e->getMessage();
        }
        
        return $this->render('cmdshow',[
            'cmd'=>$cmd
            ]);
        
    }//end Showcomment
    public function actionShowform()
    {
         $sql="
            SELECT  concat(u.avatar_base_url,'/',u.avatar_path) as images 
             FROM  user_profile as u
            WHERE user_id = ".Yii::$app->user->identity->id."
             LIMIT 1        
        "; 
        $img = Yii::$app->db->createCommand($sql)->queryOne();
        echo $img['images']; 
        return $this->renderAjax("create",[
            'img'=>$img
        ]);
    }//end Showform
    public function actionCreate()
    {

        $model = new Cmd();
        
        if(!empty($_POST))
        {

            $date=Date('Y-m-d');
            $vote = trim($_POST["vote"]);
            $message = trim($_POST["message"]);
            $message_alert=[];

            if($model->checkValidations($message) == true){
                $message_alert=[
                'status'=>404,
                'message'=>'คุณไม่สามารถกรอก script ได้นะครับ'
                ];
                echo json_encode($message_alert);
                exit();
            }


            $columns = array(
                'gid' => 1,
                'user_id' => 1, 
                'vote'=>$vote,
                'message'=>$message,
                'created_by'=>$date,
                'created_at'=>$date,
                'updated_by'=>$date,
                'updated_at'=>$date,
                );          
            $model->save();
            $cmd = Yii::$app->db->createCommand();
            $table= $this->table;
            $cmd->insert($table,$columns);

            if($cmd->execute())
            {
               $message_alert=[
               'status'=>200,
               'message'=>'บันทึกข้อมูลแล้วครับ'
               ];
               echo json_encode($message_alert);
               exit();
           } 

        }//end Create
        




    }//actionCreate ---> Create
    public function actionUpdate($id)
    {

    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Cmd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
