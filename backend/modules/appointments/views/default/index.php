<!--
/*
 * Module Follow up.
 * Developed by Mark.
 * Date : 2016-04-01
*/
-->
<?php
use yii\helpers\Url;

$this->title = Yii::t('', 'CCA-02 Follow Up.');

$dateNow = new DateTime('NOW');
$interval543Year = new DateInterval('P543Y');
$interval2Month = new DateInterval('P2M');
$dateSub = new DateTime('NOW');
$dateSub->add($interval543Year);
$dateSub->sub($interval2Month);
$dateAdd = new DateTime('NOW');
$dateAdd->add($interval543Year);
$dateAdd->add($interval2Month);

$this->registerJs('
    function getValueCheckbox(){
        // Get value from checked in checkbox.
        var chkMonthArray = [];
        
        $(".filterAppointmentMonth:checked").each(function() {
            chkMonthArray.push($(this).val());
        });
        
        if( !(chkMonthArray.length>0) ){
            // Get all value when unchecked.
            $(".filterAppointmentMonth").each(function() {
                chkMonthArray.push($(this).val());
            });
        }
        
        var selected = chkMonthArray.join(\',\');
        
        return selected;
    }
    
    $(".btnExportXls").click(function(){
        var tableTreatment = $("#treatment").html();
        tableTreatment = encodeURIComponent(tableTreatment);
        console.log(tableTreatment);
        window.open("data:application/vnd.ms-excel,"+tableTreatment);
    });
    
    $(".btnFilterAppointmentMonth").click(function(){
        $("#ReportAppointment").html(\'<tr><td colspan="7"><div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div></td></tr>\');
        
        var selected = getValueCheckbox();
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "'.Url::to('/appointments/default/filter-appointment').'",
            data    : {
                selected: selected,
                sitecode: "'.$qrySiteDetails['hcode'].'"
            },
            success  : function(response) {
                $("#ReportAppointment").html(response);
            },
            error : function(){
                $("#ReportAppointment").html("Error");
            }
        });
    });
    
    $(".btnViewReport").click(function(){
        $("#ReportAppointment").html(\'<tr><td colspan="7"><div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div></td></tr>\');
        
        var selected = getValueCheckbox();
        var startDate = $(".startDate").val();
        var endDate = $(".endDate").val();
        
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "'.Url::to('/appointments/default/filter-appointment').'",
            data    : {
                selected: selected,
                startDate: startDate,
                endDate: endDate,
                sitecode: "'.$qrySiteDetails['hcode'].'"
            },
            success  : function(response) {
                $("#ReportAppointment").html(response);
            },
            error : function(){
                $("#ReportAppointment").html("Error");
            }
        });
    });
    
    $("#filterRange4Month").click(function(){
        $("#ReportAppointment").html(\'<tr><td colspan="7"><div class="row text-center"><i class="fa fa-spinner fa-spin fa-3x" style="margin:50px 0;"></i></div></td></tr>\');
        $.ajax({
            type    : "GET",
            cache   : false,
            url     : "'.Url::to('/appointments/default/get-report').'",
            data    : {
                numOfMonthBefore: 2,
                numOfMonthAfter: 2,
                sitecode: "'.$qrySiteDetails['hcode'].'"
            },
            success  : function(response) {
                $("#ReportAppointment").html(response);
            },
            error : function(){
                $("#ReportAppointment").html("Error");
            }
        });
    });
');
?>
<div class="appointments-default-index">
    <h1><?= $qrySiteDetails['hcode']. " : " .$qrySiteDetails['name'] ?></h1>
    <div class="row">
        <article>
            <div class="container">
                <div class="col-md-8">
                    <form class="form form-group appointmentFilter">
                        <dl class="dl-horizontal">
                            <dt>คัดกรองตามการนัดหมาย</dt>
                            <dd>
                                <div class="col-xs-12 col-sm-7 col-md-7">
                                    <label class="checkbox-inline">
                                        <input class="filterAppointmentMonth" id="filterAppointmentMonth" type="checkbox" value="1"> นัด 1 ปี
                                    </label>
                                    <label class="checkbox-inline">
                                        <input class="filterAppointmentMonth" id="filterAppointmentMonth" type="checkbox" value="2"> นัด 6 เดือน
                                    </label>
                                    <label class="checkbox-inline">
                                        <input class="filterAppointmentMonth" id="filterAppointmentMonth" type="checkbox" value="3"> Suspected CCA
                                    </label>
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <button type="button" class="btn btn-info form-control btnFilterAppointmentMonth">ดูข้อมูลตามการนัดหมาย</button>
                                </div>
                            </dd>
                            <dt><br>คัดกรองตามวันเข้าตรวจ</dt>
                            <dd>
                                <div class="col-md-12">
                                    <div class="row">
                                        <br>
                                        <div class="col-xs-4 col-sm-3 col-md-3">
                                            <strong><p class="text-right">จากวันที่</p></strong>
                                        </div>
                                        <div class="col-xs-8 col-sm-9 ol-md-9">
                                            <input type="date" name="startDate" id="startDate" class="startDate form-control" min="2013-02-09" max="<?= $dateNow->format('Y-m-d') ?>" value="2013-02-09">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br>
                                        <div class="col-xs-4 col-sm-3 col-md-3">
                                            <strong><p class="text-right">ถึงวันที่</p></strong>
                                        </div>
                                        <div class="col-xs-8 col-sm-9 col-md-9">
                                            <input type="date" name="endDate" id="endDate" class="endDate form-control" min="2013-02-09" max="<?= $dateNow->format('Y-m-d') ?>" value="<?= $dateNow->format('Y-m-d') ?>">
                                        </div>
                                    </div>
                                </div>
                            </dd>
                            <dt></dt>
                            <dd>
                                <br>
                                <button type="button" class="btnViewReport btn btn-info form-control">ดูข้อมูลรายงาน</button>
                            </dd>
                        </dl>
                    </form>
                    <hr>
                    <dl class="dl-horizontal">
                        <dt>ข้อมูลในช่วง 4 เดือน</dt>
                        <dd>
                            <button id="filterRange4Month" class="btn btn-info form-control"><?= $dateSub->format('d/m/Y').' - '.$dateAdd->format('d/m/Y') ?></button>
                        </dd>
                    </dl>
                </div>
            </div>
        </article>
    </div>
    <br>
    <article>
        <div class="row">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">
                <button class="btn btn-info form-control btnExportXls">Export to xls</button>
            </div>
        </div>
        <br>
        <div class="treatment" id="treatment">
            <table class="table table-bordered table-hover table-striped tebltTreatment">
                <thead>
                <tr>
                    <td>No.</td>
                    <td>รหัสโรงพยาบาล</td>
                    <td>ชื่อผู้ทำอัลตราซาวน์</td>
                    <td>Date Visit</td>
                    <td>Month Appointment</td>
                    <td>ผ่านการตรวจมาเป็นเวลา [เดือน]</td>
                    <td>Date Appointment</td>
                </tr>
                </thead>
                <tbody id="ReportAppointment">
                <?= $strReportAppointment ?>
                </tbody>
            </table>
            <div class="loading"></div>
        </div>
    </article>
</div>
