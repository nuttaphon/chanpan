<?php

use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

?>

<?php
    if (1 == $type) {
?>

<label>ตัวอย่าง</label>
<hr>
<select class="form-control js-sin">
  <?php
    foreach ($dynamic_datas as $key => $dynamic_data) {
        echo "<option value='".$key."'>".$dynamic_data."</option>";
    }
  ?>
</select>
<br>
<?php
    } else {
?>

<label>ตัวอย่าง</label>
<hr>
<select class="form-control js-mul" multiple="">
  <?php
    foreach ($dynamic_datas as $key => $dynamic_data) {
        echo "<option value='".$key."'>".$dynamic_data."</option>";
    }
  ?>
</select>
<br>
<?php
    }
?>

<?php $this->registerJsFile('/select2-3.5.2/select2.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?php $this->registerCssFile("/select2-3.5.2/select2.css");?>
<?php $this->registerCssFile("/select2-3.5.2/select2-bootstrap.css");?>

<?php
  $this->registerJs("$('.js-sin').select2();");
  $this->registerJs("$('.js-mul').select2();");
?>
