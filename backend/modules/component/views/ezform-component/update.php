<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\component\models\EzformComponent */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Ezform Component',
]) . ' ' . $model->comp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezform Components'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->comp_id, 'url' => ['view', 'id' => $model->comp_id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="ezform-component-update">

    <?= $this->render('_form', [
        'model' => $model,
        'listData' => $listData,
        'listFormFields' => $listFormFields,
    ]) ?>

</div>
