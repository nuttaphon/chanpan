<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\component\models\EzformComponentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ezform-component-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'comp_id') ?>

    <?= $form->field($model, 'comp_name') ?>

    <?= $form->field($model, 'comp_type') ?>

    <?= $form->field($model, 'comp_select') ?>

    <?= $form->field($model, 'ezf_id') ?>

    <?php // echo $form->field($model, 'field_id_key') ?>

    <?php // echo $form->field($model, 'field_id_desc') ?>

    <?php // echo $form->field($model, 'comp_order') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'user_create') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
