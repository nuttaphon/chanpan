<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\component\models\EzformComponent */

$this->title = 'ตัวอย่าง เป้าหมาย';//$model->comp_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Ezform Components'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ezform-component-view">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="itemModalLabel"><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="modal-body">
        <?php $form = \yii\bootstrap\ActiveForm::begin(['id'=>$model->formName()]); ?>
        <?php
        echo $form->field($model, 'field_id_desc')->widget(Select2::classname(), [
            'id' =>'field_id_desc',
            'data' => $dynamic_datas,
            'options' => ['placeholder' => 'เลือก', 'multiple' => false],
            'pluginOptions' => [

                'maximumInputLength' => 10
            ],
        ])->label($model->comp_name);
        ?>
        <?php \yii\bootstrap\ActiveForm::end(); ?>
</div>
</div>



