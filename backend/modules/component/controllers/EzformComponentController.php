<?php

namespace backend\modules\component\controllers;

use Yii;
use backend\modules\component\models\EzformComponent;
use backend\modules\component\models\EzformComponentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use common\lib\codeerror\helpers\GenMillisecTime;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformFields;
use yii\helpers\ArrayHelper;
use backend\models\Dynamic;
use yii\widgets\ActiveForm;

/**
 * EzformComponentController implements the CRUD actions for EzformComponent model.
 */
class EzformComponentController extends Controller
{
     public $layout = '@backend/views/layouts/common';
   
    public function behaviors()
    {
        return [
        'access' => [
        'class' => AccessControl::className(),
        'rules' => [
        [
        'allow' => true,
        'actions' => ['index', 'view'],
        'roles' => ['?', '@'],
        ],
        [
        'allow' => true,
        'actions' => ['create', 'update', 'delete', 'undo'],
        'roles' => ['@'],
        ],
        ],
        ],
        'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
        'delete' => ['post'],
        ],
        ],
        ];
    }

    public function beforeAction($action) {
        if (parent::beforeAction($action)) {
            if (in_array($action->id, array('create', 'update'))) {

            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all EzformComponent models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new EzformComponentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProviderMyForm = $searchModel->searchMyFormComponent(Yii::$app->request->queryParams);
        $dataProviderMyFormDelete = $searchModel->searchMyFormComponentDelete(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderMyForm' => $dataProviderMyForm,
            'dataProviderMyFormDelete' => $dataProviderMyFormDelete,
            'id' => $id,
            ]);
    }

    /**
     * Displays a single EzformComponent model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->getRequest()->isAjax) {

            $model = $this->findModel($id);

            /**  Get field key name. */
            $field_key = EzformFields::find()
            ->where(['ezf_field_id' => $model->field_id_key])
            ->one();

            /** Get field desc name. */
            if (!empty($model->field_id_desc)) {
                $field_desc_array = explode(',', $model->field_id_desc);
            }

            $field_desc_list = '';

            foreach ($field_desc_array as $value) {

                if (!empty($value)) {
                    $field_desc = EzformFields::find()
                    ->where(['ezf_field_id' => $value])
                    ->one();

                    $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
                }

            }

            $field_desc_list = substr($field_desc_list, 0 ,strlen($field_desc_list)-6);

            /** Get table name. */
            $ezform = Ezform::find()
            ->where(['ezf_id' => $model->ezf_id])
            ->one();

            $dynamic_datas = ArrayHelper::map(Yii::$app->db->createCommand('SELECT *, CONCAT('.$field_desc_list.') AS field_desc FROM '.$ezform->ezf_table)
                ->queryAll(), $field_key->ezf_field_name, 'field_desc');

            //\yii\helpers\VarDumper::dump($select, 10, true);

            return $this->renderAjax('view', [
                'model' => $model,
                'dynamic_datas' => $dynamic_datas
                ]);
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Creates a new EzformComponent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->getRequest()->isAjax) {

            isset($_GET['id']) ? $id = $_GET['id'] : $id = 0;

            $ez_forms = Ezform::find()
            ->where('ezf_id > :ezf_id', [':ezf_id' => 100000])
            ->andWhere('ezform.status <> :status AND ezform.user_create = :user_create', [':status' => 3, ':user_create'=>Yii::$app->user->id])
            ->all();

            $listData = ArrayHelper::map($ez_forms, 'ezf_id', 'ezf_name');

            $ezformFields = EzformFields::find()->where(['ezf_id' => $id])->all();
            $defaultField = EzformFields::find()->where(['ezf_id' => $id,'ezf_field_name'=>'id'])->one();
            $listFormFields = ArrayHelper::map($ezformFields, 'ezf_field_id', 'ezf_field_label');

            $model = new EzformComponent();

            $model->ezf_id = $id;

            $field_id = [];

            $field_id = Yii::$app->request->post('EzformComponent');

            if ($model->load(Yii::$app->request->post())) {

                Yii::$app->response->format = Response::FORMAT_JSON;

                $model->comp_id = GenMillisecTime::getMillisecTime();
                $model->comp_type = 2;
                $model->status = 1;
                $model->user_create = Yii::$app->user->id;

                $model->field_search = implode(",", $field_id['field_search']);
                $model->field_id_desc = implode(",", $field_id['field_id_desc']);


                //\yii\helpers\VarDumper::dump($model);
                //$model->ezf_id = $id;

                /*if (!empty($model->field_id_desc)) {

                    $field_id_desc_sum = '';
                    foreach ($model->field_id_desc as $key => $field_id_desc) {
                        $field_id_desc_sum .= $field_id_desc.',';
                    }

                    $model->field_id_desc = substr($field_id_desc_sum, 0 ,strlen($field_id_desc_sum)-1);
                }*/

        //if ($model->validate()) {
            // all inputs are valid
        //} else {
            // validation failed: $errors is an array containing error messages
            //$errors = $model->errors;
        //}

        //\yii\helpers\VarDumper::dump($model->attributes,10,true);exit;

        //Yii::app()->end();

                if ($model->save()) {
                    $result = [
                    'status' => 'success',
                    'action' => 'create',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
                    'data' => $model,
                    ];
                    return $result;
                } else {
                    $result = [
                    'status' => 'error',
                    'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
                    'data' => $model,
                    ];
                    return $result;
                }
            } else {
                $model->shared = 1;
                $model->comp_order = 10;

                return $this->renderAjax('create', [
                    'model' => $model,
                    'listData' => $listData,
                    'listFormFields' => $listFormFields,
                    'defaultField' => $defaultField,
                    ]);
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Updates an existing EzformComponent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = $this->findModel($id);

            $ez_forms = Ezform::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $model->ezf_id])
            ->andWhere('ezform.status <> :status', [':status' => 3])
            ->all();

            $listData = ArrayHelper::map($ez_forms, 'ezf_id', 'ezf_name');

            $EzformFields = EzformFields::find()
                                        ->where(['ezf_id' => $model->ezf_id])
                                        ->all();

            $listFormFields = ArrayHelper::map($EzformFields, 'ezf_field_id', 'ezf_field_label');

            $field_id = Yii::$app->request->post('EzformComponent');

            if ($model->load(Yii::$app->request->post())) {
                $model->field_search = implode(",", $field_id['field_search']);
                $model->field_id_desc = implode(",", $field_id['field_id_desc']);

                Yii::$app->response->format = Response::FORMAT_JSON;
                if ($model->save()) {

                    $result = [
                    'status' => 'success',
                    'action' => 'update',
                    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
                    'data' => $model,
                    ];
                    return $result;
                } else {
                    $result = [
                    'status' => 'error',
                    'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not update the data.'),
                    'data' => $model,
                    ];
                    return $result;
                }
            } else {
                $model->field_id_desc = explode(',', $model->field_id_desc);
                $model->field_search = explode(',', $model->field_search);

                return $this->renderAjax('update', [
                    'model' => $model,
                    'listData' => $listData,
                    'listFormFields' => $listFormFields,
                    ]);
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Deletes an existing EzformComponent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            if (3 == $model->status) {
                $this->findModel($id)->delete();

                $result = [
                'status' => 'success',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
                ];
            } else {
                $model->status = 3;
            }

            if ($model->save()) {
                $result = [
                'status' => 'success',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
                ];
                return $result;
            } else {
                $result = [
                'status' => 'error',
                'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not delete the data.'),
                'data' => $id,
                ];
                return $result;
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionPreview($type, $field_id, $field_desc, $form_id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = new EzformComponent();

            /**  Get field key name. */
            $field_key = EzformFields::find()
            ->where(['ezf_field_id' => $field_id])
            ->one();

            /** Get field desc name. */
            if (!empty($field_desc)) {
                $field_desc_array = explode(',', $field_desc);
            }

            $field_desc_list = '';

            foreach ($field_desc_array as $value) {
                $field_desc = EzformFields::find()
                ->where(['ezf_field_id' => $value])
                ->one();

                $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
            }

            $field_desc_list = substr($field_desc_list, 0 ,strlen($field_desc_list)-6);
            str_replace('`', '', $field_desc_list);
            /** Get table name. */
            $ezform = Ezform::find()
            ->where(['ezf_id' => $form_id])
            ->one();

            //$dynamic = new Dynamic();

            //$dynamic->setTableName($ezform->ezf_table);

            //$data_list = [];

            $dynamic_datas = ArrayHelper::map(Yii::$app->db->createCommand('SELECT *, CONCAT('.$field_desc_list.') AS field_desc FROM '.$ezform->ezf_table)
                ->queryAll(), $field_key->ezf_field_name, 'field_desc');

            //\yii\helpers\VarDumper::dump($dynamic_datas, 10, true);

            return $this->renderAjax('preview', [
                'model' => $model,
                'type' => $type,
                'dynamic_datas' => $dynamic_datas,
                ]);
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionOption($form_id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            $model = new EzformComponent();

            $ezformFields = EzformFields::find()
            ->where(['ezf_id' => $form_id])
            ->all();

            $listFormFields = ArrayHelper::map($ezformFields, 'ezf_field_id', 'ezf_field_label');

            return $this->renderAjax('option', [
                'model' => $model,
                'listFormFields' => $listFormFields,
                ]);
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionUndo($id)
    {
        if (Yii::$app->getRequest()->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);

            $model->status = 1;

            if ($model->save()) {
                $result = [
                'status' => 'success',
                'action' => 'update',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
                'data' => $id,
                ];
                return $result;
            } else {
                $result = [
                'status' => 'error',
                'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not delete the data.'),
                'data' => $id,
                ];
                return $result;
            }
        } else {
            throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Finds the EzformComponent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EzformComponent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EzformComponent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
