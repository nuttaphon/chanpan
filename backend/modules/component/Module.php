<?php

namespace backend\modules\component;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\component\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
