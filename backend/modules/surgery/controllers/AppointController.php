<?php

namespace backend\modules\surgery\controllers;

use Yii;
use yii\web\Controller;

class AppointController extends Controller
{
    public function actionIndex()
    {
	$dataDept = \backend\modules\surgery\classes\SurgeryQuery::getDept();
	
	if($dataDept){
	    $dataDept = \yii\helpers\ArrayHelper::map($dataDept, 'id', 'name');
	} else {
	    $dataDept = [];
	}
	
	$person = isset($_GET['person'])?$_GET['person']:1;
	$dept = isset($_GET['dept'])?$_GET['dept']:1;
	$now_date = isset($_GET['date'])?$_GET['date']:date('Y-m-d');
	
	$events = [];
	$dataEvents = \backend\modules\surgery\classes\SurgeryQuery::getEvent($person, $dept, $now_date);
	if($dataEvents){
	    foreach ($dataEvents as $key => $value) {
		//\yii\helpers\VarDumper::dump($value,10,true);
		
		$event = new \yii2fullcalendar\models\Event();
		$event->id = '1_my_'.$key;
		$event->title = "{$dataDept[$dept]} ({$value['my']})";
		$event->start = $value['ddate'];
		$event->color = '#7BD148';
		$event->url = '#';
		$events[] = $event;
		
		$event = new \yii2fullcalendar\models\Event();
		$event->id = '2_other_'.$key;
		$event->title = "Other ({$value['other']})";
		$event->start = $value['ddate'];
		$event->color = '#FFF';
		$event->textColor = '#3c8dbc';
		$events[] = $event;
		
		$event = new \yii2fullcalendar\models\Event();
		$event->id = '3_all_'.$key;
		$event->title = 'All';
		$event->url = '#';
		$event->start = $value['ddate'];
		$event->color = '#F691B2';
		$events[] = $event;
		    
		//#FF7537
	    }
	    
	    //exit();
	}
	
	$y = substr($now_date, 0, 4);
	$m = substr($now_date, 5, 2);
	
	$stopEvents = \backend\modules\surgery\classes\SurgeryQuery::getEventStop($y);
	$stopEventsCustom = \backend\modules\surgery\classes\SurgeryQuery::getEventStopCustom($y, $m);
	
	if($stopEvents){
	    if($stopEventsCustom){
		$stopEvents = \yii\helpers\ArrayHelper::merge($stopEvents, $stopEventsCustom);
	    }
	    
	    foreach ($stopEvents as $key => $value) {
		$event = new \yii2fullcalendar\models\Event();
		$event->id = '4_stop_'.$key;
		$event->title = $value['hname'];
		$event->start = $value['ddate'];
		$event->color = '#FFF';
		$event->textColor = '#FF0000';
		$events[] = $event;
	    }
	}
	
	$commtEvents = \backend\modules\surgery\classes\SurgeryQuery::getComment($person, $dept, $now_date);
	
        return $this->render('index',[
	    'person'=>$person,
	    'dept'=>$dept,
	    'now_date'=>$now_date,
	    'events'=>$events,
	    'dataDept'=>$dataDept,
	    'commtEvents'=>$commtEvents
	]);
    }
    
    public function actionViewLink()
    {
	
	    $ptype = isset($_GET['ptype'])?$_GET['ptype']:1;
	    $division = isset($_GET['division'])?$_GET['division']:1;
	    $ddate = isset($_GET['ddate'])?$_GET['ddate']:date('Y-m-d');
	    $rurl = isset($_GET['rurl'])?$_GET['rurl']:base64_encode(Yii::$app->request->url);
	    
	    $datasetArr = [
		'ptype' => $ptype,
		'division' => $division,
		'ddate'=>$ddate,
	    ];
	    $dataset = base64_encode(\yii\helpers\Json::encode($datasetArr));
	    
	    $url = \yii\helpers\Url::to(['/inputdata/step2',
			'ezf_id'=>'1464033435067331400',
			'target'=>'',
			'dataset'=>$dataset,
			'comp_id_target'=>'1437725343023632100',
			'rurl'=>$rurl,
		]);
	    return $this->redirect($url);
	
    }
    
    public function actionViewAll()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $person = isset($_GET['person'])?$_GET['person']:1;
	    $dept = isset($_GET['dept'])?$_GET['dept']:1;
	    $date = isset($_GET['date'])?$_GET['date']:date('Y-m-d');
	    
	    $dp = \backend\modules\surgery\classes\SurgeryQuery::getDpAll($person, $dept, $date);
	    
	    return $this->renderAjax('_view_all', [
		'person'=>$person,
		'dept'=>$dept,
		'date'=>$date,
		'dp'=>$dp,
	    ]);
	} 
    }
    
    public function actionView()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $person = isset($_GET['person'])?$_GET['person']:1;
	    $dept = isset($_GET['dept'])?$_GET['dept']:1;
	    $date = isset($_GET['date'])?$_GET['date']:date('Y-m-d');
	    
	    $dp = \backend\modules\surgery\classes\SurgeryQuery::getDp($person, $dept, $date);
	    
	    return $this->renderAjax('_view_all', [
		'person'=>$person,
		'dept'=>$dept,
		'date'=>$date,
		'dp'=>$dp,
	    ]);
	} 
    }
    
}
