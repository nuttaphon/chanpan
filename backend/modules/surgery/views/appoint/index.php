<?php
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use appxq\sdii\widgets\ModalForm;
use yii\helpers\Url;
use yii\helpers\VarDumper;

//\yii\helpers\VarDumper::dump($events, 10, true);
?>
<h3>ตารางเวร</h3>
<hr>

<?php $form = ActiveForm::begin([
		'id' => 'jump_menu',
		'action' => ['index'],
		'method' => 'get',
		'layout' => 'inline',
		'options' => ['style'=>'display: inline-block;']	    
	    ]); 
	    $datasetArr = [
		'ptype' => $person,
		'division' => $dept,
	    ];
	    $dataset = base64_encode(\yii\helpers\Json::encode($datasetArr));
	    ?>
	    <?= Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูลนัดหมาย', Url::to(['/inputdata/step2',
			'ezf_id'=>'1464033435067331400',
			'target'=>'',
			'dataset'=>$dataset,
			'comp_id_target'=>'1437725343023632100',
			'rurl'=>base64_encode(Yii::$app->request->url),
		    ]), [
			'class'=>'btn btn-success',
			'data-toggle'=>'tooltip',
			'title'=>'เพิ่มข้อมูล',
		    ]);?>
		<?= Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มหมายเหตุ', Url::to(['/inputdata/step4',
			'ezf_id'=>'1469513470040385000',
			'target'=>'skip',
			'rurl'=>base64_encode(Yii::$app->request->url),
		    ]), [
			'class'=>'btn btn-primary',
			'data-toggle'=>'tooltip',
			'title'=>'เพิ่มข้อมูล',
		    ]);?>   
		    <?= Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มวันหยุดประจำปี', Url::to(['/inputdata/step4',
			'ezf_id'=>'1469514376009269700',
			'target'=>'skip',
			'rurl'=>base64_encode(Yii::$app->request->url),
		    ]), [
			'class'=>'btn btn-warning',
			'data-toggle'=>'tooltip',
			'title'=>'เพิ่มข้อมูล',
		    ]);?>   
	    <?= Html::dropDownList('person', $person, [1=>'Admit', 2=>'Endoscopy', 3=>'OR Minor', 4=>'ESWL'], ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()'])?>
	    <?= Html::dropDownList('dept', $dept, $dataDept, ['class'=>'form-control', 'onChange'=>'$("#jump_menu").submit()'])?>
	    
<?php ActiveForm::end(); ?>
<div class="pull-right"></div>

<?=  ModalForm::widget([
    'id' => 'modal-appoint',
    'size'=>'modal-lg modal-xl',
]);

$datasetArr = [
		'ptype' => $person,
		'division' => $dept,
		'ddate'=>$date,
	    ];
	    $dataset = base64_encode(\yii\helpers\Json::encode($datasetArr));
	    
?>
<div class="row">
    <div class="col-md-8">
	<?= \yii2fullcalendar\yii2fullcalendar::widget([
	    'options'       => [
		  'id'       => 'calendar',

	      ],
	    'events'=> $events,
	    'clientOptions' => [
		'lang' => 'th',
		'navLinks'=>true,
		'dayClick'=>new \yii\web\JsExpression("
		      function(date, jsEvent, view) {
			   
			   window.location.href = '".Url::to(['/surgery/appoint/view-link',
			       'ptype' => $person,
				'division' => $dept,
			       'rurl'=>base64_encode(Yii::$app->request->url),
				'ddate'=>'',
			       ])."'+date.format();

		      }
		  "),
		//'ajaxEvents' => Url::to(['/timetrack/default/jsoncalendar']),
		'header'=>[
		    'left'=>'prev,next today',
		    'center'=>'title',
		    'right'=>'',
		],
		  'defaultDate'=>$now_date,
		  'eventOrder'=>'id',
		  'loading' => new \yii\web\JsExpression("
		      function(bool) {
			  $('#loading').toggle(bool);
			  console.log(bool);
		      }
		  "),
		  'eventClick' => new \yii\web\JsExpression("
		      function(calEvent, jsEvent, view) {
			  var str = calEvent.id;
			  if(str.search('all_')!=-1){
			      var url = '".yii\helpers\Url::to(['/surgery/appoint/view-all', 'person'=>$person, 'dept'=>$dept, 'date'=>''])."'+calEvent.start._i;
			      modalAppoint(url);    
			  } else if(str.search('other_')!=-1){
			    
			  } else if(str.search('stop_')!=-1){
			    
			  } else {
			      var url = '".yii\helpers\Url::to(['/surgery/appoint/view', 'person'=>$person, 'dept'=>$dept, 'date'=>''])."'+calEvent.start._i;
			      modalAppoint(url);
			  }

		      }
		  "),
	      ],
	]);
	?>
    </div>
    <div class="col-md-4">
	<br>
	<h4><code>หมายเหตุ</code></h4>
	
	<?php
	if($commtEvents){
	    foreach ($commtEvents as $key => $value) {
		?>
	<code><?=$value['comment']?></code><br>
	<?php
	    }
	}
	?>
    </div>
</div>
  

<?php  $this->registerJs("
var person = '$person';
var dept = '$dept';    

        
  $('.fc-prev-button, .fc-next-button, .fc-today-button').click(function(){
    var moment = $('#calendar').fullCalendar('getDate');
    
    window.location.href = '".yii\helpers\Url::to(['/surgery/appoint/index', 'person'=>$person, 'dept'=>$dept, 'date'=>''])."'+moment.format('YYYY-MM-DD');
    
    return false;
  });
	
function modalAppoint(url) {
    $('#modal-appoint .modal-content').html('<div class=\"sdloader \"><i class=\"sdloader-icon\"></i></div>');
    $('#modal-appoint').modal('show')
    .find('.modal-content')
    .load(url);
}
");?>

