<?php
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="appointment">

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">นัดหมาย</h4>
    </div>

    <div class="modal-body ">
	
	<?php
    //$arr = date_parse($date);
    //$date = new DateTime($arr["year"].'-'.$arr["month"].'-'.$arr["day"]);
    //$date =$date->format('d/m/').($date->format('Y')+543);
	$datasetArr = [
		'ptype' => $person,
		'division' => $dept,
		'ddate'=>$date,
	    ];
	    $dataset = base64_encode(\yii\helpers\Json::encode($datasetArr));
	    
	?>
	<div class="text-right">
	<?= Html::a('<i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูลนัดหมาย', Url::to(['/inputdata/step2',
			'ezf_id'=>'1464033435067331400',
			'target'=>'',
			'dataset'=>$dataset,
			'comp_id_target'=>'1437725343023632100',
			'rurl'=>base64_encode(Yii::$app->request->url),
		    ]), [
			'class'=>'btn btn-success ',
			'data-toggle'=>'tooltip',
			'title'=>'เพิ่มข้อมูล',
		    ]);?>
	</div>
	<?php  Pjax::begin(['id'=>'appoint-grid-pjax']);?>
	<?= \common\lib\sdii\widgets\SDGridView::widget([
	    'id' => 'appoint-grid',
	    'panel' => false,
	    'dataProvider' => $dp,
	    'columns' => [
		[
		    'attribute'=>'hncode',
		    'label'=>'HN',
		    'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:100px; text-align: center;'],
		],
		[
		    'attribute'=>'name',
		    'label'=>'ชื่อ',
		    //'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:200px; '],
		],
		[
		    'attribute'=>'age',
		    'label'=>'อายุ',
		    'value'=>function ($data){ 
			
			return $data['age']>0?$data['age']:''; 
		    },
		    'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:100px; text-align: center;'],
		],
		[
		    'attribute'=>'telephone',
		    'label'=>'เบอร์โทร',
		    'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:100px; text-align: center;'],
		],
		[
		    'attribute'=>'diagnosis',
		    'label'=>'Diagnosis',
		    'contentOptions'=>['style'=>'width:300px;'],
		],
		[
		    'attribute'=>'plan',
		    'label'=>'Plan',
		    'contentOptions'=>['style'=>'width:150px; '],
		],
		[
		    'attribute'=>'tname',
		    'label'=>'อาจารย์',
		    //'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:200px; '],
		],
		[
		    'attribute'=>'bed',
		    'label'=>'เตียง',
		    'value'=>function ($data){ 
			
			return $data['bed']==1?'พิเศษ':'สามัญ'; 
		    },
		    'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:80px; text-align: center;'],
		],
		[
		    'attribute'=>'opd',
		    'label'=>'Type',
		    'value'=>function ($data){ 
			
			return $data['opd']==1?'OPD':'IPD'; 
		    },
		    'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:60px; text-align: center;'],
		],
		[
		    'attribute'=>'postpone',
		    'label'=>'เลื่อน',
		    'value'=>function ($data){ 
			
			return $data['postpone']>0?$data['postpone']:''; 
		    },
		    'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:60px; text-align: center;'],
		],
		[
		    'header'=>'',
		    'value'=>function ($data){ 
			$rowReturn = \yii\helpers\Html::a('<i class="glyphicon glyphicon-pencil"></i> ', Url::to(['/inputdata/redirect-page',
					'ezf_id'=>1464033435067331400,
					'dataid'=>$data['aid'],
					'rurl'=>base64_encode(Url::to(['/surgery/appoint/index'])),
				    ]), [
					'class'=>'btn btn-warning',
					'data-toggle'=>'tooltip',
					'title'=>'แก้ไขข้อมูล',
				    ]);
			return $rowReturn; 
		    },
		    'format'=>'raw',    
		    'headerOptions'=>['style'=>'text-align: center;'],
		    'contentOptions'=>['style'=>'width:80px; text-align: center;'],
		],
	    ],
	]); ?>
	<?php  Pjax::end();?>
    </div>

</div>