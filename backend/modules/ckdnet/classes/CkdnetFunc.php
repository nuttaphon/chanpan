<?php
namespace backend\modules\ckdnet\classes;

use Yii;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 พ.ย. 2559 7:53:01
 * @link http://www.appxq.com/
 * @example 
 */
class CkdnetFunc {
    
    public static function getDb() {
	
	if(isset(Yii::$app->session['dynamic_connection']) && !empty(Yii::$app->session['dynamic_connection'])){
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    //unset(Yii::$app->session['dynamic_connection']);
	    if(Yii::$app->session['dynamic_connection']['sitecode']!=$sitecode){
		$data_con = CkdnetQuery::getDbConfig($sitecode);
		
		$obj = [
		    'sitecode'=>$sitecode,
		    'db' => $data_con
		];
		Yii::$app->session['dynamic_connection'] = $obj;
	    }
	} else {
	    $sitecode = Yii::$app->user->identity->userProfile->sitecode;
	    $data_con = CkdnetQuery::getDbConfig($sitecode);
	    
	    $obj = [
		'sitecode'=>$sitecode,
		'db' => $data_con
	    ];
	    Yii::$app->session['dynamic_connection'] = $obj;
	}
	
	$data_config = Yii::$app->session['dynamic_connection']['db'];
	
	//\appxq\sdii\utils\VarDumper::dump(Yii::$app->session['dynamic_connection']);
	
	if(isset($data_config) && !empty($data_config)){
	    $dsn = "mysql:host={$data_config['server']};port={$data_config['port']};dbname={$data_config['db']}";

	    $db = new \yii\db\Connection([
		'dsn' => $dsn,
		'username' => $data_config['user'],
		'password' => $data_config['passwd'],
		'charset' => 'utf8',
		// 'enableSchemaCache' => true,
		// 'schemaCacheDuration' => 3600,
	    ]);
	    //$db->open();
	    
	    return $db;
	}
	
	return FALSE;
    }
    
    public static function rawSql($sql, $param=[]) {
	$db = self::getDb();
	if($db){
	    return $db->createCommand($sql, $param)->rawSql;
	}
	return FALSE;
    }
    
    public static function execute($sql, $param=[]) {
	$db = self::getDb();
	if($db){
	    return $db->createCommand($sql, $param)->execute();
	}
	return FALSE;
    }
    
    public static function queryAll($sql, $param=[]) {
	$db = self::getDb();
	//\appxq\sdii\utils\VarDumper::dump($db);
	if($db){
	    return $db->createCommand($sql, $param)->queryAll();
	}
	return FALSE;
    }
    
    public static function queryOne($sql, $param=[]) {
	$db = self::getDb();
	if($db){
	    return $db->createCommand($sql, $param)->queryOne();
	}
	return FALSE;
    }
    
    public static function queryScalar($sql, $param=[]) {
	$db = self::getDb();
	if($db){
	    return $db->createCommand($sql, $param)->queryScalar();
	}
	return FALSE;
    }
    
    public static function convertColumn($colArr) {
	$colArrReturn=[];
	foreach ($colArr as $key => $value) {
	    $colArrReturn[$key] = strtolower($value);
	}
	return $colArrReturn;
	
    }
    
    public static function getCkdDiagram($sitecode, $sdate, $edate) {
        try {
            $count = CkdnetQuery::getAll($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
	$obj[] = [
                    'id'=>0,
                    'label'=>"All ($count)",
                    'value'=>$count,
                ];
        
        try {
            $count = CkdnetQuery::getTotalPop($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
	$obj[] = [
                    'id'=>1,
                    'label'=>"1) Total population ($count)",
                    'value'=>$count,
                ];
        
        
        
        try {
            $count = CkdnetQueryRight::getCkdNonDMHT($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>13,
                    'label'=>"13) NON DM/HT ($count)",
                    'value'=>$count,
                ];
        
        
        
        try {
            $count = CkdnetQueryRight::getCkdNonDMHTReportedCKD($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>14,
                    'label'=>"14) Reported CKD ($count)",
                    'value'=>$count,
                ];
        
        try {
            $countNDM = CkdnetQueryRight::getNonDMHTCkdConfirm($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $countNDM = 0;
        }
        $obj[] = [
                    'id'=>15,
                    'label'=>"15) Confirmed CKD ($countNDM)",
                    'value'=>$countNDM,
                ];
        
       
        
        
        
        try {
            $count = CkdnetQuery::getDmHt($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>2,
                    'label'=>"2) DM/HT ($count)",
                    'value'=>$count,
                ];
        
       try {
            $count = CkdnetQueryRight::getCkdReport($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>3,
                    'label'=>"3) Reported CKD ($count)",
                    'value'=>$count,
                ];
	
         try {
            $countRCON = CkdnetQueryRight::getCkdReportConfirm($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $countRCON = 0;
        }
        $obj[] = [
                    'id'=>16,
                    'label'=>"16) Confirmed CKD ($countRCON)",
                    'value'=>$countRCON,
                ];
        
        $countLT = $countNDM+$countRCON;
        try {
            $count = CkdnetQueryRight::getCkdTotalConfirmCKD($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>17,
                    'label'=>"17) Total Confirmed CKD ($countLT)",
                    'value'=>$count,
                ];
        
        try {
            $count = CkdnetQueryRight::getCkdTargetScreening($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>4,
                    'label'=>"4) CKD Screened target ($count)",
                    'value'=>$count,
                ];
        
        
        //----
        try {
            $count = CkdnetQueryRight::getCkdScreened($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>5,
                    'label'=>"5) Screenend (Both eGFR & UA available) ($count)",
                    'value'=>$count,
                ];
        
        try {
            $count = CkdnetQueryRight::getCkdScreenedNotCkd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>6,
                    'label'=>"6) Not CKD ($count)",
                    'value'=>$count,
                ];
        
        try {
            $count = CkdnetQueryRight::getCkdScreenedWaitVer($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>7,
                    'label'=>"7) Waiting for verification ($count)",
                    'value'=>$count,
                ];
        
        try {
            $count = CkdnetQueryRight::getCkdScreenedConfirmCkd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>8,
                    'label'=>"8) Confirmed CKD ($count)",
                    'value'=>$count,
                ];
        
        try {
            $count = CkdnetQueryRight::getCkdNotScreened($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>9,
                    'label'=>"9) Not Screenend (Some/none eGFR, UA available) ($count)",
                    'value'=>$count,
                ];
        
        
        try {
            $count = CkdnetQueryRight::getCkdNotScreenedNotCkd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>10,
                    'label'=>"10) Insufficient info for diagnosis of CKD ($count)",
                    'value'=>$count,
                ];
        
        try {
            $count = CkdnetQueryRight::getCkdNotScreenedWaitVer($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>11,
                    'label'=>"11) Waiting for verification ($count)",
                    'value'=>$count,
                ];
        
        try {
            $count = CkdnetQueryRight::getCkdNotScreenedConfirmCkd($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        $obj[] = [
                    'id'=>12,
                    'label'=>"12) Confirmed CKD ($count)",
                    'value'=>$count,
                ];
        
        
        
        $countRT = $countLT+$countS+$countNS;
        try {
            $count = CkdnetQueryRight::getCkdTotalAllConfirmCKD($sitecode, $sdate, $edate);
        } catch (\yii\db\Exception $e) {
            $count = 0;
        }
        
        $obj[] = [
                    'id'=>18,
                    'label'=>"18) Total Confirmed CKD ($count)",
                    'value'=>$count,
                ];
        
	return $obj;
    }
    
}
