<?php
namespace backend\modules\ckdnet\classes;

use Yii;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 พ.ย. 2559 7:53:01
 * @link http://www.appxq.com/
 * @example 
 */
class CkdnetQuery {
    
    public static function getCkdPatientCentralField($hospcode, $ptlink, $field) {
	$sql = "SELECT ckd.$field FROM ckd_patient_central ckd WHERE ckd.ptlink = :ptlink AND ckd.hospcode = :hospcode";
	
	return Yii::$app->db->createCommand($sql, [':hospcode'=>$hospcode, ':ptlink'=>$ptlink])->queryScalar();
    }
    
    public static function getDbConfig($sitecode) {
	$sql = "SELECT db_config_province.province, 
			db_config_province.zonecode, 
			db_config_province.`server`, 
			db_config_province.`user`, 
			db_config_province.passwd, 
			db_config_province.`port`, 
			db_config_province.db, 
			db_config_province.webservice
		FROM db_config_province INNER JOIN all_hospital_thai ON db_config_province.province = all_hospital_thai.provincecode
		WHERE all_hospital_thai.hcode = :sitecode
		";
	return Yii::$app->dbbot_ip8->createCommand($sql, [':sitecode'=>$sitecode])->queryOne();
    }
    
    public static function getColumn($table) {
	$sql = "SELECT COLUMN_NAME AS `column` FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = :tbname AND table_schema = :tbschema";
	return CkdnetFunc::queryAll($sql, [':tbname' => $table, ':tbschema' => 'tdc_data']);
    }
    
    public static function genSelect() {
        $convert = isset(Yii::$app->session['convert_ckd']) ? Yii::$app->session['convert_ckd'] : 0;
        $key = isset(Yii::$app->session['key_ckd']) ?Yii::$app->session['key_ckd'] : '';

        $selectKey = "`f_person`.HOSPCODE,
                `f_person`.PID,
                `f_person`.sitecode,
                `f_person`.ptlink,
                `f_person`.CID,
                `f_person`.`Pname`,
                `f_person`.`Name`,
                `f_person`.`Lname`";

        if($key!=''){
            $selectKey = "`f_person`.hospcode,
                `f_person`.pid,
                `f_person`.sitecode,
                `f_person`.ptlink,
                decode(unhex(f_person.CID),sha2('$key',256)) AS CID,
                decode(unhex(f_person.Pname),sha2('$key',256)) AS Pname,
                decode(unhex(f_person.`Name`),sha2('$key',256)) AS Name,
                decode(unhex(f_person.Lname),sha2('$key',256)) AS Lname";

            if ($convert==1) {
                $selectKey = "`f_person`.hospcode,
                `f_person`.pid,
                `f_person`.sitecode,
                `f_person`.ptlink,
                convert(decode(unhex(f_person.CID),sha2('$key',256)) using tis620) AS CID,
                convert(decode(unhex(f_person.Pname),sha2('$key',256)) using tis620) AS Pname,
                convert(decode(unhex(f_person.`Name`),sha2('$key',256)) using tis620) AS Name,
                convert(decode(unhex(f_person.Lname),sha2('$key',256)) using tis620) AS Lname";

            }
        }
        
        return $selectKey;
    }
    public static function getAll($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `hospcode` = :sitecode
                ";
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`hospcode` = :sitecode
                    AND `f_person`.`sitecode` = :sitecode
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'keyID'=>'ptlink',
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getTotalPop($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` = 0 AND 
                    `hospcode` = :sitecode
                ";
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                    AND `patient_profile_hospital`.`hospcode` = `f_person`.`sitecode`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode
                    AND `f_person`.`sitecode` = :sitecode
                ";
            
            return [
                'sql' =>$sqlDp,
                'keyID'=> function ($model) {
                    return ['sitecode'=>$model['sitecode'], 'PID'=>$model['PID'], 'HOSPCODE'=>$model['HOSPCODE']];
                },
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }

    public static function getDmHt($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND (dm >= 1 OR ht >= 1) AND
                    `hospcode` = :sitecode
                ";
        
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND (dm >= 1 OR ht >= 1) AND
                    `patient_profile_hospital`.`hospcode` = :sitecode
                    AND `f_person`.`sitecode` = :sitecode
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getNotDmHt($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    ((dm = 0 OR dm is null) AND (ht = 0 OR dm is null))
                ";
        
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    AND `f_person`.`sitecode` = :sitecode
                    ((dm = 0 OR dm is null) AND (ht = 0 OR dm is null))
                ";
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getRckd($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    ((dm = 0 OR dm is null) AND (ht = 0 OR dm is null))  AND (reported_ckd = 1)
                ";
        
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    AND `f_person`.`sitecode` = :sitecode
                    ((dm = 0 OR dm is null) AND (ht = 0 OR dm is null))  AND (reported_ckd = 1)
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getNRckd($sitecode, $sdate, $edate, $dp=false) {
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` = 0 AND 
                    `hospcode` = :sitecode AND
                    (dm = 1 OR ht = 1) AND (reported_ckd = 1 OR reported_ckd is null)
                ";
        
        if($dp){
            $selectKey = self::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` = 0 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    AND `f_person`.`sitecode` = :sitecode
                    (dm = 1 OR ht = 1) AND (reported_ckd = 1 OR reported_ckd is null)
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        } 
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    
}
