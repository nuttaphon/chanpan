<?php
namespace backend\modules\ckdnet\classes;

use Yii;

/**
 * newPHPClass class file UTF-8
 * @author SDII <iencoded@gmail.com>
 * @copyright Copyright &copy; 2015 AppXQ
 * @license http://www.appxq.com/license/
 * @version 1.0.0 Date: 4 พ.ย. 2559 7:53:01
 * @link http://www.appxq.com/
 * @example 
 */
class CkdnetQueryRight {
    
    private static $sqlDMHT="IFNULL(patient_profile_hospital.dm,0) = 1 OR IFNULL(patient_profile_hospital.dm,0) = 2 OR IFNULL(patient_profile_hospital.ht,0) = 1 OR IFNULL(patient_profile_hospital.ht,0) = 2";
    #private static $sqlCkdTarget="target = 1";
    #private static $sqlCkdDMHTReported = "(patient_profile_hospital.dm = 1 or patient_profile_hospital.dm = 2 or patient_profile_hospital.ht = 1) AND patient_profile_hospital.reported_ckd=1";
    private static $sqlCkdReportedCKD  =  "IFNULL(patient_profile_hospital.reported_ckd,0)=1";
    #private static $sqlCkcDMHTTarget = "(patient_profile_hospital.dm=1 or patient_profile_hospital.dm=2 or patient_profile_hospital.ht) AND (patient_profile_hospital.reported_ckd=0 or patient_profile_hospital.reported_ckd is null)";
    #private static $sqlCkdTarget="patient_profile_hospital.target=1 and (patient_profile_hospital.death_date>='2015-10-01' or patient_profile_hospital.death=0)";
    private static $sqlCkdTarget="IFNULL(patient_profile_hospital.reported_ckd,0) = 0";
    #private static $sqlCkdTargetNon="patient_profile_hospital.target = 0 OR patient_profile_hospital.target is null";
    private static $sqlCkdScreen="IFNULL(patient_profile_hospital.screened,0) = 1";
    private static $sqlCkdScreenNon="IFNULL(patient_profile_hospital.screened,0) = 0 or patient_profile_hospital.screened is null";
    //private static $sqlConfirmCkdNo="IFNULL(patient_profile_hospital.cf_lab,0) = 0 OR IFNULL(patient_profile_hospital.cf_doctor,0) = 0 OR IFNULL(patient_profile_hospital.cf_icd,0) = 0 OR IFNULL(patient_profile_hospital.ckd_final,0)=0";
    //private static $sqlConfirmCkd="patient_profile_hospital.cf_lab = 1 OR patient_profile_hospital.cf_doctor = 1 OR patient_profile_hospital.cf_icd = 1 OR patient_profile_hospital.ckd_final=1";
    private static $sqlConfirmCkdNo="patient_profile_hospital.ckd_final is null";
    private static $sqlConfirmCkd="patient_profile_hospital.ckd_final=1";

    //private static $sqlConfirmCkdWait="patient_profile_hospital.cf_lab is null AND patient_profile_hospital.cf_doctor is null AND patient_profile_hospital.cf_icd is null AND patient_profile_hospital.ckd_final is null "; # ยังไม่สรุปผลใดๆ ทั้งสิ้น
    private static $sqlConfirmCkdWait="(patient_profile_hospital.ckd_final is null or patient_profile_hospital.ckd_final=0) AND ifnull(cf_lab,0)=1"; # ยังไม่สรุปผลใดๆ ทั้งสิ้น
    # insufficient ยังไม่ชัว
    
    
    # Begin Left
    public static function getNonDMHTCkdConfirm($sitecode, $sdate, $edate, $dp=false) {
        # call CkdnetQueryRight::getNonDMHTCkdConfirm
        # ckd: Non DM/HT -> Reported CKD => Confirmed CKD -> (15)
        # - Non DM/HT -
        # - Reported CKD -
        # - Confirmed CKD -> (15)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sqlConfirmCkd = self::$sqlConfirmCkd;
        $sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    Not (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.")
                    (".$sqlConfirmCkd.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    Not (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.")
                    (".$sqlConfirmCkd.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    
    # End Left
    
    # call CkdnetQueryRight::getCkdScreenedConfirmCkd
    #==============================#
    # Begin Added Rigth
    
    public static function getCkdNonDMHT($sitecode, $sdate, $edate, $dp=false) {
        # ckd: Non DM/HT -> (13)
        # - Non DM/HT - (13)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    Not (".$sqlDMHT.") 
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    Not (".$sqlDMHT.") 
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdNonDMHTReportedCKD($sitecode, $sdate, $edate, $dp=false) {
        # ckd: Non DM/HT => Reported CKD -> (14)
        # - Non DM/HT -
        # - Reported CKD -
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    Not (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    Not (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdReport($sitecode, $sdate, $edate, $dp=false) {
        # ckd: DM/HT => Reported CKD -> (3)
        # - DM/HT -
        # - Reported CKD - (3)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdReportConfirm($sitecode, $sdate, $edate, $dp=false) {
        # ckd: DM/HT -> Reported CKD  => Confirmed CKD -> (16)
        # - DM/HT -
        # - Reported CKD  -
        # - Confirmed CKD - (16)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sqlConfirmCkd = self::$sqlConfirmCkd;
        $sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.") AND
                    (".$sqlConfirmCkd.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdReportedCKD.") AND
                    (".$sqlConfirmCkd.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdTotalConfirmCKD($sitecode, $sdate, $edate, $dp=false) {
        # ckd: Non DM/HT + DM/HT -> Reported CKD  => Confirmed CKD -> (17)
        # - Non DM/HT + DM/HT -
        # - Reported CKD  -
        # - Confirmed CKD - 
        # - Total Confirmed CKD -> (17)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sqlConfirmCkd = self::$sqlConfirmCkd;
        $sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlCkdReportedCKD.") AND
                    (".$sqlConfirmCkd.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlCkdReportedCKD.") AND
                    (".$sqlConfirmCkd.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    public static function getCkdTotalAllConfirmCKD($sitecode, $sdate, $edate, $dp=false) {
        # ckd: Non DM/HT + DM/HT -> Reported CKD  + Target  => Confirmed CKD -> (18)
        # - Non DM/HT + DM/HT -
        # - Reported CKD + Target  -
        # - Confirmed CKD - 
        # - Total Confirmed CKD -> (18)
	$sqlConfirmCkd = self::$sqlConfirmCkd;
        $sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlConfirmCkd.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlConfirmCkd.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    # Target for screening
    public static function getCkdTargetScreening($sitecode, $sdate, $edate, $dp=false) {
        # ckd: DM/HT -> CKD Screening Target -> (4)
        # - DM/HT -
        # - CKD Screening Target - (4)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    NOT(".$sqlCkdReportedCKD.") 
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    NOT(".$sqlCkdReportedCKD.") 
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    
    # Screening (screened)
    public static function getCkdScreened($sitecode, $sdate, $edate, $dp=false) {
        # ckd: DM/HT -> CKD Screening Target  => Screened -> (5)
        # - DM/HT  -
        # - CKD Screening Target -
        # - Screened -  (5)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlCkdScreen = self::$sqlCkdScreen;
        #$sqlCkdReportedCKD = self::$sqlCkdReportedCKD;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreen.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreen.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdScreenedNotCkd($sitecode, $sdate, $edate, $dp=false) {
        # ckd: DM/HT -> CKD Screening Target  -> Screened => Not CKD -> (6)
        # - DM/HT  -
        # - CKD Screening Target -
        # - Screened -  
        # - Not CKD - (6)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdScreen = self::$sqlCkdScreen;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlConfirmCkdNo = self::$sqlConfirmCkdNo;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreen.") AND
                    (".$sqlConfirmCkdNo.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreen.") AND
                    (".$sqlConfirmCkdNo.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdScreenedWaitVer($sitecode, $sdate, $edate, $dp=false) {
        # call CkdnetQueryRight::getCkdScreenedWaitVer
        # ckd: DM/HT -> CKD Screening Target  -> Screened => Waiting for verification -> (7)
        # - DM/HT  -
        # - CKD Screening Target -
        # - Screened - 
        # - Waiting for verification - (7)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdScreen = self::$sqlCkdScreen;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlConfirmCkdWait = self::$sqlConfirmCkdWait;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreen.") AND
                    (".$sqlConfirmCkdWait.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreen.") AND
                    (".$sqlConfirmCkdWait.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdScreenedConfirmCkd($sitecode, $sdate, $edate, $dp=false) {
        # ckd: DM/HT -> CKD Screening Target  -> Screened => Confirmed CKD -> (8)
        # - DM/HT  -
        # - CKD Screening Target -
        # - Screened - 
        # - Confirmed CKD - (8)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdScreen = self::$sqlCkdScreen;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlConfirmCkd = self::$sqlConfirmCkd;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreen.") AND
                    (".$sqlConfirmCkd.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreen.") AND
                    (".$sqlConfirmCkd.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    #   -> Target not screen
    public static function getCkdNotScreened($sitecode, $sdate, $edate, $dp=false) {
        # call CkdnetQueryRight::getCkdNotScreened
        # ckd: DM/HT -> CKD Not Screened -> (9)
        # - CKD Not Screened - (9)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlCkdScreenNon = self::$sqlCkdScreenNon;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreenNon.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreenNon.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdNotScreenedNotCkd($sitecode, $sdate, $edate, $dp=false) {
        # call CkdnetQueryRight::getCkdNotScreened
        # ckd: DM/HT -> CKD Not Screened => Insufficient info for diagosis of CKD -> (10)
        # - Insufficient info for diagosis of CKD - (10)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlCkdScreenNon = self::$sqlCkdScreenNon;
        $sqlConfirmCkdNo = self::$sqlConfirmCkdNo;
        $sqlConfirmCkd = self::$sqlConfirmCkd;
        $sqlConfirmCkdWait = self::$sqlConfirmCkdWait;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreenNon.") AND
                    Not (".$sqlConfirmCkdWait.") AND
                    (".$sqlConfirmCkdNo.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreenNon.") AND
                    Not (".$sqlConfirmCkdWait.") AND
                    (".$sqlConfirmCkdNo.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdNotScreenedWaitVer($sitecode, $sdate, $edate, $dp=false) {
        # call CkdnetQueryRight::getCkdNotScreenedWaitVer
        # ckd: DM/HT -> CKD Not Screened => Waiting for verification -> (11)
        # -  DM/HT -
        # - CKD Not Screened -
        # - Waiting for verification - (11)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlCkdScreenNon = self::$sqlCkdScreenNon;
        $sqlConfirmCkdWait = self::$sqlConfirmCkdWait;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreenNon.") AND
                    (".$sqlConfirmCkdWait.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreenNon.") AND
                    (".$sqlConfirmCkdWait.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    public static function getCkdNotScreenedConfirmCkd($sitecode, $sdate, $edate, $dp=false) {
        # call CkdnetQueryRight::getCkdNotScreenedConfirmCkd
        # ckd: DM/HT -> CKD Not Screened => Waiting for verification -> (12)
        # -  DM/HT -
        # - CKD Not Screened -
        # - Confirmed CKD - (12)
        $sqlDMHT = self::$sqlDMHT;
        $sqlCkdTarget = self::$sqlCkdTarget;
        $sqlCkdScreenNon = self::$sqlCkdScreenNon;
        $sqlConfirmCkdNo = self::$sqlConfirmCkdNo;
        $sqlConfirmCkd = self::$sqlConfirmCkd;
	$sql = "SELECT
                    COUNT(*)
                FROM
                    `patient_profile_hospital`
                WHERE
                    `death` <> 1 AND 
                    `hospcode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreenNon.") AND
                    (".$sqlConfirmCkd.")
                ";
        
        if($dp){
            $selectKey = CkdnetQuery::genSelect();
            
            $sqlDp = "SELECT
                    $selectKey
                FROM
                    `patient_profile_hospital`
                    INNER JOIN `f_person`
                    ON `patient_profile_hospital`.`hospcode` = `f_person`.`HOSPCODE`
                    AND `patient_profile_hospital`.`pid` = `f_person`.`PID`
                WHERE
                    `patient_profile_hospital`.`death` <> 1 AND 
                    `patient_profile_hospital`.`hospcode` = :sitecode AND
                    `f_person`.`sitecode` = :sitecode AND
                    (".$sqlDMHT.") AND
                    (".$sqlCkdTarget.") AND
                    (".$sqlCkdScreenNon.") AND
                    (".$sqlConfirmCkd.")
                ";
            
            return [
                'sql' =>$sqlDp,
                'param'=>[':sitecode'=>$sitecode],
                
                'total'=>CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]),
            ];
        }
        
	return CkdnetFunc::queryScalar($sql, [':sitecode'=>$sitecode]);//':sdate' => $sdate, ':edate' => $edate, 
    }
    # End Added
    #==============================#
    
}
