<?php

namespace backend\modules\ckdnet\controllers;
use Yii;
use yii\helpers\Json;
class EmrController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $request = Yii::$app->request;
        
        $hospcode=$request->get('hospcode');
        $pid=$request->get('pid');
        
        $active['emr']=true;
        $items = [
            [
                'label'=>'<i class="fa fa-home"></i> หน้าแรก',
                'active'=>$active['home'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/default/redirect'])]                           
            ],
            [
                'label'=>'<i class="fa fa-check-square-o"></i> กำกับงาน',
                'active'=>$active['workspace'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/workspace/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-male"></i> EMR',
                'active'=>$active['emr'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/emr/redirect'])]
            ],
            [
                'label'=>'<i class="fa fa-area-chart"></i> รายงาน',
                'active'=>$active['report'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/report/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-map-marker"></i> แผนที่',
                'active'=>$active['map'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/map/redirect'])]                
            ],
            [
                'label'=>'<i class="fa fa-table"></i> Matrix',
                'active'=>$active['matrix'],
                'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/matrix/redirect'])]
            ],
        ];        
        $tab = Yii::$app->request->get('tab');
        return $this->render('index',
            [
                'items' =>  $items,
                'tab'   =>  $tab,
                'hospcode' => $hospcode,
                'pid' => $pid,
            ]);
    }
    public function actionRedirect() {
        return $this->redirect('/ckdnet/emr',302);
    }
    public function actionPhr() {
        $request = Yii::$app->request;
        $hospcode=$request->get('hospcode');
        $pid=$request->get('pid');
        
        return Json::encode($this->renderPartial('phr',
                [
                'hospcode' => $hospcode,
                'pid' => $pid,
                ]));
    }
    public function actionCkd() {
        $request = Yii::$app->request;
        $hospcode=$request->get('hospcode');
        $pid=$request->get('pid');
        
        return Json::encode($this->renderPartial('ckd',
                [
                'hospcode' => $hospcode,
                'pid' => $pid,
                ]));                
                
    }
}
