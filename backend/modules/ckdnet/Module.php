<?php

namespace backend\modules\ckdnet;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ckdnet\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
