<?php
use yii\bootstrap\Modal;
use kartik\tabs\TabsX;
use kartik\form\ActiveForm;
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = Yii::t('backend', Html::img('../img/buttons_cloud/ckd.png',['style' => ['width' => '40px', 'height' => '40px']]).' CHRONIC KIDNEY DISEASE PREVENTION IN 
THE NORTHEAST OF THAILAND');
$this->params['breadcrumbs'][] = ['label'=>'Modules','url'=>  yii\helpers\Url::to('/tccbots/my-module')];
$this->params['breadcrumbs'][] = Yii::t('backend', 'CKDNET');
$activetab2 = $tab;
if ($activetab2 != "") {
    $active[$activetab2]=true;
}else{
    $active['emr']=true;
}

$items2 = [
    [
        'label'=>'<i class="fa fa-user"></i> CKD EMR',
        'content'=>$this->renderAjax('ckd'),
        'active'=>$active['emr'],
        'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/emr/ckd?hospcode='.$hospcode.'&pid='.$pid])]
    ],
//    [
//        'label'=>'<i class="fa fa-user-plus"></i> PHR',
//        'content'=>$this->renderAjax('phr'),
//        'active'=>$active['phr'],
//        'linkOptions'=>['data-url'=>\yii\helpers\Url::to(['/ckdnet/emr/phr'])]
//    ],
];

?>
<div class="ckdnet-default-index">
    
<?php
echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
?>
<div class="form-inline pull-right">    
    <div class="input-group margin-bottom-sm">
      <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
      <input class="form-control" type="text" name="cid" placeholder="เลขที่บัตรประชาชน">
    </div> 
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
</div>
<?php    
echo TabsX::widget([
    'items'=>$items2,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
?>

</div>