<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use appxq\sdii\helpers\SDNoty;
use appxq\sdii\helpers\SDHtml;

/* @var $this yii\web\View */
/* @var $model backend\modules\ckdnet\models\FPerson */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="fperson-form">

    <?php $form = ActiveForm::begin([
	'id'=>$model->formName(),
    ]); ?>

    <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="itemModalLabel">Fperson</h4>
    </div>

    <div class="modal-body">
	<?= $form->field($model, 'sitecode')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'HOSPCODE')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'CID')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'PID')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'HID')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'PreName')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Pname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Lname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'HN')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'sex')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Birth')->textInput() ?>

	<?= $form->field($model, 'Mstatus')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Occupation_Old')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Occupation_New')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Race')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Nation')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Religion')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Education')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Fstatus')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Father')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Mother')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Couple')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Vstatus')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'MoveIn')->textInput() ?>

	<?= $form->field($model, 'Discharge')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Ddischarge')->textInput() ?>

	<?= $form->field($model, 'ABOGROUP')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'RHGROUP')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'Labor')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'PassPort')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'TypeArea')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'D_Update')->textInput() ?>

	<?= $form->field($model, 'ptlink')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="modal-footer">
	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	<?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php  $this->registerJs("
$('form#{$model->formName()}').on('beforeSubmit', function(e) {
    var \$form = $(this);
    $.post(
	\$form.attr('action'), //serialize Yii2 form
	\$form.serialize()
    ).done(function(result) {
	if(result.status == 'success') {
	    ". SDNoty::show('result.message', 'result.status') ."
	    if(result.action == 'create') {
		$(\$form).trigger('reset');
		$.pjax.reload({container:'#fperson-grid-pjax'});
	    } else if(result.action == 'update') {
		$(document).find('#modal-fperson').modal('hide');
		$.pjax.reload({container:'#fperson-grid-pjax'});
	    }
	} else {
	    ". SDNoty::show('result.message', 'result.status') ."
	} 
    }).fail(function() {
	". SDNoty::show("'" . SDHtml::getMsgError() . "Server Error'", '"error"') ."
	console.log('server error');
    });
    return false;
});

");?>