<?php
use yii\helpers\Html;

$key = Yii::$app->keyStorage->get('app_url');
$suburl = isset($key)?$key.'.':'';
?>

เรียน คุณ <?php echo Html::encode($user->firstname.' '.$user->lastname) ?><br>

คุณยังไม่ทำการยืนยันตัวตน กรุณาอัพโหลดเอกสารยืนยันตัวตน <?php echo Yii::$app->keyStorage->get('link_profile');?><br>
หากท่านไม่ยืนยันตัวตนภายใน 30 วัน คุณจะไม่สามารถเข้าใช้งานระบบได้
<br>

<br>
<br>
<?php echo Yii::$app->keyStorage->get('email_signature');?>
