<?php
use yii\helpers\Html;

$old = backend\modules\ezforms\components\EzformFunc::getSite($data['sitecode_old']);
$new = backend\modules\ezforms\components\EzformFunc::getSite($data['sitecode_new']);
?>

คุณ<?php echo Html::encode($user->firstname.' '.$user->lastname) ?> <br>ขอย้ายหน่วยงานจาก <?=(isset($old['name']))?$old['name']:'';?> ไปหน่วยงาน <?=(isset($new['name']))?$new['name']:'';?><br>
<br>

สถานะ : <?php echo $status;?> => <?=$data['feedback']?>
<br><br>

<?php echo Yii::$app->keyStorage->get('email_signature');?><br>