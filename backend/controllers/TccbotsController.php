<?php
namespace backend\controllers;

use Yii;
use yii\data\SqlDataProvider;

/**
 * Site controller
 */
class TccbotsController extends \yii\web\Controller
{

    public function actionSetup(){
        return $this->render('setup',[
        ]);
    }
    public function actionSetupTccbot(){
        return $this->render('setup-tccbot',[
        ]);
    }

    public function actionCreateModule(){
        return $this->render('create-module');
    }

    public function actionMyModule(){
        return $this->render('my-module');
    }

    public function actionMyModuleApp(){
        return $this->render('my-module-app');
    }

    public function actionMyModuleCancer(){
        return $this->render('my-module-cancer');
    }

    public function actionModules(){
        return $this->render('modules');
    }

}
