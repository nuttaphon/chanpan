<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserForm;
use backend\models\search\UserSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\UserProfile;
use backend\modules\ezforms\components\EzformQuery;
use yii\validators\EmailValidator;
use backend\models\UserAllowSearch;
use backend\models\UserAllow;
use yii\data\ActiveDataProvider;
use appxq\sdii\helpers\SDHtml;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserlistController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
	$sitecode;
	$pcode;
	
	if(isset($_GET['pcode']) && !empty($_GET['pcode'])){
	    $pcode = $_GET['pcode'];
	    Yii::$app->session['pcode'] = $pcode;
	    if(isset($_GET['site'])){
		$sitecode = $_GET['site'];
		if(isset(Yii::$app->session['site']) && Yii::$app->session['site']!=$sitecode){
		    $sql = "SELECT all_hospital_thai.hcode, 
				all_hospital_thai.`name`, 
				all_hospital_thai.tambon, 
				all_hospital_thai.amphur, 
				all_hospital_thai.province, 
				all_hospital_thai.code5,
				all_hospital_thai.amphurcode, 
				all_hospital_thai.provincecode, 
				all_hospital_thai.tamboncode
			FROM all_hospital_thai
			WHERE all_hospital_thai.hcode = :code";

		    $data = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();

		    $pcode = $data['provincecode'];
		}
		
		Yii::$app->session['site'] = $sitecode;
	    } else {
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		Yii::$app->session['site'] = $sitecode;
	    }
	} else {
	    if(isset($_GET['site'])){
		$sitecode = $_GET['site'];
		Yii::$app->session['site'] = $sitecode;
	    } else {
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		Yii::$app->session['site'] = $sitecode;
	    }
	    $sql = "SELECT all_hospital_thai.hcode, 
			all_hospital_thai.`name`, 
			all_hospital_thai.tambon, 
			all_hospital_thai.amphur, 
			all_hospital_thai.province, 
			all_hospital_thai.code5,
			all_hospital_thai.amphurcode, 
			all_hospital_thai.provincecode, 
			all_hospital_thai.tamboncode
		FROM all_hospital_thai
		WHERE all_hospital_thai.hcode = :code";

	    $data = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();

	    $pcode = $data['provincecode'];
	    Yii::$app->session['pcode'] = $pcode;
	}
	
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchSite(Yii::$app->request->queryParams, $sitecode,0);
	
	$sql = "select distinct h.hcode,CONCAT(h.`name`, ' ต.', h.`tambon`,' อ.', h.`amphur`,' จ.', h.`province`) AS name from user_profile as p left join all_hospital_thai as h on p.sitecode=h.hcode"
                . " where h.hcode=:code";
	
	$sitelist = Yii::$app->db->createCommand($sql, [':code'=>$sitecode])->queryOne();
	
        $userallow = UserAllow::find()->where(['sitecode'=>$sitecode]);
       
        $userallowdata = new ActiveDataProvider([
            'query' => $userallow,
        ]);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'sitelist' => $sitelist,
	    'sitecode' => $sitecode,
	    'pcode' => $pcode,
            'userallowdata' => $userallowdata,
        ]);
    }
    
    public function actionVolunteer()
    {
	$sitecode;
	$pcode;
	
	if(isset($_GET['pcode']) && !empty($_GET['pcode'])){
	    $pcode = $_GET['pcode'];
	    Yii::$app->session['pcode'] = $pcode;
	    if(isset($_GET['site'])){
		$sitecode = $_GET['site'];
		if(isset(Yii::$app->session['site']) && Yii::$app->session['site']!=$sitecode){
		    $sql = "SELECT all_hospital_thai.hcode, 
				all_hospital_thai.`name`, 
				all_hospital_thai.tambon, 
				all_hospital_thai.amphur, 
				all_hospital_thai.province, 
				all_hospital_thai.code5,
				all_hospital_thai.amphurcode, 
				all_hospital_thai.provincecode, 
				all_hospital_thai.tamboncode
			FROM all_hospital_thai
			WHERE all_hospital_thai.hcode = :code";

		    $data = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();

		    $pcode = $data['provincecode'];
		}
		
		Yii::$app->session['site'] = $sitecode;
	    } else {
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		Yii::$app->session['site'] = $sitecode;
	    }
	} else {
	    if(isset($_GET['site'])){
		$sitecode = $_GET['site'];
		Yii::$app->session['site'] = $sitecode;
	    } else {
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		Yii::$app->session['site'] = $sitecode;
	    }
	    $sql = "SELECT all_hospital_thai.hcode, 
			all_hospital_thai.`name`, 
			all_hospital_thai.tambon, 
			all_hospital_thai.amphur, 
			all_hospital_thai.province, 
			all_hospital_thai.code5,
			all_hospital_thai.amphurcode, 
			all_hospital_thai.provincecode, 
			all_hospital_thai.tamboncode
		FROM all_hospital_thai
		WHERE all_hospital_thai.hcode = :code";

	    $data = Yii::$app->db->createCommand($sql, [':code' => $sitecode])->queryOne();

	    $pcode = $data['provincecode'];
	    Yii::$app->session['pcode'] = $pcode;
	}
	
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchSite(Yii::$app->request->queryParams, $sitecode,0);
	
	$sql = "select distinct h.hcode,CONCAT(h.`name`, ' ต.', h.`tambon`,' อ.', h.`amphur`,' จ.', h.`province`) AS name from user_profile as p left join all_hospital_thai as h on p.sitecode=h.hcode"
                . " where h.hcode=:code";
	
	$sitelist = Yii::$app->db->createCommand($sql, [':code'=>$sitecode])->queryOne();
	
        $userallow = UserAllow::find()->where('sitecode=:sitecode ', [':sitecode'=>$sitecode]);
       
        $userallowdata = new ActiveDataProvider([
            'query' => $userallow,
        ]);
        
	$sModel = new \frontend\modules\volunteer\models\VolunteerJobSearch();
        $dp = $sModel->search(Yii::$app->request->queryParams);
	
        return $this->render('volunteer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'sitelist' => $sitelist,
	    'sitecode' => $sitecode,
	    'pcode' => $pcode,
            'userallowdata' => $userallowdata,
	    'sModel'=>$sModel,
	    'dp'=>$dp,
        ]);
    }
    
    public function actionUpdateSitecode()//update-sitecode
    {
	if(!Yii::$app->user->can('administrator')){
	    echo 'Drump user by administrator only';
	    exit();
	}
	$tt = 0;
	$tf = 0;
	$model = UserProfile::find()->where('length(`sitecode`)<5')->all();
	if($model){
	    
	    foreach ($model as $key => $value) {
		$len = strlen($value['sitecode']);
		if($len<5){
		    $sitecode = $value['sitecode'];
		    $count = 5-$len;
		    for($i=0;$i<$count;$i++){
			$sitecode = '0'.$sitecode;
		    }
		    
		    $r=Yii::$app->db->createCommand()->update('user_profile', [
			'sitecode'=>$sitecode,
			'department'=>$sitecode,
		    ], 'user_id=:user_id', [':user_id'=>$value['user_id']])->execute();
		    if($r){
			echo 'แก้ไข '.$value['sitecode']. ' เป็น '. $sitecode.'<br>';
			$tt++;
		    } else {
			echo 'แก้ไข '.$value['sitecode']. ' ไม่ได้'.'<br>';
			$tf++;
		    }
		}
	    }
	}
	
	echo "success[ $tt ] unsuccessful[ $tf ] total " . count($model);
    }
    
//    public function actionDrumpTest()
//    {
//	if(!Yii::$app->user->can('administrator')){
//	    echo 'Drump user by administrator only';
//	    exit();
//	}
//	$limit = isset($_GET['row'])?$_GET['row']:0;
//	$end = isset($_GET['end'])?$_GET['end']:0;
//	
//	if($end>0){
//	    if($limit>$end){
//		echo 'Drump end time';
//		exit();
//	    }
//	}
//	
//	ini_set('max_execution_time', 0);
//	set_time_limit(0);
//	ini_set('memory_limit','512M'); 
//	
//	$sql = "SELECT patient.ptid, patient.ptid_key
//	    FROM patient
//	    WHERE patient.vconsent=1
//	    limit $limit, 100
//	    ";
//	
//	$data = Yii::$app->dbcascap->createCommand($sql)->queryAll();
//	if($data){
//	    foreach ($data as $key => $value) {
//		$sql = "SELECT id, ptid, icf_upload1, icf_upload2, icf_upload3
//		FROM tb_data_1
//		WHERE id=ptid AND id=:ptid
//		";
//		$tb_data_1 = Yii::$app->dbcascaputf8->createCommand($sql, [':ptid'=>$value['ptid']])->queryOne();
//	    }
//	}
//	echo "success <br>";
//	echo 'http://backend.yii2-starter-kit.dev'.\yii\helpers\Url::to(['/userlist/drump-test', 'row'=>$limit+100]);
//	echo '<meta http-equiv="refresh" content="2; URL='.\yii\helpers\Url::to(['/userlist/drump-test', 'end'=>$end,'row'=>$limit+100]).'">';
//	echo \yii\helpers\Html::a('NEXT '.$limit.'-'.($limit+100).' All 124,861', \yii\helpers\Url::to(['/userlist/drump-test', 'row'=>$limit+100]));
//	//sleep(3);
//	//return $this->redirect(['/userlist/drump-test', 'end'=>$end, 'row'=>$limit+100]);
//	
//    }
//    
//    public function actionDrumpFile2()
//    {
//	if(!Yii::$app->user->can('administrator')){
//	    echo 'Drump user by administrator only';
//	    exit();
//	}
//	$limit = isset($_GET['row'])?$_GET['row']:0;
//	$end = isset($_GET['end'])?$_GET['end']:0;
//	
//	if($end>0){
//	    if($limit>$end){
//		echo 'Drump end time';
//		exit();
//	    }
//	}
//	
//	ini_set('max_execution_time', 0);
//	set_time_limit(0);
//	ini_set('memory_limit','512M'); 
//	
//	$sql = "SELECT 	file_upload.ptid, 
//			file_upload.c2enc, 
//			file_upload.filename, 
//			file_upload.filenamesys, 
//			file_upload.fullpath, 
//			file_upload.filetype, 
//			file_upload.filesize, 
//			file_upload.fieldname, 
//			file_upload.tablename, 
//			file_upload.fileactive, 
//			file_upload.id
//	    FROM file_upload
//	    WHERE file_upload.fieldname = 'cca02p1'
//	    limit $limit, 100
//	    ";
//	
//	$data = Yii::$app->dbcascap->createCommand($sql)->queryAll();
//	
//	if($data){
//	    $i=0;
//	    $file=0;
//	    foreach ($data as $key => $value) {
//		
//		$sql = "SELECT id, ptid, f2p1_fileupload
//		FROM tb_data_4
//		WHERE id=:id
//		";
//		$tb_data = Yii::$app->db->createCommand($sql, [':id'=>$value['c2enc']])->queryOne();
//		
//		if($tb_data){
//		    $i++;
//		    if($tb_data['f2p1_fileupload']==null){
//			
//			
//			$urlfile = "http://www1.cascap.in.th{$value['fullpath']}/{$value['filenamesys']}";
//			$file_target = Yii::$app->basePath . '/../backend/web/fileinput/' . $value['filenamesys'];
//
//			$imageString = file_get_contents($urlfile);
//			$save = file_put_contents($file_target, $imageString);
//
//			$status = 0;
//			if($save){
//			    $file++;
//			    
//			    $status = 1;
//
//			    $file_db = new \backend\modules\ezforms\models\FileUpload();
//			    $file_db->tbid = $tb_data['id'];
//			    $file_db->ezf_id = '1454041742064651700';
//			    $file_db->ezf_field_id = '1459501746059853500';
//			    $file_db->file_active = $value['fileactive']==1?1:0;
//			    $file_db->file_name = $value['filenamesys'];
//			    $file_db->file_name_old = $value['filename'];
//			    $file_db->target = $tb_data['ptid'];
//			    $file_db->mode = 1;
//
//			    $file_db->save();
//
//			    if($value['fileactive']==1){
//				Yii::$app->db->createCommand()->update('tb_data_4', [
//				    'f2p1_fileupload'=>$value['filenamesys']
//				], 'id=:id', [':id'=>$tb_data['id']])
//				->execute();
//			    }
//			    
//			    Yii::$app->db->createCommand()->insert('log_file', [
//				'ptid'=>$tb_data['id'],
//				'ptid_key'=>$tb_data['ptid'],
//				'file_id'=>$value['id'],
//				'upload_date'=>new \yii\db\Expression('NOW()'),
//				'file_type'=>'cca02p1',
//				'status'=>$status,
//			    ])->execute();
//			} else {
//			    Yii::$app->db->createCommand()->insert('log_file', [
//				'ptid'=>$tb_data['id'],
//				'ptid_key'=>$tb_data['ptid'],
//				'file_id'=>0,
//				'upload_date'=>new \yii\db\Expression('NOW()'),
//				'file_type'=>'cca02p1_not_found',
//				'status'=>$status,
//			    ])->execute();
//			}
//		    }
//		    
//		}
//		
//	    }
//	    \yii\helpers\VarDumper::dump($i.' #upload='.$file,10,true);
//	    echo "success <br>";
//	    echo '<meta http-equiv="refresh" content="2; URL='.\yii\helpers\Url::to(['/userlist/drump-file2', 'end'=>$end,'row'=>$limit+100]).'">';
//	    echo \yii\helpers\Html::a('NEXT '.$limit.'-'.($limit+100).' All 1,079', \yii\helpers\Url::to(['/userlist/drump-file2', 'row'=>$limit+100]));
//	} else {
//	    echo "not found";
//	}
//	
//    }
//    
//    public function actionDrumpFile3()
//    {
//	if(!Yii::$app->user->can('administrator')){
//	    echo 'Drump user by administrator only';
//	    exit();
//	}
//	$limit = isset($_GET['row'])?$_GET['row']:0;
//	$end = isset($_GET['end'])?$_GET['end']:0;
//	
//	if($end>0){
//	    if($limit>$end){
//		echo 'Drump end time';
//		exit();
//	    }
//	}
//	
//	ini_set('max_execution_time', 0);
//	set_time_limit(0);
//	ini_set('memory_limit','512M'); 
//	
//	$sql = "SELECT 	file_upload.ptid, 
//			file_upload.c2enc, 
//			file_upload.filename, 
//			file_upload.filenamesys, 
//			file_upload.fullpath, 
//			file_upload.filetype, 
//			file_upload.filesize, 
//			file_upload.fieldname, 
//			file_upload.tablename, 
//			file_upload.fileactive, 
//			file_upload.id
//	    FROM file_upload
//	    WHERE file_upload.fieldname = 'fopnote'
//	    limit $limit, 100
//	    ";
//	
//	$data = Yii::$app->dbcascap->createCommand($sql)->queryAll();
//	
//	if($data){
//	    $i=0;
//	    $file=0;
//	    foreach ($data as $key => $value) {
//		
//		$sql = "SELECT id, ptid, f3_fileupload
//		FROM tb_data_7
//		WHERE id=:id
//		";
//		$tb_data = Yii::$app->db->createCommand($sql, [':id'=>$value['c2enc']])->queryOne();
//		
//		if($tb_data){
//		    $i++;
//		    if($tb_data['f3_fileupload']==null){
//			
//			
//			$urlfile = "http://www1.cascap.in.th{$value['fullpath']}/{$value['filenamesys']}";
//			$file_target = Yii::$app->basePath . '/../backend/web/fileinput/' . $value['filenamesys'];
//
//			$imageString = file_get_contents($urlfile);
//			$save = file_put_contents($file_target, $imageString);
//
//			$status = 0;
//			if($save){
//			    $file++;
//			    
//			    $status = 1;
//
//			    $file_db = new \backend\modules\ezforms\models\FileUpload();
//			    $file_db->tbid = $tb_data['id'];
//			    $file_db->ezf_id = '1451381257025574200';
//			    $file_db->ezf_field_id = '1459504076029543100';
//			    $file_db->file_active = $value['fileactive']==1?1:0;
//			    $file_db->file_name = $value['filenamesys'];
//			    $file_db->file_name_old = $value['filename'];
//			    $file_db->target = $tb_data['ptid'];
//			    $file_db->mode = 1;
//
//			    $file_db->save();
//
//			    if($value['fileactive']==1){
//				Yii::$app->db->createCommand()->update('tb_data_7', [
//				    'f3_fileupload'=>$value['filenamesys']
//				], 'id=:id', [':id'=>$tb_data['id']])
//				->execute();
//			    }
//			    
//			    Yii::$app->db->createCommand()->insert('log_file', [
//				'ptid'=>$tb_data['id'],
//				'ptid_key'=>$tb_data['ptid'],
//				'file_id'=>$value['id'],
//				'upload_date'=>new \yii\db\Expression('NOW()'),
//				'file_type'=>'fopnote',
//				'status'=>$status,
//			    ])->execute();
//			} else {
//			    Yii::$app->db->createCommand()->insert('log_file', [
//				'ptid'=>$tb_data['id'],
//				'ptid_key'=>$tb_data['ptid'],
//				'file_id'=>0,
//				'upload_date'=>new \yii\db\Expression('NOW()'),
//				'file_type'=>'fopnote_not_found',
//				'status'=>$status,
//			    ])->execute();
//			}
//		    }
//		    
//		}
//		
//	    }
//	    \yii\helpers\VarDumper::dump($i.' #upload='.$file,10,true);
//	    echo "success <br>";
//	    echo '<meta http-equiv="refresh" content="2; URL='.\yii\helpers\Url::to(['/userlist/drump-file3', 'end'=>$end,'row'=>$limit+100]).'">';
//	    echo \yii\helpers\Html::a('NEXT '.$limit.'-'.($limit+100).' All 1,007', \yii\helpers\Url::to(['/userlist/drump-file3', 'row'=>$limit+100]));
//	} else {
//	    echo "not found";
//	}
//	
//    }
//    
//    public function actionDrumpFile4()
//    {
//	if(!Yii::$app->user->can('administrator')){
//	    echo 'Drump user by administrator only';
//	    exit();
//	}
//	$limit = isset($_GET['row'])?$_GET['row']:0;
//	$end = isset($_GET['end'])?$_GET['end']:0;
//	
//	if($end>0){
//	    if($limit>$end){
//		echo 'Drump end time';
//		exit();
//	    }
//	}
//	
//	ini_set('max_execution_time', 0);
//	set_time_limit(0);
//	ini_set('memory_limit','512M'); 
//	
//	$sql = "SELECT 	file_upload.ptid, 
//			file_upload.c2enc, 
//			file_upload.filename, 
//			file_upload.filenamesys, 
//			file_upload.fullpath, 
//			file_upload.filetype, 
//			file_upload.filesize, 
//			file_upload.fieldname, 
//			file_upload.tablename, 
//			file_upload.fileactive, 
//			file_upload.id
//	    FROM file_upload
//	    WHERE file_upload.fieldname = 'cca04'
//	    limit $limit, 100
//	    ";
//	
//	$data = Yii::$app->dbcascap->createCommand($sql)->queryAll();
//	
//	if($data){
//	    $i=0;
//	    $file=0;
//	    foreach ($data as $key => $value) {
//		
//		$sql = "SELECT id, ptid, f4_fileupload
//		FROM tb_data_8
//		WHERE id=:id
//		";
//		$tb_data = Yii::$app->db->createCommand($sql, [':id'=>$value['c2enc']])->queryOne();
//		
//		if($tb_data){
//		    $i++;
//		    if($tb_data['f4_fileupload']==null){
//			
//			
//			$urlfile = "http://www1.cascap.in.th{$value['fullpath']}/{$value['filenamesys']}";
//			$file_target = Yii::$app->basePath . '/../backend/web/fileinput/' . $value['filenamesys'];
//
//			$imageString = file_get_contents($urlfile);
//			$save = file_put_contents($file_target, $imageString);
//
//			$status = 0;
//			if($save){
//			    $file++;
//			    
//			    $status = 1;
//
//			    $file_db = new \backend\modules\ezforms\models\FileUpload();
//			    $file_db->tbid = $tb_data['id'];
//			    $file_db->ezf_id = '1452061550097822200';
//			    $file_db->ezf_field_id = '1459504129021565300';
//			    $file_db->file_active = $value['fileactive']==1?1:0;
//			    $file_db->file_name = $value['filenamesys'];
//			    $file_db->file_name_old = $value['filename'];
//			    $file_db->target = $tb_data['ptid'];
//			    $file_db->mode = 1;
//
//			    $file_db->save();
//
//			    if($value['fileactive']==1){
//				Yii::$app->db->createCommand()->update('tb_data_8', [
//				    'f4_fileupload'=>$value['filenamesys']
//				], 'id=:id', [':id'=>$tb_data['id']])
//				->execute();
//			    }
//			    
//			    Yii::$app->db->createCommand()->insert('log_file', [
//				'ptid'=>$tb_data['id'],
//				'ptid_key'=>$tb_data['ptid'],
//				'file_id'=>$value['id'],
//				'upload_date'=>new \yii\db\Expression('NOW()'),
//				'file_type'=>'cca04',
//				'status'=>$status,
//			    ])->execute();
//			} else {
//			    Yii::$app->db->createCommand()->insert('log_file', [
//				'ptid'=>$tb_data['id'],
//				'ptid_key'=>$tb_data['ptid'],
//				'file_id'=>0,
//				'upload_date'=>new \yii\db\Expression('NOW()'),
//				'file_type'=>'cca04_not_found',
//				'status'=>$status,
//			    ])->execute();
//			}
//		    }
//		    
//		}
//		
//	    }
//	    \yii\helpers\VarDumper::dump($i.' #upload='.$file,10,true);
//	    echo "success <br>";
//	    echo '<meta http-equiv="refresh" content="2; URL='.\yii\helpers\Url::to(['/userlist/drump-file4', 'end'=>$end,'row'=>$limit+100]).'">';
//	    echo \yii\helpers\Html::a('NEXT '.$limit.'-'.($limit+100).' All 1,396', \yii\helpers\Url::to(['/userlist/drump-file4', 'row'=>$limit+100]));
//	} else {
//	    echo "not found";
//	}
//	
//    }
//    
//    public function actionDrumpFile()
//    {
//	if(!Yii::$app->user->can('administrator')){
//	    echo 'Drump user by administrator only';
//	    exit();
//	}
//	$limit = isset($_GET['row'])?$_GET['row']:0;
//	$end = isset($_GET['end'])?$_GET['end']:0;
//	
//	if($end>0){
//	    if($limit>$end){
//		echo 'Drump end time';
//		exit();
//	    }
//	}
//	
//	ini_set('max_execution_time', 0);
//	set_time_limit(0);
//	ini_set('memory_limit','512M'); 
//	
//	$sql = "SELECT patient.ptid, patient.ptid_key
//	    FROM patient
//	    WHERE patient.vconsent=1
//	    limit $limit, 100
//	    ";
//	
//	$data = Yii::$app->dbcascap->createCommand($sql)->queryAll();
//	
//	if($data){
//	    $i=0;
//	    foreach ($data as $key => $value) {
//		
//		$sql = "SELECT id, ptid, icf_upload1, icf_upload2, icf_upload3
//		FROM tb_data_1
//		WHERE id=ptid AND id=:ptid
//		";
//		$tb_data_1 = Yii::$app->db->createCommand($sql, [':ptid'=>$value['ptid']])->queryOne();
//		
//		if($tb_data_1){
//		    $i++;
//		    
//		    if($tb_data_1['icf_upload1']==null){
//			
//			$sql = "SELECT file_upload.id, file_upload.tablename, 
//				    file_upload.fieldname, 
//				    file_upload.ptid, 
//				    file_upload.fileactive, 
//				    file_upload.filename, 
//				    file_upload.filenamesys, 
//				    file_upload.fullpath, 
//				    file_upload.filetype, 
//				    file_upload.filesize
//			    FROM file_upload
//			    WHERE file_upload.ptid = :ptid AND file_upload.fieldname='vconsent'
//			    ";
//
//			$data_file = Yii::$app->dbcascap->createCommand($sql, [':ptid'=>$value['ptid_key']])->queryAll();
//			
//			if($data_file){
//			    
//			    foreach ($data_file as $value_file) {
//				$urlfile = "http://www1.cascap.in.th{$value_file['fullpath']}/{$value_file['filenamesys']}";
//				$file_target = Yii::$app->basePath . '/../backend/web/fileinput/' . $value_file['filenamesys'];
//				
//				$imageString = file_get_contents($urlfile);
//				$save = file_put_contents($file_target, $imageString);
//				
//				$status = 0;
//				if($save){
//				    $status = 1;
//				    
//				    $file_db = new \backend\modules\ezforms\models\FileUpload();
//				    $file_db->tbid = $tb_data_1['id'];
//				    $file_db->ezf_id = '1437377239070461301';
//				    $file_db->ezf_field_id = '1446652646071443400';
//				    $file_db->file_active = $value_file['fileactive']==1?1:0;
//				    $file_db->file_name = $value_file['filenamesys'];
//				    $file_db->file_name_old = $value_file['filename'];
//				    $file_db->target = $tb_data_1['ptid'];
//				    $file_db->mode = 1;
//				    
//				    $file_db->save();
//				    
//				    if($value_file['fileactive']==1){
//					Yii::$app->db->createCommand()->update('tb_data_1', [
//					    'icf_upload1'=>$value_file['filenamesys']
//					], 'id=:id', [':id'=>$tb_data_1['id']])
//					->execute();
//				    }
//				    
//				}
//				
//				Yii::$app->db->createCommand()->insert('log_file', [
//				    'ptid'=>$value['ptid'],
//				    'ptid_key'=>$value['ptid_key'],
//				    'file_id'=>$value_file['id'],
//				    'upload_date'=>new \yii\db\Expression('NOW()'),
//				    'file_type'=>'vconsent',
//				    'status'=>$status,
//				])->execute();
//				
//			    }
//			}
//			
//		    }
//		    
//		    if($tb_data_1['icf_upload2']==null){
//			$sql = "SELECT file_upload.id,
//				    file_upload.tablename, 
//				    file_upload.fieldname, 
//				    file_upload.ptid, 
//				    file_upload.fileactive, 
//				    file_upload.filename, 
//				    file_upload.filenamesys, 
//				    file_upload.fullpath, 
//				    file_upload.filetype, 
//				    file_upload.filesize
//			    FROM file_upload
//			    WHERE file_upload.ptid = :ptid AND file_upload.fieldname='cid'
//			    ";
//
//			$data_file = Yii::$app->dbcascap->createCommand($sql, [':ptid'=>$value['ptid']])->queryAll();
//			
//			if($data_file){
//			    foreach ($data_file as $value_file) {
//				$urlfile = "http://www1.cascap.in.th{$value_file['fullpath']}/{$value_file['filenamesys']}";
//				$file_target = Yii::$app->basePath . '/../backend/web/fileinput/' . $value_file['filenamesys'];
//				
//				$imageString = file_get_contents($urlfile);
//				$save = file_put_contents($file_target, $imageString);
//				$status = 0;
//				if($save){
//				    $status = 1;
//				    
//				    $file_db = new \backend\modules\ezforms\models\FileUpload();
//				    $file_db->tbid = $tb_data_1['id'];
//				    $file_db->ezf_id = '1437377239070461301';
//				    $file_db->ezf_field_id = '1446652758038914800';
//				    $file_db->file_active = $value_file['fileactive']==1?1:0;
//				    $file_db->file_name = $value_file['filenamesys'];
//				    $file_db->file_name_old = $value_file['filename'];
//				    $file_db->target = $tb_data_1['ptid'];
//				    $file_db->mode = 1;
//				    
//				    $file_db->save();
//				    
//				    if($value_file['fileactive']==1){
//					Yii::$app->db->createCommand()->update('tb_data_1', [
//					    'icf_upload2'=>$value_file['filenamesys']
//					], 'id=:id', [':id'=>$tb_data_1['id']])
//					->execute();
//				    }
//				    
//				}
//				
//				Yii::$app->db->createCommand()->insert('log_file', [
//				    'ptid'=>$value['ptid'],
//				    'ptid_key'=>$value['ptid_key'],
//				    'file_id'=>$value_file['id'],
//				    'upload_date'=>new \yii\db\Expression('NOW()'),
//				    'file_type'=>'cid',
//				    'status'=>$status,
//				])->execute();
//				
//			    }
//			} else {
//			    Yii::$app->db->createCommand()->insert('log_file', [
//				'ptid'=>$value['ptid'],
//				'ptid_key'=>$value['ptid_key'],
//				'file_id'=>0,
//				'upload_date'=>new \yii\db\Expression('NOW()'),
//				'file_type'=>'not_found',
//				'status'=>0,
//			    ])->execute();
//			}
//			
//		    }
//		    
//		}
//		
//		//exit();
//	    }
//	    \yii\helpers\VarDumper::dump($i,10,true);
//	    echo "success <br>";
//	    echo '<meta http-equiv="refresh" content="2; URL='.\yii\helpers\Url::to(['/userlist/drump-file', 'end'=>$end,'row'=>$limit+100]).'">';
//	    echo \yii\helpers\Html::a('NEXT '.$limit.'-'.($limit+100).' All 124,861', \yii\helpers\Url::to(['/userlist/drump-file', 'row'=>$limit+100]));
//	} else {
//	    echo "not found";
//	}
//	
//    }
//    
//    public function actionDrump()
//    {
//	if(!Yii::$app->user->can('administrator')){
//	    echo 'Drump user by administrator only';
//	    exit();
//	}
//	$limit = isset($_GET['row'])?$_GET['row']:0;
//	
//	$sql = "SELECT puser.pid, 
//		    puser.username, 
//		    puser.passwords, 
//		    puser.title, 
//		    puser.`name`, 
//		    puser.surname, 
//		    puser.upriv, 
//		    puser.priv29txt, 
//		    puser.cid, 
//		    puser.cidstart, 
//		    puser.cidend, 
//		    puser.bankcompany, 
//		    puser.bankid, 
//		    puser.InvestigatorName, 
//		    puser.Affiliation, 
//		    puser.InvestigatorPhone, 
//		    puser.InvestigatorEmail, 
//		    puser.Registering, 
//		    puser.TypeofOrg, 
//		    puser.Country, 
//		    puser.HospitalName, 
//		    puser.OrganizationName, 
//		    puser.OrganizationAddress, 
//		    puser.OrganizationAcronyms, 
//		    puser.OrganizationParent, 
//		    puser.OrgProvCode, 
//		    puser.OrgProvince, 
//		    puser.OfficialRepresentative, 
//		    puser.Phone, 
//		    puser.Email, 
//		    puser.OrganizationURL, 
//		    puser.FundingOrganization, 
//		    puser.RegulatoryAuthority, 
//		    puser.RegulatoryAuthorityAddress, 
//		    puser.dadd, 
//		    puser.dupdate, 
//		    puser.lastsessionreg, 
//		    puser.stdid, 
//		    puser.stdprogram, 
//		    puser.stduniversity, 
//		    puser.hcode, 
//		    puser.tamboncode, 
//		    puser.tambon, 
//		    puser.amphurcode, 
//		    puser.amphur, 
//		    puser.provincecode, 
//		    puser.province, 
//		    puser.postcode, 
//		    puser.pid_link
//	    FROM puser
//	    LIMIT $limit ,1";
//	
//	$data = Yii::$app->dbcascap->createCommand($sql)->queryAll();
//	
//	$total = isset($_GET['total'])?$_GET['total']:0;
//	$total_f = isset($_GET['total_f'])?$_GET['total_f']:0;
//	$total_t = isset($_GET['total_t'])?$_GET['total_t']:0;
//	
//	if($data){
//	    foreach ($data as $key => $value) {
//		$total++;
//		$email = $value['Email'];
//		$validator = new EmailValidator();
//
//		if (!$validator->validate($email, $error)) {
//		   $email = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'@gmail.com';
//		} 
//		
//		$model = new User();
//		$model->id = \common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
//		$model->username = $value['username'];
//		$model->email = $email;
//		$model->status = true;
//		
//		if (isset($value['passwords'])) {
//		    $model->setPassword($value['passwords']);
//		}
//		if(isset($value['OrganizationName']) && !empty($value['OrganizationName'])){
//		    if($model->save()){
//
//			$auth =  Yii::$app->authManager;
//			//$auth->revokeAll($model->getId());
//			$auth->assign($auth->getRole(User::ROLE_MANAGER), $model->getId());
//
//			$modelProfile = new UserProfile();
//
//			$modelProfile->user_id = $model->getId();
//			$modelProfile->firstname = iconv('TIS-620', 'UTF-8', $value['name']);;
//			$modelProfile->lastname = iconv('TIS-620', 'UTF-8', $value['surname']);
//			$modelProfile->cid = isset($value['cid'])?$value['cid']:'9999999999999';
//			$modelProfile->gender = 1;
//			$modelProfile->sitecode = $value['OrganizationName'];
//			$modelProfile->email = $email;
//			$modelProfile->telephone = isset($value['Phone'])?$value['Phone']:\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime();
//			$modelProfile->title = $value['title'];
//			$modelProfile->status = '1';
//			$modelProfile->status_personal = '2';
//			$modelProfile->address_province = $value['provincecode'];
//			$modelProfile->address_amphur = $value['amphurcode'];
//			$modelProfile->address_tambon = $value['tamboncode'];
//			$modelProfile->address_text = iconv('TIS-620', 'UTF-8', $value['OrganizationAddress']);
//			$modelProfile->approved = 1;
//			
//			if($modelProfile->save()){
//			    $total_t++;
//			} else {
//			    $total_f++;
//			}
//			\yii\helpers\VarDumper::dump($modelProfile->attributes,10,true);
//			echo "name>>$modelProfile->firstname : success[ $total_t ] unsuccessful[ $total_f ] total $total";
//			//exit();
//			echo '<meta http-equiv="refresh" content="3; URL='.\yii\helpers\Url::to(['drump', 'row'=>$limit+1, 'total'=>$total, 'total_t'=>$total_t, 'total_f'=>$total_f]).'">';
//			//header( "refresh: 3; url=".\yii\helpers\Url::to(['drump', 'row'=>$limit+1, 'total'=>$total, 'total_t'=>$total_t, 'total_f'=>$total_f]) );
//			//return $this->redirect(['drump', 'row'=>$limit+1, 'total'=>$total, 'total_t'=>$total_t, 'total_f'=>$total_f]);
//		    } else {
//			$total_f++;
//			\yii\helpers\VarDumper::dump($model->attributes,10,true);
//			echo "user>>{$value['username']} : success[ $total_t ] unsuccessful[ $total_f ] total $total";
//			//exit();
//			echo '<meta http-equiv="refresh" content="3; URL='.\yii\helpers\Url::to(['drump', 'row'=>$limit+1, 'total'=>$total, 'total_t'=>$total_t, 'total_f'=>$total_f]).'">';
//			//header( "refresh: 3; url=".\yii\helpers\Url::to(['drump', 'row'=>$limit+1, 'total'=>$total, 'total_t'=>$total_t, 'total_f'=>$total_f]) );
//			//return $this->redirect(['drump', 'row'=>$limit+1, 'total'=>$total, 'total_t'=>$total_t, 'total_f'=>$total_f]);
//		    }
//		}
//	    }
//	} else {
//	    echo "success[ $total_t ] unsuccessful[ $total_f ] total $total";
//	}
//	
//    }
    
    
    public function actionSiteList($q = null, $id = null, $pcode=null) {
	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	$out = ['results' => []];
	if (!is_null($q)) {
	    $sql = "select distinct h.hcode as id,CONCAT(h.`name`, ' ต.', h.`tambon`,' อ.', h.`amphur`,' จ.', h.`province`) as text from user_profile as p left join all_hospital_thai as h on p.sitecode=h.hcode
                    WHERE h.hcode like :code OR h.`name` like :name GROUP BY h.`hcode` limit 20";
	    $data = Yii::$app->db->createCommand($sql, [':code'=>"$q%", ':name'=>"%$q%"])->queryAll();
	    
	    if($data){
		$out['results'] = array_values($data);
	    }
	} elseif (is_null($q)) {
	    $sql = "SELECT h.hcode as id, 
			concat(h.`name`, ' ต.', h.`tambon`,' อ.', h.`amphur`,' จ.', h.`province`, ' (', COUNT(*), ')') as text, 
			COUNT(*) as num,
			h.provincecode
		FROM user_profile p INNER JOIN all_hospital_thai h ON p.sitecode = h.hcode
		Where h.provincecode = :provincecode
		GROUP BY h.hcode
		limit 1000";
	    $data = Yii::$app->db->createCommand($sql, [':provincecode'=>$pcode])->queryAll();
	    if($data){
		$out['results'] = array_values($data);
	    }
	    
	} elseif ($id > 0) {
	    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$id])->one();
	    $out['results'] = ['id' => $id, 'text' => $model->name];
	}
	return $out;
    }
	
    public function actionAdminSite()
    {
	$sitecode = isset($_GET['site'])?$_GET['site']:Yii::$app->user->identity->userProfile->sitecode;
        
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchSite(Yii::$app->request->queryParams, $sitecode);
	
	$sql = "SELECT *
		FROM site_url";
	
	$sitelist = Yii::$app->db->createCommand($sql)->queryAll();
	
        return $this->render('admin-site', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	    'sitelist' => $sitelist,
        ]);
    }
    public function actionUploadFavicon($id) {
	
	
	$model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$id])->one();
	$favicon_file_old = $model->favicon;
	
	$model->favicon = \yii\web\UploadedFile::getInstanceByName('favicon');
	
	if ($model->favicon !== null) {
	    if($favicon_file_old!=''){
		unlink(Yii::$app->basePath . '/../storage/web/form-upload/' . $favicon_file_old);
	    }
	    $nowFileName = 'favicon_'.\common\lib\codeerror\helpers\GenMillisecTime::getMillisecTime().'.ico';
	    
	    $fullPath = Yii::$app->basePath . '/../storage/web/form-upload/' . $nowFileName;
	    
	    $model->favicon->saveAs($fullPath);
	    $model->favicon = $nowFileName;
	    $model->save();
	}
	return $this->redirect('page-setting');
    }
    
    public function actionUpdatesite($id) {
		if (Yii::$app->getRequest()->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			
			if (isset($_POST['siteurl']) && $_POST['siteurl']!='') {
			    
				if(in_array($_POST['siteurl'], ['www', 'backendURL', 'frontendURL', 'admin', 'storageUrl', 'common', 'frontend', 'backend', 'console', 'storage', 'tests'])){
				    $result = [
					    'status' => 'error',
					    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'ห้ามตั้งชื่อเว็บไซท์ที่เป็นคำสงวน'),

				    ];
				    return $result;
				}
				
				$checkDup = \backend\models\SiteUrl::find()->where('url=:url AND code<>:code', [':code'=>$id, ':url'=>$_POST['siteurl']])->count();
				if($checkDup>0){
				    $result = [
					    'status' => 'error',
					    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'URL นี้ถูกใช้งานแล้ว'),

				    ];
				    return $result;
				}
				$sql = "UPDATE `site_url` SET `url` = :url WHERE `code` = :code;";
				if (Yii::$app->db->createCommand($sql, [':url'=>$_POST['siteurl'], ':code'=>$id])->execute()) {

					$result = [
						'status' => 'success',
						'action' => 'update',
						'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
						
					];
					return $result;
				} else {
					$result = [
						'status' => 'success',
						'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
						
					];
					return $result;
				}
			} else {
				$result = [
					'status' => 'error',
					'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> '. Yii::t('app', 'Not null value.'),
					
				];
				return $result;
			}
		} else {
			throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
		}
	}
    public function actionManager($id, $auth)
    {
	Yii::$app->response->format = Response::FORMAT_JSON;
        if ($id == Yii::$app->user->getId()) {
	    
	    $result = [
		    'status' => 'error',
		    'action' => 'alert',
		    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'You can not action your own account.'),
		    'data' => $id,
	    ];
	    return $result;
        } else {
            $getAuth = Yii::$app->authManager->getAssignment($auth, $id);
	    if (isset($getAuth->roleName)) {
		$authorRole = Yii::$app->authManager->getRole($auth);
		Yii::$app->authManager->revoke($authorRole, $id);
		
		$modelProfile = UserProfile::find()->where('user_id=:id', [':id'=>$id])->one();
		$modelProfile->approved = 3;
		$modelProfile->save();
		
		$userProfile = \Yii::$app->user->identity->userProfile;
		$modelComment = new \backend\models\UserComment();
		$modelComment->user_id = $id;
		$modelComment->name = $userProfile->firstname. ' '. $userProfile->lastname;
		$modelComment->action = $modelProfile->approved;
		$modelComment->comment = "ยกเลิกสิทธิ์ $auth";
		$modelComment->save();
		
		$result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'User has been remove manager.'),
			'data' => $id,
		];
		return $result;
            } else {
		$authorRole = Yii::$app->authManager->getRole($auth);
                Yii::$app->authManager->assign($authorRole, $id);
		
		$modelProfile = UserProfile::find()->where('user_id=:id', [':id'=>$id])->one();
		$modelProfile->approved = 3;
		$modelProfile->save();
		
		$userProfile = \Yii::$app->user->identity->userProfile;
		$modelComment = new \backend\models\UserComment();
		$modelComment->user_id = $id;
		$modelComment->name = $userProfile->firstname. ' '. $userProfile->lastname;
		$modelComment->action = $modelProfile->approved;
		$modelComment->comment = "ยกเลิกสิทธิ์ $auth";
		$modelComment->save();
		
		$result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'User has been add manager.'),
			'data' => $id,
		];
		return $result;
            }
        }
    }
    
    public function actionStatus($id)
    {
	Yii::$app->response->format = Response::FORMAT_JSON;
        if ($id == Yii::$app->user->getId()) {
	    $result = [
		    'status' => 'error',
		    'action' => 'alert',
		    'message' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'You can not action your own account.'),
		    'data' => $id,
	    ];
	    return $result;
        } else {
            $user = $this->findModel($id);
            
	    if ($user->status==1) {
		$user->updateAttributes(['status' => 0]);
		$result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'User has been disabled.'),
			'data' => $id,
		];
		return $result;
            } else {
                $user->updateAttributes(['status' => 1]);
		$result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-ok-sign"></i> Success!</strong> ' . Yii::t('user', 'User has been active.'),
			'data' => $id,
		];
		return $result;
            }
        }
    }
    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserForm();
	$modelProfile = new UserProfile();
	$modelProfile->status = 0;
        $dataProvince = EzformQuery::getProvince();
	$dataHospital = [];
		
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && $modelProfile->load(Yii::$app->request->post()) && $model->save()) {
	    if($modelProfile->sitecode == '00000' || $modelProfile->sitecode == '') {
                $modelProfile->sitecode = $_POST['UserProfile']['sitecode'];
            }
	    
	    $modelProfile->save();
	    
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
	    'modelProfile'=>$modelProfile,
	    'dataProvince'=>$dataProvince,
	    'dataHospital'=>$dataHospital,
            'roles' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description')
        ]);
    }

    /**
     * Updates an existing User model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = new UserForm();
	$modelProfile = UserProfile::find()->where('user_id=:id', [':id'=>$id])->one();
        $dataProvince = EzformQuery::getProvince();
	$sqlHospital = "SELECT * FROM all_hospital_thai WHERE hcode=:hcode ";
        $dataHospital = Yii::$app->db->createCommand($sqlHospital,[':hcode'=>$modelProfile->sitecode])->queryOne();
	$modelProfile->sitecode = $dataHospital['hcode'] . ' ' . $dataHospital['name']. ' ต.' . $dataHospital['tambon']. ' อ.' . $dataHospital['amphur']. ' จ.' . $dataHospital['province'];
	$modelProfile->department = $dataHospital['hcode'];
        $model->setModel($this->findModel($id));
	
	$userProfile = \Yii::$app->user->identity->userProfile;
	$modelComment = new \backend\models\UserComment();
	$modelComment->user_id = $id;
	$modelComment->name = $userProfile->firstname. ' '. $userProfile->lastname;
	
	$roles_str = implode(', ', $model->roles);
	$dept_old = $modelProfile->department;
	
        if ($model->load(Yii::$app->request->post()) && $modelProfile->load(Yii::$app->request->post())) {
	    
	    $model->save();
	    
	    if($modelProfile->department==''){
		Yii::$app->session->setFlash('alert', [
		      'options' => ['class' => 'alert-danger'],
		      'body' => Yii::t('backend', 'กรุณาระบุหน่วยบริการของท่าน', [], $modelProfile->locale)
		]);
		  return $this->render('update', [
		    'model' => $model,
		    'modelProfile'=>$modelProfile,
		    'dataProvince'=>$dataProvince,
		    'dataHospital'=>$dataHospital,
		    'dataComment'=>$dataComment,
		    'modelComment'=>$modelComment,
		    'user'=>$user,
		    'roles' => $roles_db
		]);
	    }
	    
	    if($modelProfile->cid==''){
		$modelProfile->cid = $model->username;
	    }
	    $modelProfile->gender = (int)$modelProfile->gender;
	    $modelProfile->sitecode = $modelProfile->department;
	    $modelProfile->middlename = isset($modelProfile->middlename)?$modelProfile->middlename:'';
	    $modelProfile->avatar_path = isset($modelProfile->avatar_path)?$modelProfile->avatar_path:'';
	    $modelProfile->avatar_base_url = isset($modelProfile->avatar_base_url)?$modelProfile->avatar_base_url:'';
	    
	    
	    if(isset($_POST['reset_file']) && $_POST['reset_file']==1){
		@unlink(Yii::$app->basePath . '/../storage/web/source/' . $modelProfile->citizenid_file);
		@unlink(Yii::$app->basePath . '/../storage/web/source/' . $modelProfile->secret_file);
		
		$modelProfile->citizenid_file = '';
		$modelProfile->secret_file = '';
	    }
//	    \yii\helpers\VarDumper::dump($modelProfile,10,true);
//	    exit();
	    $modelProfile->save();
	    
	    $dept_new = $modelProfile->department;
	    
	    $roles = implode(', ', $model->roles);
	    $strR = '';
	    $conditionR = ($roles!=$roles_str);
	    if($conditionR){
		$strR = "[$roles] ";
	    }
	    $strD = '';
	    $conditionD = ($dept_old!=$dept_new);
	    if($conditionD){
		$strD = "[$dept_old -> $dept_new] ";
	    }

	    if($conditionR || $conditionD){
		$modelComment->action = $modelProfile->approved;
		$modelComment->comment = $strR.$strD.$modelComment->comment;
		$modelComment->save();
	    }
		    
            return $this->redirect(['update', 'id'=>$id]);
        }
	$roles = Yii::$app->authManager->getRoles();
	//ArrayHelper::remove($roles, 'administrator');
	
        return $this->render('update', [
            'model' => $model,
	    'modelProfile'=>$modelProfile,
	    'dataProvince'=>$dataProvince,
	    'dataHospital'=>$dataHospital,
            'roles' => ArrayHelper::map($roles, 'name', 'description')
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //Yii::$app->authManager->revokeAll($id);
	if ($id == Yii::$app->user->getId()) {
	    
	    Yii::$app->session->setFlash('alert', [
		'options' => ['class' => 'alert-danger'],
		'body' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('user', 'You can not action your own account.')
	    ]);
	    
	    return $this->redirect(['index']);
        } 
	
        $model = $this->findModel($id);
	
	$model->status = 0;
	$model->status_del = 1;
	$model->save();
	
        return $this->redirect(['index']);
    }
	
	public function actionPageSetting(){
	    $action = isset($_GET['action'])?$_GET['action']:'';
	    $code = isset($_GET['code'])?$_GET['code']:'';
	    
	    if($code!=''){
		if($action=='restore'){
		    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$code])->one();
		    if($model){
			$model->status = 0;
			$model->save();
		    }
		} elseif ($action=='create') {
		    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$code])->one();
		    if($model){
			$model->delete();
		    }
		} elseif ($action=='delete') {
		    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$code])->one();
		    if($model){
			$model->url = $model->code;
			$model->status = 2;
			$model->save();
			
			$sql = "DELETE FROM `site_menu` WHERE `code` = '$model->code';";
			Yii::$app->db->createCommand($sql)->execute();
		    }
		} elseif ($action=='ban') {
		    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$code])->one();
		    if($model){
			$model->status = 1;
			$model->save();
		    }
		} elseif ($action=='accept') {
		    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$code])->one();
		    if($model){
			$model->status = 3;
			$model->save();
		    }
		} elseif ($action=='block') {
		    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$code])->one();
		    if($model){
			$model->status = 4;
			$model->save();
		    }
		} elseif ($action=='remove') {
		    $model = \backend\models\SiteUrl::find()->where('code=:code', [':code'=>$code])->one();
		    if($model){
			$model->url = $model->code;
			$model->status = 5;
			$model->save();
			
			$sql = "DELETE FROM `site_menu` WHERE `code` = '$model->code';";
			Yii::$app->db->createCommand($sql)->execute();
		    }
		}
		
		return $this->redirect('page-setting');
	    }
	    
	    return $this->render('page-setting');
	}

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

	public function actionDeleteVo($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    $model = $this->findModelVo($id);
	    $model->job_status = 4;
	    $model->job_active = 0;
	    $model->updated_by = Yii::$app->user->id;
	    $model->updated_at = date('Y-m-d H:i:s');
	    $model->job_end = date('Y-m-d H:i:s');
	    
	    if ($model->save()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionUpdateVo($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModelVo($id);

	    if ($model->load(Yii::$app->request->post())) {
		$model->job_status = 1;
		$model->job_active = 1;
		$model->updated_by = Yii::$app->user->id;
		$model->updated_at = date('Y-m-d H:i:s');
		$model->job_start= date('Y-m-d H:i:s');
		
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => SDHtml::getMsgSuccess() . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'message' => SDHtml::getMsgError() . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('_form_popup', [
		    'model' => $model,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    protected function findModelVo($id)
    {
        if (($model = \frontend\modules\volunteer\models\VolunteerJob::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
