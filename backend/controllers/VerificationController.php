<?php
namespace backend\controllers;

use common\components\keyStorage\FormModel;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * Site controller
 *  จัดการเกี่ยวกับ การเข้าดู Report ของข้อมูล
 */
class VerificationController extends \yii\web\Controller
{
    public function actionScreening(){

        $debug = false;
//        $debug = true;
        if($debug){
            return $this->render('screening',[
                'z' => 0,
                'result' => 0,
                'phpsessionid' => 0,
                //'valudsession' => Yii::$app()->session['__id'];
                'valuetime' => 0,
                'userdet' => 0,
                'puser_login' => 0,
                'debug'=>true
            ]);
        }

        // get user detail
        $userdet = self::userDetail( $_SESSION["__id"] );
        $time = time();
        Yii::$app->formatter->locale = 'th_TH';
        $current_time = Yii::$app->formatter->asDate($time, 'yyyyMMddkkmmss');
        $puser_login = self::userLoginToCascapCloud($userdet);
        $userprof = self::userDetailFull($_SESSION["__id"]);
        $userhosp = self::userOnHospital($userprof[0]['sitecode']);
        //
        $a="test";
        $sql="select name,surname from tb_data_1 where length(name)>0 limit 1";
        $result =  Yii::$app->db->createCommand($sql)->query()->count();

        return $this->render('screening',[
            'z' => $a,
            'result' => $result,
            'phpsessionid' => $_COOKIE["PHPSESSID"],
            //'valudsession' => Yii::$app()->session['__id'];
            'valuetime' => $current_time,
            'userdet' => $userdet,
            'puser_login' => $puser_login,
            'userprof' => $userprof,
            'userhosp' => $userhosp,
         ]);


    }
    
    public function actionCascaptools(){
        // get user detail
        $userdet = self::userDetail( $_SESSION["__id"] );
        $userprof = self::userDetailFull($_SESSION["__id"]);
        $time = time();
        Yii::$app->formatter->locale = 'th_TH';
        $current_time = Yii::$app->formatter->asDate($time, 'yyyyMMddkkmmss');
        $puser_login = self::userLoginToCascapCloud($userdet);
        $a="redirect to cascap tolls";
        return $this->render('cascaptools',[
            'a' => $a,
            'userid' => $_SESSION["__id"],
            'userdet' => $userdet,
            'userprof' => $userprof,
         ]);
    }
    
    public function actionTest(){
        // get user detail
        $a='x';
        return $this->render('test',[
            'a' => $a,
         ]);
    }

    public function actionReport1(){
        $a='x';
        return $this->render('report1',[
            'myData' => $a,
         ]);
    }

    public function actionReport2(){
        $a='x';
        return $this->render('report2',[
            'myData' => $a,
         ]);
    }
    
    public function actionDatasummary(){
        if(count($_GET)>0){
            foreach ($_GET as $k => $v){
                $_vget[$k]=$v;
            }
        }
        $ugpriv=self::userPrivilegeGroup($_SESSION["__id"]);
        $sumCCA01Amphur = self::dataCCA01SumOfAmphur();
        $sumCCA02Amphur = self::dataCCA02SumOfAmphur();
        $hospitalOfAmp = self::hospitalSumOfAmphur();
        $setupUS = self::hospitalSetupUSSumOfAmphur();
        $a='x';
        return $this->render('datasummary',[
            'ugpriv' => $ugpriv,
            'sumCCA01Amphur' => $sumCCA01Amphur,
            'sumCCA02Amphur' => $sumCCA02Amphur,
            'hospitalOfAmp' => $hospitalOfAmp,
            'setupUS' => $setupUS,
            'a' => $a,
            '_vget' => $_vget,
         ]);
    }
    
    public function actionDatasummaryamphur(){
        
        $amphurcode=$_GET['amphur'];
        $provincecode=$_GET['province'];
        if(count($_GET)>0){
            foreach ($_GET as $k => $v){
                $_vget[$k]=$v;
            }
        }
        $sumCCA01Hospital = self::dataCCA01SumOnHospital($_GET['province'],$_GET['amphur']);
        $sumCCA02Hospital = self::dataCCA02SumOnHospital($_GET['province'],$_GET['amphur']);
        $hospitalOfAmp = self::hospitalSumOnProvince($_GET['province']);
        $setupUS = self::hospitalSetupUSSumOfAmphur();
        $a='x';
        return $this->render('datasummaryamphur',[
            'sumCCA01Hospital' => $sumCCA01Hospital,
            'sumCCA02Hospital' => $sumCCA02Hospital,
            'hospitalOfAmp' => $hospitalOfAmp,
            'setupUS' => $setupUS,
            'provincecode' => $provincecode,
            'amphurcode' => $amphurcode,
            'a' => $a,
            '_vget' => $_vget,
         ]);
    }
    
    public function actionDatasummarycca01advance(){
        // get user detail
        if(count($_GET)>0){
            foreach ($_GET as $k => $v){
                $_vget[$k]=$v;
            }
        }
        $sumCCA01 = self::dataCCA01SumAdvance();
        $hospitalOfAmp = self::hospitalSumOfAmphur();
        $setupUS = self::hospitalSetupUSSumOfAmphur();
        $a='x';
        return $this->render('datasummarycca01advance',[
            'sumCCA01' => $sumCCA01,
            'hospitalOfAmp' => $hospitalOfAmp,
            'setupUS' => $setupUS,
            'a' => $a,
            '_vget' => $_vget,
         ]);
    }
    
    public function actionPay(){
        // get user detail
        $a='x';
        return $this->render('pay',[
            'a' => $a,
         ]);
    }
    
    
    
    public function userDetail($userid){
        
        //$userid=1;
        $sql = "select * from user where id=\"".addslashes($userid)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
    
    public function userDetailFull($userid){
        
        //$userid=1;
        $sql = "select * from user_profile where user_id=\"".addslashes($userid)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
    public function userOnHospital($sitecode){
        
        //$userid=1;
        $sql = "select * from all_hospital_thai where hcode=\"".addslashes($sitecode)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
    public function userPrivilegeGroup($userid){
        unset($out);
        // กลุ่ม ตามการ Assign สิทธิ์ให้ โดยผู้ดูแลระบบ
        $sql = "select * from rbac_auth_assignment where user_id=\"".addslashes($userid)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if(count($result)>0){
            foreach($result as $k => $v){
                $out['usergroup'][$result[$k]['item_name']]=true;
            }
        }
        unset($result);
        // กลุ่ม ตามหน่วยบริการ อัตโนมัติ (sitecode)
        $sql = "select sitecode from user_profile where user_id=\"".addslashes($userid)."\" ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if( strlen($result[0]['sitecode'])>0 ){
            $sitecode=$result[0]['sitecode'];
        }
        if(strlen($sitecode)>0){
            unset($result);
            $sql = "select code4 from all_hospital_thai where hcode=\"".addslashes($sitecode)."\" ";
            $result =  Yii::$app->db->createCommand($sql)->queryAll();
            if( strlen($result[0]['code4'])>0 ){
                $code4=$result[0]['code4'];
            }
        }
        if($code4=='1'){
            $out['usergroup']['fprovince']=true;
            $out['usergroup']['sprovince']='1';
        }
        return $out;   
    }
    
    /*
     *  การทำงานของ userLoginToCascapCloud
     *      1. สร้าง connection เข้าสู่ www.cascap.in.th แบบ real time
     *          โดยใช้
     *              username
     *              $_COOKIE["PHPSESSID"]
     *              date ถ้าหากเวลา เมื่อ ลบกันแล้ว ยังไม่เกิน 2400 หรือ 1 วันก็สามารถ Login เข้าสู่ระบบได้เลย
     *
     */
    private function userLoginToCascapCloud($userdet){
        
        if( $userdet[0]["username"]=="webmaster" ){
            //$userdet[0]["username"]="bandit";
        }
        
        $sql="select puser_login.*,NOW()-datetimelogin as datecheck from v04cascap.puser_login ";
        $sql.="where source=\"".$_SERVER["HTTP_HOST"]."\" ";
        $sql.="and mobile_session=\"".$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"]."\" ";
        $sql.="and pid=\"".$_SESSION["__id"]."\" ";
        $sql.="and NOW()-datetimelogin<3600 ";
        //echo $sql;
        $puser_login =  Yii::$app->db->createCommand($sql)->queryAll();
        //if(strlen($puser_login[0][pid])>0){
        if( count($puser_login)>0 ){
            // ไม่ต้องเพิ่ม
        }else{
            // insert new record to puser_login
            $sql="insert into v04cascap.puser_login ";
            $sql.="set source=\"".$_SERVER["HTTP_HOST"]."\" ";
            $sql.=", mobile_session=\"".$_COOKIE["PHPSESSID"].$userdet[0]["auth_key"]."\" ";
            $sql.=", pid=\"".$_SESSION["__id"]."\" ";
            $sql.=",mobile_login=\"".addslashes($userdet[0]["username"])."\" ";
            $sql.=",datetimelogin=NOW() ";
            $puser_login_insert = Yii::$app->db->createCommand($sql)->query();
        }
        return $puser_login;
    }

    public function actionCascapGraph(){
        $debug = false;
//        $debug = true;
        return $this->render('graph',[
            'debug'=>$debug
        ]);
    }
    
    
    /*
     *  รายละเอียดเกี่ยวกับการสรุปการนำเข้าข้อมูล
     * 
     */
    public function dataCCA01SumOfAmphur(){
        
        if($_GET['orderby']=='province'){
            $sql = "select provincecode from cascap_capy_pp.now_tb_data_2 ";
            $sql.= "group by provincecode ";
            $sql.= "order by count(ptid) desc ";
            $result =  Yii::$app->db->createCommand($sql)->queryAll();
            $fieldprovince="";
            if(count($result)>0){
                foreach($result as $k => $v){
                    $fieldprovince.=",'".$result[$k]['provincecode']."'";
                }
            }
        }
        //$userid=1;
        $sql = "select zone_code,provincecode,amphurcode,count(ptid) as recs from cascap_capy_pp.now_tb_data_2 ";
        $sql.= "where rstat not in ('0','3') and length(provincecode)>0 and length(amphurcode)>0 ";
        $sql.= "group by zone_code,provincecode,amphurcode ";
        if(strlen($fieldprovince)>0){
            $sql.= "order by FIELD(provincecode".$fieldprovince."),count(*) desc ";
        }else{
            $sql.= "order by count(ptid) desc ";
        }
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
    public function dataCCA01SumAdvance(){
        
        if($_GET['orderby']=='province'){
            $sql = "select provincecode from cascap_capy_pp.now_tb_data_2 ";
            $sql.= "where rstat not in ('0','3') and length(provincecode)>0 and length(amphurcode)>0 ";
            if($_GET['criteriamoreth']=='1'){
                $sql.= "and age>=40 ";
                if($_GET['obj1']=='1'){
                    // มีประวัติ เคยติดเชื่อพยาธิใบไม้ตับ
                    $sql.= "and f1v7='2' ";
                }
                if($_GET['obj2']=='1'){
                    // มีประวัติ เคยติดเชื่อพยาธิใบไม้ตับ
                    $sql.= "and f1v8 in ('1','2','3') ";
                }
                if($_GET['obj3']=='1'){
                    // มีประวัติ เคยติดเชื่อพยาธิใบไม้ตับ
                    $sql.= "and f1v13='1' ";
                }
            }else if($_GET['criteriamoreth']=='2'){
                $sql.= "and age>=40 ";
                $sql.= "and criteria_count>=2 ";
            }else if($_GET['criteriamoreth']=='3'){
                $sql.= "and age>=40 ";
                $sql.= "and criteria_count>=3 ";
            }else if($_GET['criteriamoreth']=='4'){
                $sql.= "and age>=40 ";
                $sql.= "and criteria_count>=4 ";
            }
            $sql.= "group by provincecode ";
            $sql.= "order by count(*) desc ";
            $result =  Yii::$app->db->createCommand($sql)->queryAll();
            $fieldprovince="";
            if(count($result)>0){
                foreach($result as $k => $v){
                    $fieldprovince.=",'".$result[$k]['provincecode']."'";
                }
            }
        }
        //$userid=1;
        $sql = "select zone_code,provincecode,amphurcode,count(ptid) as recs from cascap_capy_pp.now_tb_data_2 ";
        $sql.= "where rstat not in ('0','3') and length(provincecode)>0 and length(amphurcode)>0 ";
        if($_GET['criteriamoreth']=='1'){
            $sql.= "and age>=40 ";
            if($_GET['obj1']=='1'){
                // มีประวัติ เคยติดเชื่อพยาธิใบไม้ตับ
                $sql.= "and f1v7='2' ";
            }
            if($_GET['obj2']=='1'){
                // มีประวัติ เคยติดเชื่อพยาธิใบไม้ตับ
                $sql.= "and f1v8 in ('1','2','3') ";
            }
            if($_GET['obj3']=='1'){
                // มีประวัติ เคยติดเชื่อพยาธิใบไม้ตับ
                $sql.= "and f1v13='1' ";
            }
        }else if($_GET['criteriamoreth']=='2'){
            $sql.= "and age>=40 ";
            $sql.= "and criteria_count>=2 ";
        }else if($_GET['criteriamoreth']=='3'){
            $sql.= "and age>=40 ";
            $sql.= "and criteria_count>=3 ";
        }else if($_GET['criteriamoreth']=='4'){
            $sql.= "and age>=40 ";
            $sql.= "and criteria_count>=4 ";
        }
        
        $sql.= "group by zone_code,provincecode,amphurcode ";
        if(strlen($fieldprovince)>0){
            $sql.= "order by FIELD(provincecode".$fieldprovince."),count(ptid) desc ";
        }else{
            $sql.= "order by count(ptid) desc ";
        }
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
    public function dataCCA02SumOfAmphur(){
        
        if($_GET['orderby']=='province'){
            $sql = "select provincecode from cascap_capy_pp.now_tb_data_3 group by provincecode order by count(*) desc ";
            $result =  Yii::$app->db->createCommand($sql)->queryAll();
            $fieldprovince="";
            if(count($result)>0){
                foreach($result as $k => $v){
                    $fieldprovince.=",'".$result[$k]['provincecode']."'";
                }
            }
        }
        
        //$userid=1;
        $sql = "select zone_code,provincecode,amphurcode,count(*) as recs from cascap_capy_pp.now_tb_data_3 ";
        $sql.= "where provincecode is not null and rstat not in ('0','3') and length(provincecode)>0 and length(amphurcode)>0 ";
        $sql.= "group by zone_code,provincecode,amphurcode ";
        if(strlen($fieldprovince)>0){
            $sql.= "order by FIELD(provincecode".$fieldprovince."),count(*) desc ";
        }else{
            $sql.= "order by count(*) desc ";
        }
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
    public function hospitalSumOfAmphur(){
        //$userid=1;
        $sql = "select distinct provincecode,amphurcode,concat(provincecode,amphurcode) as amphurkey,amphur,province from all_hospital_thai where provincecode is not null and length(provincecode)=2 and length(amphurcode)=2 order by provincecode,amphurcode ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if(count($result)>0){
            foreach($result as $k => $v){
                $out['amphur'][$result[$k]['amphurkey']]=$result[$k];
                $out['province'][$result[$k]['provincecode']]=$result[$k];
            }
        }
        return $out;
        
    }
    
    public function hospitalSetupUSSumOfAmphur(){
        //$userid=1;
        $sql = "select distinct zone_code,provincecode,amphurcode,concat(provincecode,amphurcode) as amphurkey,amphur,province,hcode,name from cascap_cpay.hospital_us_2015 where provincecode is not null and length(provincecode)=2 and length(amphurcode)=2 order by provincecode,amphurcode,hcode ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if(count($result)>0){
            foreach($result as $k => $v){
                $out['hospital'][$result[$k]['hcode']]=$result[$k];
                $out['amphur'][$result[$k]['amphurkey']]=$result[$k];
                $out['province'][$result[$k]['provincecode']]=$result[$k];
            }
        }
        return $out;
        
    }
    
    
    public function dataCCA01SumOnHospital($provincecode,$amphurcode){
        
        //$userid=1;
        $sql = "select zone_code,provincecode,amphurcode,hsitecode,count(*) as recs ";
        $sql.= "from cascap_capy_pp.now_tb_data_2 where rstat not in ('0','3') ";
        $sql.= "and provincecode='".addslashes($provincecode)."' ";
        $sql.= "and amphurcode='".addslashes($amphurcode)."' ";
        $sql.= "group by zone_code,provincecode,amphurcode,hsitecode order by count(*) desc ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
    public function dataCCA02SumOnHospital($provincecode,$amphurcode){
        
        //$userid=1;
        $sql = "select zone_code,provincecode,amphurcode,hsitecode,count(*) as recs ";
        $sql.= "from cascap_capy_pp.now_tb_data_3 where provincecode is not null and rstat not in ('0','3') ";
        $sql.= "and provincecode=\"".addslashes($provincecode)."\" ";
        $sql.= "and amphurcode=\"".addslashes($amphurcode)."\" ";
        $sql.= "group by zone_code,provincecode,amphurcode,hsitecode order by count(*) desc ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
        
    }
    
    public function hospitalSumOnProvince($provincecode){
        //$userid=1;
        $sql = "select distinct provincecode,amphurcode,hcode,concat(provincecode,amphurcode) as amphurkey ";
        $sql.= ",amphur,province,name ";
        $sql.= "from all_hospital_thai ";
        $sql.= "where provincecode is not null and length(provincecode)=2 ";
        $sql.= "and provincecode=\"".addslashes($provincecode)."\" ";
        $sql.= "order by provincecode,amphurcode,hcode ";
        $result =  Yii::$app->db->createCommand($sql)->queryAll();
        if(count($result)>0){
            foreach($result as $k => $v){
                $out['hospital'][$result[$k]['hcode']]=$result[$k];
                $out['amphur'][$result[$k]['amphurkey']]=$result[$k];
                $out['province'][$result[$k]['provincecode']]=$result[$k];
            }
        }
        return $out;
        
    }
    
    
}
