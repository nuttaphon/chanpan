<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 8/2/14
 * Time: 11:20 AM
 */

namespace backend\controllers;

use backend\models\LoginForm;
use backend\models\AccountForm;
use common\models\User;
use common\models\UserProfile;
use frontend\modules\user\models\Signupauto;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\web\Controller;

class SignInController extends Controller
{

    public $defaultAction = 'login';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post']
                ]
            ]
        ];
    }

    public function actions()
    {
        return [
            'oauth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successOAuthCallback']
            ],
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(215, 215);
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }

    public static function tis620_to_utf8($text) {
        $utf8 = "";
        for ($i = 0; $i < strlen($text); $i++) {
            $a = substr($text, $i, 1);
            $val = ord($a);

            if ($val < 0x80) {
                $utf8 .= $a;
            } elseif ((0xA1 <= $val && $val < 0xDA) || (0xDF <= $val && $val <= 0xFB)) {
                $unicode = 0x0E00+$val-0xA0; $utf8 .= chr(0xE0 | ($unicode >> 12));
                $utf8 .= chr(0x80 | (($unicode >> 6) & 0x3F));
                $utf8 .= chr(0x80 | ($unicode & 0x3F));
            }
        }
        return $utf8;
    }

    public function actionGoback() {
            $profile = Yii::$app->user->identity->userProfile;
            
            $profile->sitecode = Yii::$app->user->identity->userProfile->department;
            if ($profile->save()) {
                $this->redirect(Yii::getAlias('@backendUrl')); //return $this->redirect("/");
            }else{
                VarDumper::dump($model->getErrors(),10,true);
            }        
    }
    
    public function actionLogin()
    {
        $this->layout = 'base';
	
	
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $profile = Yii::$app->user->identity->userProfile;
            
            $profile->sitecode = Yii::$app->user->identity->userProfile->department;
            if (!$profile->save()) {
                VarDumper::dump($model->getErrors(),10,true);
            }
            
            setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);


            $TCC['username']=Yii::$app->request->post('LoginForm')['username'];
            $TCC['password']=Yii::$app->request->post('LoginForm')['password'];
            $TCC['email']=Yii::$app->user->identity->userProfile->email;
            $TCC['fname']=Yii::$app->user->identity->userProfile->firstname;
            $TCC['lname']=Yii::$app->user->identity->userProfile->lastname;
            $TCC['cid']=Yii::$app->user->identity->userProfile->cid;
            $TCC['mobile']=Yii::$app->user->identity->userProfile->telephone;
            $TCC['office']=Yii::$app->user->identity->userProfile->sitecode;
            $addr = Yii::$app->db->createCommand("Select * from all_hospital_thai where code5=:office",[':office'=>$TCC['office']])->queryOne();
            $TCC['address']=$addr['name'];
            $TCC['tambon']=$addr['provincecode'].$addr['amphurcode'].$addr['tamboncode'];
            $TCC['amphur']=$addr['provincecode'].$addr['amphurcode']."00";
            $TCC['changwat']=$addr['provincecode']."0000";

            setcookie("CloudUser", base64_encode(json_encode($TCC)), time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
            //echo VarDumper::dump($TCC,10,true); exit;
            return $this->goBack();
        } else {
            //check from CASCAP Tools Database
            if(Yii::$app->request->post()){
                $loginForm = Yii::$app->request->post('LoginForm');

                //เช็คข้อมูลที่เดิม ว่ามีข้อมูลหรือไม่
                $resx = Yii::$app->db->createCommand("SELECT `id` FROM `user` WHERE username = '".$loginForm['username']."';")->queryOne();
                if($resx['id']) {
                    return $this->render('login', [
                        'model' => $model
                    ]);
                }else{
                    $resx = Yii::$app->db->createCommand("SELECT `user_id` FROM user_profile WHERE cid = '".$loginForm['username']."';")->queryOne();
                    if($resx['user_id']) {
                        return $this->render('login', [
                            'model' => $model
                        ]);
                    }
                }
                $res = Yii::$app->dbcascap->createCommand("SELECT pid, username, passwords, `name`, `surname`, cid, OrganizationName, `Email`, `Phone` FROM puser WHERE username = '".$loginForm['username']."';");
		
                if($res->query()->count()){
                    $res = $res->queryOne();

                    if($loginForm['password'] == $res['passwords']){
                        //VarDumper::dump('pass', 10, true);
                        //Yii::$app->end();
                        $signup = new Signupauto();
                        $signup->username = $res['username'];
                        $signup->password = $res['passwords'];
                        $signup->email = $res['Email'];
                        //
                        $mprofile = new UserProfile();
                        $mprofile->sitecode = $res['OrganizationName'];
                        $mprofile->firstname = $this->tis620_to_utf8($res['name']);
                        $mprofile->lastname = $this->tis620_to_utf8($res['surname']);
                        $mprofile->email = $res['Email'];

                        $resx = Yii::$app->dbcascap->createCommand("select nologin from puser_priv WHERE pid = '".$res['pid']."';");

                        if($resx->query()->count()) {
                            $resx = $resx->queryOne();
                            //ยัง login ไม่ได้
                            if($resx['nologin']==1){
                                $mprofile->status = 0; // ให้เป็นบุคลากรทั่วไป
                            }else if($resx['nologin']==0  || $resx['nologin'] == ""){
                                $mprofile->status = 1; // ให้เป็นบุคลากร
                            }
                        }else {
                            $mprofile->status = 0; // ให้เป็นบุคลากรทั่วไป
                        }

                        $mprofile->status_personal = 3; //ผู้ให้บริการด้านการแพทย์และสาธารณสุข
                        $mprofile->locale = Yii::$app->language;
                        $mprofile->nation = 0;
                        $mprofile->cid = $res['cid'];
                        $mprofile->telephone = $res['Phone'];
                        $mprofile->department = $res['OrganizationName'];

                        //VarDumper::dump($mprofile, 10, true);
                        //Yii::$app->end();
                        $signup->signup($mprofile);

                        $model = new LoginForm();
                        if ($model->load(Yii::$app->request->post()) && $model->login()) {
                            setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()+180000, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
                            return $this->goBack();
                        }
                    }
                }
                //Yii::$app->end();
            }
	    //return $this->redirect(\Yii::getAlias('@frontendUrl').'/user/sign-in/login');
	    
            return $this->render('login', [
                'model' => $model
            ]);
        }
    }

    public static function actionViewimg($img)
    {
        return \backend\modules\ovcca\classes\OvccaFunc::getIframeFUrl($img);
    }
    
    public function actionLogout()
    {
        Yii::$app->user->logout();
        setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);
        
	return Yii::$app->getResponse()->redirect(Yii::getAlias('@frontendUrl'));
    }

    public function actionProfile()
    {
        // echo "<pre>";
        // print_r($_POST);
        // exit;
        $model = Yii::$app->user->identity->userProfile;
	// \backend\modules\ezforms\components\EzformQuery::getUserProfile(Yii::$app->user->id);
	
	$secret_file_old = $model->secret_file;
	$citizenid_file_old = $model->citizenid_file;


		
        $sqlHospital = "SELECT `hcode` as code,`name` FROM all_hospital_thai WHERE hcode='".$model->sitecode."' ";
        $dataHospital = Yii::$app->db->createCommand($sqlHospital)->queryOne();
        if ($model->load($_POST)) {
	    

	    
	    
	    $model->citizenid_file = UploadedFile::getInstance($model, 'citizenid_file');
	    $model->secret_file = UploadedFile::getInstance($model, 'secret_file');
	    $data = \common\models\UserProfile::getAdminsite($model->sitecode);
	    
	    if ($model->secret_file !== null) {
		$fileItems = $_FILES['UserProfile']['name']['secret_file'];
		$fileType = $_FILES['UserProfile']['type']['secret_file'];
		
		if($data){
		    $model->approved = 3;
		} else {
		    $model->approved = 4;
		}
	    
		$lname = 'jpg';
		$fileArr = explode('/', $fileType);
		if (isset($fileArr[1])) {
		    $lname = $fileArr[1];
		}
		
		
		if($secret_file_old!=''){
		    unlink(Yii::$app->basePath . '/../storage/web/source/' . $secret_file_old);
		}
		$idName = '';
		if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
			$idName = 'mycom';
		} else {
			$idName = $_SERVER["REMOTE_ADDR"];
		}
		//date("YmdHis")
		$nowFileName = 'secret_'.date('YmdHis').'.'.$lname;
		$fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

		$model->secret_file->saveAs($fullPath);
		$model->secret_file = $nowFileName;
	    } else {
		$model->secret_file = $secret_file_old;
	    }
	    
	    if ($model->citizenid_file !== null) {
		$fileItems = $_FILES['UserProfile']['name']['citizenid_file'];
		$fileType = $_FILES['UserProfile']['type']['citizenid_file'];
		
		if($data){
		    $model->approved = 3;
		} else {
		    $model->approved = 4;
		}
		
		$lname = 'jpg';
		$fileArr = explode('/', $fileType);
		if (isset($fileArr[1])) {
		    $lname = $fileArr[1];
		}
		
		if($citizenid_file_old!=''){
		    unlink(Yii::$app->basePath . '/../storage/web/source/' . $citizenid_file_old);
		}
		$idName = '';
		if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
			$idName = 'mycom';
		} else {
			$idName = $_SERVER["REMOTE_ADDR"];
		}
		//date("YmdHis")
		$nowFileName = 'citizenid_'.date('YmdHis').'.'.$lname;
		$fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

		$model->citizenid_file->saveAs($fullPath);
		$model->citizenid_file = $nowFileName;
	    } else {
		$model->citizenid_file = $citizenid_file_old;
	    }

            if($model->sitecode == '00000' || $model->sitecode == '') {
                $model->sitecode = $_POST['UserProfile']['sitecode'];
                $model->department = $_POST['UserProfile']['sitecode'];
            }
	    
            $model->department = isset($_POST['UserProfile']['sitecode'])?$_POST['UserProfile']['sitecode']:$model->sitecode;
            
            if ($model->save()) {
                
                Yii::$app->session->setFlash('alert', [
                    'options' => ['class' => 'alert-success'],
                    'body' => Yii::t('backend', 'Your profile has been successfully saved', [], $model->locale)
                ]);
                return $this->refresh();
            }else{
                //VarDumper::dump($model,10,true);
            }
        }
        return $this->render('profile', ['model'=>$model,'dataHospital'=>$dataHospital]);
    }
    public function actionFindhospital($q=null,$id=null){
	
	$strWhere = '';
	
	if(!Yii::$app->user->can('administrator')){
	    if(Yii::$app->user->can('adminnchangwat')){
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		$sql_hosp = "SELECT provincecode, amphurcode FROM `all_hospital_thai` WHERE hcode = :hcode";
		$data_hosp = Yii::$app->db->createCommand($sql, ['hcode'=>$sitecode])->queryOne();

		$strWhere = " AND provincecode={$data_hosp['provincecode']} ";
	    } elseif(Yii::$app->user->can('adminnamphur')){
		$sitecode = Yii::$app->user->identity->userProfile->sitecode;
		$sql_hosp = "SELECT provincecode, amphurcode FROM `all_hospital_thai` WHERE hcode = :hcode";
		$data_hosp = Yii::$app->db->createCommand($sql, ['hcode'=>$sitecode])->queryOne();

		$strWhere = " AND provincecode={$data_hosp['provincecode']} AND amphurcode={$data_hosp['amphurcode']} ";
	    } 
	}
	
        $sql = "SELECT `hcode` as code,`name` FROM `all_hospital_thai` WHERE CONCAT(`hcode`, ' ', `name`) LIKE '%".$q."%' $strWhere LIMIT 0,100";
        
	$data = Yii::$app->db->createCommand($sql)->queryAll();
        
	$i = 0;
        $out = "";
        foreach($data as $value){
            $out["results"][$i] = ['id'=>$value['code'],'text'=> $value["code"] ." " .$value["name"]];
            $i++;
        }
	if(!$data){
	    $out = ['results' => []];
	}
        return json_encode($out);
    }
    public function actionAccount()
    {
        $user = Yii::$app->user->identity;
        $model = new AccountForm();
        $model->username = $user->username;
        if ($model->load($_POST) && $model->validate()) {
	    
	    //$password_old = $model->password_old;
	    $password = $model->password;
	    $change = 0;
	    
            if ($model->password) {
                $user->setPassword($model->password);
            }
	    
	    if ($model->username != $user->username) {
		$change = 1;
	    }
	    
	    $user->username = $model->username;
	    
	    //if($user->validatePassword($password_old)){
		$user->save();
		if($change){
		    Yii::$app->user->logout();
		    setcookie("CloudUserID", $user->username, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);

		    return Yii::$app->getResponse()->redirect(Yii::getAlias('@frontendUrl'));
		}
//	    } else {
//		Yii::$app->session->setFlash('alert', [
//		    'options'=>['class'=>'alert-danger'],
//		    'body'=>Yii::t('backend', 'กรุณากรอกรหัสผ่านเดิมให้ถูกต้องเพื่อยืนยันตัวตน')
//		]);
//		return $this->refresh();
//	    }
            
	    
	    $res = Yii::$app->dbcascap->createCommand("SELECT username, passwords, `name`, `surname`, cid, OrganizationName, `Email`, `Phone` FROM puser WHERE username = :username ;", [':username'=>$user->username]);

	    if($res->query()->count()){
		Yii::$app->dbcascap->createCommand()->update('puser', ['passwords'=>$password], 'username = :username', [':username' => $user->username]);
	    }
	    
            Yii::$app->session->setFlash('alert', [
                'options'=>['class'=>'alert-success'],
                'body'=>Yii::t('backend', 'Your account has been successfully saved')
            ]);
            return $this->refresh();
        }
        return $this->render('account', ['model'=>$model]);
    }

    public function actionShowUserPanel(){
        $model = Yii::$app->user->identity->userProfile;
        $sqlHospital = "SELECT `hcode` as code,`name` FROM all_hospital_thai WHERE hcode='".$model->sitecode."' ";
        $about = Yii::$app->db->createCommand($sqlHospital)->queryOne();
        $userGroup = Yii::$app->db->createCommand("SELECT * FROM `rbac_auth_assignment` WHERE user_id = :user_id;", [':user_id'=>$model->user_id])->queryAll();
        return $this->renderAjax('_modal_user_info',[
            'about' => $about,
            'usergroup' => $userGroup,
        ]);
    }
}
