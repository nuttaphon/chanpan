<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\UploadForm;
use yii\web\UploadedFile;
use \backend\models\Dynamic;
use common\lib\codeerror\helpers\GenMillisecTime;

class FileUploadController extends Controller
{
     public $layout = '@backend/views/layouts/common';
   
    public function actionIndex()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->excelFile = UploadedFile::getInstance($model, 'excelFile');


            if ($model->upload()) {
                // file is uploaded successfully
                return $this->render('upload', ['model' => $model]);
            }

        }
        return $this->render('index',['model' => $model]);
    }
    
    public function actionUpload()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        return $this->render('upload', ['model' => $model]);
    }
    
    public function actionGenezform() 
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $post=Yii::$app->request->post('UploadForm');
        
        $ezf_id=$post['ezf_id'];

        $objPHPExcel = \PHPExcel_IOFactory::load(Yii::$app->basePath. '/uploads/'.urlencode($post['excelFile']));
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $header = $sheetData[1];
        $varindex=1;
        
        $connection = \Yii::$app->db;
        $query = $connection->createCommand('DELETE FROM `ezform_fields` WHERE ezf_id =\''.$ezf_id.'\'')->query();
        
        $query = $connection->createCommand('DROP table IF EXISTS tbdata_'.$ezf_id)->query();
        $query = $connection->createCommand('Create table tbdata_'.$ezf_id.' like tbdata_init')->query();
        

        foreach($header as $key => $value) {
            $fieldInsert = new Dynamic();
            $fieldInsert->setTableName('ezform_fields');  
            $fieldInsert->ezf_field_id=GenMillisecTime::getMillisecTime();
            $fieldInsert->ezf_id=$ezf_id;
            //$fieldInsert->ezf_field_group=0;
            $fieldInsert->ezf_field_ref=0;
            $fieldInsert->ezf_field_rows=0;
            $fieldInsert->ezf_component=0;
            $fieldInsert->ezf_field_name='var'.$varindex;
            $fieldInsert->ezf_field_label=$value;
            $fieldInsert->ezf_field_order=$varindex;
            $fieldInsert->ezf_field_type=1;
            $varname[$varindex]=$fieldInsert->ezf_field_name;
            $fieldInsert->save();
            $query = $connection->createCommand('Alter table tbdata_'.$ezf_id.' ADD COLUMN `'.$fieldInsert->ezf_field_name.'` varchar(50)')->query();
            $varindex++;
        }
        $dataid=1;
        foreach($sheetData as $rowkey => $row) {
            if ($rowkey==1)                continue;            
            $varindex=1;
            $data = new Dynamic();
            $data->setTableName('tbdata_'.$ezf_id);

            foreach ($row as $col => $value) {
                $data->id=$dataid;
                $data->rstat=2;
                $fieldname = $varname[$varindex];
                $data->user_create=Yii::$app->user->id;
                $data->create_date=Date("Y-m-d H:i:s");
                $data[$fieldname]=$value."";
                //echo $fieldname . " $value<br>";
                $varindex++;
            }
            $dataid++;
            $data->save();
        }
        
        $connection->close();
        return $this->redirect('/ezforms/ezform/update?id='.$ezf_id);
    }
}
