<?php
namespace backend\controllers;

use common\components\keyStorage\FormModel;
use Yii;

/**
 * Site controller
 */
class ReportController extends \yii\web\Controller
{

    public function actionReport1(){
        return $this->render('report1');
    }

    public function actionReport2(){
        return $this->render('report2');
    }

    public function actionReport3(){
        return $this->render('report3');
    }

    public function actionReport4(){
        return $this->render('report4');
    }    

}
