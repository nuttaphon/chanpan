<?php

namespace backend\controllers;

use backend\models\EzformCoDev;
use backend\models\QueryList;
use backend\models\QueryManager;
use backend\models\QueryReply;
use backend\models\UserProfile;
use backend\modules\ezforms\components\EzActiveForm;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformDynamic;
use backend\modules\ezforms\models\EzformFields;
use backend\modules\ezforms\models\EzformSqlLog;
use backend\modules\ezforms\models\FileUpload;
use Yii;
use backend\models\QueryRequest;
use backend\models\QueryRequestSearch;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveField;

/**
 * QueryRequestController implements the CRUD actions for QueryRequest model.
 */
class QueryRequestController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all QueryRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QueryRequestSearch();
        if(Yii::$app->request->get('ezf_id')=='all') {
            $dataProvider = $searchModel->searchAll(Yii::$app->request->queryParams);

            return $this->render('index_all', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionViewByForm($ezf_id)
    {
        $searchModel = new QueryRequestSearch();
        $dataProvider = $searchModel->searchByForm(Yii::$app->request->queryParams, $ezf_id);

        $ezformCoDev = EzformCoDev::find()->where('ezf_id = :ezf_id', [':ezf_id'=>$ezf_id])->all();
        $ezform = Ezform::find()->select('ezf_name, user_create')->where('ezf_id = :ezf_id', [':ezf_id'=>$ezf_id])->one();

        $chkOwner = QueryManager::find()->where('ezf_id = :ezf_id', [':ezf_id'=>$ezf_id])->andWhere('status = :status', [':status'=>'1']);
        //ถ้าไม่มี add auto
        if(!$chkOwner->count()) {

            //insert Owner
            Yii::$app->db->createCommand()->batchInsert('query_manager', ['user_id', 'ezf_id', 'status', 'user_group'],
                [
                    [$ezform->user_create, $ezf_id, '1', '1'],
                ])
                ->execute();

            //insert Co-dev
            $arrCodev=[]; $i = 0;
            foreach ($ezformCoDev AS $val) {
                $arrCodev[$i][] = $val->user_co;
                $arrCodev[$i][] = $ezf_id;
                $arrCodev[$i][] = '2';
                $arrCodev[$i][] = '1';
                $i++;
            }
            if(count($arrCodev)) {
                //VarDumper::dump($arrCodev, 10, true); exit;
                Yii::$app->db->createCommand()->batchInsert('query_manager', ['user_id', 'ezf_id', 'status', 'user_group'], $arrCodev)
                    ->execute();
            }
        }

        //user from site
        $userDataManager = Yii::$app->db->createCommand('SELECT `user_id` AS id, CONCAT(`firstname`, " ", `lastname`) AS text FROM `user_profile` WHERE sitecode = :sitecode', [':sitecode' => Yii::$app->user->identity->userProfile->sitecode,])->queryAll();
        $userDataManager = ArrayHelper::map($userDataManager, 'id', 'text');

        //VarDumper::dump($userDataManager, 10, true); Yii::$app->end();

        //admin กลาง
        $queryManager = QueryManager::find()->where('ezf_id = :ezf_id', [':ezf_id'=>$ezf_id])->all();
        $i=0; $userOwnerNotIn = ''; $initiUserManager=[]; $initiUserManagerSite=[]; $userOwnerNotInSite='';
        foreach($queryManager AS $val){
            if($val->user_group == 1){
                // 1= ผู้สร้าง
                // 2 = ผู้ร่วมสร้าง
                if($val->status ==1 || $val->status ==2){
                    $user = UserProfile::findOne($val->user_id);
                    $queryManager[$i]->user_id = $user->firstname . ' ' . $user->lastname;
                    unset($userDataManager[$val->oldAttributes['user_id']]); //ไม่เอาคนสองกลุ่มนี้
                }
                // 3 datamanager กลาง
                else if($val->status == 3){
                    $initiUserManager[] = $val->user_id;
                }
            }else if($val->user_group == 2){
                if($val->status ==1){
                    $initiUserManagerSite[] = $val->user_id;
                }
            }
            $i++;
        }

        //
        $userOwner = $ezform->user_create;

        //list user site admin
        $userDataManagerSiteAdmin = Yii::$app->db->createCommand('SELECT
	b.`user_id` AS id,
	CONCAT(b.`firstname`, " ", b.`lastname`) AS text
FROM
	`rbac_auth_assignment` as a
INNER JOIN
	user_profile as b
ON a.user_id = b.user_id
WHERE
	b.sitecode = :sitecode AND
	a.item_name = "adminsite";', [':sitecode' => Yii::$app->user->identity->userProfile->sitecode,])->queryAll();
        $userDataManagerSiteAdmin = ArrayHelper::map($userDataManagerSiteAdmin, 'id', 'text');

        foreach($userDataManagerSiteAdmin as $key => $val){
            unset($userDataManager[$key]);
        }

        //VarDumper::dump($userDataManager, 10, true); Yii::$app->end();

        return $this->render('index_all', [
            'dataProvider' => $dataProvider,
            'ezform' => $ezform,
            'queryManager' => $queryManager,
            'userDataManager' => $userDataManager,
            'userDataManagerSiteAdmin' => $userDataManagerSiteAdmin,
            'userOwner' => $userOwner,
            'initiUserManager' => $initiUserManager,
            'initiUserManagerSite' =>$initiUserManagerSite,

        ]);
    }

    public function actionSaveDataManager()
    {
        if (Yii::$app->getRequest()->isAjax) {
            if(Yii::$app->request->get('action')=='central'){
                $user_group = 1;
                $status = 3;
            }else if(Yii::$app->request->get('action')=='site'){
                $user_group =2;
                $status = 1;
            }

            $ezf_id = Yii::$app->request->post('ezf_id');
            Yii::$app->db->createCommand("DELETE FROM query_manager WHERE ezf_id='$ezf_id' AND status = :status AND user_group = :user_group;", [
                ':user_group'=>$user_group,
                ':status'=>$status,
            ])->query();
            $user_id = Yii::$app->request->post('val');
            $values = '';
            //$userlist = '';
            foreach ($user_id as $k => $val) {
                $values .= "('$ezf_id', '$val', '$status', '$user_group'), ";
                //$userlist .= ',' . $val;
            }

            $values = substr($values, 0, -2);
            //$userlist = substr($userlist, 1);

            $sql = "INSERT INTO query_manager (`ezf_id`, `user_id`, `status`, `user_group`) VALUES" . $values;
            Yii::$app->db->createCommand($sql)->query();

            //
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [
                'status' => 'success',
                'action' => 'create',
                'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', ''),
            ];
            return $result;
        }
    }

    public function actionEncodeJsonOldval(){
        VarDumper::dump($_POST, 10, true); Yii::$app->end();
        //return json_encode($_POST['SDDynamicModel']);
    }

    /**
     * Displays a single QueryRequest model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelQueryLog = QueryRequest::find()->where('ezf_id = :ezf_id', [':ezf_id' => $model->ezf_id])->andWhere('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $model->ezf_field_id])->andWhere('target_id = :target_id', [':target_id' => $model->target_id])->orderBy(['time_create'=>SORT_DESC])->all();

        if($model->status == 1){
            $model->status = 'Waiting';
        }else if($model->status ==2){
            $model->status = 'Resolve with some change';
        }else if($model->status ==3){
            $model->status = 'Resolve without any change';
        }else if($model->status ==4){
            $model->status = 'Unresolvable';
        }else if($model->status ==5){
            $model->status = 'Remark';
        }
        //
        if ($model->target_id) {
            //$user = common\models\UserProfile::findOne($model->target);
            //echo $ezf_table_comp; echo'<hr>'; print_r($arr_comp_desc_field_name); exit;
            $comp_name = new \backend\models\Dynamic();
            $ezf_table_comp = \backend\modules\ezforms\components\EzformQuery::checkIsTableComponent($model->ezf_id);
            $arr_desc = explode(',', $ezf_table_comp['field_id_desc']);
            $arr_comp_desc_field_name =[];
            foreach ($arr_desc AS $val) {
                $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$val';")->queryOne();
                $arr_comp_desc_field_name[] = $ezf_fields['ezf_field_name'];
            }
            $ezf_table_comp = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezf_table_comp['ezf_id']);
            $comp_name->setTableName($ezf_table_comp->ezf_table);
            $comp_name = $comp_name::findOne($model->target_id);
            $str = '';
            foreach ($arr_comp_desc_field_name as $val) {
                $str .= $comp_name->$val . ' ';
            }
            $model->target_id =  $str;
            //return $model->firstname.' '.$model->lastname;
            //return $model->target;
        } else
            $model->target_id = 'ไม่ระบุเป้าหมาย';
        //
        $ezform = \backend\modules\ezforms\models\Ezform::find()->select('ezf_name')->where('ezf_id = :ezf_id', [':ezf_id'=>$model->ezf_id])->one();
        $model->ezf_id = $ezform->ezf_name;
        //
        $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name, ezf_field_label FROM ezform_fields WHERE ezf_field_id = :ezf_field_id", [':ezf_field_id' => $model->ezf_field_id])->queryOne();
        $model->ezf_field_id = $ezf_fields['ezf_field_label'].' ('.$ezf_fields['ezf_field_name'].')';

        //old value
        $ezform = EzformQuery::getFormTableName($model->oldAttributes['ezf_id']);
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $model->oldAttributes['ezf_id']])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        //VarDumper::dump($model->oldAttributes['ezf_id'], 10, true); exit;

        $modelDynamic = new EzformDynamic($ezform->ezf_table);
        $model_table = $modelDynamic->find()->where('id = :id', [':id' => $model->data_id])->One();

        $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        $model_gen->attributes = $model_table->attributes;

        $ezf_field = EzformFields::find()->where(['ezf_field_id'=>$model->oldAttributes['ezf_field_id']])->one();
        $form = ActiveForm::begin(['id'=>$model->formName()]);
        $options_input['query_tools_render']=1;
        //VarDumper::dump(json_decode($model->old_val), true); exit;

        //loop old value
        $arr = json_decode($model->old_val);
        foreach($arr as $key=>$val){
            $model_gen->$key = $val;
        }
        if($model->status ==2){
            if($ezf_field->ezf_field_type == 14 || $ezf_field->ezf_field_type == 13){
                $model->old_val = '';
            }
            else if($ezf_field->ezf_field_type == 14) {
                $fileModel = \backend\modules\ezforms\models\FileUploadSearch::find()->where('ezf_id=:ezf_id AND ezf_field_id =:ezf_field_id AND tbid =:tbid AND file_active = -9');
                $dataProvider = $fileModel->search();
                $view = Yii::$app->getView();
                $model->old_val = $view->render('/inputdata/_files_upload', ['dataProvider' => $dataProvider, 'field' => $ezf_field]);
            }

        }
        if($ezf_field->ezf_field_type == 14 || $ezf_field->ezf_field_type == 13){
            $model->old_val = '';
        }
        else {
            $model->old_val = \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $ezf_field, $form, $options_input);
        }

        //loop new value
        $arr = json_decode($model->new_val);
        foreach($arr as $key=>$val){
            $model_gen->$key = $val;
        }

        if($model->status ==2){
            if($ezf_field->ezf_field_type == 14 || $ezf_field->ezf_field_type == 13){
                $model->old_val = '';
            }
            else if($ezf_field->ezf_field_type == 14) {
                $fileModel = \backend\modules\ezforms\models\FileUploadSearch::find()->where('ezf_id=:ezf_id AND ezf_field_id =:ezf_field_id AND tbid =:tbid AND file_active = 9');
                $dataProvider = $fileModel->search();
                $view = Yii::$app->getView();
                $model->new_val = $view->render('/inputdata/_files_upload', ['dataProvider' => $dataProvider, 'field' => $ezf_field]);
            }

        }
        if($ezf_field->ezf_field_type == 14 || $ezf_field->ezf_field_type == 13){
            $model->old_val = '';
        }
        else {
            $options_input['query_tools_input'] = 9;
            $model->new_val = \backend\modules\ezforms\components\EzformFunc::getTypeEform($model_gen, $ezf_field, $form, $options_input);
        }
        //

        $user = UserProfile::findOne($model->user_create);
        $model->user_create = $user->firstname . ' ' . $user->lastname;
        //
        $user = UserProfile::findOne($model->user_update);
        $model->user_update = $user->firstname . ' ' . $user->lastname;
        //
        $user = UserProfile::findOne($model->approved_by);
        $model->approved_by = $user->firstname . ' ' . $user->lastname;
        //
        $user = UserProfile::findOne($model->userkey);
        $model->userkey = $user->firstname . ' ' . $user->lastname;
        //
        $modelReply = new QueryReply();
        $modelReplyData = new ActiveDataProvider([
            'query' => QueryReply::find()->where('query_id = :query_id', [':query_id' => $id])->orderBy('time_create DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $modelReplyDataTotal = $modelReplyData->count;
        for($i=0;$i<$modelReplyDataTotal;$i++){
            if($modelReplyData->models[$i]->status == 1){
                $modelReplyData->models[$i]->status = 'Waiting';
            }else if($modelReplyData->models[$i]->status ==2){
                $modelReplyData->models[$i]->status = 'Resolve with some change';
            }else if($modelReplyData->models[$i]->status ==3){
                $modelReplyData->models[$i]->status = 'Resolve without any change';
            }else if($modelReplyData->models[$i]->status ==4){
                $modelReplyData->models[$i]->status = 'Unresolvable';
            }else if($modelReplyData->models[$i]->status ==5){
                $modelReplyData->models[$i]->status = 'Remark';
            }
            //
            if ($modelReplyData->models[$i]->target_id) {
                //$user = common\models\UserProfile::findOne($model->target);
                //echo $ezf_table_comp; echo'<hr>'; print_r($arr_comp_desc_field_name); exit;
                $comp_name = new \backend\models\Dynamic();
                $ezf_table_comp = \backend\modules\ezforms\components\EzformQuery::checkIsTableComponent($modelReplyData->models[$i]->ezf_id);
                $arr_desc = explode(',', $ezf_table_comp['field_id_desc']);
                $arr_comp_desc_field_name =[];
                foreach ($arr_desc AS $val) {
                    $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$val';")->queryOne();
                    $arr_comp_desc_field_name[] = $ezf_fields['ezf_field_name'];
                }
                $ezf_table_comp = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezf_table_comp['ezf_id']);
                $comp_name->setTableName($ezf_table_comp->ezf_table);
                $comp_name = $comp_name::findOne($modelReplyData->models[$i]->target_id);
                $str = '';
                foreach ($arr_comp_desc_field_name as $val) {
                    $str .= $comp_name->$val . ' ';
                }
                $modelReplyData->models[$i]->target_id =  $str;
                //return $model->firstname.' '.$model->lastname;
                //return $model->target;
            } else
                $modelReplyData->models[$i]->target_id = 'ไม่ระบุเป้าหมาย';
            //
            $ezform = \backend\modules\ezforms\models\Ezform::find()->select('ezf_name')->where('ezf_id = :ezf_id', [':ezf_id'=>$modelReplyData->models[$i]->ezf_id])->one();
            $modelReplyData->models[$i]->ezf_id = $ezform->ezf_name;
            //
            $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name, ezf_field_label FROM ezform_fields WHERE ezf_field_id = :ezf_field_id", [':ezf_field_id' => $modelReplyData->models[$i]->ezf_field_id])->queryOne();
            $modelReplyData->models[$i]->ezf_field_id = $ezf_fields['ezf_field_label'].'('.$ezf_fields['ezf_field_name'].')';
            //
            $user = UserProfile::findOne($modelReplyData->models[$i]->user_create);
            $modelReplyData->models[$i]->user_create = $user->firstname . ' ' . $user->lastname;
        }

        $queryManager = QueryManager::find()->where('user_id = :user_id', [':user_id'=>Yii::$app->user->id])->one();
        //\yii\helpers\VarDumper::dump($modelRaw->oldAttributes['ezf_id'], 10 ,true); Yii::$app->end();

        return $this->render('view', [
            'model' => $model,
            'modelReply' => $modelReply,
            'modelReplyData' => $modelReplyData,
            'queryManager' => $queryManager,
            'modelQueryLog' => $modelQueryLog,
        ]);
    }

    /**
     * Creates a new QueryRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $ezf_id = Yii::$app->request->get('ezf_id');
        if(Yii::$app->request->get('dataid')) {
            $data_id = Yii::$app->request->get('dataid');
            $ezf_field_id = Yii::$app->request->get('ezf_field_id');
        }else if(Yii::$app->request->post('QueryRequest')){
            $data_id = $_POST['QueryRequest']['data_id'];
            $ezf_field_id = $_POST['QueryRequest']['ezf_field_id'];
        }
        $query_id = self::actionFindDataModel($ezf_field_id, $data_id);
        //VarDumper::dump($_POST, 10, true); Yii::$app->end();
        //update mode
        if($query_id['id']){
            //VarDumper::dump($query_id, 10, true); Yii::$app->end();
            $model = $this->findModel($query_id['id']);
            $modelQueryList = QueryList::find()->where('query_id = :query_id AND user_id_by= :user_id_by', [':query_id'=>$query_id['id'], ':user_id_by'=>Yii::$app->user->id])->one();
        }else{
            $model = new QueryRequest();
            $modelQueryList = new QueryList();
        }
        //VarDumper::dump($_POST, 10, true); Yii::$app->end();

        if($model->load(Yii::$app->request->post())) {

            //VarDumper::dump($model, 10, true); Yii::$app->end();
            if ($_POST['ezf_field_type'] == 7 || $_POST['ezf_field_type'] == 253) {
                // set Data Sql Format

                $explodeDate = explode('/', $_POST['SDDynamicModel'][$_POST['ezf_field_name']]);
                $formateDate = ($explodeDate[2]-543) . '-' . $explodeDate[1] . '-' . $explodeDate[0];
                $_POST['SDDynamicModel'][$_POST['ezf_field_name']] = $formateDate;

                $model->new_val = json_encode($_POST['SDDynamicModel']);

            }else if($_POST['ezf_field_type'] == 14 && false){
                if((isset($_FILES['SDDynamicModel']['name'][$_POST['ezf_field_name']]) && $_FILES['SDDynamicModel']['name'][$_POST['ezf_field_name']] !='' && is_array($_FILES['SDDynamicModel']['name'][$_POST['ezf_field_name']]))){
                    $fileItems = $_FILES['SDDynamicModel']['name'][$_POST['ezf_field_name']];
                    $fileType = $_FILES['SDDynamicModel']['type'][$_POST['ezf_field_name']];
                    $newFileItems = [];
                    $action = false;
                    foreach ($fileItems as $i => $fileItem) {
                        if($fileItem!=''){
                            $action = true;
                            $fileArr = explode('/', $fileType[$i]);
                            if (isset($fileArr[1])) {
                                $fileBg = $fileArr[1];

                                //$newFileName = $fileName;
                                $newFileName = $_POST['ezf_field_name'].'_'.($i+1).'_'.date("Ymd_His") . '.' . $fileBg;
                                //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                                $fullPath = Yii::$app->basePath . '/../backend/web/fileinput/' . $newFileName;
                                $fieldname = 'SDDynamicModel[' . $_POST['ezf_field_name'] . '][' . $i . ']';

                                $file = UploadedFile::getInstanceByName($fieldname);
                                $file->saveAs($fullPath);
                                $newFileItems[] = $newFileName;
                                //add file to db
                                $file_db = new \backend\modules\ezforms\models\FileUpload();
                                $file_db->tbid = $model->data_id;
                                $file_db->ezf_id = $model->ezf_id;
                                $file_db->ezf_field_id = $model->ezf_field_id;
                                $file_db->file_active = 0;
                                $file_db->mode = 2; //hidden
                                $file_db->file_name = $newFileName;
                                $file_db->file_name_old = $fileItem;
                                $file_db->target = isset($model->target_id)?$model->target_id:'';
                                $file_db->save();
                            }
                        }
                    }//end loop
                    if($action) {
                        $_POST['SDDynamicModel'][$_POST['ezf_field_name']] = $newFileItems;
                        $model->new_val = json_encode($_POST['SDDynamicModel']);
                    }
                }
            }else  if ($_POST['ezf_field_type'] == 24) {
                if (stristr($model->$_POST['ezf_field_name'], 'tmp.png') == TRUE) {
                    //set data Drawing
                    $fileArr = explode(',', $model->$_POST['ezf_field_name']);
                    $fileName = $fileArr[0];
                    $fileBg = $fileArr[1];
                    $newFileName = $fileName;
                    $newFileBg = $fileBg;
                    $nameEdit = false;
                    $bgEdit = false;
                    if (stristr($fileName, 'tmp.png') == TRUE) {
                        $nameEdit = true;
                        $newFileName = date("Ymd_His") . '.png';
                        @copy(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName, Yii::$app->basePath . '/../storage/web/drawing/data/' . $newFileName);
                        @unlink(Yii::$app->basePath . '/../backend/web/drawing/' . $fileName);
                    }
                    if (stristr($fileBg, 'tmp.png') == TRUE) {
                        $bgEdit = true;
                        $newFileBg = 'bg_' . date("Ymd_His") . '.png';
                        @copy(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg, Yii::$app->basePath . '/../storage/web/drawing/bg/' . $newFileBg);
                        @unlink(Yii::$app->basePath . '/../storage/web/drawing/' . $fileBg);
                    }

                    $_POST['SDDynamicModel'][$_POST['ezf_field_name']] = $newFileName . ',' . $newFileBg;
                    $model->new_val = json_encode($_POST['SDDynamicModel']);
                    /*
                    $modelTmp = EzformQuery::getDynamicFormById($table_name->ezf_table, $model->data_id);
                    if (isset($modelTmp['id'])) {
                        $fileArr = explode(',', $modelTmp[$value['ezf_field_name']]);
                        if (count($fileArr) > 1) {
                            $fileName = $fileArr[0];
                            $fileBg = $fileArr[1];
                            if ($nameEdit) {
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/data/' . $fileName);
                            }
                            if ($bgEdit) {
                                @unlink(Yii::$app->basePath . '/../storage/web/drawing/bg/' . $fileBg);
                            }
                        }
                    }
                    */
                }
            }else{
                $model->new_val = json_encode($_POST['SDDynamicModel']);
            }

            $model->status = 1;
            $model->user_create = Yii::$app->user->identity->id;
            $model->time_create = new Expression('NOW()');
            //VarDumper::dump($model, 10, true); Yii::$app->end();
            if ($model->save()) {
                //save query list
                $modelQueryList->query_id = $model->id;
                $modelQueryList->user_id_by = Yii::$app->user->identity->id;
                $modelQueryList->user_id_to = $_POST['queryUserTo'] ? $_POST['queryUserTo'] : $model->userkey;
                $modelQueryList->create_at = new Expression('NOW()');
                $modelQueryList->save();
                //end save
                return $this->redirect(['view', 'id'=>$model->id]);
            }
        }
        //เมื่อ update
        else if($query_id['id']){
            //VarDumper::dump($_POST, 10, true); Yii::$app->end();
            self::actionUpdate($query_id['id']);
        }else {
            $model->data_id = $data_id;
            //
            $modelValue = self::getModelValue($ezf_id, $data_id);
            //list of field form Ezform
            //$map_field = self::mapFields($ezf_id);
            $queryManager = QueryManager::find()->where('user_id = :user_id', [':user_id'=>Yii::$app->user->id])->one();
            //VarDumper::dump($map_field, 10, true);

            $modeQuery = self::actionSelectDataField($ezf_id, $ezf_field_id, $data_id, null);
            $ezf_field = EzformFields::find()->where(['ezf_field_id'=>$ezf_field_id])->one();

            $options_input['query_tools_render'] = 99;
            $options_input['query_tools_input'] = 1;
            //VarDumper::dump($options_input, 10, true); exit;

            //user from site
            $userTo = Yii::$app->db->createCommand('SELECT `user_id` AS id, CONCAT(`firstname`, " ", `lastname`) AS text FROM `user_profile` WHERE sitecode = :sitecode', [':sitecode' => Yii::$app->user->identity->userProfile->sitecode,])->queryAll();
            $userTo = ArrayHelper::map($userTo, 'id', 'text');

            return $this->renderAjax('create', [
                'model' => $model,// value from query_request
                'modelValue' => $modelValue,
                'modeQuery' => $modeQuery,
                'ezf_field' => $ezf_field,
                'options_input' =>$options_input,
                'queryManager' => $queryManager,
                'userTo' =>$userTo
            ]);
        }
    }

    public function mapFields($ezf_id){
        $ezf_field1 = EzformFields::find()->select("`ezf_field_id`, CONCAT(`ezf_field_label`, '(', `ezf_field_name`, ')') AS `ezf_field_label`, `ezf_field_type`")->where('(ezf_id = :ezf_id AND ezf_field_type <> 0 AND ezf_field_type <>250 AND ezf_field_type <>23 AND ezf_field_type <>25 AND ezf_field_type <>13 AND ezf_field_type <>15 AND ezf_field_type <>18)', [':ezf_id' => $ezf_id])->all();
        $ezf_field2 = EzformFields::find()->select("`ezf_field_id`, CONCAT(`ezf_field_label`, '(', `ezf_field_name`, ')') AS `ezf_field_label`, `ezf_field_type`")->where('(ezf_id = :ezf_id AND ezf_field_type <>23 AND ezf_field_type <>250 AND ezf_field_type <>25 AND  ezf_field_type <>13 AND ezf_field_type <>15 AND ezf_field_type <>18) AND (ezf_field_sub_id IS NOT NULL)', [':ezf_id' => $ezf_id])->all();
        $ezf_field = array_merge ($ezf_field1, $ezf_field2);
        //$map_field = ['ezf_field_label' => 'กรุณาเลือก Field ที่ต้องการแก้ไขข้อมูล'];
        //$map_field = array_merge($map_field, ArrayHelper::map($ezf_field1, 'ezf_field_id', 'ezf_field_label'));
        $map_field = ArrayHelper::map($ezf_field, 'ezf_field_id', 'ezf_field_label');
        /*
        foreach($ezf_field as $key => $val){
            if($val->ezf_field_type ==250){
                $ezf_field_label = $val->ezf_field_label;

            }
            else if($val->ezf_field_type ==251){
                $ezf_field[$key]->ezf_field_label = $ezf_field_label;
                //echo $ezf_field[$key]->ezf_field_label;
            }
        }
        */
        //VarDumper::dump($ezf_field[1]->ezf_field_label, 10, true);
        //Yii::$app->end();
        return $map_field;
    }

    public function getModelValue($ezf_id, $data_id){
        $ezform = EzformQuery::getFormTableName($ezf_id);
        try {
            //echo '0';
            $PIDFromtable = Yii::$app->db->createCommand("SELECT id, ptid as ptid, user_create FROM `" . $ezform->ezf_table . "` WHERE id = '$data_id';")->queryOne();
        } catch (\yii\db\Exception $e) {
            try {
                //echo '1';
                $PIDFromtable = Yii::$app->db->createCommand("SELECT id, target as ptid, user_create FROM `" . $ezform->ezf_table . "` WHERE id = '$data_id';")->queryOne();
            } catch (\yii\db\Exception $e) {
                try {
                    //echo '2';
                    $PIDFromtable = Yii::$app->db->createCommand("SELECT id, ptid_key as ptid, user_create FROM `" . $ezform->ezf_table . "` WHERE id = '$data_id';")->queryOne();
                } catch (\yii\db\Exception $e) {
                    try {
                        //echo '3';
                        $PIDFromtable = Yii::$app->db->createCommand("SELECT id, pid as ptid, user_create FROM `" . $ezform->ezf_table . "` WHERE id = '$data_id';")->queryOne();
                    } catch (\yii\db\Exception $e) {
                        //
                    }
                }
            }
        }


        //VarDumper::dump($PIDFromtable ,10, true);

        $modelValue = [];
        $ezform_comp = EzformQuery::checkIsTableComponent($ezf_id);
        if ($ezform_comp['special'] > 0) {
            $modelValue['target_id'] = $PIDFromtable['ptid'];
        } else {
            $modelValue['target_id'] = $PIDFromtable['id'];
        }
        $modelValue['user_create'] = $PIDFromtable['user_create'];

        return (object)$modelValue;
    }

    //old value json
    public function actionSelectDataField($ezf_id=null, $ezf_field_id=null, $data_id=null, $query_id=null){
        //$array =['text'=>123,];
        //$ezf_field_id = Yii::$app->request->post('ezf_field_id');
        //$data_id = Yii::$app->request->post('data_id');
        //$query_id = Yii::$app->request->post('query_id');
/*
        if($ezf_field_id) {
            $fields = Yii::$app->db->createCommand("SELECT ezf_id, ezf_field_id, ezf_field_name, ezf_field_label, ezf_field_sub_id, ezf_field_help, ezf_field_type, ezf_component FROM `ezform_fields` WHERE ezf_field_id = :ezf_field_id AND ezf_field_head_label <>1", [':ezf_field_id' => $ezf_field_id])->queryOne();
        }
        try {
            //find old data
            if ($data_id) {
                $ezform = EzformQuery::getFormTableName($fields['ezf_id']);
                $res = Yii::$app->db->createCommand("SELECT `" . $fields['ezf_field_name'] . "` AS text FROM `" . $ezform->ezf_table . "` WHERE id = :idx", [':idx' => $data_id])->queryOne();
            }
        }catch (\yii\db\Exception $e){

        }

        try {
            if ($query_id) {
                $res = Yii::$app->db->createCommand("SELECT `new_val` AS text FROM `query_request` WHERE id = :idx", [':idx' => $query_id])->queryOne();
            }
        }catch (\yii\db\Exception $e){

        }
*/
        //$modelForm = new QueryRequest();
        //$modelForm->new_val = $res['text'];

        $ezform = EzformQuery::getFormTableName($ezf_id);
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();

        $modelDynamic = new EzformDynamic($ezform->ezf_table);
        $model_table = $modelDynamic->find()->where('id = :id', [':id' => $data_id])->One();
        $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        $model_gen->attributes = $model_table->attributes;

        //VarDumper::dump($form, 10, true);

        /*
        return $this->renderAjax('_render_type', [
            'fields' => $fields,
            'fieldName' => 'QueryRequest[new_val]',
            'modelForm' => $modelForm,
        ]);
        */
        return $model_gen;
    }

    //old_val
    public function actionSelectDataOld($ezf_field_id=null, $data_id=null){
        $res = Yii::$app->db->createCommand("SELECT ezf_id, ezf_field_name FROM `ezform_fields` WHERE ezf_field_id = :ezf_field_name", [':ezf_field_name' => $ezf_field_id])->queryOne();
        $ezform = EzformQuery::getFormTableName($res['ezf_id']);
        $res = Yii::$app->db->createCommand("SELECT `".$res['ezf_field_name']."` AS text FROM `".$ezform->ezf_table."` WHERE id = :idx", [':idx' => $data_id])->queryOne();

        return $res;
    }

    public function actionFindDataModel($ezf_field_id=null, $data_id=null){
        //$array =['text'=>123,];
        //$ezf_field_id = Yii::$app->request->post('ezf_field_id');
        //$data_id = Yii::$app->request->post('data_id');
        $res = Yii::$app->db->createCommand("SELECT `id`, `new_val`, `note` FROM `query_request` WHERE data_id = :data_id AND ezf_field_id = :ezf_field_id AND status = 1", [':data_id' => $data_id, ':ezf_field_id' => $ezf_field_id])->queryOne();
        //return json_encode($res);
        return $res;
    }

    public function actionApprovedQuery($id){
        $model = $this->findModel($id);
        //QueryRequest']['note'] field ชั่วคราวเอาไว้เก็บไฟล์
        if (count($_POST)) {
            //VarDumper::dump($_POST, 10, true); Yii::$app->end();
            //save table reply data
            $modelReply = new QueryReply();
            $modelReply->query_id = $id;
            if(count($_FILES['QueryRequest']['name']['note']) && $_FILES['QueryRequest']['name']['note'][0]) {
                //print_r($_FILES['QueryRequest']['name']['note']); Yii::$app->end();
                $fileItems = $_FILES['QueryRequest']['name']['note'];
                $newFileItems = [];
                foreach ($fileItems as $i => $fileItem) {
                    if($fileItem!=''){
                        $fileArr = explode('.', $fileItem);
                        //$fileName = $fileArr[0];
                        if (isset($fileArr[1])) {
                            $fileBg = $fileArr[1]; //extension file
                            $newFileName = 'query_file_'.($i+1).'_'.date("Ymd_His") . '.' . $fileBg;
                            //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                            $fullPath = Yii::$app->basePath . '/../backend/web/uploads/query_tools/' . $newFileName;
                            $fieldname = "QueryRequest[note][" . $i . "]";

                            $file = UploadedFile::getInstanceByName($fieldname);
                            $file->saveAs($fullPath);
                            $newFileItems[] = $newFileName;
                        }
                    }
                }
                $modelReply->file_upload = implode(',', $newFileItems);
            }
            $modelReply->ezf_id = $model->ezf_id;
            $modelReply->target_id = $model->target_id;
            $modelReply->data_id = $model->data_id;
            $modelReply->ezf_field_id = $model->ezf_field_id;
            //$modelReply->old_val = $model->old_val;
            $modelReply->new_val = $model->new_val;
            $modelReply->note = $model->note;
            $modelReply->status = $_POST['QueryRequest']['status'] >= 2 ? $_POST['QueryRequest']['status'] : $model->status;
            $modelReply->user_create = $model->user_create;
            $modelReply->time_create = new Expression('NOW()');;
            $modelReply->reply_note = $_POST['QueryRequest']['approved_note'];
            $modelReply->save();
            //save table reply data

            if($_POST['QueryRequest']['status']>=2){
                $model->approved_note = $_POST['QueryRequest']['approved_note'];
                $model->approved_by = Yii::$app->user->identity->id;
                $model->time_update = new Expression('NOW()');
                $model->user_update = Yii::$app->user->identity->id;
                $model->status = $_POST['QueryRequest']['status'];
                //save file
                if($_POST['QueryRequest']['status']==2){
                    $model->new_changed =  $model->new_val;
                }
                //print_r(count($_FILES['QueryRequest']['name']['note']));
                //VarDumper::dump($_POST, 10, true); Yii::$app->end();
                if($model->save() && $_POST['QueryRequest']['status']==2) {
                    //process update value from table data
                    //new value
                    $ezf_field_id = $model->ezf_field_id;
                    $data_id = $model->data_id;
                    $res = Yii::$app->db->createCommand("SELECT ezf_id FROM `ezform_fields` WHERE ezf_field_id = :ezf_field_id", [':ezf_field_id' => $ezf_field_id])->queryOne();
                    $ezform = EzformQuery::getFormTableName($res['ezf_id']);

                    Yii::$app->db->createCommand()->update($ezform->ezf_table, json_decode($model->new_val, true), 'id = :idx', [':idx' => $data_id])->execute();
                    //process update value from table data

                    //save sql log
                    $sqlLog = new EzformSqlLog();
                    $sqlLog->data_id = $model->id;
                    $sqlLog->ezf_id = $_POST['ezf_id'];
                    $sqlLog->sql_log =  (Yii::$app->db->createCommand()->update($ezform->ezf_table, json_decode($model->new_val, true), 'id = :idx', [':idx' => $data_id])->rawSql);
                    $sqlLog->user_id = Yii::$app->user->id;
                    $sqlLog->create_date = new Expression('NOW()');
                    $sqlLog->rstat = $model->rstat;
                    $sqlLog->xsourcex = $model->xsourcex;
                    $sqlLog->save();
                    //

                    if ($_POST['ezf_field_type'] == 14) {
                        //copy original to file_active = -9 //hidden for query tools
                        Yii::$app->db->createCommand('INSERT INTO file_upload (tbid, target, file_name, file_name_old, file_active, ezf_id, ezf_field_id, created_by, created_at) SELECT tbid, target, file_name, file_name_old, -9, ezf_id, ezf_field_id, created_by, created_at FROM file_upload WHERE tbid = :data_id AND ezf_id = :ezf_id AND ezf_field_id = :ezf_field_id AND file_active <> 9 AND  file_active <> -9', [':data_id'=>$model->data_id, ':ezf_id'=>$model->ezf_id, ':ezf_field_id' => $model->ezf_field_id])->query();
                        //set  original to file_active = -10 //hidden for input
                        Yii::$app->db->createCommand()->update('file_upload', ['file_active'=>'-10'], 'tbid = :data_id AND ezf_id = :ezf_id AND ezf_field_id = :ezf_field_id AND file_active <> 9 AND  file_active <> -9', [':data_id'=>$model->data_id, ':ezf_id'=>$model->ezf_id, ':ezf_field_id' => $model->ezf_field_id])->execute();
                        //set new file_active = 0 //active
                        Yii::$app->db->createCommand()->update('file_upload', ['file_active'=>'0'], 'tbid = :data_id AND ezf_id = :ezf_id AND ezf_field_id = :ezf_field_id AND file_active =9', [':data_id'=>$model->data_id, ':ezf_id'=>$model->ezf_id, ':ezf_field_id' => $model->ezf_field_id])->execute();
                    }
                }
            }

            return $this->redirect(['/query-request/view', 'id' => $model->id]);
        } else {
            $queryManager = QueryManager::find()->where('user_id = :user_id', [':user_id'=>Yii::$app->user->id])->one();
            if(Yii::$app->user->can('adminsite')){
                $queryManager->user_id = 1;
            }
            else if(Yii::$app->user->can('administrator')){
                $queryManager->user_id = 1;
            }

            //load ezf field
            $ezform = EzformQuery::getFormTableName($model->ezf_id);
            $modelfield = EzformFields::find()
                ->where('ezf_id = :ezf_id', [':ezf_id' => $model->ezf_id])
                ->orderBy(['ezf_field_order' => SORT_ASC])
                ->all();
            //VarDumper::dump($model->oldAttributes['ezf_id'], 10, true); exit;

            $modelDynamic = new EzformDynamic($ezform->ezf_table);
            $model_table = $modelDynamic->find()->where('id = :id', [':id' => $model->data_id])->One();

            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $model_gen->attributes = $model_table->attributes;

            $ezf_field = EzformFields::find()->where(['ezf_field_id' => $model->ezf_field_id])->one();

            //new value
            $arr = json_decode($model->new_val);
            foreach ($arr as $key => $val) {
                $model_gen->$key = $val;
            }

            $model->note = null;
            $model->approved_note = null;

            return $this->renderAjax('_approved', [
                'model' => $model,
                'queryManager' => $queryManager,
                'model_gen' => $model_gen,
                'ezf_field' => $ezf_field,
            ]);
        }
    }

    /**
     * Updates an existing QueryRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($idUpdate=null)
    {
        if($idUpdate)
            $id = $idUpdate;
        else
            $id = Yii::$app->request->get('id');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $json =  json_encode($_POST['SDDynamicModel']);
            $model->new_val = $json;
            $model->status = 1;
            $model->user_update = Yii::$app->user->identity->id;
            $model->time_update = new Expression('NOW()');
            if($model->save()) {
                //save query list
                $modelQueryList = QueryList::find()->where('query_id = :query_id AND user_id_by= :user_id_by', [':query_id'=>$id, ':user_id_by'=>Yii::$app->user->id])->one();
                $modelQueryList->query_id = $model->id;
                $modelQueryList->user_id_by = Yii::$app->user->identity->id;
                $modelQueryList->user_id_to = Yii::$app->user->identity->id;
                $modelQueryList->create_at = new Expression('NOW()');
                $modelQueryList->save();
                //end save
                return $this->redirect(['/query-request/view', 'id' => $model->id]);
            }
        } else {
            $ezf_id = $model->ezf_id;
            $data_id = $model->data_id;
            $ezf_field_id = $model->ezf_field_id;
            //
            $modelValue = self::getModelValue($ezf_id, $data_id);
            //list of field form Ezform
            //$map_field = self::mapFields($ezf_id);

            //VarDumper::dump($modelValue, 10, true);
            //$query_id = self::actionFindDataModel($ezf_field_id, $data_id);
            $modeQuery = self::actionSelectDataField($ezf_id, $ezf_field_id, $data_id, null);
            $ezf_field = EzformFields::find()->where(['ezf_field_id'=>$ezf_field_id])->one();
            //VarDumper::dump($modeQuery, 10, true); exit;
            $arr = json_decode($model->new_val);
            foreach($arr as $key=>$val){
                $modeQuery->$key = $val;
            }

            //VarDumper::dump(, 10, true); exit;
            $options_input['query_tools_render'] = 99;
            $options_input['query_tools_input'] = 1;

            if($idUpdate) {
                echo $this->renderAjax('create', [
                    'model' => $model,// value from query_request
                    'modelValue' => $modelValue,
                    'modeQuery' => $modeQuery,
                    'ezf_field' => $ezf_field,
                    'options_input' => $options_input,
                    //'queryManager' => $queryManager,
                ]);
            }else{
                echo $this->render('create', [
                    'model' => $model,// value from query_request
                    'modelValue' => $modelValue,
                    'modeQuery' => $modeQuery,
                    'ezf_field' => $ezf_field,
                    'options_input' => $options_input,
                    //'queryManager' => $queryManager,
                ]);
            }
            return ;
        }
    }

    /**
     * Deletes an existing QueryRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        QueryList::deleteAll('query_id = :query_id AND user_id_by= :user_id_by', [':query_id'=>$id, ':user_id_by'=>Yii::$app->user->id]);
        return $this->redirect(['index']);
    }

    public function actionCloseTopic($id)
    {
        $model = QueryRequest::findOne($id);
        $model->open_status = 1;
        $model->save();
        return $this->redirect(['/query-request/view', 'id' => $model->id]);
    }

    /**
     * Finds the QueryRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return QueryRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QueryRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
