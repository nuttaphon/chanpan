<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class MapController extends Controller
{

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        return $this->render('index', [
            
        ]);
    }

    public function actionMap()
    {
	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	
	$sql = "SELECT tb_data_1.id, 
		    tb_data_1.title, 
		    tb_data_1.lat, 
		    tb_data_1.lng, 
		    tb_data_1.sitecode, 
		    tb_data_1.ptcode, 
		    tb_data_1.ptid_key, 
		    tb_data_1.create_date
	    FROM tb_data_1
	    WHERE tb_data_1.lat is not null AND tb_data_1.lat <> ''
	    ORDER BY create_date DESC
	    LIMIT 1000;";
	
	$dataSql = Yii::$app->db->createCommand($sql)->queryAll();
	
	$data = [];
	foreach ($dataSql as $key => $value) {
	    $data[] = [
		'icon' => Yii::getAlias('@web/images/icon/icon_17.png'),
		'lat' => $value['lat'],
		'lng' => $value['lng'],
		'title' => $value['title'],
		'content' => $value['sitecode'],
		'linkto' => '#',
	    ];
	}
	
	$result = [
		'status' => 'success',
		'message' => Yii::t('app', 'Data completed.'),
		'data' => $data,
	];
	
	return $result;
    }
}
