<?php

namespace backend\controllers;

use Yii;
use backend\models\Edat;
use backend\models\EdatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\View;
use yii\filters\VerbFilter;

/**
 * ViewDataController implements the CRUD actions for Tbdata model.
 */
class EdatController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tbdata models.
     * @return mixed
     */
    public function actionIndex()
    {
      //  return $this->renderContent('<hr><h3>ขออภัย กำลังปรุง...</h3>');
        
        $params=Yii::$app->request->queryParams;
        $ezfid=$params['id'];

        $ezform = Yii::$app->db->createCommand("SELECT * FROM ezform WHERE ezf_id ='".$ezfid."' ")->queryOne();
        $tablename = $ezform['ezf_table'];

        $ezfield = Yii::$app->db->createCommand("SELECT DISTINCT COLUMN_NAME FROM information_schema.columns WHERE table_name ='$tablename'  ")->query()->readAll();
        //print_r($ezfield);
        //exit;
        $searchModel = new EdatSearch();
        $searchModel->setTableName($tablename);
        $searchModel->setEZFid($ezfid);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ezfield' => $ezfield,
            'tablename' => $tablename
        ]);
    }
    public function actionFrequency()
    {   
    $paramsval=Yii::$app->request->post('val');
    $paramsindex=Yii::$app->request->post('val2');
    $tbname=Yii::$app->request->post('val3');
    $xsourex=Yii::$app->user->identity->userProfile->sitecode;
    //echo $params;
   
    $f_val = array(); 
    $i=0;
    $fre_val = array();
    //$fre_total = array();
    $fre_total = 0;
    
    foreach ($paramsval as $value) {
      # code...
      $key=$paramsindex[$i]; 
      
      $f_val[$key] = $value;
      $i++;
      $fre_val[]= Yii::$app->db->createCommand("SELECT COUNT(IFNUll($key,'')) AS countval, $key AS listval FROM $tbname where xsourcex = '$xsourex' GROUP BY ifnull($key,'') ")->queryAll();
    
      
    }
 
    $fre_total = Yii::$app->db->createCommand("SELECT COUNT(*) AS counttotal FROM $tbname where xsourcex= '$xsourex'  ")->queryOne();


   //  print_r($f_val);
    // print_r($paramsval);

   return $this->renderAjax('frequency', [

         'fre_total' => $fre_total,
         'fre_val' => $fre_val,
         'f_val' => $f_val
     ]);

  }
  public function actionSummary()
  {
    $varname = array(); 
    $i=0;
    $j=0;
    $varstat = array();
    $fre_val = array();
    $fre_total = 0;
    $f_val = array(); 
     
    $paramsval=Yii::$app->request->post('val');
    $paramsindex=Yii::$app->request->post('val2');
    $tbname=Yii::$app->request->post('val3');
    $xsourcex=Yii::$app->user->identity->userProfile->sitecode;
    
    foreach ($paramsval as $value) {
      $key=$paramsindex[$i]; 
      $varname[$key] = $value;
      $i++;
      $varstat[]= Yii::$app->db->createCommand("SELECT ROUND(MIN($key+0),2) AS minval, ROUND(MAX($key+0),2) AS maxval, ROUND(AVG($key+0),2) AS meanval, ROUND(STDDEV($key+0),2) AS sdval , median($key+0) as median_val FROM $tbname WHERE xsourcex = '$xsourcex'  ")->queryAll();
    }
    
     foreach ($paramsval as $value1) {
      $key1=$paramsindex[$j]; 
      $f_val[$key1] = $value1;
      
      $j++;
      $fre_val[]= Yii::$app->db->createCommand("SELECT COUNT(IFNUll($key1,'')) AS countval, $key1 AS listval FROM $tbname where xsourcex = '$xsourcex' GROUP BY IFNUll($key1,'') ")->queryAll();
    }
 
    $fre_total = Yii::$app->db->createCommand("SELECT COUNT(*) AS counttotal FROM $tbname where xsourcex= '$xsourcex'  ")->queryOne();

   
    return $this->renderAjax('summary',[
         'varstat' => $varstat,
         'varname' => $varname,
         'f_val' => $f_val,
         'fre_val' => $fre_val,
         'fre_total' => $fre_total
      ]);

  }
   public function actionCross()
  {
    $paramsval=Yii::$app->request->post('val');
    $paramsindex=Yii::$app->request->post('val2');
    $tbname=Yii::$app->request->post('val3');
    $ddval=Yii::$app->request->post('ddval'); 
    $sitecode=Yii::$app->user->identity->userProfile->sitecode;
   
  
    $f_val = array(); $i=0; $j=0;
    $fre_val = array();
    $cross_val = array();
    $column_total = array();

    $fre_total= Yii::$app->db->createCommand("SELECT COUNT(*) AS counttotal FROM $tbname where xsourcex = '$sitecode' ")->queryColumn();
  
    $ddval_list= Yii::$app->db->createCommand("SELECT ifnull($ddval,'') AS listvalue FROM $tbname where xsourcex = '$sitecode' GROUP BY ifnull($ddval,'') ")->queryColumn();
  
// \yii\helpers\VarDumper::dump($ddval_list, 10);
 //  exit;
   
   foreach ($paramsval as $value) {
      $key=$paramsindex[$i];
      $i++;
      $f_val[$key] = $value;
      $fre_val[]= Yii::$app->db->createCommand("SELECT COUNT(IFNUll($key,'')) AS countval, $key AS listval FROM $tbname where xsourcex = '$sitecode' GROUP BY ifnull($key,'') ")->queryAll();
        
   
     foreach($fre_val[$j++] as $val4){
      foreach ($ddval_list as $val2){
         $val5=$val4['listval'];
         $cross_val[] = Yii::$app->db->createCommand("SELECT COUNT(*) as c_val from $tbname where xsourcex = '$sitecode' and $ddval = '$val2' and $key = '$val5' ")->queryColumn();
         $column_total[] = Yii::$app->db->createCommand("SELECT COUNT(*) as c_total from $tbname where xsourcex = '$sitecode' and $ddval  ='$val2' ")->queryColumn();
         }
       }
   }

    return $this->renderAjax('cross',[
        'ddval' => $ddval,
        'ddval_list' => $ddval_list,
        'f_val' => $f_val,
        'fre_val' => $fre_val,
        'cross_val' => $cross_val,
        'fre_total' => $fre_total,
        'column_total' => $column_total
      ]);
  }
  
  
  public function actionList()
  {
    $paramsval=Yii::$app->request->post('val');
    $paramsindex=Yii::$app->request->post('val2');
    $tbname=Yii::$app->request->post('val3');
    $xsourex = Yii::$app->user->identity->userProfile->sitecode;
    
    $i=0;
    $f_val=array();
    $fre_val=array();
    foreach ($paramsval as $value) {
      # code...
      $key=$paramsindex[$i]; $i++;
      $f_val[$key] = $value;

      $fre_val[]= Yii::$app->db->createCommand("SELECT $key AS listval FROM $tbname where xsourcex = '$xsourex'")->queryColumn();
    }

    return $this->renderAjax('list',[
        'f_val' => $f_val,
        'fre_val' => $fre_val
      ]);
  }
 
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tbdata model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Edat();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tbdata model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tbdata model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tbdata model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tbdata the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Edat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
