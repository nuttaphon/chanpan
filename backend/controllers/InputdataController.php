<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\controllers;

use backend\models\EzformTarget;
use backend\models\QueryManager;
use backend\models\QueryRequest;
use backend\modules\ckdnet\classes\CkdnetFunc;
use backend\modules\component\models\EzformComponent;
use backend\modules\ezforms\components\EzformFunc;
use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\Ezform;
use backend\modules\ezforms\models\EzformChoice;
use backend\modules\ezforms\models\EzformDynamic;
use backend\modules\ezforms\models\EzformReply;
use backend\modules\ovcca\classes\OvccaFunc;
use common\lib\codeerror\helpers\GenMillisecTime;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use backend\models\InputDataSearch;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use Yii;
use backend\modules\ezforms\models\EzformFields;
use yii\web\Response;
use yii\web\UploadedFile;
use backend\modules\inv\models\TbdataAll;

/**
 * Description of TestController
 *
 * @author Gundam-macbook
 */
class InputdataController extends Controller
{
    public function beforeAction($action)
    {
        if($_POST['onmobile']) {
            $this->enableCsrfValidation = false;
        }

        if (parent::beforeAction($action)) {
            return true;
        } else {
            return false;
        }
    }

    //put your code here
    public function actionIndex($session=null)
    {


        if (stristr(Yii::$app->keyStorage->get('inputdata.lock-site', true), Yii::$app->user->identity->userProfile->sitecode) || Yii::$app->keyStorage->get('inputdata.lock-site', true) == 'all') {
            //String exists within $string
            return $this->render('lock-page');
        }else{
            //String does not exist
        }

        //$model = Yii::$app->db->createCommand('SELECT * FROM kongvut_test')->queryAll();
        //$model = Test::findBySql('SELECT * FROM kongvut_test')->all();
        $searchModel = new InputDataSearch();
        $dataProvider = $searchModel->search();
        //$dataProvider -> waiting;
        //echo Yii::$app->runAction('/sign-in/profile');
        //echo Yii::$app->runAction('/sign-in/account');
        $ezfAllCanUse = self::ezfCanUse();

        if($session=='') {
            unset($_SESSION['inputTarget']);
            unset($_SESSION['ezf_id']);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'ezfAllCanUse' => $ezfAllCanUse,
        ]);
    }

    public function actionWatchVideo()
    {
            return $this->renderPartial('watch-video', [
                'type' => $_GET['type']
        ]);
    }

    public static function ezfCanUse()
    {
        $publicForm = Yii::$app->db->createCommand('SELECT a.ezf_id FROM ezform as a WHERE  a.status <> 3 AND a.`shared` = 1 ORDER BY ezf_id')->queryColumn();
        $assignForm = Yii::$app->db->createCommand('SELECT a.ezf_id FROM ezform as a INNER JOIN ezform_assign AS b ON a.ezf_id = b.ezf_id WHERE  a.status <> 3 AND b.user_id = :user_id ORDER BY ezf_id', [':user_id' => Yii::$app->user->id])->queryColumn();
        $publicForm = array_flip($publicForm);
        $assignForm = array_flip($assignForm);
        $result = array_merge($assignForm, $publicForm);

        return count($result);
    }

    public static function actionViewimg($img)
    {
        return \backend\modules\ovcca\classes\OvccaFunc::getIframeFix($img);
    }

    public function actionFavoriteForm()
    {
        if (Yii::$app->request->get('action') == 'remove') {
            //$data_div = Yii::$app->request->post('data_div');
            $data_ezf_id = Yii::$app->request->get('ezf_id');

            Yii::$app->db->createCommand("DELETE FROM ezform_favorite WHERE userid = :userid AND ezf_id = :ezf_id", [':userid' => Yii::$app->user->id, ':ezf_id' => $data_ezf_id])->execute();
            //echo '<i data-div="'.$data_div.'" data-ezf_id="'.$data_ezf_id.'" data-action="add" class="fa fa-star-o fa-2x clickable-fav"></i>';

        } else if (Yii::$app->request->get('action') == 'add') {
            $userid = Yii::$app->user->id;
            //$data_div = Yii::$app->request->get('data_div');
            $data_ezf_id = Yii::$app->request->get('ezf_id');

            Yii::$app->db->createCommand("REPLACE INTO ezform_favorite (`ezf_id`, `userid`, `forder`) VALUES ('$data_ezf_id', '$userid', '999');")->execute();
            Yii::$app->db->createCommand("set @i:=0; update ezform_favorite set forder=10*(@i:=@i+1) WHERE userid = :userid order by forder+0;", [':userid' => $userid])->execute();
            //echo '<i data-div="'.$data_div.'" data-ezf_id="'.$data_ezf_id.'" data-action="remove" style="color: orangered;" class="fa fa-star fa-2x clickable-fav"></i>';

        }
        $publicForm = Yii::$app->db->createCommand('SELECT a.ezf_id, a.ezf_name FROM ezform as a WHERE  a.status <> 3 AND a.`shared` = 1 ORDER BY ezf_id')->queryAll();
        $assignForm = Yii::$app->db->createCommand('SELECT a.ezf_id, a.ezf_name FROM ezform as a INNER JOIN ezform_assign AS b ON a.ezf_id = b.ezf_id WHERE  a.status <> 3 AND b.user_id = :user_id ORDER BY ezf_id', [':user_id' => Yii::$app->user->id])->queryAll();
        $siteForm = Yii::$app->db->createCommand('SELECT a.ezf_id, a.ezf_name FROM ezform as a WHERE  a.status <> 3 AND a.shared = 3 AND a.xsourcex=:xsourcex ORDER BY ezf_id', [':xsourcex' => Yii::$app->user->identity->userProfile->sitecode])->queryAll();
        $privateForm = Yii::$app->db->createCommand('SELECT a.ezf_id, a.ezf_name FROM ezform as a WHERE a.status <> 3 and a.user_create=:user_id ORDER BY ezf_id', [':user_id' => Yii::$app->user->id])->queryAll();
        $favForm = Yii::$app->db->createCommand('SELECT a.ezf_id, a.ezf_name FROM ezform as a INNER JOIN ezform_favorite AS b ON a.ezf_id = b.ezf_id WHERE b.userid = :user_id ORDER BY b.forder DESC', [':user_id' => Yii::$app->user->id])->queryAll();

        return $this->renderAjax('favorite', [
            'publicForm' => $publicForm,
            'assignForm' => $assignForm,
            'siteForm' => $siteForm,
            'privateForm' => $privateForm,
            'favForm' => $favForm,
        ]);

    }

    public function actionFavoriteOrder()
    {
        if (Yii::$app->request->get('action') == 'up') {
            //-15
            Yii::$app->db->createCommand("UPDATE ezform_favorite SET forder=forder+15 WHERE userid = :userid AND ezf_id = :ezf_id", [':userid' => Yii::$app->request->get('userid'), ':ezf_id' => Yii::$app->request->get('ezfid')])->execute();

        } else if (Yii::$app->request->get('action') == 'down') {
            //+15
            Yii::$app->db->createCommand("UPDATE ezform_favorite SET forder=forder-15 WHERE userid = :userid AND ezf_id = :ezf_id", [':userid' => Yii::$app->request->get('userid'), ':ezf_id' => Yii::$app->request->get('ezfid')])->execute();
        }
        Yii::$app->db->createCommand("set @i:=0; update ezform_favorite set forder=10*(@i:=@i+1) WHERE userid = :userid order by forder+0;", [':userid' => Yii::$app->request->get('userid')])->execute();

        $searchModel = new InputDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $ezfAllCanUse = self::ezfCanUse();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'ezfAllCanUse' => $ezfAllCanUse,
        ]);
    }

    public static function checkCid($pid)
    {
        if (strlen($pid) != 13) return false;
        for ($i = 0, $sum = 0; $i < 12; $i++)
            $sum += (int)($pid{$i}) * (13 - $i);
        if ((11 - ($sum % 11)) % 10 == (int)($pid{12}))
            return true;
        return false;
    }

    public function actionRedirectPage(){
        if(Yii::$app->request->get('comp_id_target')) {
            $comp_id_target = Yii::$app->request->get('comp_id_target');
            //$ezf_id
            $comp = EzformComponent::find()
                ->where(['comp_id' => $comp_id_target])
                ->andWhere('comp_id <> 100000 AND comp_id <> 100001')
                ->one();

            $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id' => $comp->ezf_id])->queryOne();
            $comp_id = $ezform['comp_id_target'];

            if ($comp_id) {
                $this->redirect(Url::to(['step2', 'comp_id_target' => $comp_id, 'ezf_id' => $comp->ezf_id]));
            } else {
                $this->redirect(Url::to(['step4', 'target' => 'skip', 'ezf_id' => $comp->ezf_id]));
            }
        }else if(Yii::$app->request->get('ezf_id') AND Yii::$app->request->get('dataid')){
            $ezf_id = Yii::$app->request->get('ezf_id');
            $dataid = Yii::$app->request->get('dataid');

            $ezform = Yii::$app->db->createCommand("SELECT comp_id_target FROM ezform WHERE ezf_id = :ezf_id;", [':ezf_id' => $ezf_id])->queryOne();
            $comp_id = $ezform['comp_id_target'];

            $dataTarget = EzformQuery::getTargetFormEzf($ezf_id, $dataid);

            if ($comp_id) {
                $this->redirect(Url::to(['step4', 'comp_id_target' => $comp_id, 'ezf_id' => $ezf_id,'target'=>base64_encode($dataTarget['ptid']), 'dataid' => $dataid, 'rurl' =>Yii::$app->request->get('rurl')]));
            } else {
                $this->redirect(Url::to(['step4', 'target' => 'skip', 'ezf_id' => $ezf_id, 'dataid' => $dataid,  'rurl' =>Yii::$app->request->get('rurl')]));
            }
        }
    }

    public static function actionRegisterFromComponent($comp_id, $cid){

        $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id])->queryOne();
        $out = ['results' => ['id' => '', 'text' => '']];
        $modelComp = self::actionSpecialTarget1($ezform_comp);

        $res = self::taskSpecial1($out, $cid, $modelComp['sqlSearch'], $ezform_comp['ezf_id'], $ezform_comp);
        //VarDumper::dump($res,10,true);

        if($res['results'][0]['id']=='-990'){
            $queryParams['task'] = 'add-new';
            $queryParams['comp_id'] = $comp_id;
            $queryParams['target'] = $cid;
            //VarDumper::dump($res,10,true);
        }else if($res['results'][0]['id']=='-991'){
            $queryParams['task'] = 'add-from-site';
            $queryParams['comp_id'] = $comp_id;
            $queryParams['target'] = $cid;
        }

        //if foreigner
        $cid_k = substr($cid, 0, 1) =='0' ? true : false;
        if($cid_k){
            $queryParams['mode'] = 'gen-foreigner';
        }

        return $queryParams;

    }

    public static function actionLookupTarget()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        $string_q = Yii::$app->request->post('q');
        $string_decode = base64_decode(Yii::$app->request->post('search')); //sql
        $ezf_id = Yii::$app->request->post('ezf_id');
        $ezf_comp = EzformComponent::find()
            ->where(['comp_id' => Yii::$app->request->post('ezf_comp_id')])
            ->one();

        //หากมีการกำหนด special component
        if ($ezf_comp['special'] == 1) {
            $out = self::taskSpecial1($out, $string_q, $string_decode, $ezf_id, $ezf_comp);
        } else {
            //ถ้าไม่มีการกำหนด ให้ค้นหาแบบธรรมดา
            $ortQuery = Yii::$app->db->createCommand(str_replace('$q', $string_q, $string_decode));
            $data = $ortQuery->queryAll();
            $out['results'] = array_values($data);

            if (!$ortQuery->query()->count())
                $out['results'] = [['id' => '-992', 'text' => 'ไม่พบคำที่ค้นหา! ลองค้นคำอื่นๆ (คลิกที่นี่ถ้าต้องการเพิ่มเป้าหมายใหม่)']];
        }

        echo json_encode($out);
        return;
    }

    //สำหรับกำหนดเป้าหมาย field, table (พิเศษ)
    public static function actionSpecialTarget1($ezform_comp)
    {
        $ezf_table = EzformQuery::getTablenameFromComponent($ezform_comp['comp_id']);

        // array("hsitecode", 'hptcode', "name", "surname");
        //$arr = explode(',', $ezform_comp['field_id_desc']);
        $concatSearch ='';
        $modelComp = [];
        $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id in(".$ezform_comp['field_id_desc'].");")->queryAll();
        foreach ($ezf_fields AS $val) {
            $modelComp['arr_comp_desc_field_name'][] = $val['ezf_field_name'];
            $concatSearch .= "IFNULL(`".$val['ezf_field_name']."`, ''), ' ', ";
        }
        $concatSearch = substr($concatSearch, 0, -2);

        $sqlSearch = "select ptid as id, concat(".$concatSearch.") as text FROM `".($ezf_table->ezf_table)."` WHERE 1";

        //VarDumper::dump($sqlSearch); 

        $modelComp['comp_id'] = $ezform_comp['comp_id'];
        $modelComp['concatSearch'] = $concatSearch;
        $modelComp['tagMultiple'] = false;
        $modelComp['sqlSearch'] = $sqlSearch;
        $modelComp['special'] = true;
        //ใช้แสดงชื่อเป้าหมาย ขั้นตอนที่ 3
        $modelComp['ezf_table_comp'] = $ezf_table->ezf_table;

        $modelComp['comp_key_name'] = 'ptid';
        return $modelComp;
    }

    public static function setFieldSearch($ezf_comp){
        //
        //$arr = explode(',', $ezform_comp['field_search']);
        try {
            $concatSearch = '';
            $concatLabel = '';
            $modelComp = [];
            $listIdField = $ezf_comp['field_search'];
            if ($ezf_comp['field_search_cid']){
                $listIdField = $ezf_comp['field_search'] . ',' . $ezf_comp['field_search_cid'];
	    }
	    
            $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_id, ezf_field_name, ezf_field_label FROM ezform_fields WHERE ezf_field_id in(" . $listIdField . ");")->queryAll();
            $ezf_fields = ArrayHelper::index($ezf_fields, 'ezf_field_id');
            foreach ($ezf_fields AS $key => $val) {
                if ($key <> $ezf_comp['field_search_cid']) {
                    $modelComp['arr_comp_field_search'][] = $val['ezf_field_name'];
                    $concatSearch .= "IFNULL(`" . $val['ezf_field_name'] . "`, ''), ' ', ";
                    $concatLabel .= $val['ezf_field_label'] . ', ';
                }
            }

            $concatSearch = substr($concatSearch, 0, -2);
            $modelComp['concat_search'] = $concatSearch;
            $modelComp['concat_label'] = $concatLabel . $ezf_fields[$ezf_comp['field_search_cid']]['ezf_field_label'];
            return $modelComp;
        }catch (\yii\base\Exception $ex){
             echo 'กำหนด Field ค้นที่ที่ Component <code>'. $ezf_comp['comp_name'].'</code> ไม่ถูกต้อง กรุณาตั้งค่าใหม่';
             exit;
             return;
        }
    }

    //copy patient from cascap
    public function copyPatientCascap($ezf_id, $cid, $ezf_table){

        //ค้นข้อมูล
        //หาข้อมูลที่สมบูรณ์ก่อน
        $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `tb_data_1` WHERE  `cid` LIKE '" . $cid . "' AND (error IS NULL OR error = '') AND rstat <> 3 AND rstat IS NOT NULL ORDER BY update_date DESC;")->queryOne();
        //หาไม่เจอเอาข้อมูลที่แรกให้
        if(!$model['id']) {
            $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `tb_data_1` WHERE  `cid` LIKE '" . $cid . "' AND id=ptid AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
        }

        //หาไม่เจอเลยเอาข้อมูลที่พอมีมาให้
        if(!$model['id']) {
            $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `tb_data_1` WHERE  `cid` LIKE '" . $cid . "' AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
        }

        if($model['id']) {
            Yii::$app->db->createCommand()->insert($ezf_table, $model)->execute();

            //save EMR
            $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `ezform_target` WHERE  data_id=:data_id AND ezf_id=:ezf_id;", [':data_id' => $model['id'], ':ezf_id' => $ezf_id])->queryOne();
            Yii::$app->db->createCommand()->insert('ezform_target', $model)->execute();
            //
        }

        return true;
    }

    //เป้าหมาย (พิเศษ)
    public static function taskSpecial1($out, $string_q, $string_decode, $ezf_id, $ezf_comp)
    {
        if (is_numeric($string_q) AND strlen($string_q) < 13 AND strlen($string_q) > 10) {
            //ถ้าเป็นตัวเลข และน้อยกว่า 13 หลัก
            $out['results'] = [['id' => '-999', 'text' => 'เลขบัตรประชาชนยังไม่ครบ 13 หลัก']];
        } else if (is_numeric($string_q) AND strlen($string_q) > 13) {
            //ถ้าเป็นตัวเลขมากกว่า 13 หลัก
            $out['results'] = [['id' => '-999', 'text' => 'เลขบัตรประชาชนมากกว่า 13 หลัก']];
        } else if (is_numeric($string_q) AND strlen($string_q) == 13) {
            //ดูดข้อมูล
            if(Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org'){
                $ezform = Ezform::find()->select('comp_id_target')->where(['ezf_id'=>$ezf_id])->one();
                $ezform = EzformQuery::getTablenameFromComponent($ezform->comp_id_target);
                $res = Yii::$app->db->createCommand("SELECT id FROM `".($ezform->ezf_table)."` WHERE rstat <> 3 AND `cid` LIKE '".$string_q."';")->queryOne();
                //print_r($res); exit;
                //ถ้าหาไม่เจอ
                if(!$res['id']){
                    //ดูดข้อมูล
                    self::copyPatientCascap($ezf_id, $string_q, ($ezform->ezf_table));
                }
            }
            $cid_k = substr($string_q, 0, 1) =='0' ? false : true;
            //ถ้าเป็นตัวเลข 13 หลักแล้ว (ทั้งต่างด้าวที่ขึ้นต้นด้วย 0)
            if (!self::checkCid($string_q) && $cid_k) {
                //กรณีเลขบัตรไม่ถูกต้อง
                $out['results'] = [['id' => '-999', 'text' => 'เลขบัตรประชาชน 13 หลักไม่ถูกต้อง']];
            }
            else {
                $ezfField = EzformQuery::getFieldNameByID($ezf_comp['field_search_cid']);
                //กรณีเลขบัตรถูกต้อง
                //ptid=ptid_key คือเลือกเอาเฉพาะตัวมีค่าเท่ากัน
                $string_decode_all = $string_decode . " AND rstat <> 3 AND `".($ezfField->ezf_field_name)."` LIKE '" . $string_q . "' AND id=ptid";
                $ortQuery_ptid = \Yii::$app->db->createCommand($string_decode_all);

                //หาทั้งหมด
                $string_decode_all = $string_decode . " AND rstat <> 3 AND `".($ezfField->ezf_field_name)."` LIKE '" . $string_q . "'";
                $ortQuery_all = \Yii::$app->db->createCommand($string_decode_all);

                $string_decode .= " AND rstat <> 3 AND `".($ezfField->ezf_field_name)."` LIKE '" . $string_q . "' AND xsourcex ='" . Yii::$app->user->identity->userProfile->sitecode . "'";
                $ortQuery = \Yii::$app->db->createCommand($string_decode);

                //เมื่อพบข้อมูลที่ site ตัวเอง
                if ($ortQuery->query()->count()) {
                    $data = $ortQuery->queryAll();
                    $out['results'] = array_values($data);
                } //เจอข้อมูลที่ site อื่นๆ ต้องการเพิ่มใหม่
                else if ($ortQuery_ptid->query()->count() || $ortQuery_all->query()->count()) {
                    $out['results'] = [['id' => '-991', 'text' => 'พบข้อมูลที่หน่วยงานอื่น ต้องการลงทะเบียน "' . $string_q . '" นี้ในหน่วยงานของท่าน หรือไม่?']];
                } //เมื่อไม่พบข้อมูลเลย ให้เพิ่มใหม่
                else if (!$ortQuery_ptid->query()->count() && !$ortQuery_all->query()->count()) {
                    $out['results'] = [['id' => '-990', 'text' => 'เพิ่มข้อมูลใหม่สำหรับ "' . $string_q . '" ในหน่วยงานของท่าน']];
                }

            }
        } else {
            //for string
            $string_q = str_replace(' ', ",", trim($string_q));
            $string_q = explode(',', $string_q);
            $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
            $fieldSearch = self::setFieldSearch($ezf_comp);
            $countLenStr = count($string_q);
            if ($countLenStr == 1) {
                //fullcode ค้นได้เฉพาะฟอร์ม CCA-02
                if(($ezf_id == '1437619524091524800' || Yii::$app->user->can('datamanager')) && strlen($string_q[0]) == 10) {
                    //เตรียมข้อมูล
                    $modelComp = self::actionSpecialTarget1($ezf_comp);
                    $sqlSearch = "select ptid as id, concat(".$modelComp['concatSearch'].") as text, hsitecode, cid, `name`, surname FROM `".($modelComp['ezf_table_comp'])."` WHERE 1";

                    //หาด้วย codefull (hsitecode + hptcode)
                    //หา site ตัวเองก่อน
                    $string_decodex = " AND rstat <> 3 AND CONCAT(`hsitecode`, `hptcode`) LIKE '".$string_q[0]."' AND hsitecode <> '' AND hptcode <> '' ORDER BY `hptcode` DESC LIMIT 0,1";
                    $ortQuery = \Yii::$app->db->createCommand($sqlSearch.$string_decodex)->queryAll();
                    if ($ortQuery[0]['cid']) {
                        $string_decodex = " AND rstat <> 3 AND `cid` LIKE '" . $ortQuery[0]['cid'] . "' AND xsourcex = :xsourcex";
                        $ortQuery_site = \Yii::$app->db->createCommand($sqlSearch." AND xsourcex = :xsourcex".$string_decodex, [':xsourcex'=>$xsourcex])->queryAll();
                        if ($ortQuery_site[0]['cid']) {
                            $out['results'] = array_values($ortQuery_site);
                            return $out;
                        }else{
                            $string_decode_all = $sqlSearch . " AND rstat <> 3 AND `cid` LIKE '" . $ortQuery[0]['cid'] . "' AND id=ptid";
                            $ortQuery_all = \Yii::$app->db->createCommand($string_decode_all)->queryAll();

                            if(!$ortQuery_all[0]['cid']) {
                                //หาทั้งหมด
                                $string_decode_all = $sqlSearch . " AND rstat <> 3 AND `cid` LIKE '" . $ortQuery[0]['cid'] . "'";
                                $ortQuery_all = \Yii::$app->db->createCommand($string_decode_all)->queryAll();
                            }

                            if ($ortQuery_all[0]['cid']) {
                                $out['results'] = [['id' => '-991', 'text' => 'พบข้อมูล '.$ortQuery_all[0]['name'].' '.$ortQuery_all[0]['surname'].' ที่ Site อื่น ลงทะเบียน "' . $ortQuery[0]['cid'] . '" นี้ในหน่วยงานของท่าน']];
                                return $out;
                            }
                        }
                    }else{
                        $string_decodex = " AND rstat <> 3 AND CONCAT(`hsitecode`, `hptcode`) LIKE '".$string_q[0]."' AND hsitecode <> '' AND hptcode <> '' ORDER BY `hptcode` DESC LIMIT 0,1";
                        $ortQuery = \Yii::$app->db->createCommand($sqlSearch.$string_decodex)->queryOne();
                        if ($ortQuery['cid']) {
                            $out['results'] = [['id' => '-991', 'text' => 'พบข้อมูล '.$ortQuery['name'].' '.$ortQuery['surname'].' ที่หน่วยงานอื่น ลงทะเบียน "' . $ortQuery['cid'] . '" นี้ในหน่วยงานของท่าน']];
                            return $out;
                        }
                    }

                }//data manager

                //หาด้วย hncode
                $frontendDomain = Yii::$app->keyStorage->get('frontend.domain');
                if ($frontendDomain == 'cascap.in.th' || $frontendDomain == 'dpmcloud.org') {
                    $ezform = EzformQuery::getTablenameFromComponent($ezf_comp['comp_id']);
                    $getColumnName = EzformQuery::getColumnName($ezform->ezf_table, 'hncode');
                    if($getColumnName=='hncode') {
                        $string_decodex = " AND rstat <> 3 AND (`hptcode`  LIKE '%" . ($string_q[0]) . "%' OR LOWER(`hncode`) LIKE LOWER('%" . ($string_q[0]) . "%')) AND hncode IS NOT NULL AND hncode <> '' ORDER BY `hptcode` ASC LIMIT 0,20";
                        $ortQuery = \Yii::$app->db->createCommand($string_decode . " AND xsourcex = :xsourcex" . $string_decodex, [':xsourcex' => $xsourcex])->queryAll();
                        if ($ortQuery[0]['id']) {
                            $out['results'] = array_values($ortQuery);
                            return $out;
                        }
                    }
                }


                //ค้นตาม field_search ที่กำหนดไว้ใน component
                $string_decode .= " AND rstat <> 3 AND xsourcex = '" . $xsourcex . "' AND (CONCAT(".$fieldSearch['concat_search'].") LIKE '%" . ($string_q[0]) . "%') ORDER BY `hptcode` DESC LIMIT 0,20";

            }
            //ค้นกรณีมีการเว้นวรรคคำ 2 คำ ขึ้นไป
            else if ($countLenStr >= 2){
                for($i=0;$i<$countLenStr;$i++){
                    $strSearch = "CONCAT(".$fieldSearch['concat_search'].") LIKE '%" . ($string_q[$i]) . "%' AND ";
                }
                $strSearch = substr($strSearch, 0, -5);
                $string_decode .= " AND rstat <> 3 AND xsourcex = '" . $xsourcex . "' AND (".$strSearch.") ORDER BY `hptcode` DESC LIMIT 0,20";
            }

            if($countLenStr <=2) {
                $ortQuery = \Yii::$app->db->createCommand($string_decode);
                //echo ($ortQuery->rawSql);
                //Yii::$app->end();
                $data = $ortQuery->queryAll();
                $out['results'] = array_values($data);

                if (!$ortQuery->query()->count())
                    $out['results'] = [['id' => '-999', 'text' => 'ไม่พบคำที่ค้นหา! ลองค้นคำอื่นๆ หรือค้นหาจากเลข 13 หลัก']];
            }else{
                $out['results'] = [['id' => '-999', 'text' => 'ไม่พบคำที่ค้นหา! ลองค้นคำอื่นๆ หรือค้นหาจากเลข 13 หลัก']];
            }
        }
        return $out;
    }

    public function actionInsertRunPid(){
        $params['comp_id'] = Yii::$app->request->get('comp_id');
        $params['target'] = self::actionGenForeigner($params['comp_id']); //cid
        $params['mode'] = 'gen-foreigner';

        return self::actionInsertSpecial($params);
    }

    public static function actionInsertSpecial($params=null)
    {
        //กำหนดค่าให้เลือกเป้าหมายเดิมหลัง insert ใหม่
        $session = Yii::$app->session;

        if($params){
            //
        }else{
            $params = [
                'comp_id' => Yii::$app->request->post('comp_id'),
                'target' => Yii::$app->request->post('target'), //cid
                'task' => Yii::$app->request->post('task'),
                'mode' => Yii::$app->request->post('mode'),
            ];
        }

        //final re check (task add new or add new from other site ?)
        $paramsx = self::actionRegisterFromComponent($params['comp_id'], $params['target']);
        $params['task'] = $paramsx['task'];

        $comp = EzformComponent::find()
            ->select('ezf_id, comp_id, field_search_cid')
            ->where(['comp_id' => $params['comp_id']])
            ->one();
        $ezform = Ezform::find()
            ->select('ezf_table')
            ->where(['ezf_id' => $comp->ezf_id])
            ->one();

        $result_success = [
            'status' => 'success',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'บันทึกข้อมูลสำเร็จ.'),
        ];
        $result_error = [
            'status' => 'error',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ไม่สามารถดำเนินการได้ โปรดลองใหม่ภายหลัง.'),
        ];

        $cid = preg_replace("/[^0-9]/", "", $params['target']);
        if(!self::checkCid($cid) && $params['mode'] != 'gen-foreigner'){
            $result_error['message'] = '<strong><i class="glyphicon glyphicon-remove-sign"></i> Error!</strong> ' . Yii::t('app', 'ข้อมูลเลขบัตรประชาชนไม่ถูกต้อง.');
            return json_encode($result_error);
        }

        //set field cid
        $fieldCid = EzformQuery::getFieldNameByID($comp->field_search_cid);
        $fieldCid = $fieldCid->ezf_field_name;

        if ($params['task'] == 'add-new') {
            //type of special
            $id = GenMillisecTime::getMillisecTime();
            $hsitecode = Yii::$app->user->identity->userProfile->sitecode;
            $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
            $user_create = Yii::$app->user->id;
            $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `".($ezform->ezf_table)."` WHERE xsourcex = :xsourcex;", [':xsourcex'=>$hsitecode])->queryOne();
            $pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
            $ptcodefull = $hsitecode.$pid;

            Yii::$app->db->createCommand()->insert($ezform->ezf_table, [
                'id' =>$id,
                'ptid' =>$id,
                'xsourcex' => $hsitecode,
                'xdepartmentx' => $xdepartmentx,
                'target' => $id,
                'user_create' => $user_create,
                'create_date' => new Expression('NOW()'),
                'sitecode' => $hsitecode,
                'ptcode' =>$pid,
                'ptcodefull' =>$ptcodefull,
                'hsitecode' =>$hsitecode,
                'hptcode' =>$pid,
                $fieldCid => $cid,
                'rstat' => '0',
            ])->execute();

            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $comp->ezf_id;
            $model->data_id = $id;
            $model->target_id = $id;
            $model->comp_id = $comp->comp_id;
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $hsitecode;
            $model->save();
            //

            //set session
            $session->set('ezf_id', $comp->ezf_id);
            $session->set('inputTarget', base64_encode($id));//as base64

            //$url_redirect = Url::to(['/inputdata/step4', 'ezf_id' =>$comp->ezf_id, 'comp_id_target'=>$comp->comp_id, 'target' => base64_encode($id), 'dataid' => $id]);
            //self::redirect($url_redirect, 302);

        } else if ($params['task'] == 'add-from-site') {
            //type of special

            $id = GenMillisecTime::getMillisecTime();
            $hsitecode = Yii::$app->user->identity->userProfile->sitecode;
            $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
            $user_create = Yii::$app->user->id;
            $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `".($ezform->ezf_table)."` WHERE xsourcex = :xsourcex;", [':xsourcex'=>$hsitecode])->queryOne();
            $pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
            //หาข้อมูลที่สมบูรณ์ก่อน
            $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND (error IS NULL OR error = '') AND rstat <> 3 AND rstat IS NOT NULL ORDER BY update_date DESC;")->queryOne();
            //หาไม่เจอเอาข้อมูลที่แรกให้
            if(!$model['id'])  $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND id=ptid AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();
            //หาไม่เจอเอาข้อมูลที่พบมาให้
            if(!$model['id'])  $model = Yii::$app->db->createCommand("SELECT * FROM `".($ezform->ezf_table)."` WHERE  `".$fieldCid."` LIKE '" . $cid . "' AND rstat <> 3 AND rstat IS NOT NULL;")->queryOne();

            //set values
            $model['id'] = $id;
            $model['xsourcex'] = $hsitecode;
            $model['xdepartmentx'] = $xdepartmentx;
            $model['user_create'] = $user_create;
            $model['create_date'] = new Expression('NOW()');
            $model['update_date'] = new Expression('NOW()');
            if($model['hncode']) $model['hncode'] = null;
            $model['tccbot'] = 0;
            $model['hsitecode'] = $hsitecode;
            $model['hptcode'] = $pid;
            $model['rstat'] = '0';
            $ptid = $model['ptid'];
            $model['id'] = $id;

            Yii::$app->db->createCommand()->insert($ezform->ezf_table, $model)->execute();

            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $comp->ezf_id;
            $model->data_id = $id;
            $model->target_id = $ptid;
            $model->comp_id = $comp->comp_id;
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $hsitecode;
            $model->save();
            //

            //set session
            $session->set('ezf_id', $comp->ezf_id);
            $session->set('inputTarget', base64_encode($ptid));//as base64
        }

        if(Yii::$app->request->get('redirect')=='url')
            return self::redirect(Url::to(['/inputdata/redirect-page', 'ezf_id' => $comp->ezf_id, 'dataid' => $id]), 302);

        $result_success['dataid'] = $id;
        $result_success['ezf_id'] = $comp->ezf_id;
        $result_success['cid'] = $cid;
        $result_success['hsitecode'] = $hsitecode;
        $result_success['hptcode'] = $pid;
        return json_encode($result_success);

    }

    public static function findTargetComponent($ezform_comp)
    {
        /*
         1. ดูว่า EZForm มี comp_id_target อะไร
         2. ไปดูว่า ezform_comp_id นั้น ในตาราง ezf_component มี comp_select = 1 or 2 (select2 นั้น เลือกได้ 1 หรือมากกว่า 1)
         3. ดูว่า ezf_id ชี้ไปที่ ezform ไหน และ ezf_table อะไร
         4. ดูว่า จะ select id,description จาก field ชื่ออะไร (ดูได้จาก field_id_key field_id_desc และ ezf_table)
         5. สรุป select field_id_key as id,concat(field_id_desc1,' ',field_id_desc2) as description from ezf_table;
        */
        $comp_id = $ezform_comp['comp_id'];
        $ezf_id = $ezform_comp['ezf_id'];
        $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id='$ezf_id';")->queryOne();
        $ezf_table = $ezform['ezf_table'];
	
        //$arr_desc = explode(',', $ezform_comp['field_id_desc']);
        $str_desc = '';
        $arr_comp_desc_field_name = array();
        $whereIsNull = '';
        $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id in (".$ezform_comp['field_id_desc'].");")->queryAll();
        foreach ($ezf_fields AS $val) {
            $str_desc .= "IFNULL(`" . $val['ezf_field_name'] . "`, ''),' ',";
            $whereIsNull .= "(`" . $val['ezf_field_name'] . "` LIKE '' OR `" . $val['ezf_field_name'] . "` IS NULL) AND ";
            $arr_comp_desc_field_name[] = $val['ezf_field_name'];
        }
        $str_desc = substr($str_desc, 0, -5);
        $whereIsNull = substr($whereIsNull, 0, -5);
        //echo $str_desc; exit;

        //get field first in component
        //$ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$arr_desc[0]]';")->queryOne();
        //$whereIsNull = $ezf_fields['ezf_field_name'];

        $field_id_key = $ezform_comp['field_id_key'];
        $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id='$field_id_key';")->queryOne();
        //select concat(ifnull(firstname,''),' ',ifnull(lastname,'')) from user_profile where not((firstname like '' or firstname is null) and (lastname like '' or lastname is null));
        $sql = "select " . $ezf_fields['ezf_field_name'] . " as id, concat(" . $str_desc . ") as text from " . $ezf_table . " WHERE NOT(" . $whereIsNull . ")";
        //$sql = "select ".$ezf_fields['ezf_field_name']." as id, concat(".$str_desc.") as text from ".$ezf_table." where $whereIsNull IS NOT NULL";
        //echo $sql; exit;
        $modelComp = Yii::$app->db->createCommand($sql)->queryAll();
        //echo ($modelComp); exit;
        //print_r  ($modelComp); exit;
        //$strModel =array();
        //$strModel = \yii\helpers\ArrayHelper::map($modelComp, 'id', 'text');

        if ($ezform_comp['comp_select'] == 1)
            $tagMultiple = false;
        else if ($ezform_comp['comp_select'] == 2)
            $tagMultiple = true;

        //create sql search
        if ($ezform_comp['shared'] == 1) {
            //public
        } else {
            //private
            if ($comp_id == 100000 OR $comp_id == 100001) {
                $sql = $sql . " AND sitecode = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
            } else {
                $sql = $sql . " AND xsourcex = '" . Yii::$app->user->identity->userProfile->sitecode . "'";
            }
        }
        $sqlSearch = $sql . " AND CONCAT(" . $str_desc . ") LIKE '%\$q%' LIMIT 0,10";

        $modelComp['comp_id'] = $comp_id;
        $modelComp['tagMultiple'] = $tagMultiple;
        $modelComp['sqlSearch'] = $sqlSearch;
        $modelComp['ezf_table_comp'] = $ezf_table;
        $modelComp['arr_comp_desc_field_name'] = $arr_comp_desc_field_name;
        $modelComp['comp_key_name'] = $ezf_fields['ezf_field_name'];
        return $modelComp;
    }

    public function actionStep2()
    {
        $searchModel = new InputDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $ezf_id = Yii::$app->request->get('ezf_id');

        //remove ezform_reply_temp
        Yii::$app->db->createCommand("delete from ezform_reply_temp where user_create=:user_create", [':user_create'=>Yii::$app->user->id])->execute();

        if (Yii::$app->request->get('comp_id_target')) {
            $comp_id = Yii::$app->request->get('comp_id_target');
            $ezform_comp = Yii::$app->db->createCommand("SELECT * FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$comp_id])->queryOne();

            if (isset($_SESSION['inputTarget']) && empty($_GET['addnext']) && $ezf_id != $ezform_comp['ezf_id']) {
                $EzformQuery_onclick = Ezform::find()->select('comp_id_target')->where(['ezf_id' => $ezf_id])->one();
                $EzformQuery_session = Ezform::find()->select('comp_id_target')->where(['ezf_id' => $_SESSION['ezf_id']])->one();
                if ($EzformQuery_session->comp_id_target == $EzformQuery_onclick->comp_id_target) {
                    $this->redirect(Url::to(['step4', 'comp_id_target' => $comp_id, 'target' => $_SESSION['inputTarget'], 'ezf_id' => $ezf_id, 'dataset'=>$_GET['dataset']]));
                }
            }

            if ($ezform_comp['special'] == 1) {
                //หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (ageing)
                $modelComp = self::actionSpecialTarget1($ezform_comp);
            } else {
                //หาแบบปกติ
                $modelComp = self::findTargetComponent($ezform_comp);
            }
        } else {
            $str_model = '';
            $tagMultiple = false;
        }

        $modelform = $this->findModelEzform($ezf_id);

        $ezfAllCanUse = self::ezfCanUse();

        return $this->render('index', [
            'dataComponent' => $modelComp,
            'dataProvider' => $dataProvider,
            'modelEzform' => $modelform,
            'ezform_comp' => $ezform_comp,
            'ezfAllCanUse' => $ezfAllCanUse,
        ]);
    }

    public function actionStep4()
    {
        if (stristr(Yii::$app->keyStorage->get('inputdata.lock-site', true), Yii::$app->user->identity->userProfile->sitecode) || Yii::$app->keyStorage->get('inputdata.lock-site', true) == 'all') {
            //String exists within $string
            return $this->render('lock-page');
        }else{
            //String does not exist
        }

        //session rurl
        if($_REQUEST['rurl']){
            $session = Yii::$app->session;
            $session->set('input_rurl', $_REQUEST['rurl']);
        }

        $dataProvider = InputDataSearch::search();
        $xsourcex = Yii::$app->user->identity->userProfile->sitecode;

        $GET_target = Yii::$app->request->get('target');
        $ezf_id = Yii::$app->request->get('ezf_id');
        $comp_id = Yii::$app->request->get('comp_id_target');
        $modelform = $this->findModelEzform($ezf_id);

        $modelComp = []; $dataProvidertargetEMR = []; $ezform_comp = [];
        if (Yii::$app->request->get('del_dataid')) {
            $del_dataid = Yii::$app->request->get('del_dataid');
            $ezform_comp = EzformQuery::checkIsTableComponent($ezf_id);

            Yii::$app->db->createCommand("UPDATE `".($modelform->ezf_table)."` SET rstat='3' WHERE id = :id;", [':id'=>$del_dataid])->execute();
            Yii::$app->db->createCommand("UPDATE `ezform_target` SET rstat='3' WHERE `data_id` = :data_id;", [':data_id'=>$del_dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :data_id;", [':data_id'=>$del_dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `query_request` WHERE `data_id` = :data_id;", [':data_id'=>$del_dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_data_log` WHERE `dataid` = :data_id;", [':data_id'=>$del_dataid])->execute();

            $this->redirect(Url::to(['step4', 'comp_id_target' => $ezform_comp['comp_id'], 'target' => $GET_target, 'ezf_id' => $ezf_id]));
            return;
        } else if ($comp_id) {

            $ezform_comp = EzformQuery::checkIsTableComponent($ezf_id);
            //Redirect page Focus ที่เป้าหมาย เมื่อเป็น All, byme, skip
            $dataid = Yii::$app->request->get('dataid');

            if($GET_target == 'byme' || $GET_target == 'skip' || $GET_target == 'all') {
                if ($ezform_comp['special'] && $dataid) {
                    $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($ezf_id, $dataid);
                    $target = base64_encode($target['ptid']);
                    $session = Yii::$app->session;
                    $session->set('ezf_id', $ezf_id);
                    $session->set('inputTarget', $target);//as base64
                    $this->redirect(Url::to(['step4', 'comp_id_target' => $comp_id, 'ezf_id' => $ezf_id, 'target' => $target, 'dataid' => Yii::$app->request->get('dataid')]));
                    return;

                } else if ($dataid) {
                    $target = \backend\modules\ezforms\components\EzformQuery::getTargetFormEzf($ezf_id, $dataid);
                    $target = base64_encode($target['target']);

                    $session = Yii::$app->session;
                    $session->set('ezf_id', $ezf_id);
                    $session->set('inputTarget', $GET_target);//as base64

                    $this->redirect(Url::to(['step4', 'comp_id_target' => $comp_id, 'ezf_id' => $ezf_id, 'target' => $target, 'dataid' => Yii::$app->request->get('dataid')]));
                    return;
                }
            }

            if ($ezform_comp['special'] == 1) {
                //หาเป้าหมายพิเศษประเภทที่ 1 (CASCAP) (Thaipalliative) (Ageing)
                $modelComp = self::actionSpecialTarget1($ezform_comp);
                $dataProvidertargetEMR = InputDataSearch::searchEMR($GET_target);
            } else {
                //หาแบบปกติ
                $modelComp = self::findTargetComponent($ezform_comp);
                $dataProvidertargetEMR = InputDataSearch::searchEMR($GET_target);
            }
        } else {
            //target skip
            $modelComp['sqlSearch'] = '';
            $modelComp['tagMultiple'] = FALSE;
            $modelComp['ezf_table_comp'] = '';
            $modelComp['arr_comp_desc_field_name'] = '';
        }

        //set dynamic model
        $modelDynamic = new EzformDynamic($modelform->ezf_table);

        //get คนเดียว
        $modelSelectTarget = [];
        if ($GET_target == 'skip') {
            $modelSelectTarget['text'] = 'Skip';
        } else if ($GET_target == 'byme') {
            $modelSelectTarget['text'] = 'By me';
        }else if ($GET_target == 'all') {
            $modelSelectTarget['text'] = 'All';
        }
        else if ($GET_target) {
            $target = base64_decode($GET_target);

            $sql = "";
            foreach ($modelComp['arr_comp_desc_field_name'] AS $val) {
                $sql .= "IFNULL(`" . $val . "`, ''),' ',";
            }

            $sql = substr($sql, 0, -5);
           //VarDumper::dump($sql,10,true);exit;
            $sql_target = 'SELECT CONCAT(' . $sql . ') AS text, 1 as chk FROM ' . $modelComp['ezf_table_comp'] . ' WHERE ' . $modelComp['comp_key_name'] . ' = \'' . $target . '\'';
            if ($comp_id == 100000 OR $comp_id == 100001) {
                $sql = $sql_target;
            } else {
                $sql = $sql_target . " AND rstat<>3;";
            }
            //echo $sql; exit;
            $modelSelectTarget = Yii::$app->db->createCommand($sql)->queryOne();
            if ($modelSelectTarget['text']=='') {
                $modelSelectTarget['text']='';
            }

            //check unique_record
            if($modelform->unique_record==2){
                //VarDumper::dump($modelform->oldAttributes,10,true); exit();
                if($modelDynamic->find()->where('ptid = :target AND rstat <>3', [':target' =>$target])->count()){
                    $modelform->unique_record = 9; //set
                }

            }
        }
        //VarDumper::dump($modelSelectTarget, 10, true); Yii::$app->end();
	
        $dataProvidertarget = InputDataSearch::searchTarget($ezf_id, $GET_target, Yii::$app->request->get('rstat'), $comp_id);
        $ezfAllCanUse = self::ezfCanUse();


        if (isset($_GET['dataid'])) {

            //SQL Check
            if($modelform->sql_condition){
                $rst = Yii::$app->db->createCommand("select ezf_id from ezform WHERE ezf_id=:ezf_id and (`sql_condition` like '%update%' or `sql_condition` like '%delete%' or `sql_condition` like 'DROP%' or `sql_condition` like 'ALTER%' or `sql_condition` like '%TRUNCATE%' or `sql_condition` like '%remove%')", [':ezf_id'=>$ezf_id])->queryScalar();
                if(!$rst)
                {
                    $sql = str_replace('DATAID', $_GET['dataid'], $modelform->sql_condition);
                    $res = Yii::$app->db->createCommand($sql);
                    //
                    try{
                        $chk = $res->query()->count()>0 ? true : false;
                    }catch (Exception $exception){
                        echo '<h1>Input data error</h1><hr>';
                        echo '<h2>'.$exception->getName().'</h2>';
                        echo $exception->getMessage();
                        exit();
                    }
                    //
                    if ($chk) {
                        $res = $res->queryOne();
                        $error = '';
                        foreach ($res as $val) {
                            if (trim($val) != "") $error .= $val . '<br>';
                        }
                        Yii::$app->db->createCommand("UPDATE `" . $modelform->ezf_table . "` SET `error` = :error WHERE id = :id", ['error' => $error, ':id' => $_GET['dataid']])->execute();
                    } else {
                        Yii::$app->db->createCommand("UPDATE `" . $modelform->ezf_table . "` SET `error` = '' WHERE id = :id", [':id' => $_GET['dataid']])->execute();
                    }
                }else{
                    return 'SQL Condition danger command';
                }

            }

            //SQL Announce
            if($modelform->sql_announce){
                $rst = Yii::$app->db->createCommand("select ezf_id from ezform WHERE ezf_id=:ezf_id and (`sql_announce` like '%update%' or `sql_announce` like '%delete%' or `sql_announce` like 'DROP%' or `sql_announce` like 'ALTER%' or `sql_announce` like 'TRUNCATE%' or `sql_announce` like 'remove%')", [':ezf_id'=>$ezf_id])->queryScalar();
                if(!$rst)
                {
                    $sql = str_replace('DATAID', $_GET['dataid'], $modelform->sql_announce);
                    try{
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                    }catch (Exception $exception){
                        echo '<h1>Input data error</h1><hr>';
                        echo '<h2>'.$exception->getName().'</h2>';
                        echo $exception->getMessage();
                        exit();
                    }

                    if (count($res)) {
                        $sql_announce = '';
                        foreach ($res as $val) {
                            if (trim($val) != "") $sql_announce .= $val . '<br>';
                        }
                    }
                }else{
                    return 'SQL Announce danger command';
                }

            }

            $modelfield = EzformFields::find()
                ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
                ->orderBy(['ezf_field_order' => SORT_ASC])
                ->all();

            $model_table = $modelDynamic->find()->where('id = :id', [':id' => $_GET['dataid']])->One();
            //keep ezf_json_old

            $rst = Yii::$app->db->createCommand("select data_id from ezform_reply_temp where ezf_id=:ezf_id and data_id=:data_id", [':ezf_id'=>$_GET['ezf_id'], ':data_id'=>$_GET['dataid']])->queryScalar();
            if($rst && count($model_table->attributes)) {
                Yii::$app->db->createCommand()->update('ezform_reply_temp', [
                    'ezf_json_old' => json_encode($model_table->attributes),
                    'create_date' => new Expression('now()'),
                    'user_create' => Yii::$app->user->id,
                    'xsourcex' => $xsourcex
                ], ['ezf_id'=>$_GET['ezf_id'], 'data_id'=>$_GET['dataid']])->execute();
            }else if(count($model_table->attributes)){
                Yii::$app->db->createCommand()->insert('ezform_reply_temp', [
                    'ezf_id' => $_GET['ezf_id'],
                    'data_id' => $_GET['dataid'],
                    'ezf_json_old' => json_encode($model_table->attributes),
                    'create_date' => new Expression('now()'),
                    'user_create' => Yii::$app->user->id,
                    'xsourcex' => $xsourcex
                ])->execute();
            }

            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);

            //replace data set
            if($_REQUEST['dataset']) {
                $dataSet = Json::decode(base64_decode($_REQUEST['dataset']));
                foreach ($dataSet as $key=>$value){
                    $model_table->{$key} = $value;
                }
            }
            $model_gen->attributes = $model_table->attributes;

            //การเลือกเป้าหมาย กรณีหาใน site ตัวเองไม่เจอ
            if ($modelSelectTarget['text']=='' && $model_table->xsourcex != $xsourcex) {
                //ให้ไปหาที่ site อื่น
                $sql = $sql_target . 'AND xsourcex = :xsourcex;';
                $modelSelectTarget = Yii::$app->db->createCommand($sql, [':xsourcex'=>$model_table->xsourcex])->queryOne();
                $modelSelectTarget['chk'] =0;
            }
            //for doctor
            if(Yii::$app->user->can('doctorcascap') && $model_table->xsourcex != $xsourcex){
                $model_table->rstat =9; //save draft
            }

            //check rstat for remove
            if($model_table->rstat ==3){
                Yii::$app->db->createCommand("UPDATE `".($modelform->ezf_table)."` SET rstat='3' WHERE id = :id;", [':id'=>$_GET['dataid']])->execute();
                Yii::$app->db->createCommand("UPDATE `ezform_target` SET rstat='3' WHERE `data_id` = :data_id;", [':data_id'=>$_GET['dataid']])->execute();
                Yii::$app->db->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :data_id;", [':data_id'=>$_GET['dataid']])->execute();
                Yii::$app->db->createCommand("DELETE FROM `query_request` WHERE `data_id` = :data_id;", [':data_id'=>$_GET['dataid']])->execute();
                Yii::$app->db->createCommand("DELETE FROM `ezform_data_log` WHERE `dataid` = :data_id;", [':data_id'=>$_GET['dataid']])->execute();
            }

        } else {
            $model_gen =[];
            $modelfield = [];
            $model_table=[];
        }

        return $this->render('index', [
            'modelfield' => $modelfield,
            'dataProvider' => $dataProvider,
            'dataProvidertarget' => $dataProvidertarget,
            'modelSelectTarget' => $modelSelectTarget,
            'dataComponent' => $modelComp,
            'sql_announce' => $sql_announce,
            'model_gen' => $model_gen,
            'ezform_comp' => $ezform_comp,
            'modelEzform' => $modelform,
            'ezfAllCanUse' => $ezfAllCanUse,
            'dataProvidertargetEMR' => $dataProvidertargetEMR,
            'datamodel_table' => $model_table, //data of table form
        ]);
    }

    public function actionResaveDraft(){
        $queryManager = QueryManager::find()->where('user_id = :user_id AND user_group = :user_group', [':user_id'=>(Yii::$app->user->id), ':user_group' => 1])->one();
        if(!$queryManager){
            $queryManager = new QueryManager();
        }
        if(Yii::$app->user->can('adminsite')){
            $queryManager->user_id = 1;
        }
        else if(Yii::$app->user->can('administrator')){
            $queryManager->user_id = 1;
        }

        if($queryManager->user_id){
            $dataid = Yii::$app->request->get('re_dataid');
            $ezf_id = Yii::$app->request->get('ezf_id');
            //
            $ezform = EzformQuery::getFormTableName($ezf_id);
            Yii::$app->db->createCommand("UPDATE `".$ezform->ezf_table."` SET rstat='1' WHERE id = :id;", [':id'=>$dataid])->execute();
            Yii::$app->db->createCommand("UPDATE `ezform_target` SET rstat='1' WHERE `data_id` = :id;", [':id'=>$dataid])->execute();

            $this->redirect(Url::to(['redirect-page', 'dataid' => $dataid, 'ezf_id' => $ezf_id]));
        }
    }

    public static function actionGenTarget()
    {
        //ถ้าตัวที่คลิกกับตัวในเซสชั่นมี component เท่ากัน ให้แสดงเป้าหมายเดิม
        $session = Yii::$app->session;
        $session->set('ezf_id', ($_POST['ezf_id']));
        $session->set('inputTarget', base64_encode($_POST['target']));//as base64
        echo base64_encode($_POST['target']);
    }

    public static function actionInsertRecord($params=null, $redirect=true){

        /*post
        ezf_id
        target
        comp_id_target
        */

        //session rurl
        if($_REQUEST['rurl']){
            $session = Yii::$app->session;
            $session->set('input_rurl', $_REQUEST['rurl']);
        }

        $ezf_id = $_REQUEST['ezf_id'];
        $target = $_REQUEST['target'];
        $comp_id_target = $_REQUEST['comp_id_target'];
        $chk_json = $_REQUEST['json'];
        $purl = $_REQUEST['purl'];
        $xsourcex = Yii::$app->user->identity->userProfile->sitecode;//hospitalcode
        $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;//xdepartmentx

        //is set params
        if($params){
            $ezf_id = $params['ezf_id'];
            $target = $params['target'];
            $comp_id_target = $params['comp_id_target'];
            $xsourcex = $params['xsourcex'];
            $chk_json = $params['json'];
        }

        $haveTarget = ($target == 'all' || $target == 'byme' || $target == 'skip') ? false : true;

        //reset record
        if(Yii::$app->request->get('dataid')){
            $dataid = Yii::$app->request->get('dataid');

            $modelform = Ezform::findOne($ezf_id);
            Yii::$app->db->createCommand("DELETE FROM `".($modelform->ezf_table)."` WHERE id = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_target` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_data_relation` WHERE `target_ezf_data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `query_request` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
            Yii::$app->db->createCommand("DELETE FROM `ezform_data_log` WHERE `dataid` = :data_id;", [':data_id'=>$dataid])->execute();

            //กรณี dpmcloud ลบข้อมูล ให้ลบฝั่ง cascap ออกด้วย
            if(Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org'){
                $res = Yii::$app->db->createCommand("SELECT * FROM ezform_sync WHERE ezf_local = :ezf_id", [':ezf_id'=>$_POST['ezf_id']])->queryOne();
                //ดึงข้อมูลที่ Remote server
                $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id'=>$res['ezf_remote']])->queryOne();
                if($ezform['ezf_id']){
                    //ถ้าพบข้อมูลให้ลบข้อมูลฝั่งนั้นออก
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `".($ezform['ezf_table'])."` WHERE id = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_target` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_data_relation` WHERE `target_ezf_data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_reply` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `query_request` WHERE `data_id` = :data_id;", [':data_id'=>$dataid])->execute();
                    Yii::$app->dbcascaputf8->createCommand("DELETE FROM `ezform_data_log` WHERE `dataid` = :data_id;", [':data_id'=>$dataid])->execute();
                }
            }

        }else {
            $dataid = GenMillisecTime::getMillisecTime();
        }
        //VarDumper::dump($_POST, 10, true);
        //Yii::$app->end();

        $model_fields = EzformQuery::getFieldsByEzf_id($ezf_id);
        $model_form = EzformFunc::setDynamicModelLimit($model_fields);
        $table_name = EzformQuery::getFormTableName($ezf_id);

        $model = new \backend\modules\ezforms\models\EzformDynamic($table_name->ezf_table);

        //check unique_record
        if($table_name->unique_record==2){
            $model_chk = $model->find()->select('id')->where('ptid = :target AND rstat <>3', [':target' =>base64_decode($target)])->one();
            if($model_chk->id){
                if($chk_json){
                    header('Access-Control-Allow-Origin: *');
                    header("content-type:text/javascript;charset=utf-8");
                    echo json_encode(['dataid'=>$model_chk->id]);
                    return ;
                }
                else if($redirect) {
                    //page url
                    if ($purl) {
                        $url = base64_decode($purl)."&comp_id_target=".$comp_id_target."&ezf_id=".$ezf_id."&target=".$target."&dataid=".$model_chk->id;
                    }else{
                        $url = Url::to(['step4', 'comp_id_target' => $comp_id_target, 'ezf_id' => $ezf_id, 'target' => $target, 'dataid' => $model_chk->id,]);
                    }
                    return Yii::$app->getResponse()->redirect($url);
                }else{
                    return $model_chk->id;
                }
            }
        }

        //check new record
        if($haveTarget)
            $model_chk = $model->find()->select('id')->where('(ptid = :target OR target = :target) AND xsourcex = :xsourcex AND rstat =0 AND user_create=:user', [':target' =>base64_decode($target), ':xsourcex' => $xsourcex, ':user'=>Yii::$app->user->id])->one();
        else
            $model_chk = $model->find()->select('id')->where('xsourcex = :xsourcex AND rstat =0 AND user_create=:user', [':xsourcex' => $xsourcex, ':user'=>Yii::$app->user->id])->one();

        if($model_chk->id){
            if($chk_json){
                header('Access-Control-Allow-Origin: *');
                header("content-type:text/javascript;charset=utf-8");
                echo json_encode(['dataid'=>$model_chk->id]);
                return ;
            }
            else if($redirect) {
                //page url
                if ($purl) {
                    $url = base64_decode($purl)."&comp_id_target=".$comp_id_target."&ezf_id=".$ezf_id."&target=".$target."&dataid=".$model_chk->id;
                }else{
                    $url = Url::to(['step4', 'comp_id_target' => $comp_id_target, 'ezf_id' => $ezf_id, 'target' => $target, 'dataid' => $model_chk->id,]);
                }
                return Yii::$app->getResponse()->redirect($url);
            }else{
                return $model_chk->id;
            }
        }


        //replace data set
        if($_REQUEST['dataset']) {
            $dataSet = Json::decode(base64_decode($_REQUEST['dataset']));
            foreach ($dataSet as $key=>$value){
                $model_form->{$key} = $value;
            }
        }
        $model->attributes = $model_form->attributes;


        $model->id = $dataid;
        $model->rstat = 0;
        $model->user_create = Yii::$app->user->id;
        $model->create_date = new Expression('NOW()');
        $model->user_update = Yii::$app->user->id;
        $model->update_date = new Expression('NOW()');
        $model->xsourcex = $xsourcex;//hospitalcode
        $model->xdepartmentx = $xdepartmentx;

        $ezform = EzformQuery::checkIsTableComponent($ezf_id);
        // ตาราง Register ของ  Component
        if($ezform['ezf_id'] == $ezf_id){
            //save target
            $model->target = $dataid;
            $target = $dataid;
        }
        // ตาราง ทั่วไป ของ special Component
        else if ($ezform['special']) {
            //save target
            $target = base64_decode($target);
            $ezform = EzformQuery::getTablenameFromComponent($comp_id_target);
            $targetx = EzformQuery::getTargetFromtable($ezform->ezf_table, $target, $xsourcex);
            $model->target = $targetx['id'];
            //save ptid

            $targetx = EzformQuery::getIDkeyFromtable($ezform->ezf_table, $target);
            $model->ptid = $targetx['ptid'];

            //save sitecode, ptcode. ptcodefull
            $ptcodefull = $targetx['sitecode'].$targetx['ptcode'];
            $model->sitecode = $targetx['sitecode'];
            $model->ptcode = $targetx['ptcode'];

            $res = Yii::$app->db->createCommand("select hsitecode, hptcode from `".($ezform->ezf_table)."` where ptid = :target AND hptcode <> '' AND hsitecode <> '' AND xsourcex = :xsourcex AND rstat <>3 order by create_date DESC", [':target' => $targetx['ptid'], ':xsourcex' => $xsourcex])->queryOne();
            $model->hsitecode = $res['hsitecode'];
            $model->hptcode = $res['hptcode'];
            $model->ptcodefull = $ptcodefull;
            //VarDumper::dump($targetx, 10, true);

        } else if($ezform['comp_id']) {
            $target = base64_decode($target);
            $model->target = $target;
        }

        foreach ($model_fields as $key => $value) {

            if ($value['ezf_field_type'] == 7 || $value['ezf_field_type'] == 253) {

                //$model->{$value['ezf_field_name']} = date('Y-m-d');

            }
            else if ($value['ezf_field_type'] == 9) {

                //$model->{$value['ezf_field_name']} = date('Y-m-d H:i:s');

            }
            else if ($value['ezf_field_type'] == 18) {
                //EzformQuery::saveReferenceFields($ezf_field_ref_field, $ezf_field_ref_table, $target, $ezf_id, $ezf_field_id, $dataid);
                EzformQuery::saveReferenceFields($value['ezf_field_ref_field'], $value['ezf_field_ref_table'], $target, $value['ezf_id'], $value['ezf_field_id'], $dataid);
            }

        }

        if ($model->save()) {
            //save EMR
            $model = new EzformTarget();
            $model->ezf_id = $ezf_id;
            $model->data_id = $dataid;
            $model->target_id = $target;
            isset($_POST['comp_id_target']) ? $model->comp_id = $_POST['comp_id_target'] : '';
            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->user_update = Yii::$app->user->id;
            $model->update_date = new Expression('NOW()');
            $model->rstat = 0;
            $model->xsourcex = $xsourcex;
            $model->save();
            //
        }

        //cascap reference fields CCA01
        if($ezf_id =='1437377239070461302') {
            $res = Yii::$app->db->createCommand("SELECT v2 as birthd, v3 as sex, add1n8code, add1n7code, add1n6code FROM tb_data_1 WHERE ptid = :ptid;", [':ptid'=>$target])->queryOne();
            Yii::$app->db->createCommand("UPDATE tb_data_2 SET f1v1a1 = :f1v1a1, f1v1a2 = :f1v1a2, f1v1a3 = :f1v1a3, f1v2 = :f1v2, f1v3 = :f1v3 WHERE id = :id;", [
                ':id' => $dataid,
                ':f1v1a1' => $res['add1n8code'],
                ':f1v1a2' => $res['add1n7code'],
                ':f1v1a3' => $res['add1n6code'],
                ':f1v2' => $res['birthd'],
                ':f1v3' => $res['sex'],
            ])->query();
        }

        //Palliative reference fields Refer
        if($ezf_id =='1450928555015607100') {
            $res = Yii::$app->db->createCommand("SELECT var27_province, var27_amphur, var27_tumbon FROM  tbdata_1 WHERE ptid = '".$target."' AND xsourcex = '".$xsourcex."';")->queryOne();
            Yii::$app->db->createCommand("UPDATE tbdata_4 SET refer_province = :val_province, refer_amphur = :val_amphur, refer_tumbon = :val_tumbon WHERE id = :id;", [
                ':id' => $dataid,
                ':val_province' => $res['var27_province'],
                ':val_amphur' => $res['var27_amphur'],
                ':val_tumbon' => $res['var27_tumbon'],
            ])->query();
        }

        //save next record
        if(Yii::$app->request->post('addnext')){
            $dataid_before = Yii::$app->request->post('addnext');
            $fields = EzformFields::find()->select('ezf_field_name, ezf_field_options')->where('ezf_id = :ezf_id', [':ezf_id' => $ezf_id])->all();
            $listField ='';
            $listFieldUpdate = '';
            foreach($fields as $field){
                $json = json_decode($field->ezf_field_options, true);
                if($json['remember_field']){
                    $listField .= $field->ezf_field_name.', ';
                    $listFieldUpdate .= $field->ezf_field_name.' = :'.$field->ezf_field_name.', ';
                }
            }

            $listField = substr($listField, 0, -2);
            $listFieldUpdate = substr($listFieldUpdate, 0, -2);
            if($listField) {
                $res = Yii::$app->db->createCommand("SELECT " . $listField . " FROM " . $table_name->ezf_table . " WHERE id = '" . $dataid_before . "';")->queryOne();
                Yii::$app->db->createCommand("UPDATE " . $table_name->ezf_table . " SET " . $listFieldUpdate . " WHERE id = '$dataid';", $res)->query();
            }
        }



        if($chk_json){
            header('Access-Control-Allow-Origin: *');
            header("content-type:text/javascript;charset=utf-8");
            echo json_encode(['dataid'=>$dataid]);
            return ;
        }
        else if($redirect) {
            //page url
            if ($purl) {
                $url = base64_decode($purl)."&comp_id_target=".$comp_id_target."&ezf_id=".$ezf_id."&target=".$haveTarget ? base64_encode($target) : $target."&dataid=".$model_chk->id;
            }else{
                $url = Url::to(['step4', 'comp_id_target' => $comp_id_target, 'ezf_id' => $ezf_id, 'target' => $haveTarget ? base64_encode($target) : $target, 'dataid' => $dataid,]);
            }
            return Yii::$app->getResponse()->redirect($url);
        }else{
            return $dataid;
        }


    }

    public function actionPrintform($id, $message = '')
    {
        $modelform = $this->findModelEzform($id);
        $modelfield = EzformFields::find()
            ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
            ->orderBy(['ezf_field_order' => SORT_ASC])
            ->all();
        if (isset($_GET['dataid'])) {
            $modelDynamic = new EzformDynamic($modelform->ezf_table);
            $model_table = $modelDynamic->find()->where('id = :id', [':id' => Yii::$app->request->get('dataid')])->One();
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $model_gen->attributes = $model_table->attributes;
        }else {
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
        }
        //VarDumper::dump($model_gen->attributes, 10, true);
        //Yii::$app->end();

        $textProvince = 'จ.#area1# อ.#area2# ต.#area3#'; //set value
        foreach($modelfield as $fields){
            $ezf_field_name = $fields->ezf_field_name;
            if($fields->ezf_field_type==0 AND $fields->ezf_field_sub_id != ''){
                $EzformFields = EzformFields::find()->select('ezf_field_type, ezf_field_id, ezf_field_name, ezf_field_label')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id' => $fields['ezf_field_sub_id']])->one();
                if($EzformFields->ezf_field_type==19){
                    //single checkbox
                }
            }else if($fields->ezf_field_type==4 || $fields->ezf_field_type==6){
                //4=radio, 6=select
                $ezformChoice = EzformChoice::find()->where('ezf_field_id = :ezf_field_id AND ezf_choicevalue = :ezf_choicevalue', [':ezf_field_id' => $fields->ezf_field_id, ':ezf_choicevalue' => $model_gen->{$ezf_field_name}])->one();
                //VarDumper::dump($ezformChoice, 10, true);
                //Yii::$app->end();
                $model_gen->{$ezf_field_name} = $ezformChoice->ezf_choicelabel;
            }else if($fields->ezf_field_type==7){
                if($model_gen->{$ezf_field_name}+0) {
                    $explodeDate = explode('-', $model_gen->{$ezf_field_name});
                    $formateDate = $explodeDate[2] . "/" . $explodeDate[1] . "/" . ($explodeDate[0] + 543);
                    $model_gen->{$ezf_field_name} = $formateDate;
                }
            }else if($fields->ezf_field_type==10){
                //conponent
                $comp = EzformComponent::find()->where('comp_id = :comp_id', [':comp_id' => $fields->ezf_component])->one();
                $ezform = EzformQuery::getFormTableName($comp->ezf_id);
                $field_id_key = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$comp->field_id_key])->one();
                $field_id_desc = explode(',', $comp->field_id_desc);
                $fields='';
                foreach($field_id_desc as $val){
                    $fieldx = EzformFields::find()->select('ezf_field_name')->where('ezf_field_id = :ezf_field_id', [':ezf_field_id'=>$val])->one();
                    $fields .= $fieldx->ezf_field_name.', ';
                }
                $fields = substr($fields, 0, -2);

                $res = Yii::$app->db->createCommand("SELECT ".$fields." FROM ".$ezform->ezf_table." WHERE ".$field_id_key->ezf_field_name." = '".$model_gen->$ezf_field_name."';'")->queryOne();
                $text = '';
                foreach($res as $val){
                    $text .= $val.' ';
                }
                //VarDumper::dump($text);
                $model_gen->{$ezf_field_name} = $text;
            }else if($fields->ezf_field_type==11){
                //snomed
            }else if($fields->ezf_field_type==13){
                $fieldx = EzformFields::find()->select('ezf_field_name, ezf_field_label')->where('ezf_field_sub_id = :ezf_field_sub_id', [':ezf_field_sub_id'=>$fields->ezf_field_id])->all();

                foreach($fieldx as $val) {
                    $ezf_field_namex = $val->ezf_field_name;
                    //Province, amphur, tombon
                    if ($val->ezf_field_label == 1) {
                        $sql = "SELECT `PROVINCE_ID`, `PROVINCE_CODE`,`PROVINCE_NAME` FROM `const_province` WHERE PROVINCE_CODE='" . $model_gen->$ezf_field_namex . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area1#', $res['PROVINCE_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 2) {
                        $sql = "SELECT AMPHUR_NAME FROM `const_amphur` WHERE AMPHUR_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area2#', $res['AMPHUR_NAME'], $textProvince);

                    } else if ($val->ezf_field_label == 3) {
                        $sql = "SELECT DISTRICT_NAME FROM `const_district` WHERE DISTRICT_CODE='" . $model_gen->{$ezf_field_namex} . "'";
                        $res = Yii::$app->db->createCommand($sql)->queryOne();
                        $textProvince = str_replace('#area3#', $res['DISTRICT_NAME'], $textProvince);
                        $modelfield->{$ezf_field_namex} = $textProvince;
                        //echo $modelfield->$ezf_field_namex;
                    }
                }
                //$modelfield->$ezf_field_id = $textProvince;
                $textProvince = 'จ.#area1# อ.#area2# ต.#area3#';
            }else if($fields->ezf_field_type==14){
                //fileinputValidate
            }else if($fields->ezf_field_type==17){
                //activeHospitalSave
            }
            else if($fields->ezf_field_type==27){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd9 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }
            else if($fields->ezf_field_type==28){
                //activeIcd9Save
                $res = Yii::$app->db->createCommand("SELECT concat(code, ' : ',  name) as name FROM icd10 WHERE code=:code", [':code'=>$model_gen->{$ezf_field_name}])->queryOne();
                $model_gen->{$ezf_field_name} = $res['name'];
            }

        }
       // \yii\helpers\VarDumper::dump($model_gen->attributes, 10, true);
        return $this->renderPartial('print', [
            'modelform' => $modelform,
            'modelfield' => $modelfield,
            'message' => $message,
            'model_gen' => $model_gen]);
    }

    //delete data (register special component)
    public function actionRemoveData(){
        $ezf_id = $_GET['ezf_id'];
        $data_id = $_GET['data_id'];
        $target = $_GET['target'];
        $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
        $ezform_comp = EzformQuery::checkIsTableComponent($ezf_id);


        $target = base64_decode($target);

        //จาก site ตัวเอง
        $query = EzformTarget::find()->where('(target_id = :target AND rstat <>3 AND xsourcex = :xsourcex )', [':target' => $target, ':xsourcex' => $xsourcex])->orderBy(['create_date' => SORT_ASC]);

        $dataProvidertargetMysite = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $query->count(),
            //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
            'pagination' => array(
                'pageSize' => 20,
            ),
        ]);
        //end my site

        //จาก site อื่นๆ
        $query = EzformTarget::find()->where('(target_id = :target AND rstat <>3 AND xsourcex <> :xsourcex )', [':target' => $target, ':xsourcex' => $xsourcex])->orderBy(['create_date' => SORT_ASC]);

        $dataProvidertargetOthersite = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $query->count(),
            //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
            'pagination' => array(
                'pageSize' => 20,
            ),
        ]);

        //check remove target
        $totalCountEMR =[];
        $totalCountEMR['othersite'] = $dataProvidertargetOthersite->totalCount;
        $totalCountEMR['mysite'] = $dataProvidertargetMysite->totalCount;

        return $this->renderAjax('_remove_data', [
            'dataProvidertargetMysite' => $dataProvidertargetMysite,
            'dataProvidertargetOthersite' => $dataProvidertargetOthersite,
            'ezform_comp' =>$ezform_comp,
            'totalCountEMR' => $totalCountEMR
        ]);
    }

    //for pallitaive care
    public function actionGenForeigner($comp_id=null){
        if($comp_id){
            $ezf_table = EzformQuery::getTablenameFromComponent($comp_id);
            //gen foreigner
            do {
                $cid = implode('', self::actionGenFid());
                //tbdata_1
                $res = Yii::$app->db->createCommand("SELECT cid AS total FROM `".($ezf_table->ezf_table)."` WHERE cid = :cid", [':cid'=>$cid])->queryOne();
            }while($res['total']);
            return $cid;
        }
    }

    public function actionGenFid(){
        $id = array();
        for($i = 0; $i < 13; $i++){
            if($i==0){
                $id[$i] = 0;
            }else{
                $id[$i] = rand(1,9);
            }
        }
        return $id;
    }


    public function renderEzfReplyList($ezf_id, $data_id)
    {
        $modelReplyData = new ActiveDataProvider([
            'query' => EzformReply::find()->where('ezf_id = :ezf_id AND data_id = :data_id', [':ezf_id' => $ezf_id, ':data_id' => $data_id])->orderBy('create_date DESC'),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $view = Yii::$app->getView();
        return $view->render('@backend/views/inputdata/ezf_reply', ['modelReplyData'=>$modelReplyData]);
    }

    public function queryLog($ezf_id, $data_id)
    {
        $modelQueryLog = QueryRequest::find()->where('ezf_id = :ezf_id AND data_id = :data_id', [':ezf_id' => $ezf_id, ':data_id' => $data_id])->orderBy(['time_create'=>SORT_DESC]);

        $modelQueryLog = new ActiveDataProvider([
            'query' => $modelQueryLog,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $view = Yii::$app->getView();
        return $view->render('query_log', ['modelQueryLog'=>$modelQueryLog]);
    }

    public function actionEzfReply()
    {
        $model = new EzformReply();
        if($model->load(Yii::$app->request->post())){

            if(count($_FILES['EzformReply']['name']['file_upload']) && $_FILES['EzformReply']['name']['file_upload'][0]) {
                //print_r($_FILES['QueryRequest']['name']['note']); Yii::$app->end();
                $fileItems = $_FILES['EzformReply']['name']['file_upload'];
                $newFileItems = [];
                foreach ($fileItems as $i => $fileItem) {
                    if($fileItem!=''){
                        $extension = end(explode(".", $fileItem));
                        $newFileName = 'consult_file_'.($i+1).'_'.date("Ymd_His") . '.' . $extension;
                        //chmod(Yii::$app->basePath . '/../backend/web/fileinput/', 0777);
                        $fullPath = Yii::$app->basePath . '/../backend/web/uploads/ezf_reply/' . $newFileName;
                        $fieldname = "EzformReply[file_upload][" . $i . "]";

                        $file = UploadedFile::getInstanceByName($fieldname);
                        $file->saveAs($fullPath);
                        $newFileItems[] = $newFileName;
                    }
                }
                $model->file_upload = implode(',', $newFileItems);
            }

            $model->user_create = Yii::$app->user->id;
            $model->create_date = new Expression('NOW()');
            $model->xsourcex = Yii::$app->user->identity->userProfile->sitecode;
            if($model->save()){
                if(($model->type==2 or $model->type==3) and !(self::checkUserConsultant(Yii::$app->user->id, $model->ezf_id))) {
                    self::consultNotify($model);
                    //send to telegram
                    $user = \common\models\UserProfile::findOne($model->user_create);
                    $sql = "SELECT hcode, name, tambon, amphur, province FROM all_hospital_thai WHERE hcode=:hcode";
                    $hosp = Yii::$app->db->createCommand($sql, [':hcode' => $model->xsourcex])->queryOne();
                    $sql = "SELECT ezf_name, consultant_users, telegram FROM ezform WHERE ezf_id=:ezf_id";
                    $ezform = Yii::$app->db->createCommand($sql, [':ezf_id' => $model->ezf_id])->queryOne();

                    $text = '*Consult Tools from : '.Yii::$app->keyStorage->get('sender').'* %0A*เรื่อง :* '.mb_substr($model->ezf_comment, 0,50,'UTF-8').'... %0A*โดย :* '.($user->firstname.' '.$user->lastname).' %0A*หน่วยงาน : '.$hosp['hcode'].' '.$hosp['name'].'* %0A*E-mail : '.$user->email.'* %0A*เบอร์โทรศัพท์ :* '.$user->telephone.' %0A*URL รายละเอียด :* [Click to open]('.urlencode(Url::toRoute(['/inputdata/redirect-page', 'ezf_id'=>$model->ezf_id, 'dataid'=>$model->data_id], true)).') %0A*รายละเอียด :* %0A '.addcslashes(strip_tags($model->ezf_comment), '*,/,_,[,],\\');
                    $bot = new \common\lib\telegram\helpers\TelegramBot();
                    $bot->token = Yii::$app->keyStorage->get('telegram.token');
                    $bot->sendMessage($ezform['telegram'], $text);

                }else if (self::checkUserConsultant(Yii::$app->user->id, $model->ezf_id)){
                    $sql = "SELECT ezf_table FROM ezform WHERE ezf_id=:ezf_id";
                    $table = Yii::$app->db->createCommand($sql, [':ezf_id' => $model->ezf_id])->queryOne();
                    $sql = "SELECT user_create FROM `".$table['ezf_table']."` WHERE id=:id";
                    $table = Yii::$app->db->createCommand($sql, [':id' => $model->data_id])->queryOne();
                    //send email to user key
                    if($table['user_create']) {
                        self::consultNotify($model, $table['user_create']);
                    }
                }
            }
            return $this->redirect(Url::to(Yii::$app->request->referrer));
        }
    }

    public static function consultNotify($model, $sendTo=null){
        $data['sender'] = Yii::$app->keyStorage->get('sender');
        $data['title'] = '[ขอความช่วยเหลือ] ' . mb_substr($model->ezf_comment, 0, 50, 'UTF-8') . '...';
        $sql = "SELECT ezf_name, consultant_users, telegram FROM ezform WHERE ezf_id=:ezf_id";
        $ezform = Yii::$app->db->createCommand($sql, [':ezf_id' => $model->ezf_id])->queryOne();

        $emailConsult = [];
        //กรณีส่งอีเมลจาก consultant_users ของ ezform
        if(strlen($ezform['consultant_users'])>10) {
            $users = \common\models\UserProfile::find()->select('email')->where('user_id in(' . $ezform['consultant_users'] . ') and (email is not null or LENGTH(email)>0)')->all();

            foreach ($users as $val) {
                $emailConsult[] = $val->email;
            }
        }
        //กรณีกำหนด send to เอง
        if($sendTo) {
            $user = \common\models\UserProfile::findOne($model->user_create);
            $emailConsult[] = $user->email;
        }

        //กรณีมีเมลเท่านั้นถึงจะส่ง
        if(count($emailConsult)) {
            $data['ezf_name'] = $ezform['ezf_name'];
            if ($model->type == 1)
                $data['type'] = 'แสงความคิดเห็นทั่วไป';
            else if ($model->type == 2)
                $data['type'] = 'ขอคำปรึกษา';
            else if ($model->type == 3)
                $data['type'] = 'SOS';
            $data['detail'] = $model->ezf_comment;
            $data['create_date'] = date('d/m/Y H:i:s');
            $user = \common\models\UserProfile::findOne($model->user_create);
            $data['from_name'] = $user->firstname . ' ' . $user->lastname;
            $sql = "SELECT hcode, name, tambon, amphur, province FROM all_hospital_thai WHERE hcode=:hcode";
            $hosp = Yii::$app->db->createCommand($sql, [':hcode' => $model->xsourcex])->queryOne();
            $data['from_site'] = $hosp['hcode'] . ' : ' . $hosp['name'] . ' ' . $hosp['province'];
            $data['from_phone'] = $user->telephone;
            $data['from_email'] = $user->email;
            $data['ezf_id'] = $model->ezf_id;
            $data['data_id'] = $model->data_id;

            $mailer = Yii::$app->mailer->compose('@app/mail/consult', [
                'data' => $data,
            ])
                ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')])
                ->setTo($emailConsult)
                ->setSubject('Consult tools from ' . Yii::$app->keyStorage->get('sender') . ' : ' . $data['title']);
            $mailer->send();
            //end send email
        }
    }

    public static function checkUserConsultant($user_id=null, $ezf_id=null){
        $res = Yii::$app->db->createCommand("select ezf_id from `ezform` WHERE ezf_id = :ezf_id AND consultant_users LIKE :user_id;", [':user_id'=>('%'.$user_id.'%'), 'ezf_id'=>$ezf_id])->queryOne();
        return $res['ezf_id'] ? true : false;
    }

    public function actionConsult(){
        $ezformItems = Yii::$app->db->createCommand("select ezf_id, ezf_name from `ezform` WHERE ezf_id in(select ezf_id from `ezform_favorite` WHERE userid=:userid)", [':userid'=>Yii::$app->user->identity->id])->queryAll();
        $initEzfId = $ezformItems[0]['ezf_id'];
        $ezformItems = ArrayHelper::map($ezformItems, 'ezf_id', 'ezf_name');
        //VarDumper::dump($ezformItems,10,true); exit;
        //
        if($_GET['type']=='all')
            $type = new Expression('type<> 9');
        else if($_GET['type']=='')
            $type ='type =2';
        else
            $type = 'type = '.$_GET['type'];

            $type .= " AND ezf_id = '".($_GET['ezf_id'] ? $_GET['ezf_id'] : $initEzfId)."'";
        //
        $dataProvider = new ActiveDataProvider([
            'query' => EzformReply::find()->select('id, ezf_id, data_id, ezf_comment, file_upload, user_create, create_date, update_date, xsourcex, rstat, type')->where($type)->groupBy('data_id')->orderBy('create_date DESC'),
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        return $this->render('consult', [
            'dataProvider' => $dataProvider,
            'ezformItems' => $ezformItems
        ]);
    }

    public function actionConsultView($id){
        $model = EzformReply::find()->where('id=:id', [':id'=>$id])->one();
        //->xsourcex
        $sql="SELECT hcode, name, tambon, amphur, province FROM all_hospital_thai WHERE hcode=:hcode";
        $hosp = \Yii::$app->db->createCommand($sql,[':hcode' => $model->xsourcex])->queryOne();
        $model->xsourcex  = $hosp['hcode'].' : '.$hosp['name'].' '.$hosp['province'];
        //
        $date = new \DateTime($model->create_date);
        $model->create_date = $date->format('d/m/Y');
        //
        if($model->type == 1)
            $model->type = 'แสงความคิดเห็นทั่วไป';
        else if($model->type == 2)
            $model->type = 'ขอคำปรึกษา';
        else if($model->type == 3)
            $model->type = 'SOS';
        //
        $model->ezf_comment = mb_substr($model->ezf_comment, 0,50,'UTF-8').'...';
        //
        $sql="SELECT ezf_name FROM ezform WHERE ezf_id=:ezf_id";
        $hosp = \Yii::$app->db->createCommand($sql,[':ezf_id' => $model->ezf_id])->queryOne();
        $model->ezf_id = $hosp['ezf_name'];
        //
        //$model->oldAttributes[];

        return $this->render('consult-view', [
            'model' => $model,
        ]);
    }

    public static function actionStep2Confirm($queryParams=null){

        $queryParams = $queryParams ? $queryParams : Yii::$app->request->queryParams;

        $queryParams['target'] = preg_replace("/[^0-9]/", "", $queryParams['target']);
        $ezform_comp = Yii::$app->db->createCommand("SELECT ezf_id, field_id_desc FROM ezform_component WHERE comp_id=:comp_id;", [':comp_id'=>$queryParams['comp_id']])->queryOne();

        $dataOtherSite=[];
        if($queryParams['task']=='add-from-site'){
            $arr_desc = explode(',', $ezform_comp['field_id_desc']);
            $dataComponent=[];
            foreach ($arr_desc AS $key => $val) {
                //คือไม่เอาตัวแรก
                if($key>1) {
                    $ezf_fields = Yii::$app->db->createCommand("SELECT ezf_field_name FROM ezform_fields WHERE ezf_field_id=:id;", [':id'=>$val])->queryOne();
                    $dataComponent[] = $ezf_fields['ezf_field_name'];
                }
            }
            $dataComponent[] = 'xsourcex';
            $dataComponent[] = 'id';
            $ezfrom = EzformQuery::getFormTableName($ezform_comp['ezf_id']);
            $query = new Query();
            $query->select($dataComponent)->from($ezfrom->ezf_table)->where('rstat <> 3 AND `cid` LIKE :cid AND id=ptid', [':cid'=>$queryParams['target']]);
            $query = $query->createCommand()->queryOne();
            $hospital = EzformQuery::getHospital($query['xsourcex']);
            $query['hospital'] = $hospital['name']. ' ต.'. $hospital['tambon']. ' อ.'. $hospital['amphur']. ' จ.'. $hospital['province'];
            $dataOtherSite=$query;
            //primary ezf_id
            $queryParams['comp_ezf_id'] = $ezform_comp['ezf_id'];
            $queryParams['comp_ezf_name'] = $ezfrom->ezf_name;
        }else if($queryParams['task'] == 'gen-foreigner'){
            $target = self::actionGenForeigner($queryParams['comp_id']);
            $queryParams['target'] = $target;
            $queryParams['task'] = 'add-new';
            $queryParams['mode'] = 'gen-foreigner';
        }
        //check tcc mapping fields
        $chkMappingTccBot = Yii::$app->db->createCommand("select count(*) as total from `ezform_maping_tcc` WHERE ezf_id=:ezf_id;", [':ezf_id'=>$ezform_comp['ezf_id']])->queryOne();

        //กรณีมีการส่งค่าผ่านทาง GET
        if($_GET['comp_id']){
            return Yii::$app->view->renderAjax('@backend/views/inputdata/_step2confirm', [
                'queryParams' => $queryParams,
                'dataOtherSite' => $dataOtherSite,
                'chkMappingTccBot' => $chkMappingTccBot['total'],
            ]);
        }else {
            $arr['queryParams'] = $queryParams;
            $arr['dataOtherSite'] = $dataOtherSite;
            $arr['chkMappingTccBot'] = $chkMappingTccBot;
            return $arr;
        }
    }

    public static function getPersonOldServer($sitecode, $cid, $key, $convert){
        $str = "decode(unhex(person.address),sha2(:key,256)) AS address,
		    decode(unhex(person.cid),sha2(:key,256)) AS cid,
		    decode(unhex(person.hn),sha2(:key,256)) AS hn,
		    decode(unhex(person.pname),sha2(:key,256)) AS pname,
		    decode(unhex(person.fname),sha2(:key,256)) AS fname,
		    decode(unhex(person.lname),sha2(:key,256)) AS lname,";

        if ($convert==1) {
            $str = "convert(decode(unhex(person.address),sha2(:key,256)) using tis620) AS address,
		    convert(decode(unhex(person.cid),sha2(:key,256)) using tis620) AS cid,
		    convert(decode(unhex(person.hn),sha2(:key,256)) using tis620) AS hn,
		    convert(decode(unhex(person.pname),sha2(:key,256)) using tis620) AS pname,
		    convert(decode(unhex(person.fname),sha2(:key,256)) using tis620) AS fname,
		    convert(decode(unhex(person.lname),sha2(:key,256)) using tis620) AS lname,";
        }

        $sql = "SELECT person.khet,
		    person.province,
		    person.amphur,
		    person.tambon,
		    person.hospcode,
		    person.hospname,
		    person.person_id,
		    person.house_id,
		    $str
		    person.sex,
		    person.nationality,
		    person.education,
		    person.type_area,
		    person.religion,
		    person.birthdate,
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) AS age,
		    person.village_id,
		    person.village_code,
		    person.village_name,
		    person.pttype,
		    person.pttype_begin_date,
		    person.pttype_expire_date,
		    person.pttype_hospmain,
		    person.pttype_hospsub,
		    person.marrystatus,
		    person.death,
		    person.death_date
	    FROM person
	    WHERE person.sitecode = :hospcode and cidlink like md5(:cid)
	    ";

        //ip 8
        return Yii::$app->dbbot_ip8->createCommand($sql, [':cid'=>$cid, ':hospcode'=>$sitecode, ':key'=>$key])->queryOne();
    }

    public static function getPersonOneByCid($sitecode, $cid, $key, $convert) {
        //new server
        $str = "
		    decode(unhex(f_person.cid),sha2(:key,256)) AS cid,
		    decode(unhex(f_person.HN),sha2(:key,256)) AS hn,
		    decode(unhex(f_person.Pname),sha2(:key,256)) AS pname,
		    decode(unhex(f_person.Name),sha2(:key,256)) AS fname,
		    decode(unhex(f_person.Lname),sha2(:key,256)) AS lname,";

        if ($convert==1) {
            $str = "
		    convert(decode(unhex(f_person.cid),sha2(:key,256)) using tis620) AS cid,
		    convert(decode(unhex(f_person.HN),sha2(:key,256)) using tis620) AS hn,
		    convert(decode(unhex(f_person.Pname),sha2(:key,256)) using tis620) AS pname,
		    convert(decode(unhex(f_person.Name),sha2(:key,256)) using tis620) AS fname,
		    convert(decode(unhex(f_person.Lname),sha2(:key,256)) using tis620) AS lname,";
        }

        $sql = "SELECT
		    $str
		    f_person.sex,
		    f_person.Birth as birthdate,
		    f_person.ptlink
	    FROM f_person
	    WHERE f_person.sitecode = :hospcode and ptlink like md5(:cid)
	    ";

        $res = CkdnetFunc::queryOne($sql, [':cid'=>$cid, ':hospcode'=>$sitecode, ':key'=>$key]);

        $str = "decode(unhex(f_address.HouseNo),sha2(:key,256)) AS address ";

        if ($convert==1) {
            $str = "convert(decode(unhex(f_address.HouseNo),sha2(:key,256)) using tis620) AS address ";
        }

        if($res['cid']) {
            $resAddr = CkdnetFunc::queryOne("select f_address.Village as village_no, 
        f_address.villaname as village_name,
        f_address.Changwat as changwat,
        f_address.Ampur as ampur,
        f_address.Tambon as tambon, $str from f_address where f_address.sitecode = :hospcode and f_address.ptlink like :ptlink", [':hospcode'=>$sitecode, ':ptlink' => $res['ptlink'], ':key' => $key]);

            $res['address'] = $resAddr['address'];
            $res['village_no'] = $resAddr['village_no'];
            $res['village_name'] = $resAddr['village_name'];
            $res['changwat'] = $resAddr['changwat'];
            $res['ampur'] = $resAddr['ampur'];
            $res['tambon'] = $resAddr['tambon'];
        }else {
            //old server
            $res = self::getPersonOldServer($sitecode, $cid, $key, $convert);
        }

        return $res;
    }

    public function actionRegFromBot(){
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        if (isset($_POST['findbot'])) {
            $cid = $_POST['findbot']['cid'];
            $key = $_POST['findbot']['key'];
            $convert = isset($_POST['findbot']['convert'])?$_POST['findbot']['convert']:0;
            $comp_id = $_POST['comp_id'];

            $checkCid = OvccaFunc::check_citizen($cid);
            if($checkCid){

                $tccbot = self::getPersonOneByCid($sitecode, $cid, $key, $convert);
                //ดึงข้อมูลจาก tcc bot
                //เช็คความถูกต้องของข้อมูล
                //นำเข้าข้อมูล

                if(($tccbot)){

                    //+ getPersonOldServer
                    if(!OvccaFunc::check_citizen($tccbot['cid'])){
                        $tccbot = self::getPersonOldServer($sitecode, $cid, $key, $convert);
                    }

                    if(OvccaFunc::check_citizen($tccbot['cid'])){
                        $checkthaiword = trim(OvccaFunc::checkthai($tccbot['fname']));

                        if ($checkthaiword  != '') {

                            if($_POST['import_botconfirm']) {
                                $r = self::importDataFromBot($sitecode, $cid, $tccbot, $comp_id);
                                if (count($r)) {
                                    $arr = [];
                                    $message = "นำเข้าข้อมูลแล้ว <code>เลขบัตรประชาชน: {$tccbot['cid']} ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}</code>";
                                    $arr['message'] = $message;
                                    $arr['status'] = 'success';
                                    $arr['import'] = 'success';
                                    return json_encode(array_merge($arr, $r));
                                } else {
                                    $arr = [];
                                    $message = "ไม่สามารถนำเข้าข้อมูลเข้าสู่ระบบ <code>เลขบัตรประชาชน: {$tccbot['cid']} ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}</code>";
                                    $arr['message'] = $message;

                                    return json_encode($arr);
                                }
                            }
                            $arr = [];
                            $message = "พบข้อมูล <code>เลขบัตรประชาชน: {$tccbot['cid']}</code> ชื่อ: {$tccbot['pname']}{$tccbot['fname']} {$tccbot['lname']}";
                            $arr['message'] = $message;
                            $arr['status'] = 'success';
                            return json_encode($arr);

                        } else {
                            $arr = [];
                            $message = "กรณีชื่อ-สกุล อ่านไม่ออกให้เข้ารหัสแบบ tis620";
                            $arr['message'] = $message;
                            return json_encode($arr);
                        }
                    } else {
                        $arr = [];
                        $message = "Convert ข้อมูลไม่ได้ หรือ เลขบัตรประชาชน ไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง";
                        $arr['message'] = $message;
                        return json_encode($arr);
                    }

                } else {
                    $arr = [];
                    $arr['message'] = 'ไม่พบข้อมูลใน TDC';
                    return json_encode($arr);
                }

            } else {
                $arr = [];
                $arr['message'] = 'เลขบัตรประชาชนไม่ถูกต้อง';
                return json_encode($arr);
            }


        }

        return $this->renderAjax('_form_regbot', ['queryParams'=>$_POST]);
    }

    public function importDataFromBot($sitecode, $cid, $tccbot, $comp_id=null){

        $comp = EzformComponent::find()
            ->select('ezf_id, comp_id')
            ->where(['comp_id' => $comp_id])
            ->one();
        $ezform = Ezform::find()
            ->select('ezf_id, ezf_table')
            ->where(['ezf_id' => $comp->ezf_id])
            ->one();

        //นำเข้าข้อมูลใหม่
        $id = GenMillisecTime::getMillisecTime();
        $hpcode = $sitecode;
        $user_create = Yii::$app->user->id;
        $create_date = date('Y-m-d H:i:s');
        $pid = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `".($ezform->ezf_table)."` WHERE xsourcex = '$hpcode';")->queryOne();
        $pid = str_pad($pid['pidcode']+1, 5, "0", STR_PAD_LEFT);
        //$pid = $pid['pidcode'];
        $ptcodefull = $hpcode.$pid;

        if(strlen($tccbot['village_code'])>2){
            //หมู่ที่
            $add1n5=substr($tccbot['village_code'], -2);
            //จังหวัด
            $add1n8code=substr($tccbot['village_code'], 0, 2);
            //อำเภอ
            $add1n7code =substr($tccbot['village_code'], 0, 4);
            //ตำบล
            $add1n6code=substr($tccbot['village_code'], 0, 6);
        }else{
            //หมู่ที่
            $add1n5=$tccbot['village_no'];
            //จังหวัด
            $add1n8code=$tccbot['changwat'];
            //อำเภอ
            $add1n7code =$tccbot['ampur'];
            //ตำบล
            $add1n6code=$tccbot['tambon'];
        }
        //check ptid from system
        //หาข้อมูลล่าสุดก่อนมาให้
        $modelFromSys = Yii::$app->db->createCommand("SELECT ptid FROM `".($ezform->ezf_table)."` WHERE  `cid` LIKE '" . $cid . "' AND rstat <> 3 AND rstat IS NOT NULL ORDER BY update_date DESC;")->queryOne();

        $r = Yii::$app->db->createCommand()->insert($ezform->ezf_table,  [
            'id' => $id,
            'ptid' => $modelFromSys['ptid'] ? $modelFromSys['ptid'] : $id,
            'xsourcex' => $hpcode,
            'target' => $id,
            'user_create' => $user_create,
            'create_date' => $create_date,
            'sitecode' => $hpcode,
            'ptcode' => $pid,
            'ptcodefull' => $ptcodefull,
            'hsitecode' => $hpcode,
            'hptcode' => $pid,
            'cid' => $cid,
            'rstat' => 1,
            //new
            'tccbot'=>1,
            'title' => $tccbot['pname'],
            'name' => $tccbot['fname'],
            'surname' => $tccbot['lname'],
            'v2' => $tccbot['birthdate'],
            'v3' => $tccbot['sex'],
            'age' => OvccaFunc::getAge($tccbot['birthdate']),

            'add1n2' => $tccbot['village_name'],
            'add1n1' => $tccbot['address'],
            'add1n5'=>$add1n5,
            'add1n8code'=>$add1n8code,
            'add1n7code'=>$add1n7code,
            'add1n6code'=>$add1n6code,

            'hn' => $tccbot['hn'],
            'hncode' => $tccbot['hn'],
        ])->execute();

        if($r){
            //save EMR
            $modelTarget = new EzformTarget();
            $modelTarget->ezf_id = $ezform->ezf_id;
            $modelTarget->comp_id = $comp_id;
            $modelTarget->data_id = $id;
            $modelTarget->target_id = $id;
            $modelTarget->user_create = Yii::$app->user->id;
            $modelTarget->create_date = new Expression('NOW()');
            $modelTarget->user_update = Yii::$app->user->id;
            $modelTarget->update_date = new Expression('NOW()');
            $modelTarget->rstat = 1;
            $modelTarget->xsourcex = $hpcode;
            $modelTarget->save();

            $arr = [
                'hsitecode' => $hpcode,
                'hptcode' => $pid,
                'cid' => $cid,
                'ezf_id' => $ezform->ezf_id,
                'dataid' => $id,
            ];
            return $arr;

        } else{
        }
    }

    public function actionRegFromTdc(){
        $sitecode = Yii::$app->user->identity->userProfile->sitecode;

        if (isset($_POST['findbot'])) {
            $cid = $_POST['findbot']['cid'];
            $key = $_POST['findbot']['key'];
            $convert = isset($_POST['findbot']['convert'])?$_POST['findbot']['convert']:0;
            $comp_id = $_POST['comp_id'];

            $checkCid = OvccaFunc::check_citizen($cid);
            if($checkCid){

                $tdc = self::getPersonTdcOneByCid($sitecode, $cid, $key, $convert);
                //ดึงข้อมูลจาก tdc
                //เช็คความถูกต้องของข้อมูล
                //นำเข้าข้อมูล

                if(($tdc)){
                    if(OvccaFunc::check_citizen($tdc['CID'])){
                        $checkthaiword = trim(OvccaFunc::checkthai($tdc['Name']));

                        if ($checkthaiword  != '') {

                            if($_POST['import_botconfirm']) {
                                $r = self::importDataFromTdc($sitecode, $cid, $tdc, $comp_id);
                                if (count($r)) {
                                    $arr = [];
                                    $message = "นำเข้าข้อมูลแล้ว <code>เลขบัตรประชาชน: {$tdc['CID']} ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}</code>";
                                    $arr['message'] = $message;
                                    $arr['status'] = 'success';
                                    $arr['import'] = 'success';
                                    return json_encode(array_merge($arr, $r));
                                } else {
                                    $arr = [];
                                    $message = "ไม่สามารถนำเข้าข้อมูลเข้าสู่ระบบ <code>เลขบัตรประชาชน: {$tdc['CID']} ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}</code>";
                                    $arr['message'] = $message;

                                    return json_encode($arr);
                                }
                            }
                            $arr = [];
                            $message = "พบข้อมูล <code>เลขบัตรประชาชน: {$tdc['CID']}</code> ชื่อ: {$tdc['Pname']}{$tdc['Name']} {$tdc['Lname']}";
                            $arr['message'] = $message;
                            $arr['status'] = 'success';
                            return json_encode($arr);

                        } else {
                            $arr = [];
                            $message = "กรณีชื่อ-สกุล อ่านไม่ออกให้เข้ารหัสแบบ tis620";
                            $arr['message'] = $message;
                            return json_encode($arr);
                        }
                    } else {
                        $arr = [];
                        $message = "Convert ข้อมูลไม่ได้ หรือ เลขบัตรประชาชน ไม่ถูกต้องกรุณาใส่ Key ใหม่เพื่อถอดรหัสให้ถูกต้อง";
                        $arr['message'] = $message;
                        return json_encode($arr);
                    }

                } else {
                    $arr = [];
                    $arr['message'] = 'ไม่พบข้อมูลใน TDC';
                    return json_encode($arr);
                }

            } else {
                $arr = [];
                $arr['message'] = 'เลขบัตรประชาชนไม่ถูกต้อง';
                return json_encode($arr);
            }


        }

        return $this->renderAjax('_form_regtdc', ['queryParams'=>$_POST]);
    }

    public static function getPersonTdcOneByCid($sitecode, $cid, $key, $convert) {
        $str = "decode(unhex(address.HouseNo),sha2(:key,256)) AS HouseNo,
		    decode(unhex(person.CID),sha2(:key,256)) AS CID,
		    decode(unhex(person.HN),sha2(:key,256)) AS HN,
		    decode(unhex(person.Pname),sha2(:key,256)) AS Pname,
		    decode(unhex(person.`Name`),sha2(:key,256)) AS `Name`,
		    decode(unhex(person.Lname),sha2(:key,256)) AS Lname,";

        if ($convert==1) {
            $str = "convert(decode(unhex(address.HouseNo),sha2(:key,256)) using tis620) AS HouseNo,
		    convert(decode(unhex(person.CID),sha2(:key,256)) using tis620) AS CID,
		    convert(decode(unhex(person.HN),sha2(:key,256)) using tis620) AS HN,
		    convert(decode(unhex(person.Pname),sha2(:key,256)) using tis620) AS Pname,
		    convert(decode(unhex(person.`Name`),sha2(:key,256)) using tis620) AS `Name`,
		    convert(decode(unhex(person.Lname),sha2(:key,256)) using tis620) AS Lname,";
        }

        $sql = "SELECT person.HOSPCODE,
		    person.PID,
		    person.HID,
		    person.HouseID,
		    person.PreName,
		    $str
		    person.sex,
		    person.Birth,
		    floor(DATEDIFF(CURRENT_DATE, STR_TO_DATE(person.Birth, '%Y%m%d'))/365) AS age,
		    person.Mstatus,
		    person.Occupation_Old,
		    person.Occupation_New,
		    person.Race,
		    person.Nation,
		    person.Religion,
		    person.Education,
		    person.Fstatus,
		    person.Father,
		    person.Mother,
		    person.Couple,
		    person.Vstatus,
		    person.MoveIn,
		    person.Discharge,
		    person.Ddischarge,
		    person.BloodGroup,
		    person.Rh,
		    person.Labor,
		    person.PassPort,
		    person.TypeArea,
		    person.D_Update,
		    address.addresstype,
		    address.housetype,
		    address.House_id,
		    address.roomno,
		    address.condo,
		    address.soisub,
		    address.soimain,
		    address.villaname,
		    address.address,
		    address.Road,
		    address.Village,
		    address.Tambon,
		    address.Ampur,
		    address.Changwat,
		    address.TelePhone,
		    address.Mobile
	    FROM person INNER JOIN address ON person.PID = address.PID
	    WHERE person.HOSPCODE = :hospcode and cidlink like md5(:cid)
	    ";

        return Yii::$app->dbtdc->createCommand($sql, [':cid'=>$cid, ':hospcode'=>$sitecode, ':key'=>$key])->queryOne();
    }

    public static function importDataFromTdc($sitecode, $tdc_data, $comp_id=null){

        $comp = EzformComponent::find()
            ->select('ezf_id, comp_id')
            ->where(['comp_id' => $comp_id])
            ->one();
        $ezform = Ezform::find()
            ->select('ezf_id, ezf_table')
            ->where(['ezf_id' => $comp->ezf_id])
            ->one();

        //นำเข้าข้อมูลใหม่
	$table = $ezform->ezf_table;
	$id = GenMillisecTime::getMillisecTime();
        $hpcode = $sitecode;
        $pid_data = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `$table` WHERE xsourcex = '$hpcode';")->queryOne();
        $pid = str_pad($pid_data['pidcode']+1, 5, "0", STR_PAD_LEFT);
        //$pid = $pid_data['pidcode'];
        $ptcodefull = $hpcode.$pid;

	$tbdata = new TbdataAll();
        $tbdata->setTableName($table);

	//data set
	$tbdata->attributes = $tdc_data;

	//Fix
	$tbdata->id = $id;
	$tbdata->ptid = $id;
	$tbdata->xsourcex = $hpcode;
	$tbdata->target = $id;
	$tbdata->user_create = Yii::$app->user->id;
	$tbdata->create_date = new Expression('NOW()');
	$tbdata->sitecode = $hpcode;
	$tbdata->ptcode = $pid;
	$tbdata->ptcodefull = $ptcodefull;
	$tbdata->hsitecode = $hpcode;
	$tbdata->hptcode = $pid;
	$tbdata->rstat = 1;

	if($tbdata->save()){
            //save EMR
            $modelTarget = new EzformTarget();
            $modelTarget->ezf_id = $ezform->ezf_id;
            $modelTarget->comp_id = $comp_id;
            $modelTarget->data_id = $tbdata['id'];
            $modelTarget->target_id = $tbdata['id'];
            $modelTarget->user_create = Yii::$app->user->id;
            $modelTarget->create_date = new Expression('NOW()');
            $modelTarget->user_update = Yii::$app->user->id;
            $modelTarget->update_date = new Expression('NOW()');
            $modelTarget->rstat = 1;
            $modelTarget->xsourcex = $tbdata['xsourcex'];
            $modelTarget->save();

            $arr = [
                'hsitecode' => $tbdata['hsitecode'],
                'hptcode' => $tbdata['hptcode'],
                'cid' => $tbdata['CID'],
                'ezf_id' => $ezform->ezf_id,
                'dataid' => $tbdata['id'],
            ];
            return $arr;

        }
	return FALSE;
    }

    public static function insertTdcAll($sitecode, $table, $tdc_data){
	//นำเข้าข้อมูลใหม่
        $id = GenMillisecTime::getMillisecTime();
        $hpcode = $sitecode;
        $pid_data = Yii::$app->db->createCommand("SELECT MAX(CAST(hptcode AS UNSIGNED))  AS pidcode FROM `$table` WHERE xsourcex = '$hpcode';")->queryOne();
        $pid = str_pad($pid_data['pidcode']+1, 5, "0", STR_PAD_LEFT);
        //$pid = $pid_data['pidcode'];
        $ptcodefull = $hpcode.$pid;

	$tbdata = new TbdataAll();
        $tbdata->setTableName($table);

	//data set
	$tbdata->attributes = $tdc_data;

	//Fix
	$tbdata->id = $id;
	$tbdata->ptid = $id;
	$tbdata->xsourcex = $hpcode;
	$tbdata->target = $id;
	$tbdata->user_create = Yii::$app->user->id;
	$tbdata->create_date = new Expression('NOW()');
	$tbdata->sitecode = $hpcode;
	$tbdata->ptcode = $pid;
	$tbdata->ptcodefull = $ptcodefull;
	$tbdata->hsitecode = $hpcode;
	$tbdata->hptcode = $pid;
	$tbdata->rstat = 1;

	//custom data

	return $tbdata->save();
    }

    public function actionAutoSaveForm(){
        $arr = [];
        $strStart = strpos($_POST['ezf_name'],"[");
        $field = substr($_POST['ezf_name'], $strStart+1);
        $field = substr($field, 0, -1);
        //
        $ezform = EzformQuery::getFormTableName($_POST['ezf_id']);
        $res = Yii::$app->db->createCommand("SELECT xsourcex, rstat FROM `". ($ezform->ezf_table)."` WHERE id=:id", [':id'=>$_POST['data_id']])->queryOne();
        if($res['xsourcex'] == Yii::$app->user->identity->userProfile->sitecode && ($res['rstat'] == 0 || $res['rstat'] == 1)){
            $chkField = true;
            $fields = EzformFields::find()->select('ezf_field_type')->where(['ezf_id'=>$_POST['ezf_id'], 'ezf_field_name' => $field])->one();
            if($fields->ezf_field_type==7 || $fields->ezf_field_type==253){
                //date
                $chkField = false;
            }else if($fields->ezf_field_type==14){
                //fileinputValidate
                $chkField = false;
            }

            if($chkField) {
                $query = Yii::$app->db->createCommand()->update($ezform->ezf_table, [$field => $_POST['value'], 'update_date' => new Expression('NOW()')], ['id' => $_POST['data_id']]);
                //echo $query->getRawSql();
                $query->execute();
                $arr['status'] = 'success';
                $arr['message'] = "<strong><i class=\"glyphicon glyphicon-ok\"></i> save success!</strong>";
            }else{
                $arr['status'] = 'error';
                $arr['message'] = "Can't Edit";
            }
        }else if (Yii::$app->user->can('doctorcascap')){
          //
        }else{
            $arr['status'] = 'error';
            $arr['message'] = "Can't Edit";
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $arr;
    }

    public function actionLastChkCascapReg($dataid){
        $res = Yii::$app->db->createCommand("select dlastcheck from tb_data_1 where id=:id;", [':id'=>$dataid])->queryOne();

        $text = '';
        if($res['dlastcheck'] == '')
            $text = '<h3 id="last-chk-cascap-reg" class="text text-danger">ตรวจล่าสุดเมื่อ : (ยังไม่ได้ตรวจ)</h3>';
        else
            $text = '<h3 id="last-chk-cascap-reg" class="text text-danger">ตรวจล่าสุดเมื่อ : '.$res['dlastcheck'].'</h3>';

        echo $text;
        return ;
    }

    public function actionRenderEzfData()
    {
        $xsourcex = Yii::$app->user->identity->userProfile->sitecode;
        $ezf_id = Yii::$app->request->get('ezf_id');
        $comp_id = Yii::$app->request->get('comp_id_target');

        try {
            $modelform = \backend\modules\ezforms\models\Ezform::find()->where(['ezf_id' => $ezf_id])->one();
            $modelfield = \backend\modules\ezforms\models\EzformFields::find()
                ->where('ezf_id = :ezf_id', [':ezf_id' => $modelform->ezf_id])
                ->orderBy(['ezf_field_order' => SORT_ASC])
                ->all();
            $model_gen = \backend\modules\ezforms\components\EzformFunc::setDynamicModelLimit($modelfield);
            $modelDynamic = new \backend\modules\ezforms\models\EzformDynamic($modelform->ezf_table);
            if ($_GET['dataid']) {
                $model_table = $modelDynamic->find()->where('id = :id', [':id' => $_GET['dataid']])->One();
                $model_gen->attributes = $model_table->attributes;
            }
            //
            $ezform_comp =  EzformComponent::find()
                ->where(['comp_id' => $comp_id])
                ->one();

            return $this->render('_render_form', [
                'modelEzform' => $modelform,
                'modelfield' => $modelfield,
                'model_gen' => $model_gen,
                'datamodel_table' => $model_table,
                'ezform_comp' =>$ezform_comp
            ]);
        }catch (\yii\base\Exception $exception){
            echo $exception->getMessage();
            exit;
        }
    }

    protected function findModelEzform($id)
    {
        if (($model = \backend\modules\ezforms\models\Ezform::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
