<?php

namespace backend\controllers;

use Yii;
use backend\models\MoveUnits;
use backend\models\MoveUnitsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * MoveUnitsController implements the CRUD actions for MoveUnits model.
 */
class MoveUnitsController extends Controller
{
    public function behaviors()
    {
        return [
	    'access' => [
		'class' => AccessControl::className(),
		'rules' => [
		    [
			'allow' => true,
			'actions' => ['index', 'view'], 
			'roles' => ['?', '@'],
		    ],
		    [
			'allow' => true,
			'actions' => ['create', 'update', 'delete'], 
			'roles' => ['@'],
		    ],
		],
	    ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
	if (parent::beforeAction($action)) {
	    if (in_array($action->id, array('create', 'update'))) {
		
	    }
	    return true;
	} else {
	    return false;
	}
    }
    
    /**
     * Lists all MoveUnits models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MoveUnitsSearch();
	$searchModel->status = 'waiting';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MoveUnits model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    return $this->renderAjax('view', [
		'model' => $this->findModel($id),
	    ]);
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionCheckadmin($sitecode)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    
	    
	    $data = \common\models\UserProfile::getAdminsite($sitecode);
	    if($data){
		$comma = '';
		$text = 'หน่วยงานใหม่ที่ท่านต้องการย้ายมี admin site โปรดติดต่อ admin site เพื่ออนุมัติการย้ายหน่วยงาน ดังนี้<br>';
		foreach ($data as $key => $value){
		    $text .= $comma."ชื่อ: {$value['fullname']} e-mail: {$value['email']} เบอร์โทร: {$value['telephone']}";
		    $comma = ',<br>';
		}
		
		$result = [
		    'status' => 'success',
		    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
		    'text' => $text,
		];
		return $result;
	    }
	    $result = [
		    'status' => 'unsuccess',
		    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
		];
		return $result;
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Creates a new MoveUnits model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = new MoveUnits();
	    $model->user_id = Yii::$app->user->id;
	    $model->sitecode_old = Yii::$app->user->identity->userProfile->sitecode;
	    $model->status = 'waiting';
	    $sitecode = '';
	    $file_old = $model->file;
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model->file = UploadedFile::getInstance($model, 'file');
		
		if ($model->file !== null) {
		    $nameArr = explode('/', $model->file->type);
		    $lname = $nameArr[1];
		    
		    if($file_old!=''){
			unlink(Yii::$app->basePath . '/../storage/web/source/' . $file_old);
		    }
		    $idName = '';
		    if ($_SERVER["REMOTE_ADDR"] == '::1' || $_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
			    $idName = 'mycom';
		    } else {
			    $idName = $_SERVER["REMOTE_ADDR"];
		    }
		    //date("YmdHis")
		    $nowFileName = 'moveunits_'.date('YmdHis').'.'.$lname;
		    $fullPath = Yii::$app->basePath . '/../storage/web/source/' . $nowFileName;

		    $model->file->saveAs($fullPath);
		    $model->file = $nowFileName;
		} else {
		    $model->file = $secret_file_old;
		}
		
		if ($model->save()) {
		    $user = \common\models\User::find()->where('id=:user_id',[':user_id'=>$model['user_id']])->one();
//		    $sendMailCC = Yii::$app->commandBus->handle(new \common\commands\command\SendEmailCommand([
//			'from' => [Yii::$app->params['adminEmail'] => Yii::$app->params['adminEmail']],
//			'to' => \backend\modules\ezforms\components\EzformFunc::getEmailSite(),
//			'subject' => 'User sign up ('.$user->userProfile->firstname.' '.$user->userProfile->lastname.')',
//			'view' => 'move',
//			'params' => ['data'=>$model, 'user' => $user->userProfile, 'username'=>$user->username]
//		    ]));
//		    
		    Yii::$app->mailer->compose('@app/mail/move',[
			'data'=>$model, 
			'user' => $user->userProfile, 
			'username'=>$user->username,
			'status' => 'รออนุมัติ',
		    ])
		    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')])
		    ->setTo($user->email)
		    ->setCc(\backend\modules\ezforms\components\EzformFunc::getEmailSite())
		    ->setSubject('ขอย้ายหน่วยงาน')
		    ->attach(Yii::getAlias('@storageUrl').'/source/'.$model->file)
		    ->send();

		    $result = [
			'status' => 'success',
			'action' => 'create',
			'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('create', [
		    'model' => $model,
		    'sitecode' => $sitecode,
		]);
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Updates an existing MoveUnits model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    $model = $this->findModel($id);
	    
	    if ($model->load(Yii::$app->request->post())) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if ($model->save()) {
		    
		    $status = 'ไม่อนุมัติ';
		    if($model->status=='approve'){
			$status = 'อนุมัติ';
			$secret_file = Yii::$app->user->identity->userProfile->secret_file;
			
			//@unlink(Yii::$app->basePath . '/../storage/web/source/' . $secret_file);
			Yii::$app->authManager->revokeAll($model->user_id);
			
			Yii::$app->db->createCommand()->update('user_profile', [
			    'sitecode'=>$model->sitecode_new,
			    'department'=>$model->sitecode_new,
			    'secret_file'=>$model->file,
			],'user_id=:user_id', [':user_id' => $model->user_id])->execute();
		    }
		    
		    $user = \common\models\User::find()->where('id=:user_id',[':user_id'=>$model['user_id']])->one();
		    
		    Yii::$app->mailer->compose('@app/mail/move',[
			'data'=>$model, 
			'user' => $user->userProfile, 
			'username'=>$user->username,
			'status' => $status,
		    ])
		    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')])
		    ->setTo($user->email)
		    ->setCc(\backend\modules\ezforms\components\EzformFunc::getEmailSite())
		    ->setSubject('ขอย้ายหน่วยงาน')
		    //->attach(Yii::getAlias('@storageUrl').'/source/'.$model->file)
		    ->send();
		    
		    $result = [
			'status' => 'success',
			'action' => 'update',
			'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
			'data' => $model,
		    ];
		    return $result;
		} else {
		    $result = [
			'status' => 'error',
			'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not update the data.'),
			'data' => $model,
		    ];
		    return $result;
		}
	    } else {
		return $this->renderAjax('update', [
		    'model' => $model,
		]);
	    }
	} else {
	    $model = $this->findModel($id);

	    if ($model->load(Yii::$app->request->post())) {
		if($model->save()){
		    $status = 'ไม่อนุมัติ';
		    if($model->status=='approve'){
			$status = 'อนุมัติ';
			Yii::$app->db->createCommand()->update('user_profile', ['sitecode'=>$model->sitecode_new, 'department'=>$model->sitecode_new],'user_id=:user_id', [':user_id' => $model->user_id])->execute();
		    }
		    $user = \common\models\User::find()->where('id=:user_id',[':user_id'=>$model['user_id']])->one();
		    
		    Yii::$app->mailer->compose('@app/mail/move',[
			'data'=>$model, 
			'user' => $user->userProfile, 
			'username'=>$user->username,
			'status' => $status,
		    ])
		    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')])
		    ->setTo($user->email)
		    ->setCc(\backend\modules\ezforms\components\EzformFunc::getEmailSite())
		    ->setSubject('ขอย้ายหน่วยงาน')
		    //->attach(Yii::getAlias('@storageUrl').'/source/'.$model->file)
		    ->send();
		}
		
		return $this->redirect(['view', 'id' => $model->muid]);
	    } else {
		return $this->render('update', [
		    'model' => $model,
		]);
	    }
	}
    }
    
    public function getSite($sitecode){
	
        $sqlHospital = "SELECT `hcode` as code,`name` FROM all_hospital_thai WHERE hcode=:sitecode ";
        $dataHospital = Yii::$app->db->createCommand($sqlHospital, [':sitecode'=>$sitecode])->queryOne();
	return $dataHospital;
    }

    /**
     * Deletes an existing MoveUnits model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	if (Yii::$app->getRequest()->isAjax) {
	    Yii::$app->response->format = Response::FORMAT_JSON;
	    if ($this->findModel($id)->delete()) {
		$result = [
		    'status' => 'success',
		    'action' => 'update',
		    'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
		    'data' => $id,
		];
		return $result;
	    } else {
		$result = [
		    'status' => 'error',
		    'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not delete the data.'),
		    'data' => $id,
		];
		return $result;
	    }
	} else {
	    throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }

    /**
     * Finds the MoveUnits model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MoveUnits the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MoveUnits::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
