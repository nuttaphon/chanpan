<?php

/**
 * Eugine Terentev <eugine@terentev.net>
 */

namespace backend\controllers;

use Yii;
use yii\caching\Cache;
use yii\caching\TagDependency;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Class CacheController
 * @package backend\controllers
 */
class DmpController extends Controller {

    /**
     * @return string
     */
    public function actionIndex() {
	if (!Yii::$app->user->can('administrator')) {

	    return 'ไม่มีสิทธิ์ใช้หน้านี้';
	}

	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit', '512M');

	return $this->render('index');
    }

    public function actionUpmember() {
	if (!Yii::$app->user->can('administrator')) {

	    return 'ไม่มีสิทธิ์ใช้หน้านี้';
	}

	ini_set('max_execution_time', 0);
	set_time_limit(0);
	ini_set('memory_limit', '512M');


	$sql = " SELECT dep_member.`name`, 
			dep_member.`level`, 
			dep_member.username, 
			dep_member.id, 
			dep_member.`code`
		FROM dep_member
	   ";

	$data = Yii::$app->dbsr->createCommand($sql)->queryAll();

	$all = 0;
	$num = 0;

	if ($data) {
	    foreach ($data as $key => $value) {
		$all++;

		$sql = " SELECT user_profile.user_id, 
		user_profile.firstname, 
		user_profile.lastname
	FROM user_profile
	WHERE '{$value['name']}' like concat(user_profile.firstname, '%') AND '{$value['name']}' like concat('%', user_profile.lastname, '%')
		   ";

		$data_user = Yii::$app->dbdmp->createCommand($sql)->queryOne();
		if ($data_user) {

		    $saveSql = Yii::$app->dbsr->createCommand($sql)->update('dep_member', [
			'user_id'=>$data_user['user_id']
				    ], "id = {$value['id']}")->execute();
				    
		    if ($saveSql) {
			$num++;
		    }
		} else {
		    echo $value['username'].', ';
		}
	    }

	    echo $num . '/' . $all;
	}
    }

}
