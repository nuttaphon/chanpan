<?php

namespace backend\controllers;

use Yii;
use backend\models\ActCategory;
use backend\models\ActCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use common\lib\sdii\SDFunc;
/**
 * ActCategoryController implements the CRUD actions for ActCategory model.
 */
class ActCategoryController extends Controller
{
    public function behaviors()
    {
        return [
        'access' => [
        'class' => AccessControl::className(),
        'rules' => [
            [
            'allow' => true,
            'actions' => ['index', 'view', 'terms'],
            'roles' => ['?', '@'],
            ],
            [
            'allow' => true,
            'actions' => ['create', 'update', 'delete'],
            'roles' => ['@'],
            ],
        ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
    if (parent::beforeAction($action)) {
        if (in_array($action->id, array('create', 'update'))) {

        }
        return true;
    } else {
        return false;
    }
    }

    /**
     * Lists all ActCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
    $type = (isset($_GET['type']) ? $_GET['type'] : 1);

        $searchModel = new ActCategorySearch();
        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams, $type);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        'type' => $type,
        ]);
    }

    /**
     * Displays a single ActCategory model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
    if (Yii::$app->getRequest()->isAjax) {
        return $this->renderAjax('view', [
        'model' => $this->findModel($id),
        ]);
    } else {
        throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
    }
    }

    public function actionTerms()
    {
    if (Yii::$app->getRequest()->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $taxonomy = SDFunc::getTaxonomyDropDownList(0, 1);
        $html = '<option value>none</option>';
        foreach ($taxonomy as $key => $value) {
            $html .= '<option value="' . $key . '">' . $value . '</option>';
        }
        $result = [
        'status' => 'success',
        'action' => 'create',
        'message' => '<strong><i class="glyphicon glyphicon-info-sign"></i> Success!</strong> ' . Yii::t('app', 'Loading Completed.'),
        'content' => $html,
        ];
        return $result;
    } else {
        throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
    }
    }
    /**
     * Creates a new ActCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    if (Yii::$app->getRequest()->isAjax) {
        $type = (isset($_GET['type']) ? $_GET['type'] : 1);

        $model = new ActCategory();

        if ($model->load(Yii::$app->request->post())) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->act_id = SDFunc::genID();
        $model->act_type = $type;
        $model->user_create = Yii::$app->user->id;
        $model->date_create = date('Y-m-d H:i:s');
        $model->user_update = Yii::$app->user->id;
        $model->date_update = date('Y-m-d H:i:s');

        if ($model->save()) {
            $result = [
            'status' => 'success',
            'action' => 'create',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'data' => $model,
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not create the data.'),
            'data' => $model,
            ];
            return $result;
        }
        } else {
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
        }
    } else {
        throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
    }
    }

    /**
     * Updates an existing ActCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    if (Yii::$app->getRequest()->isAjax) {
        $type = (isset($_GET['type']) ? $_GET['type'] : 1);

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->save()) {
            $model->user_update = Yii::$app->user->id;
            $model->date_update = date('Y-m-d H:i:s');
            $result = [
            'status' => 'success',
            'action' => 'update',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Data completed.'),
            'data' => $model,
            ];
            return $result;
        } else {
            $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Can not update the data.'),
            'data' => $model,
            ];
            return $result;
        }
        } else {
        return $this->renderAjax('update', [
            'model' => $model,
        ]);
        }
    } else {
        throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
    }
    }

    /**
     * Deletes an existing ActCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    if (Yii::$app->getRequest()->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($this->findModel($id)->delete()) {
        $result = [
            'status' => 'success',
            'action' => 'update',
            'message' => '<strong><i class="glyphicon glyphicon-remove-sign"></i> Success!</strong> ' . Yii::t('app', 'Deleted completed.'),
            'data' => $id,
        ];
        return $result;
        } else {
        $result = [
            'status' => 'error',
            'content' => '<strong><i class="glyphicon glyphicon-warning-sign"></i> Error!</strong> ' . Yii::t('app', 'Can not delete the data.'),
            'data' => $id,
        ];
        return $result;
        }
    } else {
        throw new NotFoundHttpException('Invalid request. Please do not repeat this request again.');
    }
    }

    public function actionTree()
    {
        $type = (isset($_GET['type']) ? $_GET['type'] : 1);

        $trees = ActCategory::find()
                            ->all();

        return $this->render('tree', [
        'type' => $type,
        'trees' => $trees,
        ]);
    }

    /**
     * Finds the ActCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ActCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ActCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
