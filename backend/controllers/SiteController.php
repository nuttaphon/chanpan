<?php
namespace backend\controllers;

use common\components\keyStorage\FormModel;
use Yii;

/**
 * Site controller
 */
class SiteController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function beforeAction($action)
    {
        $this->layout = Yii::$app->user->isGuest ? 'base' : 'common';
        return parent::beforeAction($action);
    }

    public function actionViewImg($img)
    {
	return $this->renderAjax('_img_tool', [
		    'img' => $img,
		]);
    }
    
    public function actionSendMail()
    {
	//mail bot
//	$sql = "SELECT user_profile.*, `user`.email AS email2
//		FROM `user` INNER JOIN user_profile ON `user`.id = user_profile.user_id
//			 INNER JOIN (SELECT user_id, 
//					GROUP_CONCAT(item_name) AS items
//				FROM rbac_auth_assignment 
//				GROUP BY user_id) tmp ON `user`.id = tmp.user_id
//		WHERE ((user_profile.citizenid_file = '' AND user_profile.secret_file = '' AND user_profile.approved<>1) OR (user_profile.approved=2)) AND DATEDIFF(CURRENT_DATE, FROM_UNIXTIME(`user`.created_at)) >=20 AND items NOT LIKE '%administrator%'
//		GROUP BY `user`.username Limit 2
//		";
//	
//	$data = Yii::$app->db->createCommand($sql)->queryAll();
//	
//	if($data){
//	    foreach ($data as $key => $value) {
//		$sendMail = Yii::$app->commandBus->handle(new \common\commands\command\SendEmailCommand([
//		    'from' => [Yii::$app->params['adminEmail'] => Yii::$app->keyStorage->get('sender')],
//		    'to' => $value['email2'],
//		    'subject' => 'กรุณาอัพโหลดเอกสารยืนยันตัวตน',
//		    'view' => 'alermail',
//		    'params' => ['user' => $value]
//		]));
//		
//		$dataSave = Yii::$app->db->createCommand()->update('user_profile', ['activate'=>1], 'user_id=:user_id', [':user_id'=>$value['user_id']])->execute();
//	    }
//	    
//	}
	
	//return png
	$imgname = 'mailBot';
	$im = imagecreatefrompng($imgname);
	if(!$im)
	{
	    $im  = imagecreatetruecolor(1, 1);
	    imagesavealpha($im, true); 
	    $color = imagecolorallocatealpha($im,0x00,0x00,0x00,127); 
	    imagefill($im, 0, 0, $color); 
	}
	header('Content-Type: image/png');
	imagepng($im);
	imagedestroy($im);
    }
    
    public function actionCheckfile(){
        if (Yii::$app->getRequest()->isAjax) {
	   Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	   $userProfile = Yii::$app->user->identity->userProfile;
	   $date = Yii::$app->formatter->asDate(Yii::$app->user->identity->created_at, 'php:Y-m-d H:i:s');
	   
	   $str = \common\lib\sdii\components\utils\SDdate::getDiffDay($date, 10);
	   $strLimit = \common\lib\sdii\components\utils\SDdate::getCheckDiffDay($date, 1);
	   
	   if( $userProfile->approved!=1){
	       if($userProfile->approved==2 || ($userProfile->citizenid_file=='' && $userProfile->secret_file=='')){
		    if(!$strLimit){
			
		    $user = Yii::$app->user;
		    $userComment = \backend\models\UserComment::find()->where('user_id=:user_id', [':user_id'=>$user->id])->limit(1)->orderBy('update_at DESC')->one();

		    $strComment = '<strong>หมายเหตุ : </strong>'.$userComment['comment'];
		   $html = $this->renderPartial('_alert', ['time'=>$str, 'comment'=>$strComment]);
		    $result = [
			 'message' => $html,
		     ];
		     return $result;
		     }
		}
	   }
	    
	} else {
	    throw new \yii\web\NotFoundHttpException('Invalid request. Please do not repeat this request again.');
	}
    }
    
    public function actionCanceluser()
    {
        //Yii::$app->authManager->revokeAll($id);
	$id = Yii::$app->user->id;
	if($id>0){
	    $model = \common\models\User::find()->where('id=:id',[':id'=>$id])->one();

	    $model->status = 0;
	    $model->status_del = 1;
	    $model->save();


	    Yii::$app->user->logout();
	    setcookie("CloudUserID", Yii::$app->user->identity->userProfile->user->username, time()-1, "/", Yii::$app->keyStorage->get('frontend.domain'), false);

	}
        
	return $this->goHome();
    }
    
    public function actionReport1(){
        return $this->render('report/report1');
    }

    public function actionSettings()
    {
        $model = new FormModel([
            'keys' => [
                'frontend.maintenance' => [
                    'label' => Yii::t('backend', 'Frontend maintenance mode'),
                    'type' => FormModel::TYPE_DROPDOWN,
                    'items' => [
                        'disabled' => Yii::t('backend', 'Disabled'),
                        'enabled' => Yii::t('backend', 'Enabled')
                    ]
                ],
                'backend.theme-skin' => [
                    'label' => Yii::t('backend', 'Backend theme'),
                    'type' => FormModel::TYPE_DROPDOWN,
                    'items' => [
                        'skin-black' => 'skin-black',
                        'skin-blue' => 'skin-blue',
                        'skin-green' => 'skin-green',
                        'skin-purple' => 'skin-purple',
                        'skin-red' => 'skin-red',
                        'skin-yellow' => 'skin-yellow'
                    ]
                ],
                'backend.layout-fixed' => [
                    'label' => Yii::t('backend', 'Fixed backend layout'),
                    'type' => FormModel::TYPE_CHECKBOX
                ],
                'backend.layout-boxed' => [
                    'label' => Yii::t('backend', 'Boxed backend layout'),
                    'type' => FormModel::TYPE_CHECKBOX
                ],
                'backend.layout-collapsed-sidebar' => [
                    'label' => Yii::t('backend', 'Backend sidebar collapsed'),
                    'type' => FormModel::TYPE_CHECKBOX
                ]
            ]
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', [
                'body' => Yii::t('backend', 'Settings was successfully saved'),
                'options' => ['class' => 'alert alert-success']
            ]);
            return $this->refresh();
        }

        return $this->render('settings', ['model' => $model]);
    }

    public function actionRedirect($path){
        $url = yii\helpers\Url::to([$path], true);
        echo "$path : $url";
        self::redirect($url, 302);
    }
}
