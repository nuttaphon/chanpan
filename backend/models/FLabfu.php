<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_labfu".
 *
 * @property string $SITECODE
 * @property string $HOSPCODE
 * @property string $PID
 * @property string $SEQ
 * @property string $DATE_SERV
 * @property string $LABTEST
 * @property string $LABRESULT
 * @property string $CONFIRM
 * @property string $D_UPDATE
 * @property string $ptlink
 */
class FLabfu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_labfu';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbtdc2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SITECODE', 'HOSPCODE', 'PID', 'SEQ', 'LABTEST'], 'required'],
            [['SITECODE', 'HOSPCODE'], 'string', 'max' => 9],
            [['PID'], 'string', 'max' => 11],
            [['SEQ'], 'string', 'max' => 13],
            [['DATE_SERV'], 'string', 'max' => 15],
            [['LABTEST'], 'string', 'max' => 2],
            [['LABRESULT'], 'string', 'max' => 366],
            [['CONFIRM'], 'string', 'max' => 1],
            [['D_UPDATE'], 'string', 'max' => 19],
            [['ptlink'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'SITECODE' => 'Sitecode',
            'HOSPCODE' => 'Hospcode',
            'PID' => 'Pid',
            'SEQ' => 'Seq',
            'DATE_SERV' => 'Date  Serv',
            'LABTEST' => 'Labtest',
            'LABRESULT' => 'Labresult',
            'CONFIRM' => 'Confirm',
            'D_UPDATE' => 'D  Update',
            'ptlink' => 'Ptlink',
        ];
    }

    /**
     * @inheritdoc
     * @return FLabfuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FLabfuQuery(get_called_class());
    }
}
