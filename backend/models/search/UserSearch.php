<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'status_del', 'created_at', 'updated_at', 'logged_at'], 'integer'],
            [['approve_profile','notfile','username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'role', 'approve', 'difftime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'logged_at' => $this->logged_at
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
    
    public function searchSite($params, $sitecode, $volunteer)
    {
	$this->load($params);
	
	if(isset($this->role)){
	    $paramsStr = '';
	    $paramsArr = [];
	
	    if(in_array($this->role, ['0','1'])){
		$paramsStr .= ' user_profile.status = :status ';
		$paramsArr[':status'] = (int)$this->role;
	    } elseif (in_array($this->role, ['2','3','4','10','11'])) {
		$paramsStr .= ' user_profile.status_personal = :status_personal ';
		$paramsArr[':status_personal'] = (int)$this->role;
	    } elseif (in_array($this->role, ['5','6','7','8','9'])) {
		$paramsStr .= ' user_profile.status_manager = :status_manager ';
		$paramsArr[':status_manager'] = (int)$this->role;
	    }
	    
	    $query = User::find()->where($paramsStr, $paramsArr)->innerJoin('user_profile', "user_profile.user_id = user.id AND user_profile.sitecode = '$sitecode'");
	} else {
	    $query = User::find()->innerJoin('user_profile', "user_profile.user_id = user.id AND user_profile.sitecode = '$sitecode'");
	}
	
	$query->andWhere(" user.username LIKE :username OR user_profile.sitecode LIKE :username OR concat(user_profile.firstname, '', user_profile.lastname) LIKE :username", [':username'=>"%$this->username%"]);
	
	$query->andWhere('status_del=0');
	
	$query->andWhere('volunteer_status=:volunteer_status', [':volunteer_status'=>$volunteer]);
	
	if($this->approve == '1'){
	    $query->innerJoin('(
		SELECT user_id, 
			GROUP_CONCAT(item_name) AS items
		FROM rbac_auth_assignment 
		GROUP BY user_id
		) tmp ', '`user`.id = tmp.user_id')->andWhere("items NOT LIKE '%manager%' AND items NOT LIKE '%administrator%' AND items NOT LIKE '%adminsite%' AND items NOT LIKE '%frontend%'")
		    ->groupBy('user.username');
	}
	
	$query->orderBy('`user`.created_at DESC');
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'logged_at' => $this->logged_at
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
    
    public function searchManage($params,$del)
    {
	$this->load($params);
	
	if(isset($this->role)){
	    $paramsStr = '';
	    $paramsArr = [];
	
	    if(in_array($this->role, ['0','1'])){
		$paramsStr .= ' user_profile.status = :status ';
		$paramsArr[':status'] = (int)$this->role;
	    } elseif (in_array($this->role, ['2','3','4','10','11'])) {
		$paramsStr .= ' user_profile.status_personal = :status_personal ';
		$paramsArr[':status_personal'] = (int)$this->role;
	    } elseif (in_array($this->role, ['5','6','7','8','9'])) {
		$paramsStr .= ' user_profile.status_manager = :status_manager ';
		$paramsArr[':status_manager'] = (int)$this->role;
	    }
	    
	    $query = User::find()->where($paramsStr, $paramsArr)->innerJoin('user_profile', 'user_profile.user_id = user.id');
	} else {
	    $query = User::find()->innerJoin('user_profile', 'user_profile.user_id = user.id');
	}
	
	$query->andWhere(" user.username LIKE :username OR user_profile.sitecode LIKE :username OR concat(user_profile.firstname, '', user_profile.lastname) LIKE :username", [':username'=>"%$this->username%"]);
	
	if($this->difftime != '' || $this->difftime>0){
	    $query->andWhere("DATEDIFF(CURRENT_DATE, FROM_UNIXTIME(user.created_at)) >= :difftime", [':difftime'=>$this->difftime]);
	}
	
	if($this->approve == '1'){
	    $query->leftJoin('(
		SELECT user_id, 
			GROUP_CONCAT(item_name) AS items
		FROM rbac_auth_assignment 
		GROUP BY user_id
		) tmp ', '`user`.id = tmp.user_id')->andWhere("items NOT LIKE '%manager%' AND items NOT LIKE '%administrator%' AND items NOT LIKE '%adminsite%' AND items NOT LIKE '%frontend%'")
		    ->groupBy('user.username');
	}
	
	if($this->notfile == '1'){
	    $query->andWhere("(user_profile.secret_file is null OR user_profile.secret_file='') AND (user_profile.citizenid_file is null OR user_profile.citizenid_file='') ");
	}
	
	if($this->approve_profile>=0){
	    $query->andWhere("user_profile.approved=:approved", [':approved'=>$this->approve_profile]);
	} elseif ($this->approve_profile==-1) {
	    $query->andWhere("user_profile.approved IS NULL");
	}
	
	$query->andWhere("enroll_key <> 'GenModule' AND status_del=:del", [':del'=>$del]);
	
	$query->orderBy('`user`.created_at DESC');
	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
//
//        $query->andFilterWhere([
//            'user.id' => $this->id,
//            'user.status' => $this->status,
//            'user.created_at' => $this->created_at,
//            'user.updated_at' => $this->updated_at,
//            'user.logged_at' => $this->logged_at
//        ]);
//
//        $query->andFilterWhere(['like', 'user.username', $this->username])
//            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
//            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
//            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
//            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
