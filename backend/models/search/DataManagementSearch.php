<?php

namespace backend\models\search;

use backend\modules\ezforms\components\EzformQuery;
use backend\modules\ezforms\models\EzformFields;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * TimelineEventSearch represents the model behind the search form about `common\models\TimelineEvent`.
 */
class DataManagementSearch extends \backend\modules\ezforms\models\EzformDynamic
{
    /**
     * @inheritdoc
     */


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ezfdata)
    {
        //VarDumper::dump($params,10,true); exit;
        $ezform = \backend\modules\ezforms\components\EzformQuery::getFormTableName($ezfdata['ezf_id']);
        $model = new \backend\modules\ezforms\models\EzformDynamic($ezform->ezf_table);
        $model = $model->find();

        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        $arrWhere = [
            0 => 'and',
        ];

        if($params['DataManagementSearch']) {
            $fields = $params['DataManagementSearch'];
            $i = 1;
            foreach ($fields as $key => $val) {
                $arrWhere[$i][] = 'like';
                $arrWhere[$i][] = $key;
                $arrWhere[$i++][] = $val;
            }
            $model->where = $arrWhere;
        }

        if($ezfdata['xsourcex']){
            $model->andWhere(['xsourcex'=>Yii::$app->user->identity->userProfile->sitecode]);
        }

        if($params['start_date'] && $params['end_date']){
            $start_date = explode('/', $params['start_date']);
            $start_date = ($start_date[2] - 543) . '-' . $start_date[1] . '-' . $start_date[0];
            $end_date = explode('/', $params['end_date']);
            $end_date = ($end_date[2] - 543) . '-' . $end_date[1] . '-' . $end_date[0];
        }else {
            $start_date = date('Y-m-01');
            $end_date = date('Y-m-d');
        }

        if($params['show']!='all'){
            $model->andWhere("`".$ezfdata['fsearch']."` BETWEEN :start_date AND :end_date", [':start_date'=>$start_date, ':end_date'=>$end_date]);
        }

        //re change data {}
        $paramsFields = explode(',', $ezfdata['params']);
        $dataProviderTotal = $dataProvider->count;
        for($i=0;$i<$dataProviderTotal;$i++){
            foreach ($paramsFields as $key => $field){
                $ezfield = Yii::$app->db->createCommand("select ezf_field_id, ezf_field_sub_id,  ezf_field_name, ezf_field_type, ezf_component from ezform_fields where ezf_id=:ezf_id and ezf_field_name=:ezf_field_name", [':ezf_id'=>$ezfdata['ezf_id'], ':ezf_field_name' => $field])->queryOne();
                if($ezfield['ezf_field_type']==4 || $ezfield['ezf_field_type']==6 ){
                    $ezchoice = Yii::$app->db->createCommand("select ezf_choicelabel from ezform_choice where ezf_choicevalue=:ezf_choicevalue and ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_id'], ':ezf_choicevalue' => $dataProvider->models[$i]->{$ezfield['ezf_field_name']}])->queryOne();
                    $dataProvider->models[$i]->{$ezfield['ezf_field_name']}= $ezchoice['ezf_choicelabel'];
                }
                else if($ezfield['ezf_field_type']==10){
                    $ezformComponents = EzformQuery::getFieldComponent($ezfield['ezf_component']);
                    $ezformComp = EzformQuery::getFormTableName($ezformComponents->ezf_id);
                    $field_desc_array = explode(',', $ezformComponents->field_id_desc);
                    $field_key = EzformFields::find()->select('ezf_field_name')->where(['ezf_field_id' => $ezformComponents->field_id_key])->one();

                    $field_desc_list = '';
                    if(count($field_desc_array)){
                        foreach ($field_desc_array as $value) {
                            if (!empty($value)) {
                                $field_desc = EzformFields::find()->where(['ezf_field_id' => $value])->one();
                                $field_desc_list .= 'COALESCE('.$field_desc->ezf_field_name.", '-'), ' ',";
                            }
                        }
                    }
                    $field_desc_list = substr($field_desc_list, 0 ,-6);
                    $compData = Yii::$app->db->createCommand('SELECT CONCAT('.$field_desc_list.') AS field_desc FROM '.($ezformComp->ezf_table).' WHERE '.$field_key->ezf_field_name.'=:field_key', [':field_key' => $dataProvider->models[$i]->$ezfield['ezf_field_name']])->queryOne();
                    $dataProvider->models[$i]->{$ezfield['ezf_field_name']} = $compData['field_desc'];
                }
                else if($ezfield['ezf_field_type']==231){
                    $ezchoice = Yii::$app->db->createCommand("select ezf_choicelabel from ezform_choice where ezf_choicevalue=:ezf_choicevalue and ezf_field_id=:ezf_field_id", [':ezf_field_id'=>$ezfield['ezf_field_sub_id'], ':ezf_choicevalue' => $dataProvider->models[$i]->{$ezfield['ezf_field_name']}])->queryOne();
                    $dataProvider->models[$i]->$ezfield['ezf_field_name']= $ezchoice['ezf_choicelabel'];
                }
                else if($ezfield['ezf_field_type']==16 || ($ezfield['ezf_field_type'] == 0 && $ezfield['ezf_field_sub_id'])){
                    if( ($dataProvider->models[$i]->{$ezfield['ezf_field_name']}) == '1')
                        $dataProvider->models[$i]->{$ezfield['ezf_field_name']} = 'ใช่';
                    else if( ($dataProvider->models[$i]->{$ezfield['ezf_field_name']}) == '0')
                    $dataProvider->models[$i]->{$ezfield['ezf_field_name']} =  'ไม่ใช่';
                }
            }
        }
        //VarDumper::dump($dataProvider->models,10,true);exit;
        return $dataProvider;
    }
}
