<?php

namespace backend\models;

use Yii;

class Tbdata extends \yii\db\ActiveRecord
{
    protected static $table;
    public $field_desc;
    
    public function _construct($table, $scenario)
    {
        parent::_construct($scenario);
        self::$table = $table;
    }
    public function attributes()
    {
	$attrDB = array_keys(static::getTableSchema()->columns);
	$moreColumn = [];
	
	if(isset(Yii::$app->session['data_export_tmp']) && !empty(Yii::$app->session['data_export_tmp'])){
	    $dataExport = Yii::$app->session['data_export_tmp'];
	    $lable_extra = \yii\helpers\Json::decode($dataExport['lable_extra']);
	    if(is_array($lable_extra)){
		
		foreach ($lable_extra as $keyMore => $valueMore) {
		    if(!in_array($keyMore, $attrDB)){
			$moreColumn[] = $keyMore;
		    }
		}
		
	    }
	}
	
	return array_merge($attrDB, $moreColumn);
    }
    public static function tableName()
    {
        return self::$table;
    }

    /* UPDATE */
    public static function setTableName($table)
    {
        self::$table = $table;
    }

    /**
     * @inheritdoc
     * @return TbdataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TbdataQuery(get_called_class());
    }
}
