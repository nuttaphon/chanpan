<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "widget_backend_menu".
 *
 * @property integer $id
 * @property string $titlename
 * @property string $url
 * @property string $type
 */
class WidgetBackendMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget_backend_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titlename', 'url','target', 'type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'key หลัก',
            'titlename' => 'ชื่อเมนู',
            'url' => 'URL Link',
            'target' => 'ลักษณะของ Link',
            'type' => 'ประเภท Link',
        ];
    }
}
