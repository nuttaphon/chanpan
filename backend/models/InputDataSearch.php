<?php

namespace backend\models;

use backend\modules\ezforms\components\EzformQuery;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ezforms\models\Ezform;
use yii\helpers\Url;
use yii\helpers\VarDumper;


/**
 * EzformSearch represents the model behind the search form about `backend\modules\ezforms\models\Ezform`.
 */
class InputDataSearch extends Ezform {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['ezf_id', 'user_create', 'user_update', 'status', 'shared', 'public_listview', 'public_edit', 'public_delete'], 'integer'],
            [['ezf_name', 'ezf_detail', 'ezf_table', 'create_date', 'update_date', 'username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search() {

        $query = Ezform::find()
                ->select('ezform.*,`ezform_favorite`.userid,`ezform_favorite`.status AS favorite_status, `ezform_favorite`.forder AS favorite_forder')
                ->innerJoin('ezform_favorite', '`ezform`.`ezf_id` = `ezform_favorite`.`ezf_id`')
                ->where('ezform.`status` <> :status AND ezform_favorite.userid = :userid', [':status' => 3, ':userid' => Yii::$app->user->id])
                ->orderBy('`ezform_favorite`.forder DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $query->count(),
            'pagination' => array(
                'pageSize' => 15,
            ),
        ]);
        
        return $dataProvider;
    }

    public  function searchEMR($target){
        $target = base64_decode($target);

        //ดูดข้อมูลของเป้าหมายจาก EMR
        if(Yii::$app->keyStorage->get('frontend.domain') == 'dpmcloud.org'){
            $res = Yii::$app->db->createCommand("SELECT ezf_local, ezf_remote FROM ezform_sync;")->queryAll();
            foreach ($res as $valx){
                $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id'=>$valx['ezf_remote']])->queryOne();
                $model = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `".$ezform['ezf_table']."` WHERE  ptid = :ptid", [':ptid'=>$target])->queryAll();
                if($model[0]['id']) {
                    $keys_tb = '';
                    foreach ($model[0] as $key => $val) {
                        $keys_tb .= "`{$key}`, ";
                    }
                    $keys_tb = substr($keys_tb, 0, -2);

                    foreach ($model as $valy) {
                        $values = '';
                        foreach ($valy as $key => $val) {
                            $values .= "'{$val}', ";
                        }
                        $values = substr($values, 0, -2);
                        //VarDumper::dump($values,10,true); exit;

                        $ezform = Yii::$app->dbcascaputf8->createCommand("SELECT ezf_table FROM `ezform` WHERE  ezf_id = :ezf_id", [':ezf_id' => $valx['ezf_local']])->queryOne();
                        Yii::$app->db->createCommand("replace into `" . $ezform['ezf_table'] . "` ({$keys_tb}) VALUES ({$values});")->execute();

                        //save EMR
                        $ezform_target = Yii::$app->dbcascaputf8->createCommand("SELECT * FROM `ezform_target` WHERE  data_id=:data_id AND ezf_id=:ezf_id;", [':data_id' => $valy['id'], ':ezf_id' => $valx['ezf_local']])->queryOne();
                        if($ezform_target['data_id']) {
                            $values = '';
                            $keys = '';
                            foreach ($ezform_target as $key => $val) {
                                $values .= "'{$val}', ";
                                $keys .= "`{$key}`, ";
                            }
                            $values = substr($values, 0, -2);
                            $keys = substr($keys, 0, -2);
                            //VarDumper::dump($keys,10,true); exit;
                            Yii::$app->db->createCommand("replace into `ezform_target` ({$keys}) VALUES ({$values});")->execute();
                        }
                    }
                }
            }
        }

        $query = EzformTarget::find()->where('(target_id = :target AND rstat <>3)', [':target' => $target,]);
        //$query = EzformTarget::find()->where('(target_id = :target AND (rstat >= 4 OR  rstat = 2)) OR (xsourcex = :xsourcex AND target_id = :target AND rstat <> 3)', [':target' => $target, ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode])->orderBy(['create_date' => SORT_ASC]);

        $dataProvidertarget = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $query->count(),
            'sort' => [
                'defaultOrder' => ['create_date' => SORT_ASC],
            ],
            'pagination' => array(
                'pageSize' => 10,
            ),
        ]);

        return $dataProvidertarget;
    }

    public function searchTarget($ezf_id=null, $target=null, $rstat=null, $comp_target=null) {

        $ezform = Yii::$app->db->createCommand("SELECT ezf_table FROM ezform WHERE ezf_id=:ezf_id;", [':ezf_id'=>$ezf_id])->queryOne();
        $ezf_table = $ezform['ezf_table'];
        $xdepartmentx = Yii::$app->user->identity->userProfile->department_area;
        $xdepartmentx_sql = $xdepartmentx > 0 ? ("AND xdepartmentx ='".$xdepartmentx."'") : null;

        if ($target == 'byme') {
            //$tbdatatarget = Yii::$app->db->createCommand("SELECT id, target FROM ".$ezf_table." WHERE LENGTH(target)<=0;")->queryAll();
            //$tbdatatarget = $tbdatatarget['target'];
            $query = new Dynamic();
            $query->setTableName($ezf_table);
            //$sql = "select id, target, create_date, update_date FROM ".$ezf_table." WHERE LENGTH(target)<=0";
            //$query = $query->findBySql($sql)->all();
            // OR trim(target)=\'\'
             $query = $query->find()
                    ->where('rstat <> 3 AND user_create = :user_create AND xsourcex = :xsourcex '.$xdepartmentx_sql, [
                        ':user_create' => Yii::$app->user->id,
                        ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode
                    ]);
            $ezform = EzformQuery::checkIsTableComponent($ezf_id);
            if($ezform['special']){
                $query= $query->orderBy(['hptcode' => SORT_ASC]);
            }
            //->findByCondition('target<>""');
            //->all();
             //\yii\helpers\VarDumper::dump($query->createCommand()->sql, 10, TRUE);
               //Yii::$app->end();

            $dataProvidertarget = new ActiveDataProvider([
                'query' => $query,
                'totalCount' => $query->count(),
                //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ]);

       
            return $dataProvidertarget;
        } else if ($target == 'skip') {

            $query = new Dynamic();
            $query->setTableName($ezf_table);

            $query = $query->find()
                ->where('target is null AND rstat <> 3 AND xsourcex = :xsourcex '.$xdepartmentx_sql, [':xsourcex'=>Yii::$app->user->identity->userProfile->sitecode]);
             $ezform = EzformQuery::checkIsTableComponent($ezf_id);
            if($ezform['special']){
                $query= $query->orderBy(['hptcode' => SORT_ASC]);
            }

            $dataProvidertarget = new ActiveDataProvider([
                'query' => $query,
                'totalCount' => $query->count(),
                //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ]);


            return $dataProvidertarget;
        }else if ($target == 'all') {

            $query = new Dynamic();
            $query->setTableName($ezf_table);

            if($rstat) {
                if ($rstat == 'draft') {
                    $rstat = 1;
                } else if ($rstat == 'newrecord') {
                    $rstat = 0;
                } else if ($rstat == 'submitted') {
                    $rstat = 2;
                }

                $query = $query->find()->where('rstat = :rstat AND xsourcex = :xsourcex '.$xdepartmentx_sql, [
                    ':rstat' => $rstat,
                    ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode
                ]);
                $ezform = EzformQuery::checkIsTableComponent($ezf_id);
                if($ezform['special']){
                    $query= $query->orderBy(['hptcode' => SORT_ASC]);
                }
            }else{
                $query = $query->find()->where('rstat <> 3 AND xsourcex = :xsourcex '.$xdepartmentx_sql, [
                    ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode
                ]);
                $ezform = EzformQuery::checkIsTableComponent($ezf_id);
                if($ezform['special']){
                    $query= $query->orderBy(['hptcode' => SORT_ASC]);
                }
            }

            $dataProvidertarget = new ActiveDataProvider([
                'query' => $query,
                'totalCount' => $query->count(),
                //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ]);
       
            return $dataProvidertarget;
        } else {
            //หา ข้อมูลจาก target ที่ฟอร์มโดยตรง
            $query = new Dynamic();
            $query->setTableName($ezf_table);
            $ezform = EzformQuery::checkIsTableComponent($ezf_id);
            if($ezform['special']=='1'){
                $target = base64_decode($target);
                $query = $query->find()->where('ptid = :target AND (rstat <>3)', [':target' => $target,]);

            }else {
                $target = base64_decode($target);
                //$query = $query->find()->where('target = :target AND xsourcex = :xsourcex AND (rstat <> 3)', [':target' => $target, ':xsourcex' => Yii::$app->user->identity->userProfile->sitecode]);
                $query = $query->find()->where('target = :target AND (rstat <> 3)', [':target' => $target]);
            }

            $dataProvidertarget = new ActiveDataProvider([
                'query' => $query,
                'totalCount' => $query->count(),
                //'sort' => ['attributes' => ['ezf_id', 'ezf_name']],
                'sort' => [
                    'defaultOrder' => ['create_date' => SORT_ASC],
                ],
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ]);
            return $dataProvidertarget;
        }
        //------------------------------
    }

}
