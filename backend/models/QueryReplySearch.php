<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\QueryReply;

/**
 * QueryReplySearch represents the model behind the search form about `backend\models\QueryReply`.
 */
class QueryReplySearch extends QueryReply
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'query_id', 'create_by', 'rstat', 'bookmark'], 'integer'],
            [['query_comment', 'create_at', 'update_at', 'file_upload'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = QueryReply::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'query_id' => $this->query_id,
            'create_by' => $this->create_by,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'rstat' => $this->rstat,
            'bookmark' => $this->bookmark,
        ]);

        $query->andFilterWhere(['like', 'query_comment', $this->query_comment])
            ->andFilterWhere(['like', 'file_upload', $this->file_upload]);

        return $dataProvider;
    }
}
