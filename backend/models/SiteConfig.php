<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_config".
 *
 * @property string $sitename
 * @property string $sitemode
 * @property string $updated_at
 */
class SiteConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_at'], 'safe'],
            [['sitename', 'sitemode'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sitename' => 'ชื่อเว็บไซต์',
            'sitemode' => 'โหมดเว็บไซต์',
            'updated_at' => 'Updated At',
        ];
    }

    

}