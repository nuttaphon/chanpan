<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FLabfu]].
 *
 * @see FLabfu
 */
class FLabfuQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FLabfu[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FLabfu|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}