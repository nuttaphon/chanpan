<?php

namespace backend\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "move_units".
 *
 * @property integer $muid
 * @property integer $user_id
 * @property string $sitecode_old
 * @property string $sitecode_new
 * @property string $status
 * @property string $file
 * @property string $comment
 * @property integer $created_by
 * @property string $created_at
 * @property integer $updated_by
 * @property string $updated_at
 */
class MoveUnits extends \yii\db\ActiveRecord
{
    public $user_name;
    public $user_lname;
    public $user;

    public function behaviors() {
	    return [
		    [
			    'class' => TimestampBehavior::className(),
			    'value' => new Expression('NOW()'),
		    ],
		    [
			    'class' => BlameableBehavior::className(),
		    ],
	    ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'move_units';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sitecode_new', 'status', 'file'], 'required'],
            [['user_id', 'created_by', 'updated_by'], 'integer'],
            [['feedback', 'comment'], 'string'],
            [['user_name', 'file', 'created_at', 'updated_at'], 'safe'],
            [['sitecode_old', 'sitecode_new'], 'string', 'max' => 100],
	    //[['file'], 'file', 'skipOnEmpty' => FALSE, 'extensions' => ['pdf','png','jpg','jpeg']],
            [['status'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'muid' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'รหัสผู้ใช้'),
            'sitecode_old' => Yii::t('app', 'หน่วยงานเดิม'),
            'sitecode_new' => Yii::t('app', 'หน่วยงานใหม่'),
            'status' => Yii::t('app', 'สถานะ'),
            'file' => Yii::t('app', 'เอกสารรักษาความลับ'),
            'comment' => Yii::t('app', 'ระบุสาเหตุของการย้ายหน่วยงาน'),
            'created_by' => Yii::t('app', 'สร้างโดย'),
            'created_at' => Yii::t('app', 'สร้างเวลา'),
            'updated_by' => Yii::t('app', 'แก้ไขโดย'),
            'updated_at' => Yii::t('app', 'แก้ไขเวลา'),
	    'user_lname' => Yii::t('app', 'สกุล'),
	    'user_name' => Yii::t('app', 'ชื่อ'),
	    'user' => Yii::t('app', 'Username'),
	    'feedback' => Yii::t('app', 'Feedback'),
	    
        ];
    }
}
