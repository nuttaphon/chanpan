<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Tbdata]].
 *
 * @see Tbdata
 */
class TbdataQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Tbdata[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Tbdata|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}