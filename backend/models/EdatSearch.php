<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Edat;

/**
 * TbdataSearch represents the model behind the search form about `backend\models\Tbdata`.
 */
class EdatSearch extends Edat
{
    protected static $table;
    protected static $ezf_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    public static function setTableName($table)
    {
        self::$table = $table;
    }
    public static function setEZFid($ezf_id)
    {
        self::$ezf_id = $ezf_id;
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Tbdata::find();
        $queryx = new Edat();
        $queryx->setTableName(self::$table);
        $query=$queryx->find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'rstat' => $this->rstat,
            'user_create' => $this->user_create,
            'create_date' => $this->create_date,
            'user_update' => $this->user_update,
            'update_date' => $this->update_date,
        ]);
        $ezfield = Yii::$app->db->createCommand('SELECT * FROM `ezform_fields` WHERE ezf_id =\''.self::$ezf_id.'\'')->query()->readAll();
        if (count($ezfield)>0) {
            foreach($ezfield as $key => $form) {
//            echo $form['ezf_field_name'] . "<br>";

                //echo $form->ezf_field_name;exit;
                //echo $form['ezf_field_name'];exit;
                //echo $this->var1;
                //print_r($form);
                //echo $form[0]->ezf_field_name; exit;
                ////$this[$form['ezf_field_name']];
                //$this->$form['ezf_field_name'];
            //$query->andFilterWhere(['like', $form['ezf_field_name'], $this[$form['ezf_field_name']]]);
            }
        }
//        \yii\helpers\VarDumper::dump($ezfield);exit;
        $query->andFilterWhere(['like', 'xsourcex', $this->xsourcex]);
//            ->andFilterWhere(['like', 'var1', $this->var1])
//            ->andFilterWhere(['like', 'var2', $this->var2])
//            ->andFilterWhere(['like', 'var3', $this->var3])
//            ->andFilterWhere(['like', 'var4', $this->var4])
//            ->andFilterWhere(['like', 'var5', $this->var5])
//            ->andFilterWhere(['like', 'var6', $this->var6])
//            ->andFilterWhere(['like', 'var7', $this->var7])
//            ->andFilterWhere(['like', 'var8', $this->var8])
//            ->andFilterWhere(['like', 'var9', $this->var9])
//            ->andFilterWhere(['like', 'var10', $this->var10])
//            ->andFilterWhere(['like', 'var11', $this->var11])
//            ->andFilterWhere(['like', 'var12', $this->var12])
//            ->andFilterWhere(['like', 'var13', $this->var13])
//            ->andFilterWhere(['like', 'var14', $this->var14])
//            ->andFilterWhere(['like', 'var15', $this->var15])
//            ->andFilterWhere(['like', 'var16', $this->var16])
//            ->andFilterWhere(['like', 'var17', $this->var17])
//            ->andFilterWhere(['like', 'var18', $this->var18])
//            ->andFilterWhere(['like', 'var19', $this->var19])
//            ->andFilterWhere(['like', 'var20', $this->var20])
//            ->andFilterWhere(['like', 'var21', $this->var21])
//            ->andFilterWhere(['like', 'var22', $this->var22])
//            ->andFilterWhere(['like', 'var23', $this->var23])
//            ->andFilterWhere(['like', 'var24', $this->var24])
//            ->andFilterWhere(['like', 'var25', $this->var25])
//            ->andFilterWhere(['like', 'var26', $this->var26])
//            ->andFilterWhere(['like', 'var27', $this->var27])
//            ->andFilterWhere(['like', 'var28', $this->var28])
//            ->andFilterWhere(['like', 'var29', $this->var29])
//            ->andFilterWhere(['like', 'var30', $this->var30])
//            ->andFilterWhere(['like', 'var31', $this->var31])
//            ->andFilterWhere(['like', 'var32', $this->var32]);

        return $dataProvider;
    }
}
