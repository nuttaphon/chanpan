<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ActCategory;
use common\lib\sdii\SDFunc;
/**
 * ActCategorySearch represents the model behind the search form about `backend\models\ActCategory`.
 */
class ActCategorySearch extends ActCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['act_id', 'act_order', 'act_type', 'parent_id', 'ezform_count', 'status', 'user_create', 'user_update'], 'integer'],
            [['act_name', 'act_detail', 'date_create', 'date_update'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchCustom($params, $type=1)
    {
	$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
	
	$name = '';
	if(!empty($params)){
//	    \yii\helpers\VarDumper::dump($params,10,true);
//	    Yii::$app->end();
	   if(isset($params['ActCategorySearch'])){
	       $name = $params['ActCategorySearch']['act_name'];
	   }
	   
	}
	if (empty($name)) {
	    return SDFunc::getTaxonomyDataProvider($type);
	} else{
	    $params['ActCategorySearch']['act_type'] = $type;
	    return $this->search($params);
	}
	
    }
    
    public function search($params)
    {
        $query = ActCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'act_id' => $this->act_id,
            'act_order' => $this->act_order,
            'act_type' => $this->act_type,
            'parent_id' => $this->parent_id,
            'ezform_count' => $this->ezform_count,
            'status' => $this->status,
            'user_create' => $this->user_create,
            'date_create' => $this->date_create,
            'user_update' => $this->user_update,
            'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'act_name', $this->act_name])
            ->andFilterWhere(['like', 'act_detail', $this->act_detail]);

        return $dataProvider;
    }
}
