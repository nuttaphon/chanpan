<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_allow".
 *
 * @property integer $user_id
 * @property string $sitecode
 */
class UserAllow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_allow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sitecode'], 'required'],
            [['user_id'], 'integer'],
            [['sitecode'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'sitecode' => 'Sitecode',
        ];
    }
}
