<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ezform".
 *
 * @property string $ezf_id
 * @property string $ezf_name
 * @property string $ezf_detail
 * @property string $ezf_table
 * @property string $user_create
 * @property string $create_date
 * @property string $user_update
 * @property string $update_date
 * @property integer $status
 * @property integer $shared
 * @property integer $public_listview
 * @property integer $public_edit
 * @property integer $public_delete
 * @property string $comp_id_target
 *
 * @property ActFormRelation[] $actFormRelations
 * @property ActCategory[] $acts
 * @property EzformAssign[] $ezformAssigns
 * @property User[] $users
 * @property EzformCoDev[] $ezformCoDevs
 * @property EzformComponent[] $ezformComponents
 * @property EzformFields[] $ezformFields
 * @property EzformTarget[] $ezformTargets
 */
class Ezform extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'ezf_detail', 'ezf_table', 'user_create', 'create_date', 'user_update', 'update_date', 'status'], 'required'],
            [['ezf_id', 'user_create', 'user_update', 'status', 'shared', 'public_listview', 'public_edit', 'public_delete', 'comp_id_target'], 'integer'],
            [['ezf_detail'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['ezf_name', 'ezf_table'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_id' => 'รหัสฟอร์ม',
            'ezf_name' => 'ชื่อฟอร์ม',
            'ezf_detail' => 'รายละเอียด',
            'ezf_table' => 'ตาราง',
            'user_create' => 'ผู้สร้าง',
            'create_date' => 'วันที่สร้าง',
            'user_update' => 'ผู้แก้ไข',
            'update_date' => 'วันที่แก้ไข',
            'status' => 'สถานะ',
            'shared' => 'private = 0, public = 1',
            'public_listview' => 'อนุญาตให้ view List Data table ของฟอร์ม',
            'public_edit' => 'อนุญาตให้ Edit/Update List Data table ของฟอร์ม',
            'public_delete' => 'อนุญาตให้ Remove List Data table ของฟอร์ม',
            'comp_id_target' => 'เป้าหมายถ้าไม่เลือกคือตัวเอง',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActFormRelations()
    {
        return $this->hasMany(ActFormRelation::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActs()
    {
        return $this->hasMany(ActCategory::className(), ['act_id' => 'act_id'])->viaTable('act_form_relation', ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformAssigns()
    {
        return $this->hasMany(EzformAssign::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('ezform_assign', ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformCoDevs()
    {
        return $this->hasMany(EzformCoDev::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformComponents()
    {
        return $this->hasMany(EzformComponent::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformFields()
    {
        return $this->hasMany(EzformFields::className(), ['ezf_id' => 'ezf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzformTargets()
    {
        return $this->hasMany(EzformTarget::className(), ['ezf_id' => 'ezf_id']);
    }
}
