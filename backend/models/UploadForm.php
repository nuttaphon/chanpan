<?php

namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $excelFile;
    public $ezf_id;

    public function rules()
    {
        return [
            [['excelFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx'],
            [['ezf_id'], 'string'],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $this->excelFile->saveAs(Yii::$app->basePath. '/uploads/' . urlencode($this->excelFile->name));
            return true;
        } else {
            return false;
        }
    }
}
?>