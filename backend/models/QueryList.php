<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "query_list".
 *
 * @property string $id
 * @property string $query_id
 * @property string $user_id_by
 * @property string $user_id_to
 * @property string $create_at
 * @property string $update_at
 * @property integer $status
 */
class QueryList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'query_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query_id', 'user_id_by', 'user_id_to', 'status'], 'integer'],
            [['create_at', 'update_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'query_id' => 'Query ID',
            'user_id_by' => 'User Id By',
            'user_id_to' => 'User Id To',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'status' => 'Status',
        ];
    }
}
