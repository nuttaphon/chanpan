<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "script_results".
 *
 * @property integer $rid
 * @property integer $gid
 * @property integer $sid
 * @property integer $aid
 * @property string $title
 * @property string $name
 * @property string $description
 * @property string $result
 * @property integer $rating
 * @property string $result_at
 */
class ScriptResults extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'script_results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gid', 'sid', 'aid', 'title', 'name', 'description', 'rating', 'result_at'], 'required'],
            [['gid', 'sid', 'aid', 'rating'], 'integer'],
            [['title', 'result'], 'string'],
            [['result_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rid' => Yii::t('app', 'Rid'),
            'gid' => Yii::t('app', 'Gid'),
            'sid' => Yii::t('app', 'Sid'),
            'aid' => Yii::t('app', 'Aid'),
            'title' => Yii::t('app', 'Title'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'result' => Yii::t('app', 'Result'),
            'rating' => Yii::t('app', 'Rating'),
            'result_at' => Yii::t('app', 'Result At'),
        ];
    }
}
