<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "act_category".
 *
 * @property string $act_id
 * @property integer $act_order
 * @property string $act_name
 * @property string $act_detail
 * @property integer $act_type
 * @property string $parent_id
 * @property integer $ezform_count
 * @property integer $status
 * @property string $user_create
 * @property string $date_create
 * @property string $user_update
 * @property string $date_update
 *
 * @property ActCategory $parent
 * @property ActCategory[] $actCategories
 * @property ActFormRelation[] $actFormRelations
 * @property Ezform[] $ezfs
 */
class ActCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'act_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['act_name'], 'required'],
            [['act_id', 'act_order', 'act_type', 'parent_id', 'ezform_count', 'status', 'user_create', 'user_update'], 'integer'],
            [['act_detail'], 'string'],
	    [['act_order', 'parent_id', 'ezform_count'],'default','value'=>'0'],
	    [['act_type', 'status'],'default','value'=>'1'],
            [['date_create', 'date_update'], 'safe'],
            [['act_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'act_id' => Yii::t('app', 'รหัส'),
            'act_order' => Yii::t('app', 'Act Order'),
            'act_name' => Yii::t('app', 'ชื่อกิจกรรม'),
            'act_detail' => Yii::t('app', 'คำอธิบาย'),
            'act_type' => Yii::t('app', 'ประเภทกิจกรรม'),
            'parent_id' => Yii::t('app', 'กิจกรรมแม่'),
            'ezform_count' => Yii::t('app', 'จำนวนฟอร์ม'),
            'status' => Yii::t('app', 'สถานะ'),
            'user_create' => Yii::t('app', 'ผู้สร้าง'),
            'date_create' => Yii::t('app', 'วันที่สร้าง'),
            'user_update' => Yii::t('app', 'ผู้แก้ไข'),
            'date_update' => Yii::t('app', 'วันที่แก้ไข'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ActCategory::className(), ['act_id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActCategories()
    {
        return $this->hasMany(ActCategory::className(), ['parent_id' => 'act_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActFormRelations()
    {
        return $this->hasMany(ActFormRelation::className(), ['act_id' => 'act_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzfs()
    {
        return $this->hasMany(Ezform::className(), ['ezf_id' => 'ezf_id'])->viaTable('act_form_relation', ['act_id' => 'act_id']);
    }

    /**
     * @inheritdoc
     * @return ActCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActCategoryQuery(get_called_class());
    }
}
