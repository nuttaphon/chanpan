<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "f_person".
 *
 * @property string $sitecode
 * @property string $HOSPCODE
 * @property string $CID
 * @property string $PID
 * @property string $HID
 * @property string $PreName
 * @property string $Pname
 * @property string $Name
 * @property string $Lname
 * @property string $HN
 * @property string $sex
 * @property string $Birth
 * @property string $Mstatus
 * @property string $Occupation_Old
 * @property string $Occupation_New
 * @property string $Race
 * @property string $Nation
 * @property string $Religion
 * @property string $Education
 * @property string $Fstatus
 * @property string $Father
 * @property string $Mother
 * @property string $Couple
 * @property string $Vstatus
 * @property string $MoveIn
 * @property string $Discharge
 * @property string $Ddischarge
 * @property string $ABOGROUP
 * @property string $RHGROUP
 * @property string $Labor
 * @property string $PassPort
 * @property string $TypeArea
 * @property string $D_Update
 * @property string $ptlink
 */
class FPerson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'f_person';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dbtdc2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sitecode', 'HOSPCODE', 'PID'], 'required'],
            [['Birth', 'MoveIn', 'Ddischarge', 'D_Update'], 'safe'],
            [['sitecode', 'HOSPCODE'], 'string', 'max' => 9],
            [['CID', 'Father', 'Mother', 'Couple'], 'string', 'max' => 26],
            [['PID', 'HID', 'TypeArea'], 'string', 'max' => 11],
            [['PreName', 'Race', 'Nation'], 'string', 'max' => 3],
            [['Pname'], 'string', 'max' => 50],
            [['Name', 'Lname'], 'string', 'max' => 100],
            [['HN'], 'string', 'max' => 18],
            [['sex', 'Mstatus', 'Fstatus', 'Vstatus', 'Discharge'], 'string', 'max' => 1],
            [['Occupation_Old', 'Occupation_New'], 'string', 'max' => 4],
            [['Religion', 'Education'], 'string', 'max' => 2],
            [['ABOGROUP'], 'string', 'max' => 20],
            [['RHGROUP'], 'string', 'max' => 5],
            [['Labor'], 'string', 'max' => 10],
            [['PassPort'], 'string', 'max' => 8],
            [['ptlink'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sitecode' => 'Sitecode',
            'HOSPCODE' => 'Hospcode',
            'CID' => 'Cid',
            'PID' => 'Pid',
            'HID' => 'Hid',
            'PreName' => 'Pre Name',
            'Pname' => 'Pname',
            'Name' => 'Name',
            'Lname' => 'Lname',
            'HN' => 'Hn',
            'sex' => 'Sex',
            'Birth' => 'Birth',
            'Mstatus' => 'Mstatus',
            'Occupation_Old' => 'Occupation  Old',
            'Occupation_New' => 'Occupation  New',
            'Race' => 'Race',
            'Nation' => 'Nation',
            'Religion' => 'Religion',
            'Education' => 'Education',
            'Fstatus' => 'Fstatus',
            'Father' => 'Father',
            'Mother' => 'Mother',
            'Couple' => 'Couple',
            'Vstatus' => 'Vstatus',
            'MoveIn' => 'Move In',
            'Discharge' => 'Discharge',
            'Ddischarge' => 'Ddischarge',
            'ABOGROUP' => 'Abogroup',
            'RHGROUP' => 'Rhgroup',
            'Labor' => 'Labor',
            'PassPort' => 'Pass Port',
            'TypeArea' => 'Type Area',
            'D_Update' => 'D  Update',
            'ptlink' => 'Ptlink',
        ];
    }

    /**
     * @inheritdoc
     * @return FPersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FPersonQuery(get_called_class());
    }
}
