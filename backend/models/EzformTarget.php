<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ezform_target".
 *
 * @property string $ezf_id
 * @property string $data_id
 * @property string $comp_id
 * @property string $target_id
 * @property string $user_create
 * @property string $create_date
 * @property string $update_date
 *
 * @property Ezform $ezf
 */
class EzformTarget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ezform_target';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ezf_id', 'data_id'], 'required'],
           // [['ezf_id', 'data_id', 'comp_id', 'target_id', 'user_create'], 'integer'],
            [['create_date', 'update_date', 'ezf_id', 'data_id', 'comp_id', 'target_id', 'user_create'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ezf_id' => 'Ezf ID',
            'data_id' => 'Data ID',
            'comp_id' => 'Comp ID',
            'target_id' => 'Target ID',
            'user_create' => 'User Create',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEzf()
    {
        return $this->hasOne(Ezform::className(), ['ezf_id' => 'ezf_id']);
    }
}
