<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[EzformAssign]].
 *
 * @see EzformAssign
 */
class EzformAssignQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EzformAssign[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EzformAssign|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}