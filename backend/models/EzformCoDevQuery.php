<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[EzformCoDev]].
 *
 * @see EzformCoDev
 */
class EzformCoDevQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EzformCoDev[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EzformCoDev|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}