<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "site_url".
 *
 * @property string $code
 * @property string $name
 * @property string $url
 * @property string $create_at
 */
class SiteUrl extends \yii\db\ActiveRecord
{
    public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'value' => new Expression('NOW()'),
			]
			
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'site_url';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'url'], 'required'],
            [['created_at','updated_at'], 'safe'],
            [['code', 'status', 'favicon'], 'safe'],
            [['name'], 'string', 'max' => 255],
	    //[['code', 'status'], 'unique'],
            [['url'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'รหัสสถานบริการ'),
            'name' => Yii::t('app', 'ชื่อเวบไซท์'),
            'url' => Yii::t('app', 'ที่อยู่เวบไซท์'),
            'create_at' => Yii::t('app', 'วันที่สร้าง'),
	    'updated_at' => Yii::t('app', 'วันที่แก้ไข'),
	    'status' => Yii::t('app', 'สถานะ'),
        ];
    }

    /**
     * @inheritdoc
     * @return SiteUrlQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SiteUrlQuery(get_called_class());
    }
}
