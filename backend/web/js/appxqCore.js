/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
	timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
	timezoneClip = /[^-+\dA-Z]/g,
	pad = function (val, len) {
		val = String(val);
		len = len || 2;
		while (val.length < len) val = "0" + val;
		return val;
	};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
		d = date[_ + "Date"](),
		D = date[_ + "Day"](),
		m = date[_ + "Month"](),
		y = date[_ + "FullYear"](),
		H = date[_ + "Hours"](),
		M = date[_ + "Minutes"](),
		s = date[_ + "Seconds"](),
		L = date[_ + "Milliseconds"](),
		o = utc ? 0 : date.getTimezoneOffset(),
		flags = {
			d:    d,
			dd:   pad(d),
			ddd:  dF.i18n.dayNames[D],
			dddd: dF.i18n.dayNames[D + 7],
			m:    m + 1,
			mm:   pad(m + 1),
			mmm:  dF.i18n.monthNames[m],
			mmmm: dF.i18n.monthNames[m + 12],
			yy:   String(y).slice(2),
			yyyy: y,
			h:    H % 12 || 12,
			hh:   pad(H % 12 || 12),
			H:    H,
			HH:   pad(H),
			M:    M,
			MM:   pad(M),
			s:    s,
			ss:   pad(s),
			l:    pad(L, 3),
			L:    pad(L > 99 ? Math.round(L / 10) : L),
			t:    H < 12 ? "a"  : "p",
			tt:   H < 12 ? "am" : "pm",
			T:    H < 12 ? "A"  : "P",
			TT:   H < 12 ? "AM" : "PM",
			Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
			o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
			S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
		};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
	"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
	"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
	"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
	"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};
//================End Function Dateformat=============================================

var clearFormElements = function(ele){
	$(ele).find(':input').each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
		if (type == 'text' || type == 'password' || tag == 'textarea')
			this.value = '';
		else if (type == 'checkbox' || type == 'radio')
			this.checked = false;
		else if (tag == 'select')
			this.value = '';
	});
}

var resetFormEleCustom = function(ele){
	$(ele).find(':input').each(function() {
		var type = this.type;
		
		if (type == 'radio' || type == 'checkbox'){
			if(this.checked){
				$('#'+this.id+'_txt').attr('disabled', false);
			}else{
				$('#'+this.id+'_txt').attr('disabled', true);
			}
		}
	});
}

//first default  $('select option:first-child').attr("selected", "selected");
//==================End Function===================================================

var setFormElements = function(data, model){
	for(var key in data){
		
		var type_id = $('#'+model+'_'+key).attr('type');
		var type_name = $('input[name="'+model+'['+key+']"][value="'+ data[key] +'"]').attr('type');
		var tag = $('#'+model+'_'+key).prop('tagName');
		var type_list = $('input[name="'+model+'['+key+'][]"]').attr('type');

		if (type_id == 'text' || type_id == 'hidden' || type_id == 'password' || tag == 'TEXTAREA'){
			$('#'+model+'_'+key).val(data[key]);
		}
		
		if (type_name == 'checkbox' || type_name == 'radio'){
			$('input[name="'+model+'['+key+']"][value="'+ data[key] +'"]').attr('checked', true);
		}
		
		if (type_list == 'checkbox'){
			var arr = jQuery.parseJSON(data[key]);
			for(var akey in arr){	
				$('input[name="'+model+'['+key+'][]"][value="'+ arr[akey] +'"]').attr('checked', true);
			}
		}
		
		if (tag == 'SELECT'){
			if($('#'+model+'_'+key).attr('multiple')=='multiple'){
				var arr = jQuery.parseJSON(data[key]);
				
				for(var akey in arr){		
					$('#'+model+'_'+key+' option[value="'+ arr[akey] +'"]').attr('selected', true);
				}
			}
			else{
				$('#'+model+'_'+key).val(data[key]);
			}
			
		}
	}
}
//==================End Function===================================================


/*
 * Constructor ตัวอย่างการเขียน Javascript แบบ OOP
 */
var Constructor = function(name) {
	// initialization here
	// PRIVATE VARIABLES AND FUNCTIONS 
	// ONLY PRIVELEGED METHODS MAY VIEW/EDIT/INVOKE
	var weight=1;
	
	// PRIVILEGED METHODS 
	// MAY BE INVOKED PUBLICLY AND MAY ACCESS PRIVATE ITEMS 
	// MAY NOT BE CHANGED; MAY BE REPLACED WITH PUBLIC FLAVORS 
	this.toString = function(){
		return 'myName';
	} 
	
	// PUBLIC PROPERTIES -- ANYONE MAY READ/WRITE 
	this.myname = name
};
// PUBLIC METHODS -- ANYONE MAY READ/WRITE 
// PROTOTYOPE PROERTIES -- ANYONE MAY READ/WRITE (but may be overridden) 
Constructor.prototype =  {
	mymethod:function(){
		alert("my name is : " + this.myname+ ' '+ Constructor.app);
	}
};
//==================End Function===================================================
var checkAge = function(dob) {
	var today = new Date(); 
	if (!/\d{2}\/\d{2}\/\d{4}/.test(dob)) {   // check valid format
		return false;
	}

	dob = dob.split("/");
	var byr = parseInt(dob[2]-543); 
	var nowyear = today.getFullYear();
	if (byr >= nowyear || byr < 1900) {  // check valid year
		return false;
	}
	var bmth = parseInt(dob[1],10)-1;   // radix 10!
	if (bmth <0 || bmth >11) {  // check valid month 0-11
		return false;
	}
	var bdy = parseInt(dob[0],10);   // radix 10!
	var dim = daysInMonth(bmth+1,byr);
	if (bdy <1 || bdy > dim) {  // check valid date according to month
		return false;
	}

	var age = nowyear - byr;
	var nowmonth = today.getMonth();
	var nowday = today.getDate();
	if (bmth > nowmonth) {
		age = age - 1
	}  // next birthday not yet reached
	else if (bmth == nowmonth && nowday < bdy) {
		age = age - 1
	}

	return age;
}

function daysInMonth(month,year) {  // months are 1-12
	var dd = new Date(year, month, 0);
	return dd.getDate();
} 

function addContentWithSort(html, eachBy, dataId, dataSort){
	var tmpId = '';
	var valueId = '';
	var valueSort = '';
	var beforeStatus = false;
	$(eachBy).each(function(){
		tmpId = this.id;
		valueId = parseInt($('#'+this.id).attr('data-id'));
		valueSort = parseInt($('#'+this.id).attr('data-sort'));
		
		if(dataSort < valueSort){
			beforeStatus = true;
			return false;
		} else if(dataSort == valueSort){
			if(dataId < valueId){
				beforeStatus = true;
			}
			return false;
		}
	});
	if(tmpId){
		if(beforeStatus){
			$("#"+tmpId).before(html);
		}else{
			$("#"+tmpId).after(html);
		}
	}else{
		var arrID = eachBy.split(' ');
		$(arrID[0]).append(html);
	}
	
}

function removeContentGroup(eachBy, dataGroup){
	var valueG = '';
	var delStatus = true;
	if(dataGroup){
		$(eachBy+' .itemQ').each(function(){
			valueG = $(this).attr('data-group');
			if(valueG == dataGroup){
				delStatus = false;
				return false;
			}
		});

		if(delStatus){
			$(eachBy+' .groupQ').each(function(){
				valueG = $(this).attr('data-group');
				if(valueG == dataGroup){
					$(this).remove();
				}
			});
		}
	}
	
}

function toDayDiff(sDate){
	var dateArr = sDate.split("-");
	var dayArr = dateArr[2].split(" ");
	var dataDate = new Date(dateArr[0], dateArr[1]-1, dayArr[0]);
	var nowDate = new Date();
	if(nowDate.format('isoDate')==dataDate.format('isoDate')){
		return true;
	}
	return false;
}