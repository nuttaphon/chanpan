   function addComponentField(rowItem, form){
    var Url = baseUrlTo+'/component/insertfield?forder=' + rowItem + '&component_id=' + $('#js-component').val();
        $.post(
            Url, //serialize Yii2 form
            form.serialize()
            ).done(function(result){
                if(result.status == 'success'){
                    location.reload();
                }else{
                    $.unblockUI();
                    var noty_id = noty({"text":result.message, "type":result.status});
                    $('button[type=submit]').removeAttr('disabled');
                    $('button[type=submit]').html('สร้างคำถาม');
                    $('#ezformfields-ezf_field_name').attr('style','border-color:1px red solid !important;width:150px;color:red;');
                    $('#errorAddField').html('<span style="color:red">&nbsp;&nbsp;ตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณากรอกใหม่</span>');
                }
            }).fail(function(){
                console.log('server error');
        });
   }
   function delComponentField(fieldid){
    $.post(baseUrlTo+'/component/formdelete?id='+fieldid,function(result){
        $.unblockUI();
            ". SDNoty::show('result.message', 'result.status') ."
            $('div[item-id='+result.data+']').remove();
        });
   }
   function updateComponentField(ezf_id, form){

    var Url = baseUrlTo+'/component/updatefield?id='+ezf_id;
        $.post(
            Url, //serialize Yii2 form
            form.serialize()
        ).done(function(result){
            if(result.status == 'success'){
                     location.reload();
             }else{
                $.unblockUI();
                 var noty_id = noty({"text":result.message, "type":result.status});
                 $('button[type=submit]').removeAttr('disabled');
                 $('button[type=submit]').html('สร้างคำถาม');
                 $('#ezformfields-ezf_field_name').attr('style','border-color:1px red solid !important;width:150px;color:red;');
                 $('#errorAddField').html('<span style="color:red">&nbsp;&nbsp;ตัวแปรนี้มีอยู่ในฐานข้อมูลแล้วกรุณากรอกใหม่</span>');
             }
        }).fail(function(){
            console.log('server error');
        });
   }
