function addGridField(rowItem,form){
    var Url = baseUrlTo+'/grid/insertfield?forder='+rowItem;
    $.post(
        Url, //serialize Yii2 form
        form.serialize()
    ).done(function(result){
            if(result.status == 'success'){
                location.reload();
            }else if(result.status == 'uniquevalue'){
                $.unblockUI();
                var noty_id = noty({"text":result.message, "type":result.action,"layout":'center'});
                $('button[type=submit]').removeAttr('disabled');
                $('button[type=submit]').html('สร้างคำถาม');

            }else{
                $.unblockUI();
                console.log('///');
            }
        }).fail(function(){
            console.log('server error');
        });
}
function delGridField(fieldid){
    $.post(baseUrlTo+'/grid/deletegrid?id='+fieldid,function(result){
        $.unblockUI();
        //console.log(result);
        $('div[item-id='+result.data+']').remove();
    });
}
function updateGridField(ezf_id,form){

    var Url = baseUrlTo+'/grid/updatefield?id='+ezf_id;

    $.post(
        Url, //serialize Yii2 form
        form.serialize()
    ).done(function(result){
            if(result.status == 'success'){
                location.reload();
            }else if(result.status == 'uniquevalue'){
                $.unblockUI();
                var noty_id = noty({"text":result.message, "type":result.action,"layout":'center'});
                $('button[type=submit]').removeAttr('disabled');
                $('button[type=submit]').html('สร้างคำถาม');

            }else{
                $.unblockUI();
                console.log('///');
            }
        }).fail(function(){
            console.log('server error');
        });
}