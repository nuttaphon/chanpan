<?php
return [
    'id' => 'backend',
    'basePath' => dirname(__DIR__),
    'components' => [
        'urlManager'=>require(__DIR__.'/_urlManager.php')
    ],
    'bootstrap' => ['backend\components\AppComponent'],
    'modules' => [
        'guide' => [
            'class' => 'backend\modules\guide\Module',
        ],
    ],
];
