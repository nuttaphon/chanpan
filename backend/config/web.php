<?php

$config = [
    'timeZone' => 'Asia/Bangkok',
    'homeUrl'=>Yii::getAlias('@backendUrl'),
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute'=>'timeline-event/index',
    'controllerMap'=>[
        'file-manager-elfinder' => [
            'class' => 'mihaildev\elfinder\Controller',
            'access' => ['manager'],
            'disabledCommands' => ['netmount'],
            'roots' => [
                [
                    'baseUrl' => '@storageUrl',
                    'basePath' => '@storage',
                    'path'   => '/',
                    'access' => ['read' => 'manager', 'write' => 'manager']
                ]
            ]
        ]
    ],
    'components'=>[
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'cookieValidationKey' => getenv('BACKEND_COOKIE_VALIDATION_KEY')
        ],
        'user' => [
            'class'=>'yii\web\User',
            'identityClass' => 'common\models\User',
            'loginUrl'=>['sign-in/login'],
            'enableAutoLogin' => true,
            'authTimeout' => 86400, //Seconds **
            'as afterLogin' => 'common\behaviors\LoginTimestampBehavior'
        ],
        'db2' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=www1.cascap.in.th;dbname=v04cascap',
            'username' => 'cascapcloud',
            'password' => 'cascapcloud!@#$%',
            'charset' => 'utf8',
        ],
    ],
    'modules'=>[
    'comments'=>[
            'class' => 'backend\modules\comments\Comments',
        ],//nut
        'line' => [
            'class' => 'backend\modules\line\Module',
        ],        
        'mailing' => [
            'class' => 'backend\modules\mailing\Module',
        ],
        'ckd' => [
            'class' => 'backend\modules\ckd\Module',
        ],        
        'ckdnet' => [
            'class' => 'backend\modules\ckdnet\Module',
        ],        
	'test' => [
            'class' => 'backend\modules\test\Module',
        ],
        'dpm' => [
     'class' => 'backend\modules\dpm\Module',
 ],
        'thaipalliative' => [
    'class' => 'backend\modules\thaipalliative\Module',
        ],
        'project84' => [
    'class' => 'backend\modules\project84\Module',
        ],
        'forum' => [
            'class' => 'backend\modules\forum\Module',
        ],
        'i18n' => [
            'class' => 'backend\modules\i18n\Module',
            'defaultRoute'=>'i18n-message/index'
        ],
        'ezforms' => [
            'class' => 'backend\modules\ezforms\Module',
        ],
	'ezforms2' => [
            'class' => 'backend\modules\ezforms2\Module',
        ],
		'article' => [
            'class' => 'app\modules\article\Module',
        ],
        'managedata' => [
            'class' => 'backend\modules\managedata\Module',
        ],
        'component' => [
            'class' => 'backend\modules\component\Module',
        ],
        'dynagrid'=> [
             'class'=>'\kartik\dynagrid\Module',
             // other module settings
            'defaultPageSize'=>100,
         ],
         'gridview'=> [
             'class'=>'\kartik\grid\Module',
             // other module settings
         ],
        'treemanager' =>  [
             'class' => '\kartik\tree\Module',
         ],
        'webboard' => [
            'class' => 'backend\modules\webboard\Module',
        ],
        'notification' => [
            'class' => 'backend\modules\notification\Module',
        ],
        'tccbot' => [
            'class' => 'backend\modules\tccbot\Module',
        ],
	'ovcca' => [
            'class' => 'backend\modules\ovcca\Module',
        ],
	'ov2' => [
            'class' => 'backend\modules\ov2\Module',
        ],
	'dashboard' => [
            'class' => 'backend\modules\dashboard\Module',
        ],
        'refer' => [

            'class' => 'backend\modules\refer\Module',

        ],
	'inv' => [
            'class' => 'backend\modules\inv\Module',
        ],
	'report' => [
            'class' => 'backend\modules\report\Module',
        ],
        'ultrasound' => [
                'class' => 'backend\modules\ultrasound\Module',
        ],
        /*
         * Module US Finding.
         * Developed by Mark.
         * Date : 2016-03-01
        */
        'usfinding' => [
            'class' => 'backend\modules\usfinding\Module',
        ],
        /*
         * Module Follow up.
         * Developed by Mark.
         * Date : 2016-04-01
        */
        'appointments' => [
            'class' => 'backend\modules\appointments\Module',
        ],
        /*
         * Module Add doctor.
         * Developed by Mark.
         * Date : 2016-04-25
        */
        'adddoctor' => [
            'class' => 'backend\modules\adddoctor\Module',
        ],


        'icf' => [

            'class' => 'backend\modules\icf\Module',

        ],
	'surgery' => [
            'class' => 'backend\modules\surgery\Module',
        ],
        'tccadmin' => [
            'class' => 'backend\modules\tccadmin\Module',
        ],
        'coc' => [
            'class' => 'backend\modules\coc\Module',
        ],
	'cpay' => [
            'class' => 'backend\modules\cpay\Module',
        ],

	'testform' => [
            'class' => 'backend\modules\testform\Module',
	    ],
        'teleradio' => [
            'class' => 'backend\modules\teleradio\Module',

        ],
        'ezdata' => [
            'class' => 'backend\modules\ezdata\Module',
        ],
        'level' => [
            'class' => 'backend\modules\level\Module',
        ],
    ],
    'as globalAccess'=>[
        'class'=>'\common\behaviors\GlobalAccessBehavior',
        'rules'=>[
	    [
                'controllers'=>['script-validate'],
                'allow' => true,
                'roles' => ['administrator'],
            ],
            [
                'controllers'=>['move-units'],
                'allow' => true,
                'roles' => ['administrator'],
            ],
            [
                'controllers'=>['move-units'],
                'allow' => true,
                'roles' => ['@'],
                'actions'=>['create']
            ],
            [
                'controllers'=>['move-units'],
                'allow' => false,
            ],

            [
                'controllers'=>['sign-in'],
                'allow' => true,
                'roles' => ['?'],
                'actions'=>['login']
            ],
            [
                'controllers'=>['sign-in'],
                'allow' => true,
                'roles' => ['@'],
                'actions'=>['show-user-panel']
            ],
            [
                'controllers'=>['sign-in'],
                'allow' => true,
                'roles' => ['@'],
                'actions'=>['profile']
            ],
            [
                'controllers'=>['sign-in'],
                'allow' => true,
                'roles' => ['@'],
                'actions'=>['account', 'avatar-upload']
            ],
            [
                'controllers'=>['sign-in'],
                'allow' => true,
                'roles' => ['@'],
                'actions'=>['logout']
            ],
            [
                'controllers'=>['sign-in'],
                'allow' => true,
                'roles' => ['@'],
                'actions'=>['findhospital']
            ],
            [
                'controllers'=>['site'],
                'allow' => true,
                'roles' => ['?', '@'],
                'actions'=>['error']
            ],
	    
            [
                'controllers'=>['debug/default'],
                'allow' => true,
                'roles' => ['?'],
            ],
            [
                'controllers'=>['user'],
                'allow' => true,
                'roles' => ['administrator'],
            ],
            [
                'controllers'=>['user/view'],
                'allow' => true,
                'roles' => ['manager'],
            ],
            [
                'controllers'=>['user'],
                'allow' => false,
            ],
            [
                'allow' => true,
                'roles' => ['manager'],
            ]
        ]
    ]
];

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'class'=>'yii\gii\Module',
        'generators' => [
            'crud' => [
                'class'=>'yii\gii\generators\crud\Generator',
                'templates'=>[
                    'yii2-starter-kit' => Yii::getAlias('@backend/views/_gii/templates'),
		    'mycrud' => '@common/templates/mycrud'
                ],
                'messageCategory' => 'backend'
            ]
        ]
    ];

}

return $config;
